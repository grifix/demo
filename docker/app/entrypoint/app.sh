#!/bin/bash
#
# (c) Mike Shapovalov <smike.mbx@gmail.com>
# For the full copyright and license information, please view the LICENSE file that was
# distributed with this source code.
#

directory=/var/www
echo "starting app..."
chmod -R 777 ${directory}
chown -R www-data ${directory}
chmod +x ${directory}/start.sh
chmod +x ${directory}/stop.sh
source /etc/apache2/envvars && exec /usr/sbin/apache2 -DFOREGROUND
true > tmp/app.log
true > tmp/cron.log
