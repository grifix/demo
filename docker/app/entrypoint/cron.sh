#!/bin/bash

sleep 60
printenv | grep -v "no_proxy" >> /etc/environment

touch /var/www/tmp/cron.log
crontab /var/www/crontab
cron -f
