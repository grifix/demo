#!/bin/bash

#
# (c) Mike Shapovalov <smike.mbx@gmail.com>
# For the full copyright and license information, please view the LICENSE file that was
# distributed with this source code.
#

sleep 60
printenv | grep -v "no_proxy" >> /etc/environment

/var/www/cli shared:event:subscriber_starter_process
