<?php declare(strict_types = 1);

/** Namespaces for auto wiring */
return [
    'Grifix',
    'App'
];
