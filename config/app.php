<?php
declare(strict_types = 1);
namespace {

    use Grifix\Demo\Application\Command\Test\TestCommand as TestCommandAlias;

    return [
        'grifix' => [
            'kit' => [
                'queue' => [
                    'async' => true
                ],
                'cqrs' => [
                    'asyncCommands' => [
                        TestCommandAlias::class
                    ]
                ],
                'http' => [
                    'host' => sprintf(
                        'http://%s.%s:%s',
                        getenv('PROJECT_NAME'),
                        getenv('PROJECT_DOMAIN'),
                        getenv('HTTP_PORT')
                    ),
                ],
                'db' => [
                    'default' => [
                        'host' => 'db',
                        'user' => getenv('POSTGRES_USER'),
                        'db' => getenv('POSTGRES_DB'),
                        'password' => getenv('POSTGRES_PASSWORD'),
                    ]
                ]
            ],
        ],
    ];
}
