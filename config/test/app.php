<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace {

    use Grifix\Shared\Domain\Currency\CurrencyInterface;

    return [
        'grifix' => [
            'shared' => [
                'currency' => [
                    CurrencyInterface::USD => true
                ]
            ],
            'kit' => [
                'db' => [
                    'default' => [
                        'host' => 'db_test',
                    ],
                    'concurrent' => [
                        'type' => 'postgres',
                        'host' => 'db_test',
                        'user' => getenv('POSTGRES_USER'),
                        'password' => getenv('POSTGRES_PASSWORD'),
                        'db' => getenv('POSTGRES_DB'),
                        'port' => '5432',
                    ]
                ],
                'queue' => [
                    'rabbit' => [
                        'host' => getenv('RABBITMQ_HOST').'_test',
                        'port' => 5672,
                        'user' => getenv('RABBITMQ_USER'),
                        'pass' => getenv('RABBITMQ_PASS')
                    ]
                ],

                'intl' => [
                    'config' => [
                        'enabledLocales' => ['ru_RU', 'en_US'],
                        'enabledLangs' => ['ru', 'en']
                    ]
                ]
            ],
        ],
    ];
}
