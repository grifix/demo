<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace {

    use Grifix\Kit\Alias;
    use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\InMemoryProcessStarter;
    use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\ProcessStarter;
    use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\ProcessStarterInterface;
    use Grifix\Kit\Ioc\IocContainerInterface;
    use Grifix\Kit\Mailer\MailerFactoryInterface;
    use Grifix\Kit\Mailer\MailerInterface;
    use Grifix\Kit\MessageBroker\MessageBrokerFactoryInterface;
    use Grifix\Kit\MessageBroker\MessageBrokerInterface;
    use Grifix\Shared\Test\Common\Stub\Mailer\FakeMailer;

    return [
        MessageBrokerInterface::class => function () {
            /**@var $this IocContainerInterface */
            if (getenv('GRIFIX_SYNC_QUEUE_IN_TESTS')) {
                return $this->get(MessageBrokerFactoryInterface::class)->createSyncMessageBroker();
            } else {
                return $this->get(MessageBrokerFactoryInterface::class)->createDelayedMessageBroker();
            }
        },

        ProcessStarterInterface::class => function (): ProcessStarterInterface {
            /**@var $this IocContainerInterface */
            if (getenv('GRIFIX_SYNC_QUEUE_IN_TESTS')) {
                return $this->get(InMemoryProcessStarter::class);
            } else {
                return $this->get(ProcessStarter::class);
            }
        },

        MailerInterface::class => function () {
            /**@var $this IocContainerInterface */
            return new FakeMailer($this->get(MailerFactoryInterface::class)->create(), $this->get(Alias::TMP_DIR));
        },
    ];
}
