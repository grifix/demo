<?php
declare(strict_types=1);

namespace {

    use Grifix\Kit\Cache\Adapter\MemCacheServerDto;

    return [
        'main' => new MemCacheServerDto(
            'memcached'
        )
    ];
}
