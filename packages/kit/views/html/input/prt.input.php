<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);
/**@var $this \Grifix\Kit\View\ViewInterface */
$description = $this->getTextVar('description');
$placeholder = $this->getTextVar('placeholder');
if ($placeholder && $this->getVar('required')) {
    $placeholder = $placeholder . '*';
}
?>
<input
        <?php if($description):?>
            data-opt="<?=$this->jsonEncode(['content'=>$this->getVar('description'), 'position' =>'w'])?>"
        <?php endif;?>
        type="<?= $this->getTextVar('type', 'text') ?>"
        <?php if ($this->getVar('required')): ?>required="required"<?php endif ?>
        data-role="<?= $this->getTextVar('dataRole') ?>"
        class="<?= $this->getTextVar('class', 'form-input') ?> <?php if($description):?> wg-grifix_kit_tooltip<?php endif;?>"
        name="<?= $this->getTextVar('name') ?>"
        placeholder="<?= $placeholder?>"
        value="<?= $this->getTextVar('value') ?>"
>
