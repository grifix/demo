<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\ViewInterface */
$description = $this->getTextVar('description');
?>
<input
    style="display: none"
    <?php if($description):?>
        data-opt="<?=$this->jsonEncode(['content'=>$this->getVar('description'), 'opensOn'=>'Hover', 'position' =>'LeftCenter'])?>"
    <?php endif;?>
    type="file"
    class="wg-grifix_kit_Uploader <?php if($description):?> wg-grifix_kit_tooltip<?php endif;?>"
    name="<?=$this->getTextVar('name')?>">
