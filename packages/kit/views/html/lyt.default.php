<?php
declare(strict_types=1);
/**@var $this \Grifix\Kit\View\ViewInterface */
?>
<html>
<head>
    <title><?php $this->startBlock('title') ?>Grifix<?php $this->endBlock() ?></title>

    <?php $this->includeJsPack('vendor', [
        'jquery/dist/jquery.min.js',
        'jquery-form/dist/jquery.form.min.js',
        '{src}/grifix/kit/views/{skin}'
    ]) ?>

    <?php $this->includeCssPack('global', [
        '{src}/grifix/kit/views/{skin}'
    ]) ?>

    <?php $this->startBlock('head') ?>

    <?php $this->endBlock() ?>
    <?php $this->addCss('{src}/grifix/kit/views/{skin}/lyt.default.css') ?>

    <?php $this->includeViewJs() ?>

    <?php $this->includeViewCss() ?>
    <script type="text/javascript">
        jQuery(function () {
            gfx.initWidgets();
            gfx.setUp();
        });
    </script>
</head>
<body class="<?= $this->makeCssClass() ?>">
<?php $this->startBlock('content') ?>

<?php $this->endBlock() ?>
</body>
</html>
