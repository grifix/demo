<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\Helper\ModuleViewHelper;
    use Grifix\Kit\View\Helper\RequestViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    /**@var $exception Exception */
    $exception = $this->getVar('exception');
    $this->inherits('grifix.kit.{skin}.lyt.default');
    ?>

    <?php $this->startBlock('content') ?>


    <div class="panel">
        <div class="panel-heading">
            <?= $this->translate('grifix.kit.error') ?> <?= $this->getVar('code') ?>
        </div>
        <div class="panel-body">
            <?= $this->getVar('message') ?>
        </div>

    </div>
    <?php if ($exception->getCode() == 401 && $this->getHelper(ModuleViewHelper::class)->hasModule('grifix', 'acl')): ?>
        <?php $this->addJs('{src}/grifix/acl/views/{skin}/SignIn.js') ?>
        <?= $this->renderPartial('grifix.acl.{skin}.prt.signIn',
            ['redirect' => $this->getHelper(RequestViewHelper::class)->getRelativeUri()]) ?>
    <?php endif; ?>
    <?php if ($this->getVar('debug')): ?>
        <?= $this->renderPartial('grifix.kit.html.prt.exceptionDebug') ?>
    <?php endif; ?>


    <?php $this->endBlock() ?>

    <?php
}

