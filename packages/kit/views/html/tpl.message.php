<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    $this->inherits('grifix.kit.{skin}.lyt.default');
    ?>

    <?php $this->startBlock('content') ?>

    <div class="panel">
        <?php if ($this->getVar('title')): ?>
            <div class="panel-heading">
                <?= $this->getVar('title') ?>
            </div>
        <?php endif; ?>
        <div class="panel-body">
            <?= $this->getVar('text') ?>
        </div>

    </div>

    <?php $this->endBlock() ?>

    <?php
}

