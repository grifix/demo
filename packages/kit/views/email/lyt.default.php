<?php
declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    ?>
    <html>
    <body>
    <?php $this->startBlock('content') ?>

    <?php $this->endBlock() ?>
    </body>
    </html>
    <?php
}
