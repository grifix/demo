<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Test\Unit\Config;

use Grifix\Kit\Config\Config;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Test\Unit\AbstractTest;

class ConfigTest extends AbstractTest
{
    /**
     * @dataProvider itGetsProvider
     */
    public function testItGets(string $key, $expectedValue): void
    {
        $config = new Config([
            'fruit' => 'apple',
            'meal' => '{fruit} pie',
            'nested' => [
                'fruit' => 'banana',
                'meal' => '{nested.fruit} ice cream'
            ]
        ], new ArrayHelper());

        self::assertEquals($expectedValue, $config->get($key));
    }

    public function itGetsProvider(): array
    {
        return [
            ['fruit', 'apple'],
            ['meal', 'apple pie'],
            ['nested.fruit', 'banana'],
            ['nested.meal', 'banana ice cream'],
            ['nested', [
                'fruit' => 'banana',
                'meal' => 'banana ice cream'
            ]]
        ];
    }
}
