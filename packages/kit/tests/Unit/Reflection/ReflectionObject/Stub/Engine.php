<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Reflection\ReflectionObject\Stub;

class Engine
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
