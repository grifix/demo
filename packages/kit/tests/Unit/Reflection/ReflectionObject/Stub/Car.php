<?php
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Reflection\ReflectionObject\Stub;

class Car
{

    /**
     * @var string
     */
    private $producer;

    /**
     * @var \Grifix\Kit\Test\Unit\Reflection\ReflectionObject\Stub\Engine
     */
    private $engine;

    /**
     * Car constructor.
     * @param string $producer
     * @param Engine $engine
     */
    public function __construct(string $producer, Engine $engine)
    {
        $this->producer = $producer;
        $this->engine = $engine;
    }

    /**
     * @return string
     */
    public function getProducer(): string
    {
        return $this->producer;
    }

    /**
     * @return Engine
     */
    public function getEngine(): Engine
    {
        return $this->engine;
    }
}
