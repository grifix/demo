<?php
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Reflection\ReflectionObject;

use Grifix\Kit\Reflection\ReflectionObject\Exception\CannotCopyToAnotherClassException;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObject;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Test\Unit\Reflection\ReflectionObject\Stub\Car;
use Grifix\Kit\Test\Unit\Reflection\ReflectionObject\Stub\Engine;

class ReflationObjectTest extends AbstractTest
{
    public function testItGetsPropertyValue(): void
    {
        $car = new Car('Mercedes', new Engine(5));
        $wrapper = new ReflectionObject($car);
        self::assertEquals($car->getProducer(), $wrapper->getPropertyValue('producer'));
        self::assertEquals($car->getEngine()->getValue(), $wrapper->getPropertyValue('engine.value'));
    }

    public function testItSetsPropertyValue(): void
    {
        $car = new Car('Mercedes', new Engine(5));
        $newEngineValue = 10;
        $newCarProducer = 'Volvo';
        $wrapper = new ReflectionObject($car);
        $wrapper->setPropertyValue('producer', $newCarProducer);
        $wrapper->setPropertyValue('engine.value', $newEngineValue);
        self::assertEquals($newCarProducer, $car->getProducer());
        self::assertEquals($newEngineValue, $car->getEngine()->getValue());
    }

    public function testItCopiesTo(): void
    {
        $carA = new Car('Mercedes', new Engine(5));
        $carB = new Car('Opel', new Engine(3));
        $wrapper = new ReflectionObject($carA);
        $wrapper->copyTo($carB);
        self::assertEquals('Mercedes', $carB->getProducer());
        self::assertSame($carA->getEngine(), $carB->getEngine());
    }

    public function testItFailsWhenCopyToWrongClassObject(): void
    {
        $objectA = new Car('Mercedes', new Engine(5));
        $objectB = new Engine(3);
        $wrapper = new ReflectionObject($objectA);

        self::expectException(CannotCopyToAnotherClassException::class);
        $wrapper->copyTo($objectB);
    }

    public function testItMakesArray(): void
    {
        $object = new Car('Audi', new Engine(5));
        $wrapper = new ReflectionObject($object);
        self::assertEquals(
            [
                'producer' => 'Audi',
                'engine' => [
                    'value' => 5
                ]
            ],
            $wrapper->toArray()
        );
    }
}
