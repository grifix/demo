<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Test\Unit\Validation;

use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Validation\Validator\MoneyValidator;

class MoneyValidatorTest extends AbstractTest
{
    /**
     * @dataProvider itValidatesProvider
     */
    public function testItValidates($value, bool $expectedResult):void {
        $validator = new MoneyValidator();
        self::assertSame($expectedResult, $validator->validate($value));
    }

    public function itValidatesProvider():array {
        return [
            [1, true],
            ['1', true],
            ['1.1', true],
            ['1,1', true],
            [1.1, true],
            ['1.10', true],
            ['1,10', true],
            [1.10, true],
            ['1.111', false],
            ['1,111', false],
            [1.111, false],
            ['01', true],
            ['1,1', true],
            ['1a', false],
            ['1.1.1', false],
            ['1,1,1', false]
        ];
    }
}

