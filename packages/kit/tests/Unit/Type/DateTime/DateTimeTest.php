<?php declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Type\DateTime;

use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Type\DateTime\DateTime;

class DateTimeTest extends AbstractTest
{
    protected $date;

    public function testItAddsSeconds()
    {
        $date = new DateTime('2000-01-01 15:00:00');
        $newDate = $date->addSeconds(10);
        self::assertEquals('2000-01-01 15:00:10', $newDate->format('Y-m-d H:i:s'));
        self::assertEquals('2000-01-01 15:00:00', $date->format('Y-m-d H:i:s'));
    }
}
