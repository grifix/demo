<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Ioc\Stub;

/**
 * Class Vehicle
 *
 * @category Alias
 * @package  Alias\Ioc\Tests\Dummies
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Car implements CarInterface
{
    public $engine;
    public $body;
    public $color;
    public $tonnage;
    
    /**
     * Vehicle constructor.
     *
     * @param EngineInterface $engine
     * @param Body            $body
     * @param string          $color   <cfg:cars.color>
     * @param int             $tonnage <cfg:cars.tonnage>
     */
    public function __construct(EngineInterface $engine, Body $body, string $color, int $tonnage)
    {
        $this->engine = $engine;
        $this->body = $body;
        $this->color = $color;
        $this->tonnage = $tonnage;
    }
}
