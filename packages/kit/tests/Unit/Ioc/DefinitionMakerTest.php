<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Ioc;

use Grifix\Kit\Helper\ClassHelper;
use Grifix\Kit\Ioc\DefinitionMaker\DefinitionMaker;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinder;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Test\Unit\Ioc\Stub\Body;
use Grifix\Kit\Test\Unit\Ioc\Stub\Car;
use Grifix\Kit\Test\Unit\Ioc\Stub\CarInterface;
use Grifix\Kit\Test\Unit\Ioc\Stub\EngineInterface;
use Mockery;
use Psr\SimpleCache\CacheInterface;

/**
 * Class DefinitionMakerTest
 *
 * @category Alias
 * @package  Alias\Ioc\Tests
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DefinitionMakerTest extends AbstractTest
{
    public function testMakeDefinition()
    {
        $cache = $this->mockCache();
        $definitionMaker = new DefinitionMaker($cache, new ImplementationFinder($cache, new ClassHelper(), ['Grifix']));
        $definition = $definitionMaker->makeDefinition(CarInterface::class);
        self::assertEquals(Car::class, $definition->getClassName());
        self::assertEquals([
            EngineInterface::class,
            Body::class,
            'cfg:cars.color',
            'cfg:cars.tonnage'
        ], $definition->getArgs());
    }

    protected function mockCache(): CacheInterface
    {
        $cache = [];

        $result = Mockery::mock(CacheInterface::class);
        $result->shouldReceive('has')->andReturnUsing(function ($key) use (&$cache) {
            return array_key_exists($key, $cache);
        });
        $result->shouldReceive('get')->andReturnUsing(function ($key) use (&$cache) {
            if (array_key_exists($key, $cache)) {
                return $cache[$key];
            }
            return null;
        });
        $result->shouldReceive('set')->andReturnUsing(function ($key, $value) use (&$cache) {
            $cache[$key] = $value;
            return true;
        });
        return $result;
    }
}
