<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Ioc;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Helper\ClassHelper;
use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;
use Grifix\Kit\Ioc\DefinitionMaker\DefinitionMaker;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinder;
use Grifix\Kit\Ioc\IocContainer;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Test\Unit\Ioc\Stub\Body;
use Grifix\Kit\Test\Unit\Ioc\Stub\Car;
use Grifix\Kit\Test\Unit\Ioc\Stub\CarInterface;
use Grifix\Kit\Test\Unit\Ioc\Stub\Engine;
use Grifix\Kit\Test\Unit\Ioc\Stub\Engine2;
use Grifix\Kit\Test\Unit\Ioc\Stub\EngineInterface;
use Mockery;
use Psr\SimpleCache\CacheInterface;

/**
 * Class IocContainerTest
 *
 * @category Alias
 * @package  Alias\Kit\Ioc\Tests
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class IocContainerTest extends AbstractTest
{
    /**
     * @return CacheInterface
     */
    protected function prepareCacheStub()
    {
        $cache = [];

        $result = Mockery::mock(CacheInterface::class);
        $result->shouldReceive('has')->andReturnUsing(function ($key) use (&$cache) {
            return array_key_exists($key, $cache);
        });
        $result->shouldReceive('get')->andReturnUsing(function ($key) use (&$cache) {
            if (array_key_exists($key, $cache)) {
                return $cache[$key];
            }
            return null;
        });
        $result->shouldReceive('set')->andReturnUsing(function ($key, $value) use (&$cache) {
            $cache[$key] = $value;
            return true;
        });
        return $result;
    }

    /**
     * @return ConfigInterface|\Mockery\MockInterface
     */
    protected function prepareConfigStub()
    {
        return \Mockery::mock(ConfigInterface::class, [
            'get' => 'stub'
        ]);
    }

    public function testGet()
    {
        $config = \Mockery::mock(ConfigInterface::class);
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $config->shouldReceive('get')->once()->andReturn('red');
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $config->shouldReceive('get')->once()->andReturn(100);

        /**@var $config ConfigInterface */
        /**@var $cache CacheInterface */
        $ioc = new IocContainer(
            [],
            new DefinitionMaker(
                $this->prepareCacheStub(),
                new ImplementationFinder($this->prepareCacheStub(), new ClassHelper(), ['Grifix'])
            ),
            $config,
            new ClassHelper()
        );

        /**@var $car Car */
        $car = $ioc->get(CarInterface::class);

        self::assertInstanceOf(Engine::class, $car->engine);
        self::assertInstanceOf(Body::class, $car->body);
        self::assertEquals('red', $car->color);
        self::assertEquals(100, $car->tonnage);
    }

    public function testAddDefinition()
    {
        $ioc = new IocContainer(
            [],
            new DefinitionMaker(
                $this->prepareCacheStub(),
                new ImplementationFinder($this->prepareCacheStub(), new ClassHelper(), ['Grifix'])
            ),
            $this->prepareConfigStub(),
            new ClassHelper()
        );
        $ioc->set(EngineInterface::class, new DependencyDefinition(Engine2::class));
        self::assertInstanceOf(Engine2::class, $ioc->get(EngineInterface::class));
    }

    public function testCreateNewInstance()
    {
        $config = \Mockery::mock(ConfigInterface::class);
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $config->shouldReceive('get')->once()->andReturn('red');
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $config->shouldReceive('get')->once()->andReturn(100);
        $ioc = new IocContainer(
            [],
            new DefinitionMaker(
                $this->prepareCacheStub(),
                new ImplementationFinder($this->prepareCacheStub(), new ClassHelper(), ['Grifix'])
            ),
            $config,
            new ClassHelper()
        );
        $car = $ioc->createNewInstance(Car::class);
        self::assertInstanceOf(Engine::class, $car->engine);
        self::assertInstanceOf(Body::class, $car->body);
        self::assertEquals('red', $car->color);
        self::assertEquals(100, $car->tonnage);
    }
}
