<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\View;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelper;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Kernel\Kernel;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\Module;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\View\Asset\Asset;
use Grifix\Kit\View\Asset\AssetFactory;
use Grifix\Kit\View\Asset\Exception\NoRealPathsException;
use Grifix\Kit\View\Skin\SkinFactory;
use Mockery as m;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class AssetTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Unit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AssetTest extends AbstractTest
{
    
    /**
     * @var SkinFactory
     */
    protected $skinFactory;
    
    /**
     * @var AssetFactory
     */
    protected $assetFactory;
    
    /**
     * @var string
     */
    protected $rootDir;
    
    public function setUp():void
    {
        $this->rootDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub';
        
        $kernel = m::mock(KernelInterface::class, [
            'getAppDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'app',
            'getVendorDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'vendor',
        ]);
        
        /**@var $kernel KernelInterface */
        $this->skinFactory = new SkinFactory(
            $kernel,
            new FilesystemHelper(new Filesystem()),
            new ArrayHelper(),
            new ClassMaker(),
            'skin1'
        );

        $kernel = m::mock(KernelInterface::class)->shouldReceive('getModules')->andReturnUsing(function (){
            return [
                new Module(
                    'grifix',
                    'kit',
                    1,
                    m::mock(KernelInterface::class),
                    m::mock(FilesystemHelperInterface::class),
                    m::mock(ArrayHelperInterface::class)
                ),
                new Module(
                    'grifix',
                    'demo',
                    2,
                    m::mock(KernelInterface::class),
                    m::mock(FilesystemHelperInterface::class),
                    m::mock(ArrayHelperInterface::class)
                )
            ];
        })->getMock();
        
        $this->assetFactory = new AssetFactory(
            new ClassMaker([Asset::class => Asset::class]),
            $this->rootDir,
            new FilesystemHelper(new Filesystem()),
            $kernel
        );
        parent::setUp();
    }
    
    public function testGetRealPath()
    {
        $asset = $this->assetFactory->create('{src}/grifix/kit/views/{skin}/test.js',
            $this->skinFactory->create('skin3'));
        self::assertEquals([
            $this->rootDir . '/vendor/grifix/kit/views/skin1/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin2/test.js',
            $this->rootDir . '/app/grifix/kit/views/skin2/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin3/test.js',
            $this->rootDir . '/app/grifix/kit/views/skin3/test.js',
        ], $asset->getRealPaths());
        
        $asset = $this->assetFactory->create('vendor/grifix/kit/views/{skin}/test.js',
            $this->skinFactory->create('skin3'));
        self::assertEquals([
            $this->rootDir . '/vendor/grifix/kit/views/skin1/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin2/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin3/test.js',
        ], $asset->getRealPaths());
        
        $asset = $this->assetFactory->create('{src}/grifix/kit/views/skin3/test.js',
            $this->skinFactory->create('skin3'));
        self::assertEquals([
            $this->rootDir . '/vendor/grifix/kit/views/skin3/test.js',
            $this->rootDir . '/app/grifix/kit/views/skin3/test.js',
        ], $asset->getRealPaths());
        
        $asset = $this->assetFactory->create('app/grifix/kit/views/skin3/test.js',
            $this->skinFactory->create('skin3'));
        self::assertEquals([
            $this->rootDir . '/app/grifix/kit/views/skin3/test.js',
        ], $asset->getRealPaths());
        
        $asset = $this->assetFactory->create('{src}/grifix/kit/views/{skin}/test.js',
            $this->skinFactory->create('skin2'));
        self::assertEquals([
            $this->rootDir . '/vendor/grifix/kit/views/skin1/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin2/test.js',
            $this->rootDir . '/app/grifix/kit/views/skin2/test.js',
        ], $asset->getRealPaths());
        
        $asset = $this->assetFactory->create('{src}/grifix/kit/views/{skin}/sub',
            $this->skinFactory->create('skin3'), 'js');
        self::assertEquals([
            $this->rootDir . '/vendor/grifix/kit/views/skin1/sub/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin1/sub/test2.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin2/sub/test.js',
            $this->rootDir . '/vendor/grifix/kit/views/skin2/sub/test2.js',
            $this->rootDir . '/app/grifix/kit/views/skin3/sub/test.js',
            $this->rootDir . '/app/grifix/kit/views/skin3/sub/test2.js',
        ], $asset->getRealPaths());

        $asset = $this->assetFactory->create('app/{module}/views/{skin}/test.js',
            $this->skinFactory->create('skin3'));
        self::assertEquals([
            $this->rootDir . '/app/grifix/kit/views/skin2/test.js',
            $this->rootDir . '/app/grifix/kit/views/skin3/test.js',
            $this->rootDir . '/app/grifix/demo/views/skin2/test.js',
        ], $asset->getRealPaths());
    }
    
    public function testAssetHasNotRealPaths()
    {
        $this->expectException(NoRealPathsException::class);
        $asset = $this->assetFactory->create('{src}/grifix/kit/views/{skin}/no_file.js',
            $this->skinFactory->create('skin3'));
        $asset->getRealPaths();
    }
}
