<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\View;

use Grifix\Kit\Event\Bus\EventBus;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelper;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\Module;
use Grifix\Kit\MessageBroker\MessageBrokerInterface;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\View\Asset\Asset;
use Grifix\Kit\View\Asset\AssetCombinerFactory;
use Grifix\Kit\View\Asset\AssetFactory;
use Grifix\Kit\View\Skin\SkinFactory;
use Mockery as m;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class AssetCombinerTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Unit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AssetCombinerTest extends AbstractTest
{
    /**
     * @var SkinFactory
     */
    protected $skinFactory;

    /**
     * @var AssetFactory
     */
    protected $assetFactory;

    /**
     * @var AssetCombinerFactory
     */
    protected $assetCombinerFactory;

    /**
     * @var string
     */
    protected $rootDir;

    public function setUp():void
    {
        $this->rootDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub';

        $kernel = m::mock(KernelInterface::class, [
            'getAppDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'app',
            'getVendorDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'vendor',
        ]);

        /**@var $kernel KernelInterface */
        $this->skinFactory = new SkinFactory(
            $kernel,
            new FilesystemHelper(new Filesystem()),
            new ArrayHelper(),
            new ClassMaker(),
            'skin1'
        );

        $this->assetFactory = new AssetFactory(
            new ClassMaker([Asset::class => Asset::class]),
            $this->rootDir,
            new FilesystemHelper(new Filesystem()),
            m::mock(KernelInterface::class)->shouldReceive('getModules')->andReturn([
                new Module(
                    'grifix',
                    'kit',
                    1,
                    m::mock(KernelInterface::class),
                    m::mock(FilesystemHelperInterface::class), new ArrayHelper()
                )
            ])->getMock()
        );
        $this->assetCombinerFactory = new AssetCombinerFactory(
            new ClassMaker(),
            $this->assetFactory,
            new FilesystemHelper(new Filesystem()),
            new EventBus(m::mock(MessageBrokerInterface::class)),
            new ArrayHelper()
        );
        parent::setUp();
    }

    public function testCombine()
    {
        $assetCombiner = $this->assetCombinerFactory->create($this->skinFactory->create('skin3'));
        $assetCombiner->addAsset('{src}/grifix/kit/views/{skin}/sub/test.js');
        $assetCombiner->addAsset('{src}/grifix/kit/views/{skin}/test.js');
        $assetCombiner->addAsset('{src}/grifix/kit/views/{skin}/sub', 'js');
        self::assertEquals(
            "\n" . implode(
                "\n",
                [
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin1/sub/test.js--*/",
                    "var path = 'vendor/skin1/sub/test.js';",
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin2/sub/test.js--*/",
                    "var path = 'vendor/skin2/sub/test.js';",
                    "/*--" . $this->rootDir . "/app/grifix/kit/views/skin3/sub/test.js--*/",
                    "var path = 'app/skin1/sub/test.js';",
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin1/test.js--*/",
                    "var path = 'vendor/skin1/test.js';",
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin2/test.js--*/",
                    "var path = 'vendor/skin2/test.js';",
                    "/*--" . $this->rootDir . "/app/grifix/kit/views/skin2/test.js--*/",
                    "var path = 'app/skin2/test.js';",
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin3/test.js--*/",
                    "var path = 'vendor/skin3/test.js';",
                    "/*--" . $this->rootDir . "/app/grifix/kit/views/skin3/test.js--*/",
                    "var path = 'app/skin3/test.js';",
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin1/sub/test2.js--*/",
                    "var path = 'vendor/skin1/sub/test2.js';",
                    "/*--" . $this->rootDir . "/vendor/grifix/kit/views/skin2/sub/test2.js--*/",
                    "var path = 'vendor/skin2/sub/test2.js';",
                    "/*--" . $this->rootDir . "/app/grifix/kit/views/skin3/sub/test2.js--*/",
                    "var path = 'app/skin1/sub/test2.js';",
                ]
            ),
            $assetCombiner->combine()
        );
    }

    public function testDependency()
    {
        $assetCombiner = $this->assetCombinerFactory->create($this->skinFactory->create('depend'));
        $assetCombiner->addAsset('vendor/grifix/kit/views/depend/b.js');
        $assetCombiner->combine();
        self::assertEquals([
            $this->rootDir . '/vendor/grifix/kit/views/depend/lib.js',
            $this->rootDir . '/vendor/grifix/kit/views/depend/app.js',
            $this->rootDir . '/vendor/grifix/kit/views/depend/a.js',
            $this->rootDir . '/vendor/grifix/kit/views/depend/b.js',
        ], $assetCombiner->getPaths());
    }
}
