<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit;

use PHPUnit\Framework\TestCase;

/**
 * Class AbstractTest
 * @package Grifix\Kit\Test\Unit
 */
abstract class AbstractTest extends TestCase
{

}
