<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Helper;

use Grifix\Kit\Helper\ArrayContains\Diff\KeyDoesNotExistDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\ValueIsNotArrayDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\ValueIsNotEqualDiff;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Test\Unit\AbstractTest;

/**
 * Class ArrayHelperTest
 *
 * @category           Alias
 * @package            Alias\Common
 * @author             Mike Shapovalov <smikebox@gmail.com>
 * @license            http://opensource.org/licenses/MIT MIT
 * @link               http://grifix.net/utils/arr
 * @coversDefaultClass Alias\Common\ArrayHelper
 */
class ArrayHelperTest extends AbstractTest
{
    /**
     * Tests set and get methods
     *
     * @return void
     */
    public function testSetAndGet()
    {
        $array = [];
        $helper = new ArrayHelper();
        $helper->set($array, 'foo.bar', 'test');
        self::assertEquals(
            [
                'foo' => [
                    'bar' => 'test'
                ]
            ],
            $array
        );
        self::assertEquals('test', $helper->get($array, 'foo.bar'));
    }

    /**
     * @dataProvider itContainsDataProvider
     */
    public function testItContains(array $haystack, array $needle, $expectedResult): void
    {
        $helper = new ArrayHelper();
        self::assertEquals($expectedResult, $helper->contains($haystack, $needle));
    }

    public function itContainsDataProvider(): array
    {
        return [
            [
                [],
                [],
                true
            ],
            [
                [1],
                [],
                true
            ],
            [
                [1],
                [1],
                true
            ],
            [
                [],
                [1, 2],
                [
                    new KeyDoesNotExistDiff('0'),
                    new KeyDoesNotExistDiff('1')
                ]
            ],
            [
                [],
                [
                    'a' => 1,
                    'b' => 2
                ],
                [
                    new KeyDoesNotExistDiff('a'),
                    new KeyDoesNotExistDiff('b'),
                ]
            ],
            [
                [
                    'a' => 2,
                    'b' => 1
                ],
                [
                    'a' => 1,
                    'b' => 2
                ],
                [
                    new ValueIsNotEqualDiff('a', 1, 2),
                    new ValueIsNotEqualDiff('b', 2, 1),
                ]
            ],
            [
                [
                    'a' => 1,
                    'b' => 2
                ],
                [
                    'a' => [],
                    'b' => []
                ],
                [
                    new ValueIsNotArrayDiff('a'),
                    new ValueIsNotArrayDiff('b'),
                ]
            ],
            [
                [
                    'a' => [
                        'one' => 1
                    ],
                    'b' => [
                        'one' => 2
                    ]
                ],
                [
                    'a' => [
                        'one' => 2
                    ],
                    'b' => [
                        'one' => 1
                    ]
                ],
                [
                    new ValueIsNotEqualDiff('a.one', 2, 1),
                    new ValueIsNotEqualDiff('b.one', 1, 2),
                ]
            ]
        ];
    }

    public function testKeyExist(): void
    {
        $helper = new ArrayHelper();
        $array = [
            'numbers' => [1, 2, 3],
            'name' => 'Joe',
            'items' => [
                'apple' => [
                    'name' => 'Apple',
                    'color' => 'red'
                ]
            ]
        ];

        self::assertTrue($helper->keyExist($array, 'numbers'));
        self::assertTrue($helper->keyExist($array, 'numbers.0'));
        self::assertFalse($helper->keyExist($array, 'numbers.3'));
        self::assertFalse($helper->keyExist($array, 'name.title'));
        self::assertTrue($helper->keyExist($array, 'items.apple.name'));
        self::assertFalse($helper->keyExist($array, 'items.apple.weight'));
    }
}
