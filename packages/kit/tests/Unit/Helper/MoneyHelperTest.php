<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Test\Unit\Helper;

use Grifix\Kit\Helper\Exception\InvalidAmountException;
use Grifix\Kit\Helper\MoneyHelper;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Validation\Validator\MoneyValidator;

class MoneyHelperTest extends AbstractTest
{
    /**
     * @dataProvider itConvertsToIntDataProvider
     */
    public function testItConvertsToInt(string $amount, int $expectedResult):void {
        $helper = new MoneyHelper(new MoneyValidator());
        self::assertSame($expectedResult, $helper->toInt($amount));
    }

    public function itConvertsToIntDataProvider():array {
        return [
            ['1', 100],
            ['1.0', 100],
            ['1.00', 100],
            ['1,12', 112],
            ['1,1', 110]
        ];
    }

    public function testItDoesNotConvertInvalidAmount():void {
        $helper = new MoneyHelper(new MoneyValidator());
        self::expectException(InvalidAmountException::class);
        $helper->toInt('text');
    }
}
