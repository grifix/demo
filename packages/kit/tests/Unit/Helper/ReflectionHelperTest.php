<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Helper;


use Grifix\Kit\Helper\ReflectionHelper;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Test\Unit\Helper\Stub\Dummy;

/**
 * Class ReflectionHelperTest
 * @package Grifix\Kit\Test\Unit\Helper
 */
class ReflectionHelperTest extends AbstractTest
{

    public function testGetProperty()
    {
        $helper = new ReflectionHelper();
        $object = new Dummy('test');
        self::assertEquals('test', $helper->getPropertyValue($object, 'value'));
    }

    public function testGetNestedProperty()
    {
        $helper = new ReflectionHelper();
        $object = new Dummy(new Dummy('test'));

        self::assertEquals('test', $helper->getPropertyValue($object, 'value.value'));
    }
}