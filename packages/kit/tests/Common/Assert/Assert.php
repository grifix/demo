<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Test\Common\Assert;

use Grifix\Kit\Helper\ArrayContains\Diff\KeyDoesNotExistDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\ValueIsNotArrayDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\ValueIsNotEqualDiff;
use Grifix\Kit\Helper\ArrayHelper;

abstract class Assert extends \PHPUnit\Framework\Assert
{
    public static function arrayContains(array $haystack, array $needle): void
    {
        $diffs = (new ArrayHelper())->contains($haystack, $needle);
        if (true === $diffs) {
            return;
        }
        $message = [
            'Arrays are not equal'
        ];
        foreach ($diffs as $diff) {
            if ($diff instanceof ValueIsNotArrayDiff) {
                $message[] = sprintf('"%s" is not array', $diff->getPath());
                continue;
            }
            if ($diff instanceof KeyDoesNotExistDiff) {
                $message[] = sprintf('"%s" does not exist', $diff->getPath());
                continue;
            }
            if ($diff instanceof ValueIsNotEqualDiff) {
                $message[] = sprintf(
                    '"%s" must be %s but %s given',
                    $diff->getPath(),
                    var_export($diff->getNeedleValue(), true),
                    var_export($diff->getHaystackValue(), true)
                );
                continue;
            }
        }
        throw new \Exception(implode("\n", $message));
    }
}
