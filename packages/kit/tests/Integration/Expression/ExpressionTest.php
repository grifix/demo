<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Expression;


use Grifix\Kit\Expression\Builder\ExpressionBuilderFactoryInterface;
use Grifix\Kit\Expression\ExpressionFactoryInterface;
use Grifix\Kit\Test\Integration\AbstractTest;

/**
 * Class ExpressionTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ExpressionTest extends AbstractTest
{
    public function testExpressionBuilder()
    {
        $phpBuilder = $this->getShared(ExpressionBuilderFactoryInterface::class)->createPhpBuilder();
        $sqlBuilder = $this->getShared(ExpressionBuilderFactoryInterface::class)->createSqlBuilder();
        $f = $this->getShared(ExpressionFactoryInterface::class);
        
        $expr = $f->orX($f->eq('a', 'b'), $f->eq('b', 'a'));
        self::assertEquals("('a' == 'b') || ('b' == 'a')", $phpBuilder->buildExpression($expr));
        self::assertEquals("(a = b) OR (b = a)", $sqlBuilder->buildExpression($expr));
        
        $expr = $f->eq($f->eq($f->eq('a', 'b'), $f->eq('b', 'a')), $f->eq(1, 'a'));
        self::assertEquals("(('a' == 'b') == ('b' == 'a')) == (1 == 'a')", $phpBuilder->buildExpression($expr));
        
        $expr = $f->eq(1, 1);
        self::assertEquals("1 == 1", $phpBuilder->buildExpression($expr));
        
        $expr = $f->in(1, [1, 2, 3]);
        self::assertEquals('in_array(1,array (
  0 => 1,
  1 => 2,
  2 => 3,
))', $phpBuilder->buildExpression($expr));
    
        self::assertEquals('1 IN (1, 2, 3)', $sqlBuilder->buildExpression($expr));
    }
}
