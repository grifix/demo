<?php declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator;

use Grifix\Kit\Orm\SchemaGenerator\SchemaGeneratorInterface;
use Grifix\Kit\Test\Integration\AbstractTest;
use Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub\AggregateInterface;
use JsonSchema\Validator;

class SchemaGeneratorTest extends AbstractTest
{
    public function testItGenerates(): void
    {
        $schemaGenerator = $this->getShared(SchemaGeneratorInterface::class);
        $expected = <<<JSON
{
    "oneOf": [
        {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "_type",
                "vo",
                "id",
                "date",
                "ip",
                "childIds",
                "items",
                "objects"
            ],
            "properties": {
                "_type": {
                    "type": "string"
                },
                "vo": {
                    "oneOf": [
                        {
                            "type": null
                        },
                        {
                            "type": "object",
                            "additionalProperties": false,
                            "required": [
                                "_type",
                                "name",
                                "price",
                                "items",
                                "numbers"
                            ],
                            "properties": {
                                "_type": {
                                    "type": "string"
                                },
                                "name": {
                                    "type": "string"
                                },
                                "price": {
                                    "type": [
                                        "integer",
                                        "null"
                                    ]
                                },
                                "items": {
                                    "oneOf": [
                                        {
                                            "type": "array"
                                        },
                                        {
                                            "type": "object"
                                        }
                                    ]
                                },
                                "numbers": {
                                    "type": "array",
                                    "items": {
                                        "type": "integer"
                                    }
                                }
                            }
                        }
                    ]
                },
                "id": {
                    "type": "object",
                    "additionalProperties": false,
                    "required": [
                        "value"
                    ],
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                },
                "date": {
                    "oneOf": [
                        {
                            "type": "null"
                        },
                        {
                            "type": "object",
                            "additionalProperties": false,
                            "required": [
                                "value"
                            ],
                            "properties": {
                                "value": {
                                    "type": "string"
                                }
                            }
                        }
                    ]
                },
                "ip": {
                    "type": "object",
                    "additionalProperties": false,
                    "required": [
                        "value"
                    ],
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                },
                "childIds": {
                    "oneOf": [
                        {
                            "type": "array"
                        },
                        {
                            "type": "object"
                        }
                    ]
                },
                "items": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "additionalProperties": false,
                        "required": [
                            "id",
                            "name"
                        ],
                        "properties": {
                            "id": {
                                "type": "string"
                            },
                            "name": {
                                "type": "string"
                            }
                        }
                    }
                },
                "objects": {
                    "oneOf": [
                        {
                            "type": "array"
                        },
                        {
                            "type": "object"
                        }
                    ]
                }
            }
        },
        {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "_type",
                "ip",
                "id",
                "date",
                "vo",
                "childIds",
                "items",
                "objects"
            ],
            "properties": {
                "_type": {
                    "type": "string"
                },
                "ip": {
                    "oneOf": [
                        {
                            "type": "null"
                        },
                        {
                            "type": "object",
                            "additionalProperties": false,
                            "required": [
                                "value"
                            ],
                            "properties": {
                                "value": {
                                    "type": "string"
                                }
                            }
                        }
                    ]
                },
                "id": {
                    "type": "object",
                    "additionalProperties": false,
                    "required": [
                        "value"
                    ],
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                },
                "date": {
                    "oneOf": [
                        {
                            "type": "null"
                        },
                        {
                            "type": "object",
                            "additionalProperties": false,
                            "required": [
                                "value"
                            ],
                            "properties": {
                                "value": {
                                    "type": "string"
                                }
                            }
                        }
                    ]
                },
                "vo": {
                    "oneOf": [
                        {
                            "type": "object",
                            "additionalProperties": false,
                            "required": [
                                "_type",
                                "name",
                                "price",
                                "items",
                                "numbers"
                            ],
                            "properties": {
                                "_type": {
                                    "type": "string"
                                },
                                "name": {
                                    "type": "string"
                                },
                                "price": {
                                    "type": [
                                        "integer",
                                        "null"
                                    ]
                                },
                                "items": {
                                    "oneOf": [
                                        {
                                            "type": "array"
                                        },
                                        {
                                            "type": "object"
                                        }
                                    ]
                                },
                                "numbers": {
                                    "type": "array",
                                    "items": {
                                        "type": "integer"
                                    }
                                }
                            }
                        }
                    ]
                },
                "childIds": {
                    "oneOf": [
                        {
                            "type": "array"
                        },
                        {
                            "type": "object"
                        }
                    ]
                },
                "items": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "additionalProperties": false,
                        "required": [
                            "id",
                            "name"
                        ],
                        "properties": {
                            "id": {
                                "type": "string"
                            },
                            "name": {
                                "type": "string"
                            }
                        }
                    }
                },
                "objects": {
                    "oneOf": [
                        {
                            "type": "array"
                        },
                        {
                            "type": "object"
                        }
                    ]
                }
            }
        }
    ]
}
JSON;

        self::assertEquals($expected, $schemaGenerator->generate(AggregateInterface::class));
    }

    /**
     * @dataProvider itValidatesDataProvider
     */
    public function testItValidates(array $json, bool $result, array $errors = [])
    {
        $json = json_decode(json_encode($json));
        $validator = new Validator();
        $schema = json_decode($this->getShared(SchemaGeneratorInterface::class)->generate(AggregateInterface::class));
        $validator->validate($json, $schema);
        self::assertEquals($validator->isValid(), $result);
        self::assertEquals($validator->getErrors(), $errors);
    }

    public function itValidatesDataProvider(): array
    {
        return [
            [
                [
                    'id' => [
                        'value' => '123'
                    ],
                    '_type' => 'item',
                    'date' => [
                        'value' => '2019-01-01'
                    ],
                    'ip' => [
                        'value' => '127.0.0.1'
                    ],
                    'childIds' => [

                    ],
                    'items' => [

                    ],
                    'objects' => [

                    ],
                    'vo' => null
                ],
                true,
                []
            ],
            [
                [
                    'id' => [
                        'value' => '123'
                    ],
                    '_type' => 'item',
                    'date' => null,
                    'ip' => [
                        'value' => '127.0.0.1'
                    ],
                    'childIds' => [
                        1,
                        2,
                        3
                    ],
                    'items' => [
                        [
                            'id' => '1',
                            'name' => 'test'
                        ],
                        [
                            'id' => '2',
                            'name' => 'test2'
                        ]
                    ],
                    'vo' => null,
                    'objects' => [
                        'red',
                        'green',
                        'yellow'
                    ],
                ],
                true,
                []
            ],
            [
                [
                    'id' => [
                        'value' => '123'
                    ],
                    '_type' => 'item',
                    'date' => null,
                    'ip' => null,
                    'childIds' => [],
                    'items' => [],
                    'objects' => [
                        'a' => 1,
                        'b' => 2,
                        'c' => 3
                    ],
                    'vo' => [
                        '_type' => 'test',
                        'name' => 'test',
                        'price' => 100,
                        'items' => ['a'],
                        'numbers' => [1, 2, 3]
                    ]
                ],
                true,
                []
            ],
        ];
    }
}
