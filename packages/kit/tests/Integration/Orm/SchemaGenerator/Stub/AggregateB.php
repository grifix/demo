<?php
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

class AggregateB extends AbstractAggregate
{
    /** @var \Grifix\Shared\Domain\IpAddress\IpAddressInterface|null */
    protected $ip;
}
