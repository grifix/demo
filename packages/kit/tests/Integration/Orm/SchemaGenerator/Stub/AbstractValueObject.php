<?php declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

abstract class AbstractValueObject implements ValueObjectInterface
{
    /** @var string */
    protected $name;

    /** @var int|null */
    protected $price;

    /** @var array */
    protected $items;

    /** @var int[] */
    protected $numbers;

    public function __construct(string $name, ?int $price, array $items, array $numbers)
    {
        $this->name = $name;
        $this->price = $price;
        $this->items = $items;
        $this->numbers = $numbers;
    }


    public function getName(): string
    {
        return $this->name;
    }
}
