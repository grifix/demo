<?php declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

use Grifix\Shared\Domain\Uuid\UuidInterface;

interface AggregateInterface
{
    public function getId(): UuidInterface;
}
