<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

interface ItemInterface
{
    public function getId(): string;

    public function getName(): string;
}
