<?php declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

interface ValueObjectInterface
{
    public function getName(): string;
}
