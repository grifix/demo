<?php
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class AggregateA extends AbstractAggregate
{
    /** @var \Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub\ValueObjectInterface|null */
    protected $vo;

    public function __construct(
        UuidInterface $id,
        ?DateTimeInterface $date,
        IpAddressInterface $ip,
        ?ValueObjectInterface $vo,
        Infrastructure $infrastructure
    ) {
        parent::__construct($id, $date, $ip, $vo, $infrastructure);
    }
}
