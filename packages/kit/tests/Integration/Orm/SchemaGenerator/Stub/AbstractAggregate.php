<?php
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub;

use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\AssocArray;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

abstract class AbstractAggregate implements AggregateInterface
{
    /** @var \Grifix\Shared\Domain\Uuid\UuidInterface */
    protected $id;

    /** @var \Grifix\Shared\Domain\DateTime\DateTimeInterface|null */
    protected $date;

    /** @var \Grifix\Shared\Domain\IpAddress\IpAddressInterface */
    protected $ip;

    /** @var \Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub\ValueObjectInterface */
    protected $vo;

    /** @var CollectionInterface|string[] */
    protected $childIds;

    /** @var \Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub\ItemInterface[] */
    protected array $items;

    protected array $objects = [];

    #[Dependency]
    /**
     * @var \Grifix\Kit\Test\Integration\Orm\SchemaGenerator\Stub\Infrastructure
     */
    protected $infrastructure;

    public function __construct(
        UuidInterface $id,
        ?DateTimeInterface $date,
        IpAddressInterface $ip,
        ValueObjectInterface $vo,
        Infrastructure $infrastructure
    ) {
        $this->id = $id;
        $this->date = $date;
        $this->ip = $ip;
        $this->vo = $vo;
        $this->infrastructure = $infrastructure;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }
}
