<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\TechnicalInspection;

use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\TechnicalInspection\TechnicalInspection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\VehicleBlueprint;
use Grifix\Shared\Infrastructure\Internal\Serializer\ObjectSerializer;

/**
 * Class TechnicalInspectionBlueprint
 * @package Grifix\Kit\Test\Integration\Orm\Stub\TechnicalInspection
 */
class TechnicalInspectionBlueprint extends AbstractBlueprint
{
    const VEHICLE_COLUMN = 'vehicle_id';

    /**
     * {@inheritdoc}
     */
    protected function init(): void
    {
        $this->addRelationToParent(
            AbstractVehicle::class,
            VehicleBlueprint::TECHNICAL_INSPECTIONS_PROPERTY,
            self::VEHICLE_COLUMN
        );
    }

    protected function getEntityClassName(): string
    {
        return TechnicalInspection::class;
    }

    public function getTable(): string
    {
        return 'test.technical_inspection';
    }

    public function getSerializerClass(): string
    {
        return ObjectSerializer::class;
    }
}
