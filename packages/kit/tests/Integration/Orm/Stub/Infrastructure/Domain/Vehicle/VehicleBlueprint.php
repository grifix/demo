<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle;

use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Bus;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Engine\Repair\Repair;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\TechnicalInspection\TechnicalInspection;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Truck;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\Engine\Repair\RepairBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\Engine\Repair\RepairCollection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\TechnicalInspection\TechnicalInspectionBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\TechnicalInspection\TechnicalInspectionCollection;

class VehicleBlueprint extends AbstractBlueprint
{
    const TECHNICAL_INSPECTIONS_PROPERTY = 'technicalInspections';

    const ENGINE_REPAIRS_PROPERTY = 'engine.repairs';

    /**
     * {@inheritdoc}
     */
    protected function init(): void
    {
        $this->addRelationToChild(
            self::TECHNICAL_INSPECTIONS_PROPERTY,
            TechnicalInspection::class,
            TechnicalInspectionBlueprint::VEHICLE_COLUMN,
            TechnicalInspectionCollection::class
        );

        $this->addRelationToChild(
            self::ENGINE_REPAIRS_PROPERTY,
            Repair::class,
            RepairBlueprint::VEHICLE_COLUMN,
            RepairCollection::class
        );

        $this->setClassDetector([
            'bus' => Bus::class,
            'truck' => Truck::class
        ]);
    }

    protected function getEntityClassName(): string
    {
        return AbstractVehicle::class;
    }

    public function getTable(): string
    {
        return 'test.vehicle';
    }

    public function getSerializerClass(): string
    {
        return VehicleSerializer::class;
    }
}
