<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle;

use Grifix\Kit\Orm\Serializer\Definition\TypeSerializationDefinition;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Engine\Engine;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank\DieselTank;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank\GasolineTank;
use Grifix\Shared\Infrastructure\Internal\Serializer\ObjectSerializer;

/**
 * Class VehicleSerializer
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure
 */
class VehicleSerializer extends ObjectSerializer
{
    public function init(): void
    {
        parent::init();

        $this->addMultiTypePropertySerializer('tank', [
            new TypeSerializationDefinition(
                'gasoline',
                GasolineTank::class,
                ObjectSerializer::class
            ),
            new TypeSerializationDefinition(
                'diesel',
                DieselTank::class,
                ObjectSerializer::class
            )
        ]);
    }
}
