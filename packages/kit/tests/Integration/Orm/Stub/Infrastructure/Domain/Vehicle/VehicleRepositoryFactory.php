<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle;

use Grifix\Kit\Orm\Repository\RepositoryFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\VehicleRepositoryInterface;

/**
 * Class VehicleRepositoryFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
class VehicleRepositoryFactory implements VehicleRepositoryFactoryInterface
{
    /**
     * @var RepositoryFactoryInterface
     */
    protected $repositoryFactory;

    /**
     * VehicleRepositoryFactory constructor.
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryFactory = $repositoryFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createVehicleRepository(): VehicleRepositoryInterface
    {
        return $this->repositoryFactory->createWrapper(
            AbstractVehicle::class,
            VehicleRepository::class
        );
    }
}
