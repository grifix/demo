<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\VehicleRepositoryInterface;

/**
 * Class VehicleRepositoryFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
interface VehicleRepositoryFactoryInterface
{
    /**
     * @return VehicleRepositoryInterface
     */
    public function createVehicleRepository(): VehicleRepositoryInterface;
}
