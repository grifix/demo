<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\Engine\Repair;

use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Engine\Repair\Repair;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Domain\Vehicle\VehicleBlueprint;
use Grifix\Shared\Infrastructure\Internal\Serializer\ObjectSerializer;

/**
 * Class RepairBlueprint
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair
 */
class RepairBlueprint extends AbstractBlueprint
{
    const VEHICLE_COLUMN = 'vehicle_id';

    public function init(): void
    {
        $this->addRelationToParent(
            AbstractVehicle::class,
            VehicleBlueprint::ENGINE_REPAIRS_PROPERTY,
            self::VEHICLE_COLUMN
        );
    }

    protected function getEntityClassName(): string
    {
        return Repair::class;
    }

    public function getTable(): string
    {
        return 'test.repair';
    }

    public function getSerializerClass(): string
    {
        return ObjectSerializer::class;
    }
}
