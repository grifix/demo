CREATE SCHEMA test;

CREATE TABLE test.vehicle (
  id VARCHAR NOT NULL,
  data JSONB NOT NULL,
  PRIMARY KEY(id)
)
WITH (oids = false);


CREATE TABLE test.technical_inspection (
  id VARCHAR NOT NULL,
  vehicle_id VARCHAR NOT NULL,
  data JSONB NOT NULL,
  PRIMARY KEY(id)
)
WITH (oids = false);

ALTER TABLE test.technical_inspection
  ADD CONSTRAINT technical_inspection_fk FOREIGN KEY ("vehicle_id")
REFERENCES test."vehicle"(id)
ON DELETE CASCADE
ON UPDATE CASCADE
  NOT DEFERRABLE;

CREATE TABLE test.repair (
  id VARCHAR NOT NULL,
  vehicle_id VARCHAR NOT NULL,
  data JSONB NOT NULL,
  PRIMARY KEY(id)
)
WITH (oids = false);


ALTER TABLE test.repair
  ADD CONSTRAINT repair_fk FOREIGN KEY ("vehicle_id")
REFERENCES test."vehicle"(id)
ON DELETE CASCADE
ON UPDATE CASCADE
  NOT DEFERRABLE;