<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle;

use Grifix\Shared\Domain\EventPublisherInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\TechnicalInspection\TechnicalInspectionCollectionInterface;

/**
 * Class VehicleInfrastructure
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure
 */
interface VehicleInfrastructureInterface extends EventPublisherInterface
{
    /**
     * @return string
     * @throws \Exception
     */
    public function generateTechInspectionId(): string;

    /**
     * @return TechnicalInspectionCollectionInterface
     */
    public function createTechInspectionCollection(): TechnicalInspectionCollectionInterface;
}
