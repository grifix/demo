<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event;

class TechInspectionWasMadeEvent
{
    private string $vehicleId;

    public function __construct(string $vehicleId)
    {
        $this->vehicleId = $vehicleId;
    }

    public function getVehicleId(): string
    {
        return $this->vehicleId;
    }
}
