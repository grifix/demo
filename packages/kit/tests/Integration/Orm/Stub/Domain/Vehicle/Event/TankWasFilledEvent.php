<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event;

/**
 * Class TankWasFilledEvent
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event
 */
class TankWasFilledEvent
{
    private string $vehicleId;

    private float $fullAmount;

    public function __construct(string $vehicleId, float $fullAmount)
    {
        $this->vehicleId = $vehicleId;
        $this->fullAmount = $fullAmount;
    }

    public function getVehicleId(): string
    {
        return $this->vehicleId;
    }

    public function getFullAmount(): float
    {
        return $this->fullAmount;
    }
}
