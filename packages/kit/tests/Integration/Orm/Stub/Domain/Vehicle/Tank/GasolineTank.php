<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank;

/**
 * Class GasolineTank
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Tank
 */
class GasolineTank extends AbstractTank
{

}
