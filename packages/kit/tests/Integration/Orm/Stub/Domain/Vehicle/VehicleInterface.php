<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Engine\EngineInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank\TankInterface;

/**
 * Interface VehicleInterface
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
interface VehicleInterface
{

    /**
     * @param float $fuelAmount
     * @throws \Exception
     */
    public function fillTank(float $fuelAmount): void;

    public function makeTechnicalInspection(): void;

    /**
     * @param $miles
     * @throws \Exception
     */
    public function drive(float $miles): void;

    /**
     * @param float $cost
     */
    public function repairEngine(float $cost): void;

    public function getId(): string;

    public function getEngine(): EngineInterface;

    public function getTank(): TankInterface;

    public function getModel(): string;
}
