<?php
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Relation;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Engine\EngineInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event\EngineWasRepairedEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event\TankWasFilledEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event\TechInspectionWasMadeEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event\VehicleWasDrivenEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\TechnicalInspection\TechnicalInspectionCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\TechnicalInspection\TechnicalInspectionFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank\TankInterface;

/**
 * Class AbstractVehicle
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
abstract class AbstractVehicle implements VehicleInterface
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank\TankInterface
     */
    protected $tank;

    #[Relation]
    protected ?TechnicalInspectionCollectionInterface $technicalInspections = null;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Engine\EngineInterface
     */
    protected $engine;

    public function __construct(
        #[Dependency]
        protected VehicleInfrastructureInterface $infrastructure,
        string $id,
        string $model,
        TankInterface $tank,
        EngineInterface $engine,
        #[Dependency]
        protected TechnicalInspectionFactoryInterface $technicalInspectionFactory
    ) {
        $this->infrastructure = $infrastructure;
        $this->id = $id;
        $this->model = $model;
        $this->tank = $tank;
        $this->technicalInspectionFactory = $technicalInspectionFactory;
        $this->technicalInspections = $this->infrastructure->createTechInspectionCollection();
        $this->engine = $engine;
    }

    /**
     * @param float $fuelAmount
     * @throws \Exception
     */
    public function fillTank(float $fuelAmount): void
    {
        $this->tank = $this->tank->fill($fuelAmount);
        $this->infrastructure->publishEvent(new TankWasFilledEvent($this->id, $fuelAmount), $this);
    }

    public function makeTechnicalInspection(): void
    {
        $this->technicalInspections->add(
            $this->technicalInspectionFactory->createTechnicalInspection(
                $this->infrastructure->generateTechInspectionId(),
                $this->isOperable()
            )
        );
        $this->infrastructure->publishEvent(new TechInspectionWasMadeEvent($this->id), $this);
    }

    /**
     * @return bool
     */
    protected function isOperable(): bool
    {
        if ($this->tank->isOperable() && !$this->engine->needRepair()) {
            return true;
        }
        return false;
    }

    /**
     * @param $miles
     * @throws \Exception
     */
    public function drive(float $miles): void
    {
        $engine = $this->engine->drive($miles);
        $tank = $this->tank->useFuel($miles);

        $this->engine = $engine;
        $this->tank = $tank;

        $this->infrastructure->publishEvent(new VehicleWasDrivenEvent($this->id, $miles), $this);
    }

    /**
     * @param float $cost
     */
    public function repairEngine(float $cost): void
    {
        $this->engine = $this->engine->repair($cost);
        $this->infrastructure->publishEvent(new EngineWasRepairedEvent($this->id, $cost), $this);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEngine(): EngineInterface
    {
        return $this->engine;
    }

    public function getTank(): TankInterface
    {
        return $this->tank;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function getTechnicalInspections(): TechnicalInspectionCollectionInterface
    {
        return $this->technicalInspections;
    }
}
