<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Specification;

use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\AbstractSpecification;

/**
 * Class BigBusSpecification
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain
 */
class BigBusSpecification extends AbstractSpecification
{
    /***
     * {@inheritdoc}
     */
    protected function createExpression(): ExpressionInterface
    {
        return $this->andX(
            $this->equal('type', 'bus'),
            $this->greaterThanOrEqual('engine.volume', 4),
            $this->greaterThanOrEqual('tank.capacity', 50),
            $this->greaterThanOrEqual('seats', 30)
        );
    }
}
