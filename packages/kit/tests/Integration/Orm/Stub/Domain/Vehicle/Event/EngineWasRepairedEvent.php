<?php

declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event;

class EngineWasRepairedEvent
{
    private string $vehicleId;

    private float $cost;

    public function __construct(string $vehicleId, float $cost)
    {
        $this->vehicleId = $vehicleId;
        $this->cost = $cost;
    }

    public function getVehicleId(): string
    {
        return $this->vehicleId;
    }

    public function getCost(): float
    {
        return $this->cost;
    }
}
