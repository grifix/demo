<?php

declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event;

class VehicleWasDrivenEvent
{
    private string $vehicleId;

    private float $miles;

    public function __construct(string $vehicleId, float $miles)
    {
        $this->vehicleId = $vehicleId;
        $this->miles = $miles;
    }

    public function getVehicleId(): string
    {
        return $this->vehicleId;
    }

    public function getMiles(): float
    {
        return $this->miles;
    }
}
