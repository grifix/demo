<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle;

/**
 * Class Truck
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
interface TruckInterface extends VehicleInterface
{
    /**
     * @param float $amount
     * @throws \Exception
     */
    public function loadCargo(float $amount): void;
}
