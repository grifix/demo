<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Tank;

/**
 * Class DieselTank
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Tank
 */
class DieselTank extends AbstractTank
{

}
