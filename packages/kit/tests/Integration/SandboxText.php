<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration;

use Grifix\Kit\Migration\MigrationRunnerInterface;

/**
 * Class SandboxText
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SandboxText extends AbstractTest
{
    public function testSomething()
    {
        $migrationRunner = $this->getShared(MigrationRunnerInterface::class);
        $migrationRunner->down();
    }
}