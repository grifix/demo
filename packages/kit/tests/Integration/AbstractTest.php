<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\EntryPoint\IntegrationTestEntryPoint;
use Grifix\Kit\Ioc\IocContainerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractTest extends TestCase
{
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    protected function setUp(): void
    {
        $rootDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
        include $rootDir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

        $context = (new IntegrationTestEntryPoint($rootDir))->enter();
        $this->iocContainer = $context->getIocContainer();
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }

    /**
     * @param string $alias
     * @param        $value
     *
     * @return void
     */
    protected function setShared(string $alias, $value)
    {
        $this->iocContainer->set($alias, $value);
    }

    public function tearDown(): void
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
        $this->getShared(ConnectionInterface::class)->rollBackTransaction();
    }
}
