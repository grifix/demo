<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Specification\Dummy;

/**
 * Class Item
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Specification\Stub
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Item
{
    protected $name;
    protected $date;
    protected $size;
    
    /**
     * Item constructor.
     *
     * @param string    $name
     * @param \DateTime $date
     * @param Size $size
     */
    public function __construct(string $name, \DateTime $date, Size $size = null)
    {
        $this->name = $name;
        $this->date = $date;
        $this->size = $size;
    }
    
    /**
     * @param Size $size
     *
     * @return $this
     */
    public function setSize(Size $size){
        $this->size = $size;
        return $this;
    }
    
    /**
     * @return Size
     */
    public function getSize(){
        return $this->size;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }
    
    /**
     * @param \DateTime $date
     *
     * @return Item
     */
    public function setDate(\DateTime $date): Item
    {
        $this->date = $date;
        
        return $this;
    }
}