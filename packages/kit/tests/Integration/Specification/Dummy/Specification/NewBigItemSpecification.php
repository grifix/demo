<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Specification\Dummy\Specification;

use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\AbstractSpecification;

/**
 * Class NewBigBoxSpecification
 * @package Grifix\Kit\Test\Integration\Specification\Dummy\Specification
 */
class NewBigItemSpecification extends AbstractSpecification
{
    /**
     * (@inheritdoc)
     */
    protected function createExpression(): ExpressionInterface
    {
        return $this->andX(
            $this->greaterThanOrEqual('date', new \DateTime('01.01.2018')),
            $this->greaterThanOrEqual('size.height', 20),
            $this->greaterThanOrEqual('size.width', 20),
            $this->greaterThanOrEqual('size.length', 20)
        );
    }
}
