<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace {
    
    return [
        'compiledDir' => '/compiled/intl',
        'alwaysCompile' => true,
        'enabledLangs' => [
            'ru',
            'en',
        ],
        'enabledLocales'=> [
            'ru_RU',
            'en_US'
        ]
    ];
}
