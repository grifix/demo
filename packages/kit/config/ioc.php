<?php

declare(strict_types=1);

namespace {

    use Grifix\Kit\Alias;
    use Grifix\Kit\Config\ConfigInterface;
    use Grifix\Kit\Cqrs\Command\CommandBus\AsyncCommandBus;
    use Grifix\Kit\Cqrs\Command\CommandBus\CommandBus;
    use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
    use Grifix\Kit\Cqrs\Command\CommandBus\TransactionalCommandBus;
    use Grifix\Kit\Event\Bus\EventBusFactoryInterface;
    use Grifix\Kit\Event\Bus\EventBusInterface;
    use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\InMemoryProcessStarter;
    use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\ProcessStarter;
    use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\ProcessStarterInterface;
    use Grifix\Kit\Intl\TranslatorFactoryInterface;
    use Grifix\Kit\Intl\TranslatorInterface;
    use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;
    use Grifix\Kit\Ioc\IocContainerInterface;
    use Grifix\Kit\Logger\Logger;
    use Grifix\Kit\Mailer\MailerFactoryInterface;
    use Grifix\Kit\Mailer\MailerInterface;
    use Grifix\Kit\MessageBroker\MessageBrokerFactoryInterface;
    use Grifix\Kit\MessageBroker\MessageBrokerInterface;
    use Grifix\Kit\Route\UrlMaker\UrlMakerFactoryInterface;
    use Grifix\Kit\Route\UrlMaker\UrlMakerInterface;
    use Grifix\Shared\Application\Common\Clock\ClockInterface;
    use Grifix\Shared\Infrastructure\Application\Common\Clock\SystemClock;
    use Psr\Log\LoggerInterface;

    return [
        MessageBrokerInterface::class => function (): MessageBrokerInterface {
            /**@var $this IocContainerInterface */
            if ($this->get(ConfigInterface::class)->get('grifix.kit.queue.async')) {
                return $this->get(MessageBrokerFactoryInterface::class)->createRabbitMqMessageBroker();
            } else {
                return $this->get(MessageBrokerFactoryInterface::class)->createSyncMessageBroker();
            }
        },

        ProcessStarterInterface::class => function (): ProcessStarterInterface {
            /**@var $this IocContainerInterface */
            if ($this->get(ConfigInterface::class)->get('grifix.kit.queue.async')) {
                return $this->get(ProcessStarter::class);
            } else {
                return $this->get(InMemoryProcessStarter::class);
            }
        },

        EventBusInterface::class => function (): EventBusInterface {
            /**@var  $this IocContainerInterface */
            return ($this->get(EventBusFactoryInterface::class)->create());
        },

        UrlMakerInterface::class => function (): UrlMakerInterface {
            /**@var  $this IocContainerInterface */
            return $this->get(UrlMakerFactoryInterface::class)->create(
                $this->get(ConfigInterface::class)->get('grifix.kit.http.host')
            );
        },

        MailerInterface::class => function (): MailerInterface {
            /**@var  $this IocContainerInterface */
            return $this->get(MailerFactoryInterface::class)->create();
        },

        TranslatorInterface::class => function (): TranslatorInterface {
            /**@var  $this IocContainerInterface */
            return $this->get(TranslatorFactoryInterface::class)->create();
        },

        ClockInterface::class => function (): ClockInterface {
            /**@var  $this IocContainerInterface */
            return new SystemClock();
        },

        /** Command bus */
        Alias::SYNC_COMMAND_BUS => new DependencyDefinition(TransactionalCommandBus::class, [
            CommandBus::class
        ]),

        Alias::ASYNC_COMMAND_BUS => new DependencyDefinition(AsyncCommandBus::class, [
            Alias::SYNC_COMMAND_BUS
        ]),

        CommandBusInterface::class => function (): CommandBusInterface {
            /**@var  $this IocContainerInterface */
            return $this->get(Alias::ASYNC_COMMAND_BUS);
        },

        LoggerInterface::class => new DependencyDefinition(Logger::class),
    ];
}
