<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {
    return [
        'logPath' => '{grifix.kit.rootDir}/tmp/app.log',
        'enabled' => (bool) getenv('GRIFIX_LOGGER_ENABLED')
    ];
}
