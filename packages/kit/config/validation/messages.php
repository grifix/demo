<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Kit\Validation\ErrorInterface;
    use Grifix\Kit\Validation\Validator\ArrayValidator;
    use Grifix\Kit\Validation\Validator\BoolValidator;
    use Grifix\Kit\Validation\Validator\DateValidator;
    use Grifix\Kit\Validation\Validator\EmailValidator;
    use Grifix\Kit\Validation\Validator\FloatValidator;
    use Grifix\Kit\Validation\Validator\GreaterThanValidator;
    use Grifix\Kit\Validation\Validator\InArrayValidator;
    use Grifix\Kit\Validation\Validator\IntValidator;
    use Grifix\Kit\Validation\Validator\IpValidator;
    use Grifix\Kit\Validation\Validator\LessOrEqualThanValidator;
    use Grifix\Kit\Validation\Validator\MaxNumOfSymbolsValidator;
    use Grifix\Kit\Validation\Validator\MinNumOfCapsValidator;
    use Grifix\Kit\Validation\Validator\MinNumOfLettersValidator;
    use Grifix\Kit\Validation\Validator\MinNumOfNumbersValidator;
    use Grifix\Kit\Validation\Validator\MinNumOfSpecSymbolsValidator;
    use Grifix\Kit\Validation\Validator\MinNumOfSymbolsValidator;
    use Grifix\Kit\Validation\Validator\MoneyValidator;
    use Grifix\Kit\Validation\Validator\NoTagsValidator;
    use Grifix\Kit\Validation\Validator\NotEmptyValidator;
    use Grifix\Kit\Validation\Validator\RomanAlphabetValidator;
    use Grifix\Kit\Validation\Validator\UploadedFileValidator;
    use Grifix\Kit\Validation\Validator\UrlValidator;
    use Grifix\Kit\Validation\Validator\UuidValidator;

    return [
        IntValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidIntInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        DateValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidDateInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        FloatValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidFloatInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        MinNumOfCapsValidator::class => function (ErrorInterface $error) {
            /**@var $validator MinNumOfCapsValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_invalidMinNumOfCapsInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'num' => $validator->getNumOfCaps(),
            ]);
        },
        EmailValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidEmailInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        LessOrEqualThanValidator::class => function (ErrorInterface $error) {
            /**@var $validator LessOrEqualThanValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_valueIsBiggerThan', [
                'maxValue' => $validator->getMaxValue(),
                'field' => $error->getField()->getLabel()
            ]);
        },
        MaxNumOfSymbolsValidator::class => function (ErrorInterface $error) {
            /**@var $validator MaxNumOfSymbolsValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_invalidMaxNumOfSymbolsInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'num' => $validator->getNumOfSymbols(),
            ]);
        },
        MinNumOfLettersValidator::class => function (ErrorInterface $error) {
            /**@var $validator MinNumOfLettersValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_invalidMinNumOfLettersInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'num' => $validator->getNumOfLetters(),
            ]);
        },
        MinNumOfNumbersValidator::class => function (ErrorInterface $error) {
            /**@var $validator MinNumOfNumbersValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_invalidMinNumOfNumbersInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'num' => $validator->getNumOfNumbers(),
            ]);
        },
        MinNumOfSpecSymbolsValidator::class => function (ErrorInterface $error) {
            /**@var $validator MinNumOfSpecSymbolsValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_invalidMinNumOfSpecSymbolsInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'num' => $validator->getNumOfSymbols(),
            ]);
        },
        MinNumOfSymbolsValidator::class => function (ErrorInterface $error) {
            /**@var $validator MinNumOfSymbolsValidator */
            $validator = $error->getValidator();

            return $error->getTranslator()->translate('grifix.kit.msg_invalidMinNumOfSymbolsInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'num' => $validator->getNumOfSymbols(),
            ]);
        },
        MoneyValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidMoneyInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        NoTagsValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidNoTagsInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        NotEmptyValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidNotEmptyInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        RomanAlphabetValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidRomanAlphabetInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        UrlValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidUrlInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        InArrayValidator::class => function (ErrorInterface $error) {
            /**@var $validator InArrayValidator */
            $validator = $error->getValidator();
            return $error->getTranslator()->translate('grifix.kit.msg_invalidInArrayInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
                'values' => implode(',', $validator->getArray())
            ]);
        },
        ArrayValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidArrayInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel(),
            ]);
        },
        IpValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidIpInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel()
            ]);
        },
        UploadedFileValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidFileInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel()
            ]);
        },
        UuidValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidUuidInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel()
            ]);
        },
        BoolValidator::class => function (ErrorInterface $error) {
            return $error->getTranslator()->translate('grifix.kit.msg_invalidBoolInField', [
                'value' => $error->getValue(),
                'field' => $error->getField()->getLabel()
            ]);
        },
        GreaterThanValidator::class => function(ErrorInterface $error) {
            /**@var $validator GreaterThanValidator */
            $validator = $error->getValidator();
            return $error->getTranslator()->translate('grifix.kit.msg_invalidGreaterThanInField', [
                'minValue' => $validator->getMinValue(),
                'field' => $error->getField()->getLabel()
            ]);
        }

    ];
}
