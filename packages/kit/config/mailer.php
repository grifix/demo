<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace {
    
    use Grifix\Kit\Mailer\MailerFactoryInterface;
    
    return [
        'fromEmail' => 'no-reply@grifix.net',
        'fromName' => 'Grifix mailer',
        'transport' => MailerFactoryInterface::TRANSPRORT_SMTP,
        'host' => 'mail',
        'port' => 1025,
        'userName' => 'smike.mbx@gmail.com',
        'password' => '71724640-0dce-11ea-bb53-7d35e8f1505a',
    ];
}
