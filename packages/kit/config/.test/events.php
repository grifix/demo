<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);
namespace {

    use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Vehicle\Event;

    return [
        Event\EngineWasRepairedEvent::class => Event\EngineWasRepairedEvent::class,
        Event\TankWasFilledEvent::class => Event\TankWasFilledEvent::class,
        Event\TechInspectionWasMadeEvent::class => Event\TechInspectionWasMadeEvent::class,
        Event\VehicleWasDrivenEvent::class => Event\VehicleWasDrivenEvent::class
    ];
}
