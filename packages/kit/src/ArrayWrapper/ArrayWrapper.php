<?php declare(strict_types=1);

namespace Grifix\Kit\ArrayWrapper;

use Grifix\Kit\Helper\ArrayHelperInterface;

class ArrayWrapper implements ArrayWrapperInterface
{
    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    /** @var array */
    protected $array;

    public function __construct(ArrayHelperInterface $arrayHelper, array $array = [])
    {
        $this->arrayHelper = $arrayHelper;
        $this->array = $array;
    }


    public function get(string $key, $defaultValue = null)
    {
        return $this->arrayHelper->get($this->array, $key, $defaultValue);
    }

    public function set(string $key, $value): void
    {
        $this->arrayHelper->set($this->array, $key, $value);
    }

    public function remove(string $key)
    {
        unset($this->array[$key]);
    }

    public function getArray(): array
    {
        return $this->array;
    }

    public function has(string $key): bool
    {
        return $this->arrayHelper->has($this->array, $key);
    }

    public function keyExists(string $key): bool
    {
        return $this->arrayHelper->keyExist($this->array, $key);
    }
}
