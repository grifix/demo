<?php declare(strict_types = 1);

namespace Grifix\Kit\ArrayWrapper;

interface ArrayWrapperFactoryInterface
{
    public function create(array $array = []): ArrayWrapperInterface;
}
