<?php declare(strict_types = 1);

namespace Grifix\Kit\ArrayWrapper;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class ArrayWrapperFactory extends AbstractFactory implements ArrayWrapperFactoryInterface
{
    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    public function __construct(ClassMakerInterface $classMaker, ArrayHelperInterface $arrayHelper)
    {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
    }

    public function create(array $array = []): ArrayWrapperInterface
    {
        $class = $this->makeClassName(ArrayWrapper::class);
        return new $class($this->arrayHelper, $array);
    }
}
