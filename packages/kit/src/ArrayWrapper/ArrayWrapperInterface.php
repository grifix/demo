<?php declare(strict_types = 1);

namespace Grifix\Kit\ArrayWrapper;

interface ArrayWrapperInterface
{
    public function get(string $key, $defaultValue = null);

    public function set(string $key, $value): void;

    public function getArray(): array;

    public function has(string $key): bool;

    public function remove(string $key);

    public function keyExists(string $key): bool;
}
