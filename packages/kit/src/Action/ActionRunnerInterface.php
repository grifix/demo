<?php
declare(strict_types=1);

namespace Grifix\Kit\Action;

/**
 * Class ActionRunner
 * @package Grifix\Kit\Action
 */
interface ActionRunnerInterface
{
    /**
     * @param $actionClass
     * @param array $params
     * @return mixed
     * @throws \ReflectionException
     */
    public function runAction($actionClass, array $params = []);
}
