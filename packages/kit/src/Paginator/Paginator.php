<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Paginator;

/**
 * Class Paginator
 *
 * @category Grifix
 * @package  Grifix\Kit\Paginator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Paginator
{
    /**
     * @var int current page
     */
    private $currentPage;
    
    /**
     * @var int total number of records
     */
    private $totalNumOfRecords;
    
    /**
     * @var int numbers of records to show
     */
    private $numOfPages;
    
    /**
     * Paginator constructor.
     *
     * @param int $totalNumOfRecords
     * @param int $currentPage
     * @param int $numOfPages
     */
    public function __construct(int $totalNumOfRecords, int $currentPage = 1, int $numOfPages = 10)
    {
        $this->totalNumOfRecords = $totalNumOfRecords;
        $this->currentPage = $currentPage;
        $this->numOfPages = $numOfPages;
    }
    
    /**
     * Returns number of pages to show
     *
     * @return array
     */
    public function getPagesToShowNumbers(): array
    {
        //TODO
        return [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    }
    
    /**
     * Returns number of the last page
     *
     * @return int
     */
    public function getLastPageNumber(): int
    {
        //TODO
        return 100;
    }
    
    /**
     * Return next page number
     *
     * @return int
     */
    public function getNextPageNumber(): int
    {
        //TODO
        return 21;
    }
    
    /**
     * Returns prev page number
     *
     * @return int
     */
    public function getPrevPageNumber(): int
    {
        //TODO
        return 9;
    }
    
    /**
     * @param int $currentPage
     *
     * @return void
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }
}
