<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Ui\Action;


/**
 * Class RequestHandlerFactoryResolver
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ActionHandlerFactoryResolverInterface
{
    /**
     * @param string $action
     * @param array  $params
     *
     * @return mixed
     */
    public function resolveFactory(string $action, array $params): ActionHandlerFactoryInterface;
}
