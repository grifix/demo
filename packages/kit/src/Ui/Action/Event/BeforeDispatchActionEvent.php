<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Action\Event;

/**
 * Class BeforeDispatchRequestEvent
 *
 * @category Grifix
 * @package  Grifix\Admin\Ui\Http\Request\Event
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class BeforeDispatchActionEvent
{
    /**
     * @var string
     */
    protected $action;
    
    /**
     * @var array
     */
    protected $params;

    /**
     * BeforeDispatchRequestEvent constructor.
     *
     * @param string $action
     * @param array $params
     * @param string|null $method
     */
    public function __construct(
        string $action,
        array $params = []
    ) {
        $this->action = $action;
        $this->params = $params;
    }
    
    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
    
    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
