<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ui\Action;

/**
 * Action handler can be executed directly using HTTP request trough  /grifix/shared/action/{alias}
 * You can also execute handler in your php code using \Grifix\Kit\Ui\Action\ActionDispatcherInterface::dispatch
 */
interface ActionHandlerInterface
{
    /**
     * returns alias for using this action from HTTP trough /grifix/shared/action/{alias}
     */
    public static function getAlias(): string;

    public function hasSideEffects(): bool;

    /**
     * @param array $params
     *
     * @return array
     */
    public function __invoke(array $params = []): array;
}
