<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Ui\Action;

use Grifix\Kit\Ui\Action\Exception\CannotInvokeActionHandlerException;

/**
 * Class RequestDispatcher
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ActionDispatcherInterface
{
    /**
     * @param string|null $mandatoryMethod if you want to ensure that action handler allows this method
     *
     */
    public function dispatch(string $actionAlias, array $params = [], ?bool $shouldHaveSideEffects = null): array;
}
