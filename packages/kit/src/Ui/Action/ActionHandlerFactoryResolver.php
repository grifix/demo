<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ui\Action;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class RequestHandlerFactoryResolver
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ActionHandlerFactoryResolver implements ActionHandlerFactoryResolverInterface
{

    protected $classMaker;

    protected $iocContainer;

    /**
     * RequestHandlerFactoryResolver constructor.
     *
     * @param ClassMakerInterface $classMaker
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        IocContainerInterface $iocContainer
    ) {
        $this->classMaker = $classMaker;
        $this->iocContainer = $iocContainer;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveFactory(string $action, array $params): ActionHandlerFactoryInterface
    {
        $arr = explode('.', $action);
        foreach ($arr as &$v) {
            $v = ucfirst($v);
        }
        $vendor = array_shift($arr);
        $module = array_shift($arr);
        $factoryClass = $this->classMaker->makeClassName($vendor . '\\' . $module . '\\Ui\\Http\\Request\\' . implode('\\',
                $arr) . 'ActionHandlerFactory');
        if (!class_exists($factoryClass)) {
            $factoryClass = $this->classMaker->makeClassName(ActionHandlerFactory::class);
        }

        return new $factoryClass($this->iocContainer);

    }
}
