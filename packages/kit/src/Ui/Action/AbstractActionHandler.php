<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Action;

use Grifix\Demo\Ui\Common\Producer\RequestValidator\CreateProducerRequestValidator;
use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;
use Grifix\Kit\ArrayWrapper\ArrayWrapperInterface;
use Grifix\Kit\Conversion\Converter\ConverterFactoryInterface;
use Grifix\Kit\Conversion\Converter\StringToBoolConverter;
use Grifix\Kit\Conversion\Converter\StringToIntConverter;
use Grifix\Kit\Cqrs\CqrsClientTrait;
use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Ioc\ServiceLocatorTrait;
use Grifix\Kit\Type\DateTime\DateTime;
use Grifix\Shared\Ui\Common\RequestValidator\RequestValidatorFactoryInterface;

/**
 * Class AbstractRequestHandler
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractActionHandler implements ActionHandlerInterface
{
    use CqrsClientTrait;
    use ServiceLocatorTrait;

    public function __construct(IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
        $this->init();
    }

    protected function init(): void
    {
    }

    protected function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR
    ): string {
        return $this->getShared(TranslatorInterface::class)->translate($key, $vars, $quantity, $case, $form);
    }

    /**
     * @return LangInterface[]
     */
    protected function getLangs(): array
    {
        return $this->getShared(TranslatorInterface::class)->getLangs();
    }

    protected function convertBool($value)
    {
        return $this->getShared(ConverterFactoryInterface::class)
            ->create(StringToBoolConverter::class)
            ->convert($value);
    }

    protected function wrapArray(array $array = []): ArrayWrapperInterface
    {
        return $this->getShared(ArrayWrapperFactoryInterface::class)->create($array);
    }

    protected function nullifyEmptyStrings(array &$array): void
    {
        foreach ($array as $key => &$value) {
            if (is_array($value)) {
                $this->nullifyEmptyStrings($value);
                continue;
            }
            if ($value === '') {
                $value = null;
            }
        }
    }

    protected function convertDate(string $date): int
    {
        $result = new DateTime($date);
        return $result->getTimestamp();
    }
}
