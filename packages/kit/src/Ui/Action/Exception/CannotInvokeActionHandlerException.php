<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Action\Exception;

use Exception;

/**
 * Class InvalidRequestMethodException
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CannotInvokeActionHandlerException extends Exception
{
    protected $code = 400;

    public function __construct(protected bool $shouldHaveSideEffects, protected string $action)
    {
        if($shouldHaveSideEffects){
            parent::__construct(sprintf('Action "%s" must have side effects!', $action));
        }
        else{
            parent::__construct(sprintf('Action "%s" cannot have side effects!', $action));
        }
    }
}
