<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ui\Action;

use Grifix\Kit\Event\Bus\EventBusInterface;
use Grifix\Kit\Ui\Action\Event\BeforeDispatchActionEvent;
use Grifix\Kit\Ui\Action\Exception\CannotInvokeActionHandlerException;

/**
 * Class RequestDispatcher
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ActionDispatcher implements ActionDispatcherInterface
{

    /**
     * @var ActionHandlerFactoryResolverInterface
     */
    protected $actionHandlerFactoryResolver;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * RequestDispatcher constructor.
     *
     * @param ActionHandlerFactoryResolverInterface $actionHandlerFactoryResolver
     * @param EventBusInterface $eventBus
     */
    public function __construct(
        ActionHandlerFactoryResolverInterface $actionHandlerFactoryResolver,
        EventBusInterface $eventBus
    ) {
        $this->actionHandlerFactoryResolver = $actionHandlerFactoryResolver;
        $this->eventBus = $eventBus;
    }

    /**
     * {@inheritDoc}
     */
    public function dispatch(string $actionAlias, array $params = [], ?bool $shouldHaveSideEffects = null): array
    {
        $actionHandler = $this->actionHandlerFactoryResolver
            ->resolveFactory($actionAlias, $params)
            ->createActionHandler($actionAlias, $params);
        if (null !== $shouldHaveSideEffects && $actionHandler->hasSideEffects() !== $shouldHaveSideEffects) {
            throw new CannotInvokeActionHandlerException($shouldHaveSideEffects, $actionAlias);
        }

        $this->eventBus->send(new BeforeDispatchActionEvent($actionAlias, $params));
        return $actionHandler->__invoke($params);
    }
}
