<?php
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Exception;

use Exception;

class NoModulesRegisteredException extends Exception
{

}
