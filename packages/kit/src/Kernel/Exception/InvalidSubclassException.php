<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Exception;

/**
 * Class InvalidSubclassException
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidSubclassException extends \Exception
{
    protected $childClass;
    protected $parentClass;
    
    /**
     * InvalidSubclassException constructor.
     *
     * @param string $childClass
     * @param string $parentClass
     */
    public function __construct(string $childClass, string $parentClass)
    {
        $this->childClass = $childClass;
        $this->parentClass = $parentClass;
        
        $this->message = '"' . $childClass . '" must be subclass of ' . $parentClass;
        
        parent::__construct();
    }
}