<?php
declare(strict_types=1);

namespace Grifix\Kit\Kernel\Exception;

use Throwable;

/**
 * Class InvalidJsonException
 * @package Grifix\Kit\Kernel\Exception
 */
class InvalidJsonException extends \RuntimeException
{
    protected $path;

    public function __construct(string $path)
    {
        $this->path = $path;
        parent::__construct(sprintf('Invalid json in %s', $path));
    }
}
