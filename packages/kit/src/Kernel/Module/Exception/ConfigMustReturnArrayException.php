<?php declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Module\Exception;


class ConfigMustReturnArrayException extends \RuntimeException
{
    /** @var string */
    protected $path;

    public function __construct(string $path)
    {
        $this->path = $path;
        parent::__construct(sprintf('Config %s must return array!', $path));
    }

}
