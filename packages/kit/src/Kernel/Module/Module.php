<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Kernel\Module;

use Grifix\Kit\Exception\ExceptionProcessorInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\Exception\ConfigMustReturnArrayException;
use Grifix\Kit\Kernel\Module\Exception\InvalidCommandException;
use Grifix\Kit\Kernel\Module\Exception\InvalidExceptionProcessorException;
use Grifix\Kit\Middleware\MiddlewareInterface;

/**
 * Class Module
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Module implements ModuleInterface
{
    /**
     * @var string
     */
    protected $vendor;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $priority;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var bool
     */
    protected $isVendorModule;

    /**
     * @var bool
     */
    protected $isAppModule;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * Module constructor.
     *
     * @param string $vendor
     * @param string $name
     * @param int $priority
     * @param KernelInterface $kernel
     * @param FilesystemHelperInterface $filesystemHelper
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(
        string $vendor,
        string $name,
        int $priority,
        KernelInterface $kernel,
        FilesystemHelperInterface $filesystemHelper,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->vendor = $vendor;
        $this->name = $name;
        $this->priority = $priority;
        $this->kernel = $kernel;
        $this->filesystemHelper = $filesystemHelper;
        $this->arrayHelper = $arrayHelper;
    }


    /**
     * {@inheritdoc}
     */
    public function isVendorModule(): bool
    {
        if (is_null($this->isVendorModule)) {
            $this->isVendorModule = $this->filesystemHelper->fileExists($this->getVendorDir());
        }

        return $this->isVendorModule;
    }

    /**
     * {@inheritdoc}
     */
    public function isAppModule(): bool
    {
        if (is_null($this->isAppModule)) {
            $this->isAppModule = $this->filesystemHelper->fileExists($this->getAppDir());
        }

        return $this->isAppModule;
    }

    /**
     * {@inheritdoc}
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        if (is_null($this->namespace)) {
            $this->namespace = ucfirst($this->vendor) . '\\' . ucfirst($this->name);
        }

        return $this->namespace;
    }

    /**
     * {@inheritdoc}
     */
    public function getIoc()
    {
        return $this->kernel->getIoc();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getVendor(): string
    {
        return $this->vendor;
    }

    /**
     * {@inheritdoc}
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * {@inheritdoc}
     */
    public function getAppDir(): string
    {
        return $this->kernel->getAppDir() . DIRECTORY_SEPARATOR . $this->vendor . DIRECTORY_SEPARATOR . $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorDir(): string
    {
        return $this->kernel->getVendorDir() . DIRECTORY_SEPARATOR . $this->vendor . DIRECTORY_SEPARATOR . $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorPublicDir(): string
    {
        return $this->getVendorDir() . DIRECTORY_SEPARATOR . 'public';
    }

    protected function getAppConfigDir(): string
    {
        return $this->getAppDir() . DIRECTORY_SEPARATOR . 'config';
    }

    protected function getVendorConfigDir(): string
    {
        return $this->getVendorDir() . DIRECTORY_SEPARATOR . 'config';
    }

    /**
     * {@inheritdoc}
     */
    public function getAppPublicDir(): string
    {
        return $this->getAppDir() . DIRECTORY_SEPARATOR . 'public';
    }

    private function getAppEnvConfigDir(string $env): string
    {
        return $this->getAppConfigDir() . DIRECTORY_SEPARATOR . '.' . $env;
    }

    private function getVendorEnvConfigDir(string $env): string
    {
        return $this->getVendorConfigDir() . DIRECTORY_SEPARATOR . '.' . $env;
    }

    public function getConfigPrefix(): string
    {
        return $this->getVendor() . '.' . $this->getName();
    }

    /**
     * @{@inheritdoc}
     */
    public function getAssetsDir(): string
    {
        return $this->kernel->getAssetsDir() . DIRECTORY_SEPARATOR . $this->vendor . DIRECTORY_SEPARATOR . $this->name;
    }

    protected function loadConfig(string $path): array
    {
        $result = [];

        if ($this->filesystemHelper->fileExists($path)) {
            $files = $this->filesystemHelper->scanDir($path, true);
            foreach ($files as $file) {
                if ($file['type'] == FilesystemHelperInterface::TYPE_FILE && $file['extension'] = 'php') {
                    $key = str_replace(
                        [
                            $path . DIRECTORY_SEPARATOR,
                            DIRECTORY_SEPARATOR,
                            '.php',
                        ],
                        ['', '.', ''],
                        $file['path']
                    );
                    /** @noinspection PhpIncludeInspection */
                    $config = include $file['path'];
                    if (!is_array($config)) {
                        throw new ConfigMustReturnArrayException($file['path']);
                    }
                    $this->arrayHelper->set($result, $key, $config);
                }
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(string $environment = null): array
    {
        if (is_null($this->config)) {
            $this->config = [];
            if ($this->isVendorModule()) {
                $this->config = $this->arrayHelper->merge(
                    $this->config,
                    $this->loadConfig($this->getVendorConfigDir())
                );
                if($environment && $this->getVendorEnvConfigDir($environment)){
                    $this->config = $this->arrayHelper->merge(
                        $this->config,
                        $this->loadConfig($this->getVendorEnvConfigDir($environment))
                    );
                }
            }
            if ($this->isAppModule()) {
                $this->config = $this->arrayHelper->merge(
                    $this->config,
                    $this->loadConfig($this->getAppConfigDir())
                );
                if($environment && $this->getAppEnvConfigDir($environment)){
                    $this->config = $this->arrayHelper->merge(
                        $this->config,
                        $this->loadConfig($this->getAppEnvConfigDir($environment))
                    );
                }
            }
        }

        return $this->config;
    }

    /**
     * @param string $className
     *
     * @return void
     * @throws InvalidCommandException
     */
    protected function runCommand(string $className)
    {
        $command = $this->makeModuleObject($className);

        if ($command) {
            if (!($command instanceof ModuleCommandInterface)) {
                throw new InvalidCommandException($command);
            }

            $command->run();
        }
    }

    /**
     * @return MiddlewareInterface|null
     */
    public function makeMiddleware()
    {
        return $this->makeModuleObject('Middleware');
    }

    /**
     * @param $className
     *
     * @return ModuleClassInterface|MiddlewareInterface|ExceptionProcessorInterface|null
     */
    protected function makeModuleObject($className)
    {
        $class = '\\App\\' . $this->getNamespace() . '\\Infrastructure\\Grifix\\' . $className;
        $result = null;
        if (class_exists($class)) {
            $result = new $class($this);
        }

        $class = '\\' . $this->getNamespace() . '\\Infrastructure\\Grifix\\' . $className;
        if (class_exists($class)) {
            $result = new $class($this);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function processException(\Throwable $exception): \Throwable
    {
        $result = $exception;
        $processor = $this->makeModuleObject('ExceptionProcessor');
        if ($processor) {
            if (!($processor instanceof ExceptionProcessorInterface)) {
                throw new InvalidExceptionProcessorException($processor);
            }

            $result = $processor->process($exception);
        }

        return $result;
    }

    public function publishAssets()
    {
        $fs = $this->kernel->getIoc()->get(FilesystemHelperInterface::class);
        if ($fs->isDir($this->getAssetsDir())) {
            $fs->deleteDir($this->getAssetsDir());
        }
        if ($fs->isDir($this->getVendorPublicDir())) {
            $fs->copyDir($this->getVendorPublicDir(), $this->getAssetsDir());
        }
        if ($fs->isDir($this->getAppPublicDir())) {
            $fs->copyDir($this->getAppPublicDir(), $this->getAssetsDir());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function bootstrap(): void
    {
        $this->runCommand('Bootstrap');
    }

    /**
     * {@inheritdoc}
     */
    public function install(bool $publishAssets = true): void
    {
        if ($publishAssets) {
            $this->publishAssets();
        }
        $this->runCommand('Install');
    }

    /**
     * {@inheritdoc}
     */
    public function update(bool $publishAssets = true): void
    {
        if ($publishAssets) {
            $this->publishAssets();
        }
        $this->runCommand('Update');
    }
}
