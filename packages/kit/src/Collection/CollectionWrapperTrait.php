<?php
declare(strict_types=1);

namespace Grifix\Kit\Collection;

/**
 * Class CollectionTrait
 * @package Grifix\Kit\Orm\Collection
 */
trait CollectionWrapperTrait
{

    /**
     * @return \Grifix\Kit\Collection\GenericCollectionInterface
     */
    protected function getWrappedCollection(): GenericCollectionInterface
    {
        return $this->collection;
    }

    /**
     * @param $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->getWrappedCollection()->offsetGet($offset);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $this->getWrappedCollection()->unserialize($serialized);
    }

    public function clear()
    {
        $this->getWrappedCollection()->clear();
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->getWrappedCollection()->count();
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->getWrappedCollection()->isEmpty();
    }

    /**
     * @param $offset
     * @param $value
     */
    public function offsetSet($offset, $value)
    {
        $this->getWrappedCollection()->offsetSet($offset, $value);
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->getWrappedCollection()->jsonSerialize();
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return $this->getWrappedCollection()->serialize();
    }

    /**
     * @param $offset
     */
    public function offsetUnset($offset)
    {
        $this->getWrappedCollection()->offsetUnset($offset);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->getWrappedCollection()->toArray();
    }

    /**
     * @param $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->getWrappedCollection()->offsetExists($offset);
    }

    /**
     * @return \Ds\Collection
     */
    public function copy(): \Ds\Collection
    {
        return $this->getWrappedCollection()->copy();
    }

    /**
     * @return \Traversable
     */
    public function getIterator()
    {
        return $this->getWrappedCollection()->getIterator();
    }
}
