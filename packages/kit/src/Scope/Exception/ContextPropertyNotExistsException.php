<?php
declare(strict_types=1);

namespace Grifix\Kit\Scope\Exception;

/**
 * Class ContextPropertyNotExistsException
 * @package Grifix\Kit\Context\Exception
 */
class ContextPropertyNotExistsException extends \Exception
{
    protected $property;

    /**
     * ContextPropertyAlreadySetException constructor.
     * @param string $property
     */
    public function __construct(string $property)
    {
        $this->property = $property;
        parent::__construct(sprintf('Context property "%s" does not exist!', $property));
    }
}
