<?php
declare(strict_types=1);

namespace Grifix\Kit\Scope;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ContextFactory
 * @package Grifix\Kit\Context
 */
class ScopeFactory extends AbstractFactory implements ScopeFactoryInterface
{
    /**
     * @return ScopeInterface
     */
    public function createContext(): ScopeInterface
    {
        $class = $this->makeClassName(Scope::class);
        return new $class;
    }
}
