<?php
declare(strict_types=1);

namespace Grifix\Kit\Scope;

/**
 * Class ContextFactory
 * @package Grifix\Kit\Context
 */
interface ScopeFactoryInterface
{
    /**
     * @return ScopeInterface
     */
    public function createContext(): ScopeInterface;
}
