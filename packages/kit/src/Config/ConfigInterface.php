<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Config;

use Grifix\Kit\Contract\ArrayableInterface;

/**
 * Class Config
 *
 * @category Grifix
 * @package  Grifix\Kit\Config
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ConfigInterface extends ArrayableInterface
{
    /**
     * {@inheritdoc}
     */
    public function get($key, $defaultValue = null);
    
    /**
     * {@inheritdoc}
     */
    public function set($key, $val);
    
    /**
     * {@inheritdoc}
     */
    public function clear();

    /**
     * @param array $values
     *
     * @return $this
     */
    public function merge(array $values);
}
