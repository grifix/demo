<?php

declare(strict_types=1);

namespace Grifix\Kit\Uuid;

use Ramsey\Uuid\Uuid;

class UuidGenerator implements UuidGeneratorInterface
{
    public function generateUuid4(): string
    {
        return Uuid::uuid4()->toString();
    }
}
