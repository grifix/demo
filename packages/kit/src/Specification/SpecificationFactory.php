<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Specification;

use Grifix\Kit\Expression\Builder\ExpressionBuilderFactoryInterface;
use Grifix\Kit\Expression\ExpressionFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class SpecificationFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SpecificationFactory extends AbstractFactory implements SpecificationFactoryInterface
{

    /**
     * @var ExpressionFactoryInterface
     */
    protected $expressionFactory;

    /**
     * @var ExpressionBuilderFactoryInterface
     */
    protected $builderFactory;

    /**
     * @var \Grifix\Kit\Expression\Builder\ExpressionBuilderInterface
     */
    protected $builder;

    /**
     * SpecificationFactory constructor.
     *
     * @param ExpressionFactoryInterface $expressionFactory
     * @param ExpressionBuilderFactoryInterface $queryBuilderFactory
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(
        ExpressionFactoryInterface $expressionFactory,
        ExpressionBuilderFactoryInterface $queryBuilderFactory,
        ClassMakerInterface $classMaker
    ) {
        $this->expressionFactory = $expressionFactory;
        $this->builderFactory = $queryBuilderFactory;
        $this->builder = $this->builderFactory->createPhpBuilder();
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function createOrSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): NestedSpecificationInterface {
        $class = $this->makeClassName(OrSpecification::class);

        return new $class($specification, $property);
    }

    /**
     * {@inheritdoc}
     */
    public function createAndSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): NestedSpecificationInterface {
        $class = $this->makeClassName(AndSpecification::class);

        return new $class($specification, $property);
    }

    /**
     * {@inheritdoc}
     */
    public function createSpecification($class): SpecificationInterface
    {
        $class = $this->makeClassName($class);

        return new $class($this->expressionFactory, $this->builder, $this);
    }

    /**
     * {@inheritdoc}
     */
    public function createSimpleSpecification(string $property, $value, int $logic = SimpleSpecification::LOGIC_EQUAL)
    {
        $class = $this->makeClassName(SimpleSpecification::class);

        return new $class($property, $value, $logic, $this->expressionFactory, $this->builder, $this);
    }
}