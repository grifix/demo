<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Specification;

use Grifix\Kit\Expression\Builder\ExpressionBuilderInterface;
use Grifix\Kit\Expression\ExpressionFactoryInterface;
use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\Exception\InvalidLogicException;

/**
 * Class SimpleSpecification
 * Простая спецификация на одно поле
 *
 * @category Grifix
 * @package  Grifix\Kit\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SimpleSpecification extends AbstractSpecification
{
    const LOGIC_EQUAL = 1;
    const LOGIC_NOT_EQUAL = 2;
    const LOGIC_GREATER_THAN = 3;
    const LOGIC_GREATER_THAN_OR_EQUAL = 4;
    const LOGIC_IN = 5;
    const LOGIC_LESS_THAN = 6;
    const LOGIC_LESS_THAN_OR_EQUAL = 7;
    
    protected $property;
    protected $value;
    protected $logic;
    
    /**
     * SimpleSpecification constructor.
     *
     * @param string                        $property
     * @param ExpressionBuilderInterface              $value
     * @param int                           $logic
     * @param ExpressionFactoryInterface    $expressionFactory
     * @param ExpressionBuilderInterface              $builder
     * @param SpecificationFactoryInterface $specificationFactory
     */
    public function __construct(
        string $property,
        $value,
        int $logic,
        ExpressionFactoryInterface $expressionFactory,
        ExpressionBuilderInterface $builder,
        SpecificationFactoryInterface $specificationFactory
    ) {
        $this->property = $property;
        $this->value = $value;
        $this->logic = $logic;
        parent::__construct($expressionFactory, $builder, $specificationFactory);
       
    }
    
    /**
     * {@inheritdoc}
     */
    protected function createExpression(): ExpressionInterface
    {
        switch ($this->logic) {
            case self::LOGIC_EQUAL:
                return $this->expressionFactory->eq($this->property, $this->value);
            case self::LOGIC_NOT_EQUAL:
                return $this->expressionFactory->ne($this->property, $this->value);
            case self::LOGIC_GREATER_THAN:
                return $this->expressionFactory->gt($this->property, $this->value);
            case self::LOGIC_GREATER_THAN_OR_EQUAL:
                return $this->expressionFactory->gte($this->property, $this->value);
            case self::LOGIC_IN:
                return $this->expressionFactory->in($this->property, $this->value);
            case self::LOGIC_LESS_THAN:
                return $this->expressionFactory->lt($this->property, $this->value);
            case self::LOGIC_LESS_THAN_OR_EQUAL:
                return $this->expressionFactory->lte($this->property, $this->value);
        }
    
        throw new InvalidLogicException($this);
    }
}