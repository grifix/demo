<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector;


use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class SpecificationInjector
 * @package Grifix\Kit\Orm\Collection\SpecificationInjector
 *
 * Injects specification into collection query
 */
interface SpecificationInjectorInterface
{
    /**
     * @param SpecificationInterface $specification
     * @throws \Grifix\Kit\Expression\Exception\OperandNotExistsException
     * @throws \Grifix\Kit\Expression\Exception\UnknownExpressionException
     * @throws \ReflectionException
     */
    public function inject(SpecificationInterface $specification);
}