<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ValueConverter;


/**
 * Class ValueConverterFactory
 * @package Grifix\Kit\Specification\Injector\ValueConverter
 */
interface ValueConverterFactoryInterface
{
    /**
     * @param \Closure[] $converters
     * @return ValueConverterInterface
     */
    public function createValueConverter(array $converters = []): ValueConverterInterface;

    /**
     * @param array $converters
     * @return ValueConverterInterface
     */
    public function createJsonValueConverter(array $converters = []): ValueConverterInterface;
}