<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ValueConverter;


/**
 * Class ValueConverter
 * @package Grifix\Kit\Specification\Injector\ValueConverter
 */
interface ValueConverterInterface
{
    /**
     * @param string $varName
     * @param $value
     * @return mixed|string
     */
    public function convertValue(string $varName, $value);
}