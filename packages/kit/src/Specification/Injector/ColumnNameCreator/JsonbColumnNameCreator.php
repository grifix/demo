<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ColumnNameCreator;

/**
 * Class ColumnNameCreater
 * @package Grifix\Kit\Specification\Injector\ColumnNameCreator
 */
class JsonbColumnNameCreator implements ColumnNameCreatorInterface
{

    protected $map;

    /**
     * JsonbColumnNameCreator constructor.
     * @param array $map
     */
    public function __construct(array $map = [])
    {
        $this->map = $map;
    }

    /**
     * {@inheritdoc}
     */
    public function crateColumnName(string $varName): string
    {
        if (isset($this->map[$varName])) {
            return $this->map[$varName];
        }
        $result = [];
        $arr = explode('.', $varName);
        foreach ($arr as $v) {
            $result[] = "'" . $v . "'";
        }

        $result = 'data->' . implode('->', $result);
        return $result;
    }
}
