<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Specification;


/**
 * Class SpecificationFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SpecificationFactoryInterface
{
    /**
     * @param SpecificationInterface $specification
     * @param string|null            $property
     *
     * @return NestedSpecificationInterface
     */
    public function createOrSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): NestedSpecificationInterface;
    
    /**
     * @param SpecificationInterface $specification
     * @param string|null            $property
     *
     * @return NestedSpecificationInterface
     */
    public function createAndSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): NestedSpecificationInterface;

    /**
     * @param $class
     * @return SpecificationInterface
     */
    public function createSpecification($class): SpecificationInterface;
    
    /**
     * Создает простую спецификацию на одно поле
     *
     * @param string $property
     * @param        $value
     * @param int    $logic
     *
     * @return mixed
     */
    public function createSimpleSpecification(string $property, $value, int $logic = SimpleSpecification::LOGIC_EQUAL);
}