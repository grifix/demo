<?php declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

abstract class AbstractEmailMessage implements EmailMessageInterface
{

    /** @var string */
    protected $recipientEmail;

    /** @var string */
    protected $subject;

    /** @var object */
    protected $viewVars;

    /** @var string */
    protected $viewPath;

    /** @var string|null */
    protected $recipientName;

    /** @var string|null */
    protected $senderEmail;

    /** @var string|null */
    protected $senderName;

    public function __construct(
        string $recipientEmail,
        string $subject,
        object $viewVars,
        string $viewPath,
        ?string $recipientName = null,
        ?string $senderEmail = null,
        ?string $senderName = null
    ) {
        $this->recipientEmail = $recipientEmail;
        $this->subject = $subject;
        $this->viewVars = $viewVars;
        $this->viewPath = $viewPath;
        $this->recipientName = $recipientName;
        $this->senderEmail = $senderEmail;
        $this->senderName = $senderName;
    }

    public function getRecipientEmail(): string
    {
        return $this->recipientEmail;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getViewVars(): object
    {
        return $this->viewVars;
    }

    public function getViewPath(): string
    {
        return $this->viewPath;
    }

    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    public function getSenderEmail(): ?string
    {
        return $this->senderEmail;
    }

    public function getSenderName(): ?string
    {
        return $this->senderName;
    }
}
