<?php declare(strict_types = 1);

namespace Grifix\Kit\Mailer\Exception;

use Exception;

class CannotSendEmailException extends Exception
{
    public function __construct(string $originSequenceEmail, string $toEmail)
    {
        parent::__construct(sprintf("Cannot send email from %s to %s!", $originSequenceEmail, $toEmail));
    }
}
