<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer\Exception;

/**
 * Class InvalidTransportTypeException
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidTransportTypeException extends \Exception
{
    protected $transportType;
    
    /**
     * InvalidTransportTypeException constructor.
     *
     * @param string $transportMethod
     */
    public function __construct(string $transportMethod)
    {
        $this->transportType = $transportMethod;
        $this->message = 'Invalid transport type "' . $transportMethod . '"!';
        parent::__construct();
    }
}