<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;


/**
 * Class Mailer
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface MailerInterface
{
    public function send(
        string $toEmail,
        string $subject,
        string $content,
        string $toName,
        ?string $fromEmail = null,
        ?string $fromName = null
    ): void;

    public function sendMessage(EmailMessageInterface $message): void;
}
