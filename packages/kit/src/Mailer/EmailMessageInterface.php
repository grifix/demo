<?php declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

interface EmailMessageInterface
{
    public function getRecipientEmail(): string;

    public function getRecipientName(): ?string;

    public function getSenderEmail(): ?string;

    public function getSenderName(): ?string;

    public function getViewVars(): object;

    public function getViewPath(): string;

    public function getSubject(): string;
}
