<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Mailer\Exception\InvalidTransportTypeException;
use Grifix\Kit\View\ViewFactoryInterface;
use Swift_SmtpTransport;

/**
 * Class MailerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MailerFactory extends AbstractFactory implements MailerFactoryInterface
{
    protected Swift_SmtpTransport $transport;

    protected string $defaultFromEmail;

    protected string $defaultFromName;

    protected ViewFactoryInterface $viewFactory;
    
    /**
     *
     * @param string                  $defaultFromEmail <cfg:grifix.kit.mailer.fromEmail>
     * @param string                  $defaultFormName  <cfg:grifix.kit.mailer.fromName>
     * @param string                  $transportType    <cfg:grifix.kit.mailer.transport>
     * @param string                  $host             <cfg:grifix.kit.mailer.host>
     * @param int                     $port             <cfg:grifix.kit.mailer.port>
     * @param string|null             $userName         <cfg:grifix.kit.mailer.userName>
     * @param string|null             $password         <cfg:grifix.kit.mailer.password>
     *
     * @throws InvalidTransportTypeException
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        ViewFactoryInterface $viewFactory,
        string $defaultFromEmail,
        string $defaultFormName,
        string $transportType = self::TRANSPRORT_SMTP,
        string $host = 'localhost',
        int $port = 25,
        string $userName = null,
        string $password = null
    ) {
        switch ($transportType) {
            case self::TRANSPRORT_SMTP:
                $this->transport = new Swift_SmtpTransport($host, $port);
                if ($userName) {
                    $this->transport->setUsername($userName);
                }
                if ($password) {
                    $this->transport->setPassword($password);
                }
                break;
            default:
                throw new InvalidTransportTypeException($transportType);
        }
        $this->defaultFromEmail = $defaultFromEmail;
        $this->defaultFromName = $defaultFormName;
        $this->viewFactory = $viewFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * @return MailerInterface
     */
    public function create(): MailerInterface
    {
        $class = $this->makeClassName(Mailer::class);
        
        return new $class($this->transport, $this->viewFactory, $this->defaultFromEmail, $this->defaultFromName);
    }
}
