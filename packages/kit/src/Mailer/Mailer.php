<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

use Grifix\Kit\Mailer\Exception\CannotSendEmailException;
use Grifix\Kit\View\ViewFactoryInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_Transport;

class Mailer implements MailerInterface
{

    /** @var Swift_Mailer */
    protected $swiftMailer;

    /** @var string */
    protected $defaultFromEmail;

    /** @var string */
    protected $defaultFromName;

    /** @var ViewFactoryInterface */
    protected $viewFactory;

    public function __construct(
        Swift_Transport $transport,
        ViewFactoryInterface $viewFactory,
        string $defaultFromEmail,
        string $defaultFromName
    ) {
        $this->swiftMailer = new Swift_Mailer($transport);
        $this->defaultFromEmail = $defaultFromEmail;
        $this->defaultFromName = $defaultFromName;
        $this->viewFactory = $viewFactory;
    }

    public function send(
        string $toEmail,
        string $subject,
        string $content,
        ?string $toName = null,
        ?string $fromEmail = null,
        ?string $fromName = null
    ): void {
        $message = (new Swift_Message($subject, $content))
            ->setTo($toEmail, $toName)
            ->setFrom(
                $fromEmail ?? $this->defaultFromEmail,
                $fromName ?? $this->defaultFromName
            );
        $this->sendSwiftMessage($message);
    }

    public function sendMessage(EmailMessageInterface $message): void
    {
        $view = $this->viewFactory->create($message->getViewPath());
        $swiftMessage = new Swift_Message(
            $message->getSubject(),
            $view->render(['content' => $message->getViewVars()]),
            'text/html'
        );
        $swiftMessage
            ->setTo($message->getRecipientEmail())
            ->setFrom(
                $message->getSenderEmail() ?? $this->defaultFromEmail,
                $message->getSenderName() ?? $this->defaultFromName
            );
        $this->sendSwiftMessage($swiftMessage);
    }

    protected function sendSwiftMessage(Swift_Message $message): void
    {
        if ($this->swiftMailer->send($message) === 0) {
            throw new CannotSendEmailException($message->getFrom(), $message->getTo()[0]);
        }
    }
}
