<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Conversion\Converter;

/**
 * Class StringToIntConverter
 *
 * @category Grifix
 * @package  Grifix\Kit\Conversion\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StringToBoolConverter extends AbstractConverter
{
    /**
     * {@inheritdoc}
     */
    protected function doConvert($value)
    {
        if(in_array($value, array('true', 'on'))){
            return true;
        }

        if(in_array($value, array('false', 'off', ''))){
            return false;
        }
        
        return boolval($value);
    }
}
