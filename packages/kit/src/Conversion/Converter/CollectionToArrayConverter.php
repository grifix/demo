<?php


namespace Grifix\Kit\Conversion\Converter;

use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class CollectionToArrayConverter
 * @package Grifix\Kit\Conversion\Serializer
 */
class CollectionToArrayConverter extends AbstractConverter
{
    /**
     * {@inheritdoc}
     */
    protected function doConvert($value)
    {
        if ($value instanceof CollectionInterface) {
            return $value->toArray();
        }
        return $value;
    }
}