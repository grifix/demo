<?php
declare(strict_types=1);

namespace Grifix\Kit\Conversion\Converter;

/**
 * Class VoToStringConveter
 *
 * @package Grifix\Kit\Conversion\Serializer
 */
class VoToStringConverter extends AbstractConverter
{
    /**
     * @param object $value
     *
     * @return mixed
     */
    public function doConvert($value)
    {
        return $value->__toString();
    }
}