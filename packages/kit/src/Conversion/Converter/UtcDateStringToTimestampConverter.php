<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Converter;

use DateTimeImmutable;
use DateTimeZone;
use Throwable;

class UtcDateStringToTimestampConverter extends AbstractConverter
{
    protected function doConvert($value)
    {
        if (!$value) {
            return null;
        }
        try {
            return (new DateTimeImmutable($value, new DateTimeZone('UTC')))->getTimestamp();
        } catch (Throwable $e) {
            return $value;
        }
    }
}
