<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Conversion\Converter;

/**
 * Class AbstractConverter
 *
 * @category Grifix
 * @package  Kit\Conversion\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractConverter implements ConverterInterface
{

    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * {@inheritdoc}
     */
    public function convert($value)
    {
        if ($this->enabled) {
            return $this->doConvert($value);
        }
        return $value;
    }

    /**
     * @return ConverterInterface
     */
    public function enable(): ConverterInterface
    {
        $this->enabled = true;
        return $this;
    }

    /**
     * @return ConverterInterface
     */
    public function disable(): ConverterInterface
    {
        $this->enabled = false;
        return $this;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    abstract protected function doConvert($value);
}
