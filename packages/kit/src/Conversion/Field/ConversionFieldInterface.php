<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Conversion\Field;

use Grifix\Kit\Conversion\Exception\ConverterAlreadyExistsException;
use Grifix\Kit\Conversion\Exception\ConverterDoesNotExistsException;
use Grifix\Kit\Conversion\Converter\ConverterInterface;


/**
 * Class Field
 *
 * @category Grifix
 * @package  Grifix\Kit\Field
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ConversionFieldInterface
{
    /**
     * @return ConversionFieldInterface
     */
    public function enable(): ConversionFieldInterface;
    
    /**
     * @return ConversionFieldInterface
     */
    public function disable(): ConversionFieldInterface;
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @return bool
     */
    public function isEnabled(): bool;
    
    /**
     * @param string $converterClass
     *
     * @return bool
     */
    public function hasConverter(string $converterClass): bool;
    
    /**
     * @param string $converterClass
     *
     * @return ConversionFieldInterface
     */
    public function removeConverter(string $converterClass): ConversionFieldInterface;
    
    /**
     * @param string $converterClass
     *
     * @return ConverterInterface
     * @throws ConverterDoesNotExistsException
     */
    public function getConverter(string $converterClass): ConverterInterface;
    
    /**
     * @param ConverterInterface $converter
     *
     * @return ConversionFieldInterface
     * @throws ConverterAlreadyExistsException
     */
    public function addConverter(ConverterInterface $converter): ConversionFieldInterface;
    
    /**
     * @param string $converterClass
     *
     * @return ConverterInterface
     */
    public function createConverter(string $converterClass): ConverterInterface;
    
    /**
     * @param $value
     *
     * @return mixed
     */
    public function convert($value);
    
    /**
     * @param string $name
     *
     * @return ConversionFieldInterface
     */
    public function setName(string $name): ConversionFieldInterface;
}
