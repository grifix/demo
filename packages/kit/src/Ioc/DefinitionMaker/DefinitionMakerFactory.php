<?php declare(strict_types = 1);

namespace Grifix\Kit\Ioc\DefinitionMaker;

use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\SimpleCache\CacheInterface;

class DefinitionMakerFactory extends AbstractFactory implements DefinitionMakerFactoryInterface
{
    public function __construct(
        ClassMakerInterface $classMaker,
        ImplementationFinderFactoryInterface $implementationFinderFactory
    ) {
        parent::__construct($classMaker);
        $this->implementationFinderFactory = $implementationFinderFactory;
    }

    /** @var ImplementationFinderFactoryInterface */
    protected $implementationFinderFactory;

    public function create(array $namespaces, CacheInterface $cache): DefinitionMakerInterface
    {
        return new DefinitionMaker($cache, $this->implementationFinderFactory->create($namespaces, $cache));
    }
}
