<?php


namespace Grifix\Kit\Ioc\DefinitionMaker;

/**
 * Class Definition
 * @package Grifix\Kit\Ioc
 */
class DependencyDefinition
{
    protected string $className;

    protected array $args = [];

    /**
     * Definiton constructor.
     * @param string $className
     * @param array $args
     */
    public function __construct(string $className, array $args = [])
    {
        $this->className = $className;
        $this->args = $args;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param array $args
     * @return DependencyDefinition
     */
    public function withArgs(array $args)
    {
        return new self($this->className, $args);
    }
}
