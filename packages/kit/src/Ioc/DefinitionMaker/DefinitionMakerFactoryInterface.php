<?php declare(strict_types = 1);

namespace Grifix\Kit\Ioc\DefinitionMaker;

use Psr\SimpleCache\CacheInterface;

interface DefinitionMakerFactoryInterface
{
    public function create(array $namespaces, CacheInterface $cache): DefinitionMakerInterface;
}
