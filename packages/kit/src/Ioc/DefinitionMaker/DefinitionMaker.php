<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ioc\DefinitionMaker;

use Grifix\Kit\Alias;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class DefinitionMaker
 *
 * @category Grifix
 * @package  Grifx\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DefinitionMaker implements DefinitionMakerInterface
{
    /** @var CacheInterface */
    protected $cache;

    /** @var ImplementationFinderInterface */
    protected $implementationFinder;

    public function __construct(CacheInterface $cache, ImplementationFinderInterface $implementationFinder)
    {
        $this->cache = $cache;
        $this->implementationFinder = $implementationFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function makeDefinition(string $alias): DependencyDefinition
    {
        $cached = $this->cache->get($this->makeCacheKey($alias));
        if (null !== $cached) {
            return $cached;
        }

        $className = $this->detectClassName($alias);
        $definition = new DependencyDefinition($className, $this->defineConstructorArgs($className));

        $this->cache->set($this->makeCacheKey($alias), $definition);

        return $definition;
    }

    /**
     * {@inheritdoc}
     */
    public function complementDefinition(DependencyDefinition $definition): DependencyDefinition
    {
        $autoDefinition = $this->makeDefinition($definition->getClassName());
        $autoArgs = $autoDefinition->getArgs();
        $args = $definition->getArgs();
        foreach ($autoArgs as $i => $arg) {
            if (!array_key_exists($i, $args)) {
                $args[$i] = $autoArgs[$i];
            }
        }
        ksort($args);
        return $definition->withArgs($args);
    }

    /**
     * @param string $className
     * @return array
     * @throws \ReflectionException
     */
    protected function defineConstructorArgs(string $className)
    {
        $constructor = (new \ReflectionClass($className))->getConstructor();
        $result = [];

        if ($constructor) {
            $params = $constructor->getParameters();
            $doc = $constructor->getDocComment();
            foreach ($params as $param) {
                $matches = [];
                $pattern = '/\$' . $param->getName() . '\s*<(.*)>/';
                if ($doc && preg_match($pattern, $doc, $matches)) {
                    $result[] = $matches[1];
                } elseif ($param->getType()) {
                    $result[] = $param->getType()->getName();
                } else {
                    $result[] = Alias::UNKNOWN;
                }
            }
        }
        return $result;
    }

    protected function detectClassName(string $alias)
    {
        if (interface_exists($alias)) {
            return $this->implementationFinder->find($alias);
        }
        return $this->findClass($alias);
    }

    protected function findClass(string $class): ?string
    {
        $appClass = $this->makeAppClass($class);
        if (class_exists($appClass)) {
            return $appClass;
        }
        return $class;
    }

    protected function makeAppClass(string $class): string
    {
        return sprintf('App\\%s', $class);
    }

    /**
     * @param string $alias
     *
     * @return string
     */
    protected function makeCacheKey(string $alias): string
    {
        return str_replace('\\', '.', get_class($this) . '.' . $alias);
    }
}
