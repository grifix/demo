<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ioc\DefinitionMaker;

use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;

/**
 * Class DefinitionMaker
 *
 * @category Grifix
 * @package  Grifx\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface DefinitionMakerInterface
{
    /**
     * @param string $alias
     * @return DependencyDefinition
     */
    public function makeDefinition(string $alias): DependencyDefinition;

    /**
     * @param string $alias
     * @param DependencyDefinition $definition
     * @return DependencyDefinition
     */
    public function complementDefinition(DependencyDefinition $definition): DependencyDefinition;
}
