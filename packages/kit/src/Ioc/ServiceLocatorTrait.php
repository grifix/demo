<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ioc;

/**
 * Class ServiceLocatorTrait
 *
 * @category Grifix
 * @package  Grifix\Kit\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
trait ServiceLocatorTrait
{
    private $iocContainer;

    /**
     * ServiceLocatorTrait constructor.
     *
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }
}