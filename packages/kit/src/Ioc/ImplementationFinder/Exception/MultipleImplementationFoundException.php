<?php declare(strict_types = 1);

namespace Grifix\Kit\Ioc\ImplementationFinder\Exception;

use Mockery\Exception\RuntimeException;

class MultipleImplementationFoundException extends RuntimeException
{
    /** @var string[] */
    private $realizations = [];

    /** @var string */
    private $interfaceName;

    /**
     * @param string $interfaceName
     * @param string[] $realizations
     */
    public function __construct(string $interfaceName, array $realizations)
    {
        $this->realizations = $realizations;
        $this->interfaceName = $interfaceName;
        parent::__construct(
            sprintf(
                'There is more than one implementation for the interface %s [%s]',
                $interfaceName,
                implode(', ', $realizations)
            )
        );
    }


}
