<?php declare(strict_types = 1);

namespace Grifix\Kit\Ioc\ImplementationFinder\Exception;

use Mockery\Exception\RuntimeException;

class ImplementationNotFoundException extends RuntimeException
{
    /** @var string */
    private $interface;

    public function __construct(string $interface)
    {
        $this->interface = $interface;
        parent::__construct(sprintf('There is no implementation for the interface %s', $interface));
    }
}
