<?php declare(strict_types = 1);

namespace Grifix\Kit\Ioc\ImplementationFinder;

use Grifix\Kit\Ioc\ImplementationFinder\Exception\ThereIsNoDefaultImplementationForInterfaceException;

interface ImplementationFinderInterface
{
    public function find(string $interface): string;

    /**
     * @return string[]
     */
    public function findAll(string $interface): array;

    public function getDefault(string $interface): string;

    public function findDefault(string $interface): ?string;
}
