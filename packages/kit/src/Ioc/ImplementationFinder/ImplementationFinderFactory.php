<?php declare(strict_types=1);

namespace Grifix\Kit\Ioc\ImplementationFinder;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\SimpleCache\CacheInterface;

class ImplementationFinderFactory extends AbstractFactory implements ImplementationFinderFactoryInterface
{
    /** @var ClassHelperInterface */
    protected $classHelper;

    public function __construct(ClassMakerInterface $classMaker, ClassHelperInterface $classHelper)
    {
        parent::__construct($classMaker);
        $this->classHelper = $classHelper;
    }

    public function create(array $namespaces, CacheInterface $cache): ImplementationFinderInterface
    {
        $class = $this->makeClassName(ImplementationFinder::class);
        return new $class($cache, $this->classHelper, $namespaces);
    }
}
