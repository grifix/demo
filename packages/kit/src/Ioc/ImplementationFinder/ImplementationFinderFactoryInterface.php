<?php declare(strict_types = 1);

namespace Grifix\Kit\Ioc\ImplementationFinder;

use Psr\SimpleCache\CacheInterface;

interface ImplementationFinderFactoryInterface
{
    public function create(array $namespaces, CacheInterface $cache): ImplementationFinderInterface;
}
