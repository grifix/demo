<?php declare(strict_types=1);

namespace Grifix\Kit\Ioc\ImplementationFinder;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\ImplementationFinder\Exception\ImplementationNotFoundException;
use Grifix\Kit\Ioc\ImplementationFinder\Exception\MultipleImplementationFoundException;
use Grifix\Kit\Ioc\ImplementationFinder\Exception\ThereIsNoDefaultImplementationForInterfaceException;
use Psr\SimpleCache\CacheInterface;
use ReflectionClass;

class ImplementationFinder implements ImplementationFinderInterface
{
    private const KEY_CLASSES = 'classes';

    protected CacheInterface $cache;

    /** @var string[] */
    protected array $namespaces;

    /** @var string[] */
    protected array $classes = [];

    protected ClassHelperInterface $classHelper;

    /**
     * @param string[] $namespaces
     */
    public function __construct(CacheInterface $cache, ClassHelperInterface $classHelper, array $namespaces)
    {
        $this->cache = $cache;
        $this->namespaces = $namespaces;
        $this->classHelper = $classHelper;
    }

    public function find(string $interface): string
    {
        $result = array_unique($this->findAll($interface));

        if (!$result) {
            throw new ImplementationNotFoundException($interface);
        }

        if (count($result) === 1) {
            return $result[0];
        }

        if (count($result) === 2 && $this->isAppExtension($result[1], $result[0])) {
            return $result[1];
        }

        $defaultImplementation = $this->findDefaultImplementation($interface, $result);

        if ($defaultImplementation) {
            return $defaultImplementation;
        } elseif (count($result) > 1) {
            throw new MultipleImplementationFoundException($interface, $result);
        }

        throw new ImplementationNotFoundException($interface);
    }

    public function findAll(string $interface): array
    {
        $result = [];
        foreach ($this->getClasses() as $class) {
            if ($class !== $interface
                && is_subclass_of($class, $interface)
                && !(new ReflectionClass($class))->isAbstract()
            ) {
                $result[] = $class;
            }
        }
        return $result;
    }

    public function getDefault(string $interface): string
    {
        $result = $this->findDefault($interface);
        if (empty($result)) {
            throw new ThereIsNoDefaultImplementationForInterfaceException($interface);
        }
        return $result;
    }

    public function findDefault(string $interface): ?string
    {
        return $this->findDefaultImplementation($interface, $this->findAll($interface));
    }

    protected function getClasses(): array
    {
        if (!$this->classes) {
            $cacheKey = $this->makeCacheKey(self::KEY_CLASSES);
            $this->classes = $this->cache->get($cacheKey) ?? [];
            if (!$this->classes) {
                $this->classes = $this->fetchClasses();
                $this->cache->set($cacheKey, $this->classes);
            }
        }
        return $this->classes;
    }

    protected function fetchClasses(): array
    {
        $result = [];
        foreach ($this->namespaces as $namespace) {
            $result = array_merge($result, $this->classHelper->fetchClasses($namespace));
        }
        return $result;
    }

    protected function makeCacheKey(string $alias): string
    {
        return str_replace('\\', '.', get_class($this) . '.' . $alias);
    }

    protected function isAppExtension($appClass, $baseClass): bool
    {
        return $appClass === sprintf('App\\%s', $baseClass);
    }

    protected function findDefaultImplementation(string $interface, array $classes): ?string
    {
        $interface = $this->classHelper->makeShortClassName($interface);
        $pattern = '/Interface$/';
        if (preg_match($pattern, $interface)) {
            $class = preg_replace($pattern, '', $interface);
            if (in_array($class, $classes)) {
                return $class;
            }
        }

        return null;
    }
}
