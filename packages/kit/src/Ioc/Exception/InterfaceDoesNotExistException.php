<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Ioc\Exception;

class InterfaceDoesNotExistException extends \Exception
{
    public function __construct(string $interface)
    {
        parent::__construct(sprintf('Interface "%s" does not exist!'), $interface);
    }
}
