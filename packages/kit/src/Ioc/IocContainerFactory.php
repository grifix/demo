<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ioc;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\DefinitionMaker\DefinitionMakerFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\SimpleCache\CacheInterface;

class IocContainerFactory extends AbstractFactory implements IocContainerFactoryInterface
{

    protected DefinitionMakerFactoryInterface $definitionMakerFactory;

    protected ClassHelperInterface $classHelper;

    public function __construct(
        ClassMakerInterface $classMaker,
        DefinitionMakerFactoryInterface $definitionMakerFactory,
        ClassHelperInterface $classHelper
    ) {
        parent::__construct($classMaker);
        $this->definitionMakerFactory = $definitionMakerFactory;
        $this->classHelper = $classHelper;
    }

    public function createIocContainer(
        CacheInterface $cache,
        ConfigInterface $config,
        array $namespaces = [],
        array $definitions = []
    ): IocContainerInterface {
        $class = $this->makeClassName(IocContainer::class);
        return new $class(
            $definitions,
            $this->definitionMakerFactory->create($namespaces, $cache),
            $config,
            $this->classHelper
        );
    }
}
