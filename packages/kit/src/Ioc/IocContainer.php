<?php declare(strict_types=1);

namespace Grifix\Kit\Ioc;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\DefinitionMaker\DefinitionMakerInterface;
use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;
use Grifix\Kit\Ioc\Exception\CircularDependencyException;
use Grifix\Kit\Ioc\Exception\InterfaceDoesNotExistException;
use ReflectionException;

class IocContainer implements IocContainerInterface
{
    protected array $resolvers = [];
    protected DefinitionMakerInterface $definitionMaker;
    protected ConfigInterface $config;
    protected array $recursion = [];
    protected array $definitions = [];
    protected ClassHelperInterface $classHelper;

    public function __construct(
        array $resolvers,
        DefinitionMakerInterface $definitionMaker,
        ConfigInterface $config,
        ClassHelperInterface $classHelper
    ) {
        $resolvers[IocContainerInterface::class] = function () {
            return $this;
        };
        $this->definitionMaker = $definitionMaker;
        $this->config = $config;
        $this->classHelper = $classHelper;
        foreach ($resolvers as $alias => $resolver) {
            $this->set($alias, $resolver);
        }
    }

    public function get(string $alias)
    {
        if (!array_key_exists($alias, $this->resolvers)) {
            $this->resolvers[$alias] = $this->resolveDependency($alias);
        }

        $resolver = $this->resolvers[$alias];

        if ($resolver instanceof \Closure) {
            $this->resolvers[$alias] = $resolver->call($this);
        }

        if ($resolver instanceof DependencyDefinition) {
            $this->resolvers[$alias] = $this->createNewInstance($alias);
        }

        return $this->resolvers[$alias];
    }

    public function set(string $alias, $value): IocContainerInterface
    {
        $this->resolvers[$alias] = $value;
        if ($value instanceof DependencyDefinition) {
            $this->definitions[$alias] = $value;
        }

        return $this;
    }

    public function has(string $alias): bool
    {
        return isset($this->resolvers[$alias]);
    }

    /**
     * @return array|mixed|object
     * @throws ReflectionException
     */
    protected function resolveDependency(string $alias)
    {
        if ($this->recursion && $alias == $this->recursion[0]) {
            $this->recursion[] = $alias;
            throw new CircularDependencyException($this->recursion);
        }
        $this->recursion[] = $alias;

        if ($this->isConfig($alias)) {
            $this->recursion = [];
            return $this->config->get(str_replace('cfg:', '', $alias));
        }

        $result = $this->createNewInstance($alias);
        $this->recursion = [];
        return $result;
    }

    /**
     * @param string $alias
     * @return bool
     */
    protected function isConfig(string $alias): bool
    {
        return strpos($alias, 'cfg:') === 0;
    }

    public function getImplementationClass(string $interface): string
    {
        if (!interface_exists($interface)) {
            throw new InterfaceDoesNotExistException($interface);
        }
        return $this->getDefinition($interface)->getClassName();
    }

    protected function getDefinition(string $alias): DependencyDefinition
    {
        $alias = $this->classHelper->makeShortClassName($alias);
        if (isset($this->definitions[$alias])) {
            return $this->definitionMaker->complementDefinition($this->definitions[$alias]);
        }
        return $this->definitionMaker->makeDefinition($alias);
    }

    /**
     * {@inheritdoc}
     */
    public function createNewInstance(string $alias, array $args = [])
    {
        $definition = $this->getDefinition($alias);

        $reflectionClass = new \ReflectionClass($definition->getClassName());

        if (null !== $reflectionClass->getConstructor()) {
            foreach ($reflectionClass->getConstructor()->getParameters() as $i => $parameter) {
                if (isset($args[$parameter->getName()])) {
                    $args[$i] = $args[$parameter->getName()];
                    unset($args[$parameter->getName()]);
                }
                if ($parameter->getType() && isset($args[$parameter->getType()->getName()])) {
                    $args[$i] = $args[$parameter->getType()->getName()];
                    unset($args[$parameter->getType()->getName()]);
                }
            }
        }

        foreach ($definition->getArgs() as $i => $argAlias) {
            if (!array_key_exists($i, $args)) {
                $args[$i] = $this->get($argAlias);
            }
        }
        ksort($args);

        return $reflectionClass->newInstanceArgs($args);
    }
}
