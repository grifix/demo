<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Expression;

/**
 * Class TwoOperandsExpression
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class TwoOperandsExpression extends AbstractExpression
{
    /**
     * TwoOperandsExpression constructor.
     *
     * @param $leftOperand
     * @param $rightOperand
     */
    public function __construct($leftOperand, $rightOperand)
    {
        $this->operands[0] = $leftOperand;
        $this->operands[1] = $rightOperand;
    }

    /**
     * @return mixed
     */
    public function getLeftOperand()
    {
        return $this->operands[0];
    }

    /**
     * @return mixed
     */
    public function getRightOperand()
    {
        return $this->operands[1];
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setLeftOperand($value)
    {
        $this->operands[0] = $value;
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setRightOperand($value)
    {
        $this->operands[1] = $value;
        return $this;
    }
}
