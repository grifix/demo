<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Expression;


/**
 * Class ExpressionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ExpressionFactoryInterface
{
    /**
     * @param ExpressionInterface|mixed $leftOperand
     * @param ExpressionInterface|mixed $rightOperand
     *
     * @return ExpressionInterface
     */
    public function eq($leftOperand, $rightOperand): ExpressionInterface;
    
    /**
     * @param ExpressionInterface|mixed $leftOperand
     * @param ExpressionInterface|mixed $rightOperand
     *
     * @return ExpressionInterface
     */
    public function ne($leftOperand, $rightOperand): ExpressionInterface;

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @param mixed ...$operands
     * @return ExpressionInterface
     */
    public function andX($leftOperand, $rightOperand, ...$operands): ExpressionInterface;

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @param mixed ...$operands
     * @return ExpressionInterface
     */
    public function orX($leftOperand, $rightOperand, ...$operands): ExpressionInterface;
    
    /**
     * @param mixed $needle
     * @param array $haystack
     *
     * @return ExpressionInterface
     */
    public function in($needle, array $haystack): ExpressionInterface;
    
    /**
     * @param mixed $leftOperand
     * @param mixed $rightOperand
     *
     * @return ExpressionInterface
     */
    public function lt($leftOperand, $rightOperand): ExpressionInterface;
    
    /**
     * @param mixed $leftOperand
     * @param mixed $rightOperand
     *
     * @return ExpressionInterface
     */
    public function lte($leftOperand, $rightOperand): ExpressionInterface;
    
    /**
     * @param mixed $leftOperand
     * @param mixed $rightOperand
     *
     * @return ExpressionInterface
     */
    public function gte($leftOperand, $rightOperand): ExpressionInterface;
    
    /**
     * @param mixed $leftOperand
     * @param mixed $rightOperand
     *
     * @return ExpressionInterface
     */
    public function gt($leftOperand, $rightOperand): ExpressionInterface;
}
