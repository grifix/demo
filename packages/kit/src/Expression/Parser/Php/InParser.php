<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression\Parser\Php;

use Grifix\Kit\Expression\ExpressionInterface;

/**
 * Class InParser
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\Parser\Php
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InParser extends SimpleParser
{
    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    public function parse(ExpressionInterface $expression)
    {
        $operands = $this->prepareOperands($expression);
        
        return 'in_array('.$operands[0].','.$operands[1].')';
    }
}