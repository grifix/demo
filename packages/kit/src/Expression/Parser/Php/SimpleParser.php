<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Expression\Parser\Php;

use Grifix\Kit\Expression\AndExpression;
use Grifix\Kit\Expression\EqualExpression;
use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Expression\GreaterThanExpression;
use Grifix\Kit\Expression\GreaterThanOrEqualToExpression;
use Grifix\Kit\Expression\LessThanExpression;
use Grifix\Kit\Expression\LessThanOrEqualToExpression;
use Grifix\Kit\Expression\NotEqualExpression;
use Grifix\Kit\Expression\OrExpression;
use Grifix\Kit\Expression\Parser\AbstractParser;

/**
 * Class SimpleParser
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\Parser
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SimpleParser extends AbstractParser
{
    protected $operatorsMap = [
        EqualExpression::class => '==',
        NotEqualExpression::class => '!=',
        AndExpression::class => '&&',
        OrExpression::class => '||',
        GreaterThanOrEqualToExpression::class => '>=',
        LessThanOrEqualToExpression::class => '<=',
        GreaterThanExpression::class => '>',
        LessThanExpression::class => '<',
    ];

    /**
     * {@inheritdoc}
     */
    protected function prepareOperand($operand)
    {

        if (!($operand instanceof ExpressionInterface)) {
            return var_export(parent::prepareOperand($operand), true);
        }
        return parent::prepareOperand($operand);
    }
}