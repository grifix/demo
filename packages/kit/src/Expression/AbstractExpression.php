<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression;

use Grifix\Kit\Expression\Exception\OperandNotExistsException;


/**
 * Class AbstractExpression
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractExpression implements ExpressionInterface
{
    /**
     * @var array
     */
    protected $operands = [];
    
    
    /**
     * {@inheritdoc}
     */
    public function addOperand($operand): ExpressionInterface
    {
        $this->operands[] = $operand;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setOperand($index, $operand): ExpressionInterface
    {
        $this->operands[$index] = $operand;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getOperand($index)
    {
        if (!isset($this->operands[$index])) {
            throw new OperandNotExistsException($index);
        }
        
        return $this->operands[$index];
    }
    
    /**
     * {@inheritdoc}
     */
    public function setOperands(array $operands): ExpressionInterface
    {
        $this->operands = $operands;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getOperands()
    {
        return $this->operands;
    }
}