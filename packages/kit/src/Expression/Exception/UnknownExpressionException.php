<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression\Exception;

use Grifix\Kit\Expression\ExpressionInterface;

/**
 * Class UnknownExpressionException
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UnknownExpressionException extends \Exception
{
    /**
     * @var ExpressionInterface
     */
    protected $expression;
    
    /**
     * UnknownExpressionException constructor.
     *
     * @param ExpressionInterface $expression
     */
    public function __construct(ExpressionInterface $expression)
    {
        $this->expression = $expression;
        $this->message = 'Unknown expression "' . get_class($expression) . '"!';
        parent::__construct();
    }
}
