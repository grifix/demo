<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Expression\Builder;

use Grifix\Kit\Expression\Exception\UnknownExpressionException;
use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Expression\Parser\ParserInterface;


/**
 * Class Builder
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ExpressionBuilderInterface
{
    /**
     * @param ExpressionInterface $expression
     *
     * @return string
     * @throws UnknownExpressionException
     */
    public function buildExpression(ExpressionInterface $expression);
    
    /**
     * @param string          $expressionClass
     * @param ParserInterface $parser
     *
     * @return ExpressionBuilderInterface
     */
    public function setParser(string $expressionClass, ParserInterface $parser): ExpressionBuilderInterface;
}