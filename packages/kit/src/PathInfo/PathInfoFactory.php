<?php
declare(strict_types=1);

namespace Grifix\Kit\PathInfo;

use Grifix\Kit\Kernel\AbstractFactory;

class PathInfoFactory extends AbstractFactory implements PathInfoFactoryInterface
{
    public function createPathInfo(string $path): PathInfoInterface
    {
        $class = $this->makeClassName(PathInfo::class);
        return new $class($path);
    }
}
