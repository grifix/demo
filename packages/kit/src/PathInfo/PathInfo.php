<?php

declare(strict_types=1);

namespace Grifix\Kit\PathInfo;

use Grifix\Kit\PathInfo\Exception\DirectoryHasNoExtensionException;
use Grifix\Kit\PathInfo\Exception\DirectoryHasNoMimeTypeException;
use Grifix\Kit\PathInfo\Exception\FileIsNotImageException;
use Grifix\Kit\PathInfo\Exception\FileNotExistsException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class PathInfo implements PathInfoInterface
{

    protected string $path;

    protected array $info;

    protected ?string $mime = null;

    protected ?bool $isDir = null;

    protected ?int $size = null;

    protected ?bool $isImage = null;

    protected ?int $imageWidth = null;

    protected ?int $imageHeight = null;

    public function __construct(string $path)
    {
        if (!file_exists($path)) {
            throw new FileNotExistsException($path);
        }
        $this->path = $path;
        $this->info = pathinfo($this->path);
    }

    public function getMimeType(): string
    {
        if ($this->isDir) {
            throw new DirectoryHasNoMimeTypeException($this->path);
        }
        if (null === $this->mime) {
            $this->mime = mime_content_type($this->path);
        }
        return $this->mime;
    }

    public function getExtension(): string
    {
        if ($this->isDir()) {
            throw new DirectoryHasNoExtensionException($this->path);
        }
        return $this->info['extension'];
    }

    public function getDirName(): string
    {
        return $this->info['dirname'];
    }

    public function getBaseName(): string
    {
        return $this->info['basename'];
    }

    public function getFileName(): string
    {
        return $this->info['filename'];
    }

    public function isDir(): bool
    {
        if (null === $this->isDir) {
            $this->isDir = is_dir($this->path);
        }
        return $this->isDir;
    }

    public function getSize(): int
    {
        if (null === $this->size) {
            if ($this->isDir()) {
                $this->size = $this->getDirectorySize();
            } else {
                $this->size = filesize($this->path);
            }
        }
        return $this->size;
    }

    public function isImage(): bool
    {
        if (null === $this->isImage) {
            $this->isImage = in_array($this->getMimeType(), [
                image_type_to_mime_type(IMAGETYPE_GIF),
                image_type_to_mime_type(IMAGETYPE_JPEG),
                image_type_to_mime_type(IMAGETYPE_PNG),
            ]);
        }
        return $this->isImage;
    }

    public function getImageHeight(): int
    {
        if (null === $this->imageHeight) {
            $this->getImageSize();
        }
        return $this->imageHeight;
    }

    public function getImageWidth(): int
    {
        if (null === $this->imageWidth) {
            $this->getImageSize();
        }
        return $this->imageWidth;
    }

    private function getDirectorySize(): int
    {
        $size = 0;
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->path)) as $file) {
            $size += $file->getSize();
        }
        return $size;
    }

    private function getImageSize()
    {
        if (!$this->isImage()) {
            throw new FileIsNotImageException($this->path);
        }
        [$this->imageWidth, $this->imageHeight] = getimagesize($this->path);
    }
}
