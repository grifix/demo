<?php
declare(strict_types = 1);

namespace Grifix\Kit\PathInfo;

interface PathInfoFactoryInterface
{
    public function createPathInfo(string $path): PathInfoInterface;
}
