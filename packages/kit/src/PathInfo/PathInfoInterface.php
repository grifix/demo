<?php
declare(strict_types=1);

namespace Grifix\Kit\PathInfo;

interface PathInfoInterface
{
    public function getMimeType(): string;

    public function getExtension(): string;

    public function getDirName(): string;

    public function getBaseName(): string;

    public function getFileName(): string;

    public function isDir(): bool;

    public function getSize(): int;

    public function isImage(): bool;

    public function getImageHeight(): int;

    public function getImageWidth(): int;
}
