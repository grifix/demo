<?php
declare(strict_types=1);

namespace Grifix\Kit\PathInfo\Exception;

class FileNotExistsException extends \RuntimeException
{
    protected $path;

    /**
     * FileNotExistsException constructor.
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
        parent::__construct(sprintf('Path %s does not exists!', $path));
    }
}
