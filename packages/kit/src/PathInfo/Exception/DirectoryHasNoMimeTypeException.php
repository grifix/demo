<?php
declare(strict_types=1);

namespace Grifix\Kit\PathInfo\Exception;

class DirectoryHasNoMimeTypeException extends \RuntimeException
{
    /**
     * @var string
     */
    protected $path;

    /**
     * PathIsDirectoryException constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
        parent::__construct(sprintf('Directory %s has no mime type!', $path));
    }


}
