<?php declare(strict_types = 1);

namespace Grifix\Kit\PathInfo\Exception;

class FileIsNotImageException extends \RuntimeException
{
    public function __construct(string $path)
    {
        parent::__construct(sprintf('%s is not image!', $path));
    }
}
