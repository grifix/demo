<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\ClassFetcher\ClassFetcher;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Psr\SimpleCache\CacheInterface;

final class ClassFetcher implements ClassFetcherInterface
{
    protected CacheInterface $cache;

    protected ClassHelperInterface $classHelper;

    protected KernelInterface $kernel;

    public function __construct(CacheInterface $cache, ClassHelperInterface $classHelper, KernelInterface $kernel)
    {
        $this->cache = $cache;
        $this->classHelper = $classHelper;
        $this->kernel = $kernel;
    }

    public function fetchClasses(string $nameSpace, ?string $type = null, bool $onlyInstantiable = false): array
    {
        $cacheKey = 'grifix.kit.classFetcher.' . $nameSpace;
        if ($type) {
            $cacheKey .= '.' . $type;
        }
        if ($onlyInstantiable) {
            $cacheKey .= '.instantiable';
        }
        $result = $this->cache->get($cacheKey);
        if (null !== $result) {
            return $result;
        }
        $result = [];
        foreach ($this->kernel->getModules() as $module) {
            foreach ($this->classHelper->fetchClasses($module->getNamespace() . $nameSpace) as $class) {
                if ($type && false === $this->classHelper->isInstanceOf($class, $type)) {
                    continue;
                }
                if ($onlyInstantiable && false === $this->isInstantiabale($class)) {
                    continue;
                }
                $result[] = $class;
            }
        }
        $this->cache->set($cacheKey, $result);
        return $result;
    }

    protected function isInstantiabale(string $class): bool
    {
        return (new \ReflectionClass($class))->isInstantiable();
    }
}
