<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\ClassFetcher\ClassFetcher;

interface ClassFetcherInterface
{
    public function fetchClasses(string $nameSpace, string $type = null, bool $onlyInstantiable = false): array;
}
