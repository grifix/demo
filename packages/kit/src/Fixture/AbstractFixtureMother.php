<?php declare(strict_types=1);

namespace Grifix\Kit\Fixture;

use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Faker\FakerInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Validator\ValidatorInterface;
use Grifix\Kit\Uuid\UuidGeneratorInterface;
use Grifix\Shared\Application\Common\LangsRepository\LangDto;
use Grifix\Shared\Application\Query\GetLangs\GetLangsQuery;
use Grifix\Shared\Domain\File\FileDto;
use Grifix\Shared\Ui\Common\FileDtoFactory\FileDtoFactoryInterface;

abstract class AbstractFixtureMother
{
    /** @var IocContainerInterface */
    protected $iocContainer;

    /** @var ConnectionInterface */
    protected $connection;

    /** @var UuidGeneratorInterface */
    protected $uuidGenerator;

    /** @var LangDto[] */
    protected $langs;

    /** @var FakerInterface */
    protected $faker;

    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    /** @var ValidatorInterface */
    protected $validator;

    /** @var EntityManagerInterface */
    protected $entityManager;

    public function __construct(IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
        $this->connection = $this->iocContainer->get(ConnectionInterface::class);
        $this->uuidGenerator = $this->iocContainer->get(UuidGeneratorInterface::class);
        $this->faker = $this->iocContainer->get(FakerInterface::class);
        $this->arrayHelper = $this->iocContainer->get(ArrayHelperInterface::class);
        $this->validator = $this->iocContainer->get(ValidatorInterface::class);
        $this->entityManager = $this->iocContainer->get(EntityManagerInterface::class);
    }

    protected function executeCommand(object $command): void
    {
        $this->iocContainer->get(CommandBusInterface::class)->execute($command);
    }

    protected function generateId(): string
    {
        return $this->uuidGenerator->generateUuid4();
    }

    /**
     * @return LangDto[]
     */
    protected function getLangs(): array
    {
        if (!$this->langs) {
            $this->langs = $this->iocContainer->get(QueryBusInterface::class)->execute(new GetLangsQuery());
        }
        return $this->langs;
    }

    protected function toArray(object $object): array
    {
        return $this->arrayHelper->toArray($object);
    }

    protected function insertData(string $entityClass, array $data, array $extra = [])
    {
        $table = $this->entityManager->getBlueprint($entityClass)->getTable();
        $json = json_encode($data);
        $this->validator->validate($entityClass, $json);
        $record = array_merge(
            [
                'id' => $data['id']['value'],
                'data' => $json
            ],
            $extra
        );
        $this->connection->insert($record, $table);
    }

    protected function updateData(string $entityClass, array $data, array $extra = [])
    {
        $table = $this->entityManager->getBlueprint($entityClass)->getTable();
        $json = json_encode($data);
        $this->validator->validate($entityClass, $json);
        $record = array_merge(
            [
                'data' => $json
            ],
            $extra
        );
        $this->connection->update($record, $table, $data['id']['value']);
    }

    protected function createMultiLangText(): array
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeText();
        }
        return $result;
    }

    protected function createMultiLangWord(): array
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeWord();
        }
        return $result;
    }

    protected function createMultiLangString(string $var): array
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $var;
        }
        return $result;
    }

    protected function createFakeWord(): string
    {
        return $this->faker->setUnique(true)->fakeWord();
    }

    protected function createFileDto(string $path): FileDto
    {
        return new FileDto(
            $path,
            basename($path)
        );
    }
}
