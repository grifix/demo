<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Test;

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use InvalidArgumentException;

abstract class Asserter
{
    use ArraySubsetAsserts;

    public static function assertArrayHasSameKeys(array $keys, array $array): void
    {
        if ($keys != array_keys($array)) {
            $extraKeys = array_diff(array_keys($array), $keys);
            $missingKeys = array_diff($keys, array_keys($array));
            if ($extraKeys) {
                throw new InvalidArgumentException(sprintf('There is following extra keys in array: %s',
                    implode(', ', $extraKeys)));
            }
            if ($missingKeys) {
                throw new InvalidArgumentException(sprintf('There is following missing keys in array: %s',
                    implode(', ', $missingKeys)));
            }
        }
    }
}
