<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Cqrs;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class AbstractHandlerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Cqrs
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractHandlerFactory extends AbstractFactory
{
    
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;
    
    /**
     * AbstractHandlerFactory constructor.
     *
     * @param IocContainerInterface $iocContainer
     * @param ClassMakerInterface   $classMaker
     */
    public function __construct(IocContainerInterface $iocContainer, ClassMakerInterface $classMaker)
    {
        $this->iocContainer = $iocContainer;
        parent::__construct($classMaker);
    }
}
