<?php
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command\Queue;

class CommandMessage
{
    protected object $command;

    public function __construct(object $command)
    {
        $this->command = $command;
    }

    public function getCommand(): object
    {
        return $this->command;
    }
}
