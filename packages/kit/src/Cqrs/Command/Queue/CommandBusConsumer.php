<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command\Queue;

use Grifix\Kit\Alias;
use Grifix\Kit\Ioc\IocContainerInterface;
use Psr\Log\LoggerInterface;

class CommandBusConsumer
{
    protected IocContainerInterface $iocContainer;

    protected LoggerInterface $logger;


    public function __construct(IocContainerInterface $iocContainer, LoggerInterface $logger)
    {
        $this->iocContainer = $iocContainer;
        $this->logger = $logger;
    }

    public function __invoke(CommandMessage $message)
    {
        $this->iocContainer->get(Alias::SYNC_COMMAND_BUS)->execute($message->getCommand());
    }
}
