<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command\CommandBus;

use Grifix\Kit\Cqrs\Command\CommandHandler\CommandHandlerFactory;
use Grifix\Kit\Cqrs\Command\CommandHandler\CommandHandlerFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class CommandBus
 *
 * @category Grifix
 * @package  Grifix\Kit\Cqrs
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CommandBus implements CommandBusInterface
{
    /**
     * @var ClassMakerInterface
     */
    protected $classMaker;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    public function __construct(
        ClassMakerInterface $classMaker,
        IocContainerInterface $iocContainer
    ) {
        $this->classMaker = $classMaker;
        $this->iocContainer = $iocContainer;
    }

    /**
     * @param object $command
     *
     * @return void
     */
    public function execute($command): void
    {
        $handler = $this->resolveCommandHandlerFactory($command)->createCommandHandler($command);
        $handler($command);
    }

    /**
     * @param object $command
     * @return string
     */
    protected function resolveHandlerFactoryClass(object $command): string
    {
        $result = $this->classMaker->makeClassName(get_class($command) . 'HandlerFactory');
        if (!class_exists($result)) {
            $result = $this->classMaker->makeClassName(CommandHandlerFactory::class);
        }
        return $result;
    }


    /**
     * @param object $command
     *
     * @return CommandHandlerFactoryInterface
     */
    protected function resolveCommandHandlerFactory($command)
    {
        $class = $this->resolveHandlerFactoryClass($command);
        return new $class($this->iocContainer, $this->classMaker);
    }
}
