<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command\CommandBus;

use Grifix\Kit\Cqrs\Command\Queue\CommandMessage;
use Grifix\Kit\MessageBroker\MessageBrokerInterface;

class AsyncCommandBus implements CommandBusInterface
{
    public const EXCHANGE_NAME = 'commands';

    protected MessageBrokerInterface $messageBroker;

    protected array $asyncCommands = [];

    protected CommandBusInterface $commandBus;

    /**
     * @param array $asyncCommands <cfg:grifix.kit.cqrs.asyncCommands>
     */
    public function __construct(
        CommandBusInterface $commandBus,
        MessageBrokerInterface $messageBroker,
        array $asyncCommands = []
    ) {
        $this->messageBroker = $messageBroker;
        $this->asyncCommands = $asyncCommands;
        $this->commandBus = $commandBus;
    }

    public function execute(object $command): void
    {
        if ($this->isAsyncCommand($command)) {
            $this->messageBroker->sendMessage(
                new CommandMessage($command),
                self::EXCHANGE_NAME
            );
        } else {
            $this->commandBus->execute($command);
        }
    }

    /**
     * @param object $command
     * @return bool
     */
    protected function isAsyncCommand(object $command): bool
    {
        return in_array(get_class($command), $this->asyncCommands);
    }
}
