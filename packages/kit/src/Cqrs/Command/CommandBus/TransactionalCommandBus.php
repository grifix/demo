<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Cqrs\Command\CommandBus;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\EntityManager\Exception\OptimisticLockFailedException;
use Grifix\Shared\Domain\Exception\TransactionInterruptedException;

class TransactionalCommandBus implements CommandBusInterface
{

    protected CommandBusInterface $commandBus;

    protected EntityManagerInterface $entityManager;

    protected array $nonTransactionalCommands = [];

    /**
     * @param array $nonTransactionalCommands <cfg:grifix.kit.cqrs.nonTransactionalCommands>
     */
    public function __construct(
        CommandBusInterface $commandBus,
        EntityManagerInterface $entityManager,
        array $nonTransactionalCommands
    ) {
        $this->commandBus = $commandBus;
        $this->entityManager = $entityManager;
        $this->nonTransactionalCommands = $nonTransactionalCommands;
    }

    public function execute($command): void
    {
        if ($this->isTransactional($command)) {
            $this->executeInTransaction($command);
            return;
        }
        $this->commandBus->execute($command);
    }

    protected function executeInTransaction(object $command): void
    {
        $transactionInterrupted = false;
        $this->entityManager->beginTransaction();
        try {
            $this->commandBus->execute($command);
        } catch (TransactionInterruptedException $exception) {
            $this->entityManager->rollbackTransaction();
            $transactionInterrupted = true;
            throw $exception;
        } catch (OptimisticLockFailedException $exception) {
            $transactionInterrupted = true;
            $this->executeInTransaction($command);
        } finally {
            if (false === $transactionInterrupted) {
                $this->entityManager->commit();
            }
            else{
                $this->entityManager->rollbackTransaction();
            }
        }
    }

    public function isTransactional(object $command): bool
    {
        return false === in_array(get_class($command), $this->nonTransactionalCommands);
    }
}
