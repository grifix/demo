<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Http;

use DateTimeImmutable;
use DateTimeInterface;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class Response
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Response implements ResponseInterface
{

    use MessageTrait;

    /**
     * @var ResponseInterface
     */
    protected $psr;

    protected StreamFactoryInterface $streamFactory;

    /**
     * Response constructor.
     *
     * @param PsrResponseInterface $psrResponse
     * @param StreamFactoryInterface $streamFactory
     */
    public function __construct(PsrResponseInterface $psrResponse, StreamFactoryInterface $streamFactory)
    {
        $this->psr = $psrResponse;

        $this->streamFactory = $streamFactory;
    }

    public function getStatusCode()
    {
        return $this->psr->getStatusCode();
    }

    public function withStatus($code, $reasonPhrase = '')
    {
        $new = clone($this);
        $new->psr = $this->psr->withStatus($code, $reasonPhrase);

        return $new;
    }

    public function withContent(string $content): ResponseInterface
    {
        $new = clone($this);
        $new->psr = $this->psr->withBody($this->streamFactory->createFormString($content));

        return $new;
    }

    public function getReasonPhrase()
    {
        return $this->psr->getReasonPhrase();
    }

    public function withCookie(
        string $name,
        string $value,
        string $path = '/',
        DateTimeInterface $expiresAt = null
    ): ResponseInterface {
        if (!$expiresAt) {
            $expiresAt = new DateTimeImmutable('+365 days');
        }
        $new = clone($this);
        $new->psr = $this->psr->withAddedHeader('Set-Cookie', $name . '=' . $value);

        $new->psr = $this->psr->withAddedHeader(
            'Set-Cookie',
            sprintf('%s=%s; path=%s; expires=%s', $name, $value, $path, $expiresAt->format('r'))
        );

        return $new;
    }

    public function withRedirect(string $url): ResponseInterface
    {
        return $this->withHeader('location', $url)->withStatus(301, 'Moved Permanently');
    }
}
