<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Http;

/**
 * Class Cookie
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface CookieInterface
{
    
    /**
     * @param $name
     * @param $value
     *
     * @return CookieInterface
     */
    public function set($name, $value): CookieInterface;
    
    /**
     * @param      $name
     * @param null $defaultValue
     *
     * @return mixed
     */
    public function get($name, $defaultValue = null);
    
    /**
     * @internal
     *
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function processResponse(ResponseInterface $response): ResponseInterface;
}