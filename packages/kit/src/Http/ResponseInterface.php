<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Http;

use DateTimeInterface;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

interface ResponseInterface extends PsrResponseInterface
{

    public function withContent(string $content): ResponseInterface;


    public function withCookie(
        string $name,
        string $value,
        string $path = '/',
        DateTimeInterface $expiresAt = null
    ): ResponseInterface;

    public function withRedirect(string $url):ResponseInterface;
}
