<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

interface ServerRequestFactoryInterface
{
    /**
     * @param string|null|resource|StreamInterface $body Request body
     * @param UriInterface|string $uri request uri
     */
    public function create(
        string $method,
        $uri,
        array $headers = [],
        $body = null,
        ?string $version = null
    ): ServerRequestInterface;

    public function createFromGlobals(): ServerRequestInterface;
}
