<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

class Cookie implements CookieInterface
{
    protected ServerRequestInterface $request;
    
    protected array $data = [];

    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    public function set($name, $value): CookieInterface
    {
        $this->data[$name] = $value;
        
        return $this;
    }

    public function get($name, $defaultValue = null)
    {
        return $this->request->getCookieParam($name, $defaultValue);
    }

    public function processResponse(ResponseInterface $response): ResponseInterface
    {
        if ($this->data) {
            foreach ($this->data as $key => $val) {
                $response = $response->withCookie($key, $val);
            }
        }
        
        return $response;
    }
}
