<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Psr\Http\Message\StreamInterface;

/**
 * Class MessageTrait
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
trait MessageTrait
{
    /**
     * @var \GuzzleHttp\Psr7\MessageTrait
     */
    protected $psr;
    
    /**
     * {@inheritdoc}
     */
    public function getProtocolVersion()
    {
        return $this->psr->getProtocolVersion();
    }
    
    /**
     * {@inheritdoc}
     */
    public function withProtocolVersion($version)
    {
        $new = clone($this);
        $new->psr = $this->psr->withProtocolVersion($version);
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHeaders()
    {
        return $this->psr->getHeaders();
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasHeader($name)
    {
        return $this->psr->hasHeader($name);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHeader($name)
    {
        return $this->psr->getHeader($name);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHeaderLine($name)
    {
        return $this->psr->getHeaderLine($name);
    }
    
    /**
     * {@inheritdoc}
     */
    public function withHeader($name, $value)
    {
        $new = clone($this);
        $new->psr = $this->psr->withHeader($name, $value);
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withAddedHeader($name, $value)
    {
        $new = clone($this);
        $new->psr = $this->psr->withAddedHeader($name, $value);
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withoutHeader($name)
    {
        $new = clone($this);
        $new->psr = $this->psr->withoutHeader($name);
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBody()
    {
        return $this->psr->getBody();
    }
    
    /**
     * {@inheritdoc}
     */
    public function withBody(StreamInterface $body)
    {
        $new = clone($this);
        $new->psr = $this->psr->withBody($body);
        return $new;
    }
}
