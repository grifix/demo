<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Http;

use Grifix\Kit\Http\Exception\HeadersSentException;
use Psr\Http\Message\ResponseInterface;


/**
 * Class ResponseSender
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ResponseSenderInterface
{
    /**
     * @param ResponseInterface $response
     *
     * @return void
     * @throws HeadersSentException
     */
    public function send(ResponseInterface $response);
}
