<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http\Exception;

use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * Class HeadersSentException
 *
 * @category Grifix
 * @package  Grifix\Kit\Http\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class HeadersSentException extends \Exception
{
    
    protected $response;
    
    /**
     * HeadersSentException constructor.
     *
     * @param ResponseInterface $response
     * @param Exception|null    $previous
     */
    public function __construct(ResponseInterface $response, Exception $previous = null)
    {
        $this->response = $response;
        $this->message = 'Can\'t send response headers already sent!';
        parent::__construct($previous);
    }
}
