<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Stopwatch;

class Stopwatch implements StopwatchInterface
{
    /** @var float|null */
    protected $startTime;

    public function start(): void
    {
        if (null !== $this->startTime) {
            throw new \Exception('Stopwatch already started!');
        }
        $this->startTime = $this->getCurrentTime();
    }

    public function stop(): float
    {
        if (null === $this->startTime) {
            throw new \Exception('Stopwatch is not started!');
        }
        $result = $this->getCurrentTime() - $this->startTime;
        $this->startTime = null;
        return $result;
    }

    protected function getCurrentTime(): float
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}
