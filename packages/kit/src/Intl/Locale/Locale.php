<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Intl\Locale;

use Grifix\Kit\Intl\Lang\LangInterface;

class Locale implements LocaleInterface
{

    protected string $code;

    protected LangInterface $lang;

    public function __construct(string $code, LangInterface $lang)
    {
        $this->code = $code;
        $this->lang = $lang;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLang(): LangInterface
    {
        return $this->lang;
    }

    public function idEqualTo(LocaleInterface $locale): bool
    {
        return $locale->getCode() === $this->getCode();
    }
}
