<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Intl\Locale;

use Grifix\Kit\Intl\Lang\LangInterface;

interface LocaleInterface
{
    /**
     * @return string
     */
    public function getCode(): string;
    
    /**
     * @return LangInterface
     */
    public function getLang(): LangInterface;

    public function idEqualTo(LocaleInterface $locale): bool;
}
