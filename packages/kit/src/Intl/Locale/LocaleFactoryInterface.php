<?php

declare(strict_types = 1);

namespace Grifix\Kit\Intl\Locale;

interface LocaleFactoryInterface
{
    public function create($code): LocaleInterface;
}
