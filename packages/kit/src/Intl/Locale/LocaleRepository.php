<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Intl\Locale;

use Grifix\Kit\Config\ConfigInterface;

class LocaleRepository implements LocaleRepositoryInterface
{
    protected ConfigInterface $config;

    protected LocaleFactoryInterface $localeFactory;

    /** @var LocaleInterface[] */
    protected array $locales = [];

    public function __construct(ConfigInterface $config, LocaleFactoryInterface $localeFactory)
    {
        $this->config = $config;
        $this->localeFactory = $localeFactory;
    }

    public function getAll(): array
    {
        if ($this->locales) {
            return $this->locales;
        }
        foreach ($this->config->get('grifix.kit.intl.config.enabledLocales') as $localeCode) {
            $this->locales[] = $this->localeFactory->create($localeCode);
        }
        return $this->locales;
    }

    public function find(string $code): ?LocaleInterface
    {
        foreach ($this->getAll() as $locale) {
            if ($locale->getCode() === $code) {
                return $locale;
            }
        }
        return null;
    }

    public function getDefaultLocale(): LocaleInterface
    {
        return $this->getAll()[0];
    }
}
