<?php

declare(strict_types = 1);
namespace Grifix\Kit\Intl;

interface TranslatorFactoryInterface
{
    public function create(string $currentLangCode = null): TranslatorInterface;

}
