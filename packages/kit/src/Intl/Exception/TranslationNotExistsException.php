<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Exception;

use Grifix\Kit\Intl\Lang\LangInterface;

class TranslationNotExistsException extends \Exception
{
    /**
     * @var LangInterface
     */
    protected $lang;
    
    /**
     * @var string
     */
    protected $key;
    
    /**
     * TranslationNotExistsException constructor.
     *
     * @param LangInterface $lang
     * @param               $key
     */
    public function __construct(LangInterface $lang, $key)
    {
        $this->lang = $lang;
        $this->key = $key;
        $this->message = 'Translation "'.$key.'" not exists for language "'.$lang->getCode().'"';
        parent::__construct();
    }
}
