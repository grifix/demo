<?php

declare(strict_types = 1);

namespace Grifix\Kit\Intl\Lang;

class PlLang extends AbstractLang
{
    
    protected $code = 'pl';
    
    protected $name = 'Русский';
    
    /** @noinspection PhpMissingParentCallCommonInspection */
    
    /**
     * {@inheritdoc}
     */
    protected function makeQuantitative(int $quantity, string $case = null): array
    {
        if($case){
            if($quantity % 10 == 1 && $quantity % 100 != 11){
                return [self::FORM_SINGULAR, $case];
            }
            else{
                return [self::FORM_PLURAL, $case];
            }
        }
        if ($quantity % 10 == 1 && $quantity % 100 != 11) {
            return [self::FORM_SINGULAR, self::CASE_NOMINATIVE];
        }
        if ($quantity % 10 >= 2 && $quantity % 10 <= 4 && ($quantity % 10 < 10 || $quantity % 100 >= 20)) {
            return [self::FORM_SINGULAR, self::CASE_GENITIVE];
        }
        
        return [self::FORM_PLURAL, self::CASE_GENITIVE];
    }
}
