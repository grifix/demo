<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Lang;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Kernel\KernelInterface;

/**
 * Class DictionaryCompiler
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DictionaryCompiler implements DictionaryCompilerInterface
{
    protected $kernel;
    
    protected $config;
    
    protected $savePath;
    
    protected $filesystem;
    
    /**
     * DictionaryCompiler constructor.
     *
     * @param KernelInterface     $kernel
     * @param ConfigInterface     $config
     * @param FilesystemInterface $filesystem
     * @param string              $savePath <cfg:grifix.kit.intl.config.compiledDir>
     */
    public function __construct(
        KernelInterface $kernel,
        ConfigInterface $config,
        FilesystemInterface $filesystem,
        string $savePath
    ) {
        $this->kernel = $kernel;
        $this->config = $config;
        $this->savePath = $savePath;
        $this->filesystem = $filesystem;
    }
    
    /**
     * {@inheritdoc}
     */
    public function compile(string $langCode, bool $save = false): array
    {
        $result = [];
        foreach ($this->kernel->getModules() as $module) {
            $moduleDictionary = $this->config->get($module->getVendor().'.'.$module->getName() . '.intl.lang.' . $langCode);
            if ($moduleDictionary) {
                foreach ($moduleDictionary as $key => $val) {
                    $result[$module->getVendor()][$module->getName()][$key] = $val;
                }
            }
        }
        if ($save) {
            $this->filesystem->overwrite($this->savePath.'/'.$langCode.'.php', "<?php \n return ".var_export($result, true).";\n");
        }
        
        return $result;
    }
}