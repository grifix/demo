<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Intl;

use Grifix\Kit\Intl\Lang\LangInterface;
use Intl\Exception\LangNotExistsException;


/**
 * Class Translator
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface TranslatorInterface
{
    /**
     * @param string $langCode
     *
     * @return TranslatorInterface
     */
    public function setCurrentLang(string $langCode): TranslatorInterface;
    
    /**
     * @return LangInterface
     */
    public function getCurrentLang();
    
    /**
     * @param $code
     *
     * @return LangInterface
     * @throws \Grifix\Kit\Intl\Exception\LangNotExistsException
     */
    public function getLang($code): LangInterface;
    
    /**
     * @param string      $key
     * @param array       $vars
     * @param int         $quantity
     * @param string      $case
     * @param int         $form
     * @param string|null $langCode
     *
     * @return string
     */
    public function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR,
        string $langCode = null
    ): string;



    public function hasLang($code): bool;

    /**
     * @return LangInterface[]
     */
    public function getLangs(): array;

    public function translateToLang(
        string $key,
        string $langCode,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR
    ): string;

    public function getTranslationsVars(string $key, ?string $langCode = null): array;
}
