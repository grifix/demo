<?php
declare(strict_types=1);

namespace Grifix\Kit\Cache\Adapter;

/**
 * Class AdapterFactory
 * @package Grifix\Kit\Cache\Adapter
 */
interface AdapterFactoryInterface
{
    /**
     * @param array $servers
     * @return AdapterInterface
     */
    public function createMemcacheAdapter(array $servers): AdapterInterface;

    /**
     * @param string $path
     * @return AdapterInterface
     */
    public function createFileSystemAdapter(string $path): AdapterInterface;
}