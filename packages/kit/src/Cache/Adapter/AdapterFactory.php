<?php
declare(strict_types=1);

namespace Grifix\Kit\Cache\Adapter;

use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Serializer\SerializerInterface;

/**
 * Class AdapterFactory
 * @package Grifix\Kit\Cache\Adapter
 */
class AdapterFactory extends AbstractFactory implements AdapterFactoryInterface
{


    /**
     * @var FilesystemInterface
     */
    protected $filesystem;

    /**
     * AdapterFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param SerializerInterface $serializer
     * @param FilesystemInterface $filesystem
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        FilesystemInterface $filesystem
    ) {
        parent::__construct($classMaker);
        $this->filesystem = $filesystem;
    }

    /**
     * @param array $servers
     * @return AdapterInterface
     */
    public function createMemcacheAdapter(array $servers): AdapterInterface
    {
        $class = $this->makeClassName(MemCacheAdapter::class);
        $memcache = new \Memcache();
        foreach ($servers as $server) {
            $memcache->addServer(
                $server->getHost(),
                $server->getPort() ?? 11211,
                $server->getPersistent() ?? true,
                $server->getWeight() ?? 1,
                $server->getTimeout() ?? 1,
                $server->getRetryInterval() ?? 15,
                $server->getStatus() ?? true,
                $server->getFailureCallBack()
            );
        }
        return new $class($memcache);
    }

    /**
     * @param string $path
     * @return AdapterInterface
     */
    public function createFileSystemAdapter(string $path): AdapterInterface
    {
        $class = $this->makeClassName(FilesystemAdapter::class);
        return new $class($path, $this->filesystem);
    }
}
