<?php
declare(strict_types=1);

namespace Grifix\Kit\Cache\Adapter;

/**
 * Class MemCacheServer
 * @package Grifix\Kit\Cache\Adapter
 */
class MemCacheServerDto
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var ?null
     */
    protected $port;

    /**
     * @var ?bool
     */
    protected $persistent;

    /**
     * @var ?int
     */
    protected $weight;

    /**
     * @var ?int
     */
    protected $timeout;

    /**
     * @var ?int
     */
    protected $retryInterval;

    /**
     * @var ?int
     */
    protected $status;

    /**
     * @var ?callable
     */
    protected $failureCallBack;

    /**
     * @var ?int
     */
    protected $timeouts;

    /**
     * MemCacheServerDto constructor.
     * @param string $host
     * @param int|null $port
     * @param bool|null $persistent
     * @param int|null $weight
     * @param int|null $timeout
     * @param int|null $retryInterval
     * @param int|null $status
     * @param callable|null $failureCallBack
     * @param int|null $timeouts
     */
    public function __construct(
        string $host,
        ?int $port = null,
        ?bool $persistent = null,
        ?int $weight = null,
        ?int $timeout = null,
        ?int $retryInterval = null,
        ?int $status = null,
        ?callable $failureCallBack = null,
        ?int $timeouts = null
    ) {
        $this->host = $host;
        $this->port = $port;
        $this->persistent = $persistent;
        $this->weight = $weight;
        $this->timeout = $timeout;
        $this->retryInterval = $retryInterval;
        $this->status = $status;
        $this->failureCallBack = $failureCallBack;
        $this->timeouts = $timeouts;
    }


    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @return bool|null
     */
    public function getPersistent(): ?bool
    {
        return $this->persistent;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @return int|null
     */
    public function getTimeout(): ?int
    {
        return $this->timeout;
    }

    /**
     * @return int|null
     */
    public function getRetryInterval(): ?int
    {
        return $this->retryInterval;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @return callable|null
     */
    public function getFailureCallBack(): ?callable
    {
        return $this->failureCallBack;
    }

    /**
     * @return int|null
     */
    public function getTimeouts(): ?int
    {
        return $this->timeouts;
    }


}
