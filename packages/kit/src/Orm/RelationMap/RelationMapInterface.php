<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\RelationMap;

use Orm2\RelationManager\Exception\EntityHasNoParentException;

/**
 * Class RelationMap
 * @package Orm\RelationMap
 */
interface RelationMapInterface
{
    /**
     * @param object $childEntity
     * @param object $parentEntity
     */
    public function setParent($childEntity, $parentEntity): void;

    /**
     * @param $childEntity
     * @return mixed
     * @throws EntityHasNoParentException
     * @return object
     */
    public function getParent($childEntity);

    /**
     * @param object $child
     * @return bool
     */
    public function hasParent($child): bool;

    /**
     * @param object $childEntity
     */
    public function unsetParent($childEntity): void;

    /**
     * @param $child
     * @return string
     * @throws EntityHasNoParentException
     */
    public function getParentId($child): string;

    /**
     * @param $parentEntity
     * @return bool
     */
    public function hasChildren($parentEntity): bool;
}