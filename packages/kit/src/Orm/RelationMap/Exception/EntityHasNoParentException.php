<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\RelationMap\Exception;

/**
 * Class EntityHasNoParent
 * @package Orm\RelationMap\Exception
 */
class EntityHasNoParentException extends \Exception
{
    protected $entity;

    /**
     * EntityHasNoParentException constructor.
     * @param object $entity
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
        parent::__construct('Entity has no parent!');
    }
}
