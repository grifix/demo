<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Injector;

use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Orm\Collection\ArrayCollection;
use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Orm\Collection\QueryCollection;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Injects query collections into entities
 *
 * Class CollectionInjector
 * @package Grifix\Kit\Orm\Collection
 */
class CollectionInjector implements CollectionInjectorInterface
{

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * CollectionInjector constructor.
     * @param EntityManagerInterface $entityManager
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(EntityManagerInterface $entityManager, CollectionFactoryInterface $collectionFactory)
    {
        $this->entityManager = $entityManager;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param $entity
     * @throws \ReflectionException
     */
    public function injectQueryCollections($entity)
    {
        foreach ($this->entityManager->getBlueprint(get_class($entity))->getRelationsToChildren() as $relationToChild) {
            $reflection = $this->getReflection($entity, $relationToChild->getParentProperty());
            $property = $reflection->getProperty();
            $childEntity = $reflection->getEntity();
            $property->setAccessible(true);

            $value = $property->getValue($reflection->getEntity());
            $elements = [];
            if ($value instanceof QueryCollection) {
                continue;
            }
            if ($value instanceof GenericCollectionInterface) {
                $elements = $value->toArray();
            }
            $property->setValue(
                $childEntity,
                $this->collectionFactory->createQueryCollectionWrapper(
                    $this->entityManager,
                    $relationToChild->getCollectionClass(),
                    $relationToChild->getChildClass(),
                    $elements,
                    $entity
                )
            );
        }
    }

    /**
     * @param $entity
     * @param string $propertyName
     * @return Reflection
     * @throws \ReflectionException
     */
    protected function getReflection($entity, string $propertyName): Reflection
    {
        $propertyNameComponents = explode('.', $propertyName);
        $reflectionClass = new \ReflectionClass($entity);
        if (count($propertyNameComponents) == 1) {
            return new Reflection($entity, $reflectionClass, $reflectionClass->getProperty($propertyName));
        }
        $childPropertyName = array_shift($propertyNameComponents);
        $childProperty = new \ReflectionProperty($entity, $childPropertyName);
        $childProperty->setAccessible(true);
        $childEntity = $childProperty->getValue($entity);
        $childProperty->setAccessible(false);
        return $this->getReflection($childEntity, implode('.', $propertyNameComponents));
    }
}
