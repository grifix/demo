<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Injector;

/**
 * Class Extraction
 * @package Grifix\Kit\Orm\Collection\Injector
 */
class Reflection
{

    /**
     * @var object
     */
    protected $entity;

    /**
     * @var \ReflectionProperty
     */
    protected $property;

    /**
     * @var \ReflectionClass
     */
    protected $class;

    /**
     * Extraction constructor.
     * @param $entity
     * @param \ReflectionClass $class
     * @param \ReflectionProperty $property
     */
    public function __construct($entity, \ReflectionClass $class, \ReflectionProperty $property)
    {
        $this->entity = $entity;
        $this->property = $property;
        $this->class = $class;
    }

    /**
     * @return object
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return \ReflectionProperty
     */
    public function getProperty(): \ReflectionProperty
    {
        return $this->property;
    }

    /**
     * @return \ReflectionClass
     */
    public function getClass(): \ReflectionClass
    {
        return $this->class;
    }
}
