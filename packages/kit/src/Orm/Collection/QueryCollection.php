<?php
declare(strict_types = 1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Collection\ArrayWrapperTrait;
use Grifix\Kit\Collection\Finder\CollectionFinderInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Orm\Collection\Exception\EntityNotExistsException;
use Grifix\Kit\Orm\Collection\Exception\InvalidEntityClassException;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObjectFactoryInterface;
use Grifix\Kit\Specification\Injector\SpecificationInjectorFactoryInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class Orm
 * @package Grifix\Kit\Orm
 */
class QueryCollection implements CollectionInterface
{
    use ArrayWrapperTrait;

    /**
     * @var array
     */
    protected $entities = [];

    /**
     * @var QueryInterface
     */
    protected $query;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var SpecificationInjectorFactoryInterface
     */
    protected $specificationInjectorFactory;

    /**
     * @var CollectionFinderInterface
     */
    protected $collectionFinder;

    /** @var ReflectionObjectFactoryInterface */
    protected $reflectionWrapperFactory;

    public function __construct(
        string $entityClass,
        QueryInterface $query,
        EntityManagerInterface $entityManager,
        ArrayHelperInterface $arrayHelper,
        CollectionFactoryInterface $collectionFactory,
        SpecificationInjectorFactoryInterface $specificationInjectorFactory,
        CollectionFinderInterface $collectionFinder,
        ReflectionObjectFactoryInterface $reflectionWrapperFactory,
        array $entities = []
    ) {
        $this->entityClass = $entityClass;
        $this->query = $query;
        $this->entityManager = $entityManager;
        $this->arrayHelper = $arrayHelper;
        $this->collectionFactory = $collectionFactory;
        $this->specificationInjectorFactory = $specificationInjectorFactory;
        $this->collectionFinder = $collectionFinder;
        $this->reflectionWrapperFactory = $reflectionWrapperFactory;
        foreach ($entities as $entity) {
            $this->add($entity);
        }
    }

    protected function makePropertySql(string $property): string
    {
        $result = 'data';
        $propertyArray = explode('.', $property);
        $finalProperty = array_pop($propertyArray);
        if (count($propertyArray)) {
            $result .= array_reduce($propertyArray, function ($carry, $item) {
                return sprintf("%s->'%s'", $carry, $item);
            });
        }
        $result .= sprintf("->>'%s'", $finalProperty);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function findBy(string $property, $value): CollectionInterface
    {

        $query = clone $this->query;
        $query->where(sprintf('%s = :value', $this->makePropertySql($property)))
            ->bindValue('value', $value);
        return new self(
            $this->entityClass,
            $query,
            $this->entityManager,
            $this->arrayHelper,
            $this->collectionFactory,
            $this->specificationInjectorFactory,
            $this->collectionFinder,
            $this->reflectionWrapperFactory
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function setCollectionArray(
        array $elements
    ) {
        $this->entities = $elements;
    }

    /**
     * {@inheritdoc}
     */
    protected function getCollectionArray(): array
    {
        $this->load();
        return $this->entities;
    }


    protected function load(): void
    {
        $query = clone $this->query;
//        Pessimistic lock
//        if ($this->entityManager->isInTransaction()) {
//            $query->forUpdate();
//        }
        $records = $query->fetchAll();
        foreach ($records as $record) {
            $data = json_decode($record['data'], true);
            $this->addEntity(
                $this->entityManager->unSerialize($data, $this->entityClass),
                false
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function add(
        $entity
    ): void {
        $this->checkEntityClassOrFail($entity);

        $this->addEntity(
            $entity,
            !$this->entityManager->hasEntity(get_class($entity), $this->entityManager->getEntityId($entity))
        );
    }

    /**
     * @param $entity
     * @param bool $isNew
     */
    protected function addEntity(
        $entity,
        $isNew = false
    ): void {
        $entityId = $this->entityManager->getEntityId($entity);
        $entityClass = get_class($entity);
        if ($this->entityManager->hasEntity($entityClass, $entityId)) {
            $existedEntity = $this->entityManager->getEntity($entityClass, $entityId);
            //$this->reflectionWrapperFactory->create($entity)->copyTo($existedEntity);
            $entity = $existedEntity;
        }
        $this->entities[$this->entityManager->getEntityId($entity)] = $entity;
        $this->entityManager->watch($entity, $isNew);
    }

    /**
     * @param $entity
     * @throws InvalidEntityClassException
     */
    protected function checkEntityClassOrFail(
        $entity
    ): void {
        if (!($entity instanceof $this->entityClass)) {
            throw new InvalidEntityClassException(get_class($entity), $this->entityClass);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function find(
        string $id
    ) {
        $query = clone($this->query);
        $query->where('id = :id')->bindValue('id', $id);
        $record = $query->fetch();
        if ($record) {
            $entity = $this->entityManager->unSerialize(
                json_decode($record['data'], true),
                $this->entityClass
            );
            $this->addEntity($entity, false);
        }
        return $this->arrayHelper->get($this->entities, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(
        $entity
    ): void {
        $this->checkEntityClassOrFail($entity);
        $id = $this->entityManager->getEntityId($entity);
        if (!$this->find($id)) {
            throw new EntityNotExistsException($this->entityClass, $id);
        }
        $this->entityManager->delete($entity);
        unset($this->entities[$this->entityManager->getEntityId($entity)]);
    }


    /**
     * {@inheritdoc}
     */
    public function match(
        SpecificationInterface $specification
    ): CollectionInterface {
        $query = clone $this->query;
        $this->specificationInjectorFactory
            ->createJsonbSpecificationInjector($query)
            ->inject($specification);
        return new self(
            $this->entityClass,
            $query,
            $this->entityManager,
            $this->arrayHelper,
            $this->collectionFactory,
            $this->specificationInjectorFactory,
            $this->collectionFinder,
            $this->reflectionWrapperFactory
        );
    }
}
