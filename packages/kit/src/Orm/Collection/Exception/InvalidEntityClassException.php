<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Exception;

/**
 * Class InvalidEntityClassException
 * @package Grifix\Kit\Orm\Collection\Exception
 */
class InvalidEntityClassException extends \Exception
{
    protected $entityClass;

    protected $collectionEntityClass;

    /**
     * InvalidEntityClassException constructor.
     * @param string $entityClass
     * @param string $collectionEntityClass
     */
    public function __construct(string $entityClass, string $collectionEntityClass)
    {
        $this->entityClass = $entityClass;
        $this->collectionEntityClass = $collectionEntityClass;
        parent::__construct(sprintf('Entity %s is not instance of %s', $entityClass, $collectionEntityClass));
    }
}
