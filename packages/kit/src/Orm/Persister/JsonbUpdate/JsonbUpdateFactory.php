<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister\JsonbUpdate;

/**
 * Class JsonUpdater
 * @package Grifix\Kit\Orm\Persister
 */
class JsonbUpdateFactory implements JsonbUpdateFactoryInterface
{
    /**
     * @param array $data
     * @return JsonbUpdate[]
     */
    public function createUpdates(array $data): array
    {
        return $this->extractUpdate($data);
    }

    /**
     * @param array $value
     * @param array $path
     * @return array
     */
    protected function extractUpdate(array $value, array $path = []): array
    {
        $result = [];
        foreach ($value as $key => $val) {
            $valPath = array_merge($path, [$key]);
            if (!is_array($val)) {
                $result[] = new JsonbUpdate($valPath, $val);
            } else {
                $result = array_merge($result, $this->extractUpdate($val, $valPath));
            }
        }

        return $result;
    }
}
