<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister\JsonbUpdate;

/**
 * Class JsonUpdater
 * @package Grifix\Kit\Orm\Persister
 */
interface JsonbUpdateFactoryInterface
{
    /**
     * @param array $data
     * @return JsonbUpdate[]
     */
    public function createUpdates(array $data): array;
}