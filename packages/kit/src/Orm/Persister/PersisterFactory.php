<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Persister\JsonbUpdate\JsonbUpdateFactoryInterface;
use Grifix\Kit\Orm\Validator\ValidatorInterface;

/**
 * Class PersisterFactory
 * @package Grifix\Kit\Orm\Blueprint\Persister
 */
class PersisterFactory extends AbstractFactory implements PersisterFactoryInterface
{

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var JsonbUpdateFactoryInterface
     */
    protected $jsonbUpdateFactory;

    /** @var ValidatorInterface */
    private $validator;

    /**
     * PersisterFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param IocContainerInterface $iocContainer
     * @param JsonbUpdateFactoryInterface $jsonbUpdateFactory
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        IocContainerInterface $iocContainer,
        JsonbUpdateFactoryInterface $jsonbUpdateFactory,
        ValidatorInterface $validator
    ) {
        parent::__construct($classMaker);
        $this->iocContainer = $iocContainer;
        $this->jsonbUpdateFactory = $jsonbUpdateFactory;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function create(
        string $persisterClass,
        string $table,
        EntityManagerInterface $entityManager
    ): PersisterInterface {

        $class = $this->makeClassName($persisterClass);
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return new $class($this->validator, $this->jsonbUpdateFactory, $entityManager, $table);
    }
}
