<?php
declare(strict_types = 1);

namespace Grifix\Kit\Orm\Persister;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Persister\JsonbUpdate\JsonbUpdateFactoryInterface;
use Grifix\Kit\Orm\Validator\ValidatorInterface;

/**
 * Class AbstractPersister
 * @package Grifix\Kit\Orm\Persister
 */
class Persister implements PersisterInterface
{

    /**
     * @var string
     */
    protected $table;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var JsonbUpdateFactoryInterface
     */
    protected $jsonUpdateFactory;

    /** @var ValidatorInterface */
    private $validator;

    /**
     * Persister constructor.
     * @param JsonbUpdateFactoryInterface $jsonbUpdateFactory
     * @param EntityManagerInterface $entityManager
     * @param string $table
     */
    public function __construct(
        ValidatorInterface $validator,
        JsonbUpdateFactoryInterface $jsonbUpdateFactory,
        EntityManagerInterface $entityManager,
        string $table
    ) {
        $this->table = $table;
        $this->entityManager = $entityManager;
        $this->jsonUpdateFactory = $jsonbUpdateFactory;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function persist(object $entity): void
    {
        if ($this->entityManager->isNew($entity)) {
            $this->insert($entity);

        } elseif ($this->entityManager->isDirty($entity)) {
            if ($this->entityManager->isMarkedForDeletion($entity)) {
                $this->delete($entity);
            } else {
                $this->update($entity);
            }
        }
    }


    protected function insert(object $entity): void
    {
        $data = $this->entityManager->serialize($entity);
        $this->validate(get_class($entity), $data);

        $record = [
            'id' => $this->entityManager->getEntityId($entity),
            'data' => json_encode($data)
        ];
        if ($this->entityManager->hasParent($entity)) {
            $parentEntity = $this->entityManager->getParent($entity);
            if ($this->entityManager->isNew($parentEntity)) {
                $this->entityManager->persist($parentEntity);
            }
            $relation = $this->entityManager->getBlueprint(get_class($entity))
                ->getRelationToParent(get_class($parentEntity));
            $record[$relation->getChildColumn()] = $this->entityManager->getEntityId($parentEntity);
        }
        $query = $this->entityManager->getConnection()->createQuery();
        $query->insert($record)->into($this->table);
        $query->execute();
        $this->entityManager->commitChanges($entity);
    }

    /**
     * @param object $entity
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    protected function delete($entity): void
    {
        $this->entityManager->getConnection()->delete(
            $this->table, ['id' => $this->entityManager->getEntityId($entity)]
        );
    }

    protected function validate(string $entityClass, array $data): void
    {
        $this->validator->validate($entityClass, json_encode($data));
    }

    /**
     * @param $entity
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     */
    protected function update($entity): void
    {
        $this->validate(get_class($entity), $this->entityManager->serialize($entity));
        $query = $this->entityManager->getConnection()->createQuery();
        $data = $this->entityManager->serialize($entity);
        $query->update($this->table)->set([
            'data' => json_encode($data)
        ])
            ->where('id = :id')
            ->bindValue('id', $this->entityManager->getEntityId($entity))
            ->execute();

//        $updates = $this->jsonUpdateFactory->createUpdates($this->entityManager->getDiff($entity));
//        foreach ($updates as $update) {
//            $this->entityManager->getConnection()->execute(
//                'UPDATE ' . $this->table . ' SET data = jsonb_set(data, :path, :value) WHERE "id" = :id',
//                [
//                    'id' => $this->entityManager->getEntityId($entity),
//                    'path' => $update->getPathString(),
//                    'value' => json_encode($update->getValue())
//                ]
//            );
//        }

        $this->entityManager->commitChanges($entity);
    }
}
