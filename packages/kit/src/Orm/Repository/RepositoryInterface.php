<?php
declare(strict_types = 1);

namespace Grifix\Kit\Orm\Repository;

use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Specification\SpecificationInterface;

interface RepositoryInterface extends GenericCollectionInterface
{

    public function find(string $id): ?object;

    public function add(object $entity): void;

    public function delete(object $entity): void;

    public function match(SpecificationInterface $specification): CollectionInterface;

    public function first();

    public function last();

    public function findBy(string $property, $value): CollectionInterface;
}
