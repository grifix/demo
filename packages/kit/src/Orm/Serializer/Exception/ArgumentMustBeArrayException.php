<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer\Exception;

use Grifix\Kit\Cache\Exception\InvalidArgumentException;
use Throwable;

/**
 * Class AgrumentMustBeArrayException
 *
 * @package Grifix\Kit\OldRepository\Serializer\Exception
 */
class ArgumentMustBeArrayException extends InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('Value must be array!');
    }
}
