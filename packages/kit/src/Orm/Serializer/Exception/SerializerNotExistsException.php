<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer\Exception;

/**
 * Class SerializerNotExistsException
 * @package Grifix\Kit\Orm\Serializer\Exception
 */
class SerializerNotExistsException extends \Exception
{
    protected $objectClass;

    /**
     * SerializerNotExistsException constructor.
     * @param string $objectClass
     */
    public function __construct(string $objectClass)
    {
        $this->objectClass = $objectClass;
        parent::__construct(sprintf('Serializer not exists for class %s!', $objectClass));
    }
}