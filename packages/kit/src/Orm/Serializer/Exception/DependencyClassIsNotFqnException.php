<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\Serializer\Exception;

class DependencyClassIsNotFqnException extends \RuntimeException
{
    public function __construct(string $dependencyClass, string $objectClass, string $propertyName)
    {
        parent::__construct(sprintf(
            'Dependency class "%s" is not FQN for object "%s" property "%s"!',
            $dependencyClass,
            $objectClass,
            $propertyName
        ));
    }
}
