<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer\Exception;

use Grifix\Kit\Cache\Exception\InvalidArgumentException;

class InvalidClassException extends InvalidArgumentException
{
    protected $expectedClass;

    protected $actualClass;

    /**
     * InvalidClassException constructor.
     * @param $expectedClass
     * @param $actualClass
     */
    public function __construct($expectedClass, $actualClass)
    {
        $this->expectedClass = $expectedClass;
        $this->actualClass = $actualClass;
        parent::__construct('%s is not instance of %s!', $actualClass, $expectedClass);
    }


}
