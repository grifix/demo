<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

class DateTimeSerializer implements SerializerInterface
{

    protected $format = \DateTimeInterface::ATOM;

    /**
     * {@inheritdoc}
     */
    public function serialize($value)
    {
        if ($value instanceof \DateTimeInterface) {
            return $value->format($this->format);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($value)
    {
        if ($value) {
            return new \DateTimeImmutable($value);
        }

        return null;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format)
    {
        $this->format = $format;
    }
}
