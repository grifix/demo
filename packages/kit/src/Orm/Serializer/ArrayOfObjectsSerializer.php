<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeArrayException;

class ArrayOfObjectsSerializer implements SerializerInterface
{
    private ObjectSerializerInterface $objectSerializer;

    public function __construct(ObjectSerializerInterface $objectSerializer)
    {
        $this->objectSerializer = $objectSerializer;
    }

    public function serialize($value)
    {
        $this->assertValueIsArray($value);
        $result = [];
        foreach ($value as $key => $val) {
            $result[] = $this->objectSerializer->serialize($val);
        }
        return $result;
    }

    public function unSerialize($value)
    {
        $this->assertValueIsArray($value);
        $result = [];
        foreach ($value as $key => $val) {
            $result[] = $this->objectSerializer->unSerialize($val);
        }
        return $result;
    }

    private function assertValueIsArray($value): void
    {
        if (!is_array($value)) {
            throw new ArgumentMustBeArrayException();
        }
    }
}
