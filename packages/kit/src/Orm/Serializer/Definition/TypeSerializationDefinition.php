<?php
declare(strict_types = 1);

namespace Grifix\Kit\Orm\Serializer\Definition;

class TypeSerializationDefinition
{

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $objectClass;

    /**
     * @var string
     */
    protected $serializerClass;

    /**
     * SerializationDefinition constructor.
     * @param string $type
     * @param string $objectClass
     * @param string $serializerClass
     */
    public function __construct(string $type, string $objectClass, string $serializerClass)
    {
        $this->type = $type;
        $this->objectClass = $objectClass;
        $this->serializerClass = $serializerClass;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getObjectClass(): string
    {
        return $this->objectClass;
    }

    /**
     * @return string
     */
    public function getSerializerClass(): string
    {
        return $this->serializerClass;
    }
}
