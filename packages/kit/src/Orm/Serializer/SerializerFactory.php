<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\PropertyAnalyzerFactoryInterface;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObjectFactoryInterface;

class SerializerFactory extends AbstractFactory implements SerializerFactoryInterface
{

    protected IocContainerInterface $iocContainer;

    protected ClassHelperInterface $objectHelper;

    protected CollectionFactoryInterface $collectionFactory;

    protected ArrayHelperInterface $arrayHelper;

    protected ReflectionObjectFactoryInterface $reflectionWrapperFactory;

    protected PropertyAnalyzerFactoryInterface $propertyAnalyzerFactory;

    protected ImplementationFinderInterface $implementationFinder;

    public function __construct(
        ClassMakerInterface $classMaker,
        IocContainerInterface $iocContainer,
        ClassHelperInterface $objectHelper,
        CollectionFactoryInterface $collectionFactory,
        ArrayHelperInterface $arrayHelper,
        ReflectionObjectFactoryInterface $reflectionWrapperFactory,
        PropertyAnalyzerFactoryInterface $propertyAnalyzerFactory,
        ImplementationFinderInterface $implementationFinder
    ) {
        parent::__construct($classMaker);
        $this->iocContainer = $iocContainer;
        $this->objectHelper = $objectHelper;
        $this->collectionFactory = $collectionFactory;
        $this->arrayHelper = $arrayHelper;
        $this->reflectionWrapperFactory = $reflectionWrapperFactory;
        $this->propertyAnalyzerFactory = $propertyAnalyzerFactory;
        $this->implementationFinder = $implementationFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function createSerializer(string $serializerClass): SerializerInterface
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->iocContainer->createNewInstance($this->makeClassName($serializerClass));
    }

    /**
     * {@inheritdoc}
     */
    public function createObjectSerializer(
        EntityManagerInterface $entityManager,
        string $objectClass,
        string $serializerClass = AbstractObjectSerializer::class
    ): ObjectSerializerInterface {
        /** @var AbstractObjectSerializer $class */
        $class = $this->makeClassName($serializerClass);
        return new $class(
            $objectClass,
            $this,
            $this->objectHelper,
            $this->iocContainer,
            $entityManager,
            $this->arrayHelper,
            $this->propertyAnalyzerFactory,
            $this->implementationFinder
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createMultiClassSerializer(
        array $serializers,
        string $discriminatorColumn = 'type'
    ): SerializerInterface {
        $class = $this->makeClassName(MultiClassSerializer::class);
        return new $class($serializers, $discriminatorColumn);
    }

    /**
     * {@inheritdoc}
     */
    public function createCallableSerializer(callable $serialize, callable $unSerialize): SerializerInterface
    {
        $class = $this->makeClassName(CallableSerializer::class);
        return new $class($serialize, $unSerialize);
    }

    /**
     * @return SerializerInterface
     */
    public function createCollectionSerializer(): SerializerInterface
    {
        $class = $this->makeClassName(CollectionSerializer::class);
        return new $class($this->collectionFactory);
    }

    public function createArrayOfObjectsSerializer(ObjectSerializerInterface $objectSerializer): SerializerInterface
    {
        $class = $this->makeClassName(ArrayOfObjectsSerializer::class);
        return new $class($objectSerializer);
    }
}
