<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\PropertyAnalyzerFactoryInterface;
use Grifix\Kit\Orm\Serializer\Definition\TypeSerializationDefinition;
use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeArrayException;
use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeObjectException;
use Grifix\Kit\Orm\Serializer\Exception\DependencyClassIsNotFqnException;

/**
 * Class Serializer
 *
 * @package Grifix\Kit\OldRepository\Serializer
 */
abstract class AbstractObjectSerializer implements ObjectSerializerInterface
{

    /**
     * @var SerializerInterface[]
     */
    protected $propertySerializers = [];

    /**
     * @var SerializerInterface[]
     */
    protected $typeSerializers = [];

    /**
     * @var string
     */
    protected $objectClass;

    /**
     * @var \ReflectionClass
     */
    protected $reflection;

    /**
     * @var SerializerFactoryInterface
     */
    protected $serializerFactory;

    /**
     * @var ClassHelperInterface
     */
    protected $objectHelper;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /** @var PropertyAnalyzerFactoryInterface */
    protected $propertyAnalyzerFactory;

    /** @var ImplementationFinderInterface */
    protected $implementationFinder;

    public function __construct(
        string $objectClass,
        SerializerFactoryInterface $serializerFactory,
        ClassHelperInterface $objectHelper,
        IocContainerInterface $iocContainer,
        EntityManagerInterface $entityManager,
        ArrayHelperInterface $arrayHelper,
        PropertyAnalyzerFactoryInterface $propertyAnalyzerFactory,
        ImplementationFinderInterface $implementationFinder,
        array $propertySerializers = [],
        array $typeSerializers = []
    ) {
        $this->objectClass = $objectClass;
        $this->serializerFactory = $serializerFactory;
        $this->objectHelper = $objectHelper;
        $this->iocContainer = $iocContainer;
        $this->propertySerializers = $propertySerializers;
        $this->entityManager = $entityManager;
        $this->arrayHelper = $arrayHelper;
        $this->typeSerializers = $typeSerializers;
        $this->propertyAnalyzerFactory = $propertyAnalyzerFactory;
        $this->implementationFinder = $implementationFinder;
        $this->init();
    }


    abstract protected function init(): void;

    /**
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    protected function getReflection(): \ReflectionClass
    {
        if (!$this->reflection) {
            $class = $this->objectClass;
            if (interface_exists($class)) {
                $class = $this->iocContainer->getImplementationClass($class);
            }
            $this->reflection = new \ReflectionClass($class);
        }
        return $this->reflection;
    }

    /**
     * @param string $property
     * @param SerializerInterface $serializer
     * @return AbstractObjectSerializer
     */
    protected function addPropertySerializer(string $property, SerializerInterface $serializer): self
    {
        $this->propertySerializers[$property] = $serializer;
        return $this;
    }

    protected function addTypeSerializer(string $type, SerializerInterface $serializer): self
    {
        $this->typeSerializers[$type] = $serializer;
        return $this;
    }

    /**
     * @param string $property
     * @param string $objectClass
     * @param string|null $serializerClass
     * @return AbstractObjectSerializer
     */
    protected function addObjectPropertySerializer(
        string $property,
        string $objectClass,
        string $serializerClass = AbstractObjectSerializer::class
    ): self {
        $this->addPropertySerializer(
            $property,
            $this->serializerFactory->createObjectSerializer($this->entityManager, $objectClass, $serializerClass)
        );
        return $this;
    }

    /**
     * @param string $property
     * @param TypeSerializationDefinition[] $definitions
     * @param string $typeColumn
     */
    protected function addMultiTypePropertySerializer(string $property, array $definitions, string $typeColumn = 'type')
    {
        $serializers = [];
        foreach ($definitions as $definition) {
            $serializers[$definition->getType()] = $this->serializerFactory->createObjectSerializer(
                $this->entityManager,
                $definition->getObjectClass(),
                $definition->getSerializerClass()
            );
        }
        $this->addPropertySerializer(
            $property,
            $this->serializerFactory->createMultiClassSerializer($serializers, $typeColumn)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getObjectClass(): string
    {
        return $this->objectClass;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($object = null)
    {
        if ($object === null) {
            return null;
        }
        if (!is_object($object)) {
            throw new ArgumentMustBeObjectException();
        }
        $result = [];
        foreach ($this->getReflection()->getProperties() as $property) {
            $serializer = $this->getPropertySerializer($property);
            if ($serializer) {
                $property->setAccessible(true);
                $value = $property->getValue($object);
                if ($value !== null) {
                    $result[$property->getName()] = $serializer->serialize($property->getValue($object));
                } else {
                    $result[$property->getName()] = null;
                }

                $property->setAccessible(false);
            }
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($data)
    {
        if (!is_array($data)) {
            throw new ArgumentMustBeArrayException();
        }

        $result = $this->getReflection()->newInstanceWithoutConstructor();
        foreach ($this->getReflection()->getProperties() as $property) {
            $analyzedProperty = $this->propertyAnalyzerFactory->createPropertyAnalyzer($property);
            $serializer = $this->getPropertySerializer($property);
            if ($serializer) {
                $property->setAccessible(true);
                $value = $this->arrayHelper->get($data, $property->getName());
                if ($value !== null) {
                    $property->setValue(
                        $result,
                        $serializer->unSerialize($this->arrayHelper->get($data, $property->getName()))
                    );
                } else {
                    $property->setValue($result, null);
                }

                $property->setAccessible(false);
            }
            if ($analyzedProperty->isDependency()) {
                $this->injectDependency($property, $result);
            }
        }
        return $result;
    }

    /**
     * @param \ReflectionProperty $property
     * @param object $object
     */
    protected function injectDependency(\ReflectionProperty $property, $object): void
    {
        $analyzedProperty = $this->propertyAnalyzerFactory->createPropertyAnalyzer($property);
        $class = $analyzedProperty->getType();
        if (false === strpos($class, '\\')) {
            throw new DependencyClassIsNotFqnException($class, get_class($object), $property->getName());
        }
        $dependency = $this->iocContainer->get($class);
        $property->setAccessible(true);
        $property->setValue($object, $dependency);
        $property->setAccessible(false);
    }

    /**
     * @param \ReflectionProperty $property
     * @return SerializerInterface|null
     */
    protected function getPropertySerializer(\ReflectionProperty $property): ?SerializerInterface
    {
        $analyzedProperty = $this->propertyAnalyzerFactory->createPropertyAnalyzer($property);

        if ($analyzedProperty->isArrayOfObjects()) {
            return $this->serializerFactory->createArrayOfObjectsSerializer(
                $this->serializerFactory->createObjectSerializer(
                    $this->entityManager,
                    $analyzedProperty->getElementType(),
                    static::class
                )
            );
        }

        if (isset($this->propertySerializers[$property->getName()])) {
            return $this->propertySerializers[$property->getName()];
        }

        if ($analyzedProperty->isDependency() || $analyzedProperty->isRelatedCollection()) {
            return null;
        }

        $type = $analyzedProperty->getType();
        if ($analyzedProperty->isCollection()) {
            return $this->serializerFactory->createCollectionSerializer();
        }

        if ($analyzedProperty->isSimpleType()) {
            return $this->serializerFactory->createSerializer(SimpleSerializer::class);
        }

        foreach ($this->typeSerializers as $serializerType => $serializer) {
            if ($this->objectHelper->isInstanceOf($type, $serializerType)) {
                return $this->typeSerializers[$serializerType];
            }
        }

        return $this->serializerFactory->createObjectSerializer($this->entityManager, $type, static::class);
    }

    /**
     * @param \ReflectionProperty $property
     * @return mixed|string
     */
    protected function detectType(\ReflectionProperty $property)
    {
        if (!$property->getDocComment()) {
            return 'string';
        }
        $matches = [];
        preg_match('/@var\s(\S*)/', $property->getDocComment(), $matches);
        if (!$matches || !isset($matches[1])) {
            return 'string';
        }
        return explode('|', $matches[1])[0];
    }
}
