<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

use Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException;

/**
 * Class IdentityMap
 * @package Grifix\Kit\Orm\IdentityMap
 */
interface IdentityMapInterface
{
    /**
     * @param $entity
     * @param bool $isNew
     */
    public function addEntity($entity, bool $isNew = true): void;

    /**
     * @param string $entityClass
     * @param string $id
     * @return object
     */
    public function getEntity(string $entityClass, string $id);

    /**
     * @param $entity
     * @return bool
     */
    public function hasEntity($entity): bool;

    /**
     * @param string $entityClass
     * @param string $id
     * @return bool
     */
    public function hasEntityWithId(string $entityClass, string $id): bool;

    /**
     * @param callable $visitor
     */
    public function visitDirtyItems(callable $visitor): void;

    /**
     * @param callable $visitor
     */
    public function visitNewItems(callable $visitor): void;

    /**
     * @param $entity
     * @return array
     * @throws ItemNotExistsException
     */
    public function getDiff($entity): array;

    /**
     * @param $entity
     * @return bool
     * @throws ItemNotExistsException
     */
    public function isNew($entity): bool;

    /**
     * @param object $entity
     * @throws ItemNotExistsException
     */
    public function commitChanges($entity): void;

    /**
     * @param $entity
     * @return bool
     * @throws ItemNotExistsException
     */
    public function isDirty($entity): bool;

    /**
     * @param $entity
     * @return bool
     * @throws ItemNotExistsException
     */
    public function isMarkedForDeletion($entity): bool;

    /**
     * @param $entity
     * @throws ItemNotExistsException
     */
    public function markForDeletion($entity): void;

    /**
     * @param $entity
     * @throws ItemNotExistsException
     */
    public function markDeleted($entity): void;

    /**
     * @param callable $visitor
     */
    public function visitAllItems(callable $visitor): void;
}
