<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap\Exception;

/**
 * Class SameEntityAleradyExistsException
 * @package Grifix\Kit\Orm\IdentityMap\Exception
 */
class SameEntityAlreadyExistsException extends \RuntimeException
{
    protected $entityClass;

    protected $entityId;

    public function __construct(string $entityClass, string $entityId)
    {
        $this->entityClass = $entityClass;
        $this->entityId = $entityId;
        parent::__construct(
            sprintf(
                'Entity "%s" with id "%s"  already registered in the IdentityMap',
                $entityClass,
                $entityId
            )
        );
    }
}