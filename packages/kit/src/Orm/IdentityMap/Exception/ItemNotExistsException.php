<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap\Exception;

/**
 * Class ItemNotExistsException
 * @package Grifix\Kit\Orm\IdentityMap\Exception
 */
class ItemNotExistsException extends \Exception
{
    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var string
     */
    protected $entityId;

    /**
     * ItemNotExistsException constructor.
     * @param string $entityClass
     * @param string $entityId
     */
    public function __construct(string $entityClass, string $entityId)
    {
        $this->entityClass = $entityClass;
        $this->entityId = $entityId;
        parent::__construct(sprintf('There is no entity item for entity "%s" with id "%s"', $entityClass, $entityId));
    }
}
