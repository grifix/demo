<?php declare(strict_types=1);

namespace Grifix\Kit\Orm\Validator\Exception;


class InvalidJsonException extends \RuntimeException
{
    public function __construct(private string $entityClass, private array $errors, private string $jsonPath)
    {
        $this->errors = $errors;
        $message = [];
        foreach ($errors as $error) {
            $message[] = sprintf("[%s] %s\n", $error['property'], $error['message']);
        }
        parent::__construct(sprintf(
            "JSON does not match the scheme \"%s\" for entity class \"%s\": \n%s",
            $this->jsonPath,
            $this->entityClass,
            implode("\n", $message)
        ));
    }
}
