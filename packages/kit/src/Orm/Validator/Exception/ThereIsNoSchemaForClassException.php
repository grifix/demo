<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Kit\Orm\Validator\Exception;

use Grifix\Shared\Ui\Cli\Command\GenerateJsonSchemaCommand;
use Mockery\Exception\RuntimeException;

class ThereIsNoSchemaForClassException extends RuntimeException
{
    protected string $class;

    public function __construct(string $class, string $path)
    {
        $this->class = $class;
        parent::__construct(sprintf(
            'There is no json schema "%s" for class "%s", use %s "%s" to generate it!',
            $path,
            $class,
            GenerateJsonSchemaCommand::NAME,
            $class
        ));
    }
}
