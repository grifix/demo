<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\Validator;

interface ValidatorInterface
{
    public function validate(string $entityClass, string $json): void;
}
