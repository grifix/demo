<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\Validator;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Orm\Validator\Exception\InvalidJsonException;
use Grifix\Kit\Orm\Validator\Exception\ThereIsNoSchemaForClassException;
use JsonSchema\Validator as VendorValidator;
use ReflectionClass;

class Validator implements ValidatorInterface
{

    /** @var string */
    protected $rootDir;

    /** @var FilesystemHelperInterface */
    protected $fileSystemHelper;

    /** @var VendorValidator */
    protected $validator;

    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    /**
     * @var string $rootDir <root_dir>
     */
    public function __construct(
        string $rootDir,
        FilesystemHelperInterface $fileSystemHelper,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->rootDir = $rootDir;
        $this->fileSystemHelper = $fileSystemHelper;
        $this->validator = new VendorValidator();
        $this->arrayHelper = $arrayHelper;
    }


    public function validate(string $entityClass, string $json): void
    {
        $data = json_decode($json);
        $path = realpath($this->detectPath($entityClass));
        $this->validator->validate(
            $data,
            (object) ['$ref' => sprintf('file://%s', $path)]
        );

        if (!$this->validator->isValid()) {
            throw new InvalidJsonException($entityClass, $this->validator->getErrors(), $path);
        }
    }

    protected function detectPath(string $class): string
    {
        $reflection = new ReflectionClass($class);
        $path = $reflection->getFileName();
        $path = str_replace('Domain', 'Infrastructure/Domain', $path);
        $path = str_replace($reflection->getShortName().'.php', 'schema.json', $path);
        if (file_exists($path)) {
            return $path;
        }
        throw new ThereIsNoSchemaForClassException($class, $path);
    }
}
