<?php declare(strict_types=1);

namespace Grifix\Kit\Orm\SchemaGenerator;

use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\PropertyAnalyzerFactoryInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\PropertyAnalyzerInterface;
use ReflectionClass;

class SchemaGenerator implements SchemaGeneratorInterface
{

    protected PropertyAnalyzerFactoryInterface $propertyAnalyzerFactory;

    protected ImplementationFinderInterface $implementationFinder;

    public function __construct(
        PropertyAnalyzerFactoryInterface $propertyAnalyzerFactory,
        ImplementationFinderInterface $implementationFinder
    ) {
        $this->propertyAnalyzerFactory = $propertyAnalyzerFactory;
        $this->implementationFinder = $implementationFinder;
    }


    public function generate(string $class): string
    {
        return json_encode($this->generateObject($class), JSON_PRETTY_PRINT);
    }

    protected function generateProperty(PropertyAnalyzerInterface $property): array
    {
        $result = $this->definePropertyType($property);

        if ($property->isNullable()) {
            $result['type'] = [$result['type'], 'null'];
        }

        return $result;
    }

    protected function definePropertyType(PropertyAnalyzerInterface $property): array
    {
        if ($property->isString()) {
            return [
                'type' => 'string'
            ];
        }

        if ($property->isInt()) {
            return [
                'type' => 'integer'
            ];
        }

        if ($property->isFloat()) {
            return [
                'type' => 'float'
            ];
        }

        if ($property->isBool()) {
            return [
                'type' => 'boolean'
            ];
        }

        if ($property->isStringArray()) {
            return [
                'type' => 'array',
                'items' => [
                    'type' => 'string'
                ]
            ];
        }

        if ($property->isIntArray()) {
            return [
                'type' => 'array',
                'items' => [
                    'type' => 'integer'
                ]
            ];
        }

        if ($property->isFloatArray()) {
            return [
                'type' => 'array',
                'items' => [
                    'type' => 'float'
                ]
            ];
        }

        if ($property->isArray() || $property->isCollection()) {
            $type = $property->getElementType();
            if ($type && (class_exists($type) || interface_exists($type))) {
                return [
                    'type' => 'array',
                    'items' => $this->generateObject($type)
                ];
            }

            return [
                'oneOf' => [
                    [
                        'type' => 'array'
                    ],
                    [
                        'type' => 'object'
                    ]
                ]
            ];
        }

        return [
            'type' => 'string'
        ];
    }

    protected function generateInterface(string $interface, bool $isNullable = false): array
    {
        $implementations = $this->implementationFinder->findAll($interface);
        if (count($implementations) === 1) {
            return $this->generateObject($implementations[0], $isNullable);
        }
        $defaultImplementation = $this->implementationFinder->findDefault($interface);
        if ($defaultImplementation) {
            return $this->generateObject($defaultImplementation, $isNullable);
        }

        $result = [
            'oneOf' => []
        ];
        if ($isNullable) {
            $result['oneOf'][] = ['type' => null];
        }

        $schemas = [];
        foreach ($implementations as $implementation) {
            $schemas[] = json_encode($this->generateObject($implementation, false, [
                'required' => ['_type'],
                'properties' => [
                    '_type' => [
                        'type' => 'string'
                    ]
                ]
            ]));
            $schemas = array_unique($schemas);
        }
        foreach ($schemas as $schema) {
            $result['oneOf'][] = json_decode($schema, true);
        }

        return $result;
    }

    protected function generateObject(string $class, bool $isNullable = false, array $template = []): array
    {
        if (interface_exists($class)) {
            return $this->generateInterface($class, $isNullable);
        }

        $result = array_merge([
            'type' => 'object',
            'additionalProperties' => false,
            'required' => [],
            'properties' => []
        ], $template);

        $reflection = new ReflectionClass($class);

        if (count($reflection->getProperties()) < 1) {
            return [
                'type' => 'string'
            ];
        }

        foreach ($reflection->getProperties() as $property) {
            $analyzedProperty = $this->propertyAnalyzerFactory->createPropertyAnalyzer($property);

            if ($analyzedProperty->isDependency() || $analyzedProperty->isRelatedCollection()) {
                continue;
            }

            $result['required'][] = $property->getName();

            if ($analyzedProperty->isSimpleType() || $analyzedProperty->isCollection()) {
                $result['properties'][$property->getName()] = $this->generateProperty($analyzedProperty);
            } else {
                $result['properties'][$property->getName()] = $this->generateObject(
                    $analyzedProperty->getType(),
                    $analyzedProperty->isNullable()
                );
            }
        }

        if ($isNullable) {
            $result = [
                'oneOf' => [
                    [
                        'type' => 'null'
                    ],
                    $result
                ]
            ];
        }

        return $result;
    }
}
