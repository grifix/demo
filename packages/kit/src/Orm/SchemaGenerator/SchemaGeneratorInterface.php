<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\SchemaGenerator;

interface SchemaGeneratorInterface
{
    public function generate(string $class): string;
}
