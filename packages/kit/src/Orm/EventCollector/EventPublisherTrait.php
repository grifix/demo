<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

trait EventPublisherTrait
{
    protected EventCollectorInterface $eventCollector;

    public function publishEvent(object $event, object $aggregate): void
    {
        $this->eventCollector->collectEvent($event, $aggregate);
    }
}
