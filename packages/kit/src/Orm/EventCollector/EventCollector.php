<?php

declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

use Grifix\Kit\Event\Bus\EventBusInterface;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObjectFactoryInterface;

class EventCollector implements EventCollectorInterface
{
    protected array $events = [];

    public function __construct(
        protected EventStoreInterface $eventStore,
        protected EventBusInterface $eventBus,
        protected ReflectionObjectFactoryInterface $reflectionObjectFactory
    ) {
    }

    public function collectEvent(object $event, object $aggregate): void
    {
        $key = $this->createAggregateKey($aggregate);
        if (!isset($this->events[$key])) {
            $this->events[$key] = [];
        }
        $this->events[$key][] = $event;
    }

    public function storeEvents(object $aggregate): void
    {
        foreach ($this->getEvents($aggregate) as $event) {
            $this->eventStore->add($event);
        }
    }

    public function publishEvents(object $aggregate): void
    {
        $key = $this->createAggregateKey($aggregate);
        $this->eventStore->publish();
        foreach ($this->getEvents($aggregate) as $event) {
            $this->eventBus->send($event);
        }
        if(isset($this->events[$key])){
            $this->events[$key] = [];
        }
    }

    private function createAggregateKey(object $aggregate): string
    {
        $reflection = $this->reflectionObjectFactory->create($aggregate);

        return $aggregate::class . '_' . (string)$reflection->getPropertyValue('id');
    }

    private function getEvents(object $aggregate): array
    {
        $key = $this->createAggregateKey($aggregate);
        if (!isset($this->events[$key])) {
            return [];
        }
        return $this->events[$key];
    }
}
