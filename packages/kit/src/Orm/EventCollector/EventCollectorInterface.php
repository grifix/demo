<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class EventManager
 * @package Grifix\Kit\OldRepository
 */
interface EventCollectorInterface
{
    /**
     * @param object $event
     */
    public function collectEvent(object $event, object $aggregate): void;

    /**
     *
     */
    public function publishEvents(object $aggregate): void;

    public function storeEvents(object $aggregate): void;
}
