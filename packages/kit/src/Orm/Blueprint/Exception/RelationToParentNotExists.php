<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class RelationToParentNotExists
 * @package Grifix\Kit\Orm\Blueprint\Exception
 */
class RelationToParentNotExists extends \Exception
{
    protected $parentClass;

    /**
     * RelationToParentNotExists constructor.
     * @param string $parentClass
     */
    public function __construct(string $parentClass)
    {
        parent::__construct(sprintf(
            'Relation to parent class "%s" does not exist!',
            $parentClass
        ));

        $this->parentClass = $parentClass;
    }
}
