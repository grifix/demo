<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class EntityHasNoBlueprintException
 * @package Grifix\Kit\Orm\EntityManager
 */
class EntityHasNoBlueprintException extends \Exception
{
    protected $entityClass;

    /**
     * EntityHasNoBlueprintException constructor.
     * @param string $entityClass
     */
    public function __construct(string $entityClass)
    {
        parent::__construct(sprintf('"%s" has no blueprint!', $entityClass));
        $this->entityClass = $entityClass;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }
}
