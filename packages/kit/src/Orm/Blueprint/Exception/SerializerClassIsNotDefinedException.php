<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class TableNotDefinedException
 * @package Grifix\Kit\Orm\Blueprint\Exception
 */
class SerializerClassIsNotDefinedException extends \Exception
{
    protected $blueprintClass;

    /**
     * TableNotDefinedException constructor.
     * @param string $blueprintClass
     */
    public function __construct(string $blueprintClass)
    {
        $this->blueprintClass;
        parent::__construct(sprintf('Serializer class is not defined for blueprint "%s"', $blueprintClass));
    }
}
