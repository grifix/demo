<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

use Grifix\Kit\Orm\Blueprint\BlueprintInterface;

/**
 * Class NotBlueprintInterfaceException
 * @package Grifix\Kit\Orm\Blueprint\Exception
 */
class NotBlueprintInterfaceException extends \InvalidArgumentException
{
    protected $blueprintClass;

    /**
     * NotBlueprintInterfaceException constructor.
     * @param string $blueprintClass
     */
    public function __construct(string $blueprintClass)
    {
        $this->blueprintClass = $blueprintClass;
        parent::__construct($blueprintClass . ' must implement ' . BlueprintInterface::class);
    }
}
