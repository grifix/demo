<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Orm\Blueprint\Exception\BlueprintAlreadyRegisteredException;
use Grifix\Kit\Orm\Blueprint\Exception\EntityHasNoBlueprintException;

/**
 * Class BlueprintMap
 * @package Grifix\Kit\Orm\Blueprint
 */
class BlueprintMap implements BlueprintMapInterface
{
    protected $blueprints = [];
    protected $classHelper;

    /**
     * BlueprintMap constructor.
     * @param ClassHelperInterface $classHelper
     */
    public function __construct(ClassHelperInterface $classHelper)
    {
        $this->classHelper = $classHelper;
    }

    /**
     * @param BlueprintInterface $blueprint
     * @throws BlueprintAlreadyRegisteredException
     * @throws \ReflectionException
     */
    public function registerBlueprint(BlueprintInterface $blueprint)
    {
        if ($this->hasBlueprint($blueprint->getEntityClass())) {
            throw new BlueprintAlreadyRegisteredException($blueprint->getEntityClass());
        }
        $this->blueprints[$blueprint->getEntityClass()] = $blueprint;
    }

    /**
     * @param string $entityClass
     * @return BlueprintInterface
     * @throws EntityHasNoBlueprintException
     * @throws \ReflectionException
     */
    public function getBlueprint(string $entityClass): BlueprintInterface
    {
        $result = $this->detectBlueprint($entityClass);
        if (!$result) {
            throw new EntityHasNoBlueprintException($entityClass);
        }
        return $result;
    }

    /**
     * @param string $entityClass
     * @return bool
     * @throws \ReflectionException
     */
    public function hasBlueprint(string $entityClass): bool
    {
        return boolval($this->detectBlueprint($entityClass));
    }

    /**
     * @param string $entityClass
     * @return BlueprintInterface|null
     * @throws \ReflectionException
     */
    protected function detectBlueprint(string $entityClass)
    {
        $entityClass = $this->classHelper->makeFullClassName($entityClass);
        if (isset($this->blueprints[$entityClass])) {
            return $this->blueprints[$entityClass];
        }
        $reflection = new \ReflectionClass($entityClass);
        if ($reflection->getParentClass()) {
            return $this->detectBlueprint($reflection->getParentClass()->getName());
        }
        return null;
    }
}
