<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Relation;

/**
 * Class RelationToChild
 * @package Grifix\Kit\Orm\Blueprint\Relation
 */
class RelationToChild extends AbstractRelation
{

    /**
     * @var string
     */
    protected $collectionClass;

    /**
     * RelationToChild constructor.
     * @param string $parentClass
     * @param string $parentProperty
     * @param string $childClass
     * @param string $childColumn
     * @param string $collectionClass
     */
    public function __construct(
        string $parentClass,
        string $parentProperty,
        string $childClass,
        string $childColumn,
        string $collectionClass
    ) {

        $this->collectionClass = $collectionClass;
        parent::__construct($parentClass, $parentProperty, $childClass, $childColumn);
    }

    /**
     * @return string
     */
    public function getCollectionClass(): string
    {
        return $this->collectionClass;
    }
}
