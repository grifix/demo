<?php

declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Orm\Blueprint\ClassDetector\ClassDetectorFactoryInterface;
use Grifix\Kit\Orm\Blueprint\ClassDetector\ClassDetectorInterface;
use Grifix\Kit\Orm\Blueprint\Exception\RelationNotExistsException;
use Grifix\Kit\Orm\Blueprint\Exception\RelationToChildrenNotExists;
use Grifix\Kit\Orm\Blueprint\Exception\RelationToParentNotExists;
use Grifix\Kit\Orm\Blueprint\Relation\AbstractRelation;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToChild;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToParent;
use Grifix\Kit\Orm\Persister\Persister;
use Grifix\Shared\Infrastructure\Internal\Serializer\ObjectSerializer;
use ReflectionClass;

abstract class AbstractBlueprint implements BlueprintInterface
{
    protected const TRANSACTION_LOCK_PESSIMISTIC = 'pessimistic';

    protected const TRANSACTION_LOCK_OPTIMISTIC = 'optimistic';

    protected ?string $transactionLock = self::TRANSACTION_LOCK_PESSIMISTIC;

    protected array $properties = [];

    /** @var AbstractRelation[] */
    protected array $relations = [];

    protected ?ReflectionClass $reflection = null;

    protected ClassDetectorFactoryInterface $classDetectorFactory;

    protected ?ClassDetectorInterface $classDetector = null;

    protected ClassHelperInterface $classHelper;

    final public function __construct(
        ClassDetectorFactoryInterface $classDetectorFactory,
        ClassHelperInterface $classHelper
    ) {
        $this->classDetectorFactory = $classDetectorFactory;
        $this->classHelper = $classHelper;
        $this->init();
    }

    protected function init(): void
    {
    }

    public function getEntityClass(): string
    {
        return $this->classHelper->makeFullClassName($this->getEntityClassName());
    }

    public function isTransactionLockOptimistic(): bool
    {
        return $this->transactionLock === self::TRANSACTION_LOCK_OPTIMISTIC;
    }

    public function isTransactionLockPessimistic(): bool
    {
        return $this->transactionLock === self::TRANSACTION_LOCK_PESSIMISTIC;
    }

    public function hasTransactionLock(): bool
    {
        return null !== $this->transactionLock;
    }

    public function getPersisterClass(): string
    {
        return Persister::class;
    }

    public function getRelationTo(string $entityClass): AbstractRelation
    {
        $entityClass = $this->classHelper->makeFullClassName($entityClass);
        foreach ($this->relations as $relation) {
            if ($this->classHelper->makeFullClassName($relation->getChildClass()) === $entityClass
                || $this->classHelper->makeFullClassName($relation->getParentClass()) === $entityClass) {
                return $relation;
            }
        }
        throw new RelationNotExistsException($entityClass);
    }

    public function hasRelationTo(string $entityClass): bool
    {
        try {
            $this->getRelationTo($entityClass);
        } catch (RelationNotExistsException $exception) {
            return false;
        }
        return true;
    }

    public function getId($entity): string
    {
        $property = $this->getReflection()->getProperty('id');
        $property->setAccessible(true);
        $result = $property->getValue($entity);
        $property->setAccessible(false);
        return (string)$result;
    }

    protected function getReflection(): ReflectionClass
    {
        if (!$this->reflection) {
            $this->reflection = new ReflectionClass($this->getEntityClass());
        }
        return $this->reflection;
    }

    protected function addRelationToChild(
        string $property,
        string $childClass,
        string $childColumn,
        string $childCollectionClass
    ): self {
        $this->relations[] = new RelationToChild(
            $this->getEntityClass(),
            $property,
            $childClass,
            $childColumn,
            $childCollectionClass
        );
        return $this;
    }

    protected function addRelationToParent(
        string $parentClass,
        string $parentProperty,
        string $column
    ): self {
        $this->relations[] = new RelationToParent($parentClass, $parentProperty, $this->getEntityClass(), $column);
        return $this;
    }

    public function getRelationToParent(string $parentClass): RelationToParent
    {
        foreach ($this->relations as $relation) {
            if (($relation instanceof RelationToParent)
                && ($this->classHelper->isInstanceOf($parentClass, $relation->getParentClass()))) {
                return $relation;
            }
        }

        throw new RelationToParentNotExists($parentClass);
    }

    /**
     * @throws RelationToChildrenNotExists
     */
    public function getRelationToChildren(string $childrenProperty)
    {
        foreach ($this->relations as $relation) {
            if (($relation instanceof RelationToChild) && $childrenProperty == $relation->getParentProperty()) {
                return $relation;
            }
        }
        throw new RelationToChildrenNotExists($childrenProperty);
    }

    public function hasRelationToChildren(string $childrenProperty): bool
    {
        try {
            $this->getRelationToChildren($childrenProperty);
        } catch (RelationToChildrenNotExists $e) {
            return false;
        }
        return true;
    }

    public function getRelationsToChildren(): array
    {
        $result = [];
        foreach ($this->relations as $relation) {
            if ($relation instanceof RelationToChild) {
                $result[] = $relation;
            }
        }
        return $result;
    }

    /**
     * @throws ClassDetector\Exception\TypeColumnNotExists
     * @throws ClassDetector\Exception\UnknownTypeException
     */
    public function detectEntityClass(array $data): string
    {
        if ($this->classDetector) {
            return $this->classDetector->detectClass($data);
        }
        return $this->getEntityClass();
    }

    /**
     * @throws ClassDetector\Exception\UnknownClassException
     */
    public function detectEntityType(string $entityClass)
    {
        if ($this->classDetector) {
            return $this->classDetector->detectType($entityClass);
        }

        return null;
    }

    protected function setClassDetector(array $types, $column = 'type'): self
    {
        $this->classDetector = $this->classDetectorFactory->createClassDetector($types, $column);
        return $this;
    }

    public function getTypeColumn(): ?string
    {
        if ($this->classDetector) {
            return $this->classDetector->getColumn();
        }
        return null;
    }

    public function isMultiClass(): bool
    {
        return null !== $this->classDetector;
    }

    abstract protected function getEntityClassName(): string;
}
