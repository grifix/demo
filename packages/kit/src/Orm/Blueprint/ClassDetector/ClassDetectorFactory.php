<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ClassDetectorFactory
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector
 */
class ClassDetectorFactory extends AbstractFactory implements ClassDetectorFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createClassDetector(array $types, string $column = 'type'): ClassDetectorInterface
    {
        $class = $this->makeClassName(ClassDetector::class);
        return new $class($types);
    }
}
