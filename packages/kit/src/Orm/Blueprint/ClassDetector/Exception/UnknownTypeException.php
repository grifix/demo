<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector\Exception;

/**
 * Class UnknownTypeException
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector\Exception
 */
class UnknownTypeException extends \Exception
{
    /**
     * @var string
     */
    protected $type;

    /**
     * UnknownTypeException constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        $this->type = $class;
        parent::__construct(sprintf('Unknown type "%s"', $class));
    }
}
