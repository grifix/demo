<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector;


/**
 * Class ClassDetectorFactory
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector
 */
interface ClassDetectorFactoryInterface
{
    /**
     * @param array $types
     * @param string $column
     * @return ClassDetectorInterface
     */
    public function createClassDetector(array $types, string $column = 'type'): ClassDetectorInterface;
}
