<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

use Grifix\Kit\Orm\Blueprint\Exception\BlueprintAlreadyRegisteredException;
use Grifix\Kit\Orm\Blueprint\Exception\EntityHasNoBlueprintException;

/**
 * Interface BlueprintMapInterface
 * @package Grifix\Kit\Orm\Blueprint
 */
interface BlueprintMapInterface
{
    /**
     * @param BlueprintInterface $blueprint
     * @throws BlueprintAlreadyRegisteredException
     */
    public function registerBlueprint(BlueprintInterface $blueprint);

    /**
     * @param string $entityClass
     * @return BlueprintInterface
     */
    public function getBlueprint(string $entityClass): BlueprintInterface;

    /**
     * @param string $entityClass
     * @return bool
     */
    public function hasBlueprint(string $entityClass): bool;
}