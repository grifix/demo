<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\PropertyAnalyzer;

use Grifix\Kit\Kernel\AbstractFactory;
use ReflectionProperty;

class PropertyAnalyzerFactory extends AbstractFactory implements PropertyAnalyzerFactoryInterface
{
    public function createPropertyAnalyzer(ReflectionProperty $property): PropertyAnalyzerInterface
    {
        $class = $this->makeClassName(PropertyAnalyzer::class);
        return new $class($property, $this->classMaker);
    }
}
