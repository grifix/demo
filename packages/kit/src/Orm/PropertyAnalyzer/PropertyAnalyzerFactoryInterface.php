<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\PropertyAnalyzer;

use ReflectionProperty;

interface PropertyAnalyzerFactoryInterface
{
    public function createPropertyAnalyzer(ReflectionProperty $property): PropertyAnalyzerInterface;
}
