<?php declare(strict_types = 1);

namespace Grifix\Kit\Orm\PropertyAnalyzer;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\AssocArray;
use ReflectionProperty;

interface PropertyAnalyzerInterface
{
    public function isDependency(): bool;

    public function isRelatedCollection(): bool;

    public function isCollection(): bool;

    public function isSimpleType(): bool;

    public function getType(): string;

    public function isFloatArray(): bool;

    public function isIntArray(): bool;

    public function isStringArray(): bool;

    public function isArray(): bool;

    public function isBool(): bool;

    public function isString(): bool;

    public function isFloat(): bool;

    public function isInt(): bool;

    public function getProperty(): ReflectionProperty;

    public function isNullable();

    public function getElementType(): ?string;

    public function isArrayOfObjects(): bool;
}
