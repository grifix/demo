<?php declare(strict_types=1);

namespace Grifix\Kit\Orm\PropertyAnalyzer;

use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\AssocArray;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Relation;
use ReflectionClass;
use ReflectionProperty;

class PropertyAnalyzer implements PropertyAnalyzerInterface
{

    protected const TYPE_STRING = 'string';
    protected const TYPE_FLOAT = 'float';
    protected const TYPE_INT = 'int';
    protected const TYPE_BOOL = 'bool';
    protected const TYPE_ARRAY_STRING = 'string[]';
    protected const TYPE_ARRAY_FLOAT = 'float[]';
    protected const TYPE_ARRAY_INT = 'int[]';
    protected const TYPE_ARRAY = 'array';
    protected const TYPE_ARRAY_SHORT = '[]';

    protected ReflectionProperty $property;

    protected ?string $type = null;

    protected ?bool $nullable = null;

    protected ClassMakerInterface $classMaker;

    public function __construct(ReflectionProperty $property, ClassMakerInterface $classMaker)
    {
        $this->property = $property;
        $this->classMaker = $classMaker;
    }

    public function isDependency(): bool
    {
        return count($this->property->getAttributes(Dependency::class)) > 0;
    }

    public function isRelatedCollection(): bool
    {
        return count($this->property->getAttributes(Relation::class)) > 0;
    }

    public function isCollection(): bool
    {
        return strpos($this->getType(), 'CollectionInterface') !== false;
    }

    public function isSimpleType(): bool
    {
        return in_array(
            $this->getType(),
            [
                self::TYPE_STRING,
                self::TYPE_FLOAT,
                self::TYPE_INT,
                self::TYPE_BOOL,
                self::TYPE_ARRAY,
                self::TYPE_ARRAY_SHORT,
                self::TYPE_ARRAY_STRING,
                self::TYPE_ARRAY_INT,
                self::TYPE_ARRAY_FLOAT
            ]
        );
    }

    public function isInt(): bool
    {
        return $this->getType() === self::TYPE_INT;
    }

    public function isFloat(): bool
    {
        return $this->getType() === self::TYPE_FLOAT;
    }

    public function isString(): bool
    {
        return $this->getType() === self::TYPE_STRING;
    }

    public function isBool(): bool
    {
        return $this->getType() === self::TYPE_BOOL;
    }

    public function isArray(): bool
    {
        return in_array($this->getType(), [
            self::TYPE_ARRAY,
            self::TYPE_ARRAY_SHORT,
            self::TYPE_ARRAY_STRING,
            self::TYPE_ARRAY_INT,
            self::TYPE_ARRAY_FLOAT
        ]);
    }

    public function isArrayOfObjects(): bool
    {
        return $this->isArray() && (class_exists($this->getElementType() ?? '') || interface_exists($this->getElementType() ?? ''));
    }

    public function isStringArray(): bool
    {
        return $this->getType() === self::TYPE_ARRAY_STRING;
    }

    public function isIntArray(): bool
    {
        return $this->getType() === self::TYPE_ARRAY_INT;
    }

    public function isFloatArray(): bool
    {
        return $this->getType() === self::TYPE_ARRAY_FLOAT;
    }

    public function isNullable()
    {
        $this->getType();
        return $this->nullable;
    }

    protected function detectTypeFromNamedType(\ReflectionNamedType $type): void
    {
        $this->nullable = $type->allowsNull();
        if ($type->isBuiltin()) {
            $this->type = $type->getName();
        } else {
            $this->type = $this->classMaker->makeClassName($type->getName());
        }
    }

    protected function detectTypeFromDocComment(): void
    {
        $matches = [];
        preg_match('/@var\s(\S*)/', $this->property->getDocComment(), $matches);
        if (!$matches || !isset($matches[1])) {
            $this->type = 'string';
        } else {
            $types = $matches[1];
            $types = explode('|', $types);
            $this->type = trim($types[0]);
            $this->nullable = in_array('null', $types);
        }

        if (strpos($this->type, '\\') === 0) {
            $reflection = new ReflectionClass($this->type);
            if ($reflection->isUserDefined()) {
                $this->type = $this->classMaker->makeClassName($this->type);
            }
            $this->type = substr($this->type, 1);
        }
    }

    public function getType(): string
    {
        if (!$this->type) {
            $type = $this->property->getType();
            if ($type instanceof \ReflectionNamedType) {
                $this->detectTypeFromNamedType($type);
            } elseif ($this->property->getDocComment()) {
                $this->detectTypeFromDocComment();
            } else {
                $this->type = 'string';
            }
        }

        return $this->type;
    }

    public function getProperty(): ReflectionProperty
    {
        return $this->property;
    }

    public function getElementType(): ?string
    {
        if ($this->isIntArray()) {
            return self::TYPE_INT;
        }
        if ($this->isFloatArray()) {
            return self::TYPE_FLOAT;
        }
        if ($this->isStringArray()) {
            return self::TYPE_STRING;
        }
        $doc = $this->getProperty()->getDocComment();
        if (!$doc) {
            return null;
        }
        $doc = trim(str_replace(['/** @var', '*/'], '', $doc));
        $doc = explode('|', $doc);
        foreach ($doc as $type) {
            if (false !== strpos($type, '[]')) {
                return trim(str_replace('[]', '', $type));
            }
        }
        return null;
    }

}
