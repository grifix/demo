<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Orm\EntityManager\Exception;

class OptimisticLockFailedException extends \Exception
{
    public function __construct(string $entityClass, string $entityId)
    {
        parent::__construct(sprintf('Optimistic lock failed for entity %s with id %s', $entityClass, $entityId));
    }
}
