<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Orm\EntityManager\EntityStateStorage;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Orm\EntityManager\EntityStateStorage\Exception\StateNotExistException;

class StateStorage implements StateStorageInterface
{
    /** @var array[] */
    protected array $states = [];

    protected ClassHelperInterface $classHelper;

    public function __construct(ClassHelperInterface $classHelper)
    {
        $this->classHelper = $classHelper;
    }

    public function setState(string $entityClass, string $entityId, array $state): void
    {
        $this->states[$this->makeKey($entityClass, $entityId)] = json_decode(json_encode($state), true);
    }

    public function getState(string $entityClass, string $entityId): array
    {
        $key = $this->makeKey($entityClass, $entityId);
        if ($this->hasState($entityClass, $entityId)) {
            return $this->states[$key];
        }
        throw new StateNotExistException($key);
    }

    public function hasState(string $entityClass, string $entityId): bool
    {
        $key = $this->makeKey($entityClass, $entityId);
        return array_key_exists($key, $this->states);
    }

    protected function makeKey(string $entityClass, string $entityId): string
    {
        return $this->classHelper->makeFullClassName($entityClass) . '_' . $entityId;
    }
}
