<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Orm\EntityManager\EntityStateStorage;

interface StateStorageInterface
{
    public function setState(string $entityClass, string $entityId, array $state): void;

    public function getState(string $entityClass, string $entityId): array;

    public function hasState(string $entityClass, string $entityId): bool;
}
