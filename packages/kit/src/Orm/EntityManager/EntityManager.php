<?php

declare(strict_types=1);

namespace Grifix\Kit\Orm\EntityManager;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ReflectionHelperInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintFactoryInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintMapInterface;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToParent;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\Collection\Injector\CollectionInjectorFactory;
use Grifix\Kit\Orm\Collection\Injector\CollectionInjectorInterface;
use Grifix\Kit\Orm\EntityManager\EntityStateStorage\StateStorageInterface;
use Grifix\Kit\Orm\EntityManager\Exception\OptimisticLockFailedException;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\IdentityMap\IdentityItemInterface;
use Grifix\Kit\Orm\IdentityMap\IdentityMapFactoryInterface;
use Grifix\Kit\Orm\IdentityMap\IdentityMapInterface;
use Grifix\Kit\Orm\Persister\PersisterFactoryInterface;
use Grifix\Kit\Orm\Persister\PersisterInterface;
use Grifix\Kit\Orm\RelationMap\RelationMapFactoryInterface;
use Grifix\Kit\Orm\RelationMap\RelationMapInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;
use Grifix\Kit\Orm\Serializer\SerializerFactoryInterface;
use Grifix\Kit\Orm\Validator\ValidatorInterface;

class EntityManager implements EntityManagerInterface
{
    protected IdentityMapInterface $identityMap;

    protected RelationMapInterface $relationMap;

    /**
     * @var ObjectSerializerInterface[]
     */
    protected array $serializers = [];

    /**
     * @var PersisterInterface[]
     */
    protected array $persisters = [];

    protected CollectionInjectorInterface $collectionInjector;

    protected bool $inTransaction = false;

    public function __construct(
        RelationMapFactoryInterface $relationMapFactory,
        IdentityMapFactoryInterface $identityMapFactory,
        protected BlueprintMapInterface $blueprintMap,
        protected ConnectionInterface $connection,
        protected BlueprintFactoryInterface $blueprintFactory,
        protected SerializerFactoryInterface $serializerFactory,
        protected PersisterFactoryInterface $persisterFactory,
        protected CollectionInjectorFactory $collectionInjectorFactory,
        protected ReflectionHelperInterface $reflectionHelper,
        protected ValidatorInterface $validator,
        protected StateStorageInterface $stateStorage,
        protected ArrayHelperInterface $arrayHelper,
        protected EventCollectorInterface $eventCollector
    ) {
        $this->identityMap = $identityMapFactory->create($this);
        $this->relationMap = $relationMapFactory->create($this);
        $this->collectionInjector = $collectionInjectorFactory->create($this);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityId($entity): string
    {
        return $this->getBlueprint(get_class($entity))->getId($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlueprint(string $entityClass): BlueprintInterface
    {
        return $this->blueprintMap->getBlueprint($entityClass);
    }

    /**
     * {@inheritdoc}
     */
    public function hasBlueprint(string $entityClass): bool
    {
        return $this->blueprintMap->hasBlueprint($entityClass);
    }

    /**
     * {@inheritdoc}
     */
    public function registerBlueprint(string $blueprintClass): EntityManagerInterface
    {
        $this->blueprintMap->registerBlueprint($this->blueprintFactory->create($blueprintClass));
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent($entity)
    {
        return $this->relationMap->getParent($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function hasParent($entity): bool
    {
        return $this->relationMap->hasParent($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren($entity): bool
    {
        return $this->relationMap->hasChildren($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function isNew($entity): bool
    {
        return $this->identityMap->isNew($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function setParent($entity, $parent): void
    {
        $this->relationMap->setParent($entity, $parent);
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($entity): array
    {
        $blueprint = $this->getBlueprint(get_class($entity));
        $result = $this->getSerializer(get_class($entity))->serialize($entity);
        if ($blueprint->getTypeColumn()) {
            $result[$blueprint->getTypeColumn()] = $blueprint->detectEntityType(get_class($entity));
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize(array $data, string $entityClass)
    {
        $this->validator->validate($entityClass, json_encode($data));
        $realClass = $this->getBlueprint($entityClass)->detectEntityClass($data);
        $result = $this->getSerializer($realClass)->unSerialize($data);
        $this->stateStorage->setState($realClass, $this->getEntityId($result), $data);
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity(string $entityClass, string $id)
    {
        return $this->identityMap->getEntity($entityClass, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasEntity(string $entityClass, string $id)
    {
        return $this->identityMap->hasEntityWithId($entityClass, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function injectQueryCollections($entity): void
    {
        $this->collectionInjector->injectQueryCollections($entity);
    }

    /**
     * @param string $entityClass
     * @return ObjectSerializerInterface
     */
    public function getSerializer(string $entityClass): ObjectSerializerInterface
    {
        if (!isset($this->serializers[$entityClass])) {
            $this->serializers[$entityClass] = $this->serializerFactory->createObjectSerializer(
                $this,
                $entityClass,
                $this->getBlueprint($entityClass)->getSerializerClass()
            );
        }
        return $this->serializers[$entityClass];
    }

    /**
     * @param string $entityClass
     * @return PersisterInterface
     */
    protected function getPersister(string $entityClass): PersisterInterface
    {
        if (!isset($this->persisters[$entityClass])) {
            $blueprint = $this->getBlueprint($entityClass);
            $this->persisters[$entityClass] = $this->persisterFactory->create(
                $blueprint->getPersisterClass(),
                $blueprint->getTable(),
                $this
            );
        }
        return $this->persisters[$entityClass];
    }

    /**
     * {@inheritdoc}
     */
    public function isDirty($entity): bool
    {
        return $this->identityMap->isDirty($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function isMarkedForDeletion($entity): bool
    {
        return $this->identityMap->isMarkedForDeletion($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function unsetParent($entity): void
    {
        $this->relationMap->unsetParent($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function watch($entity, bool $isNew = true): void
    {
        $this->injectQueryCollections($entity);
        $this->identityMap->addEntity($entity, $isNew);
        $entityClass = get_class($entity);
        $entityId = $this->getEntityId($entity);
        if (false === $this->stateStorage->hasState($entityClass, $entityId)) {
            $this->stateStorage->setState($entityClass, $entityId, $this->serialize($entity));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function commitChanges($entity): void
    {
        $this->identityMap->commitChanges($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getDiff($entity): array
    {
        return $this->identityMap->getDiff($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function persist($entity): void
    {
        $this->getPersister(get_class($entity))->persist($entity);
        $this->eventCollector->storeEvents($entity);
    }

    public function beginTransaction(): void
    {
        if (!$this->isInTransaction()) {
            $this->connection->beginTransaction();
        }
    }

    public function isInTransaction(): bool
    {
        return $this->connection->isInTransaction();
    }

    /**
     * {@inheritdoc}
     */
    public function commit(?object $entity = null): void
    {
        if (!$this->isInTransaction()) {
            $this->getConnection()->beginTransaction();
        }
        $that = $this;

        try {
            $needAction = function (IdentityItemInterface $item) use ($that, $entity) {
                return
                    $entity === null
                    || $entity === $item->getEntity()
                    || $that->areRelated($item->getEntity(), $entity);
            };

            $this->identityMap->visitNewItems(function (IdentityItemInterface $item) use ($that, $needAction) {
                if ($needAction($item)) {
                    $that->persist($item->getEntity());
                }
            });
            $this->identityMap->visitDirtyItems(function (IdentityItemInterface $item) use ($that, $needAction) {
                if ($needAction($item)) {
                    $entityId = $this->getEntityId($item->getEntity());
                    $entityClass = get_class($item->getEntity());
                    $blueprint = $this->getBlueprint($entityClass);
                    if ($blueprint->hasTransactionLock()) {
                        $originalState = $this->stateStorage->getState($entityClass, $entityId);
                        $currentDbState = $this->getCurrentDbState($item->getEntity());
                        $this->arrayHelper->ksort($originalState);
                        $this->arrayHelper->ksort($currentDbState);
                        if ($currentDbState && $originalState !== $currentDbState) {
                            $that->rollbackTransaction();
                            throw new OptimisticLockFailedException($entityClass, $entityId);
                        }
                    }
                    $that->persist($item->getEntity());
                }
            });

            $this->getConnection()->commitTransaction(true);
        } catch (\Throwable $exception) {
            $this->connection->rollBackTransaction();
            throw $exception;
        }


        $this->identityMap->visitAllItems(function (IdentityItemInterface $item) use ($that, $needAction) {
            if ($needAction($item)) {
                $that->eventCollector->publishEvents($item->getEntity());
            }
        });
    }

    public function rollbackTransaction(): void
    {
        $this->connection->rollBackTransaction();
    }

    /**
     * @param object $childEntity
     * @param object $parentEntity
     * @return bool
     * @throws \ReflectionException
     */
    protected function areRelated(object $childEntity, object $parentEntity)
    {
        $childBlueprint = $this->getBlueprint(get_class($childEntity));
        $parentBlueprint = $this->getBlueprint(get_class($parentEntity));
        if (!$childBlueprint->hasRelationTo($parentBlueprint->getEntityClass())) {
            return false;
        }
        $relation = $childBlueprint->getRelationTo($parentBlueprint->getEntityClass());
        if (!($relation instanceof RelationToParent)) {
            return false;
        }

        /**@var CollectionInterface $children */
        $children = $this->reflectionHelper->getPropertyValue($parentEntity, $relation->getParentProperty());
        foreach ($children as $child) {
            if ($child === $childEntity) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($entity): void
    {
        $this->identityMap->markForDeletion($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getConnection(): ConnectionInterface
    {
        return $this->connection;
    }

    public function __destruct()
    {
        if ($this->isInTransaction()) {
            $this->rollbackTransaction();
        }
    }

    protected function getCurrentDbState(object $entity): array
    {
        $blueprint = $this->getBlueprint(get_class($entity));
        $query = $this->connection->createQuery()
            ->select('*')->forUpdate()->from($blueprint->getTable())
            ->where('id = :id')
            ->bindValue('id', $this->getEntityId($entity));
        if ($blueprint->isTransactionLockPessimistic()) {
            $query->forUpdate();
        }

        $record = $query->fetchOne();
        if ($record) {
            return json_decode($record['data'], true);
        }

        return [];
    }
}
