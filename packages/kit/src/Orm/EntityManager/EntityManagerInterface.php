<?php
declare(strict_types = 1);

namespace Grifix\Kit\Orm\EntityManager;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;

/**
 * Interface EntityManager
 * @package Grifix\Kit\Orm
 */
interface EntityManagerInterface
{
    /**
     * @param $entity
     * @param bool $isNew
     * @internal
     */
    public function watch($entity, bool $isNew = true): void;

    /**
     * @param null|object $entity
     * @api
     */
    public function commit(?object $entity = null): void;

    /**
     * @param object $entity
     * @internal
     */
    public function delete($entity): void;

    /**
     * @param string $entityClass
     * @return BlueprintInterface
     * @internal
     */
    public function getBlueprint(string $entityClass): BlueprintInterface;

    /**
     * @param $entity
     * @return string
     * @internal
     */
    public function getEntityId($entity): string;

    /**
     * @param $entity
     * @return object
     * @throws \Orm2\RelationManager\Exception\EntityHasNoParentException
     * @internal
     */
    public function getParent($entity);

    /**
     * @param $entity
     * @return bool
     * @internal
     */
    public function hasParent($entity): bool;

    /**
     * @param object $entity
     * @param object $parent
     * @internal
     */
    public function setParent($entity, $parent): void;

    /**
     * @param object $entity
     * @internal
     */
    public function unsetParent($entity): void;

    /**
     * @return ConnectionInterface
     * @internal
     */
    public function getConnection(): ConnectionInterface;

    /**
     * @param string $blueprintClass
     * @return mixed
     * @api
     */
    public function registerBlueprint(string $blueprintClass): EntityManagerInterface;

    /**
     * @param object $entity
     * @return array
     * @internal
     */
    public function serialize($entity): array;

    /**
     * @param array $data
     * @param string $entityClass
     * @return object
     * @internal
     */
    public function unSerialize(array $data, string $entityClass);

    /**
     * @param object $entity
     * @return array
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     * @internal
     */
    public function getDiff($entity): array;

    /**
     * @param $entity
     * @return bool
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     * @internal
     */
    public function isNew($entity): bool;

    /**
     * @param object $entity
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     * @internal
     */
    public function commitChanges($entity): void;

    /**
     * @param $entity
     * @return bool
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     * @internal
     */
    public function isDirty($entity): bool;

    /**
     * @param object $entity
     * @internal
     */
    public function persist($entity): void;

    /**
     * @param object $entity
     * @return bool
     * @internal
     */
    public function hasChildren($entity): bool;

    /**
     * Injects QueryCollections instead of ArrayCollections or null
     * @param object $entity
     * @throws \ReflectionException
     * @internal
     */
    public function injectQueryCollections($entity): void;

    /**@param string $entityClass
     * @return bool
     * @internal
     */
    public function hasBlueprint(string $entityClass): bool;

    /**
     * @param object $entity
     * @return bool
     */
    public function isMarkedForDeletion($entity): bool;

    /**
     * @param string $entityClass
     * @return ObjectSerializerInterface
     * @internal
     */
    public function getSerializer(string $entityClass): ObjectSerializerInterface;

    /**
     * @param string $entityClass
     * @param string $id
     * @return object
     * @internal
     */
    public function getEntity(string $entityClass, string $id);

    /**
     * @param string $entityClass
     * @param string $id
     * @return bool
     * @internal
     */
    public function hasEntity(string $entityClass, string $id);

    /**
     * @internal
     */
    public function beginTransaction(): void;

    public function isInTransaction(): bool;

    public function rollbackTransaction(): void;
}
