<?php declare(strict_types = 1);

namespace Grifix\Kit\Session\Dto;

class CookieParamsDto
{
    /** @var int */
    protected $lifeTime;

    /** @var string */
    protected $path;

    /** @var string */
    protected $domain;

    /** @var bool */
    protected $secure;

    /** @var string */
    protected $sameSite;

    public function __construct(int $lifeTime, string $path, string $domain, bool $secure, string $sameSite)
    {
        $this->lifeTime = $lifeTime;
        $this->path = $path;
        $this->domain = $domain;
        $this->secure = $secure;
        $this->sameSite = $sameSite;
    }

    public function getLifeTime(): int
    {
        return $this->lifeTime;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function isSecure(): bool
    {
        return $this->secure;
    }

    public function isSameSite(): string
    {
        return $this->sameSite;
    }
}
