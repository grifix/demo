<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Normalizer;

interface NormalizerInterface
{
    public function normalize(object $object): array;

    public function denormalize(array $data): object;
}
