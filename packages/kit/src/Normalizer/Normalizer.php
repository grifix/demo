<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Normalizer;

use Grifix\Kit\Normalizer\Attribute\Stateless;
use Grifix\Kit\Normalizer\Exception\TypeNotDefinedException;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObjectFactoryInterface;

class Normalizer implements NormalizerInterface
{
    protected ReflectionObjectFactoryInterface $reflectionObjectFactory;

    public const TYPE_KEY = '__TYPE';

    public function __construct(ReflectionObjectFactoryInterface $reflectionObjectFactory)
    {
        $this->reflectionObjectFactory = $reflectionObjectFactory;
    }

    public function normalize(object $object): array
    {
        return $this->normalizeValue($object);
    }

    public function denormalize(array $data): object
    {
        if (false === array_key_exists(self::TYPE_KEY, $data)) {
            throw new TypeNotDefinedException();
        }
        return $this->denormalizeObject($data, $data[self::TYPE_KEY]);
    }

    protected function normalizeObject(object $object): array
    {
        $reflection = $this->reflectionObjectFactory->create($object);
        $result = [
            self::TYPE_KEY => get_class($object)
        ];
        foreach ($reflection->getReflectionObject()->getProperties() as $property) {
            if (false === $this->isPropertyStateful($property)) {
                continue;
            }
            $result[$property->getName()] = $this->normalizeValue($reflection->getPropertyValue($property->getName()));
        }
        return $result;
    }

    protected function isPropertyStateful(\ReflectionProperty $property): bool
    {
        if(0 !== count($property->getAttributes(Dependency::class))){
            return false;
        }
        if(0 !== count($property->getAttributes(Stateless::class))){
            return false;
        }
        return true;
    }

    /**
     * @return mixed
     */
    protected function normalizeValue($value)
    {
        if (is_object($value)) {
            return $this->normalizeObject($value);
        }
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->normalizeValue($v);
            }
            return $value;
        }
        return $value;
    }

    /**
     * @return mixed
     */
    protected function denormalizeValue($value)
    {
        if (is_array($value) && array_key_exists(self::TYPE_KEY, $value)) {
            return $this->denormalizeObject($value, $value[self::TYPE_KEY]);
        }
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->denormalizeValue($v);
            }
            return $value;
        }
        return $value;
    }

    protected function denormalizeObject(array $data, string $type): object
    {
        $result = (new \ReflectionClass($type))->newInstanceWithoutConstructor();
        $reflection = $this->reflectionObjectFactory->create($result);
        foreach ($reflection->getReflectionObject()->getProperties() as $property) {
            $reflection->setPropertyValue($property->getName(), $this->denormalizeValue($data[$property->getName()]));
        }
        return $result;
    }
}

