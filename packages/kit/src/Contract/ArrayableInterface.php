<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Contract;

/**
 * Class ArrayableInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Contract
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ArrayableInterface
{
    /**
     * @return array
     */
    public function toArray():array;
}
