<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Reflection\ReflectionObject;

use ReflectionMethod;

interface ReflectionObjectInterface
{
    public function getReflectionObject(): \ReflectionObject;

    public function getPropertyValue(string $propertyName);

    public function setPropertyValue(string $propertyName, $value): void;

    public function copyTo(object $toObject): void;

    public function toArray(): array;

    /**
     * @return ReflectionMethod[]
     */
    public function getPublicMethods(): array;
}
