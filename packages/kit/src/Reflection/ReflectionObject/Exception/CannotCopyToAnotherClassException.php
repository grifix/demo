<?php declare(strict_types = 1);

namespace Grifix\Kit\Reflection\ReflectionObject\Exception;

use Exception;

class CannotCopyToAnotherClassException extends Exception
{
    public function __construct(string $originSequenceClass, string $toClass)
    {
        parent::__construct(sprintf('Cannot copy from %s to %s', $originSequenceClass, $toClass));
    }
}
