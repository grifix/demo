<?php
declare(strict_types=1);

namespace Grifix\Kit\Reflection\ReflectionObject;

use Grifix\Kit\Kernel\AbstractFactory;

class ReflectionObjectFactory extends AbstractFactory implements ReflectionObjectFactoryInterface
{
    public function create(object $object): ReflectionObjectInterface
    {
        $class = $this->makeClassName(ReflectionObject::class);
        return new $class($object);
    }
}
