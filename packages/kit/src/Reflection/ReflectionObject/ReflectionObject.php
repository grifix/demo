<?php

declare(strict_types=1);

namespace Grifix\Kit\Reflection\ReflectionObject;

use Grifix\Kit\Reflection\ReflectionObject\Exception\CannotCopyToAnotherClassException;
use ReflectionMethod;
use ReflectionObject as BaseReflectionObject;

/**
 * Class ReflectionWrapper
 * @package Grifix\Kit\Reflection
 */
class ReflectionObject implements ReflectionObjectInterface
{
    /**
     * @var object
     */
    protected $object;

    /**
     * @var BaseReflectionObject
     */
    protected $reflectionObject;

    public function __construct(object $object)
    {
        $this->reflectionObject = new BaseReflectionObject($object);
        $this->object = $object;
    }

    public function getReflectionObject(): BaseReflectionObject
    {
        return $this->reflectionObject;
    }

    public function toArray(): array
    {
        $result = [];
        foreach ($this->reflectionObject->getProperties() as $property) {
            $value = $this->getPropertyValue($property->getName());
            if (is_object($value)) {
                $value = (new self($value))->toArray();
            }
            $result[$property->getName()] = $value;
        }
        return $result;
    }

    public function getPropertyValue(string $propertyName)
    {
        $objectAndProperty = $this->getObjectAndProperty($this->object, $propertyName);
        return $this->getValue($objectAndProperty[0], $objectAndProperty[1]);
    }

    protected function getValue($object, string $propertyName)
    {
        $reflection = $this->reflectionObject;
        if ($object !== $this->object) {
            $reflection = new BaseReflectionObject($object);
        }
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);
        $result = $property->getValue($object);
        $property->setAccessible(false);
        return $result;
    }

    public function setPropertyValue(string $propertyName, $value): void
    {
        $objectAndProperty = $this->getObjectAndProperty($this->object, $propertyName);
        $this->setValue($objectAndProperty[0], $objectAndProperty[1], $value);
    }

    private function setValue($object, string $propertyName, $value): void
    {
        $reflection = $this->reflectionObject;
        if ($object !== $this->object) {
            $reflection = new BaseReflectionObject($object);
        }
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);
        $property->setValue($object, $value);
        $property->setAccessible(false);
    }

    protected function getObjectAndProperty($object, string $propertyName): array
    {
        if (strpos($propertyName, '.') === false) {
            return [$object, $propertyName];
        }
        $propertyNameArray = explode('.', $propertyName);
        $childPropertyName = array_shift($propertyNameArray);
        $childObject = $this->getValue($object, $childPropertyName);
        return $this->getObjectAndProperty($childObject, implode('.', $propertyNameArray));
    }

    public function copyTo(object $toObject): void
    {
        if (get_class($toObject) !== get_class($this->object)) {
            throw new CannotCopyToAnotherClassException(get_class($this->object), get_class($toObject));
        }

        $toWrapper = new static($toObject);
        foreach ($this->getReflectionObject()->getProperties() as $property) {
            $toWrapper->setPropertyValue($property->getName(), $this->getPropertyValue($property->getName()));
        }
    }

    /**
     * @return ReflectionMethod[]
     */
    public function getPublicMethods(): array
    {
        return $this->reflectionObject->getMethods(ReflectionMethod::IS_PUBLIC);
    }
}
