<?php
declare(strict_types=1);

namespace Grifix\Kit\Reflection\ReflectionObject;

interface ReflectionObjectFactoryInterface
{
    public function create(object $object): ReflectionObjectInterface;
}
