<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Cli;

use Grifix\Kit\Cli\Exception\CommandFailedException;

class Cli implements CliInterface
{
    protected string $rootDir;

    protected string $environment;

    /**
     * @param string $rootDir <root_dir>
     * @param string $environment <environment>
     */
    public function __construct(string $rootDir, string $environment)
    {
        $this->rootDir = $rootDir;
        $this->environment = $environment;
    }

    public function executeCommand(string $command, ...$arguments): void
    {
        $result = 0;
        $output = '';
        exec(
            $this->createCommandString($command, ...$arguments),
            $output,
            $result
        );
        if (0 !== $result) {
            throw new CommandFailedException($output, $result);
        }
    }

    public function createCommandString(string $command, ...$arguments): string
    {
        if ($arguments) {
            $arguments = '"' . implode('" "', $arguments) . '"';
        } else {
            $arguments = '';
        }
        return sprintf(
            '%s/cli --no-interaction --env=%s  %s %s',
            $this->rootDir,
            $this->environment,
            $command,
            $arguments
        );
    }
}
