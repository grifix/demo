<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Cli;

use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Shared\Application\Query\GetFreeId\GetFreeUuidQuery;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

class AbstractCommand extends Command
{
    private IocContainerInterface $iocContainer;

    private LoggerInterface $logger;

    public function __construct(IocContainerInterface $iocContainer, string $name = null)
    {
        $this->iocContainer = $iocContainer;
        $this->logger = $iocContainer->get(LoggerInterface::class);
        $this->init();
        parent::__construct($name);
    }

    protected function init()
    {
    }

    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }

    protected function executeCommand(object $command): void
    {
        $this->getShared(CommandBusInterface::class)->execute($command);
    }

    protected function executeQuery(object $query)
    {
        return $this->getShared(QueryBusInterface::class)->execute($query);
    }

    protected function getFreeId(): string
    {
        return $this->executeQuery(new GetFreeUuidQuery());
    }

    protected function warning(string $message, OutputInterface $output): void
    {
        $output->writeln(sprintf('<comment>%s</comment>', $message));
        $this->logger->warning($message);
    }

    protected function info(string $message, OutputInterface $output): void
    {
        $output->writeln(sprintf('<info>%s</info>', $message));
        $this->logger->info($message);
    }
}
