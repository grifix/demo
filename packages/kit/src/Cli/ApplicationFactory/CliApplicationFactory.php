<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Cli\ApplicationFactory;


use Grifix\Kit\ClassFetcher\ClassFetcher\ClassFetcherInterface;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Cli\CommandFactory\CommandFactoryInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputOption;

class CliApplicationFactory implements CliApplicationFactoryInterface
{
    public function __construct(
        private CommandFactoryInterface $commandFactory,
        private ClassFetcherInterface $classFetcher
    )
    {
    }

    public function createApplication(): Application
    {
        $result = new Application();
        $result->setCatchExceptions(false);
        $result->getDefinition()->addOptions([
            new InputOption('--env', null, InputOption::VALUE_OPTIONAL, 'The environment to operate in.', 'dev'),
        ]);
        $result->addCommands($this->collectCommands());
        return $result;
    }

    private function collectCommands(): array
    {
        $result = [];
        $classes = $this->classFetcher
            ->fetchClasses(
                '\Ui\Cli\Command',
                AbstractCommand::class,
                true
            );
        foreach ($classes as $class) {
            $result[] = $this->commandFactory->create($class);
        }
        return $result;
    }
}
