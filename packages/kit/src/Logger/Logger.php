<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Logger;

use Psr\Log\AbstractLogger;
use function Webmozart\Assert\Tests\StaticAnalysis\false;

class Logger extends AbstractLogger
{

    /** @var string */
    protected string $logPath;

    protected bool $enabled;

    /**
     * @param string $logPath <cfg:grifix.kit.logger.logPath>
     * @param bool $enabled <cfg:grifix.kit.logger.enabled>
     */
    public function __construct(string $logPath, bool $enabled = true)
    {
        $this->logPath = $logPath;
        $this->enabled = $enabled;
    }

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = []): void
    {
        if (false === $this->enabled) {
            return;
        }
        if (count($context)) {
            $context = print_r($context, true);
        } else {
            $context = '';
        }
        $record = [
            '[' . $level . ']',
            date('Y-m-d H:i:s'),
            $message,
            $context
        ];
        file_put_contents($this->logPath, implode("\t", $record) . "\n", FILE_APPEND);
    }
}
