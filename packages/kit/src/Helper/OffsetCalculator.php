<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class OffsetCalculator
 * @package Grifix\Kit\Helper
 */
class OffsetCalculator implements OffsetCalculatorInterface
{
    /**
     * @param int|null $page
     * @param int|null $rows
     * @return int|null
     */
    public function calculateOffset(?int $page = null, ?int $rows = null): ?int
    {
        if (!$page || !$rows) {
            return null;
        }

        return $rows * $page - $rows;
    }
}
