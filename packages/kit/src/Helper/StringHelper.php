<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Helper;

/**
 * Class StringHelper
 *
 * @category Grifix
 * @package  Grifix\Utils\Str
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StringHelper implements StringHelperInterface
{
    /**
     * {@inheritdoc}
     */
    public function crop($string, $number)
    {
        if ($number > 0) {
            return mb_substr($string, $number, mb_strlen($string));
        } else {
            return mb_substr($string, 0, mb_strlen($string) + $number);
        }
    }

    /**
     * @param string $string
     * @return bool
     */
    public function isJson(string $string): bool
    {
        return is_array(json_decode($string, true));
    }

    public function ucFirst(string $string, string $encoding = 'UTF-8'): string
    {
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
}
