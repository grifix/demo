<?php


declare(strict_types=1);

namespace Grifix\Kit\Helper\FilesystemHelper;

use Grifix\Kit\Helper\FilesystemHelper\Exception\CantWriteToFileException;
use Symfony\Component\Filesystem\Filesystem;

class FilesystemHelper implements FilesystemHelperInterface
{

    protected Filesystem $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function scanDir(
        string $dir,
        bool $recursive = false,
        int $order = SCANDIR_SORT_ASCENDING,
        array &$result = []
    ) {
        $dir = $this->replaceSeparator($dir);
        $items = scandir($dir, $order);

        foreach ($items as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            $path = $dir . DIRECTORY_SEPARATOR . $item;
            $type = self::TYPE_FILE;
            if (is_dir($path)) {
                $type = self::TYPE_DIR;
            }
            $result[] = array_merge(['type' => $type, 'path' => $path], pathinfo($path));
            if ($type === self::TYPE_DIR && $recursive !== false) {
                $result = $this->scanDir($path, $recursive, $order, $result);
            }
        }

        return $result;
    }

    public function replaceSeparator(string $path): string
    {
        return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
    }

    public function deleteDir($dir)
    {
        $dir = $this->replaceSeparator($dir);
        if (!is_dir($dir)) {
            return false;
        }
        $dh = opendir($dir);
        /** @noinspection PhpAssignmentInConditionInspection */
        while ($item = readdir($dh)) {
            if ($item != '..' && $item != '.') {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $item)) {
                    self::deleteDir($dir . DIRECTORY_SEPARATOR . $item);
                } else {
                    unlink($dir . DIRECTORY_SEPARATOR . $item);
                }
            }
        }
        closedir($dh);

        return rmdir($dir);
    }

    public function writeToFile(
        string $path,
        string $content,
        int $perms = 0664,
        int $dirPerms = 0750
    ) {
        return $this->doWriteToFile($path, $content, 'w', $perms, $dirPerms);
    }

    protected function doWriteToFile(
        string $path,
        string $content,
        string $mode = 'a',
        int $perms = 0664,
        int $dirPerms = 0750
    ) {
        $path = $this->replaceSeparator($path);
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        if (!is_dir($dir)) {
            mkdir($dir, $dirPerms, true);
        }
        $fp = fopen($path, $mode);
        flock($fp, 2);
        $result = fwrite($fp, $content);

        if ($result === false) {
            throw new CantWriteToFileException($path);
        }
        flock($fp, 3);
        fclose($fp);
        chmod($path, $perms);

        return $result;
    }

    public function appendToFile(string $path, string $content, int $perms = 0664, int $dirPerms = 0750)
    {
        return $this->doWriteToFile($path, $content, 'a', $perms, $dirPerms);
    }

    public function readFromFile(string $path): string
    {
        return file_get_contents($this->replaceSeparator($path));
    }


    public function fileExists(string $path): bool
    {
        return file_exists($this->replaceSeparator($path));
    }

    public function isDir(string $path): bool
    {
        return is_dir($this->replaceSeparator($path));
    }

    public function makeDir(string $path, int $mode = 0777, bool $recursive = true): bool
    {
        return mkdir($path, $mode, $recursive);
    }

    public function copyDir(string $originDir, string $targetDir): void
    {
        $originDir = $this->replaceSeparator($originDir);
        $targetDir = $this->replaceSeparator($targetDir);
        $this->filesystem->mirror($originDir, $targetDir);
    }

    public function getHash(string $path): string
    {
        if (!is_dir($path)) {
            return md5_file($path);
        }

        $result = [];
        $dir = dir($path);
        while (false !== ($entry = $dir->read())) {
            if ($entry !== '.' && $entry !== '..') {
                $result[] = $this->getHash($path . '/' . $entry);
            }
        }
        $dir->close();
        return md5(implode('', $result));
    }

    public function getPathInfo(string $path): PathInfo
    {
        $info = pathinfo($path);
        return new PathInfo(
            $info['dirname'],
            $info['basename'],
            $info['extension'] ?? null,
            $info['filename']
        );
    }
}
