<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Helper\FilesystemHelper;

class PathInfo
{
    private string $dirname;

    private string $basename;

    private ?string $extension;

    private string $filename;

    public function __construct(string $dirname, string $basename, ?string $extension, string $filename)
    {
        $this->dirname = $dirname;
        $this->basename = $basename;
        $this->extension = $extension;
        $this->filename = $filename;
    }

    public function getDirname(): string
    {
        return $this->dirname;
    }

    public function getBasename(): string
    {
        return $this->basename;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }
}
