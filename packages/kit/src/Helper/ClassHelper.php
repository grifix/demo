<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Helper;

use HaydenPierce\ClassFinder\ClassFinder;
use HaydenPierce\ClassFinder\Exception\ClassFinderException;

class ClassHelper implements ClassHelperInterface
{

    public function isInstanceOf(string $classA, string $classB): bool
    {
        $classA = $this->makeFullClassName($classA);
        $classB = $this->makeFullClassName($classB);

        if ($classB === $classA) {
            return true;
        }
        $reflection = new \ReflectionClass($classA);

        if ($reflection->isInstantiable()) {
            return $reflection->newInstanceWithoutConstructor() instanceof $classB;
        }
        if ($reflection->isSubclassOf($classB)) {
            return true;
        }
        if ($reflection->implementsInterface($classB)) {
            return true;
        }

        return false;
    }

    public function makeFullClassName(string $className): string
    {
        if (strpos($className, '\\') !== 0) {
            $className = '\\' . $className;
        }
        return $className;
    }

    public function makeShortClassName(string $className): string
    {
        if (strpos($className, '\\') === 0) {
            $className = substr($className, 1);
        }
        return $className;
    }

    public function fetchClasses(string $namespace): array
    {
        $result = [];
        try {
            $result = array_merge(
                $result,
                ClassFinder::getClassesInNamespace($namespace, ClassFinder::RECURSIVE_MODE)
            );
        } catch (ClassFinderException) {
        }
        try {
            $result = array_merge(
                $result,
                ClassFinder::getClassesInNamespace($this->makeAppClass($namespace), ClassFinder::RECURSIVE_MODE)
            );
        } catch (ClassFinderException) {
        }
        return $result;
    }

    public function makeAppClass(string $class): string
    {
        return sprintf('App\\%s', $class);
    }
}
