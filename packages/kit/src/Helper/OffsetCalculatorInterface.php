<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class OffsetCalculator
 * @package Grifix\Kit\Helper
 */
interface OffsetCalculatorInterface
{
    /**
     * @param int|null $page
     * @param int|null $rows
     * @return int|null
     */
    public function calculateOffset(?int $page = null, ?int $rows = null): ?int;
}
