<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Helper;

use Grifix\Kit\Helper\Exception\InvalidAmountException;
use Grifix\Kit\Validation\Validator\MoneyValidator;

class MoneyHelper implements MoneyHelperInterface
{
    public function __construct(protected MoneyValidator $moneyValidator)
    {
    }

    public function toInt(string $amount): int
    {
        if(!$this->moneyValidator->validate($amount)){
            throw new InvalidAmountException($amount);
        }
        $amount = str_replace(',', '.', $amount);
        return (int)((float) $amount * 100);
    }
}
