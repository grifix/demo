<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Helper;

use SuperClosure\SerializableClosure;

/**
 * Class ClosureHelper
 *
 * @category Grifix
 * @package  Grifix\Utils\Closure
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ClosureHelper implements ClosureHelperInterface
{
    /**
     * {@inheritdoc}
     */
    public function serializeArray(array $array): array
    {
        array_walk_recursive($array, function (&$value) {
            if ($value instanceof \Closure) {
                $value = new SerializableClosure($value);
            }
        });
        
        return $array;
    }
    
    /**
     * {@inheritdoc}
     */
    public function unSerializeArray(array $array): array
    {
        array_walk_recursive($array, function (&$value) {
            if ($value instanceof SerializableClosure) {
                $value = $value->getClosure();
            }
        });
        
        return $array;
    }
}
