<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

use Symfony\Component\Mime\MimeTypes;

class MimeHelper implements MimeHelperInterface
{
    public const PDF = 'application/pdf';
    public const DOC = 'application/msword';
    public const CSV = 'text/csv';
    public const TXT = 'text/plain';

    private MimeTypes $mimeTypes;

    public function __construct()
    {
        $this->mimeTypes = new MimeTypes();
    }

    public function getMimeType(string $extension): string
    {
        return $this->mimeTypes->getMimeTypes($extension)[0];
    }

    public function getExtension(string $mimeType): string
    {
        return $this->mimeTypes->getExtensions($mimeType)[0];
    }
}
