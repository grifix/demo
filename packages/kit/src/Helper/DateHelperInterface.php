<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Helper;

/**
 * Class DateHelper
 *
 * @category Grifix
 * @package  Grifix\Utils\Date
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface DateHelperInterface
{
    const MINUTE = 60;
    const HOUR = 60 * self::MINUTE;
    const DAY = 24 * self::HOUR;
    const MONTH = 30 * self::DAY;
    const YEAR = 12 * self::MONTH;
    
    /**
     * @param \DateInterval $dateInterval
     *
     * @return int
     */
    public function dateIntervalToSeconds(\DateInterval $dateInterval);
}
