<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

use HaydenPierce\ClassFinder\ClassFinder;

/**
 * Class ObjectHelper
 * @package Grifix\Kit\Helper
 */
interface ClassHelperInterface
{
    /**
     * @param string $classA
     * @param string $classB
     * @return bool
     */
    public function isInstanceOf(string $classA, string $classB): bool;

    /**
     * @param string $className
     * @return string
     */
    public function makeFullClassName(string $className): string;

    public function fetchClasses(string $namespace): array;

    public function makeAppClass(string $class): string;

    public function makeShortClassName(string $className): string;
}
