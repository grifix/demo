<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Helper\ArrayContains\Diff;

class ValueIsNotEqualDiff extends AbstractDiff
{
    /** @var mixed */
    protected $needleValue;

    /** @var mixed */
    protected $haystackValue;

    public function __construct(string $path, $needleValue, $haystackValue)
    {
        parent::__construct($path);
        $this->needleValue = $needleValue;
        $this->haystackValue = $haystackValue;
    }

    public function getNeedleValue()
    {
        return $this->needleValue;
    }

    public function getHaystackValue()
    {
        return $this->haystackValue;
    }
}
