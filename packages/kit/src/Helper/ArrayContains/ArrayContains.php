<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Helper\ArrayContains;

use Grifix\Kit\Helper\ArrayContains\Diff\AbstractDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\KeyDoesNotExistDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\ValueIsNotArrayDiff;
use Grifix\Kit\Helper\ArrayContains\Diff\ValueIsNotEqualDiff;

class ArrayContains
{
    /**
     * @return true|AbstractDiff[]
     */
    public function __invoke(array $haystack, array $needle)
    {
        $diffs = [];
        $this->contains($haystack, $needle, $diffs);
        if (count($diffs)) {
            return $diffs;
        }
        return true;
    }

    private function contains(array $haystack, array $needle, array &$diffs, ?string $path = null): void
    {
        foreach ($needle as $key => $value) {
            if ($path) {
                $keyPath = $path . '.' . $key;
            } else {
                $keyPath = (string) $key;
            }

            if (false === array_key_exists($key, $haystack)) {
                $diffs[] = new KeyDoesNotExistDiff($keyPath);
                continue;
            }
            if (is_array($value) && !is_array($haystack[$key])) {
                $diffs[] = new ValueIsNotArrayDiff($keyPath);
                continue;
            }

            if (is_array($value)) {
                $this->contains($haystack[$key], $value, $diffs, $keyPath);
                continue;
            }

            if ($value !== $haystack[$key]) {
                $diffs[] = new ValueIsNotEqualDiff($keyPath, $value, $haystack[$key]);
                continue;
            }
        }
    }
}
