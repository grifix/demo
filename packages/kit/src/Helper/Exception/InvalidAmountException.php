<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Helper\Exception;

use InvalidArgumentException;
use Throwable;

class InvalidAmountException extends InvalidArgumentException
{
    public function __construct(string $amount)
    {
        parent::__construct(sprintf('Invalid amount "%s"!', $amount));
    }
}
