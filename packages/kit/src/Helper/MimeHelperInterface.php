<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

interface MimeHelperInterface
{
    public function getMimeType(string $extension): string;

    public function getExtension(string $mimeType): string;
}
