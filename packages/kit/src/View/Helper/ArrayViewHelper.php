<?php declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Helper\ArrayHelperInterface;

class ArrayViewHelper extends AbstractViewHelper
{
    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    protected function init(): void
    {
        $this->arrayHelper = $this->getShared(ArrayHelperInterface::class);
        parent::init();
    }

    public function toArray($value): array
    {
        return $this->arrayHelper->toArray($value);
    }
}
