<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\View\Input\InputBuilderFactoryInterface;
use Grifix\Kit\View\Input\InputBuilderInterface;
use Grifix\Kit\View\ViewInterface;

/**
 * Class ViewHelper
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InputViewHelper extends AbstractViewHelper
{
    /**
     * @var InputBuilderFactoryInterface
     */
    protected $inputBuilderFactory;

    protected function init(): void
    {
        $this->inputBuilderFactory = $this->getShared(InputBuilderFactoryInterface::class);
        parent::init();
    }

    public function input(string $class): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createInputBuilder($class, $this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function text(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createTextInputBuilder($this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function password(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createPasswordInputBuilder($this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function email(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createEmailInputBuilder($this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function file(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createFileInputBuilder($this->view);
    }
}
