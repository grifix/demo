<?php declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\View\ViewInterface;

abstract class AbstractViewHelper
{
    /**
     * @var ViewInterface
     */
    protected $view;


    /** @var IocContainerInterface */
    private $iocContainer;

    public function __construct(
        ViewInterface $view,
        IocContainerInterface $iocContainer
    ) {
        $this->view = $view;
        $this->iocContainer = $iocContainer;
        $this->init();
    }

    protected function init(): void
    {
    }

    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }
}
