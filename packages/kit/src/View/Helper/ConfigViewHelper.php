<?php declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Config\ConfigInterface;

class ConfigViewHelper extends AbstractViewHelper
{
    public function getConfig(string $path, $defaultValue = null)
    {
        return $this->getShared(ConfigInterface::class)->get($path, $defaultValue);
    }
}
