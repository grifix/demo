<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\View\ViewInterface;

class ViewHelperFactory extends AbstractFactory implements ViewHelperFactoryInterface
{
    /** @var IocContainerInterface */
    protected $iocContainer;

    public function __construct(ClassMakerInterface $classMaker, IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
        parent::__construct($classMaker);
    }

    public function create(ViewInterface $view, string $helperClass)
    {
        $class = $this->makeClassName($helperClass);
        
        return new $class($view, $this->iocContainer);
    }
}
