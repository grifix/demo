<?php declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Helper\StringHelperInterface;

class StringViewHelper extends AbstractViewHelper
{
    public function ucFirst(string $string): string
    {
        return $this->getShared(StringHelperInterface::class)->ucFirst($string);
    }
}
