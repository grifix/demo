<?php declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Kernel\KernelInterface;

class ModuleViewHelper extends AbstractViewHelper
{
    public function hasModule(string $vendor, string $module): bool
    {
        return $this->getShared(KernelInterface::class)->hasModule($vendor, $module);
    }

    public function hasModuleCss(string $vendor, string $module, string $path):array {

    }
}
