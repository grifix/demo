<?php
declare(strict_types=1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\View\ViewInterface;

interface ViewHelperFactoryInterface
{
    public function create(ViewInterface $view, string $helperClass);
}
