<?php declare(strict_types=1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Route\UrlMaker\UrlMakerInterface;

class UrlViewHelper extends AbstractViewHelper
{
    public function makeActionUrl(string $action, string $view): string
    {
        return $this->getShared(UrlMakerInterface::class)->makeUrl(
            'grifix.shared.root.action',
            [
                'action' => $action,
                'view' => $view
            ]
        );
    }

    public function makeUrl(string $path): string
    {
        return $this->getShared(ConfigInterface::class)->get('grifix.kit.http.host') . $path;
    }

    public function makeRouteUrl(string $routeAlias, array $params = []): string
    {
        return $this->getShared(UrlMakerInterface::class)->makeUrl($routeAlias, $params);
    }
}
