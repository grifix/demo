<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\View\Helper\LocaleViewHelper\Exception;

class ThereIsNoTranslationException extends \InvalidArgumentException
{
    public function __construct(string $langCode)
    {
        parent::__construct(sprintf('There is no translation for %s lang', $langCode));
    }
}
