<?php declare(strict_types=1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Kit\View\Helper\LocaleViewHelper;

use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\Locale\LocaleInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\View\Helper\AbstractViewHelper;
use Grifix\Kit\View\Helper\LocaleViewHelper\Exception\ThereIsNoTranslationException;

class LocaleViewHelper extends AbstractViewHelper
{
    public function getCurrentLangCode(): string
    {
        return $this->getShared(TranslatorInterface::class)->getCurrentLang()->getCode();
    }

    /**
     * @return LangInterface[]
     */
    public function getLangs(): array
    {
        return $this->getShared(TranslatorInterface::class)->getLangs();
    }

    public function getCurrentLocaleCode(): string
    {
        return $this->getShared(LocaleInterface::class)->getCode();
    }

    public function getLocalValue(array $var): string
    {
        if (!array_key_exists($this->getCurrentLangCode(), $var)) {
            throw new ThereIsNoTranslationException($this->getCurrentLangCode());
        }
        return $var[$this->getCurrentLangCode()];
    }
}
