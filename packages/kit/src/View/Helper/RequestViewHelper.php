<?php declare(strict_types = 1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Http\ServerRequestInterface;

class RequestViewHelper extends AbstractViewHelper
{
    public function getRelativeUri(): string
    {
        return $this->getShared(ServerRequestInterface::class)->getRelativeUri();
    }
}
