<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Input;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\View\ViewInterface;

/**
 * Class InputBuilderFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Input
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InputBuilderFactory extends AbstractFactory implements InputBuilderFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createTextInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(TextInputBuilder::class, $view);
    }

    public function createPasswordInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(PasswordInputBuilder::class, $view);
    }

    public function createEmailInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(EmailInputBuilder::class, $view);
    }

    public function createFileInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(FileInputBuilder::class, $view);
    }

    public function createInputBuilder(string $class, ViewInterface $view): InputBuilderInterface
    {
        $class = $this->makeClassName($class);

        return new $class($view);
    }

}
