<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Input;

use Grifix\Kit\View\ViewInterface;

interface InputBuilderFactoryInterface
{
    public function createInputBuilder(string $class, ViewInterface $view): InputBuilderInterface;

    public function createFileInputBuilder(ViewInterface $view): InputBuilderInterface;

    public function createEmailInputBuilder(ViewInterface $view): InputBuilderInterface;

    /**
     * {@inheritdoc}
     */
    public function createPasswordInputBuilder(ViewInterface $view): InputBuilderInterface;

    /**
     * {@inheritdoc}
     */
    public function createTextInputBuilder(ViewInterface $view): InputBuilderInterface;
}
