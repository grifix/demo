<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Skin;

/**
 * Interface SkinInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Skin
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SkinInterface
{
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @return SkinInterface[]
     */
    public function getParentSkins(): array;
    
    /**
     * @param bool $withSelfName
     *
     * @return array
     */
    public function getParentSkinsNames($withSelfName = true): array;
    
    /**
     * @return bool
     */
    public function isPersistent(): bool;
    
    /**
     * @return SkinInterface|null
     */
    public function getParentSkin();
    
    /**
     * @return string
     */
    public function getContentType(): string;
}
