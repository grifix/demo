<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Skin;

/**
 * Class Skin
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Skin
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Skin implements SkinInterface
{
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var string
     */
    protected $contentType;
    
    /**
     * @var SkinInterface|null
     */
    protected $parentSkin;
    
    /**
     * @var SkinInterface[]
     */
    protected $parentSkins;
    
    /**
     * @var bool is skin persistent between requests
     */
    protected $persistent = false;
    
    /**
     * Skin constructor.
     *
     * @param string        $name
     * @param SkinInterface $parentSkin
     * @param bool          $persistent
     * @param string        $contentType
     */
    public function __construct(
        string $name,
        SkinInterface $parentSkin = null,
        bool $persistent = false,
        string $contentType = null
    ) {
        $this->name = $name;
        $this->parentSkin = $parentSkin;
        $this->persistent = $persistent;
        $this->contentType = $contentType;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParentSkin()
    {
        return $this->parentSkin;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getContentType(): string
    {
        if (is_null($this->contentType) && $this->getParentSkin()) {
            return $this->getParentSkin()->getContentType();
        }
        
        return $this->contentType;
    }
    
    /**
     *{@inheritdoc}
     */
    public function isPersistent(): bool
    {
        return $this->persistent;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParentSkinsNames($withSelfName = true): array
    {
        $result = [];
        $parentSkins = $this->getParentSkins();
        foreach ($parentSkins as $skin) {
            $result[] = $skin->getName();
        }
        if ($withSelfName) {
            $result[] = $this->getName();
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParentSkins(): array
    {
        if (is_null($this->parentSkins)) {
            $this->parentSkins = [];
            $current = $this;
            /** @noinspection PhpAssignmentInConditionInspection */
            while ($parent = $current->getParentSkin()) {
                $this->parentSkins[] = $parent;
                $current = $parent;
            }
            $this->parentSkins = array_reverse($this->parentSkins);
        }
        
        return $this->parentSkins;
    }
}
