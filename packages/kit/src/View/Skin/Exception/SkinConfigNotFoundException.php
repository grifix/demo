<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Skin\Exception;

class SkinConfigNotFoundException extends \Exception
{
    protected string $skinName;

    public function __construct(string $skinName, \Exception $previous = null)
    {
        $this->skinName = $skinName;
        parent::__construct('Config for skin "' . $skinName . '" was not found!', 0, $previous);
    }
}
