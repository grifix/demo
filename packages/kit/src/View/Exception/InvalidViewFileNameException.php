<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Exception;

use Grifix\Kit\View\ViewInterface;

/**
 * Class InvalidViewFileNameException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidViewFileNameException extends \Exception
{
    protected $path;
    
    /**
     * InvalidViewFileNameException constructor.
     *
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
        $this->message = 'View "'.$path.'" has invalid file name,'.
            'file name must begins from "'.ViewInterface::PREFIX_LAYOUT.'."'.
            'or "'.ViewInterface::PREFIX_TEMPLATE.'." or "'.ViewInterface::PREFIX_PARTIAL.'."!'
        ;
        parent::__construct();
    }
}