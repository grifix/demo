<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\View\Renderer;

use Grifix\Kit\View\Renderer\Exception\UnknownContentTypeException;


/**
 * Class RendererFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Renderer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RendererFactoryInterface
{
    /**
     * @param string $contentType
     *
     * @return RendererInterface
     * @throws UnknownContentTypeException
     */
    public function createRenderer(string $contentType): RendererInterface;
}