<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Renderer\Exception;

/**
 * Class UnknownContentTypeException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Renderer\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UnknownContentTypeException extends \Exception
{
    /**
     * @var string
     */
    protected $contentType;
    
    /**
     * UnknownContentTypeException constructor.
     *
     * @param string $contentClass
     */
    public function __construct(string $contentClass)
    {
        $this->contentType = $contentClass;
        $this->message = 'Unknown content type "'.$this->contentType.'". There is no renderer for this type!';
    }
}