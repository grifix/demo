<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Renderer;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\View\Renderer\Exception\UnknownContentTypeException;

/**
 * Class RendererFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Renderer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RendererFactory extends AbstractFactory implements RendererFactoryInterface
{
    /**
     * @param string $contentType
     *
     * @return RendererInterface
     * @throws UnknownContentTypeException
     */
    public function createRenderer(string $contentType):RendererInterface
    {
        switch ($contentType) {
            case 'html':
                $className = $this->makeClassName(HtmlRenderer::class);
                
                return new $className();
                break;
            case 'json':
                $className = $this->makeClassName(JsonRenderer::class);
                
                return new $className();
                break;
            default:
                throw new UnknownContentTypeException($contentType);
        }
    }
}