<?php
declare(strict_types=1);

namespace Grifix\Kit\View\Event;

/**
 * Class BeforeCreateViewEvent
 * @package Grifix\Kit\View\Event
 */
class BeforeCreateViewEvent
{
    /**
     * @var string
     */
    protected $path;

    /**
     * BeforeCreateViewEvent constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
