<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\View\Asset;


/**
 * Class AssetCombiner
 *
 * @category Grifix
 * @package  Grifix\Kit\Asset
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface AssetCombinerInterface
{
    /**
     * @param string $path
     * @param string $extension
     *
     * @return AssetCombinerInterface
     */
    public function addAsset(string $path, string $extension = null): AssetCombinerInterface;
    
    /**
     * @param string $path
     *
     * @return AssetCombinerInterface
     */
    public function excludePath(string $path): AssetCombinerInterface;
    
    /**
     * @return string
     */
    public function combine(): string ;
    
    /**
     * @param string $path
     *
     * @return int
     */
    public function combineToFile(string $path): int;
    
    /**
     * @param string $path
     *
     * @return AssetCombinerInterface
     */
    public function addPath(string $path): AssetCombinerInterface;
    
    /**
     * @return array
     */
    public function getPaths(): array;
}