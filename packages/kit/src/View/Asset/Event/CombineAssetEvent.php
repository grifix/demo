<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Asset\Event;

/**
 * Class CombineAssetEvent
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Asset\Event
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CombineAssetEvent
{
    
    /**
     * @var string
     */
    protected $assetPath;
    
    /**
     * @var string
     */
    protected $assetContent;

    /**
     * CombineAssetEvent constructor.
     *
     * @param string $assetPath
     * @param string $assetContent
     */
    public function __construct(string $assetPath, string $assetContent)
    {
        $this->assetPath = $assetPath;
        $this->assetContent = $assetContent;
    }
    
    /**
     * @return string
     */
    public function getAssetPath(): string
    {
        return $this->assetPath;
    }
    
    /**
     * @return string
     */
    public function getAssetContent(): string
    {
        return $this->assetContent;
    }
    
    /**
     * @param $assetContent
     *
     * @return void
     */
    public function setAssetContent($assetContent)
    {
        $this->assetContent = $assetContent;
    }
}
