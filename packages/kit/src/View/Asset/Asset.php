<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Asset;

use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\View\Asset\Exception\NoRealPathsException;
use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Class Asset
 *
 * @category Grifix
 * @package  Grifix\Kit\View\AssetCombiner
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Asset implements AssetInterface
{
    const MODE_BY_SKIN = 1;
    const MODE_BY_VENDOR = 2;
    const MODE_BY_SKIN_AND_VENDOR = 3;
    const MODE_NOT_EXTEND = 4;

    /**
     * @var string
     */
    protected $virtualPath;

    /**
     * @var SkinInterface
     */
    protected $skin;

    /**
     * @var int
     */
    protected $extendMode;

    /**
     * @var \Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var array
     */
    protected $realPaths = [];

    /**
     * @var string
     */
    protected $extension;

    /** @var string[] */
    protected $modules;

    /**
     * Asset constructor.
     *
     * @param string $path
     * @param SkinInterface $skin
     * @param \Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface $filesystemHelper
     * @param string $extension расширение файлов если $path указывает на папку
     */
    public function __construct(
        string $path,
        SkinInterface $skin,
        FilesystemHelperInterface $filesystemHelper,
        array $modules,
        string $extension = null
    ) {
        $this->virtualPath = $path;
        $this->skin = $skin;
        $this->extension = $extension;
        $this->extendMode = self::MODE_NOT_EXTEND;
        $this->filesystemHelper = $filesystemHelper;
        $this->modules = $modules;
        if (strpos($this->virtualPath, '{skin}')) {
            $this->extendMode = self::MODE_BY_SKIN;
        }
        if (strpos($this->virtualPath, '{src}')) {
            $this->extendMode = self::MODE_BY_VENDOR;
        }
        if (strpos($this->virtualPath, '{src}') && strpos($this->virtualPath, '{skin}')) {
            $this->extendMode = self::MODE_BY_SKIN_AND_VENDOR;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVirtualPath(): string
    {
        return $this->virtualPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getRealPaths(): array
    {
        if (!$this->realPaths) {
            foreach ($this->modules as $module){
                if ($this->extendMode == self::MODE_BY_SKIN_AND_VENDOR) {
                    $this->realPaths = array_merge(
                        $this->realPaths,
                        $this->findPathsForSkinAndVendor($this->virtualPath, $module)
                    );
                } elseif ($this->extendMode == self::MODE_BY_SKIN) {
                    $this->realPaths = array_merge(
                        $this->realPaths,
                        $this->findPathsForSkin($this->virtualPath, $module)
                    );
                } elseif ($this->extendMode == self::MODE_BY_VENDOR) {
                    $this->realPaths = array_merge(
                        $this->realPaths,
                        $this->findPathsForVendor($this->virtualPath, $module)
                    );
                } elseif ($this->extendMode == self::MODE_NOT_EXTEND) {
                    $this->realPaths[] = $this->findRealPath($this->virtualPath, $module);
                }
            }
            $this->realPaths[] = $this->findRealPath($this->virtualPath);


            $this->realPaths = array_unique(array_filter($this->realPaths));


            if (!$this->realPaths) {
                throw new NoRealPathsException($this);
            }

            foreach ($this->realPaths as $i => &$realPath) {
                if (is_dir($realPath)) {
                    unset($this->realPaths[$i]);
                    $this->realPaths = array_merge($this->realPaths, $this->scanDir($realPath));
                }
            }
        }

        return $this->realPaths;
    }

    protected function hasModule(string $virtualPath, string $module): bool {
        return
            false !== strpos($virtualPath, '{module}')
            || false !== strpos($virtualPath, $module);
    }


    protected function findPathsForSkinAndVendor(string $virtualPath, string $module): array
    {
        $result = [];
        foreach ($this->skin->getParentSkinsNames() as $skin) {
            foreach (['vendor', 'app'] as $src) {
                $realPath = $this->findRealPath($virtualPath, $module, $skin, $src);
                if(null !== $realPath){
                    $result[] = $realPath;
                }
            }
        }
        return $result;
    }

    protected function findPathsForSkin(string $virtualPath, string $module): array
    {
        $result = [];
        foreach ($this->skin->getParentSkinsNames() as $skin) {
            foreach (['vendor', 'app'] as $src) {
                $realPath = $this->findRealPath($virtualPath, $module, $skin, $src);
                if(null !== $realPath){
                    $result[] = $realPath;
                    break;
                }
            }
        }
        return $result;
    }

    protected function findPathsForVendor(string $virtualPath, string $module): array
    {
        $result = [];
        foreach (['vendor', 'app'] as $src) {
            foreach ($this->skin->getParentSkinsNames() as $skin) {
                $realPath = $this->findRealPath($virtualPath, $module, $skin, $src);
                if(null !== $result){
                    $result[] = $realPath;
                    break;
                }
            }
        }
        return $result;
    }

    protected function findRealPath(
        string $virtualPath,
        ?string $module = null,
        ?string $skin = null,
        ?string $src = null
    ): ?string {
        $path = str_replace(['{skin}', '{src}', '{module}'], [$skin, $src, $module], $virtualPath);
        if (file_exists($path)) {
            return $path;
        }
        return null;
    }

    /**
     * @param $dir
     *
     * @return array
     */
    protected function scanDir($dir)
    {
        $files = $this->filesystemHelper->scanDir($dir, true);
        $result = [];
        foreach ($files as $file) {
            if (isset($file['extension']) && $file['extension'] == $this->extension) {
                $result[] = $file['path'];
            }
        }

        return $result;
    }
}
