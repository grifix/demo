<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Asset;

use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Class AssetFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Asset
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AssetFactory extends AbstractFactory implements AssetFactoryInterface
{
    
    /**
     * @var string
     */
    protected $rootDir;
    
    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /** @var KernelInterface */
    protected $kernel;
    
    /**
     * AssetFactory constructor.
     *
     * @param ClassMakerInterface       $classMaker
     * @param string                    $rootDir <root_dir>
     * @param FilesystemHelperInterface $filesystemHelper
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        string $rootDir,
        FilesystemHelperInterface $filesystemHelper,
        KernelInterface $kernel
    ) {
        $this->rootDir = $rootDir;
        $this->filesystemHelper = $filesystemHelper;
        $this->kernel = $kernel;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function create(string $path, SkinInterface $skin, string $extension = null): AssetInterface
    {
        $class = $this->makeClassName(Asset::class);
        $modules = [];
        foreach ($this->kernel->getModules() as $module){
            $modules[] = $module->getVendor().DIRECTORY_SEPARATOR.$module->getName();
        }
        
        return new $class(
            $this->rootDir . DIRECTORY_SEPARATOR . $path,
            $skin,
            $this->filesystemHelper,
            $modules,
            $extension
        );
    }
}
