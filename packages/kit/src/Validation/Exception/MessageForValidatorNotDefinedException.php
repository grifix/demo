<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Validation\Exception;


class MessageForValidatorNotDefinedException extends \Exception
{
    public function __construct(string $validatorType)
    {
        parent::__construct(sprintf('There is no message defined for validator %s', $validatorType));
    }
}
