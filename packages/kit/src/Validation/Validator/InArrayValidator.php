<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class InArrayValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InArrayValidator extends AbstractValidator
{
    
    /**
     * @var array
     */
    protected $array = [];
    
    /**
     * {@inheritdoc}
     */
    public function doValidate($value): bool
    {
        return in_array($value, $this->array);
    }
    
    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->array;
    }
    
    /**
     * @param array $array
     *
     * @return InArrayValidator
     */
    public function setArray(array $array): InArrayValidator
    {
        $this->array = $array;
        
        return $this;
    }
    
}