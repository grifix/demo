<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MinNumOfSymbolsValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MinNumOfSymbolsValidator extends AbstractValidator
{
    protected $numOfSymbols = 1;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        return mb_strlen(strval($value)) >= $this->numOfSymbols;
    }
    
    /**
     * @return int
     */
    public function getNumOfSymbols(): int
    {
        return $this->numOfSymbols;
    }
    
    /**
     * @param int $numOfSymbols
     *
     * @return MinNumOfSymbolsValidator
     */
    public function setNumOfSymbols(int $numOfSymbols): MinNumOfSymbolsValidator
    {
        $this->numOfSymbols = $numOfSymbols;
        
        return $this;
    }
    
    
}