<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MoneyValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MoneyValidator extends AbstractValidator
{
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        $value = (string) $value;
        if((!$this->isPointSeparated($value) && !$this->isCommaSeparated($value) ) || !$this->isNumeric($value)){
            return false;
        }
        return true;
    }

    protected function isPointSeparated($value):bool {
        return (bool) preg_match("/^-?[0-9]+(?:\.[0-9]{1,2})?$/", $value);
    }

    protected function isCommaSeparated($value):bool {
        return (bool) preg_match("/^-?[0-9]+(?:,[0-9]{1,2})?$/", $value);
    }

    protected function isNumeric($value):bool {
        $value = str_replace(',', '.', $value);
        return is_numeric($value);
    }
}
