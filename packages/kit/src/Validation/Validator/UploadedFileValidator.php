<?php

declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

use Psr\Http\Message\UploadedFileInterface;

class UploadedFileValidator extends AbstractValidator
{
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        return $value instanceof UploadedFileInterface;
    }
}
