<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MaxNumOfSymbolsValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MaxNumOfSymbolsValidator extends AbstractValidator
{
    protected $numOfSymbols;
    
    protected function doValidate($value): bool
    {
        return mb_strlen(strval($value)) <= $this->numOfSymbols;
    }
    
    /**
     * @return mixed
     */
    public function getNumOfSymbols()
    {
        return $this->numOfSymbols;
    }
    
    /**
     * @param mixed $numOfSymbols
     *
     * @return MaxNumOfSymbolsValidator
     */
    public function setNumOfSymbols($numOfSymbols):MaxNumOfSymbolsValidator
    {
        $this->numOfSymbols = $numOfSymbols;
        
        return $this;
    }
    
    
}