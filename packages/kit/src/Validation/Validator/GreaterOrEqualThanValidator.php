<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class GreaterOrEqualThanValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GreaterOrEqualThanValidator extends AbstractValidator
{
    /**
     * @var mixed
     */
    protected $minValue;
    
    /**
     * @param mixed $value
     *
     * @return bool
     */
    protected function doValidate($value): bool
    {
        return $value>=$this->minValue;
    }
    
    /**
     * @return mixed
     */
    public function getMinValue()
    {
        return $this->minValue;
    }
    
    /**
     * @param mixed $minValue
     *
     * @return GreaterOrEqualThanValidator
     */
    public function setMinValue($minValue)
    {
        $this->minValue = $minValue;
        
        return $this;
    }
}
