<?php

declare(strict_types=1);

namespace Grifix\Kit\Validation\Validator;

use Ramsey\Uuid\Uuid;

class UuidValidator extends AbstractValidator
{
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        return Uuid::isValid($value);
    }
}
