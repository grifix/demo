<?php

declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

class RegExpValidator extends AbstractValidator
{
    /** @var string|null */
    protected $regExp;

    public function setRegExp(string $regExp): self
    {
        $this->regExp = $regExp;
        return $this;
    }

    protected function doValidate($value): bool
    {
        return (bool) preg_match($this->regExp, $value);
    }
}
