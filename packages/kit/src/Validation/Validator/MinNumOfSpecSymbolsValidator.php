<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MinNumOfSpecSymbolsValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MinNumOfSpecSymbolsValidator extends AbstractValidator
{
    public $numOfSymbols = 1;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        $exp=[];
        for($i=0; $i<$this->numOfSymbols; $i++){
            $exp[]='([\W])';
        }
    
        $regExp="/".implode('.*',$exp)."/";
        return boolval(preg_match($regExp, $value));
    }
    
    /**
     * @return int
     */
    public function getNumOfSymbols(): int
    {
        return $this->numOfSymbols;
    }
    
    /**
     * @param int $numOfSymbols
     *
     * @return MinNumOfSpecSymbolsValidator
     */
    public function setNumOfSymbols(int $numOfSymbols): MinNumOfSpecSymbolsValidator
    {
        $this->numOfSymbols = $numOfSymbols;
        
        return $this;
    }
    
    
}