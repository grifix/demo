<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

class DateValidator extends AbstractValidator
{
    protected $format;

    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        if ($value instanceof \DateTimeInterface) {
            return true;
        }

        try {
            $date = new \DateTimeImmutable($value);
        } catch (\Exception $E) {
            return false;
        }

        if ($this->format) {
            return $date->format($this->format) === $value;
        }

        return true;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }
}
