<?php

declare(strict_types = 1);

namespace Grifix\Kit\Validation\Field;

use Grifix\Kit\Validation\ErrorFactoryInterface;
use Grifix\Kit\Validation\Validator\GreaterThanValidator;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

class PositiveFloatValidationField extends FloatValidationField
{
    public function __construct(
        $name,
        ValidatorFactoryInterface $validatorFactory,
        ErrorFactoryInterface $errorFactory,
        $label = null,
        array $messages = []
    ) {
        parent::__construct($name, $validatorFactory, $errorFactory, $label, $messages);
        $this->addValidator($this->validatorFactory->createValidator(GreaterThanValidator::class)->setMinValue(0));
    }
}
