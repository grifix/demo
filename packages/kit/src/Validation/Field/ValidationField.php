<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Validation\Field;

use Grifix\Kit\Validation\ErrorFactoryInterface;
use Grifix\Kit\Validation\Exception\MessageForValidatorNotDefinedException;
use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Kit\Validation\Exception\ValidatorAlreadyExistsException;
use Grifix\Kit\Validation\Exception\ValidatorIsNotExistsException;
use Grifix\Kit\Validation\Validator\ArrayValidator;
use Grifix\Kit\Validation\Validator\NotEmptyValidator;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;
use Grifix\Kit\Validation\Validator\ValidatorInterface;

class ValidationField implements ValidationFieldInterface
{
    protected string $name;

    protected string $label;

    /**
     * @var ValidatorInterface[]
     */
    protected array $validators = [];

    protected bool $enabled = true;

    protected ValidatorFactoryInterface $validatorFactory;

    protected ErrorFactoryInterface $errorFactory;

    protected array $errors = [];

    protected array $messages = [];

    protected bool $arrayValue = false;

    protected bool $required = false;

    public function __construct(
        string $name,
        ValidatorFactoryInterface $validatorFactory,
        ErrorFactoryInterface $errorFactory,
        string $label = null,
        array $messages = []
    )
    {
        if (!$label) {
            $label = $name;
        }

        $this->validatorFactory = $validatorFactory;
        $this->name = $name;
        $this->label = $label;
        $this->errorFactory = $errorFactory;
        $this->messages = $messages;
        $this->init();
    }

    /**
     * @return void
     */
    protected function init()
    {
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function validate($value): bool
    {
        $this->errors = [];

        if ($this->arrayValue && !is_array($value)) {
            $this->errors[] = $this->errorFactory->create(
                $this,
                $this->validatorFactory->createValidator(ArrayValidator::class),
                $this->getMessage(ArrayValidator::class),
                $value
            );
            return false;
        }

        if (is_array($value)) {
            foreach ($value as $val) {
                if (!$this->doValidate($val)) {
                    return false;
                }
            }
            return true;
        } else {
            return $this->doValidate($value);
        }
    }

    protected function doValidate($value): bool
    {
        $result = true;
        foreach ($this->validators as $key => $validator) {
            if (!$validator->validate($value)) {
                $this->errors[] = $this->errorFactory->create(
                    $this,
                    $validator,
                    $this->getMessage(get_class($validator)),
                    $value
                );
                $result = false;
                if ($validator->isCancelOnFail()) {
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setLabel(string $label): ValidationFieldInterface
    {
        $this->label = $label;

        return $this;
    }

    public function setValueIsArray(): ValidationFieldInterface
    {
        $this->arrayValue = true;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setName(string $name): ValidationFieldInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param $value
     *
     * @return void
     * @throws ValidationException
     */
    public function validateOrFail($value)
    {
        if (!$this->validate($value)) {
            throw new ValidationException($this->errors);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * {@inheritdoc}
     */
    public function addValidator(
        ValidatorInterface $validator,
        string $place = self::PLACE_APPEND
    ): ValidationFieldInterface
    {
        $validatorClass = get_class($validator);
        if ($this->hasValidator($validatorClass)) {
            throw new ValidatorAlreadyExistsException($validatorClass);
        }
        if ($place == self::PLACE_APPEND) {
            $this->validators[$validatorClass] = $validator;
        } else {
            $validators[$validatorClass] = $validator;
            foreach ($this->validators as $k => $v) {
                $validators[$k] = $v;
            }
            $this->validators = $validators;
        }

        return $this;
    }

    /**
     * @param ValidatorInterface $validator
     *
     * @return bool
     */
    public function addValidatorIfNotExists(ValidatorInterface $validator): bool
    {
        try {
            $this->addValidator($validator);
        } catch (ValidatorAlreadyExistsException $e) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): void
    {
        $this->required = $required;
        if ($required && !$this->hasValidator(NotEmptyValidator::class)) {
            $this->createValidator(NotEmptyValidator::class);
        }
        if (!$required && $this->hasValidator(NotEmptyValidator::class)) {
            $this->removeValidator(NotEmptyValidator::class);
        }
    }

    /**
     * @return ValidationFieldInterface
     */
    public function enable(): ValidationFieldInterface
    {
        $this->enabled = true;

        return $this;
    }

    /**
     * @return ValidationFieldInterface
     */
    public function disable(): ValidationFieldInterface
    {
        $this->enabled = false;

        return $this;
    }

    public function createValidator(string $validatorClass, string $place = self::PLACE_APPEND): ValidatorInterface
    {
        $this->addValidator($this->validatorFactory->createValidator($validatorClass), $place);

        return $this->getValidator($validatorClass);
    }

    /**
     * {@inheritdoc}
     */
    public function createValidatorIfNotExists(
        string $validatorClass,
        string $place = self::PLACE_APPEND
    ): ValidatorInterface
    {
        if (!$this->hasValidator($validatorClass)) {
            return $this->createValidator($validatorClass, $place);
        }

        return $this->getValidator($validatorClass);
    }

    /**
     * {@inheritdoc}
     */
    public function removeValidator(string $validatorClass): ValidationFieldInterface
    {
        unset($this->validators[$validatorClass]);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getValidator(string $validatorClass): ValidatorInterface
    {
        if (!$this->hasValidator($validatorClass)) {
            throw new ValidatorIsNotExistsException($validatorClass);
        }

        return $this->validators[$validatorClass];
    }

    /**
     * {@inheritdoc}
     */
    public function hasValidator(string $validatorClass): bool
    {
        return in_array($validatorClass, array_keys($this->validators));
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(string $validatorClass)
    {
        if (false === array_key_exists($validatorClass, $this->messages)) {
            throw new MessageForValidatorNotDefinedException($validatorClass);
        }
        return $this->messages[$validatorClass];
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage(string $validatorClass, $message): ValidationFieldInterface
    {
        $this->messages[$validatorClass] = $message;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * {@inheritdoc}
     */
    public function setNotEmpty(bool $notEmpty = true): ValidationFieldInterface
    {
        if ($notEmpty && !$this->hasValidator(NotEmptyValidator::class)) {
            $this->createValidator(NotEmptyValidator::class, self::PLACE_PREPEND)->cancelOnFail();
        }

        if (!$notEmpty && $this->hasValidator(NotEmptyValidator::class)) {
            $this->removeValidator(NotEmptyValidator::class);
        }

        return $this;
    }


}
