<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Field;

use Grifix\Kit\Validation\ErrorFactoryInterface;
use Grifix\Kit\Validation\Validator\IpValidator;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

class IpValidationField extends ValidationField
{
    public function __construct(
        $name,
        ValidatorFactoryInterface $validatorFactory,
        ErrorFactoryInterface $errorFactory,
        $label = null,
        array $messages = []
    ) {
        parent::__construct($name, $validatorFactory, $errorFactory, $label, $messages);
        $this->createValidator(IpValidator::class);
    }
}
