<?php

declare(strict_types = 1);

namespace Grifix\Kit\Validation\Field;

use Grifix\Kit\Validation\ErrorFactoryInterface;
use Grifix\Kit\Validation\Validator\UploadedFileValidator;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

class UploadedFileValidationField extends ValidationField
{
    public function __construct(
        $name,
        ValidatorFactoryInterface $validatorFactory,
        ErrorFactoryInterface $errorFactory,
        $label = null,
        array $messages = []
    ) {
        parent::__construct($name, $validatorFactory, $errorFactory, $label, $messages);
        $this->createValidator(UploadedFileValidator::class);
    }
}
