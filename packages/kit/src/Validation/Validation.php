<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Validation;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Validation\Exception\FieldAlreadyExistsException;
use Grifix\Kit\Validation\Exception\FieldNotExistsException;
use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Kit\Validation\Field\FieldFactoryInterface;
use Grifix\Kit\Validation\Field\ValidationFieldInterface;

/**
 * Class Validation
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Validation implements ValidationInterface
{
    /**
     * @var ValidationFieldInterface[]
     */
    protected $fields = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var FieldFactoryInterface
     */
    protected $fieldFactory;

    /**
     * @var string
     */
    protected $strategy = self::STRATEGY_STRICT;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * Validation constructor.
     *
     * @param FieldFactoryInterface $fieldFactory
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(FieldFactoryInterface $fieldFactory, ArrayHelperInterface $arrayHelper)
    {
        $this->fieldFactory = $fieldFactory;
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function addField(ValidationFieldInterface $field): ValidationInterface
    {
        if ($this->hasField($field->getName())) {
            throw new FieldAlreadyExistsException($field);
        }
        $this->fields[] = $field;

        return $this;
    }


    public function setStrictStrategy(): ValidationInterface
    {
        $this->strategy = self::STRATEGY_STRICT;
        return $this;
    }

    public function setSoftStrategy(): ValidationInterface
    {
        $this->strategy = self::STRATEGY_SOFT;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createField(string $name, string $class = null): ValidationFieldInterface
    {
        $this->addField($this->fieldFactory->create($name, $class));

        return $this->getField($name);
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasField(string $name): bool
    {
        foreach ($this->fields as $field) {
            if ($field->getName() == $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getField(string $name): ValidationFieldInterface
    {
        foreach ($this->fields as $field) {
            if ($field->getName() == $name) {
                return $field;
            }
        }

        throw new FieldNotExistsException($name);
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $values): bool
    {
        $this->errors = [];
        $result = true;
        if ($this->strategy == self::STRATEGY_SOFT) {
            foreach ($values as $key => $value) {
                if ($this->hasField($key) && $this->getField($key)->isEnabled() && !$this->getField($key)->validate($value)) {
                    $result = false;
                    $this->errors = array_merge($this->errors, $this->getField($key)->getErrors());
                }
            }
        } else {
            if ($this->strategy == self::STRATEGY_STRICT) {
                foreach ($this->fields as $field) {
                    if (false === $field->isRequired() && false === isset($values[$field->getName()])) {
                        continue;
                    }
                    if ($field->isEnabled() && false === $field->validate($this->arrayHelper->get($values, $field->getName()))) {
                        $result = false;
                        $this->errors = array_merge($this->errors, $field->getErrors());
                    }
                }
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * {@inheritdoc}
     */
    public function validateOrFail(array $values)
    {
        if (!$this->validate($values)) {
            throw new ValidationException($this->errors);
        }
    }
}
