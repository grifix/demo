<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Validation;

use Grifix\Kit\Validation\Exception\FieldNotExistsException;
use Grifix\Kit\Validation\Exception\InvalidValidationStrategyException;
use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Kit\Validation\Field\ValidationFieldInterface;

/**
 * Class Validation
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ValidationInterface
{
    public const STRATEGY_STRICT = 'strict'; //validate all fields defined in validation
    public const STRATEGY_SOFT = 'soft'; //validate only fields that existed in values array
    
    /**
     * @param ValidationFieldInterface $field
     *
     * @return ValidationInterface
     * @internal param string $key
     */
    public function addField(ValidationFieldInterface $field): ValidationInterface;
    
    /**
     * @param string $name
     * @param string $class
     *
     * @return ValidationFieldInterface
     */
    public function createField(string $name, string $class = null): ValidationFieldInterface;
    
    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasField(string $name): bool;
    
    /**
     * @param string $name
     *
     * @return ValidationFieldInterface
     * @throws FieldNotExistsException
     */
    public function getField(string $name): ValidationFieldInterface;
    
    /**
     * @param array $values
     *
     * @return bool
     */
    public function validate(array $values): bool;
    
    /**
     * @return Error[]
     */
    public function getErrors(): array;
    
    /**
     * @param array $values
     *
     * @return void
     * @throws ValidationException
     */
    public function validateOrFail(array $values);

    public function setStrictStrategy(): ValidationInterface;

    public function setSoftStrategy(): ValidationInterface;
}
