<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Middleware;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

class FirstMiddleware extends AbstractMiddleware
{
    protected function after(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        if (!($response instanceof ResponseInterface)) {
            throw new InvalidResponseException();
        }
        return $response;
    }

    protected function before(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $response;
    }
}
