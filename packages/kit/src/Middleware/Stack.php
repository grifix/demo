<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Kit\Middleware;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class Stack
 *
 * @category Grifix
 * @package  Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Stack implements StackInterface
{
    /**
     * @var \SplStack
     */
    protected $stack;
    
    /**
     * Stack constructor.
     */
    public function __construct()
    {
        $this->stack = new \SplStack();
        $this->stack->setIteratorMode(\SplDoublyLinkedList::IT_MODE_LIFO | \SplDoublyLinkedList::IT_MODE_KEEP);
        $this->stack[] = new FirstMiddleware();
    }
    
    /**
     * {@inheritdoc}
     */
    public function add(MiddlewareInterface $middleware)
    {
        $middleware->setNextMiddleware($this->stack->top());
        $this->stack[]=$middleware;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function call(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {
        /**
         * @var $start MiddlewareInterface
         */
        $start = $this->stack->top();
        return $start->process($request, $response);
    }
}
