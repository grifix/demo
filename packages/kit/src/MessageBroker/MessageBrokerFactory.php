<?php

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\MessageBroker\ConsumerFactory\ConsumerFactoryInterface;
use Grifix\Kit\MessageBroker\QueueDefinitionsRepository\QueueDefinitionsRepositoryInterface;
use Grifix\Kit\Normalizer\NormalizerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class QueueFactory
 * @package Grifix\Kit\Queue
 */
class MessageBrokerFactory implements MessageBrokerFactoryInterface
{
    protected string $rabbitMqHost;

    protected int $rabbitMqPort;

    protected string $rabbitMqUser;

    protected string $rabbitMqPassword;

    protected ConsumerFactoryInterface $consumerFactory;

    protected QueueDefinitionsRepositoryInterface $definitionsCollector;

    protected NormalizerInterface $normalizer;

    protected QueueDefinitionsRepositoryInterface $queueDefinitionRepository;

    protected IocContainerInterface $iocContainer;

    /**
     * @param string $rabbitMqHost <cfg:grifix.kit.queue.rabbit.host>
     * @param int $rabbitMqPort <cfg:grifix.kit.queue.rabbit.port>
     * @param string $rabbitMqUser <cfg:grifix.kit.queue.rabbit.user>
     * @param string $rabbitMqPassword <cfg:grifix.kit.queue.rabbit.pass>
     */
    public function __construct(
        string $rabbitMqHost,
        int $rabbitMqPort,
        string $rabbitMqUser,
        string $rabbitMqPassword,
        ConsumerFactoryInterface $consumerFactory,
        QueueDefinitionsRepositoryInterface $definitionsCollector,
        NormalizerInterface $normalizer,
        IocContainerInterface $iocContainer,
        QueueDefinitionsRepositoryInterface $queueDefinitionRepository
    ) {
        $this->rabbitMqHost = $rabbitMqHost;
        $this->rabbitMqPort = $rabbitMqPort;
        $this->rabbitMqUser = $rabbitMqUser;
        $this->rabbitMqPassword = $rabbitMqPassword;
        $this->consumerFactory = $consumerFactory;
        $this->definitionsCollector = $definitionsCollector;
        $this->normalizer = $normalizer;
        $this->iocContainer = $iocContainer;
        $this->queueDefinitionRepository = $queueDefinitionRepository;
    }

    public function createRabbitMqMessageBroker(): MessageBrokerInterface
    {
        return new RabbitMqMessageBroker(
            new AMQPStreamConnection(
                $this->rabbitMqHost,
                $this->rabbitMqPort,
                $this->rabbitMqUser,
                $this->rabbitMqPassword
            ),
            $this->normalizer
        );
    }

    public function createSyncMessageBroker(): MessageBrokerInterface
    {
        $result = new SyncMessageBroker();
        $iocContainer = $this->iocContainer;

        foreach ($this->queueDefinitionRepository->find() as $queueDefinition) {
            for ($i = 0; $i < $queueDefinition->getQuantity(); $i++) {
                $result->registerConsumer(
                    function ($message) use ($queueDefinition, $iocContainer) {
                        $iocContainer->get($queueDefinition->getConsumerClass())->__invoke($message);
                    },
                    $queueDefinition->getQueueName()
                );
            }
        }

        return $result;
    }

    public function createDelayedMessageBroker(
        ?MessageBrokerInterface $messageBroker = null,
        int $delay = 1
    ): MessageBrokerInterface {
        $messageBroker = $messageBroker ?? $this->createRabbitMqMessageBroker();
        return new DelayedMessageBroker($messageBroker, $delay);
    }
}
