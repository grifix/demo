<?php

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker;

/**
 * Class SyncQueue
 * @package Grifix\Kit\Queue
 */
class SyncMessageBroker implements MessageBrokerInterface
{
    protected array $queues = [];

    protected array $exchanges = [];

    public function sendMessage(object $message, string $exchange = "", string $routingKey = ""): void
    {
        if ($exchange === "") {
            foreach ($this->queues as $queue) {
                $this->sendMessageToQueue($message, $queue);
            }
            return;
        }

        foreach ($this->exchanges[$exchange] as $queue) {
            $this->sendMessageToQueue($message, $queue);
        }
    }

    public function registerConsumer(callable $consumer, string $queue, bool $exclusive = false): void
    {
        $this->declareQueue($queue);
        if ($exclusive && count($this->queues[$queue]) > 1) {
            throw new \Exception(sprintf('Cannot register more consumers queue %q is exclusive!', $queue));
        }
        $this->queues[$queue][] = $consumer;
    }

    public function declareQueue(string $queue): void
    {
        if (!isset($this->queues[$queue])) {
            $this->queues[$queue] = [];
        }
    }

    public function bindQueue(string $queue, string $exchange): void
    {
        $this->exchanges[$exchange][] = $queue;
    }

    public function deleteQueue(string $queue): void
    {
        unset($this->queues[$queue]);
    }

    public function deleteExchange(string $exchange): void
    {
        unset($this->exchanges[$exchange]);
    }

    public function declareExchange(string $exchange): void
    {
        if (!isset($this->exchanges[$exchange])) {
            $this->exchanges[$exchange] = [];
        }
    }

    protected function sendMessageToQueue(object $message, string $queue): void
    {
        $this->declareQueue($queue);
        if (count($this->queues[$queue]) > 0) {
            $consumer = $this->queues[$queue][0];
            $consumer($message);
        }
    }

    protected function sendMessageToExchange(object $message, string $exchange): void
    {
        $this->declareExchange($exchange);
        foreach ($this->exchanges[$exchange] as $queue) {
            $this->sendMessageToQueue($message, $queue);
        }
    }
}
