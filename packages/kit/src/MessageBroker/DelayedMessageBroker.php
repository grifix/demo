<?php declare(strict_types = 1);

namespace Grifix\Kit\MessageBroker;

class DelayedMessageBroker implements MessageBrokerInterface
{
    public function __construct(protected MessageBrokerInterface $messageBroker, protected int $delay = 1)
    {
    }

    public function sendMessage(object $message, string $exchange = "", string $routingKey = ""): void
    {
        $this->messageBroker->sendMessage($message, $exchange, $routingKey);
        sleep($this->delay);
    }

    public function registerConsumer(callable $consumer, string $queue, bool $exclusive = false): void
    {
        $this->messageBroker->registerConsumer($consumer, $queue, $exclusive);
    }

    public function declareQueue(string $queue): void
    {
        $this->messageBroker->declareQueue($queue);
    }

    public function bindQueue(string $queue, string $exchange): void
    {
        $this->messageBroker->bindQueue($queue, $exchange);
    }

    public function declareExchange(string $exchange): void
    {
        $this->messageBroker->declareExchange($exchange);
    }

    public function deleteQueue(string $queue): void
    {
        $this->messageBroker->deleteQueue($queue);
    }

    public function deleteExchange(string $exchange): void
    {
        $this->messageBroker->deleteExchange($exchange);
    }
}
