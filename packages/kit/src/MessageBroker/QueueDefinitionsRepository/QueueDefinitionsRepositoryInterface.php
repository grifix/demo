<?php
declare(strict_types=1);

namespace Grifix\Kit\MessageBroker\QueueDefinitionsRepository;

use Grifix\Kit\MessageBroker\QueueDefinition;

/**
 * Class DefinitionsCollectorInterface
 * @package Grifix\Kit\Queue\DefinitionsCollector
 */
interface QueueDefinitionsRepositoryInterface
{
    /**
     * @return QueueDefinition[]
     */
    public function find(): array;
}
