<?php

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker\QueueDefinitionsRepository;

use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\MessageBroker\QueueDefinition;
use Psr\SimpleCache\CacheInterface;

class QueueDefinitionsRepository implements QueueDefinitionsRepositoryInterface
{
    protected KernelInterface $kernel;

    protected CacheInterface $cache;

    public function __construct(KernelInterface $kernel, CacheInterface $cache)
    {
        $this->kernel = $kernel;
        $this->cache = $cache;
    }

    /**
     * @return QueueDefinition[]
     */
    public function find(): array
    {
        $cacheKey = str_replace('\\', '_', get_class($this));
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }
        $result = [];
        foreach ($this->kernel->getModules() as $module) {
            $queues = $module->getConfig($this->kernel->getEnvironment())['queues'] ?? [];
            foreach ($queues as $definition) {
                $result[] = $definition;
            }
        }
        $this->cache->set($cacheKey, $result);
        return $result;
    }
}
