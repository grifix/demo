<?php

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker;

use Grifix\Kit\Helper\ObjectHelperInterface;
use Grifix\Kit\Normalizer\NormalizerInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMqMessageBroker implements MessageBrokerInterface
{
    protected AMQPStreamConnection $connection;

    protected AMQPChannel $channel;

    protected NormalizerInterface $normalizer;

    public function __construct(AMQPStreamConnection $connection, NormalizerInterface $normalizer)
    {
        $this->connection = $connection;
        $this->channel = $connection->channel();
        $this->normalizer = $normalizer;
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }

    public function sendMessage(object $message, string $exchange = "", string $routingKey = ""): void
    {
        if ($exchange) {
            $this->declareExchange($exchange);
        }
        $msg = new AMQPMessage(json_encode($this->normalizer->normalize($message)));
        $this->channel->basic_publish($msg, $exchange, $routingKey);
    }

    public function registerConsumer(callable $consumer, string $queue, bool $exclusive = false): void
    {
        $this->declareQueue($queue);
        $normalizer = $this->normalizer;
        $this->channel->basic_consume(
            $queue,
            '',
            false,
            false,
            $exclusive,
            false,
            function ($msg) use ($consumer, $normalizer) {
                $consumer($normalizer->denormalize(json_decode($msg->body, true)));
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            }
        );
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    public function declareQueue(string $queue): void
    {
        $this->channel->queue_declare($queue, false, true, false, false);
    }

    public function bindQueue(string $queue, string $exchange): void
    {
        $this->channel->queue_bind($queue, $exchange);
    }

    public function declareExchange(string $exchange): void
    {
        $this->channel->exchange_declare($exchange, 'fanout');
    }

    public function deleteQueue(string $queue): void
    {
        $this->channel->queue_delete($queue);
    }

    public function deleteExchange(string $exchange): void
    {
        $this->channel->exchange_delete($exchange);
    }
}
