<?php
declare(strict_types=1);

namespace Grifix\Kit\MessageBroker\ConsumerFactory;

interface ConsumerFactoryInterface
{
    public function create(string $consumerClass);
}
