<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker;

class QueueDefinition
{

    protected string $consumerClass;

    protected string $queueName;

    protected int $quantity;

    public function __construct(string $consumerClass, string $queueName, int $quantity)
    {
        $this->consumerClass = $consumerClass;
        $this->queueName = $queueName;
        $this->quantity = $quantity;
    }

    public function getConsumerClass(): string
    {
        return $this->consumerClass;
    }

    public function getQueueName(): string
    {
        return $this->queueName;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
