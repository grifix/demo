<?php

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker;

/**
 * Class Queue
 * @package Grifix\Kit\Queue
 */
interface MessageBrokerInterface
{
    public function sendMessage(object $message, string $exchange = "", string $routingKey = ""): void;

    public function registerConsumer(callable $consumer, string $queue, bool $exclusive = false): void;

    public function declareQueue(string $queue): void;

    public function bindQueue(string $queue, string $exchange): void;

    public function declareExchange(string $exchange): void;

    public function deleteQueue(string $queue): void;

    public function deleteExchange(string $exchange): void;

}
