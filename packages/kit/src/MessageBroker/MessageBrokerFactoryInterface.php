<?php

declare(strict_types=1);

namespace Grifix\Kit\MessageBroker;

/**
 * Class QueueFactory
 * @package Grifix\Kit\Queue
 */
interface MessageBrokerFactoryInterface
{
    /**
     * @return MessageBrokerInterface
     */
    public function createRabbitMqMessageBroker(): MessageBrokerInterface;

    /**
     * @return MessageBrokerInterface
     */
    public function createSyncMessageBroker(): MessageBrokerInterface;

    public function createDelayedMessageBroker(
        ?MessageBrokerInterface $messageBroker = null,
        int $delay = 1
    ): MessageBrokerInterface;

}
