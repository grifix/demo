<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit;

/**
 * Interface Grifix
 *
 * @category Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface Alias
{
    const SLOW_CACHE = 'slow_cache';
    const QUICK_CACHE = 'quick_cache';
    const APP_CONTEXT = 'app_context';
    const ROOT_DIR = 'root_dir';
    const PUBLIC_DIR = 'public_dir';
    const TMP_DIR = 'tmp_dir';
    const ENVIRONMENT = 'environment';
    const CURRENT_SKIN = 'current_skin';
    const CURRENT_ROUTE = 'current_route';
    const ASYNC_COMMAND_BUS = 'grifix_kit_async_command_bus';
    const SYNC_COMMAND_BUS = 'grifix_kit_sync_command_bus';
    const UNKNOWN = 'unknown';
}

