<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\SubscribersStarter;

use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Event\SubscribersStarter\ProcessStarter\ProcessStarterInterface;
use Grifix\Kit\Event\SubscriberType\EventSubscriberType;
use Grifix\Kit\Event\SubscriberType\EventSubscriberTypeRepositoryInterface;
use Grifix\Kit\Event\Subscription\EventSubscriptionFactoryInterface;
use Grifix\Kit\Event\Subscription\Repository\EventSubscriptionRepositoryInterface;

final class EventSubscribersStarter implements EventSubscribersStarterInterface
{
    public const LISTENER_ID = 'subscribers_starter';

    protected EventStoreInterface $eventStore;

    protected EventSubscriberTypeRepositoryInterface $eventSubscriberTypeRepository;

    protected EventSubscriptionRepositoryInterface $eventSubscriptionRepository;

    protected EventSubscriptionFactoryInterface $eventSubscriptionFactory;

    protected ProcessStarterInterface $processStarter;

    public function __construct(
        EventStoreInterface $eventStore,
        EventSubscriberTypeRepositoryInterface $eventSubscriberTypeRepository,
        EventSubscriptionRepositoryInterface $eventSubscriptionRepository,
        EventSubscriptionFactoryInterface $eventSubscriptionFactory,
        ProcessStarterInterface $processStarter
    ) {
        $this->eventStore = $eventStore;
        $this->eventSubscriberTypeRepository = $eventSubscriberTypeRepository;
        $this->eventSubscriptionRepository = $eventSubscriptionRepository;
        $this->eventSubscriptionFactory = $eventSubscriptionFactory;
        $this->processStarter = $processStarter;
    }

    public function start(): void
    {
        $this->eventStore->startListener(
            function (EventEnvelope $envelope) {
                foreach ($this->eventSubscriberTypeRepository->find() as $subscriberType) {
                    $this->startSubscriber($subscriberType, $envelope);
                }
            },
            self::LISTENER_ID,
            false
        );
    }

    protected function startSubscriber(EventSubscriberType $subscriberType, EventEnvelope $envelope): void
    {
        if (false === $subscriberType->isEventSupported($envelope->getEvent())) {
            return;
        }
        $subscriptionId = $subscriberType->createSubscriptionId($envelope->getEvent());
        $subscriptionAlias = $subscriberType->createSubscriptionAlias($subscriptionId);

        if ($this->processStarter->processExists($subscriptionId)) {
            return;
        }

        if ($this->eventSubscriptionRepository->has($subscriberType->getName(), $subscriptionId)) {
            $subscription = $this->eventSubscriptionRepository->get($subscriberType->getName(), $subscriptionId);
            if ($subscription->isActive()) {
                $this->processStarter->startProcess($subscriberType->getName(), $subscriptionId, $subscriptionAlias);
            }
            return;
        }

        if ($subscriberType->isStartingEvent($envelope->getEvent())) {
            $this->eventSubscriptionFactory->create($envelope, $subscriberType->getName());
            $this->processStarter->startProcess($subscriberType->getName(), $subscriptionId, $subscriptionAlias);
            return;
        }
    }
}
