<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\SubscribersStarter\ProcessStarter;

interface ProcessStarterInterface
{
    public function startProcess(string $subscriberType, string $subscriptionId, string $subscriptionAlias): void;

    public function processExists(string $subscriptionId):bool;
}
