<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\SubscribersStarter\ProcessStarter;

use Grifix\Kit\Event\Subscription\Repository\EventSubscriptionRepositoryInterface;

class InMemoryProcessStarter implements ProcessStarterInterface
{
    protected EventSubscriptionRepositoryInterface $eventSubscriptionRepository;

    protected array $activeProcesses = [];

    public function __construct(EventSubscriptionRepositoryInterface $eventSubscriptionRepository)
    {
        $this->eventSubscriptionRepository = $eventSubscriptionRepository;
    }

    public function startProcess(string $subscriberType, string $subscriptionId, string $subscriptionAlias): void
    {
        $this->eventSubscriptionRepository->get($subscriberType, $subscriptionId)->listenEvents();
        $this->activeProcesses[] = $subscriptionId;
    }

    public function processExists(string $subscriptionId): bool
    {
        return in_array($subscriptionId, $this->activeProcesses);
    }
}
