<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\SubscribersStarter\ProcessStarter;

use Grifix\Kit\Cli\CliInterface;
use Grifix\Kit\ProcessManager\Exception\ProcessAlreadyExistsException;
use Grifix\Kit\ProcessManager\ProcessManagerInterface;
use Grifix\Shared\Ui\Cli\Command\Event\Subscriber\EventSubscriberProcessCommand;
use Psr\Log\LoggerInterface;

class ProcessStarter implements ProcessStarterInterface
{
    protected ProcessManagerInterface $processManager;

    protected CliInterface $cli;

    protected LoggerInterface $logger;

    public function __construct(ProcessManagerInterface $processManager, CliInterface $cli, LoggerInterface $logger)
    {
        $this->processManager = $processManager;
        $this->cli = $cli;
        $this->logger = $logger;
    }

    public function startProcess(string $subscriberType, string $subscriptionId, string $subscriptionAlias): void
    {
        try {
            $this->processManager->startProcess(
                $this->createProcessId($subscriptionId),
                $this->cli->createCommandString(
                    EventSubscriberProcessCommand::NAME,
                    $subscriberType,
                    $subscriptionId
                )
            );
        } catch (ProcessAlreadyExistsException $exception) {
            $this->logger->notice($exception->getMessage());
        }
    }

    public function processExists(string $subscriptionId): bool
    {
        return $this->processManager->processExists($this->createProcessId($subscriptionId));
    }

    private function createProcessId(string $subscriptionId): string
    {
        return sprintf('subscriber_%s', $subscriptionId);
    }
}
