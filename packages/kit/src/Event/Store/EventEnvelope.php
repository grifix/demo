<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Store;

final class EventEnvelope
{
    private string $id;

    private object $event;

    private string $createdAt;

    private int $number;

    public function __construct(string $id, object $event, string $createdAt, int $number)
    {
        $this->id = $id;
        $this->event = $event;
        $this->createdAt = $createdAt;
        $this->number = $number;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEvent(): object
    {
        return $this->event;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getNumber(): int
    {
        return $this->number;
    }
}
