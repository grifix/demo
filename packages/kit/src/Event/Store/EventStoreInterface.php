<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Store;

use Grifix\Kit\Event\Store\Exception\ListenerStoppedException;

interface EventStoreInterface
{
    public function add(object $event): void;

    /**
     * @return EventEnvelope[]
     */
    public function fetch(EventFilter $filter): \Iterator;

    public function publish(?EventFilter $filter = null, string $listenerId = ""): void;

    /**
     * @throws ListenerStoppedException
     */
    public function startListener(callable $listener, string $listenerId, bool $exclusive = true): void;

    public function stopListener(string $listenerId): void;

    public function declareListener(string $listenerId): void;

    public function resetListener(string $listenerId): void;

    public function countEvents(): int;
}
