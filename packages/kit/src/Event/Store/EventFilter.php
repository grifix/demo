<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Store;

final class EventFilter
{
    private ?bool $isPublished = null;

    private ?int $fromNumber = null;

    private ?int $toNumber = null;

    private ?string $id = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(?bool $isPublished): self
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    public function getFromNumber(): ?int
    {
        return $this->fromNumber;
    }

    public function setFromNumber(?int $fromNumber): self
    {
        $this->fromNumber = $fromNumber;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getToNumber(): ?int
    {
        return $this->toNumber;
    }

    public function setToNumber(?int $toNumber): self
    {
        $this->toNumber = $toNumber;
        return $this;
    }
}
