<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Store;

use Grifix\Kit\Event\Store\Exception\ListenerStoppedException;
use Grifix\Kit\Event\Store\Repository\EventRepositoryInterface;
use Grifix\Kit\MessageBroker\MessageBrokerInterface;
use PhpAmqpLib\Exception\AMQPBasicCancelException;

class EventStore implements EventStoreInterface
{
    protected const EXCHANGE = 'event_store';

    protected EventRepositoryInterface $eventRepository;

    protected MessageBrokerInterface $messageBroker;

    public function __construct(EventRepositoryInterface $eventRepository, MessageBrokerInterface $messageBroker)
    {
        $this->eventRepository = $eventRepository;
        $this->messageBroker = $messageBroker;
        $this->messageBroker->declareExchange(self::EXCHANGE);
    }

    public function add(object $event): void
    {
        $this->eventRepository->add($event);
    }

    /**
     * @return EventEnvelope[]
     */
    public function fetch(EventFilter $filter): \Iterator
    {
        return $this->eventRepository->find($filter);
    }

    /**
     * @throws ListenerStoppedException
     */
    public function startListener(callable $listener, string $listenerId, bool $exclusive = true): void
    {
        try {
            $this->declareListener($listenerId);
            $this->messageBroker->registerConsumer($listener, $listenerId, $exclusive);
        } catch (AMQPBasicCancelException $exception) {
            throw new ListenerStoppedException($listenerId);
        }
    }

    public function countEvents(): int
    {
        return $this->eventRepository->count();
    }

    public function declareListener(string $listenerId): void
    {
        $this->messageBroker->declareQueue($listenerId);
        $this->messageBroker->bindQueue($listenerId, self::EXCHANGE);
    }

    public function resetListener(string $listenerId): void
    {
        $this->messageBroker->deleteQueue($listenerId);
        $this->messageBroker->declareQueue($listenerId);
        $this->messageBroker->bindQueue($listenerId, self::EXCHANGE);
    }

    public function stopListener(string $listenerId): void
    {
        $this->messageBroker->deleteQueue($listenerId);
    }

    public function publish(?EventFilter $filter = null, string $listenerId = ""): void
    {
        if (null === $filter) {
            $filter = EventFilter::create()->setIsPublished(false);
        }
        $publisher = $this->createPublisher($listenerId);
        foreach ($this->eventRepository->find($filter) as $envelope) {
            if ("" === $listenerId) {
                $this->eventRepository->publish($envelope, $publisher);
                continue;
            }
            $publisher($envelope);
        }
    }

    protected function createPublisher(string $listenerId = ""): callable
    {
        return function (EventEnvelope $envelope) use ($listenerId): void {
            $exchange = self::EXCHANGE;
            if ($listenerId) {
                $exchange = "";
            }
            $this->messageBroker->sendMessage(
                $envelope,
                $exchange,
                $listenerId
            );
        };
    }
}
