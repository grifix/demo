<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Store\Exception;

final class ListenerStoppedException extends \Exception
{

    public function __construct(string $listenerId)
    {
        parent::__construct(sprintf('Listener %s has been stopped!', $listenerId));
    }
}
