<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Store\Repository;

use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Store\EventFilter;

interface EventRepositoryInterface
{
    public function add(object $event): void;

    /**
     * @return EventEnvelope[]
     */
    public function find(?EventFilter $filter = null): \Iterator;

    public function publish(EventEnvelope $envelope, callable $publisher): void;

    public function count(): int;
}
