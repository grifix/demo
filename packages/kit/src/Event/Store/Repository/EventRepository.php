<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Store\Repository;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Store\EventFilter;
use Grifix\Kit\Event\Store\Repository\Exception\EventClassNotFoundException;
use Grifix\Kit\Event\Store\Repository\Exception\EventTypeNotFoundException;
use Grifix\Kit\Normalizer\Normalizer;
use Grifix\Kit\Normalizer\NormalizerInterface;
use Grifix\Kit\Type\DateTime\DateTime;
use Grifix\Kit\Uuid\UuidGeneratorInterface;
use Grifix\Shared\Application\Common\Clock\ClockInterface;
use Grifix\Shared\Infrastructure\Table;

class EventRepository implements EventRepositoryInterface
{
    protected const TABLE = 'grifix_kit.event';
    protected const NEXT_EVENT_NUMBER_TABLE = 'grifix_kit.next_event_number';

    protected int $chunkSize = 100;

    public function __construct(
        protected ConnectionInterface $connection,
        protected UuidGeneratorInterface $uuidGenerator,
        protected ClockInterface $clock,
        protected NormalizerInterface $normalizer,
        protected array $eventsMap
    ) {
    }

    public function add(object $event): void
    {
        if (false === $this->connection->isInTransaction()) {
            $this->connection->beginTransaction();
        }

        $eventNumber = $this->getNextEventNumber();

        $this->connection->execute(
            sprintf(
                '
                insert into %s (
                    id, 
                    type, 
                    data, 
                    created_at,
                    number
                ) 
                values (
                    :id,
                    :type,
                    :data,
                    :created_at,
                    :number
                ) 
                   ',
                self::TABLE
            ),
            [
                'id' => $this->uuidGenerator->generateUuid4(),
                'type' => $this->getEventType(get_class($event)),
                'data' => $this->serialize($event),
                'created_at' => $this->clock->getDate()->format('Y-m-d H:i:s.u'),
                'number' => $eventNumber
            ]
        );
    }

    /**
     * @return EventEnvelope[]
     */
    public function find(?EventFilter $filter = null): \Iterator
    {
        null !== $filter ?: $filter = EventFilter::create();
        $offset = 0;
        while ($this->fetchChunk($offset, $this->chunkSize, $filter)) {
            foreach ($this->fetchChunk($offset, $this->chunkSize, $filter) as $record) {
                yield $this->createEventEnvelope($record);
            }
            $offset = $offset + $this->chunkSize;
        }
    }

    protected function getNextEventNumber(): int
    {
        $row = $this->connection->fetch(
            sprintf('select value from %s where id = 1 for update', self::NEXT_EVENT_NUMBER_TABLE)
        );
        $result = (int)$row['value'];

        $this->connection->update(
            ['value' => $result+1],
            self::NEXT_EVENT_NUMBER_TABLE,
            1
        );

        return $result;
    }

    protected function fetchChunk(int $offset, int $limit, EventFilter $filter): array
    {
        $query = $this->connection->createQuery()->select('*')
            ->from(self::TABLE)
            ->orderBy('number asc')
            ->offset($offset)
            ->limit($limit);

        if (true === $filter->getIsPublished()) {
            $query->where('published_at is not null');
        }
        if (false === $filter->getIsPublished()) {
            $query->where('published_at is null');
        }
        if (null !== $filter->getFromNumber()) {
            $query->where('number >= :from_number')->bindValue('from_number', $filter->getFromNumber());
        }
        if (null !== $filter->getToNumber()) {
            $query->where('number <= :to_number')->bindValue('to_number', $filter->getToNumber());
        }

        if (null !== $filter->getId()) {
            $query->where('id = :id')->bindValue('id', $filter->getId());
        }

        return $query->fetchAll();
    }

    public function publish(EventEnvelope $envelope, callable $publisher): void
    {
        $this->connection->beginTransaction();
        $this->connection->execute(
            sprintf('update %s set published_at = :published_at where id = :id', Table::EVENT),
            [
                'published_at' => (new DateTime())->format('Y-m-d H:i:s.u'),
                'id' => $envelope->getId()
            ]
        );
        $publisher($envelope);
        if ($this->connection->isInTransaction()) {
            $this->connection->commitTransaction(true);
        }
    }

    public function count(): int
    {
        return (int )$this->connection->fetch(
            sprintf('select max(number) as max_number from %s', self::TABLE)
        )['max_number'];
    }

    protected function createEventEnvelope(array $record): EventEnvelope
    {
        return new EventEnvelope(
            $record['id'],
            $this->deserialize($record['data'], $record['type']),
            $record['created_at'],
            $record['number'],
        );
    }

    protected function getEventType(string $eventClass): string
    {
        foreach ($this->eventsMap as $type => $class) {
            if ($eventClass === $class) {
                return $type;
            }
        }
        throw new EventTypeNotFoundException($eventClass);
    }

    protected function getEventClass(string $eventType): string
    {
        if (!isset($this->eventsMap[$eventType])) {
            throw new EventClassNotFoundException($eventType);
        }
        return $this->eventsMap[$eventType];
    }

    protected function serialize(object $event): string
    {
        $array = $this->normalizer->normalize($event);
        unset($array[Normalizer::TYPE_KEY]);
        return json_encode($array);
    }

    protected function deserialize(string $json, string $type): object
    {
        $array = json_decode($json, true);
        $array[Normalizer::TYPE_KEY] = $this->getEventClass($type);
        return $this->normalizer->denormalize($array);
    }
}
