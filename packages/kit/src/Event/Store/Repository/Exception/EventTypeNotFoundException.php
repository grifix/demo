<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Store\Repository\Exception;

final class EventTypeNotFoundException extends \Exception
{
    public function __construct(
        private string $eventClass
    ) {
        parent::__construct(sprintf('Alias for event "%s" has not been found!', $this->eventClass));
    }
}
