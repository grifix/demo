<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Subscription;

use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Subscription\Exception\EventAlreadyReceivedException;
use Grifix\Kit\Event\Subscription\Exception\IncorrectEventNumberException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotActiveException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotSuspendedException;

interface EventSubscriptionInterface
{
    public static function createAlias(string $subscriberType, string $id): string;

    /**
     * @throws SubscriptionIsNotActiveException
     */
    public function listenEvents(): void;

    public function stopListenEvents(): void;

    /**
     * @throws SubscriptionIsNotActiveException
     * @throws SubscriptionIsNotSuspendedException
     */
    public function wakeUp(): void;

    /**
     * @throws SubscriptionIsNotActiveException
     */
    public function suspend(): void;

    public function getAlias(): string;

    public function getId(): string;

    public function getSubscriberType(): string;

    public function isActive(): bool;

    public function reset(?callable $onEventReceived = null): void;
}
