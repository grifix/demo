<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Subscription\Subscriber;

use Grifix\Shared\Application\Subscriber\EventSubscriberInterface;

interface EventSubscriberFactoryInterface
{
    public function create(string $subscriberType, string $subscriptionId): EventSubscriberInterface;
}
