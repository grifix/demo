<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Subscriber;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Shared\Application\Subscriber\EventSubscriberInterface;
use Grifix\Shared\Application\Subscriber\EventualSubscriber;

class EventSubscriberFactory implements EventSubscriberFactoryInterface
{
    protected ClassHelperInterface $classHelper;

    protected IocContainerInterface $iocContainer;

    public function __construct(ClassHelperInterface $classHelper, IocContainerInterface $iocContainer)
    {
        $this->classHelper = $classHelper;
        $this->iocContainer = $iocContainer;
    }

    public function create(string $subscriberType, string $subscriptionId): EventSubscriberInterface
    {
        if ($this->classHelper->isInstanceOf($subscriberType, EventualSubscriber::class)) {
            return $this->iocContainer->createNewInstance($subscriberType, [$subscriptionId]);
        }
        return $this->iocContainer->get($subscriberType);
    }
}
