<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Repository;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Event\Subscription\EventSubscription;
use Grifix\Kit\Event\Subscription\EventSubscriptionFactoryInterface;
use Grifix\Kit\Event\Subscription\EventSubscriptionInterface;
use Grifix\Kit\Event\Subscription\Repository\Exception\SubscriptionNotExistException;

final class EventSubscriptionRepository implements EventSubscriptionRepositoryInterface
{
    protected EventSubscriptionFactoryInterface $eventSubscriptionFactory;

    protected ConnectionInterface $connection;

    public function __construct(
        EventSubscriptionFactoryInterface $eventSubscriptionFactory,
        ConnectionInterface $connection
    )
    {
        $this->eventSubscriptionFactory = $eventSubscriptionFactory;
        $this->connection = $connection;
    }

    public function has(string $subscriberType, string $id): bool
    {
        return false === empty($this->fetchRow($subscriberType, $id));
    }

    public function getDto(string $id): ?EventSubscriptionDto
    {
        $row = $this->connection->createQuery()
            ->select('*')
            ->from(EventSubscription::TABLE)
            ->where('id = :id')
            ->bindValue('id', $id)
            ->fetchOne();
        if (null === $row) {
            return null;
        }

        return new EventSubscriptionDto(
            $row['subscriber_type'],
            $row['id'],
            $row['status'],
            $row['last_received_event_number'],
            $row['last_event_received_at'],
            $row['alias']
        );
    }

    public function get(string $subscriberType, string $id): EventSubscriptionInterface
    {
        $row = $this->fetchRow($subscriberType, $id);
        if (empty($row)) {
            throw new SubscriptionNotExistException(EventSubscription::createAlias($subscriberType, $id));
        }
        return $this->eventSubscriptionFactory->createFromState($row);
    }

    public function find(EventSubscriptionFilter $filter): array
    {
        $result = [];
        $query = $this->connection->createQuery();
        $query
            ->select('*')
            ->forUpdate()
            ->from(EventSubscription::TABLE);

        if (null !== $filter->getStatus()) {
            $query->where('status = :status')->bindValue('status', $filter->getStatus());
        }
        foreach ($query->fetchAll() as $row) {
            $result[] = $this->eventSubscriptionFactory->createFromState($row);
        }
        return $result;
    }

    protected function fetchRow(string $subscriberType, string $id): ?array
    {
        return $this->createQuery(
            EventSubscriptionFilter::create()->withSubscriberType($subscriberType)->withSubscriptionId($id)
        )->fetchOne();
    }

    protected function createQuery(EventSubscriptionFilter $filter): QueryInterface
    {
        $result = $this->connection->createQuery();
        $result
            ->select('*')
            ->forUpdate()
            ->from(EventSubscription::TABLE);

        if (null !== $filter->getStatus()) {
            $result->where('status = :status')->bindValue('status', $filter->getStatus());
        }

        if (null !== $filter->getSubscriptionId()) {
            $result
                ->where('id = :id')
                ->bindValue('id', $filter->getSubscriptionId());
        }

        if (null !== $filter->getSubscriberType()) {
            $result
                ->where('subscriber_type = :subscriberType')
                ->bindValue('subscriberType', $filter->getSubscriberType());
        }

        return $result;
    }
}
