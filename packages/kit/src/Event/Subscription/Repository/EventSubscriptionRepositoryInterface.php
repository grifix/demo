<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Subscription\Repository;

use Grifix\Kit\Event\Subscription\EventSubscription;
use Grifix\Kit\Event\Subscription\EventSubscriptionInterface;

interface EventSubscriptionRepositoryInterface
{
    public function has(string $subscriberType, string $id): bool;

    public function get(string $subscriberType, string $id): EventSubscriptionInterface;

    public function find(EventSubscriptionFilter $filter): array;

    public function getDto(string $id): ?EventSubscriptionDto;
}
