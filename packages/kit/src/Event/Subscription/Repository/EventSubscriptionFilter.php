<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Repository;

use Grifix\Kit\Event\Subscription\EventSubscription;

final class EventSubscriptionFilter
{
    private ?string $status = null;

    private ?string $subscriberType = null;

    private ?string $subscriptionId = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function onlyActive(): self
    {
        $this->status = EventSubscription::STATUS_ACTIVE;
        return $this;
    }

    public function onlySuspended(): self
    {
        $this->status = EventSubscription::STATUS_SUSPENDED;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function withSubscriberType(?string $subscriberType): EventSubscriptionFilter
    {
        $this->subscriberType = $subscriberType;
        return $this;
    }

    public function getSubscriptionId(): ?string
    {
        return $this->subscriptionId;
    }

    public function withSubscriptionId(?string $subscriptionId): EventSubscriptionFilter
    {
        $this->subscriptionId = $subscriptionId;
        return $this;
    }

    public function getSubscriberType(): ?string
    {
        return $this->subscriberType;
    }
}
