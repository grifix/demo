<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Subscription\Repository;

class EventSubscriptionDto
{
    public function __construct(
        private string $subscriberType,
        private string $id,
        private string $status,
        private int $lastReceivedEventNumber,
        private string $lastEventReceivedAt,
        private string $alias
    )
    {
    }

    public function getSubscriberType(): string
    {
        return $this->subscriberType;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getLastReceivedEventNumber(): int
    {
        return $this->lastReceivedEventNumber;
    }

    public function getLastEventReceivedAt(): string
    {
        return $this->lastEventReceivedAt;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }
}
