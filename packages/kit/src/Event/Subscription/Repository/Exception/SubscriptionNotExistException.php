<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Repository\Exception;

final class SubscriptionNotExistException extends \Exception
{

    public function __construct(string $subscriberType)
    {
        parent::__construct(sprintf('Subscription "%s" does not exist!', $subscriberType));
    }
}
