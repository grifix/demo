<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription;

use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Store\EventFilter;
use Grifix\Kit\Event\Store\Exception\ListenerStoppedException;
use Grifix\Kit\Event\Subscription\Exception\EventAlreadyReceivedException;
use Grifix\Kit\Event\Subscription\Exception\IncorrectEventNumberException;
use Grifix\Kit\Event\Subscription\Exception\InvalidSubscriberTypeException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsFinishedException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotActiveException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotResettableException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotSuspendedException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsPermanentException;
use Grifix\Kit\Event\Subscription\Services\EventSubscriptionServicesInterface;
use Grifix\Shared\Application\Subscriber\EventSubscriberInterface;
use Grifix\Shared\Application\Subscriber\ResettableSubscriberInterface;

class EventSubscription implements EventSubscriptionInterface
{
    public const TABLE = 'grifix_kit.subscription';

    public const STATUS_ACTIVE = 'active';

    public const STATUS_SUSPENDED = 'suspended';

    public const STATUS_FINISHED = 'finished';

    /** @var EventSubscriberInterface | ResettableSubscriberInterface */
    protected EventSubscriberInterface $subscriber;

    /** @var callable */
    protected $listener;

    protected string $subscriberType;

    protected string $id;

    protected string $status;

    protected string $alias;

    protected int $lastReceivedEventNumber;

    protected ?string $lastEventReceivedAt = null;

    protected EventSubscriptionServicesInterface $services;

    public function __construct(
        EventSubscriptionServicesInterface $services,
        EventEnvelope $startingEvent,
        string $subscriberType
    ) {
        $this->services = $services;
        $this->assertSubscriberType($subscriberType);
        /** @var EventSubscriberInterface|string $subscriberType */
        $this->id = $subscriberType::createSubscriptionId($startingEvent->getEvent());
        $this->status = self::STATUS_ACTIVE;
        $this->subscriberType = $subscriberType;
        $this->subscriber = $this->services->getEventSubscriberFactory()->create($this->subscriberType, $this->id);
        $this->listener = $this->services->getCallableEventListenerFactory()->create($this->subscriber);
        $this->alias = self::createAlias($this->subscriberType, $this->id);
        $this->lastReceivedEventNumber = 0;
        if ($this->isEventual()) {
            $this->lastReceivedEventNumber = $startingEvent->getNumber() - 1;
        }
        $this->saveState();
    }

    protected function assertSubscriberType(string $subscriberType): void
    {
        if (false === $this->services->getClassHelper()->isInstanceOf(
                $subscriberType,
                EventSubscriberInterface::class
            )) {
            throw new InvalidSubscriberTypeException($subscriberType);
        }
    }

    public static function createAlias(string $subscriberType, string $id): string
    {
        return str_replace('\\', '_', $subscriberType) . '_' . $id;
    }

    protected function saveState(): void
    {
        if ($this->stateExists()) {
            $this->services->getConnection()->update(
                $this->normalizeState(true),
                self::TABLE,
                [
                    'subscriber_type' => $this->subscriberType,
                    'id' => $this->id
                ]
            );
        } else {
            $this->services->getConnection()->insert(
                $this->normalizeState(),
                self::TABLE
            );
        }
    }

    protected function stateExists(): bool
    {
        $query = $this->services->getConnection()
            ->createQuery()
            ->select('*')
            ->from(self::TABLE)
            ->where('subscriber_type = :type')->bindValue('type', $this->subscriberType)
            ->where('id = :id')->bindValue('id', $this->id);
        return false === empty($query->fetchOne());
    }

    protected function normalizeState(bool $forUpdate = false): array
    {
        $result = [
            'id' => $this->id,
            'subscriber_type' => $this->subscriberType,
            'status' => $this->status,
            'last_received_event_number' => $this->lastReceivedEventNumber,
            'last_event_received_at' => $this->lastEventReceivedAt,
            'alias' => $this->alias
        ];
        if ($forUpdate) {
            unset($result['id'], $result['subscriber_type']);
        }
        return $result;
    }

    /**
     * @throws SubscriptionIsNotActiveException
     */
    public function suspend(): void
    {
        $this->assertIsActive();
        $this->status = self::STATUS_SUSPENDED;
        $this->stopListenEvents();
        $this->saveState();
    }

    public function reset(?callable $onEventReceived = null): void
    {
        $this->assertIsResettable();
        $this->assertIsActive();
        $this->suspend();
        $this->subscriber->reset();
        $this->lastReceivedEventNumber = 0;
        $this->saveState();
        $this->receiveNewEvents($onEventReceived, true);
        $this->wakeUp();
    }

    protected function isResettable(): bool
    {
        return $this->subscriber instanceof ResettableSubscriberInterface;
    }

    protected function assertIsResettable(): void
    {
        if (false === $this->isResettable()) {
            throw new SubscriptionIsNotResettableException($this->subscriberType, $this->id);
        }
    }

    protected function assertIsActive(): void
    {
        if (false === $this->isActive()) {
            throw new SubscriptionIsNotActiveException($this->subscriberType, $this->id);
        }
    }

    public function isActive(): bool
    {
        return self::STATUS_ACTIVE === $this->status;
    }

    public function stopListenEvents(): void
    {
        $this->services->getEventStore()->stopListener($this->alias);
    }

    /**
     * @throws SubscriptionIsNotActiveException
     * @throws SubscriptionIsNotSuspendedException
     */
    public function wakeUp(): void
    {
        $this->assertIsSuspended();
        $this->status = self::STATUS_ACTIVE;
        $this->saveState();
    }

    protected function assertIsSuspended(): void
    {
        if (false === $this->isSuspended()) {
            throw new SubscriptionIsNotSuspendedException($this->subscriberType, $this->id);
        }
    }

    protected function isSuspended(): bool
    {
        return self::STATUS_SUSPENDED === $this->status;
    }

    /**
     * @throws SubscriptionIsNotActiveException
     */
    public function listenEvents(): void
    {
        $this->assertIsActive();
        $this->receiveNewEvents();


        try {
            $this->services->getEventStore()->startListener(
                function (EventEnvelope $envelope): void {
                    try {
                        $this->receiveEvent($envelope);
                    } catch (EventAlreadyReceivedException $exception) {
                        $this->services->getLogger()->notice($exception->getMessage());
                    } catch (IncorrectEventNumberException $exception) {
                        $this->services->getLogger()->warning($exception->getMessage());
                        $this->receiveMissedEvents($envelope->getNumber());
                    } finally {
                        if (false === $this->services->getConnection()->isInTransaction()) { //if queues are in sync mode
                            $this->services->getConnection()->disconnect();
                        }
                    }
                },
                $this->alias,
                true
            );
        } catch (ListenerStoppedException $exception) {
            if ($this->isActive()) {
                throw $exception;
            }
        }
    }

    protected function receiveNewEvents(?callable $onEventReceived = null, bool $force = false): void
    {
        foreach ($this->fetchNewEvents() as $envelope) {
            try{
                $this->receiveEvent($envelope, $force, $onEventReceived);
            }
            catch (EventAlreadyReceivedException){
                return;
            }
        }
    }

    protected function receiveMissedEvents(int $missedEventNumber): void
    {
        foreach ($this->fetchMissedEvents($missedEventNumber) as $envelope) {
            $this->receiveEvent($envelope);
        }
    }

    /**
     * @return EventEnvelope[]
     */
    protected function fetchNewEvents(): \Iterator
    {
        return $this->services->getEventStore()
            ->fetch(EventFilter::create()->setFromNumber($this->lastReceivedEventNumber + 1));
    }

    /**
     * @return EventEnvelope[]
     */
    protected function fetchMissedEvents(int $missedEventNumber): \Iterator
    {
        return $this->services->getEventStore()
            ->fetch(
                EventFilter::create()
                    ->setFromNumber($this->lastReceivedEventNumber + 1)
                    ->setToNumber($missedEventNumber)
            );
    }

    /**
     * @throws EventAlreadyReceivedException
     * @throws IncorrectEventNumberException
     * @throws SubscriptionIsFinishedException
     * @throws SubscriptionIsNotActiveException
     */
    protected function receiveEvent(
        EventEnvelope $envelope,
        bool $force = false,
        callable $onEventReceived = null
    ): void {
        $this->assertIsNotFinished();
        if (false === $force) {
            $this->assertIsActive();
        }
        $this->assertEventIsNotAlreadyReceived($envelope);
        $this->assertEventNumberIsCorrect($envelope);
        $this->lastReceivedEventNumber = $envelope->getNumber();
        $this->lastEventReceivedAt = $this->services->getClock()->getDate()->format('Y-m-d H:i:s.u');

        $this->services->getConnection()->beginTransaction();
        $this->saveState();
        if ($this->subscriber->shouldReceiveEvent($envelope->getEvent())) {
            if ($this->isFinishingEvent($envelope)) {
                $this->finish();
            }
            $listener = $this->listener;
            $listener($envelope->getEvent());
        }
        if ($this->services->getConnection()->isInTransaction()) {
            $this->services->getConnection()->commitTransaction();
        }
        if (null !== $onEventReceived) {
            $onEventReceived($envelope);
        }
    }

    protected function assertIsNotFinished(): void
    {
        if ($this->isFinished()) {
            throw new SubscriptionIsFinishedException($this->subscriberType, $this->id);
        }
    }

    protected function isFinished(): bool
    {
        return self::STATUS_FINISHED === $this->status;
    }

    protected function assertEventIsNotAlreadyReceived(EventEnvelope $envelope): void
    {
        if ($this->isEventAlreadyReceived($envelope)) {
            throw new EventAlreadyReceivedException(
                $this->subscriberType,
                $this->id,
                get_class($envelope->getEvent()),
                $envelope->getId()
            );
        }
    }

    protected function isEventAlreadyReceived(EventEnvelope $envelope): bool
    {
        return $envelope->getNumber() <= $this->lastReceivedEventNumber;
    }

    protected function assertEventNumberIsCorrect(EventEnvelope $envelope): void
    {
        if (false === $this->isEventNumberCorrect($envelope)) {
            throw new IncorrectEventNumberException(
                $this->subscriberType,
                $this->id,
                $this->lastReceivedEventNumber,
                $envelope->getId(),
                get_class($envelope->getEvent()),
                $envelope->getNumber()
            );
        }
    }

    protected function isEventNumberCorrect(EventEnvelope $envelope): bool
    {
        return $envelope->getNumber() === $this->lastReceivedEventNumber + 1;
    }

    protected function isFinishingEvent(EventEnvelope $envelope)
    {
        return in_array(get_class($envelope->getEvent()), $this->subscriber::getFinishingEvents());
    }

    protected function finish(): void
    {
        $this->assertIsNotPermanent();
        $this->assertIsActive();
        $this->status = self::STATUS_FINISHED;
        $this->saveState();
        $this->stopListenEvents();
    }

    protected function assertIsNotPermanent(): void
    {
        if ($this->isPermanent()) {
            throw new SubscriptionIsPermanentException($this->subscriberType, $this->id);
        }
    }

    protected function isPermanent(): bool
    {
        /** @var EventSubscriberInterface $type */
        return count($this->subscriber::getFinishingEvents()) === 0;
    }

    protected function isEventual(): bool
    {
        return false === $this->isPermanent();
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSubscriberType(): string
    {
        return $this->subscriberType;
    }
}
