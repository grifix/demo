<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription;

use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Subscription\Services\EventSubscriptionServicesInterface;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObjectFactoryInterface;

final class EventSubscriptionFactory implements EventSubscriptionFactoryInterface
{

    protected ReflectionObjectFactoryInterface $reflectionObjectFactory;

    protected EventSubscriptionServicesInterface $eventSubscriberServices;

    public function __construct(
        ReflectionObjectFactoryInterface $reflectionObjectFactory,
        EventSubscriptionServicesInterface $eventSubscriptionServices

    ) {
        $this->reflectionObjectFactory = $reflectionObjectFactory;
        $this->eventSubscriberServices = $eventSubscriptionServices;
    }

    public function create(
        EventEnvelope $startingEvent,
        string $subscriberType
    ): EventSubscriptionInterface {
        return new EventSubscription(
            $this->eventSubscriberServices,
            $startingEvent,
            $subscriberType
        );
    }

    public function createFromState(array $state): EventSubscriptionInterface
    {
        $reflection = new \ReflectionClass(EventSubscription::class);
        /** @var EventSubscriptionInterface $result */
        $result = $reflection->newInstanceWithoutConstructor();
        $reflectionObject = $this->reflectionObjectFactory->create($result);
        $subscriber = $this->eventSubscriberServices->getEventSubscriberFactory()
            ->create($state['subscriber_type'], $state['id']);

        $reflectionObject->setPropertyValue('subscriber', $subscriber);
        $reflectionObject->setPropertyValue(
            'listener',
            $this->eventSubscriberServices->getCallableEventListenerFactory()->create($subscriber)
        );
        $reflectionObject->setPropertyValue('services', $this->eventSubscriberServices);
        $reflectionObject->setPropertyValue('subscriberType', $state['subscriber_type']);
        $reflectionObject->setPropertyValue('id', $state['id']);
        $reflectionObject->setPropertyValue('status', $state['status']);
        $reflectionObject->setPropertyValue('alias', $state['alias']);
        $reflectionObject->setPropertyValue('lastReceivedEventNumber', $state['last_received_event_number']);
        $reflectionObject->setPropertyValue('lastEventReceivedAt', $state['last_event_received_at']);

        return $result;
    }
}
