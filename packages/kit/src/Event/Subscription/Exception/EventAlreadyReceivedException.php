<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Exception;

final class EventAlreadyReceivedException extends \Exception
{

    public function __construct(string $subscriberType, string $subscriptionId, string $eventType, string $eventId)
    {
        parent::__construct(sprintf(
            'Event "%s %s" has been already received by the subscription "%s %s"',
            $eventType,
            $eventId,
            $subscriberType,
            $subscriptionId
        ));
    }
}
