<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Exception;

final class IncorrectEventNumberException extends \Exception
{

    public function __construct(
        string $subscriberType,
        string $subscriptionId,
        int $lastReceivedEventNumber,
        string $eventId,
        string $eventType,
        int $eventNumber
    ) {
        parent::__construct(sprintf(
            'Event "%s %s" has incorrect number "%d" the last received event number' .
            'by subscription "%s %s" is "%d"!',
            $eventType,
            $eventId,
            $eventNumber,
            $subscriberType,
            $subscriptionId,
            $lastReceivedEventNumber
        ));
    }
}
