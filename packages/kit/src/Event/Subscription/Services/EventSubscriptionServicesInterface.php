<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Subscription\Services;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Event\CallableEventListener\CallableEventListenerFactoryInterface;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Event\Subscription\Subscriber\EventSubscriberFactoryInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Shared\Application\Common\Clock\ClockInterface;
use Psr\Log\LoggerInterface;

interface EventSubscriptionServicesInterface
{
    public function getEventStore(): EventStoreInterface;

    public function getConnection(): ConnectionInterface;

    public function getClassHelper(): ClassHelperInterface;

    public function getClock(): ClockInterface;

    public function getLogger(): LoggerInterface;

    public function getEventSubscriberFactory(): EventSubscriberFactoryInterface;

    public function getCallableEventListenerFactory(): CallableEventListenerFactoryInterface;
}
