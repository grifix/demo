<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Subscription\Services;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Event\CallableEventListener\CallableEventListenerFactoryInterface;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Event\Subscription\Subscriber\EventSubscriberFactoryInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Shared\Application\Common\Clock\ClockInterface;
use Psr\Log\LoggerInterface;

final class EventSubscriptionServices implements EventSubscriptionServicesInterface
{
    protected EventStoreInterface $eventStore;

    protected ConnectionInterface $connection;

    protected ClassHelperInterface $classHelper;

    protected ClockInterface $clock;

    protected LoggerInterface $logger;

    protected EventSubscriberFactoryInterface $eventSubscriberFactory;

    protected CallableEventListenerFactoryInterface $callableEventListenerFactory;

    public function __construct(
        EventStoreInterface $eventStore,
        ConnectionInterface $connection,
        ClassHelperInterface $classHelper,
        ClockInterface $clock,
        LoggerInterface $logger,
        EventSubscriberFactoryInterface $eventSubscriberFactory,
        CallableEventListenerFactoryInterface $callableEventListenerFactory
    ) {
        $this->eventStore = $eventStore;
        $this->connection = $connection;
        $this->classHelper = $classHelper;
        $this->clock = $clock;
        $this->logger = $logger;
        $this->eventSubscriberFactory = $eventSubscriberFactory;
        $this->callableEventListenerFactory = $callableEventListenerFactory;
    }

    public function getEventStore(): EventStoreInterface
    {
        return $this->eventStore;
    }

    public function getConnection(): ConnectionInterface
    {
        return $this->connection;
    }

    public function getClassHelper(): ClassHelperInterface
    {
        return $this->classHelper;
    }

    public function getClock(): ClockInterface
    {
        return $this->clock;
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function getEventSubscriberFactory(): EventSubscriberFactoryInterface
    {
        return $this->eventSubscriberFactory;
    }

    public function getCallableEventListenerFactory(): CallableEventListenerFactoryInterface
    {
        return $this->callableEventListenerFactory;
    }
}
