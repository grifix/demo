<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Bus;

use Grifix\Kit\Reflection\ReflectionObject\ReflectionObject;

/**
 * Class EventBus
 * @package Grifix\Kit\EventBus
 */
class EventBus implements EventBusInterface
{
    /** @var callable[] */
    protected array $listeners = [];

    protected array $listenerEvents = [];


    public function send(object $event): void
    {
        foreach ($this->listeners as $i => $listener) {
            if (null === $this->listenerEvents[$i] || $this->listenerEvents[$i] === get_class($event)) {
                $listener($event);
            }
        }
    }

    public function addListener(callable $listener): void
    {
        $this->listeners[] = $listener;
        $this->listenerEvents[] = $this->getListenerEvent($listener);
    }

    protected function getListenerEvent($listener): ?string
    {
        $reflection = new ReflectionObject($listener);
        $method = null;
        foreach ($reflection->getPublicMethods() as $method) {
            if ($method->getName() === '__invoke') {
                break;
            }
            $method = null;
        }
        $parameter = $method->getParameters()[0];
        if (null === $parameter->getType()) {
            return null;
        }
        return $parameter->getType()->getName();
    }
}
