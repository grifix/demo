<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Bus;

use Grifix\Kit\Event\Bus\EventBusInterface;

/**
 * Class EventBusFactory
 * @package Grifix\Kit\EventBus
 */
interface EventBusFactoryInterface
{
    /**
     * @return EventBusInterface
     */
    public function create(): EventBusInterface;
}
