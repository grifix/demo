<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\Event\Bus;

use Grifix\Kit\Event\Bus\ListenerRepository\EventListenerRepositoryInterface;
use Grifix\Kit\Event\CallableEventListener\CallableEventListenerFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;

class EventBusFactory implements EventBusFactoryInterface
{
    protected IocContainerInterface $iocContainer;

    protected EventListenerRepositoryInterface $eventListenerRepository;

    protected CallableEventListenerFactoryInterface $callableEventListenerFactory;

    public function __construct(
        IocContainerInterface $iocContainer,
        EventListenerRepositoryInterface $eventListenerRepository,
        CallableEventListenerFactoryInterface $callableEventListenerFactory
    ) {
        $this->iocContainer = $iocContainer;
        $this->eventListenerRepository = $eventListenerRepository;
        $this->callableEventListenerFactory = $callableEventListenerFactory;
    }

    public function create(): EventBusInterface
    {
        $result = new EventBus();
        foreach ($this->eventListenerRepository->find() as $listenerClass) {
            $result->addListener(function ($event) use (
                $listenerClass
            ) {
                $callable = $this->callableEventListenerFactory->create($this->iocContainer->get($listenerClass));
                $callable($event);
            });
        }
        return $result;
    }
}
