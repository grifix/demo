<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Bus\Exception;

final class ListenerHasNoEventTypeException extends \InvalidArgumentException
{

    public function __construct(string $listenerType)
    {
        parent::__construct(sprintf('Listener %s has no specified event type', $listenerType));
    }
}
