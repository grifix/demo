<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\Bus\ListenerRepository;

use Grifix\Kit\ClassFetcher\ClassFetcher\ClassFetcherInterface;

class EventListenerRepository implements EventListenerRepositoryInterface
{
    protected ClassFetcherInterface $classFetcher;

    public function __construct(ClassFetcherInterface $classFetcher)
    {
        $this->classFetcher = $classFetcher;
    }

    public function find(): array
    {
        return $this->classFetcher->fetchClasses('\Application\Listener');
    }
}
