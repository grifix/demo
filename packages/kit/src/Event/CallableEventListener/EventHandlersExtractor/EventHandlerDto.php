<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\CallableEventListener\EventHandlersExtractor;

final class EventHandlerDto
{
    private string $listenerType;

    private string $method;

    private ?string $eventType;

    public function __construct(string $listenerType, string $method, ?string $eventType)
    {
        $this->listenerType = $listenerType;
        $this->method = $method;
        $this->eventType = $eventType;
    }

    public function getListenerType(): string
    {
        return $this->listenerType;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getEventType(): ?string
    {
        return $this->eventType;
    }
}
