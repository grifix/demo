<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\CallableEventListener\EventHandlersExtractor;

class EventHandlersExtractor implements EventHandlersExtractorInterface
{
    protected static ?self $instance = null;

    public static function getInstance(): self
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return EventHandlerDto[]
     */
    public function extract(string $listenerType): array
    {
        $result = [];
        $reflection = new \ReflectionClass($listenerType);
        foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            if ($this->isEventHandler($method)) {
                $result[] = new EventHandlerDto($listenerType, $method->getName(), $this->getEventType($method));
            }
        }
        return $result;
    }

    protected function isEventHandler(\ReflectionMethod $method): bool
    {
        if (strpos($method->getName(), 'on') === 0) {
            return true;
        }
        if ($method->getName() === '__invoke') {
            return true;
        }
        return false;
    }

    protected function getEventType(\ReflectionMethod $method): ?string
    {
        $type = $method->getParameters()[0]->getType();
        if (null !== $type) {
            return $type->getName();
        }
        return null;
    }
}
