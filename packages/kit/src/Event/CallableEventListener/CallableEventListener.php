<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\CallableEventListener;

use Grifix\Kit\Event\CallableEventListener\EventHandlersExtractor\EventHandlersExtractorInterface;

class CallableEventListener
{
    protected object $listener;

    /** @var callable[] */
    protected array $handlers;

    public function __construct(object $listener, EventHandlersExtractorInterface $eventHandlersExtractor)
    {
        $this->listener = $listener;
        foreach ($eventHandlersExtractor->extract(get_class($listener)) as $eventHandler) {
            $method = $eventHandler->getMethod();
            $this->handlers[$eventHandler->getEventType()] = function (object $event) use ($listener, $method) {
                $listener->$method($event);
            };
        }
    }

    public function __invoke(object $event): void
    {
        $eventType = get_class($event);
        if ($this->isEventSupported($eventType)) {
            $handler = $this->getEventHandler($eventType);
            $handler($event);
        }
    }

    protected function getEventHandler(string $eventType): callable
    {
        return $this->handlers[$eventType];
    }

    protected function isEventSupported(string $eventType): bool
    {
        return array_key_exists($eventType, $this->handlers);
    }
}
