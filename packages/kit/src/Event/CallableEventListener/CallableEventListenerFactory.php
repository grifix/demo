<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\CallableEventListener;

use Grifix\Kit\Event\CallableEventListener\EventHandlersExtractor\EventHandlersExtractorInterface;

class CallableEventListenerFactory implements CallableEventListenerFactoryInterface
{
    protected EventHandlersExtractorInterface $eventHandlersExtractor;

    public function __construct(EventHandlersExtractorInterface $eventHandlersExtractor)
    {
        $this->eventHandlersExtractor = $eventHandlersExtractor;
    }

    public function create(object $listener): callable
    {
        return new CallableEventListener($listener, $this->eventHandlersExtractor);
    }
}
