<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\SubscriberType;

use Grifix\Kit\ClassFetcher\ClassFetcher\ClassFetcherInterface;
use Grifix\Shared\Application\Subscriber\EventSubscriberInterface;

final class EventSubscriberTypeRepository implements EventSubscriberTypeRepositoryInterface
{
    protected ClassFetcherInterface $classFetcher;

    public function __construct(ClassFetcherInterface $classFetcher)
    {
        $this->classFetcher = $classFetcher;
    }

    /** @return EventSubscriberType[] */
    public function find(): array
    {
        $result = [];
        foreach ($this->fetchTypes() as $type) {
            $result[] = new EventSubscriberType($type);
        }
        return $result;
    }

    protected function fetchTypes(): array
    {
        return $this->classFetcher->fetchClasses(
            '\Application\Subscriber',
            EventSubscriberInterface::class,
            true
        );
    }
}
