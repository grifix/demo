<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Event\SubscriberType;

use Grifix\Kit\Event\Subscription\EventSubscription;
use Grifix\Shared\Application\Subscriber\EventSubscriberInterface;

final class EventSubscriberType
{
    /** @var string|EventSubscriberInterface */
    protected string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function isEventSupported(object $event): bool
    {
        return $this->name::isEventSupported($event);
    }

    public function createSubscriptionId(object $event): string
    {
        return $this->name::createSubscriptionId($event);
    }

    public function createSubscriptionAlias(string $subscriptionId): string
    {
        return EventSubscription::createAlias($this->name, $subscriptionId);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isStartingEvent(object $event): bool
    {
        if (empty($this->name::getStaringEvents())) {
            return true;
        }
        return in_array(get_class($event), $this->name::getStaringEvents());
    }

    public function isPermanent(): bool
    {
        return empty($this->name::getFinishingEvents());
    }
}
