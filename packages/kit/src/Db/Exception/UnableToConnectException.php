<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db\Exception;

use PDOException;

/**
 * Class UnableToConnectException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UnableToConnectException extends DbException
{
    /**
     * UnableToConnectException constructor.
     *
     * @param PDOException $previous
     */
    public function __construct(PDOException $previous = null)
    {
        $this->message = 'Unable to connect to the database: ' . $previous->getMessage();
        parent::__construct($this->message, $previous->getCode(), $previous);
    }
}
