<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Exception;

/**
 * Class CantSelectQueryException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CantSelectQueryException extends DbException
{
    
    /**
     * @var string
     */
    protected $sql;
    
    /**
     * CantSelectQueryException constructor.
     *
     * @param string $sql
     */
    public function __construct(string $sql)
    {
        $this->sql = $sql;
        $this->message = 'Can\'t select query "' . $sql . '!';
        parent::__construct();
    }
}
