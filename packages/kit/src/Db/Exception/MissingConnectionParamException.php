<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Exception;

use Exception;

/**
 * Class MissingConnectionParamException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MissingConnectionParamException extends DbException
{
    protected $paramName;
    
    /**
     * MissingConnectionParamException constructor.
     *
     * @param string $config
     */
    public function __construct(string $config)
    {
        $this->paramName = $config;
        $this->message = 'Missing connection config "'.$config.'"';
        parent::__construct();
    }
}