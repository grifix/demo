<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Exception;

use Grifix\Kit\Db\StatementInterface;

/**
 * Class CantCountStatementException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CantCountStatementException extends DbException
{
    /**
     * @var StatementInterface
     */
    protected $statement;
    
    public function __construct(StatementInterface $statement)
    {
        $this->statement = $statement;
        $this->message = "Can't count statement with query ".$statement->getSql();
        
        parent::__construct();
    }
}
