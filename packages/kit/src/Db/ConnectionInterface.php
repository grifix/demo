<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Db\Query\Processor\ProcessorInterface as QueryProcessorInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use PDO;

/**
 * Class AbstractConnection
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ConnectionInterface
{

    /**
     * @param string $sql
     *
     * @param array $bindings
     *
     * @return StatementInterface
     */
    public function createStatement(string $sql, array $bindings = []): StatementInterface;

    /**
     * @return PDO
     */
    public function getConnection():PDO;

    /**
     * @param string|null $table
     * @param string $primaryKey
     *
     * @return mixed
     */
    public function getLastInsertedId(string $table = null, string $primaryKey = 'id');

    public function execute(string $sql, array $bindings = [], ?string $delimiter = ';'): ConnectionInterface;

    /**
     * @return ConnectionInterface
     */
    public function beginTransaction(): ConnectionInterface;

    /**
     * @return ConnectionInterface
     */
    public function commitTransaction(bool $force = false): ConnectionInterface;

    /**
     * @return ConnectionInterface
     */
    public function rollBackTransaction(): ConnectionInterface;

    /**
     * @return QueryInterface
     */
    public function createQuery(): QueryInterface;

    /**
     * @return QueryProcessorInterface
     */
    public function getQueryProcessor(): QueryProcessorInterface;


    /**
     * @param string $table
     *
     * @param string $column
     *
     * @return int
     */
    public function allocateId(string $table, string $column = 'id'): int;


    /**
     * @param QueryInterface $query
     *
     * @return StatementInterface
     */
    public function createStatementFromQuery(QueryInterface $query): StatementInterface;

    /**
     * @param array $data
     * @param string $table
     * @param bool $returnLastId
     *
     * @return mixed
     * @throws Exception\StatementExecuteException
     */
    public function insert(array $data, string $table, bool $returnLastId = false);

    /**
     * @param string $table
     * @param array $conditions
     * @throws Exception\StatementExecuteException
     */
    public function delete(string $table, array $conditions = []): void;

    /**
     * @param string $sql
     * @param array $bindings
     * @return array
     * @throws Exception\StatementExecuteException
     */
    public function fetchAll(string $sql, array $bindings = []): array;

    /**
     * @param array $data
     * @param string $table
     * @param mixed|array $id
     *
     * @throws Exception\StatementExecuteException
     */
    public function update(array $data, string $table, $id): void;

    public function truncate(string $table);

    public function getParams(): array;

    public function disconnect(): void;

    public function isInTransaction(): bool;

    public function fetch(string $sql, array $bindings = []): ?array;

    public function connect(): void;

    public function isTableExist(string $schema, string $table): bool;

}
