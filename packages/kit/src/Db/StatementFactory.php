<?php

declare(strict_types = 1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Event\Bus\EventBusInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\SimpleCache\CacheInterface;

class StatementFactory extends AbstractFactory implements StatementFactoryInterface
{
    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * @var CacheInterface
     */
    protected $cache;

    public function __construct(
        EventBusInterface $eventBus,
        CacheInterface $cache,
        ClassMakerInterface $classMaker
    ) {
        $this->eventBus = $eventBus;
        $this->cache = $cache;
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ConnectionInterface $connection, string $sql, array $bindings = []): StatementInterface
    {
        $class = $this->makeClassName(Statement::class);

        return new $class($sql, $connection, $this, $this->eventBus, $this->cache, $bindings);
    }
}
