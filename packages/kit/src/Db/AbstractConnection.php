<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Db\Event\TransactionCommitedEvent;
use Grifix\Kit\Db\Event\TransactionStartedEvent;
use Grifix\Kit\Db\Event\TransactionWasRollbackEvent;
use Grifix\Kit\Db\Exception\MissingConnectionParamException;
use Grifix\Kit\Db\Exception\UnableToConnectException;
use Grifix\Kit\Db\Query\Processor\ProcessorInterface as QueryProcessorInterface;
use Grifix\Kit\Db\Query\QueryFactoryInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Event\Bus\EventBusInterface;
use PDO;

/**
 * Class AbstractConnection
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractConnection implements ConnectionInterface
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * @var array
     */
    protected $params;

    /**
     * @var StatementFactoryInterface
     */
    protected $statementFactory;

    /**
     * @var int
     */
    protected $transactionsCounter = 0;

    /**
     * @var QueryProcessorInterface
     */
    protected $queryProcessor;

    /**
     * @var QueryFactoryInterface
     */
    protected $queryFactory;

    /**
     * AbstractConnection constructor.
     *
     * @param EventBusInterface $eventBus
     * @param StatementFactoryInterface $statementFactory
     * @param QueryProcessorInterface $queryProcessor
     * @param QueryFactoryInterface $queryFactory
     * @param array $params
     *
     * @throws UnableToConnectException
     */
    public function __construct(
        EventBusInterface $eventBus,
        StatementFactoryInterface $statementFactory,
        QueryProcessorInterface $queryProcessor,
        QueryFactoryInterface $queryFactory,
        array $params
    ) {
        $this->eventBus = $eventBus;
        $this->params = $params;
        $this->statementFactory = $statementFactory;
        $this->queryProcessor = $queryProcessor;
        $this->queryProcessor = $queryProcessor;
        $this->queryFactory = $queryFactory;
    }

    public function getConnection(): PDO
    {
        if (null === $this->pdo) {
            try {
                $this->pdo = new PDO(
                    $this->createDsn(),
                    $this->params['user'],
                    $this->params['password'],
                    [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    ]
                );
            } catch (\PDOException $e) {
                throw new UnableToConnectException($e);
            }
        }
        return $this->pdo;
    }

    public function connect(): void
    {
        $this->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    public function getQueryProcessor(): QueryProcessorInterface
    {
        return $this->queryProcessor;
    }

    public function disconnect(): void
    {
        $this->pdo = null;
        $this->transactionsCounter = 0;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * {@inheritdoc}
     */
    public function beginTransaction(): ConnectionInterface
    {
        if ($this->transactionsCounter === 0) {
            $this->getConnection()->beginTransaction();
            $this->eventBus->send(new TransactionStartedEvent());
        }
        $this->transactionsCounter++;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function commitTransaction(bool $force = false): ConnectionInterface
    {
        if ($this->transactionsCounter == 1 || true === $force) {
            $this->getConnection()->commit();
            $this->eventBus->send(new TransactionCommitedEvent());
            $this->transactionsCounter = 1;
        }
        $this->transactionsCounter--;

        return $this;
    }

    public function isInTransaction(): bool
    {
        return (bool)$this->transactionsCounter;
    }

    /**
     * {@inheritdoc}
     */
    public function rollBackTransaction(): ConnectionInterface
    {
        if ($this->transactionsCounter) {
            $this->getConnection()->rollBack();
            $this->eventBus->send(new TransactionWasRollbackEvent());
            $this->transactionsCounter = 0;
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getRequiredParamsNames()
    {
        return [
            'user',
            'password',
        ];
    }

    /**
     * @param string $table
     * @param string $column
     *
     * @return string
     */
    abstract protected function getSequenceName(string $table, string $column = 'id'): string;

    public function execute(string $sql, array $bindings = [], ?string $delimiter = ';'): ConnectionInterface
    {
        if (null !== $delimiter) {
            $sql = explode("\n", $sql);
            foreach ($sql as $i => $string) {
                if (strpos($string, '--') === 0 || !trim($string)) {
                    unset($sql[$i]);
                }
            }
            $sql = implode(' ', $sql);
        }

        null === $delimiter ? $statements = [$sql] : $statements = explode($delimiter, $sql);
        foreach ($statements as $statement) {
            $statement = trim($statement);
            if ($statement) {
                $this->createStatement($statement, $bindings)->execute();
            }
        }

        return $this;
    }

    /**
     * @param string $sql
     * @param array $bindings
     *
     * @return array
     * @throws Exception\StatementExecuteException
     */
    public function fetchAll(string $sql, array $bindings = []): array
    {
        return $this->createStatement($sql, $bindings)->execute()->fetchAll();
    }

    public function fetch(string $sql, array $bindings = []): ?array
    {
        return $this->createStatement($sql, $bindings)->execute()->fetch();
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery(): QueryInterface
    {
        return $this->queryFactory->create($this);
    }

    /**
     * @param QueryInterface $query
     *
     * @return StatementInterface
     */
    public function createStatementFromQuery(QueryInterface $query): StatementInterface
    {
        $processorResult = $this->getQueryProcessor()->process($query);

        return $this->createStatement($processorResult->getSql(), $processorResult->getBindings());
    }

    /**
     * {@inheritdoc}
     */
    public function insert(array $data, string $table, bool $returnLastId = false)
    {
        $this->createQuery()->insert($data)->into($table)->execute();
        if ($returnLastId) {
            return $this->getLastInsertedId($table);
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function update(array $data, string $table, $id): void
    {
        $query = $this->createQuery()->update($table)->set($data);
        if (is_array($id)) {
            foreach ($id as $key => $value) {
                $query->where(sprintf('%s = :%s', $key, $key))->bindValue($key, $value);
            }
        } else {
            $query->where('id = :id')->bindValue('id', $id);
        }
        $query->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(string $table, array $conditions = []): void
    {
        $query = $this->createQuery()->delete()->from($table);
        foreach ($conditions as $key => $value) {
            $query->where('"' . $key . '" = :' . $key)->bindValue($key, $value);
        }
        $query->execute();
    }

    /**
     * @param string|null $table
     * @param string $primaryKey
     *
     * @return mixed
     */
    public function getLastInsertedId(string $table = null, string $primaryKey = 'id')
    {
        $sequence = null;
        if ($table) {
            $sequence = $this->getSequenceName($table, $primaryKey);
        }

        return $this->getConnection()->lastInsertId($sequence);
    }


    /**
     * @return void
     * @throws MissingConnectionParamException
     */
    protected function checkRequiredParams()
    {
        $requiredParamsNames = $this->getRequiredParamsNames();
        foreach ($requiredParamsNames as $paramName) {
            if (!isset($this->params[$paramName])) {
                throw new MissingConnectionParamException($paramName);
            }
        }
    }

    /**
     * @return mixed
     */
    abstract protected function createDsn();


    /**
     * {@inheritdoc}
     */
    public function createStatement(string $sql, array $bindings = []): StatementInterface
    {
        return $this->statementFactory->create($this, $sql, $bindings);
    }
}
