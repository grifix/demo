<?php

declare(strict_types = 1);

namespace Grifix\Kit\Db;

interface StatementFactoryInterface
{
    public function create(ConnectionInterface $connection, string $sql, array $bindings = []): StatementInterface;
}
