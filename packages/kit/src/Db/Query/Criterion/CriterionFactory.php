<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Criterion;

use Grifix\Kit\Kernel\AbstractFactory;


/**
 * Class CriterionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CriterionFactory extends AbstractFactory implements CriterionFactoryInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function createCriterion($expression = null): CriterionInterface
    {
        $class = $this->makeClassName(Criterion::class);
        /**
         * @var $criterion Criterion;
         */
        $criterion = new $class($this, $expression);
        if (is_callable($expression)) {
            $expression($criterion);
        } else {
            $criterion->setExpression($expression);
        }
        
        return $criterion;
    }
    
    /**
     * {@inheritdoc}
     */
    public function createOrCriterion($expression = null): CriterionInterface
    {
        return $this->createOrWrapper($this->createCriterion($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function createAndCriterion($expression = null): CriterionInterface
    {
        return $this->createAndWrapper($this->createCriterion($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function createOrWrapper(CriterionInterface $criterion): CriterionInterface
    {
        $class = $this->makeClassName(OrCriterion::class);
        
        return new $class($criterion);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createAndWrapper(CriterionInterface $criterion): CriterionInterface
    {
        $class = $this->makeClassName(AndCriterion::class);
        
        return new $class($criterion);
    }
}