<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Part;


/**
 * Class NativePart
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface NativePartInterface
{
    const LOGIC_OR = 1;
    const LOGIC_AND = 2;
    
    /**
     * @return string
     */
    public function getExpression(): string;
    
    /**
     * @return string
     */
    public function getType(): string;
    
    /**
     * @return int
     */
    public function getLogic(): int;
}