<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

/**
 * Class Update
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Update implements UpdateInterface
{
    /**
     * @var string
     */
    protected $table;
    
    /**
     * @var null|string
     */
    protected $alias;
    
    /**
     * Update constructor.
     *
     * @param string      $table
     * @param string|null $alias
     */
    public function __construct(string $table, string $alias = null)
    {
        $this->table = $table;
        $this->alias = $alias;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable(): string
    {
        return $this->table;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return $this->alias;
    }
}