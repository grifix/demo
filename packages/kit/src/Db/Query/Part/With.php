<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\QueryInterface;

/**
 * Class With
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class With implements WithInterface
{
    
    protected $query;
    
    protected $alias;
    
    /**
     * With constructor.
     *
     * @param QueryInterface $query
     * @param string         $alias
     */
    public function __construct(QueryInterface $query, string $alias)
    {
        $this->query = $query;
        $this->alias = $alias;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return $this->query;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
