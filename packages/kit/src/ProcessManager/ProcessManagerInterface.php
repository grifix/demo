<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\ProcessManager;

use Grifix\Kit\ProcessManager\Exception\CannotStartProcessException;
use Grifix\Kit\ProcessManager\Exception\CannotStopProcessException;
use Grifix\Kit\ProcessManager\Exception\ProcessAlreadyExistsException;

interface ProcessManagerInterface
{
    public function processExists(string $processId): bool;

    /**
     * @throws CannotStartProcessException
     * @throws ProcessAlreadyExistsException
     */
    public function startProcess(string $processId, string $command): void;

    /**
     * @throws CannotStopProcessException
     */
    public function stopProcess(string $processId): void;

    public function findProcesses(?string $filter = null): array;
}
