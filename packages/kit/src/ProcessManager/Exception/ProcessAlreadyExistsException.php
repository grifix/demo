<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\ProcessManager\Exception;

final class ProcessAlreadyExistsException extends \Exception
{
    private string $processId;

    public function __construct(string $processId)
    {
        $this->processId = $processId;
        parent::__construct(sprintf('Process %s already exists!', $processId));
    }

    public function getProcessId(): string
    {
        return $this->processId;
    }
}
