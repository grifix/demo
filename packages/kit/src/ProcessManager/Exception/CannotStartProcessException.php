<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\ProcessManager\Exception;

final class CannotStartProcessException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
