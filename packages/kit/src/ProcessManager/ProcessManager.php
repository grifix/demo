<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\ProcessManager;

use Grifix\Kit\ProcessManager\Exception\CannotStartProcessException;
use Grifix\Kit\ProcessManager\Exception\ProcessAlreadyExistsException;
use Grifix\Kit\ProcessManager\Exception\ProcessNotExistsException;

final class ProcessManager implements ProcessManagerInterface
{
    public function processExists(string $processId): bool
    {
        return boolval(shell_exec(sprintf('screen -list | grep "%s"', $processId)));
    }

    /**
     * @throws CannotStartProcessException
     * @throws ProcessAlreadyExistsException
     */
    public function startProcess(string $processId, string $command): void
    {
        $result = null;
        $output = [];
        if ($this->processExists($processId)) {
            throw new ProcessAlreadyExistsException($processId);
        }
        exec(sprintf('screen -dm -S "%s" %s', $processId, $command), $output, $result);

        if ($result !== 0) {
            throw new CannotStartProcessException(implode(' ', $output));
        }
    }

    public function stopProcess(string $processId): void
    {
        if (false === $this->processExists($processId)) {
            throw new ProcessNotExistsException($processId);
        }
        $result = null;
        $output = [];
        exec(sprintf('screen -X -S "%s" quit', $processId), $output, $result);
        if ($result !== 0) {
            throw new CannotStartProcessException(implode(' ', $output));
        }
    }

    public function findProcesses(?string $filter = null): array
    {
        $result = [];
        $command = 'screen -list';
        if ($filter) {
            $command .= sprintf(' | grep "%s"', $filter);
        }
        $output = shell_exec($command);
        if (empty($output)) {
            return [];
        }
        $output = explode("\n", $output);
        if (null === $filter) {
            array_shift($output);
        }
        foreach ($output as $row) {
            if (empty($row)) {
                continue;
            }
            $row = explode('.', $row);
            $row = explode("\t", $row[1]);
            $result[] = $row[0];
        }
        return $result;
    }
}
