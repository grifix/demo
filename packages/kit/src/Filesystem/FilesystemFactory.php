<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Filesystem;

use Grifix\Kit\Kernel\AbstractFactory;
use League\Flysystem\Adapter\Local;

/**
 * Class FilesystemFactory
 *
 * @category Grifix
 * @package  Grifix\FileSystem
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FilesystemFactory extends AbstractFactory implements FilesystemFactoryInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function createLocal($rootDir)
    {
        $Filesystem = $this->makeClassName(Filesystem::class);
        
        return new $Filesystem(new Local($rootDir));
    }
}
