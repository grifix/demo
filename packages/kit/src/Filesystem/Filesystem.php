<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Filesystem;

class Filesystem extends \League\Flysystem\Filesystem implements FilesystemInterface
{
    public function overwrite($path, $contents, array $config = []): bool
    {
        if ($this->has($path)) {
            $this->delete($path);
        }
        
        return $this->write($path, $contents, $config);
    }
}
