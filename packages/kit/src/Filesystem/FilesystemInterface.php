<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Filesystem;

/**
 * Class FileSystemInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Filesystem
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface FilesystemInterface extends \League\Flysystem\FilesystemInterface
{

    public const KB = 1024;
    public const MB = 1024 * self::KB;
    public const GB = 1024 * self::MB;
    public const TB = 1024 * self::GB;
    public const PB = 1024 * self::TB;
    public const EB = 1024 * self::PB;

    /**
     * @param       $path
     * @param       $contents
     * @param array $config
     *
     * @return bool
     */
    public function overwrite($path, $contents, array $config = []): bool;
}
