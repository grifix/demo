<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use League\Pipeline\PipelineInterface;

/**
 * Class WebEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class CliEntryPoint extends AbstractEntryPoint
{
    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createCliApplicationPipeline(!boolval(getenv('GRIFIX_CACHE_ENABLED')));
    }
}
