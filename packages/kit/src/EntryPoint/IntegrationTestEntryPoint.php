<?php
declare(strict_types = 1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Event\Store\EventFilter;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Event\SubscribersStarter\EventSubscribersStarterInterface;
use Grifix\Kit\Stopwatch\StopwatchInterface;
use Grifix\Shared\Ui\Cli\Command\ClearCacheCmd;
use Grifix\Shared\Ui\Cli\Command\Event\Subscriber\EventSubscribersCommand;
use Grifix\Shared\Ui\Cli\Command\Event\Starter\EventSubscribersStartersCommand;
use Grifix\Shared\Ui\Cli\Command\Queue\QueueConsumersCommand;
use League\Pipeline\PipelineInterface;

class IntegrationTestEntryPoint extends AbstractEntryPoint
{

    private const DUMP_PATH = '/tmp/test_dump.sql';

    protected static bool $installed = false;

    protected static bool $queuesRestarted = false;

    protected static bool $syncQueuesWarned = false;

    protected static bool $installationSkippedWarned = false;

    /** @var callable|null */
    protected $afterInitIoc;

    public function __construct(string $rootDir, callable $afterInitIoc = null, string $environment = null)
    {
        parent::__construct($rootDir, $environment);
        $this->afterInitIoc = $afterInitIoc;
    }

    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createCliPipeline(!boolval(getenv('GRIFIX_CACHE_ENABLED')), $this->afterInitIoc);
    }

    /**
     * @noinspection PhpMissingParentCallCommonInspection
     * @return PipelineScopeInterface
     */
    protected function createScope(): PipelineScopeInterface
    {
        return $this->scopeFactory->create(
            $this->rootDir,
            'test'
        );
    }

    protected function install(): PipelineScopeInterface
    {
        echo "Installing application...\n";
        echo shell_exec($this->rootDir . '/cli --env=test '.ClearCacheCmd::NAME);
        ob_flush();
        $scope = $this->createScope();
        $pipeline = $this->pipelineFactory->createInstallPipeline();
        $pipeline($scope);
        if (!getenv('GRIFIX_SYNC_QUEUE_IN_TESTS')) {
            sleep(60);
        }
        echo "Application has been installed.\n";
        ob_flush();
        return $scope;
    }

    protected function restartQueues(): void
    {
        if (!getenv('GRIFIX_SYNC_QUEUE_IN_TESTS')) {
            echo "Restarting all queues...\n";
            echo shell_exec($this->rootDir . '/cli --env=test '.QueueConsumersCommand::NAME.' stop');
            echo shell_exec($this->rootDir . '/cli --env=test '.QueueConsumersCommand::NAME.' start');
            echo shell_exec($this->rootDir . '/cli --env=test '.EventSubscribersCommand::NAME.' stop');
            echo shell_exec($this->rootDir . '/cli --env=test '.EventSubscribersStartersCommand::NAME.' stop');
            echo shell_exec($this->rootDir . '/cli --env=test '.EventSubscribersStartersCommand::NAME.' start');
        } elseif (!self::$syncQueuesWarned) {
            echo "\033[31mWARNING, QUEUES ARE IN SYNC MODE!\n\033[0m";
            self::$syncQueuesWarned = true;
        }
        ob_flush();
    }

    /**
     * @return PipelineScopeInterface
     */
    public function enter(): PipelineScopeInterface
    {
        if (!self::$queuesRestarted) {
            $this->restartQueues();
            self::$queuesRestarted = true;
        }

        $installed = false;
        if ((!self::$installed && !getenv('GRIFIX_SKIP_INSTALL_IN_TESTS')) || !file_exists($this->getDumpPath())) {
            $installationScope = $this->install();
            if (getenv('GRIFIX_SYNC_QUEUE_IN_TESTS')) {
                $installationScope->getIocContainer()->get(EventSubscribersStarterInterface::class)->start();
            }
            $installationScope->getIocContainer()->get(EventStoreInterface::class)->publish(EventFilter::create());
            $this->dumpDatabase($installationScope);
            self::$installed = true;
            $installed = true;
        }

        $result = parent::enter();

        if (getenv('GRIFIX_SYNC_QUEUE_IN_TESTS')) {
            $result->getIocContainer()->get(EventSubscribersStarterInterface::class)->start();
        }

        if (!$installed) {
            $this->restoreDatabase($result);
        }

        if (getenv('GRIFIX_SKIP_INSTALL_IN_TESTS') && !self::$installationSkippedWarned) {
            echo "\033[31mWARNING, INSTALLATION PROCESS WAS SKIPPED, MIGRATIONS WERE NOT PERFORMED, DATABASE WAS RESTORED FROM PREVIOUS DUMP!\n\033[0m";
            self::$installationSkippedWarned = true;
            ob_flush();
        }
        return $result;
    }

    protected function dumpDatabase(PipelineScopeInterface $scope): void
    {
        echo "Dumping database...\n";
        ob_flush();
        $params = $scope->getIocContainer()->get(ConnectionInterface::class)->getParams();

        exec(sprintf(
            'export PGPASSWORD="%s" && pg_dump -h %s -p %s -d %s -U %s -wc --if-exists > %s',
            $params['password'],
            $params['host'],
            $params['port'],
            $params['db'],
            $params['user'],
            $this->getDumpPath()
        ));

        echo "Database has been dumped!\n";
        ob_flush();
    }

    protected function restoreDatabase(PipelineScopeInterface $scope): void
    {
        echo "Restoring database...";
        ob_flush();
        $connection = $scope->getIocContainer()->get(ConnectionInterface::class);
        $params = $connection->getParams();
        $stopwatch = $scope->getIocContainer()->get(StopwatchInterface::class);
        $stopwatch->start();
        exec(sprintf(
            'export PGPASSWORD="%s" && dropdb -h %s -p %s -U %s -f %s',
            $params['password'],
            $params['host'],
            $params['port'],
            $params['user'],
            $params['db'],
        ));

        exec(sprintf(
            'export PGPASSWORD="%s" && createdb -h %s -p %s -U %s %s',
            $params['password'],
            $params['host'],
            $params['port'],
            $params['user'],
            $params['db'],
        ));

        exec(sprintf(
            'export PGPASSWORD="%s" && psql -h %s -p %s -d %s -U %s -w < %s',
            $params['password'],
            $params['host'],
            $params['port'],
            $params['db'],
            $params['user'],
            $this->getDumpPath()
        ));
        echo sprintf(" done in %s seconds!\n", $stopwatch->stop());
        ob_flush();
    }

    protected function getDumpPath()
    {
        return $this->rootDir . self::DUMP_PATH;
    }
}
