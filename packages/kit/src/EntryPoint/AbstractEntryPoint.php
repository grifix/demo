<?php

declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Dotenv\Dotenv;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeFactory;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeFactoryInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Scope\ScopeFactory;
use League\Pipeline\PipelineInterface;

abstract class AbstractEntryPoint implements EntryPointInterface
{

    protected string $rootDir;

    protected PipelineScopeFactoryInterface $scopeFactory;

    protected ClassMakerInterface $classMaker;

    protected PipelineFactoryInterface $pipelineFactory;

    protected ?string $environment = null;


    public function __construct(string $rootDir, string $environment = null)
    {
        $this->rootDir = $rootDir;
        (new Dotenv($this->rootDir))->overload();
        if (null !== $environment) {
            $_ENV['GRIFIX_ENV'] = $environment;
        }
        $this->classMaker = new ClassMaker();
        $this->scopeFactory = new PipelineScopeFactory($this->classMaker, new ScopeFactory($this->classMaker));
        $this->pipelineFactory = new PipelineFactory($this->rootDir);
        $this->environment = $environment;
    }

    abstract protected function createPipeline(): PipelineInterface;

    public function enter(): PipelineScopeInterface
    {
        $pipeline = $this->createPipeline();
        $scope = $this->createScope();
        $pipeline($scope);
        return $scope;
    }

    protected function createScope(): PipelineScopeInterface
    {
        return $this->scopeFactory->create(
            $this->rootDir,
            $this->environment ?? getenv('GRIFIX_ENV')
        );
    }
}
