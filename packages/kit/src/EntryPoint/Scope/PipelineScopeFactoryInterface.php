<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Scope;

use Grifix\Kit\Http\ServerRequestInterface;

interface PipelineScopeFactoryInterface
{
    public function create(
        string $rootDir,
        string $environment,
        ServerRequestInterface $request = null
    ): PipelineScopeInterface;
}
