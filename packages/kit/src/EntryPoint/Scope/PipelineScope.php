<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Scope;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Scope\ScopeInterface as GenericScopeInterface;
use Psr\SimpleCache\CacheInterface;

class PipelineScope implements PipelineScopeInterface
{
    protected const MODE_PROD = 'prod';
    protected const MODE_DEV = 'dev';

    protected const ROOT_DIR = 'rootDir';
    protected const ENVIRONMENT = 'environment';
    protected const QUICK_CACHE = 'quick_cache';
    protected const SLOW_CACHE = 'slow_cache';
    protected const MEM_CACHED_HOST = 'mem_cached_host';
    protected const MEM_CACHED_PORT = 'mem_cached_port';
    protected const CACHE_TTL = 'cache_ttl';

    /**
     * @var GenericScopeInterface
     */
    protected $scope;

    public function __construct(
        GenericScopeInterface $scope,
        string $rootDir,
        string $environment,
        ServerRequestInterface $request = null
    ) {
        $this->scope = $scope;
        $this->scope->set(self::ROOT_DIR, $rootDir);
        $this->scope->set(self::ENVIRONMENT, $environment);
        if ($request) {
            $this->scope->set(ServerRequestInterface::class, $request);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRootDir(): string
    {
        return $this->scope->get(self::ROOT_DIR);
    }

    /**
     * {@inheritdoc}
     */
    public function getEnvironment(): string
    {
        return $this->scope->get(self::ENVIRONMENT);
    }

    public function setFileSystem(FilesystemInterface $filesystem): void
    {
        $this->scope->set(FilesystemInterface::class, $filesystem);
    }

    /**
     * {@inheritdoc}
     */
    public function getFileSystem(): FilesystemInterface
    {
        return $this->scope->get(FilesystemInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setIocContainer(IocContainerInterface $iocContainer): void
    {
        $this->scope->set(IocContainerInterface::class, $iocContainer);
    }

    /**
     * {@inheritdoc}
     */
    public function getIocContainer(): IocContainerInterface
    {
        return $this->scope->get(IocContainerInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setKernel(KernelInterface $kernel): void
    {
        $this->scope->set(KernelInterface::class, $kernel);
    }

    /**
     * {@inheritdoc}
     */
    public function getKernel(): KernelInterface
    {
        return $this->scope->get(KernelInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setQuickCache(CacheInterface $cache): void
    {
        $this->scope->set(self::QUICK_CACHE, $cache);
    }

    /**
     * {@inheritdoc}
     */
    public function getQuickCache(): CacheInterface
    {
        return $this->scope->get(self::QUICK_CACHE);
    }

    /**
     * {@inheritdoc}
     */
    public function setSlowCache(CacheInterface $cache): void
    {
        $this->scope->set(self::SLOW_CACHE, $cache);
    }

    /**
     * {@inheritdoc}
     */
    public function getSlowCache(): CacheInterface
    {
        return $this->scope->get(self::SLOW_CACHE);
    }

    /**
     * {@inheritdoc}
     */
    public function getMemCachedHost()
    {
        return $this->scope->get(self::MEM_CACHED_HOST);
    }

    /**
     * {@inheritdoc}
     */
    public function getMemCachedPort()
    {
        return $this->scope->get(self::MEM_CACHED_PORT);
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl(): int
    {
        return $this->scope->get(self::CACHE_TTL);
    }

    /**
     * {@inheritdoc}
     */
    public function setConfig(ConfigInterface $config): void
    {
        $this->scope->set(ConfigInterface::class, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(): ConfigInterface
    {
        return $this->scope->get(ConfigInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->scope->get(ServerRequestInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setRequest(ServerRequestInterface $request): void
    {
        $this->scope->set(ServerRequestInterface::class, $request, true);
    }

    /**
     * {@inheritdoc}
     */
    public function setResponse(ResponseInterface $response): void
    {
        $this->scope->set(ResponseInterface::class, $response, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(): ResponseInterface
    {
        return $this->scope->get(ResponseInterface::class);
    }
}
