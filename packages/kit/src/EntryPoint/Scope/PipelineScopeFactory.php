<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Scope;

use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Scope\ScopeFactoryInterface as GenericContextFactoryInterface;

/**
 * Class ContextFactory
 * @package Grifix\Kit\Pipeline\Context
 */
class PipelineScopeFactory extends AbstractFactory implements PipelineScopeFactoryInterface
{

    /**
     * @var GenericContextFactoryInterface
     */
    protected $genericContextFactory;

    /**
     * ContextFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param GenericContextFactoryInterface $genericContextFactory
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        GenericContextFactoryInterface $genericContextFactory
    ) {
        $this->genericContextFactory = $genericContextFactory;
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function create(
        string $rootDir,
        string $environment,
        ServerRequestInterface $request = null
    ): PipelineScopeInterface {
        $class = $this->makeClassName(PipelineScope::class);
        return new $class(
            $this->genericContextFactory->createContext(),
            $rootDir,
            $environment,
            $request
        );
    }
}
