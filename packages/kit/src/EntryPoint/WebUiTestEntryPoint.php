<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Scope\ScopeFactory;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeFactory;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\ServerRequestFactory;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Kernel\ClassMaker;
use League\Pipeline\PipelineInterface;

/**
 * Class WebEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class WebUiTestEntryPoint extends AbstractEntryPoint
{

    /***
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * WebUiTestEntryPoint constructor.
     * @param string $rootDir
     * @param ServerRequestInterface $request
     * @throws \Grifix\Kit\Scope\Exception\ContextPropertyAlreadyExistsException
     */
    public function __construct(string $rootDir, ServerRequestInterface $request)
    {
        parent::__construct($rootDir);
        $this->request = $request;
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function createScope(): PipelineScopeInterface
    {
        return $this->scopeFactory->create(
            $this->rootDir,
            'test',
            $this->request
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createWebApplicationPipeline();
    }

    protected function install(): void
    {
        $pipeline = $this->pipelineFactory->createInstallPipeline();
        $pipeline($this->createScope());
    }

    /**
     * {@inheritdoc}
     */
    public function enter(): PipelineScopeInterface
    {
        $this->install();
        return parent::enter();
    }
}
