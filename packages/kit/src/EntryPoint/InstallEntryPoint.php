<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use League\Pipeline\PipelineInterface;

/**
 * Class ComposerInstallEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class InstallEntryPoint extends AbstractEntryPoint
{
    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createInstallPipeline();
    }
}
