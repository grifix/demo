<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;


use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\ServerRequestFactory;
use League\Pipeline\PipelineInterface;

/**
 * Class WebEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class WebEntryPoint extends AbstractEntryPoint
{

    protected $requestFactory;

    /**
     * WebEntryPoint constructor.
     * @param string $rootDir
     */
    public function __construct(string $rootDir)
    {
        parent::__construct($rootDir);
        $this->requestFactory = new ServerRequestFactory($this->classMaker, new ArrayHelper());
    }

    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createWebApplicationPipeline(!boolval(getenv('GRIFIX_CACHE_ENABLED')));
    }

    /**
     * {@inheritdoc}
     */
    protected function createScope(): PipelineScopeInterface
    {
        return $this->scopeFactory->create(
            $this->rootDir,
            $this->environment ?? getenv('GRIFIX_ENV'),
            $this->requestFactory->createFromGlobals()
        );
    }
}
