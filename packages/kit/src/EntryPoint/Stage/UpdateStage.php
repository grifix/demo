<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Alias;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Migration\MigrationRunnerInterface;

/**
 * Class ComposerInstallStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class UpdateStage implements StageInterface
{

    protected $publishAssets = true;

    /**
     * ComposerInstallStage constructor.
     * @param bool $publishAssets
     */
    public function __construct(bool $publishAssets = true)
    {
        $this->publishAssets = $publishAssets;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $scope->getIocContainer()->get(MigrationRunnerInterface::class)->up();
        $scope->getIocContainer()->get(KernelInterface::class)->updateModules($this->publishAssets);
        $scope->getIocContainer()->get(Alias::QUICK_CACHE)->clear();
        $scope->getIocContainer()->get(Alias::SLOW_CACHE)->clear();
        return $scope;
    }
}
