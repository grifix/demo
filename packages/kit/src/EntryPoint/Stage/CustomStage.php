<?php declare(strict_types = 1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;

class CustomStage implements StageInterface
{
    /** @var callable */
    protected $handler;

    public function __construct(callable $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param PipelineScopeInterface $scope
     * @return void
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $handler = $this->handler;
        $handler($scope);
        return $scope;
    }
}
