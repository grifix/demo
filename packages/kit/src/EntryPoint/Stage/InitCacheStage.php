<?php declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Cache\SimpleCacheFactoryInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelper;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class InitCacheStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitCacheStage implements StageInterface
{

    /**
     * @var SimpleCacheFactoryInterface
     */
    protected $simpleCacheFactory;

    protected $clearCache;

    protected $sourcesCacheKey = 'grifix.kit.sourcesHash';

    /**
     * InitCacheStage constructor.
     * @param SimpleCacheFactoryInterface $simpleCacheFactory
     * @param bool $clearCache
     */
    public function __construct(
        SimpleCacheFactoryInterface $simpleCacheFactory,
        bool $clearCache = false
    ) {
        $this->simpleCacheFactory = $simpleCacheFactory;
        $this->clearCache = $clearCache;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        /** @noinspection PhpIncludeInspection */
        $config = include($scope->getRootDir() . '/memcache.php');
        $evnPath = sprintf('%s/memcache_%s.php', $scope->getRootDir(), $scope->getEnvironment());
        if (file_exists($evnPath)) {
            $config = array_merge($config, include($evnPath));
        }
        $scope->setQuickCache(
            $this->simpleCacheFactory->createInMemory(array_values($config))
        );

        $scope->setSlowCache($this->simpleCacheFactory->createInFilesystem('cache'));

        if ($this->clearCache || $this->hasSourceChanged($scope)) {
            $scope->getQuickCache()->clear();
            $scope->getSlowCache()->clear();
            $scope->getQuickCache()->set($this->sourcesCacheKey, $this->getActualSourcesHash($scope));
        }
        return $scope;
    }

    protected function getActualSourcesHash(PipelineScopeInterface $scope): string
    {
        $appDir = $scope->getRootDir() . '/app';
        $vendorDir = $scope->getRootDir() . '/packages';
        $envPath = $scope->getRootDir() . '/.env';
        $filesystemHelper = new FilesystemHelper(new Filesystem());
        return $filesystemHelper->getHash($appDir) . $filesystemHelper->getHash($vendorDir) . $filesystemHelper->getHash($envPath);
    }

    protected function getStoredSourcesHash(PipelineScopeInterface $scope): ?string
    {
        return $scope->getQuickCache()->get($this->sourcesCacheKey);
    }

    protected function hasSourceChanged(PipelineScopeInterface $scope): bool
    {
        if ($scope->getEnvironment() === 'prod' || !getenv('GRIFIX_TRACK_CODE_CHANGES')) {
            return false;
        }
        return $this->getActualSourcesHash($scope) !== $this->getStoredSourcesHash($scope);
    }
}
