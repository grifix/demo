<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Filesystem\FilesystemFactoryInterface;

/**
 * Class InitFilesystemStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitFilesystemStage implements StageInterface
{
    protected $fileSystemFactory;

    /**
     * InitFilesystemStage constructor.
     * @param FilesystemFactoryInterface $filesystemFactory
     */
    public function __construct(FilesystemFactoryInterface $filesystemFactory)
    {
        $this->fileSystemFactory = $filesystemFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $scope->setFileSystem($this->fileSystemFactory->createLocal($scope->getRootDir()));
        return $scope;
    }
}
