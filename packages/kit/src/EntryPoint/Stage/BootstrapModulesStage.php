<?php declare(strict_types = 1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;

/**
 * Class BootstrapModulesStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class BootstrapModulesStage implements StageInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $scope->getKernel()->bootstrapModules();
        return $scope;
    }
}
