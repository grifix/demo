<?php
declare(strict_types = 1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Alias;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderFactoryInterface;
use Grifix\Kit\Ioc\ImplementationFinder\ImplementationFinderInterface;
use Grifix\Kit\Ioc\IocContainerFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class InitIocStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitIocStage implements StageInterface
{

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var \Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface
     */
    protected $fileSystemHelper;

    /**
     * @var IocContainerFactoryInterface
     */
    protected $iocContainerFactory;

    /**
     * InitIocStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param \Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface $filesystemHelper
     * @param IocContainerFactoryInterface $iocContainerFactory
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        FilesystemHelperInterface $filesystemHelper,
        IocContainerFactoryInterface $iocContainerFactory
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->fileSystemHelper = $filesystemHelper;
        $this->iocContainerFactory = $iocContainerFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $iocContainer = $this->iocContainerFactory->createIocContainer(
            $scope->getQuickCache(),
            $scope->getConfig(),
            $this->getAutoWiringNamespaces($scope),
            $this->getDefinitions($scope)
        );

        $iocContainer->set(CacheInterface::class, $scope->getQuickCache());
        $iocContainer->set(Alias::SLOW_CACHE, $scope->getSlowCache());
        $iocContainer->set(Alias::QUICK_CACHE, $scope->getQuickCache());
        $iocContainer->set(Alias::APP_CONTEXT, $scope);
        $iocContainer->set(Alias::ROOT_DIR, $scope->getRootDir());
        $iocContainer->set(Alias::UNKNOWN, null);
        $iocContainer->set(Alias::ROOT_DIR, $scope->getRootDir());
        $iocContainer->set(Alias::PUBLIC_DIR, $scope->getRootDir() . '/public');
        $iocContainer->set(Alias::TMP_DIR, $scope->getRootDir() . '/tmp');
        $iocContainer->set(Alias::ENVIRONMENT, $scope->getEnvironment());
        $iocContainer->set(KernelInterface::class, $scope->getKernel());
        $iocContainer->set(ConfigInterface::class, $scope->getConfig());
        $iocContainer->set(FilesystemInterface::class, $scope->getFileSystem());
        $iocContainer->set(IocContainerInterface::class, $iocContainer);
        $iocContainer->set(
            ImplementationFinderInterface::class,
            $iocContainer->get(ImplementationFinderFactoryInterface::class)
                ->create($this->getAutoWiringNamespaces($scope), $scope->getQuickCache())
        );
        $scope->getKernel()->setIoc($iocContainer);
        $scope->setIocContainer($iocContainer);
        return $scope;
    }

    protected function getDefinitions(PipelineScopeInterface $scope)
    {
        $result = [];

        $mainPath = $scope->getRootDir() . '/config/ioc.php';
        $envPath = $scope->getRootDir() . '/config/' . $scope->getEnvironment() . '/ioc.php';

        foreach ($scope->getKernel()->getModules() as $module) {
            $config = $module->getConfig($scope->getEnvironment());
            if (isset($config['ioc'])) {
                $result = $this->arrayHelper->merge($config['ioc'], $result);
            }
        }

        if ($this->fileSystemHelper->fileExists($mainPath)) {
            /** @noinspection PhpIncludeInspection */
            $result = $this->arrayHelper->merge($result, include $mainPath);
        }
        if ($this->fileSystemHelper->fileExists($envPath)) {
            /** @noinspection PhpIncludeInspection */
            $result = $this->arrayHelper->merge($result, include $envPath);
        }
        return $result;
    }

    protected function getAutoWiringNamespaces(PipelineScopeInterface $scope)
    {
        $result = [];
        $mainPath = $scope->getRootDir() . '/config/namespaces.php';
        $envPath = $scope->getRootDir() . '/config/' . $scope->getEnvironment() . '/namespaces.php';
        if ($this->fileSystemHelper->fileExists($mainPath)) {
            /** @noinspection PhpIncludeInspection */
            $result = array_merge($result, include $mainPath);
        }
        if ($this->fileSystemHelper->fileExists($envPath)) {
            /** @noinspection PhpIncludeInspection */
            $result = array_merge($result, include($envPath));
        }

        return array_unique($result);
    }
}
