<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Cli\ApplicationFactory\CliApplicationFactoryInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Throwable;

class RunCliApplicationStage implements StageInterface
{
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $application = $scope->getIocContainer()->get(CliApplicationFactoryInterface::class)->createApplication();

        try {
            $application->run();
        } catch (Throwable $exception) {
            $application->renderThrowable($exception, new ConsoleOutput());
            throw $exception;
        }
        return $scope;
    }
}
