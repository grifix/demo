<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\MessageBroker\MessageBrokerInterface;
use Grifix\Kit\MessageBroker\QueueDefinitionsRepository\QueueDefinitionsRepositoryInterface;

final class InitQueuesStage implements StageInterface
{
    protected ?MessageBrokerInterface $messageBroker;

    protected ?QueueDefinitionsRepositoryInterface $queueDefinitionRepository;

    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $this->messageBroker = $scope->getIocContainer()->get(MessageBrokerInterface::class);
        $this->queueDefinitionRepository = $scope->getIocContainer()->get(QueueDefinitionsRepositoryInterface::class);

        $this->createQueues();

        return $scope;
    }

    protected function createQueues(): void
    {
        foreach ($this->queueDefinitionRepository->find() as $queueDefinition) {
            $this->messageBroker->declareExchange($queueDefinition->getQueueName());
            $this->messageBroker->declareQueue($queueDefinition->getQueueName());
            $this->messageBroker->bindQueue($queueDefinition->getQueueName(), $queueDefinition->getQueueName());
        }
    }
}
