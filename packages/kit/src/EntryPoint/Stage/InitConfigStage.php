<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Cache\CacheKeyTrait;
use Grifix\Kit\Config\ConfigFactoryInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;

/**
 * Class InitConfigStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitConfigStage implements StageInterface
{
    use CacheKeyTrait;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * InitConfigStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param ConfigFactoryInterface $configFactory
     * @param FilesystemHelperInterface $filesystemHelper
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        ConfigFactoryInterface $configFactory,
        FilesystemHelperInterface $filesystemHelper
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->configFactory = $configFactory;
        $this->filesystemHelper = $filesystemHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $cacheKey = $this->makeCacheKey('config.'.$scope->getEnvironment());
        $cachedConfig = $scope->getQuickCache()->get($cacheKey);
        if ($cachedConfig) {
            $scope->setConfig($this->configFactory->create($cachedConfig));
        } else {
            $config = $this->createConfig($scope);
            $scope->setConfig($config);
            $scope->getQuickCache()->set($cacheKey, $config->toArray(), 0);
        }
        return $scope;
    }

    /**
     * @param PipelineScopeInterface $scope
     * @return ConfigInterface
     */
    protected function createConfig(PipelineScopeInterface $scope): ConfigInterface
    {
        $configValues = [];

        $modules = $scope->getKernel()->getModules();
        foreach ($modules as $module) {
            $this->arrayHelper->set(
                $configValues,
                $module->getVendor() . '.' . $module->getName(),
                $module->getConfig($scope->getEnvironment())
            );
        }

        $mainConfigPath = $scope->getRootDir() . '/config/app.php';
        $modeConfigPath = $scope->getRootDir() . '/config/' . $scope->getEnvironment() . '/app.php';

        /** @noinspection PhpIncludeInspection */
        $configValues = $this->arrayHelper->merge($configValues, include $mainConfigPath);

        if ($scope->getEnvironment() && $this->filesystemHelper->fileExists($modeConfigPath)) {
            /** @noinspection PhpIncludeInspection */
            $configValues = $this->arrayHelper->merge($configValues, include $modeConfigPath);
        }

        $result = $this->configFactory->create($configValues);
        $result->set('grifix.kit.rootDir', $scope->getRootDir());

        return $result;
    }
}
