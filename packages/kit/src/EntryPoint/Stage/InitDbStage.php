<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Db\ConnectionFactoryInterface;
use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class InitDbStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitDbStage implements StageInterface
{

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $scope->getIocContainer()->set(ConnectionInterface::class, function () use ($scope) {
            return $scope->getIocContainer()->get(ConnectionFactoryInterface::class)
                ->create($scope->getConfig()->get('grifix.kit.db.default'));
        });
        return $scope;
    }
}
