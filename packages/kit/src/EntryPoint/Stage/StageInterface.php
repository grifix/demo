<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;

/**
 * Interface StageInterface
 * @package Grifix\Kit\Pipeline\Stage
 */
interface StageInterface
{
    /**
     * @param PipelineScopeInterface $scope
     * @return void
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface;
}
