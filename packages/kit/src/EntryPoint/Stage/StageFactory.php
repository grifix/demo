<?php

declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Cache\SimpleCacheFactoryInterface;
use Grifix\Kit\Config\ConfigFactoryInterface;
use Grifix\Kit\Filesystem\FilesystemFactoryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\DateHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Ioc\IocContainerFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Serializer\SerializerInterface;
use Grifix\Kit\Session\SessionFactoryInterface;

class StageFactory extends AbstractFactory implements StageFactoryInterface
{
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var FilesystemFactoryInterface
     */
    protected $filesystemFactory;

    /**
     * @var SessionFactoryInterface
     */
    protected $sessionFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var DateHelperInterface
     */
    protected $dateHelper;

    /**
     * @var IocContainerFactoryInterface
     */
    protected $iocContainerFactory;

    /**
     * @var SimpleCacheFactoryInterface
     */
    protected $simpleCacheFactory;

    public function __construct(
        ClassMakerInterface $classMaker,
        ArrayHelperInterface $arrayHelper,
        ConfigFactoryInterface $configFactory,
        FilesystemHelperInterface $filesystemHelper,
        FilesystemFactoryInterface $filesystemFactory,
        SessionFactoryInterface $sessionFactory,
        SerializerInterface $serializer,
        DateHelperInterface $dateHelper,
        IocContainerFactoryInterface $iocContainerFactory,
        SimpleCacheFactoryInterface $simpleCacheFactory
    ) {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
        $this->configFactory = $configFactory;
        $this->filesystemHelper = $filesystemHelper;
        $this->filesystemFactory = $filesystemFactory;
        $this->sessionFactory = $sessionFactory;
        $this->serializer = $serializer;
        $this->dateHelper = $dateHelper;
        $this->iocContainerFactory = $iocContainerFactory;
        $this->simpleCacheFactory = $simpleCacheFactory;
    }

    public function createInstallStage(bool $publishAssets = true): StageInterface
    {
        $class = $this->makeClassName(InstallStage::class);
        return new $class($publishAssets);
    }

    public function createHandleRequestStage(): StageInterface
    {
        return $this->createSimpleStage(HandleRequestStage::class);
    }

    public function createUpdateStage(bool $publishAssets = true): StageInterface
    {
        $class = $this->makeClassName(UpdateStage::class);
        return new $class($publishAssets);
    }

    protected function createSimpleStage(string $stageClass): StageInterface
    {
        $class = $this->makeClassName($stageClass);
        return new $class();
    }

    public function createRegisterAutoloadStage(): StageInterface
    {
        $class = $this->makeClassName(RegisterAutoloadStage::class);
        return new $class($this->arrayHelper);
    }

    public function createInitConfigStage(): StageInterface
    {
        $class = $this->makeClassName(InitConfigStage::class);
        return new $class($this->arrayHelper, $this->configFactory, $this->filesystemHelper);
    }

    public function createInitFilesystemStage(): StageInterface
    {
        $class = $this->makeClassName(InitFilesystemStage::class);
        return new $class($this->filesystemFactory);
    }

    public function createInitKernelStage(): StageInterface
    {
        $class = $this->makeClassName(InitKernelStage::class);
        return new $class($this->arrayHelper, $this->filesystemHelper);
    }

    public function createInitSessionStage(): StageInterface
    {
        $class = $this->makeClassName(InitSessionStage::class);
        return new $class($this->sessionFactory);
    }

    public function createCustomStage(callable $handler): StageInterface
    {
        $class = $this->makeClassName(CustomStage::class);
        return new $class($handler);
    }

    public function createInitRegisterAutoloadStage(): StageInterface
    {
        $class = $this->makeClassName(InitSessionStage::class);
        return new $class($this->arrayHelper);
    }

    public function createRunCliApplicationStage(): StageInterface
    {
        return $this->createSimpleStage(RunCliApplicationStage::class);
    }

    public function createInitDbStage(): StageInterface
    {
        return $this->createSimpleStage(InitDbStage::class);
    }

    public function createBootstrapModulesStage(): StageInterface
    {
        return $this->createSimpleStage(BootstrapModulesStage::class);
    }

    public function createHandleExceptionStage(): StageInterface
    {
        return $this->createSimpleStage(HandleExceptionStage::class);
    }

    public function createInitIocStage(): StageInterface
    {
        $class = $this->makeClassName(InitIocStage::class);
        return new $class(
            $this->arrayHelper,
            $this->filesystemHelper,
            $this->iocContainerFactory
        );
    }

    public function createInitCacheStage(bool $clearCache = false): StageInterface
    {
        $class = $this->makeClassName(InitCacheStage::class);
        return new $class($this->simpleCacheFactory, $clearCache);
    }

    public function createInitQueuesStage(): StageInterface
    {
        return $this->createSimpleStage(InitQueuesStage::class);
    }
}
