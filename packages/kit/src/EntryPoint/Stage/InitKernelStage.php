<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\Kernel;

/**
 * Class InitKernelStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitKernelStage implements StageInterface
{
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    protected $fileSystemHelper;

    /**
     * InitKernelStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param FilesystemHelperInterface $filesystemHelper
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        FilesystemHelperInterface $filesystemHelper
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->fileSystemHelper = $filesystemHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        $kernel = new Kernel(
            $scope->getRootDir(),
            $scope->getQuickCache(),
            $this->fileSystemHelper,
            $this->arrayHelper,
            $scope->getEnvironment()
        );
        $kernel->getModules(true);
        $scope->setKernel($kernel);
        return $scope;
    }
}
