<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;


/**
 * Class StageFactory
 * @package Grifix\Kit\Pipeline\Stage
 */
interface StageFactoryInterface
{
    /**
     * @param bool $publishAssets
     * @return StageInterface
     */
    public function createInstallStage(bool $publishAssets = true): StageInterface;

    /**
     * @return StageInterface
     */
    public function createHandleRequestStage(): StageInterface;

    /**
     * @param bool $publishAssets
     * @return StageInterface
     */
    public function createUpdateStage(bool $publishAssets = true): StageInterface;

    /**
     * @return RegisterAutoloadStage
     */
    public function createRegisterAutoloadStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createInitConfigStage(): StageInterface;


    /**
     * @return StageInterface
     */
    public function createInitFilesystemStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createInitKernelStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createInitSessionStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createInitRegisterAutoloadStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createRunCliApplicationStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createInitDbStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createBootstrapModulesStage(): StageInterface;

    /**
     * @return StageInterface
     */
    public function createInitIocStage(): StageInterface;

    /**
     * @param bool $clearCache
     * @return StageInterface
     */
    public function createInitCacheStage(bool $clearCache = false): StageInterface;

    public function createCustomStage(callable $handler): StageInterface;
}
