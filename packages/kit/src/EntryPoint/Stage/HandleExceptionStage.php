<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\Exception\ExceptionNormalizerInterface;
use Psr\Log\LoggerInterface;

final class HandleExceptionStage implements StageInterface
{

    public function __invoke(PipelineScopeInterface $scope): PipelineScopeInterface
    {
        set_exception_handler(function (\Throwable $throwable) use ($scope) {
            $scope->getIocContainer()->get(LoggerInterface::class)->critical(
                $throwable->getMessage(),
                $scope->getIocContainer()->get(ExceptionNormalizerInterface::class)->normalize($throwable)
            );
            throw $throwable;
        });
        return $scope;
    }
}
