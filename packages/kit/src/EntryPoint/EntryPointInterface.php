<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Scope\ScopeInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\EntryPoint\Stage\StageInterface;

/**
 * Interface EntryPointInterface
 * @package Grifix\Kit\EntryPoint
 */
interface EntryPointInterface
{
    /**
     * @return PipelineScopeInterface
     */
    public function enter(): PipelineScopeInterface;
}
