<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\DateTime;

use DateInterval;
use DateTimeImmutable;
use Grifix\Kit\Type\TimeZone\TimeZone;
use Grifix\Kit\Type\TimeZone\TimeZoneInterface;

class DateTime extends DateTimeImmutable implements DateTimeInterface
{
    public static function makeTimeStamp(string $utcDate): int
    {
        return (new self($utcDate, new TimeZone(TimeZoneInterface::TIMEZONE_ZULU)))->getTimestamp();
    }

    public function addSeconds(int $seconds): DateTimeInterface
    {
        return $this->add(new DateInterval(sprintf('PT%dS', $seconds)));
    }
}
