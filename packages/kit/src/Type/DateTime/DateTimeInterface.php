<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\DateTime;

use DateInterval;

interface DateTimeInterface extends \DateTimeInterface
{
    public const SECONDS_IN_MINUTE = 60;
    public const SECONDS_IN_HOUR = 60 * self::SECONDS_IN_MINUTE;
    public const SECONDS_IN_DAY = 24 * self::SECONDS_IN_HOUR;
    public const SECONDS_IN_MONTH = 30 * self::SECONDS_IN_DAY;
    public const SECONDS_IN_YEAR = 365 * self::SECONDS_IN_DAY;

    public function addSeconds(int $seconds): DateTimeInterface;
}
