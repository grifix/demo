<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\DateTime;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Type\TimeZone\TimeZoneFactoryInterface;
use Grifix\Kit\Type\TimeZone\TimeZoneInterface;

class DateTimeFactory extends AbstractFactory implements DateTimeFactoryInterface
{
    /** @var TimeZoneFactoryInterface */
    protected $timeZoneFactory;

    public function __construct(ClassMakerInterface $classMaker, TimeZoneFactoryInterface $timeZoneFactory)
    {
        parent::__construct($classMaker);
        $this->timeZoneFactory = $timeZoneFactory;
    }

    public function create(string $time = 'now', string $timeZone = TimeZoneInterface::TIMEZONE_ZULU): DateTimeInterface
    {
        $class = $this->makeClassName(DateTime::class);
        return new $class($time, $this->timeZoneFactory->create($timeZone));
    }

    public function createFromTimeStamp(int $timeStamp): DateTimeInterface
    {
        $date = new DateTime("now", $this->timeZoneFactory->create(TimeZoneInterface::TIMEZONE_ZULU));
        $date = $date->setTimestamp($timeStamp);
        return $this->create($date->format('Y-m-d H:i:s'), TimeZoneInterface::TIMEZONE_ZULU);
    }
}
