<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\DateTime;

use Grifix\Kit\Type\TimeZone\TimeZoneInterface;

interface DateTimeFactoryInterface
{
    public function create(
        string $time = 'now',
        string $timeZone = TimeZoneInterface::TIMEZONE_ZULU
    ): DateTimeInterface;

    public function createFromTimeStamp(int $timeStamp): DateTimeInterface;
}
