<?php
declare(strict_types = 1);

namespace Grifix\Kit\Type\Range;

/**
 * Class Range
 * @package Grifix\Kit\Type
 */
interface RangeInterface
{
    /**
     * @return int
     */
    public function getFrom(): int;

    /**
     * @return int
     */
    public function getTo(): int;
}
