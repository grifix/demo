<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\Range;

use Grifix\Kit\Kernel\AbstractFactory;

class RangeFactory extends AbstractFactory implements RangeFactoryInterface
{
    public function create(int $from, int $to): RangeInterface
    {
        $class = $this->makeClassName(Range::class);
        return new $class($from, $to);
    }
}
