<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\Range;

interface RangeFactoryInterface
{
    public function create(int $from, int $to): RangeInterface;
}
