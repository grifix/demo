<?php
declare(strict_types = 1);

namespace Grifix\Kit\Type;

interface SimpleValueObjectInterface
{
    public function getValue();

    public function __toString(): string;
}
