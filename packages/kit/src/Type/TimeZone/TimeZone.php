<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\TimeZone;

use DateTimeZone;

class TimeZone extends DateTimeZone implements TimeZoneInterface
{
    public function __construct($timezone = self::TIMEZONE_ZULU)
    {
        parent::__construct($timezone);
    }
}
