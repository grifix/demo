<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\TimeZone;

interface TimeZoneFactoryInterface
{
    public function create(string $timeZone = TimeZoneInterface::TIMEZONE_ZULU): TimeZoneInterface;
}
