<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\TimeZone;

use Grifix\Kit\Kernel\AbstractFactory;

class TimeZoneFactory extends AbstractFactory implements TimeZoneFactoryInterface
{
    public function create(string $timeZone = TimeZoneInterface::TIMEZONE_ZULU): TimeZoneInterface
    {
        $class = $this->makeClassName(TimeZone::class);
        return new $class($timeZone);
    }
}
