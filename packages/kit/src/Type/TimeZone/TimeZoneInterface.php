<?php declare(strict_types = 1);

namespace Grifix\Kit\Type\TimeZone;

interface TimeZoneInterface
{
    public const TIMEZONE_EUROPE_WARSAW = 'Europe/Warsaw';
    public const TIMEZONE_ZULU = 'Zulu';
}
