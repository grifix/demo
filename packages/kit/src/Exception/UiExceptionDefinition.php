<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Kit\Exception;

use Grifix\Kit\Serializer\ClosureContainerInterface;

class UiExceptionDefinition implements ClosureContainerInterface
{

    /** @var string */
    protected $key;

    /** @var string */
    protected $translationKey;

    /** @var int */
    protected $httpCode;

    /** @var int */
    protected $cliCode;

    public function __construct(
        string $key,
        string $translationKey,
        int $httpCode = 400,
        ?int $cliCode = null
    ) {
        $this->key = $key;
        $this->translationKey = $translationKey;
        $this->httpCode = $httpCode;
        $this->cliCode = $cliCode ?? $this->httpCode;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getTranslationKey(): string
    {
        return $this->translationKey;
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function getCliCode(): int
    {
        return $this->cliCode;
    }
}
