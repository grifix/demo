<?php
declare(strict_types = 1);

namespace Grifix\Kit\Faker;

use DateTimeInterface;
use Faker\Factory;
use Faker\Generator;

class Faker implements FakerInterface
{
    /** @var Generator[] */
    protected $generators = [];

    /** @var Generator */
    protected $currentGenerator;

    public function __construct()
    {
        $this->currentGenerator = $this->getGenerator();
    }

    public function fakeCompanyName(): string
    {
        return $this->currentGenerator->company;
    }

    public function fakeEmail(): string
    {
        return $this->currentGenerator->email;
    }

    public function setUnique(bool $unique = true): FakerInterface
    {
        foreach ($this->generators as $generator) {
            $generator->unique(!$unique);
        }
        return $this;
    }

    public function fakeWord(): string
    {
        return $this->currentGenerator->word;
    }

    public function fakeUrl(): string
    {
        return $this->currentGenerator->url;
    }

    public function fakeIp(): string
    {
        return $this->currentGenerator->ipv4;
    }

    public function fakeText(int $maxChars = 200): string
    {
        return $this->currentGenerator->text($maxChars);
    }

    public function setLocale(string $locale = null): FakerInterface
    {
        $this->currentGenerator = $this->getGenerator($locale);
        return $this;
    }

    public function fakeDate($max = 'now', $timezone = null): DateTimeInterface
    {
        return $this->currentGenerator->dateTime($max, $timezone);
    }

    public function fakeInt($min = 0, $max = 100): int
    {
        return random_int($min, $max);
    }

    protected function getGenerator(string $locale = null): Generator
    {
        if (null === $locale) {
            $locale = Factory::DEFAULT_LOCALE;
        }
        if (!isset($this->generators[$locale])) {
            $this->generators[$locale] = Factory::create($locale);
        }
        return $this->generators[$locale];
    }
}
