<?php
declare(strict_types=1);

namespace Grifix\Kit\Faker;

use DateTimeInterface;

interface FakerInterface
{
    public function fakeCompanyName(): string;

    public function fakeEmail(): string;

    public function setUnique(bool $unique = true): FakerInterface;

    public function setLocale(string $locale = null): FakerInterface;

    public function fakeDate($max = 'now', $timezone = null): DateTimeInterface;

    public function fakeInt($min = 0, $max = 100): int;

    public function fakeWord(): string;

    public function fakeText(int $maxChars = 200): string;

    public function fakeUrl(): string;

    public function fakeIp(): string;
}
