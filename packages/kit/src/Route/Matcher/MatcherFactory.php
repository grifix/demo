<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Route\Matcher;

use Grifix\Kit\Cache\CacheKeyTrait;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class MatcherFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Mather
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MatcherFactory extends AbstractFactory implements MatcherFactoryInterface
{
    use CacheKeyTrait;
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var FilesystemInterface
     */
    protected $filesystem;

    /**
     * @var array
     */
    protected $matcherClasses = [];

    /**
     * @var array
     */
    protected $locales = [];

    /**
     * MatcherFactory constructor.
     *
     * @param CacheInterface $cache
     * @param FilesystemInterface $filesystem
     * @param ClassMakerInterface $classMaker
     * @param array $locales <cfg:grifix.kit.intl.config.enabledLocales>
     */
    public function __construct(
        CacheInterface $cache,
        FilesystemInterface $filesystem,
        ClassMakerInterface $classMaker,
        array $locales
    ) {
        $this->cache = $cache;
        $this->filesystem = $filesystem;
        $this->locales = $locales;
        parent::__construct($classMaker);
    }

    /**
     *
     * @param bool $app
     *
     * @return array
     */
    protected function getMatcherClasses($app = false)
    {
        if ($this->matcherClasses) {
            return $this->matcherClasses;
        }
        $cacheKey = $this->makeCacheKey('matcherClasses');
        $cache = $this->cache->get($cacheKey);
        if ($cache) {
            return $cache;
        }
        $dir = 'grifix/kit/src/Route/Matcher';
        if ($app) {
            $dir = '/app/' . $dir;
        } else {
            $dir = '/vendor/' . $dir;
        }
        $namespace = '\\Grifix\\Kit\\Route\\Matcher\\';
        if ($app) {
            $namespace = '\\App\\Grifix\\Kit\\Route\\Matcher\\';
        }
        $result = [];
        $files = $this->filesystem->listContents($dir);
        foreach ($files as $file) {
            $type = strtolower(str_replace('Matcher.php', '', $file['basename']));
            $className = $namespace . str_replace('.php', '', $file['basename']);
            $reflectionClass = new \ReflectionClass($className);
            if ($reflectionClass->isInstantiable() && $reflectionClass->implementsInterface(MatcherInterface::class)) {
                $result[$type] = $className;
            }
        }
        $this->cache->set($cacheKey, $result);
        $this->matcherClasses = $result;

        return $this->matcherClasses;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return MatcherInterface
     */
    public function __call($name, $arguments)
    {
        $type = strtolower(str_replace(['create', 'Matcher'], '', $name));
        $class = $this->getMatcherClasses()[$type];

        return new $class();
    }

    /**
     * @param $type
     *
     * @return MatcherInterface
     */
    public function createMatcher($type): MatcherInterface
    {
        $method = 'create' . ucfirst($type) . 'Matcher';

        return $this->{$method}();
    }

    /**
     * @return mixed
     */
    protected function createLocaleMatcher()
    {
        $class = $this->getMatcherClasses()['locale'];

        return new $class($this->locales);
    }

    /**
     * {@inheritdoc}
     */
    public function createAllMatchers()
    {
        $result = [];
        $matcherClasses = $this->getMatcherClasses();
        foreach ($matcherClasses as $type => $class) {
            $result[$type] = $this->createMatcher($type);
        }

        return $result;
    }
}
