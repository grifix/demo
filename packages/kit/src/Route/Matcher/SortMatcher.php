<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Matcher;

/**
 * Class PageMatcher
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Mather
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SortMatcher extends AbstractMatcher
{
    protected $pattern = '(asc|desc)';
    
    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function makeParam($value): string
    {
        return 'p' . $value;
    }
}
