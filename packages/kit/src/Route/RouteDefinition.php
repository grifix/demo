<?php declare(strict_types = 1);

namespace Grifix\Kit\Route;

class RouteDefinition
{
    /** @var string */
    protected $pattern;

    /** @var string|null */
    protected $handler;

    /** @var bool|null */
    protected $secured;

    /** @var array */
    protected $methods = [];

    /** @var string|null */
    protected $host;

    /** @var RouteDefinition[] */
    protected $children = [];

    /**
     * @param RouteDefinition[] $children
     */
    public function __construct(
        string $pattern,
        ?string $handler,
        array $children = [],
        ?bool $secured = null,
        array $methods = null,
        ?string $host = null
    ) {
        $this->pattern = $pattern;
        $this->handler = $handler;
        $this->secured = $secured;
        $this->methods = $methods;
        $this->host = $host;
        $this->children = $children;
    }

    public static function withHandler(string $pattern, string $handler): self
    {
        return new self($pattern, $handler);
    }

    /**
     * @param RouteDefinition[] $children
     */
    public static function withChildren(string $pattern, array $children): self
    {
        return new self($pattern, null, $children);
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    public function inherit(RouteDefinition $routeDefinition): RouteDefinition
    {
        $result = clone $this;
        $result->pattern = $routeDefinition->pattern . $this->pattern;
        null !== $result->secured ?: $result->secured = $routeDefinition->secured;
        null !== $result->methods ?: $result->methods = $routeDefinition->methods;
        null !== $result->host ?: $result->host = $routeDefinition->host;
        return $result;
    }

    public function getHandler(): ?string
    {
        return $this->handler;
    }

    public function getSecured(): ?bool
    {
        return $this->secured;
    }

    public function getMethods(): ?array
    {
        return $this->methods;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function getChildren(): array
    {
        return $this->children;
    }
}
