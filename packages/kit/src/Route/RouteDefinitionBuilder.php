<?php declare(strict_types = 1);

namespace Grifix\Kit\Route;

class RouteDefinitionBuilder
{
    /** @var string */
    protected $pattern;

    /** @var string|null */
    protected $handler;

    /** @var bool|null */
    protected $secured;

    /** @var array */
    protected $methods = [];

    /** @var string|null */
    protected $host;

    /** @var array */
    protected $children = [];

    public function __construct(string $pattern)
    {
        $this->pattern = $pattern;
    }

    public function setHandler(?string $handler): RouteDefinitionBuilder
    {
        $this->handler = $handler;
        return $this;
    }

    public function setSecured(?bool $secured): RouteDefinitionBuilder
    {
        $this->secured = $secured;
        return $this;
    }

    public function setMethods(array $methods): RouteDefinitionBuilder
    {
        $this->methods = $methods;
        return $this;
    }

    public function setHost(?string $host): RouteDefinitionBuilder
    {
        $this->host = $host;
        return $this;
    }

    public function setChildren(array $children): RouteDefinitionBuilder
    {
        $this->children = $children;
        return $this;
    }

    public function build(): RouteDefinition
    {
        return new RouteDefinition(
            $this->pattern,
            $this->handler,
            $this->children,
            $this->secured,
            $this->methods,
            $this->host
        );
    }
}
