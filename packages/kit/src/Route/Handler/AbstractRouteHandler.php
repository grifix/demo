<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Handler;

use Grifix\Kit\Cqrs\CqrsClientTrait;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Ioc\ServiceLocatorTrait;
use Grifix\Kit\Ui\Action\ActionDispatcherInterface;
use Grifix\Kit\View\ViewFactoryInterface;

/**
 * Class AbstractRouteRouteHandler
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractRouteHandler implements RouteHandlerInterface
{
    use CqrsClientTrait;
    use ServiceLocatorTrait;

    public static function getAlias():string {
        $path = explode('\\', static::class);
        if($path[0] === 'App'){
            array_shift($path);
        }
        $path[2] = null;
        $path[3] = null;
        $path[4] = null;

        $result = [];
        foreach ($path as $val){
            if(null !== $val){
                $result[] = lcfirst($val);
            }
        }
        $result = implode('.', $result);
        $result = str_replace('RouteHandler', '', $result);
        return $result;
    }

    protected function render(ResponseInterface $response, string $template, array $vars = []): ResponseInterface
    {
        return $response->withContent(
            $this->getShared(ViewFactoryInterface::class)->create($template, $vars)->render()
        );
    }

    protected function executeAction(string $actionAlias, array $params = []): array
    {
        return $this->iocContainer->get(ActionDispatcherInterface::class)->dispatch($actionAlias, $params);
    }

    protected function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR
    ): string {
        return $this->getShared(TranslatorInterface::class)->translate($key, $vars, $quantity, $case, $form);
    }
}
