<?php declare(strict_types=1);

namespace Grifix\Kit\Route\UrlMaker;

use Grifix\Kit\Intl\Locale\LocaleRepositoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Route\RouteCollectionFactoryInterface;

class UrlMakerFactory extends AbstractFactory implements UrlMakerFactoryInterface
{
    protected RouteCollectionFactoryInterface $routeCollectionFactory;

    protected LocaleRepositoryInterface $localeRepository;

    public function __construct(
        ClassMakerInterface $classMaker,
        RouteCollectionFactoryInterface $collectionFactory,
        LocaleRepositoryInterface $localeRepository
    ) {
        parent::__construct($classMaker);
        $this->routeCollectionFactory = $collectionFactory;
        $this->localeRepository = $localeRepository;
    }

    public function create(string $host = 'http://localhost'): UrlMakerInterface
    {
        $class = $this->makeClassName(UrlMaker::class);
        return new $class($this->localeRepository, $this->routeCollectionFactory->createFromConfig(), $host);
    }
}
