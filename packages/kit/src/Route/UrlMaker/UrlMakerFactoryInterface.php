<?php declare(strict_types = 1);

namespace Grifix\Kit\Route\UrlMaker;

interface UrlMakerFactoryInterface
{
    public function create(string $host = 'http://localhost'): UrlMakerInterface;
}
