<?php declare(strict_types=1);

namespace Grifix\Kit\Route\UrlMaker;

use Grifix\Kit\Intl\Locale\LocaleRepositoryInterface;
use Grifix\Kit\Route\RouteCollectionInterface;

class UrlMaker implements UrlMakerInterface
{
    private RouteCollectionInterface $routeCollection;

    private LocaleRepositoryInterface $localeRepository;

    protected string $host;

    public function __construct(
        LocaleRepositoryInterface $localeRepository,
        RouteCollectionInterface $routeCollection,
        string $host
    ) {
        $this->localeRepository = $localeRepository;
        $this->routeCollection = $routeCollection;
        $this->host = $host;
    }

    public function makeUrl(string $routeAlias, array $params = [], bool $forceLocale = false): string
    {
        if (false === $forceLocale && isset($params['locale']) && $params['locale'] === $this->localeRepository->getDefaultLocale()->getCode()) {
            unset($params['locale']);
        }
        return $this->host . $this->routeCollection->get($routeAlias)->makeUrl($params);
    }
}
