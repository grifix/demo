<?php declare(strict_types = 1);

namespace Grifix\Kit\Route\UrlMaker;

interface UrlMakerInterface
{
    public function makeUrl(string $routeAlias, array $params = [], bool $forceLocale = false): string;
}
