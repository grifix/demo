<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route;

use Grifix\Kit\Cache\CacheKeyTrait;
use Grifix\Kit\Cache\SimpleCache;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Route\Exception\NoRouteConfigPatternException;
use Grifix\Kit\Route\Handler\RouteHandlerFactoryInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class RouteCollectionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RouteCollectionFactory extends AbstractFactory implements RouteCollectionFactoryInterface
{
    use CacheKeyTrait;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var RouteFactoryInterface
     */
    protected $routeFactory;

    /**
     * @var SimpleCache
     */
    protected $cache;

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var RouteHandlerFactoryInterface
     */
    protected $handlerFactory;

    /**
     * RouteCollectionFactory constructor.
     *
     * @param ArrayHelperInterface $arrayHelper
     * @param RouteFactoryInterface $routeFactory
     * @param ConfigInterface $config
     * @param CacheInterface $cache
     * @param ClassMakerInterface $classMaker
     * @param RouteHandlerFactoryInterface $handlerFactory
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        RouteFactoryInterface $routeFactory,
        ConfigInterface $config,
        CacheInterface $cache,
        ClassMakerInterface $classMaker,
        RouteHandlerFactoryInterface $handlerFactory
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->routeFactory = $routeFactory;
        $this->cache = $cache;
        $this->config = $config;
        $this->handlerFactory = $handlerFactory;
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $routes)
    {
        $class = $this->makeClassName(RouteCollection::class);

        return new $class($routes);
    }

    /**
     *
     * {@inheritdoc}
     */
    public function createFromConfig()
    {
        $config = $this->config->toArray();
        //TODO solve problem witch closure serialization
        /*
        $cacheKey = $this->makeCacheKey('fromConfig');
        /**@var $cache RouteCollectionInterface */

        /*$cache = $this->cache->get($cacheKey);
        if ($cache) {
            return $cache;
        }*/
        $class = $this->makeClassName(RouteCollection::class);
        /**@var $result RouteCollectionInterface */
        $result = new $class;
        foreach ($config as $vendor => $packages) {
            foreach ($packages as $package => $cfg) {
                if (isset($cfg['routes'])) {
                    foreach ($cfg['routes'] as $routeName => $routeDefinition) {
                        /** @var RouteDefinition $routeDefinition */
                        $this->extractRoutes($vendor . '.' . $package . '.' . $routeName, $routeDefinition, $result);
                    }
                }
            }
        }
        //TODO solve problem witch closure serialization
        //$this->cache->set($cacheKey, $result);

        return $result;
    }

    protected function extractRoutes(
        string $routeAlias,
        RouteDefinition $routeDefinition,
        RouteCollectionInterface $collection,
        ?RouteDefinition $parentRouteDefinition = null
    ) {

        $handler = null;

        if ($routeDefinition->getHandler()) {
            $handler = $this->handlerFactory->create($routeDefinition->getHandler());
        }

        if ($parentRouteDefinition) {
            $routeDefinition = $routeDefinition->inherit($parentRouteDefinition);
        }

        $collection[$routeAlias] = $this->routeFactory->create(
            $routeAlias,
            $routeDefinition->getPattern(),
            $handler,
            $routeDefinition->getMethods() ?? [],
            $routeDefinition->getSecured() ?? false,
            $routeDefinition->getHost()
        );

        if ($routeDefinition->getChildren()) {
            foreach ($routeDefinition->getChildren() as $childName => $childRouteDefinition) {
                $this->extractRoutes(
                    $routeAlias . '.' . $childName,
                    $childRouteDefinition,
                    $collection,
                    $routeDefinition
                );
            }
        }
    }
}
