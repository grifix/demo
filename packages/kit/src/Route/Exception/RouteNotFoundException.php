<?php declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;

use Exception;

class RouteNotFoundException extends Exception
{
    public function __construct(string $alias)
    {
        parent::__construct(sprintf('Route witch alias %s not found!', $alias));
    }
}
