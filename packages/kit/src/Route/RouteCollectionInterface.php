<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Route;

use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\Exception\RouteNotFoundException;

interface RouteCollectionInterface extends CollectionInterface
{
    public function match(ServerRequestInterface $request): ?RouteInterface;

    /**
     * @throws RouteNotFoundException
     */
    public function get(string $alias): RouteInterface;
}
