<?php


namespace {


    use Grifix\Admin\Application\Contract\MenuFinder\MenuFinderInterface;
    use Grifix\Admin\Infrastructure\MenuFinder\MenuFinder;
    use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;

    return [
        MenuFinderInterface::class => new DependencyDefinition(MenuFinder::class)
    ];
}
