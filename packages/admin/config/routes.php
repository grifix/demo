<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace {

    use Grifix\Kit\Route\RouteDefinition;

    return [
        'root' => RouteDefinition::withHandler('/{locale:locale?}/grifix/admin', 'grifix.admin.desktop')
    ];
}
