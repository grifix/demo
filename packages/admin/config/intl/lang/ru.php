<?php

declare(strict_types=1);

namespace {
    
    return [
        '_module'=>'Админ',
        'menu' => 'Меню',
        'desktop' => 'Рабочий стол',
        'panel' => 'Панель адмнистратора',
        'msg_checkRecordsForDeletion' => 'Выберите строки которые нужно удалить!',
        'clearFilter' => 'Очистить фильтр',
        'equal' => 'Равно',
        'notEqual' => 'Не равно',
        'beginWith' => 'Начитаеться с',
        'endWith' => 'Заканчивается на',
        'less' => 'Меньше',
        'lessOrEqual' => 'Меньше или равно',
        'greater' => 'Больше',
        'greaterOrEqual' => 'Больше или равно',
        'contains' => 'Содержит'
    ];
}
