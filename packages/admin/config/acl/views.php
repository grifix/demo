<?php
declare(strict_types=1);

namespace {

    use Grifix\Admin\Ui\Common\AdminModulePermissions;

    return [
        'grifix.admin.{skin}.lyt.default' => AdminModulePermissions::SHOW_DESKTOP
    ];
}
