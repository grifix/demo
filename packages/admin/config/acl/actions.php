<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Admin\Ui\Common\AdminModulePermissions;

    return [
        'grifix.admin.desktop' => AdminModulePermissions::SHOW_DESKTOP,
        'grifix.admin.getMenu' => AdminModulePermissions::SHOW_DESKTOP
    ];
}
