<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Shared\Ui\Common\PermissionDefinition;
    use Grifix\Admin\Ui\Common\AdminModulePermissions;

    return [
        new PermissionDefinition(AdminModulePermissions::SHOW_DESKTOP, ['admin'])
    ];
}
