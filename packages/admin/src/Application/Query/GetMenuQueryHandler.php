<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Admin\Application\Query;

use Grifix\Admin\Application\Contract\MenuFinder\MenuFinderInterface;
use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class GetMenuQueryHandler
 *
 * @category Grifix
 * @package  Grifix\Admin\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetMenuQueryHandler
{

    /**
     * @var MenuFinderInterface
     */
    protected $menuFinder;

    /**
     * GetMenuQueryHandler constructor.
     * @param MenuFinderInterface $menuFinder
     */
    public function __construct(MenuFinderInterface $menuFinder)
    {
        $this->menuFinder = $menuFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(GetMenuQuery $query): CollectionInterface
    {
        return $this->menuFinder->find();
    }

}
