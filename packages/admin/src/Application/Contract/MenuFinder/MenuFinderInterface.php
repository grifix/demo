<?php
declare(strict_types=1);

namespace Grifix\Admin\Application\Contract\MenuFinder;

use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class MenuFinderInterface
 * @package Grifix\Admin\Application\Contract
 */
interface MenuFinderInterface
{
    /**
     * @return CollectionInterface|MenuItemDto[]
     */
    public function find(): CollectionInterface;
}
