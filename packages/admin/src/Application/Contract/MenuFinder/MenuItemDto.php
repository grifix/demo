<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Admin\Application\Contract\MenuFinder;

use Grifix\Kit\Collection\Collection;

/**
 * Class MenuItemModel
 *
 * @category Grifix
 * @package  Grifix\Admin\Application\Dto
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MenuItemDto
{
    protected $id;
    
    protected $title;
    
    protected $children;
    
    protected $url;
    
    /**
     * MenuItemModel constructor.
     *
     * @param string          $id
     * @param string          $title
     * @param string          $url
     * @param Collection|null $children
     */
    public function __construct(string $id, string $title, string $url = null, Collection $children = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->url = $url;
        $this->children = $children;
    }
    
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    
    /**
     * @return Collection|null
     */
    public function getChildren()
    {
        return $this->children;
    }
    
    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
}
