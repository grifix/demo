<?php
declare(strict_types=1);

namespace Grifix\Admin\Infrastructure\MenuFinder;

use Grifix\Admin\Application\Contract\MenuFinder\MenuFinderInterface;
use Grifix\Admin\Application\Contract\MenuFinder\MenuItemDto;
use Grifix\Admin\Infrastructure\MenuFinder\Exception\MenuItemHasNoTitleException;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Route\RouteCollectionInterface;
use Grifix\Kit\Route\RouteInterface;

/**
 * Class MenuFinder
 * @package Grifix\Admin\Infrastructure
 */
class MenuFinder implements MenuFinderInterface
{

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var RouteCollectionInterface
     */
    protected $routeCollection;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * MenuFinder constructor.
     * @param CollectionFactoryInterface $collectionFactory
     * @param ConfigInterface $config
     * @param KernelInterface $kernel
     * @param RouteCollectionInterface $routeCollection
     * @param TranslatorInterface $translator
     */
    public function __construct(
        CollectionFactoryInterface $collectionFactory,
        ConfigInterface $config,
        KernelInterface $kernel,
        RouteCollectionInterface $routeCollection,
        TranslatorInterface $translator
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
        $this->kernel = $kernel;
        $this->routeCollection = $routeCollection;
        $this->translator = $translator;
    }


    /**
     * {@inheritdoc}
     */
    public function find(): CollectionInterface
    {
        $result = $this->collectionFactory->createCollection();
        foreach ($this->kernel->getModules() as $module) {
            $id = $module->getVendor() . '.' . $module->getName();
            $menu = $this->config->get($id . '.admin.menu');

            if ($menu) {
                $result->add(
                    $this->createMenuItem(
                        $id,
                        ['title' => $id . '._module', 'children' => $menu]
                    )
                );
            }
        }

        return $result;
    }

    /**
     * @param string $id
     * @param array $item
     * @return MenuItemDto
     * @throws \Grifix\Kit\Route\Exception\ParamIsRequiredException
     */
    protected function createMenuItem(string $id, array $item)
    {
        if (!isset($item['title'])) {
            throw new MenuItemHasNoTitleException($id);
        }


        $children = $this->collectionFactory->createCollection();
        if (isset($item['children'])) {
            foreach ($item['children'] as $childId => $child) {
                $children->add($this->createMenuItem($childId, $child));
            }
        }

        $url = null;
        if (isset($item['route'])) {
            /**@var $route RouteInterface */
            $route = $this->routeCollection->offsetGet($item['route']);
            $url = $route->makeUrl();
        }

        return new MenuItemDto(
            $id,
            $this->translator->translate($item['title']),
            $url,
            $children
        );
    }
}
