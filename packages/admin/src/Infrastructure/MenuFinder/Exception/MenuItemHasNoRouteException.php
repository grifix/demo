<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Admin\Infrastructure\MenuFinder\Exception;

use Exception;
use Grifix\Kit\Cache\Exception\InvalidArgumentException;

/**
 * Class MenuItemHasNoRouteException
 *
 * @category Grifix
 * @package  Grifix\Admin\Application\Query\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MenuItemHasNoRouteException extends InvalidArgumentException
{
    
    protected $itemId;
    
    /**
     * MenuItemHasNoTitleException constructor.
     *
     * @param string $itemId
     */
    public function __construct(string $itemId)
    {
        $this->itemId = $itemId;
        $this->message = sprintf('Menu item "%s" has no route!', $itemId);
        parent::__construct();
    }
}
