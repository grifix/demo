<?php

declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

class EmailInputBuilder extends AbstractInputBuilder
{
    protected function prepareVars(): array
    {
        return array_merge(parent::prepareVars(), ['type' => 'email']);
    }
}
