<?php

declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class TextAreaInputBuilder extends AbstractInputBuilder implements TextAreaInputBuilderInterface
{

    /** @var int */
    protected $width = 200;

    /** @var int */
    protected $height = 100;

    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_Textarea');
        parent::__construct($view);
    }

    public function setWidth(int $width): TextAreaInputBuilderInterface
    {
        $this->width = $width;
        return $this;
    }

    public function setHeight(int $height): TextAreaInputBuilderInterface
    {
        $this->height = $height;
        return $this;
    }

    public function prepareVars(): array
    {
        return array_merge(
            parent::prepareVars(),
            [
                'opt' => [
                    'width' => $this->width,
                    'height' => $this->height
                ]
            ]
        );
    }
}
