<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class SwitchInputBuilder extends AbstractInputBuilder implements SwitchInputBuilderInterface
{

    /** @var string */
    protected $partial = 'grifix.admin.{skin}.input.prt.switch';

    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_Switch');
        parent::__construct($view);
    }
}
