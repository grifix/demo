<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

interface ComboInputBuilderInterface extends InputBuilderInterface
{
    public function setDataUrl(?string $dataUrl): ComboInputBuilderInterface;

    public function setValueField(?string $valueField): ComboInputBuilderInterface;

    public function setTextField(?string $textField): ComboInputBuilderInterface;
}
