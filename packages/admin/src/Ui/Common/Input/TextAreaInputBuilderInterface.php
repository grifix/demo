<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

interface TextAreaInputBuilderInterface extends InputBuilderInterface
{
    public function setWidth(int $width): TextAreaInputBuilderInterface;

    public function setHeight(int $height): TextAreaInputBuilderInterface;
}
