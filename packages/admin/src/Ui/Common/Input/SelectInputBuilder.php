<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class SelectInputBuilder extends AbstractInputBuilder implements SelectInputBuilderInterface
{
    /**
     * @var string[]
     */
    protected $options = [];

    /** @var string */
    protected $partial = 'grifix.admin.{skin}.input.prt.select';

    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_Select');
        parent::__construct($view);
    }

    public function addAddOption(string $id, string $text): SelectInputBuilderInterface
    {
        $this->options[$id] = $text;
        return $this;
    }

    public function setOptions(array $options): SelectInputBuilderInterface
    {
        $this->options = $options;
        return $this;
    }

    protected function prepareVars(): array
    {
        return array_merge(
            parent::prepareVars(),
            [
               'options' => $this->options
            ]
        );
    }
}
