<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

interface SelectInputBuilderInterface extends InputBuilderInterface
{
    public function addAddOption(string $id, string $text): SelectInputBuilderInterface;

    public function setOptions(array $values): SelectInputBuilderInterface;
}
