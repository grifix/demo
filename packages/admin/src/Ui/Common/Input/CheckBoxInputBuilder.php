<?php declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class CheckBoxInputBuilder extends AbstractInputBuilder implements CheckBoxInputBuilderInterface
{
    
    protected $partial = 'grifix.admin.{skin}.input.prt.checkbox';

    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_CheckBox');
        parent::__construct($view);
    }
}
