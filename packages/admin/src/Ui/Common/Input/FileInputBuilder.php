<?php declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class FileInputBuilder extends AbstractInputBuilder
{
    
    protected $partial = 'grifix.admin.{skin}.input.prt.file';

    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_File');
        parent::__construct($view);
    }
}
