<?php declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

abstract class AbstractInputBuilder implements InputBuilderInterface
{
    /** @var ViewInterface  */
    protected $view;

    /** @var bool  */
    protected $boxEnabled = true;

    /** @var string|null */
    protected $placeholder;

    /** @var array  */
    protected $class = ['admin-form-input'];

    /** @var string|null */
    protected $dataRole;

    /** @var string|null */
    protected $name;

    /** @var string|null */
    protected $label;

    /** @var string */
    protected $partial = 'grifix.admin.{skin}.input.prt.input';

    /** @var bool */
    protected $required = false;

    /** @var string|null */
    protected $value;

    /** @var string */
    protected $boxPartial = 'grifix.admin.{skin}.input.prt.inputBox';

    /** @var string|null */
    protected $description;

    public function __construct(ViewInterface $view)
    {
        $this->view = $view;
    }

    protected function prepareVars(): array
    {
        return [
            'name' => $this->name,
            'class' => implode(' ', array_unique($this->class)),
            'placeholder' => $this->placeholder,
            'dataRole' => $this->dataRole,
            'value' => $this->value,
            'required' => $this->required,
            'description' => $this->description
        ];
    }

    private function render(): string
    {
        return $this->view->renderPartial(
            $this->partial,
            $this->prepareVars()
        );
    }

    public function build(): string
    {
        if ($this->boxEnabled) {
            return $this->renderBox();
        } else {
            return $this->render();
        }
    }

    public function setDataRole(string $dataRole): InputBuilderInterface{
        $this->dataRole = $dataRole;
        return $this;
    }

    public function setBoxPartial(string $boxPartial): InputBuilderInterface
    {
        $this->boxPartial = $boxPartial;
        return $this;
    }

    public function setLabel(string $label): InputBuilderInterface
    {
        $this->label = $label;
        
        return $this;
    }

    public function setRequired(bool $required = true): InputBuilderInterface
    {
        $this->required = $required;
        
        return $this;
    }

    public function setPlaceholder(string $placeholder): InputBuilderInterface
    {
        $this->placeholder = $placeholder;
        
        return $this;
    }
    

    public function setValue(?string $value): InputBuilderInterface
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDescription(string $description): InputBuilderInterface
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return string
     */
    private function renderBox(): string
    {
        return $this->view->renderPartial(
            $this->boxPartial,
            [
                'label' => $this->label,
                'input' => $this->render(),
                'description' => $this->description,
                'required' => $this->required,
                'name' => $this->name
            ]
        );
    }

    public function setPartial(string $partial): InputBuilderInterface
    {
        $this->partial = $partial;
        
        return $this;
    }

    public function setName(string $name): InputBuilderInterface
    {
        $this->name = $name;
        
        return $this;
    }
    

    public function enableBox(): InputBuilderInterface
    {
        $this->boxEnabled = true;
        
        return $this;
    }
    

    public function disabledBox(): InputBuilderInterface
    {
        $this->boxEnabled = false;
        
        return $this;
    }
    

    public function setClass(string $class): InputBuilderInterface
    {
        $this->class = [$class];
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function addClass(string $class): InputBuilderInterface
    {
        $this->class[] = $class;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function removeClass(string $class): InputBuilderInterface
    {
        foreach ($this->class as $i => $v) {
            if ($v === $class) {
                unset($this->class[$i]);
            }
        }
        
        return $this;
    }
}
