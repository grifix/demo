<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input\Exception;

class NotSupportedMethodException extends \RuntimeException
{
    public function __construct(string $method, string $class)
    {
        parent::__construct(sprintf('Method %s is not supported in %s', $method, $class));
    }
}
