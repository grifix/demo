<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

interface InputBuilderInterface
{
    public function setPartial(string $partial):InputBuilderInterface;

    public function setName(string $name): InputBuilderInterface;

    public function removeClass(string $class): InputBuilderInterface;

    public function addClass(string $class): InputBuilderInterface;

    public function setClass(string $class): InputBuilderInterface;

    public function disabledBox(): InputBuilderInterface;

    public function enableBox(): InputBuilderInterface;

    public function build(): string;

    public function setValue(string $value): InputBuilderInterface;

    public function setLabel(string $label): InputBuilderInterface;

    public function setPlaceholder(string $placeholder): InputBuilderInterface;

    public function setRequired(bool $required = true): InputBuilderInterface;

    public function setDescription(string $description): InputBuilderInterface;

    public function setBoxPartial(string $boxPartial): InputBuilderInterface;

    public function setDataRole(string $dataRole): InputBuilderInterface;
}
