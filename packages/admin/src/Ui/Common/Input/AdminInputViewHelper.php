<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\Helper\AbstractViewHelper;

class AdminInputViewHelper extends AbstractViewHelper
{
    protected ?InputBuilderFactoryInterface $inputBuilderFactory;

    protected function init(): void
    {
        $this->inputBuilderFactory = $this->getShared(InputBuilderFactoryInterface::class);
        parent::init();
    }

    public function text(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createTextInputBuilder($this->view);
    }

    public function password(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createPasswordInputBuilder($this->view);
    }

    public function email(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createEmailInputBuilder($this->view);
    }

    public function file(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createFileInputBuilder($this->view);
    }

    public function checkBox(): CheckBoxInputBuilderInterface
    {
        return $this->inputBuilderFactory->createCheckBoxInputBuilder($this->view);
    }

    public function combo(): ComboInputBuilderInterface
    {
        return $this->inputBuilderFactory->createComboInputBuilder($this->view);
    }

    public function select(): SelectInputBuilderInterface
    {
        return $this->inputBuilderFactory->createSelectInputBuilder($this->view);
    }

    public function date(): DateInputBuilderInterface
    {
        return $this->inputBuilderFactory->createDateInputBuilder($this->view);
    }

    public function dateTime(): DateInputBuilderInterface
    {
        return $this->inputBuilderFactory->createDateTimeInputBuilder($this->view);
    }

    public function textarea(): TextAreaInputBuilderInterface
    {
        return $this->inputBuilderFactory->createTextareaInputBuilder($this->view);
    }
}
