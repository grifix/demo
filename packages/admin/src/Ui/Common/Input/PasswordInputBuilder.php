<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Admin\Ui\Common\Input\Exception\NotSupportedMethodException;

/**
 * Class TextInputBuilder
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Form
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class PasswordInputBuilder extends AbstractInputBuilder
{
    /** @noinspection PhpMissingParentCallCommonInspection */

    public function setValue(?string $value): InputBuilderInterface
    {
        throw new NotSupportedMethodException('setValue', get_class($this));
    }
    
    /**
     * @return array
     */
    protected function prepareVars(): array
    {
        return array_merge(parent::prepareVars(), ['type' => 'password']);
    }
}
