<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\View\ViewInterface;

class InputBuilderFactory extends AbstractFactory implements InputBuilderFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createTextInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(TextInputBuilder::class, $view);
    }

    public function createPasswordInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(PasswordInputBuilder::class, $view);
    }

    public function createEmailInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(EmailInputBuilder::class, $view);
    }

    public function createFileInputBuilder(ViewInterface $view): InputBuilderInterface
    {
        return $this->createInputBuilder(FileInputBuilder::class, $view);
    }

    public function createInputBuilder(string $class, ViewInterface $view): InputBuilderInterface
    {
        $class = $this->makeClassName($class);

        return new $class($view);
    }

    public function createComboInputBuilder(ViewInterface $view): ComboInputBuilderInterface
    {
        /** @var ComboInputBuilderInterface $result */
        $result = $this->createInputBuilder(ComboInputBuilder::class, $view);
        return $result;
    }

    public function createSelectInputBuilder(ViewInterface $view): SelectInputBuilderInterface
    {
        /** @var SelectInputBuilder $result */
        $result = $this->createInputBuilder(SelectInputBuilder::class, $view);
        return $result;
    }

    public function createDateInputBuilder(ViewInterface $view): DateInputBuilderInterface
    {
        /** @var DateInputBuilderInterface $result */
        $result = $this->createInputBuilder(DateInputBuilder::class, $view);
        return $result;
    }

    public function createDateTimeInputBuilder(ViewInterface $view): DateInputBuilderInterface
    {
        /** @var DateInputBuilderInterface $result */
        $result = $this->createInputBuilder(DateTimeInputBuilder::class, $view);
        return $result;
    }

    public function createCheckBoxInputBuilder(ViewInterface $view): CheckBoxInputBuilderInterface
    {
        /** @var CheckBoxInputBuilderInterface $result */
        $result = $this->createInputBuilder(CheckBoxInputBuilder::class, $view);
        return $result;
    }

    public function createTextareaInputBuilder(ViewInterface $view): TextAreaInputBuilderInterface
    {
        /** @var TextAreaInputBuilderInterface $result */
        $result = $this->createInputBuilder(TextAreaInputBuilder::class, $view);
        return $result;
    }
}
