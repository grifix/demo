<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class DateTimeInputBuilder extends AbstractInputBuilder implements DateInputBuilderInterface
{
    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_DateTime');
        parent::__construct($view);
    }
}
