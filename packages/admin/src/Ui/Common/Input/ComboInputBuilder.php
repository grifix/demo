<?php declare(strict_types = 1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class ComboInputBuilder extends AbstractInputBuilder implements ComboInputBuilderInterface
{
    /** @var string|null */
    protected $dataUrl;

    /** @var string */
    protected $valueField = 'id';

    /** @var string */
    protected $textField = 'name';

    /** @var int */
    protected $minChars = 1;

    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_Combo');
        parent::__construct($view);
    }

    public function setDataUrl(?string $dataUrl): ComboInputBuilderInterface
    {
        $this->dataUrl = $dataUrl;
        return $this;
    }

    public function setValueField(?string $valueField): ComboInputBuilderInterface
    {
        $this->valueField = $valueField;
        return $this;
    }

    public function setTextField(?string $textField): ComboInputBuilderInterface
    {
        $this->textField = $textField;
        return $this;
    }

    protected function prepareVars(): array
    {
        return array_merge(
            parent::prepareVars(),
            [
                'opt' => [
                    'dataUrl' => $this->dataUrl,
                    'valueField' => $this->valueField,
                    'textField' => $this->textField,
                    'minChars' => $this->minChars
                ]
            ]
        );
    }
}
