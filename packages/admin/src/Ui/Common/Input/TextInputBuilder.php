<?php

declare(strict_types=1);

namespace Grifix\Admin\Ui\Common\Input;

use Grifix\Kit\View\ViewInterface;

class TextInputBuilder extends AbstractInputBuilder
{
    public function __construct(ViewInterface $view)
    {
        $this->addClass('wg-grifix_admin_input_Text');
        parent::__construct($view);
    }
}
