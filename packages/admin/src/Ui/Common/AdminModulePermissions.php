<?php
declare(strict_types=1);

namespace Grifix\Admin\Ui\Common;

interface AdminModulePermissions
{
    public const SHOW_DESKTOP = 'grifix.admin.desktop';
}
