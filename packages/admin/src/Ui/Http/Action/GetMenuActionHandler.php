<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Admin\Ui\Http\Action;

use Grifix\Admin\Application\Query\GetMenuQuery;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class GetMenuActionHandler extends AbstractActionHandler
{

    public function hasSideEffects(): bool
    {
        return false;
    }

    public function __invoke(array $params = []): array
    {
        return ['items' => $this->executeQuery(new GetMenuQuery())];
    }
}
