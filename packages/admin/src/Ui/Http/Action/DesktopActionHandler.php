<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Admin\Ui\Http\Action;


use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class DesktopActionHandler extends AbstractActionHandler
{

    public function hasSideEffects(): bool
    {
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function __invoke(array $params = []):array
    {
        return [];
    }
}
