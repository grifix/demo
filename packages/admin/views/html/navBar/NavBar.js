/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Button.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {},
        {
            init: function () {
                var that = this;
                that.elSignOutButton = that.find('signOut');
                that.elStartButton = that.find('start');
                that.elMenu = that.find('menu');
                that.elMenu.menu({
                    hideOnUnhover:false,
                    onClick: function (item) {
                        gfx.showLoader();
                        window.location = $(item.target).data('url');
                    }
                });
                that.prepareSignOutButton();
                that.prepareStartButton();


                gfx.Widget.prototype.init.call(that);
            },

            prepareSignOutButton:function () {
                var that = this;
                that.elSignOutButton.grifix_admin_Button({
                    size:'large',
                    plain: true,
                    iconCls: 'ico-shutDown-24',
                    on:{
                        click: function () {
                            $.post(gfx.makeActionUrl('grifix.acl.signOut'),null, function () {
                                window.location = '/';
                            });
                        }
                    }
                });
            },

            prepareStartButton:function () {
                var that = this;
                that.elStartButton.grifix_admin_Button({
                    size:'large',
                    plain: true,
                    iconCls: 'ico-start-24',
                    on:{
                        click: function () {
                            that.elMenu.show();
                        }
                    }
                });
            }

        }
    );
});
