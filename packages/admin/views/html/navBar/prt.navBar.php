<?php
/**@var $this Grifix\Kit\View\ViewInterface */
declare(strict_types = 1);
?>
<div class="<?= $this->makeCssClass() ?> <?= $this->getWidgetClass() ?>">
    <div class="ico-exit"></div>
        <button
                title="<?= $this->translate('grifix.admin.menu') ?>"
                data-role="start">
        </button>
        <button
                title="<?= $this->translate('grifix.acl.signOut') ?>"
                data-role="signOut">
        </button>
        <div data-role="menu">
            <?= $this->renderComponent('grifix.admin.getMenu', 'grifix.admin.{skin}.navBar.prt.menu')?>
        </div>
</div>
