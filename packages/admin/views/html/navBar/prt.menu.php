<?php declare(strict_types = 1);

namespace {

    use Grifix\Admin\Application\Contract\MenuFinder\MenuItemDto;
    use Grifix\Kit\View\Helper\StringViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    $stringHelper = $this->getHelper(StringViewHelper::class);
    ?>

    <?php foreach ($this->getVar('items') as $item): /**@var $item MenuItemDto */ ?>
        <div data-url="<?= $item->getUrl() ?>">
            <?php if (count($item->getChildren())): ?>
                <span><?= $this->entitle($stringHelper->ucFirst($item->getTitle())) ?></span>
                <div>
                    <?= $this->renderPartial('grifix.admin.{skin}.navBar.prt.menu',
                        ['items' => $item->getChildren()]) ?>
                </div>
            <?php else: ?>
                <?= $this->entitle($stringHelper->ucFirst($item->getTitle())) ?>
            <?php endif ?>
        </div>
    <?php endforeach ?>
    <?php
}
