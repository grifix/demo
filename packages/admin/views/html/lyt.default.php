<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\Helper\ConfigViewHelper;
    use Grifix\Kit\View\Helper\LocaleViewHelper\LocaleViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    $projectName = $this->getHelper(ConfigViewHelper::class)->getConfig('grifix.shared.main.projectName');
    $lang = $this->getHelper(LocaleViewHelper::class)->getCurrentLangCode();

    ?>
    <html>
    <head>
        <title>
            <?php $this->startBlock('title') ?>

            <?= $this->translate('grifix.admin.panel') ?> - <?= $projectName ?>

            <?php $this->endBlock() ?>
        </title>

        <link rel="stylesheet" type="text/css" href="/assets/grifix/admin/easyUi/themes/metro/easyui.css">
        <link rel="stylesheet" type="text/css" href="/assets/grifix/admin/easyUi/themes/icon.css">

        <?php $this->includeJsPack('vendor', [
            'jquery/dist/jquery.min.js',
            'jquery-form/dist/jquery.form.min.js',
            'moment/min/moment.min.js',
            'grifix/admin/easyUi/jquery.easyui.min.js',
            'grifix/admin/easyUi/extensions/datagrid-filter.js',
            'grifix/admin/easyUi/extensions/datagrid-cellediting.js',
            'grifix/admin/easyUi/extensions/datagrid-dnd.js',
            sprintf('grifix/admin/easyUi/locale/easyui-lang-%s.js', $lang),
            '{src}/grifix/kit/views/{skin}/gfx.js',
            '{src}/grifix/admin/views/{skin}/gfx.js',
            '{src}/grifix/kit/views/{skin}/Widget.js',
            '{src}/grifix/admin/views/{skin}',
        ])
        ?>

        <?php $this->includeCssPack('global', [
            '{src}/grifix/kit/views/{skin}/reset.css',
            '{src}/{module}/views/{skin}/admin/ico.css',
            '{src}/grifix/admin/views/{skin}/admin.css',
            '{src}/grifix/admin/views/{skin}'
        ]) ?>


        <?php $this->startBlock('head') ?>

        <?php $this->endBlock() ?>

        <?php $this->includeViewJs() ?>
        <script type="text/javascript">
            jQuery(function () {
                gfx.initWidgets();
                gfx.setUp();
            });
        </script>

        <?php $this->addCss('{src}/grifix/admin/views/{skin}/lyt.default.css') ?>
        <?php $this->addCss('{src}/grifix/admin/views/{skin}/navBar') ?>
        <?php $this->includeViewCss() ?>
    </head>
    <body class="<?= $this->makeCssClass() ?>">


    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north'" style="height:50px">
            <?php $this->startBlock('navBar') ?>
            <?= $this->renderPartial('grifix.admin.{skin}.navBar.prt.navBar') ?>
            <?php $this->endBlock() ?>
        </div>
        <div data-options="region:'center', border:false">
            <?php $this->startBlock('content') ?>

            <?php $this->endBlock() ?>
        </div>
    </div>

    </body>
    </html>
    <?php
}
