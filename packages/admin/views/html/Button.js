/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            on: {
                click: function () {
                    return false;
                }
            },
            size: 'small',
            plain: false,
            iconCls: null,
        },
        {
            init: function () {
                var that = this;

                var config = {
                    iconCls: that.cfg.iconCls,
                    plain: that.cfg.plain,
                    size: that.cfg.size
                };

                that.el().linkbutton(config);
                that.el().click(function () {
                    that.fireEvent('click')
                });
                gfx.Widget.prototype.init.call(that);

            }

        }
    );

});
