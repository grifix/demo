jQuery(function ($) {

    var parent = gfx.clone(gfx);

    gfx.showMessage = function (text, type) {
        switch (type) {
            case 'error':
                $.messager.alert(php(/*$translate('grifix.kit.error')*/), text, 'error');
                break;

            case 'warning':
                $.messager.alert(php(/*$translate('grifix.kit.warning')*/), text, 'warning');
                break;

            default:
                $.messager.alert(php(/*$translate('grifix.kit.info')*/), text, 'info');
                break;
        }
    };

    gfx.confirm = function (handler, title, message) {
        if (!title) {
            title = php(/*$t('grifix.kit.confirmation')*/)
        }
        if (!message) {
            message = php(/*$t('grifix.kit.msg_areYouSure')*/)
        }
        $.messager.confirm(title, message, handler);
    };

    gfx.initWidgets = function (elContainer) {
        $.parser.parse(elContainer);
        return parent.initWidgets.call(this, elContainer);
    };

    gfx.openWidgetInWindow = function (widgetConfig, windowConfig, handler) {
        if (!windowConfig) {
            windowConfig = {};
        }
        if(!widgetConfig.opt){
            widgetConfig.opt = {};
        }
        widgetConfig.opt._win = true;

        $.messager.progress();
        this.createWidget(widgetConfig, function (elWidget) {
            if(!windowConfig.title){
                windowConfig.title = elWidget.data(widgetConfig.name).getTitle();
            }

            elWidget.grifix_admin_Window(windowConfig);
            if (handler) {
                handler.call(this, elWidget);
            }
            elWidget.data('grifix_admin_Window').reDraw();
            $.messager.progress('close');
        })
    };

    gfx.capitalize = function (s) {
        return s.charAt(0).toUpperCase() + s.slice(1)
    }
});

