/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";

    var parent = gfx.Widget;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            modal: true,
            collapsible: false,
            underCursor: false,
            minimizable: false,
            left: null,
            top: null,
            resizable: true,
            height: 530,
            width: 700,
            noheader: false,
            inline: false,
            title: 'Window',
            border: true,
            elContainer: $('body'),
            on: {
                close: function () {
                }
            }
        },
        {
            const:{
                inline:{
                    resizable:false,
                    title: false,
                    border: false,
                    noheader: true,
                    height: 40,
                    widht: 150,
                    underCursor: true,
                }
            },
            source: '<div>' + php(/*$t('grifix.kit.loading')*/) + '</div>',
            init: function () {
                var that = this;
                if (!that.el().parent()[0]) {
                    that.el().appendTo(that.cfg.elContainer);
                }
                if(that.cfg.underCursor){
                    that.cfg.left = gfx.mouse.x;
                    that.cfg.top = gfx.mouse.y;
                }

                that.el().css('z-index', gfx.findMaxZindex());
                that.el().window(that.configWindow());

                parent.prototype.init.call(that)
            },

            reDraw: function () {
                var that = this;
                setTimeout(function () {
                    that.el().window('resize', {
                        width: that.cfg.widht,
                        height: that.cfg.height
                    });
                    if(!that.cfg.underCursor){
                        that.el().window('center');
                    }
                }, 1);
            },

            close: function () {
                this.el().window('close');
            },

            configWindow: function () {
                var that = this;
                return {
                    modal: that.cfg.modal,
                    left: that.cfg.left,
                    top: that.cfg.top,
                    title: that.cfg.title,
                    collapsible: that.cfg.collapsible,
                    minimizable: that.cfg.minimizable,
                    noheader: that.cfg.noheader,
                    resizable: that.cfg.resizable,
                    width: that.cfg.width,
                    height: that.cfg.height,
                    inline: that.cfg.inline,
                    border: that.cfg.border,
                    onClose: function () {
                        that.el().remove();
                        that.fireEvent('close');
                    }
                };
            },

            setTitle: function (title) {
                this.el().window('setTitle', title);
                return this.el();
            }

        }
    );
});
