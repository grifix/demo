/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Window.js*/
jQuery(function ($) {
    "use strict";

    $.extend(true, $.fn.datagrid.defaults.operators, {
        nofilter: {
            text: php(/*$translate('grifix.admin.clearFilter')*/)
        },
        equal: {
            text: php(/*$translate('grifix.admin.equal')*/)
        },
        notequal: {
            text: php(/*$translate('grifix.admin.notEqual')*/)
        },
        beginwith: {
            text: php(/*$translate('grifix.admin.beginWith')*/)
        },
        endwith: {
            text: php(/*$translate('grifix.admin.endWith')*/)
        },
        less: {
            text: php(/*$translate('grifix.admin.less')*/)
        },
        lessorequal: {
            text: php(/*$translate('grifix.admin.lessOrEqual')*/)
        },
        greater: {
            text: php(/*$translate('grifix.admin.greater')*/)
        },
        greaterorequal: {
            text: php(/*$translate('grifix.admin.greaterOrEqual')*/)
        },
        contains: {
            text: php(/*$translate('grifix.admin.contains')*/)
        }
    });

    $.extend($.fn.datagrid.defaults.editors, {
        money: {
            init: function(container, options){
                var input = $('<input style="height:31px" type="number" min="0.00" step="0.01" class="datagrid-editable-input">').appendTo(container);
                return input;
            },
            destroy: function(target){
                $(target).remove();
            },
            getValue: function(target){
                return ($(target).val()*100).toFixed(0);
            },
            setValue: function(target, value){
                $(target).val((value/100).toFixed(2));
            },
            resize: function(target, width){
                $(target)._outerWidth(width);
            }
        },
        number: {
            init: function(container, options){
                var input = $('<input style="height:31px" type="number" min="0" step="1" class="datagrid-editable-input">').appendTo(container);
                return input;
            },
            destroy: function(target){
                $(target).remove();
            },
            getValue: function(target){
                return $(target).val();
            },
            setValue: function(target, value){
                $(target).val(value);
            },
            resize: function(target, width){
                $(target)._outerWidth(width);
            }
        }
    });

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            api: {
                load: null,
                delete: null,
                show: null
            },
            columns: [],
            numOfRows: 30,
            sortColumn: 'id',
            sortDirection: 'asc',
            checkbox: true,
            cellEditing: false,
            onDrop: null,
            creator: {
                widget: {},
                window: {}
            },
            editor: {
                widget: {},
                window: {}
            }
        },
        {
            editedRowIndex: null,
            currentRow: null,
            init: function () {
                var that = this;
                that.elToolbar = that.find('toolbar');
                that.elGrid = that.find('grid');
                that.elRefresh = that.find('refresh');
                that.elDelete = that.find('delete');
                that.elAdd = that.find('add');
                that.elSave = that.find('save');
                that.elUndo = that.find('undo');

                if (that.cfg.api.delete) {
                    that.cfg.columns.unshift({
                        field: '_delete',
                        type: 'button',
                        iconCls: 'ico-delete-16',
                        label: php(/*$translate('grifix.kit.delete')*/),
                        frozen: true,
                        onClick: function (record) {
                            gfx.confirm(
                                function (r) {
                                    if (r) {
                                        $.ajax({
                                            url: that.cfg.api.delete,
                                            method: 'post',
                                            data: {
                                                id: record.id
                                            },
                                            success: function () {
                                                that.reloadGrid()
                                            }
                                        })
                                    }
                                },
                                php(/*$t('grifix.kit.deletion')*/)
                            );
                        }
                    });
                }

                if (that.cfg.editor.widget.name) {
                    that.cfg.columns.unshift({
                        field: '_edit',
                        type: 'button',
                        iconCls: 'ico-edit-16',
                        label: php(/*$translate('grifix.kit.edit')*/),
                        frozen: true,
                        onClick: function (record) {
                            that.openEditor(record);
                        }
                    });
                }

                that.createGrid(that.configGrid());

                if (!that.cfg._win) {
                    that.el().panel({
                        fit: true,
                        border: false,
                        noheader: true
                    });
                }

                that.initButtons();


                gfx.Widget.prototype.init.call(that);

            },

            createGrid: function (config) {
                var that = this;
                config.height = 200;
                that.elGrid.datagrid(config);
                that.addFilters(that.cfg.columns);
                if (that.cfg.cellEditing) {
                    that.elGrid.datagrid('enableCellEditing');
                }
            },

            addFilters: function (columns) {
                var that = this;
                var filters = [];
                $.each(columns, function (i, column) {
                    var filter = {
                        field: column.field,
                        type: 'label'
                    };

                    if (column.filter) {
                        switch (column.filter.type) {
                            case 'number':
                                filter.field = column.field;
                                filter.type = 'numberbox';
                                if (!filter.op) {
                                    filter.op = ['equal', 'notequal', 'less', 'lessorequal', 'greater', 'greaterorequal'];
                                }
                                break;

                            case 'text':
                                filter.field = column.field;
                                filter.type = 'text';
                                if (!filter.op) {
                                    filter.op = ['equal', 'notequal', 'contains', 'beginwith', 'endwith'];
                                }
                                break;

                            case 'id':
                                filter.field = column.field;
                                filter.type = 'text';
                                if (!filter.op) {
                                    filter.op = ['equal'];
                                }
                                break;

                            case 'date':
                                filter.field = column.field;
                                filter.type = 'datebox';
                                if (!filter.op) {
                                    filter.op = ['equal', 'notequal', 'less', 'lessorequal', 'greater', 'greaterorequal'];
                                }
                                break;

                            case 'int':
                                filter.field = column.field;
                                filter.type = 'numberbox';
                                if (!filter.op) {
                                    filter.op = ['equal', 'notequal', 'less', 'lessorequal', 'greater', 'greaterorequal'];
                                }
                                break;
                        }
                    }

                    filters.push(filter);

                });
                that.elGrid.datagrid('enableFilter', filters);

            },

            initButtons: function () {
                var that = this;
                that.elRefresh.linkbutton({
                    plain: true
                }).click(function () {
                    that.reloadGrid();
                });

                that.elAdd.linkbutton({
                    plain: true
                }).click(function () {
                    that.openCreator();
                });

                that.elDelete.linkbutton({
                    plain: true
                }).click(function () {
                    that.deleteChecked();
                });

                that.elSave.linkbutton({
                    plain: true
                }).click(function () {
                    if (null !== that.editedRowIndex) {
                        that.elGrid.datagrid('endEdit', that.editedRowIndex);
                    }
                });

                that.elUndo.linkbutton({
                    plain: true
                }).click(function () {
                    if (that.editedRowIndex) {
                        that.elGrid.datagrid('cancelEdit', that.editedRowIndex);
                    }
                });

            },

            openCreator: function () {
                var that = this;
                gfx.openWidgetInWindow(that.cfg.creator.widget, that.cfg.creator.window, function (elWidget) {
                    elWidget.data('grifix_admin_Window').on('close', function () {
                        that.reloadGrid();
                    });
                });
            },

            openEditor: function (record, selectedTab) {
                var that = this;
                var editor = {
                    opt: {
                        wgGrid: self,
                        selectedTab: selectedTab
                    }
                };
                if (record && record.id) {
                    editor = $.extend(true, {}, editor, {
                        source: {
                            data: {
                                id: record.id
                            }
                        },
                        opt: {
                            recordId: record.id
                        }
                    }, that.cfg.editor.widget);
                } else {
                    editor = $.extend(true, {}, editor, that.cfg.editor.widget);
                }
                gfx.openWidgetInWindow(editor, that.cfg.editor.window, function (elWidget) {
                    elWidget.data('grifix_admin_Window').on('close', function () {
                        that.reloadGrid();
                    });
                });
            },

            getColumn: function (filed) {
                var that = this;
                var result = null;
                $.each(that.cfg.columns, function (k, column) {
                    if (column.field == filed) {
                        result = column;
                        return false;
                    }
                });

                return result;
            },

            changeCellValue: function (field, value, row) {
                let that = this;
                let oldValue = that.curentRow[field];
                let column = that.getColumn(field);
                if (!column.editor) {
                    throw new Error('Column' + field + 'has no editor!')
                }
                if(column.editor === 'money'){
                    value = value*100;
                }

                var postFieldName = field;
                if (column.editor.postFieldName) {
                    postFieldName = column.editor.postFieldName;
                }
                var data = {
                    id: row.id
                };
                data[postFieldName] = value;
                $.ajax({
                    url: column.editor.url,
                    type: 'post',
                    data: data,
                    success: function () {
                        that.editedRowIndex = null;
                    },
                    error: function () {
                        that.cancelCellEdit(field, oldValue);
                    }
                });

            },

            cancelCellEdit: function (field, oldValue) {
                let that = this;
                that.elGrid.datagrid('cancelEdit', that.editedRowIndex);
                that.elGrid.datagrid('editCell', {
                    index: that.editedRowIndex,
                    field: field
                });
                that.elGrid.datagrid('input', {
                    index: that.editedRowIndex,
                    field: field
                }).val(oldValue);
                that.elGrid.datagrid('endEdit', that.editedRowIndex);
                that.elGrid.datagrid('selectRow', that.editedRowIndex);
            },

            configGrid: function () {
                var that = this;
                var result = {
                    method: 'get',
                    idField: 'id',
                    pagination: true,
                    clientPaging: false,
                    url: that.cfg.api.load,
                    fit: true,
                    border: false,
                    singleSelect: true,
                    remoteFilter: true,
                    pageSize: that.cfg.numOfRows,
                    toolbar: that.elToolbar,
                    checkOnSelect: false,
                    selectOnCheck: false,
                    onBeforeCellEdit: function(index, field) {
                        console.log(index, field);
                    },
                    onBeforeEdit: function (index, row) {
                        that.curentRow = $.extend({}, row);
                        that.editedRowIndex = index;
                    },
                    onEndEdit: function (index, row, changes) {
                        $.each(changes, function (field, value) {
                            let column = that.getColumn(field);
                            if (column.editor) {
                                if (column.editor.onChange) {
                                    column.editor.onChange.call(that, that.curentRow[field], value, row);
                                } else {
                                    that.changeCellValue(field, value, row);
                                }
                            }
                        });
                    },
                    sortName: that.cfg.sortColumn,
                    sortOrder: that.cfg.sortDirection,
                    columns: [[]],
                    frozenColumns: [[]],
                    onClickCell: function (rowIndex, field, value) {
                        var column = that.getColumn(field);
                        if (column.onClick) {
                            column.onClick.call(that, that.elGrid.datagrid('getData').rows[rowIndex], rowIndex, value);
                        }

                        that.fireEvent('clickCell', {
                            rowIndex: rowIndex,
                            field: field,
                            value: value
                        });
                    },
                    onDblClickCell: function (rowIndex, field, value) {
                        var column = that.getColumn(field);
                        if (column.onDblClick) {
                            column.onDblClick.call(that, that.elGrid.datagrid('getData').rows[rowIndex], rowIndex, value);
                        }

                        that.fireEvent('dblClickCell', {
                            rowIndex: rowIndex,
                            field: field,
                            value: value
                        });
                    },
                    loader: function (param, success, error) {
                        var opt = $(this).datagrid('options');

                        param.countTotal = 1;

                        if (param.page) {
                            param.pageIndex = param.page;
                            delete param.page;
                        }

                        if (param.rows) {
                            param.pageSize = param.rows;
                            delete param.rows;
                        }

                        if (param.sort) {
                            param.sortColumn = param.sort;
                            delete param.sort;
                        }

                        if (param.order) {
                            param.sortDirection = param.order;
                            delete param.order;
                        }

                        if (param.filterRules) {
                            var filterRules = $.parseJSON(param.filterRules);
                            var columnFilters = [];
                            $.each(filterRules, function (k, rule) {
                                var condition = rule.op;
                                switch (condition) {
                                    case 'notequal':
                                        condition = 'not_equal';
                                        break;
                                    case 'beginwith':
                                        condition = 'begin_with';
                                        break;
                                    case 'endwith':
                                        condition = 'end_with';
                                        break;
                                    case 'less':
                                        condition = 'less_than';
                                        break;
                                    case 'lessorequal':
                                        condition = 'less_than_or_equal';
                                        break;
                                    case 'greater':
                                        condition = 'greater_than';
                                        break;
                                    case 'greaterorequal':
                                        condition = 'greater_than_or_equal';
                                        break
                                }
                                columnFilters.push({
                                    column: rule.field,
                                    condition: condition,
                                    value: rule.value
                                });
                            });
                            param.columnFilters = columnFilters;
                            delete param.filterRules;
                        }

                        that.fireEvent('beforeLoad', param);

                        $.ajax({
                            type: opt.method,
                            url: opt.url,
                            data: param,
                            dataType: 'json',
                            success: function (data) {
                                success(data);
                                that.fireEvent('load');
                            },
                            error: function () {
                                error.apply(this, arguments);
                            }
                        });
                    }
                };

                $.each(that.cfg.columns, function (k, column) {
                    if (column.frozen) {
                        result.frozenColumns[0].push(that.prepareGridColumn(column, k));
                    } else {
                        result.columns[0].push(that.prepareGridColumn(column, k));
                    }
                });

                if (that.cfg.checkbox) {
                    result.frozenColumns[0].unshift({
                        field: '_checkbox',
                        checkbox: true
                    });
                }

                if (that.cfg.onDrop) {
                    result.onDrop = function (targetRow, sourceRow, point) {
                        that.cfg.onDrop.call(that, targetRow, sourceRow, point)
                    };
                    result.onLoadSuccess = function () {
                        that.elGrid.datagrid('enableDnd');
                    }
                }

                return result;
            },


            prepareGridColumn: function (column, k) {
                var that = this;

                if (!column.field) {
                    throw new Error("There is no field for column " + k);
                }

                var defaultColumn = {
                    sortable: false,
                    width: null,
                    formatter: function (value, row, index, field) {
                        if (column.intl && value) {
                            value = value[gfx.langCode];
                        }
                        return value;
                    }
                };

                if (column.type === 'dateTime') {
                    defaultColumn.formatter = function (value) {
                        if (value) {
                            var date = new Date(value * 1000);
                            return date.toLocaleString(gfx.langCode);
                        }
                        return null;
                    }
                }

                if (column.type === 'bool') {
                    defaultColumn.formatter = function (value) {
                        if (true === value) {
                            return php(/*$translate('grifix.kit.yes')*/)
                        }
                        return php(/*$translate('grifix.kit.no')*/)
                    }
                }

                if (column.type === 'checkbox') {
                    if (!column.label) {
                        throw new Error('There is no label for button ' + column.field);
                    }
                    defaultColumn.formatter = function (value, row, index) {
                        if (true === value) {
                            return '<input title="' + column.label + '" type="checkbox" style="display: block; margin: auto" checked>'
                        } else {
                            return '<input title="' + column.label + '" type="checkbox" style="display: block; margin: auto"></div>'
                        }
                    }
                }

                if (column.type === 'button') {
                    if (!column.label) {
                        throw new Error('There is no label for button ' + column.field);
                    }
                    defaultColumn.formatter = function () {
                        return '<div title="' + column.label + '" style="width:16px; height:16px; cursor:pointer" class="' + column.iconCls + '"></div>';
                    }
                }

                if(column.type === 'money'){
                    defaultColumn.formatter = function (value, row, index) {
                        if(value){
                            return (value/100).toFixed(2);
                        }
                        return value;

                    }
                }

                column = $.extend({}, defaultColumn, column);

                var result = {
                    field: column.field,
                    title: column.title,
                    width: column.width,
                    sortable: column.sortable,
                    formatter: function (value, row, index) {
                        return column.formatter.call(that, value, row, index, column.field)
                    }
                };

                if (column.editor) {
                    result.editor = column.editor.type;
                }

                return result;
            },

            reloadGrid: function () {
                this.elGrid.datagrid('load');
            },

            deleteChecked: function () {
                var that = this;
                if (!that.cfg.api.massDeleteAction) {
                    throw new Error('Mass delete action is not defined in "' + that.name + '"!');
                }
                var checked = that.elGrid.datagrid('getChecked');
                if (!checked.length) {
                    gfx.showError(php(/*$translate('grifix.admin.msg_checkRecordsForDeletion')*/));
                    return false;
                }
                gfx.confirm(
                    function (result) {
                        if (result) {
                            var data = {
                                actions: []
                            };
                            $.each(checked, function (i, o) {
                                data.actions[i] = {
                                    action: that.cfg.api.massDeleteAction,
                                    params: {
                                        id: o.id
                                    }
                                };
                            });
                            $.ajax({
                                url: gfx.routeUrl(php(/*$route('grifix.shared.root.action')*/),),
                                method: 'post',
                                data: data,
                                success: function () {
                                    that.elGrid.datagrid('uncheckAll');
                                    that.elGrid.datagrid('reload');
                                }
                            })
                        }

                    },
                    php(/*$t('grifix.kit.deletion')*/)
                );
            }

        }
    );

});
