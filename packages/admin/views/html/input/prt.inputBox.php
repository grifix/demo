<?php
declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this ViewInterface */
    ?>
    <div data-role="inputBox" class="<?= $this->makeCssClass() ?>">
        <?php if ($this->getVar('label')) : ?>
            <label style="text-transform: capitalize" class="<?= $this->makeCssClass('label') ?>"><?= ($this->getTextVar('label')) ?><?php if ($this->getVar('required')) : ?>*<?php endif ?></label>
        <?php endif; ?>
        <?= $this->getVar('input') ?>
    </div>
    <?php
}
