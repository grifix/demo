/*require {src}/grifix/admin/views/{skin}/input/Input.js*/
jQuery(function ($) {
    "use strict";

    var parent = grifix.admin.input.Input;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            width: 300,
            height: 100
        },
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                that.elInput = that.find('input');
                that.elInput.textbox({
                    prompt: that.el().attr('placeholder'),
                    novalidate: true,
                    multiline: true,
                    width: that.cfg.width,
                    height: that.cfg.height,
                    onChange: function () {
                        that.hideError();
                    }
                });
                parent.prototype.init.call(that);
            }

        }
    );

});
