<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var  ViewInterface $this */
    $placeholder = $this->getTextVar('placeholder');
    if ($placeholder && $this->getVar('required')) {
        $placeholder = $placeholder . '*';
    }
    ?>
    <span
            class="<?= $this->getTextVar('class') ?>"
            data-opt='<?= json_encode($this->getVar('opt')) ?>'
            data-description="<?= $this->getTextVar('description') ?>"
            data-role="<?= $this->getTextVar('dataRole') ?>"
            data-input="<?= $this->getTextVar('name')?>"
    >
        <span data-role="wrapper">
            <input data-role="input"
                   type="<?= $this->getTextVar('type', 'text') ?>"
                   name="<?= $this->getTextVar('name') ?>"
                   placeholder="<?= $placeholder ?>"
                   value="<?= $this->getTextVar('value') ?>"
            >
        </span>
    </span>
    <?php
}
