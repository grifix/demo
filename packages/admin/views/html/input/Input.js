/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";

    var parent = gfx.Widget;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            errorClass: 'admin-input-error'
        },
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                that.elInput = that.find('input');
                that.elWrapper = that.find('wrapper');
                if (that.el().data('description')) {
                    that.el().tooltip({
                        position: 'right',
                        content: that.el().data('description')
                    });
                }

                parent.prototype.init.call(that);
            },

            showError: function (message) {
                var that = this;
                that.el().addClass(that.cfg.errorClass);
                that.elWrapper.tooltip({
                    position: 'bottom',
                    content: message,
                    onShow: function () {
                        that.elWrapper.tooltip('tip').css({
                            backgroundColor: '#fff3f3',
                            borderColor: '#FFA8A8'
                        });
                    }
                });
            },

            hideError: function () {
                var that = this;
                that.el().removeClass(that.cfg.errorClass);
                that.elWrapper.tooltip('destroy');
            }

        }
    );
});
