/*require {src}/grifix/admin/views/{skin}/input/Input.js*/
jQuery(function ($) {
    "use strict";

    var parent = grifix.admin.input.Input;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            buttonText: php(/*$translate('grifix.kit.chooseFile')*/),
            multiple: false,
            buttonIcon: null
        },
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                var config = {
                    buttonText: that.cfg.buttonText
                };
                that.elInput.filebox(config);
            }
        }
    );

});
