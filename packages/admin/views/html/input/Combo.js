/*require {src}/grifix/admin/views/{skin}/input/Input.js*/
jQuery(function ($) {
    "use strict";

    var parent = grifix.admin.input.Input;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            dataUrl: null,
            valueField: 'id',
            textField: 'name',
            minChars: 1
        },
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                var params = {
                    valueField: that.cfg.valueField,
                    textField: that.cfg.textField
                };
                if (that.cfg.dataUrl) {
                    params.mode = 'remote';
                    params.loader = function (param, success, error) {
                        var q = param.q || '';
                        if (q.length <= that.cfg.minChars) {
                            return false
                        }
                        $.ajax({
                            url: that.cfg.dataUrl,
                            data: {
                                columnFilters: [
                                    {
                                        column: that.cfg.textField,
                                        condition: 'contains',
                                        value: q
                                    }
                                ]
                            },
                            dataType: 'json',
                            success: function (data) {
                                console.log(data);
                                success(data);
                            },
                            error: function () {
                                error.apply(this, arguments);
                            }
                        })
                    }
                }
                that.elInput.combobox(params);
            }
        }
    );

});
