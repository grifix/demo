/*require {src}/grifix/admin/views/{skin}/input/Input.js*/
jQuery(function ($) {
    "use strict";

    var parent = grifix.admin.input.Input;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {},
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                that.elInput.datetimebox({
                    parser: function (s) {
                        var t = Date.parse(s);
                        if (!isNaN(t)) {
                            return new Date(t);
                        } else {
                            return new Date();
                        }
                    },
                    formatter: function (date) {
                        return moment(date).format();
                    }
                });
            }
        }
    );

});
