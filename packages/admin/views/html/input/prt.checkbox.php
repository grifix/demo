<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var  ViewInterface $this */
    ?>
    <span
            class="<?= $this->getTextVar('class') ?>"
            data-opt='<?= json_encode($this->getVar('opt')) ?>'
            data-description="<?= $this->getTextVar('description') ?>"
            data-role="<?= $this->getTextVar('dataRole') ?>"
            data-input="<?= $this->getTextVar('name')?>"
    >
        <span data-role="wrapper">
            <input data-role="input"
                   name="<?= $this->getTextVar('name') ?>"
                   value="<?= $this->getTextVar('value') ?>"
            >
        </span>
    </span>
    <?php
}
