/*require {src}/grifix/admin/views/{skin}/input/Input.js*/
jQuery(function ($) {
    "use strict";

    var parent = grifix.admin.input.Input;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {},
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                that.elInput = that.find('input');
                that.elInput.textbox({
                    prompt: that.el().attr('placeholder'),
                    novalidate: true,
                    onChange: function () {
                        that.hideError();
                    }
                });
                parent.prototype.init.call(that);
            }

        }
    );

});
