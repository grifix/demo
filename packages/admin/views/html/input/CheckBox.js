/*require {src}/grifix/admin/views/{skin}/input/Input.js*/
jQuery(function ($) {
    "use strict";

    var parent = grifix.admin.input.Input;
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
        },
        {
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
                var config = {
                    checked: that.elInput.val()
                };
                that.elInput.checkbox(config);
            }
        }
    );

});
