/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";

    var parent = gfx.Widget;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            target: {
                url: null,
                method: 'post'
            },
            on: {
                success: function (result) {
                },
                error: function (result) {
                },
                beforeSubmit: function (data) {
                },
                complete: function () {
                }
            }
        },
        {
            init: function () {
                var that = this;

                that.el().layout({
                    fit: true
                });

                that.elForm = that.find('form');
                that.elSave = that.find('save');

                that.elSave.on('click', function () {
                    that.submit();
                    return false;
                });

                parent.prototype.init.call(that);
            },

            submit: function (onSuccess) {
                $.messager.progress();
                var that = this;
                that.elForm.ajaxSubmit({
                    url: that.cfg.target.url,
                    type: that.cfg.target.type,
                    success: function (r) {
                        that.fireEvent('success', {result: r});
                        if (onSuccess) {
                            onSuccess.call(that, r);
                        }
                        if (that.el().data('grifix_admin_Window')) {
                            that.el().data('grifix_admin_Window').close();
                        }
                    },
                    beforeSubmit: function (data) {
                        $.each(data, function (k, v) {
                            var date = moment(v.value);
                            if (date.isValid()) {
                                data[k].value = date.toISOString()
                            }
                        });
                        that.fireEvent('beforeSubmit', data);
                    },
                    error: function (r) {
                        var error = JSON.parse(r.responseText);
                        $.each(error.errors, function (i, e) {
                            that.addError(e.field, e.message);
                        });
                        that.fireEvent('error', {result: r});
                    },
                    complete: function () {
                        $.messager.progress('close');
                        that.fireEvent('complete');
                    }
                })
            },

            addError: function (field, message) {
                var that = this;
                var elInput = that.el().find('[data-input="' + field + '"]');
                $.each(elInput.data(), function (i, plugin) {
                    if (plugin && plugin.showError) {
                        plugin.showError(message);
                    }
                });
            }
        }
    );
});
