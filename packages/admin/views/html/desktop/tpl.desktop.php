<?php
declare(strict_types = 1);
/**@var $this Grifix\Kit\View\ViewInterface */
$this->inherits('grifix.admin.{skin}.lyt.default');
$this->addJs('{src}/grifix/admin/views/{skin}/desktop');
$this->addCss('{src}/grifix/admin/views/{skin}/desktop');
?>

<?php $this->startBlock('content')?>
    <?= $this->renderComponent('grifix.admin.desktop','grifix.admin.{skin}.desktop.prt.desktop')?>
<?php $this->endBlock()?>