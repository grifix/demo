/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";
    
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {},
        {
            init: function () {
                var that = this;
                that.parent.prototype.init.call(that);
                that.elIcons = that.find('icon');
                that.bindIcons();
            },
    
            bindIcons: function () {
                var that = this;
                that.elIcons.on('click', function () {
                    
                });
            }
        }
    );
    
});

