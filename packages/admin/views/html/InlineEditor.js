/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/input/Text.js*/
jQuery(function ($) {
    "use strict";

    var parent = gfx.Widget;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            submitUrl:null,
            id: null,
            valueField: null,
            on:{
                confirm: function () {

                },
                cancel: function () {

                }
            }
        },
        {

            source: {
                url: gfx.makeWidgetUrl(null, 'demo.catalog.{skin}.admin.product.prt.priceEditor')
            },
            init: function () {
                var that = this;
                that.elConfrm = that.find('confirm');
                that.elCancel = that.find('cancel');
                that.elValue = that.find('value');

                that.elCancel.click(function () {
                    that.el().data('grifix_admin_Window').close();
                    that.fireEvent('cancel');
                });

                that.elConfrm.click(function () {
                    var data = {};
                    data.id = that.cfg.id;
                    data[that.cfg.valueField] = that.elValue.val();

                    $.ajax({
                        url: that.cfg.submitUrl,
                        method: 'post',
                        data: data,
                        success: function () {
                            that.el().data('grifix_admin_Window').close();
                            that.fireEvent('confirm');
                        }
                    });
                });
                that.elValue.focus();
                parent.prototype.init.call(that);
            },
        }
    );
});
