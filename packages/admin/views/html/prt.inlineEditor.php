<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
/**@var $this Grifix\Kit\View\ViewInterface */
declare(strict_types=1);

?>

<div class="<?= $this->getWidgetClass() ?> admin-content" style="display: flex">
    <input type="hidden" name="id" value="<?=$this->getVar('id')?>">
    <table>
        <tr>
            <td style="width: 100%">
                <input data-role="value" style="width: 100%; margin-right: 2px" type="text">
            </td>
            <td>
                <div title="<?=$this->translate('grifix.kit.to_confirm')?>" data-role="confirm" style="height: 24px; width: 24px; cursor:pointer;" class="ico-confirm-24"></div>
            </td>
            <td>
                <div title="<?=$this->translate('grifix.kit.to_cancel')?>" data-role="cancel" style="height: 24px; width: 24px; cursor:pointer;" class="ico-cancel-24"></div>
            </td>
        </tr>
    </table>
</div>
