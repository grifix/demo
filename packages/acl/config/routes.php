<?php

declare(strict_types = 1);

namespace {

    use Grifix\Acl\Ui\Http\Route\Admin\RolesRouteHandler;
    use Grifix\Acl\Ui\Http\Route\Admin\UsersRouteHandler;
    use Grifix\Acl\Ui\Http\Route\ConfirmEmailRouteHandler;
    use Grifix\Acl\Ui\Http\Route\SignInRouteHandler;
    use Grifix\Kit\Route\RouteDefinition;
    use Grifix\Kit\Route\RouteDefinitionBuilder;


    return [
        'root' => RouteDefinition::withChildren(
            '/{locale:locale?}/grifix/acl',
            [
                'admin' => RouteDefinition::withChildren(
                    '/admin',
                    [
                        'users' => RouteDefinition::withHandler('/users', UsersRouteHandler::getAlias()),
                        'roles' => RouteDefinition::withHandler('/roles', RolesRouteHandler::getAlias())
                    ]
                ),
                'signIn' => RouteDefinition::withHandler('/signIn', SignInRouteHandler::getAlias()),
                'confirmEmail' => (new RouteDefinitionBuilder('/confirmEmail/{token}'))
                    ->setHandler(ConfirmEmailRouteHandler::getAlias())
                    ->setMethods(['GET'])
                    ->build()
            ]
        )
    ];
}
