<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {

    use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyConfirmedException;
    use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyExistsException;
    use Grifix\Acl\Domain\User\Exception\CantAssignRoleException;
    use Grifix\Acl\Domain\User\Exception\EmailIsNotConfirmedException;
    use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
    use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
    use Grifix\Acl\Domain\User\Exception\WrongTokenException;
    use Grifix\Acl\Ui\Common\UserHelper\Exception\MoreThanOneUserFoundException;
    use Grifix\Acl\Ui\Common\UserHelper\Exception\UserNotFoundException;
    use Grifix\Kit\Exception\UiExceptionDefinition;

    return [
        UserEmailAlreadyConfirmedException::class => new UiExceptionDefinition(
            'grifix.acl.emailAlreadyConfirmed',
            'grifix.acl.msg_emailAlreadyConfirmed'
        ),

        UserEmailAlreadyExistsException::class => new UiExceptionDefinition(
            'grifix.acl.userEmailAlreadyExists',
            'grifix.acl.msg_userEmailAlreadyExists'
        ),

        CantAssignRoleException::class => new UiExceptionDefinition(
            'grifix.acl.cannotAssignRole',
            'grifix.acl.cannotAssignRole'
        ),

        EmailIsNotConfirmedException::class => new UiExceptionDefinition(
            'grifix.acl.emailNotConfirmed',
            'grifix.acl.msg_emailNotConfirmed'
        ),

        UserNotExistsException::class => new UiExceptionDefinition(
            'grifix.acl.userNotExists',
            'grifix.acl.msg_userNotExists'
        ),

        WrongTokenException::class => new UiExceptionDefinition(
            'grifix.acl.wrongToken',
            'grifix.acl.msg_wrongToken'
        ),

        MoreThanOneUserFoundException::class => new UiExceptionDefinition(
            'grifix.acl.moreThanOneUserFound',
            'grifix.acl.msg_moreThanOneUserFound'
        ),

        UserNotFoundException::class => new UiExceptionDefinition(
            'grifix.acl.userNotFound',
            'grifix.acl.msg_userNotFound',
            404
        ),

        UserIsNotActiveException::class => new UiExceptionDefinition(
            'grifix.acl.userIsBlocked',
            'grifix.acl.userIsBlocked'
        )
    ];
}
