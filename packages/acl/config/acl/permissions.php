<?php

declare(strict_types = 1);

namespace {

    use Grifix\Acl\Domain\Role\RoleInterface;
    use Grifix\Acl\Ui\Common\AclModulePermissions as P;
    use Grifix\Shared\Ui\Common\PermissionDefinition;

    return [
        P::SIGN_IN => new PermissionDefinition(
            P::SIGN_IN,
            [RoleInterface::ROLE_GUEST]
        ),
        P::SIGN_UP => new PermissionDefinition(
            P::SIGN_UP,
            [RoleInterface::ROLE_GUEST]
        ),
        P::CHANGE_PASSWORD => new PermissionDefinition(
            P::CHANGE_PASSWORD,
            [RoleInterface::ROLE_USER]
        ),
        P::CONFIRM_EMAIL => new PermissionDefinition(
            P::CONFIRM_EMAIL,
            [RoleInterface::ROLE_GUEST]
        ),
        P::RESET_PERMISSIONS => new PermissionDefinition(
            P::RESET_PERMISSIONS,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::GET_SIGNED_IN_USER => new PermissionDefinition(
            P::GET_SIGNED_IN_USER,
            [RoleInterface::ROLE_USER, RoleInterface::ROLE_GUEST, RoleInterface::ROLE_ADMIN]
        ),
        P::ADMIN_USERS => new PermissionDefinition(
            P::ADMIN_USERS,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::ADMIN_ROLES => new PermissionDefinition(
            P::ADMIN_ROLES,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_USER_TIMEZONE => new PermissionDefinition(
            P::READ_USER_TIMEZONE,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_USER_TOKEN => new PermissionDefinition(
            P::READ_USER_TOKEN,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_USER_LAST_IP => new PermissionDefinition(
            P::READ_USER_LAST_IP,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_USER_BRUTEFORCE_COUNTER => new PermissionDefinition(
            P::READ_USER_BRUTEFORCE_COUNTER,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_USER_PERMISSIONS => new PermissionDefinition(
            P::READ_USER_PERMISSIONS,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_USER_EMAIL => new PermissionDefinition(
            P::READ_USER_EMAIL,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::ASSIGN_ROLE => new PermissionDefinition(
            P::ASSIGN_ROLE,
            [RoleInterface::ROLE_ADMIN]
        )
    ];
}
