<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Kit\Type\Range\Range;

    return [
        'length' => 16,
        'upperCase' => new Range(2, 4),
        'loverCase' => new Range(2, 8),
        'numbers' => new Range(2, 8),
        'symbols' => new Range(2, 8),
    ];
}
