<?php


namespace {

    use Grifix\Acl\Application\Service\UserRoleRelationsManagerInterface;
    use Grifix\Acl\Domain\User\UserInfrastructureInterface;
    use Grifix\Acl\Infrastructure\Application\Service\UserRoleRelationsManager;
    use Grifix\Acl\Infrastructure\Domain\User\UserInfrastructure;
    use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;

    return [
        UserInfrastructureInterface::class => new DependencyDefinition(UserInfrastructure::class)
    ];

//    return [
//        UserInfrastructureInterface::class => new Definition(UserInfrastructure::class),
//        UserEmailInfrastructureInterface::class => new Definition(UserEmailInfrastructure::class),
//        PasswordInfrastructureInterface::class => new Definition(PasswordInfrastructure::class),
//        RoleInfrastructureInterface::class => new Definition(RoleInfrastructure::class),
//        RelationsManagerInterface::class => new Definition(RelationsManager::class),
//        RoleFinderInterface::class => new Definition(RoleFinder::class),
//        UserFinderInterface::class => new Definition(UserFinder::class)
//    ];
}
