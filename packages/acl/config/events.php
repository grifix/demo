<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {

    use Grifix\Acl\Domain\User\Event as User;

    return [
        'grifix.acl.user.bruteforceDetected' => User\BruteForceDetectedEvent::class,
        'grifix.acl.user.email.confirmed' => User\EmailConfirmedEvent::class,
        'grifix.acl.user.password.reset' => User\PasswordWasResetEvent::class,
        'grifix.acl.user.role.assigned' => User\RoleAssignedEvent::class,
        'grifix.acl.user.role.debarred' => User\RoleDebarredEvent::class,
        'grifix.acl.user.created' => User\UserCreatedEvent::class
    ];
}

