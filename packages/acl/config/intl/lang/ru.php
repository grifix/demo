<?php
declare(strict_types=1);

namespace {
    
    return [
        '_module' => 'ACL',
        'userNotExists' => 'Такого пользователя не существует!',
        'userIsBlockedUntil' => 'Пользователь заблокирован до {date}!',
        'userIsBlocked' => 'Пользоватль заблокирован!',
        'signIn' => 'Войти',
        'signOut' => 'Выйти',
        'user' => [
            'cases' => [
                'nom' => ['пользователь', 'пользователи'],
            ],
        ],
        'role' => [
            'cases' => [
                'nom' => ['роль', 'роли'],
            ],
        ],
        'msg_emailNotConfirmed' => 'Email не подтвержден!',
        'msg_userEmailAlreadyExists' => 'Пользователь с таким email уже есть!',
        'msg_userNotExists' => 'Такого пользователья не существует!',
        'msg_emailAlreadyConfirmed' => 'Email уже потвержден!',
        'msg_wrongToken' => 'Неправильный токен!',
        'msg_emailConfirmationSuccess' => 'Потверждение email завершено успешно, ваша учетная запись активирована!',
        'msg_emailConfirmation' => 'Подтверждение email',
        'msg_moreThanOneUserFound' => 'Найдено больше одного пользователя!',
        'msg_userNotFound' => 'Пользователь не найден!'
    ];
}
