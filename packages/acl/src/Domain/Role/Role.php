<?php
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */

/** @noinspection PhpFullyQualifiedNameUsageInspection */
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

use Grifix\Kit\Collection\Collection;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class Role implements RoleInterface
{
    protected UuidInterface $id;
    protected string $name;
    protected CollectionInterface $permissions;

    public function __construct(
        #[Dependency]
        protected RoleOutsideInterface $outside,
        UuidInterface $id,
        string $name
    ) {
        $this->id = $id;
        $this->outside = $outside;
        $this->name = $name;
        $this->permissions = new Collection();
    }

    public function grantPermission(string $permission): void
    {
        $this->permissions->add($permission);
    }

    public function denyPermission(string $permission): void
    {
        $this->permissions->remove($permission);
    }

    public function resetPermissions(): void
    {
        $this->permissions->clear();
        foreach ($this->outside->getDefaultPermissions($this->name) as $permission) {
            $this->grantPermission($permission);
        }
    }
}
