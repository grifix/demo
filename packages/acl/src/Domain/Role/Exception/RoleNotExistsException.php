<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\Role\Exception;

/**
 * Class RoleNotFoundException
 * @package Grifix\Acl\Infrastructure\Role
 */
class RoleNotExistsException extends \Exception
{
    protected string $propertyName;

    /** @var mixed */
    protected $propertyValue;

    public function __construct(string $propertyName, $propertyValue)
    {
        $this->propertyName = $propertyName;
        $this->propertyValue = $propertyValue;

        parent::__construct(sprintf('Role witch "%s" equals "%s" not exists!', $propertyName, $propertyValue));
    }
}
