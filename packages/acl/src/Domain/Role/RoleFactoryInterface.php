<?php

declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

interface RoleFactoryInterface
{
    public function createRole(string $id, string $name): RoleInterface;
}
