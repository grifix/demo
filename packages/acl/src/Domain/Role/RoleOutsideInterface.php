<?php

declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

interface RoleOutsideInterface
{
    public function getDefaultPermissions(string $roleName):array;
}
