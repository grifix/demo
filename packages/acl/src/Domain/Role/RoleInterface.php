<?php

declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

interface RoleInterface
{
    public const ID_GUEST = '1a6604fb-79b1-42ed-86cf-71193f7e1181';
    public const ID_USER = 'dc0aa3d2-6f8e-4d84-90d0-d83631c1c037';
    public const ID_ADMIN = '1a16d831-abb5-4e02-b905-159fd8e9c49e';
    public const ROLE_GUEST = 'guest';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_USER = 'user';

    public function grantPermission(string $permission): void;

    public function denyPermission(string $permission): void;

    public function resetPermissions(): void;
}
