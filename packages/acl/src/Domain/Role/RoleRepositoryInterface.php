<?php

declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

use Grifix\Kit\Collection\GenericCollectionInterface;

interface RoleRepositoryInterface extends GenericCollectionInterface
{

    public function get(string $id): RoleInterface;

    public function add(RoleInterface $role): void;

    public function delete(RoleInterface $role): void;

    public function exists(string $id): bool;
}
