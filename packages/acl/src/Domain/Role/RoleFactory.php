<?php

declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;

class RoleFactory extends AbstractFactory implements RoleFactoryInterface
{
    protected RoleOutsideInterface $roleInfrastructure;

    protected UuidFactoryInterface $uuidFactory;

    public function __construct(
        ClassMakerInterface $classMaker,
        RoleOutsideInterface $outside,
        UuidFactoryInterface $uuidFactory
    ) {
        $this->roleInfrastructure = $outside;
        $this->uuidFactory = $uuidFactory;
        parent::__construct($classMaker);
    }

    public function createRole(string $id, string $name): RoleInterface
    {
        $class = $this->makeClassName(Role::class);

        /** @var RoleInterface $role */
        $role = new $class($this->roleInfrastructure, $this->uuidFactory->createUuid($id), $name);

        return $role;
    }
}
