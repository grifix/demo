<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Kit\Collection\GenericCollectionInterface;

interface UserRepositoryInterface extends GenericCollectionInterface
{
    /**
     * @throws UserNotExistsException
     */
    public function get(string $id): UserInterface;

    public function add(UserInterface $user): void;

    public function delete(UserInterface $user): void;
}
