<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User;


use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;

interface UserOutsideInterface extends UserInfrastructureInterface
{
    public function createDate(int $timestamp = null): DateTimeInterface;

    public function createIpAddress(string $ipAddress): IpAddressInterface;
}
