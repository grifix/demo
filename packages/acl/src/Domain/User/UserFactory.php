<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\User\Email\UserEmailFactoryInterface;
use Grifix\Acl\Domain\User\Password\PasswordFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;

class UserFactory extends AbstractFactory implements UserFactoryInterface
{

    public function __construct(
        ClassMakerInterface $classMaker,
        protected PasswordFactoryInterface $passwordFactory,
        protected UserEmailFactoryInterface $userEmailFactory,
        protected UserOutsideInterface $outside,
        protected UuidFactoryInterface $uuidFactory
    ) {
        parent::__construct($classMaker);
    }

    public function create(
        string $id,
        string $email,
        string $langCode,
        string $password = null,
        bool $emailConfirmed = false
    ): UserInterface {
        $class = $this->makeClassName(User::class);
        return new $class(
            $this->outside,
            $this->uuidFactory->createUuid($id),
            $this->userEmailFactory->create($email, $emailConfirmed),
            $this->passwordFactory->create($password),
            $langCode,
            null
        );
    }
}
