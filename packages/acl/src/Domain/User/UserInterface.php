<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyConfirmedException;
use Grifix\Acl\Domain\User\Exception\CantAssignRoleException;
use Grifix\Acl\Domain\User\Exception\WeakPasswordException;
use Grifix\Acl\Domain\User\Exception\WrongTokenException;
use Grifix\Acl\Domain\User\Password\Exception\PasswordsMissMathException;

interface UserInterface
{
    const GUEST_ID = '220a4974-95fc-4fc0-b666-b812d6743960';
    const ADMIN_ID = '2f3ec765-5be5-47c2-8f9d-b271ade066f4';
    const REASON_BRUTEFORCE_ATTACK = 'brute_force_attack';

    public function activate();

    /**
     * @throws UserEmailAlreadyConfirmedException
     * @throws WrongTokenException
     */
    public function confirmEmail(string $token): void;

    /**
     * @throws PasswordsMissMathException
     */
    public function changePassword(string $oldPassword, string $newPassword, string $newPasswordAgain): void;

    public function signIn(string $password, string $ip, string $sessionId, int $currentTime): void;

    public function signOut(): void;

    public function resetPassword(): void;

    public function blockFor(int $seconds, int $currentTime, string $reason = null): void;

    public function blockUntil(int $timestamp, string $reason = null): void;

    public function block(string $reason = null): void;

    public function unBlock(): void;

    /**
     * @throws CantAssignRoleException
     */
    public function assignRole(string $roleId): void;

    public function debarRole(string $roleId): void;

    /**
     * @throws WeakPasswordException
     */
    public function setPassword(string $password): void;
}
