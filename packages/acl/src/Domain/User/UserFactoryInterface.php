<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

interface UserFactoryInterface
{
    public function create(
        string $id,
        string $email,
        string $langCode,
        string $password = null,
        bool $emailConfirmed = false
    ): UserInterface;
}
