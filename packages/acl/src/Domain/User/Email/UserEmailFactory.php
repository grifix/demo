<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class UserEmailFactory extends AbstractFactory implements UserEmailFactoryInterface
{

    public function __construct(
        ClassMakerInterface $classMaker,
        protected UserEmailOutsideInterface $outside)
    {
        parent::__construct($classMaker);
    }

    public function create(string $email, bool $confirmed = false): UserEmailInterface
    {
        $class = $this->makeClassName(UserEmail::class);
        return new $class($this->outside, $email, $confirmed);
    }
}
