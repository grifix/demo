<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;

use Grifix\Shared\Domain\Email\EmailInterface;

interface UserEmailInterface extends EmailInterface
{
    public function confirm(): UserEmailInterface;

    public function isConfirmed(): bool;
}
