<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email\Exception;

use DomainException;

class UserEmailAlreadyExistsException extends DomainException
{
    protected string $email;

    public function __construct(string $email)
    {
        parent::__construct(sprintf('User with email %s already exists!', $email));
        $this->email = $email;
    }
}
