<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;

use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyConfirmedException;
use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyExistsException;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\Email\Email;

class UserEmail extends Email implements UserEmailInterface
{
    public function __construct(
        #[Dependency]
        private UserEmailOutsideInterface $outside,
        string $value,
        private bool $confirmed = false
    ) {
        $this->outside = $outside;
        if ($this->outside->emailExists($value)) {
            throw new UserEmailAlreadyExistsException($value);
        }
        parent::__construct($value);
        $this->confirmed = $confirmed;
    }

    public function confirm(): UserEmailInterface
    {
        if ($this->confirmed) {
            throw new UserEmailAlreadyConfirmedException(strval($this->value));
        }
        $result = clone $this;
        $result->confirmed = true;
        return $result;
    }

    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
