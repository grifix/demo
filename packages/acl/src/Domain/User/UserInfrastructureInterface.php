<?php declare(strict_types = 1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Domain\User;

use Grifix\Shared\Domain\EventPublisherInterface;
use Grifix\Kit\Type\DateTime\DateTimeInterface;

interface UserInfrastructureInterface extends EventPublisherInterface
{
    public function getDefaultTimezone(): string;

    public function getMaxSignInAttempts(): int;

    public function getBruteForceBlockInterval(): int;

    public function generatePassword(): string;

    public function roleExists(string $roleId): bool;

    public function createDateTime(int $timeStamp): DateTimeInterface;
}
