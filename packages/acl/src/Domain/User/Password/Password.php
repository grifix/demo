<?php


namespace Grifix\Acl\Domain\User\Password;

use Grifix\Acl\Domain\User\Password\Exception\InvalidPasswordException;
use Grifix\Acl\Domain\User\Password\Exception\PasswordsMissMathException;
use Grifix\Acl\Domain\User\Password\Exception\WrongPasswordException;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;

class Password implements PasswordInterface
{
    protected string $value;

    public function __construct(
        #[Dependency]
        protected PasswordOutsideInterface $outside,
        string $value = null
    )
    {
        $this->outside = $outside;
        if (!$value) {
            $value = $this->outside->generatePassword();
        }
        $this->validatePassword($value);
        $this->value = $this->outside->hashPassword($value);
    }

    public function verify(string $password): void
    {
        if (!$this->outside->verifyPassword($password, $this->value)) {
            throw new WrongPasswordException();
        }
    }

    public function change(string $oldValue, string $newValue, string $newValueAgain): PasswordInterface
    {
        $this->outside->verifyPassword($oldValue, $this->value);

        if ($newValue !== $newValueAgain) {
            throw new PasswordsMissMathException();
        }
        return new static($this->outside, $newValue);
    }

    public function set(string $value): PasswordInterface
    {
        return new static($this->outside, $value);
    }

    public function __toString(): string
    {
        return $this->value;
    }

    private function validatePassword(string $password)
    {
        if (false === $this->outside->validatePassword($password)) {
            throw new InvalidPasswordException();
        }
    }
}
