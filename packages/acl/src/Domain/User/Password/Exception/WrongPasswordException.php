<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Domain\User\Password\Exception;

class WrongPasswordException extends \DomainException
{
    /** @var mixed */
    protected $message = 'Wrong password!';
}
