<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Password;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class PasswordFactory extends AbstractFactory implements PasswordFactoryInterface
{
    public function __construct(
        ClassMakerInterface $classMaker,
        protected PasswordOutsideInterface $outside
    ) {
        parent::__construct($classMaker);
    }

    public function create(string $password = null): PasswordInterface
    {
        /**@var $class Password */
        $class = $this->makeClassName(Password::class);
        return new $class($this->outside, $password);
    }
}
