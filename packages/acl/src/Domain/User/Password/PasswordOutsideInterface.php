<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Password;

interface PasswordOutsideInterface
{
    public function validatePassword(string $password): bool;

    public function verifyPassword(string $password, string $hash): bool;

    public function hashPassword(string $password): string;

    public function generatePassword(): string;
}
