<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

class RoleDebarredEvent
{
    protected string $userId;

    protected string $roleId;

    public function __construct(string $userId, string $roleId)
    {
        $this->roleId = $roleId;
        $this->userId = $userId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getRoleId(): string
    {
        return $this->roleId;
    }
}
