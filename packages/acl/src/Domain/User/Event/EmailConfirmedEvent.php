<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

class EmailConfirmedEvent
{
    protected string $userId;

    protected string $email;

    protected string $token;

    protected string $password;

    public function __construct(string $userId, string $email, string $token, string $password)
    {
        $this->userId = $userId;
        $this->email = $email;
        $this->token = $token;
        $this->password = $password;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
