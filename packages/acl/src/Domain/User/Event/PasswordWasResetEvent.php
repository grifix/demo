<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

class PasswordWasResetEvent
{
    protected string $newPassword;

    protected string $userId;

    protected string $userEmail;

    public function __construct(string $newPassword, string $userId, string $userEmail)
    {
        $this->newPassword = $newPassword;
        $this->userId = $userId;
        $this->userEmail = $userEmail;
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getUserEmail(): string
    {
        return $this->userEmail;
    }
}
