<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

class UserCreatedEvent
{
    protected string $userId;

    protected string $token;

    protected string $email;

    protected bool $emailConfirmed;

    protected string $langCode;

    public function __construct(
        string $userId,
        string $token,
        string $email,
        bool $emailConfirmed,
        string $langCode
    ) {
        $this->userId = $userId;
        $this->token = $token;
        $this->email = $email;
        $this->emailConfirmed = $emailConfirmed;
        $this->langCode = $langCode;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLangCode(): string
    {
        return $this->langCode;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }
}
