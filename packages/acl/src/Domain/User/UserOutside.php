<?php declare(strict_types = 1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Domain\User;

use Grifix\Shared\Domain\DateTime\DateTimeFactoryInterface;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressFactoryInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;

class UserOutside implements UserOutsideInterface
{
    public function __construct(
        protected DateTimeFactoryInterface $dateFactory,
        protected IpAddressFactoryInterface $ipAddressFactory,
        protected UserInfrastructureInterface $infrastructure
    )
    {
    }

    public function createDate(int $timestamp = null): DateTimeInterface
    {
        return $this->dateFactory->createDateTime($timestamp);
    }

    public function createIpAddress(string $ipAddress): IpAddressInterface
    {
        return $this->ipAddressFactory->createIpAddress($ipAddress);
    }

    public function publishEvent(object $event, object $aggregate): void
    {
        $this->infrastructure->publishEvent($event, $aggregate);
    }

    public function getDefaultTimezone(): string
    {
        return $this->infrastructure->getDefaultTimezone();
    }

    public function getMaxSignInAttempts(): int
    {
        return $this->infrastructure->getMaxSignInAttempts();
    }

    public function getBruteForceBlockInterval(): int
    {
        return $this->infrastructure->getBruteForceBlockInterval();
    }

    public function generatePassword(): string
    {
        return $this->infrastructure->generatePassword();
    }

    public function roleExists(string $roleId): bool
    {
        return $this->infrastructure->roleExists($roleId);
    }

    public function createDateTime(int $timeStamp): \Grifix\Kit\Type\DateTime\DateTimeInterface
    {
        return $this->infrastructure->createDateTime($timeStamp);
    }
}
