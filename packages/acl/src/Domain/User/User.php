<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\User\Email\UserEmailInterface;
use Grifix\Acl\Domain\User\Event\BruteForceDetectedEvent;
use Grifix\Acl\Domain\User\Event\EmailConfirmedEvent;
use Grifix\Acl\Domain\User\Event\PasswordWasResetEvent;
use Grifix\Acl\Domain\User\Event\RoleAssignedEvent;
use Grifix\Acl\Domain\User\Event\RoleDebarredEvent;
use Grifix\Acl\Domain\User\Event\UserCreatedEvent;
use Grifix\Acl\Domain\User\Exception\CantAssignRoleException;
use Grifix\Acl\Domain\User\Exception\EmailIsNotConfirmedException;
use Grifix\Acl\Domain\User\Exception\UserAlreadyActiveException;
use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
use Grifix\Acl\Domain\User\Exception\WrongTokenException;
use Grifix\Acl\Domain\User\Password\Exception\WrongPasswordException;
use Grifix\Acl\Domain\User\Password\PasswordInterface;
use Grifix\Kit\Collection\Collection;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class User implements UserInterface
{
    protected UuidInterface $id;

    protected UserEmailInterface $email;

    protected PasswordInterface $password;

    protected string $langCode;

    protected DateTimeInterface $registrationDate;

    protected ?DateTimeInterface $activationDate = null;

    protected ?DateTimeInterface $lastSignInDate = null;

    protected bool $isActive = false;

    protected int $bruteForceCounter;

    protected ?IpAddressInterface $lastIp = null;

    protected string $timeZone;

    protected ?string $blockingReason = null;

    protected ?string $token = null;

    protected ?string $sessionId = null;

    /** @var CollectionInterface|string[] */
    protected CollectionInterface $roles;

    public function __construct(
        #[Dependency]
        protected UserOutsideInterface $outside,
        UuidInterface $id,
        UserEmailInterface $email,
        PasswordInterface $password,
        string $langCode,
        string $timeZone = null
    ) {
        $this->outside = $outside;
        $this->id = $id;
        $this->email = $email;
        $this->roles = new Collection();
        $this->password = $password;
        $this->langCode = $langCode;
        $this->token = uniqid();
        $this->registrationDate = $this->outside->createDate();
        $this->isActive = true;
        $this->bruteForceCounter = 0;
        $this->timeZone = $timeZone ?? $this->outside->getDefaultTimezone();;
        $this->assignRole(RoleInterface::ID_USER);
        $this->outside->publishEvent(
            new UserCreatedEvent(
                (string)$this->id,
                $this->token,
                (string)$this->email,
                $this->email->isConfirmed(),
                $this->langCode
            ),
            $this
        );
    }

    /**
     * @throws UserAlreadyActiveException
     */
    public function activate(): void
    {
        if ($this->isActive) {
            throw new UserAlreadyActiveException((string)$this->id);
        }
        $this->isActive = true;
        $this->activationDate = null;
    }

    public function setPassword(string $password): void
    {
        $this->password = $this->password->set($password);
    }

    public function changePassword(string $oldPassword, string $newPassword, string $newPasswordAgain): void
    {
        $this->password = $this->password->change($oldPassword, $newPassword, $newPasswordAgain);
    }

    public function confirmEmail(string $token): void
    {
        if ($token !== $this->token) {
            throw new WrongTokenException((string)$this->id, $token);
        }
        $this->email = $this->email->confirm();
        $password = $this->outside->generatePassword();
        $this->setPassword($password);
        $this->outside->publishEvent(
            new EmailConfirmedEvent(
                (string)$this->id,
                strval($this->email),
                $token,
                $password
            ),
            $this
        );
    }

    /**
     * @throws EmailIsNotConfirmedException
     */
    protected function assertEmailIsConfirmed(): void
    {
        if (!$this->email->isConfirmed()) {
            throw new EmailIsNotConfirmedException((string)$this->id);
        }
    }

    /**
     * @throws UserIsNotActiveException
     */
    protected function assertIsActive(): void
    {
        if (!$this->isActive) {
            throw new UserIsNotActiveException((string)$this->id, $this->activationDate->toPhpDateTime());
        }
    }

    protected function isSignedIn(): bool
    {
        return $this->sessionId != null;
    }

    public function signOut(): void
    {
        $this->sessionId = null;
    }

    public function signIn(string $password, string $ip, string $sessionId, int $currentTime): void
    {
        $this->assertEmailIsConfirmed();

        try {
            $this->password->verify($password);
        } catch (WrongPasswordException $e) {
            $this->bruteForceCounter++;
            if ($this->signInLimitExceed()) {
                $this->blockFor(
                    $this->outside->getBruteForceBlockInterval(),
                    $currentTime,
                    self::REASON_BRUTEFORCE_ATTACK
                );
                $this->bruteForceCounter = 0;

                $this->outside->publishEvent(new BruteForceDetectedEvent((string)$this->id), $this);

                throw new UserIsNotActiveException(
                    (string)$this->id,
                    $this->activationDate ? $this->activationDate->toPhpDateTime() : null,
                    $this->blockingReason
                );
            }
            throw $e;
        }

        if (!$this->isActive && $this->canBeActive($currentTime)) {
            $this->activate();
        }

        $this->assertIsActive();

        if ($this->isSignedIn()) {
            $this->signOut();
        }

        $this->bruteForceCounter = 0;
        $this->lastSignInDate = $this->outside->createDate();
        $this->lastIp = $this->outside->createIpAddress($ip);
        $this->sessionId = $sessionId;
    }

    protected function signInLimitExceed(): bool
    {
        return $this->bruteForceCounter > $this->outside->getMaxSignInAttempts();
    }

    public function blockFor(int $seconds, int $currentTime, string $reason = null): void
    {
        $this->isActive = false;
        $this->activationDate = $this->outside->createDate(
            $this->outside->createDateTime($currentTime)->addSeconds($seconds)->getTimestamp()
        );
        $this->blockingReason = $reason;
    }

    protected function canBeActive(int $currentTime): bool
    {
        return $this->activationDate && $this->activationDate->toPhpDateTime()->getTimestamp() <= $currentTime;
    }

    public function blockUntil(int $timestamp, string $reason = null): void
    {
        $this->isActive = false;
        $this->activationDate = $this->outside->createDate($timestamp);
        $this->blockingReason = $reason;
    }

    public function block(string $reason = null): void
    {
        $this->isActive = false;
        $this->activationDate = null;
        $this->blockingReason = $reason;
    }

    public function unBlock(): void
    {
        $this->isActive = true;
        $this->activationDate = null;
    }

    public function resetPassword(): void
    {
        $newPassword = $this->outside->generatePassword();
        $this->password = $this->password->set($newPassword);
        $this->outside->publishEvent(
            new PasswordWasResetEvent(
                $newPassword,
                (string)$this->id,
                (string)$this->email
            ),
            $this
        );
    }

    public function assignRole(string $roleId): void
    {
        if (!$this->outside->roleExists($roleId)) {
            throw new CantAssignRoleException($roleId);
        }
        $this->roles->add($roleId);
        $this->outside->publishEvent(new RoleAssignedEvent((string)$this->id, $roleId), $this);
    }

    public function debarRole(string $roleId): void
    {
        $this->roles->remove($roleId);
        $this->outside->publishEvent(new RoleDebarredEvent((string)$this->id, $roleId), $this);
    }
}
