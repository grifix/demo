<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

use DomainException;

class CantAssignRoleException extends DomainException
{
    protected string $roleId;

    public function __construct(string $roleId)
    {
        parent::__construct(sprintf('Cannot assign role with id "%s", role not exists!', $roleId));
        $this->roleId = $roleId;
    }
}
