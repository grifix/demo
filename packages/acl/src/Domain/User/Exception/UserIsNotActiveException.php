<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

use DateTimeInterface;

class UserIsNotActiveException extends \DomainException
{
    protected string $userId;

    protected ?DateTimeInterface $activationDate;

    protected ?string $blockingReason;

    public function __construct(
        string $id,
        DateTimeInterface $activationDate = null,
        string $blockingReason = null
    ) {
        $this->userId = $id;
        $this->activationDate = $activationDate;
        $this->message = 'UserModel is not active!';
        $this->blockingReason = $blockingReason;
        parent::__construct();
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getActivationDate(): ?DateTimeInterface
    {
        return $this->activationDate;
    }

    public function getBlockingReason(): ?string
    {
        return $this->blockingReason;
    }
}
