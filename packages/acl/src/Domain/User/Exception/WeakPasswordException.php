<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Domain\User\Exception;

class WeakPasswordException extends \DomainException
{
    protected string $password;

    public function __construct(string $password)
    {
        $this->password = $password;
        $this->message = 'Weak password!';
        
        parent::__construct();
    }
}
