<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

use DomainException;

class UserNotExistsException extends DomainException
{
    protected string $propertyName;

    /** @var mixed */
    protected $propertyValue;

    public function __construct(string $propertyName, $propertyValue)
    {
        $this->propertyName = $propertyName;
        $this->propertyValue = $propertyValue;

        parent::__construct(sprintf('Users witch "%s" equals "%s" not exists!', $propertyName, $propertyValue));
    }
}
