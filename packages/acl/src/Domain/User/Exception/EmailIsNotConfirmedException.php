<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

use DomainException;

class EmailIsNotConfirmedException extends DomainException
{
    protected string $userId;

    public function __construct(string $email)
    {
        $this->userId = $email;
        $this->message = 'Email is not confirmed!';
        parent::__construct();
    }
}
