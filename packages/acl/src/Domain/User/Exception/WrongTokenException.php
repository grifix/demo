<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

use DomainException;

class WrongTokenException extends DomainException
{
    protected string $userId;

    protected string $token;

    public function __construct(string $email, string $token)
    {
        $this->userId = $email;
        $this->token = $token;
        $this->message = 'Wrong token!';

        parent::__construct();
    }
}
