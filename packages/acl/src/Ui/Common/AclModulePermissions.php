<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Common;

interface AclModulePermissions
{
    public const SIGN_IN = 'grifix.acl.singIn';
    public const SIGN_UP = 'grifix.acl.signUp';
    public const CHANGE_PASSWORD = 'grifix.acl.changePassword';
    public const CONFIRM_EMAIL = 'grifix.acl.confirmEmail';
    public const RESET_PERMISSIONS = 'grifix.acl.resetPermission';
    public const GET_SIGNED_IN_USER = 'grifix.acl.getSignedInUser';
    public const ADMIN_ROLES = 'grifix.acl.admin.roles';
    public const ADMIN_USERS = 'grifix.acl.admin.users';
    public const READ_USER_EMAIL = 'grifix.acl.readUserEmail';
    public const READ_USER_PERMISSIONS = 'grifix.acl.readUserPermissions';
    public const READ_USER_BRUTEFORCE_COUNTER = 'grifix.acl.readUserBruteForceCounter';
    public const READ_USER_LAST_IP = 'grifix.acl.readUserLastIp';
    public const READ_USER_TOKEN = 'grifix.acl.readUserToken';
    public const READ_USER_TIMEZONE = 'grifix.acl.readUserTimezone';
    public const ASSIGN_ROLE = 'grifix.acl.assignRole';
}
