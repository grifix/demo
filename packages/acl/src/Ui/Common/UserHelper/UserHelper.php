<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Common\UserHelper;

use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQuery;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQueryResult;
use Grifix\Acl\Ui\Common\AclModulePermissions;
use Grifix\Acl\Ui\Common\UserHelper\Exception\MoreThanOneUserFoundException;
use Grifix\Acl\Ui\Common\UserHelper\Exception\UserNotFoundException;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Shared\Ui\Common\PropertyProtector\PropertyProtectorInterface;
use Grifix\Shared\Ui\Common\PropertyProtector\ProtectionDto;

class UserHelper implements UserHelperInterface
{
    protected PropertyProtectorInterface $propertyProtector;

    protected QueryBusInterface $queryBus;

    public function __construct(
        PropertyProtectorInterface $propertyProtector,
        QueryBusInterface $queryBus
    )
    {
        $this->propertyProtector = $propertyProtector;
        $this->queryBus = $queryBus;
    }

    public function protectUser(UserDto $userDto)
    {
        $this->propertyProtector->protectProperties($userDto, [
            new ProtectionDto('email', AclModulePermissions::READ_USER_EMAIL),
            new ProtectionDto('permissions', AclModulePermissions::READ_USER_PERMISSIONS),
            new ProtectionDto('bruteForceCounter', AclModulePermissions::READ_USER_BRUTEFORCE_COUNTER),
            new ProtectionDto('lastIp', AclModulePermissions::READ_USER_LAST_IP),
            new ProtectionDto('token', AclModulePermissions::READ_USER_TOKEN),
            new ProtectionDto('timeZone', AclModulePermissions::READ_USER_TIMEZONE)
        ]);
    }

    public function getUser(UserFilter $userFilter): UserDto
    {
        /** @var FindUsersQueryResult $result */
        $result = $this->queryBus->execute(new FindUsersQuery($userFilter));
        if (count($result->getUsers()) > 1) {
            throw new MoreThanOneUserFoundException();
        }
        if (count($result->getUsers()) < 1) {
            throw new UserNotFoundException();
        }
        return $result->getUsers()[0];
    }

    public function getUserByEmail(string $email): UserDto
    {
        return $this->getUser(UserFilter::create()->setEmail($email));
    }
}
