<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Common\UserHelper;

use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;

interface UserHelperInterface
{
    public function protectUser(UserDto $userDto);

    public function getUser(UserFilter $userFilter): UserDto;

    public function getUserByEmail(string $email): UserDto;
}
