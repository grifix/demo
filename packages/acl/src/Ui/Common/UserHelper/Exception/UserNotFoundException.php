<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Ui\Common\UserHelper\Exception;

class UserNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('User not found!');
    }
}
