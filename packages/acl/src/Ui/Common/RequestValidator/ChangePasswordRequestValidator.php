<?php declare(strict_types=1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Ui\Common\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class ChangePasswordRequestValidator extends AbstractRequestValidator
{
    public const OLD_PASSWORD = 'old_password';
    public const NEW_PASSWORD = 'new_password';
    public const NEW_PASSWORD_AGAIN = 'new_password_again';

    protected function init(): void
    {
        parent::init();
        $this->addField(
            $this->buildFiled(
                self::OLD_PASSWORD,
                'grifix.acl.oldPassword'
            )
                ->required()
        )->addField(
            $this->buildFiled(
                self::NEW_PASSWORD,
                'grifix.acl.newPassword'
            )
                ->required()
        )->addField(
            $this->buildFiled(
                self::NEW_PASSWORD_AGAIN,
                'grifix.acl.newPasswordAgain'
            )
                ->required()
        );
    }
}
