<?php declare(strict_types = 1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Ui\Common\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class SignUpRequestValidator extends AbstractRequestValidator
{
    public const EMAIL = 'email';

    protected function init(): void
    {
        parent::init();
        $this->addField(
            $this->buildFiled(
                self::EMAIL,
                'grifix.kit.email'
            )
                ->email()
                ->required()
        );
    }
}
