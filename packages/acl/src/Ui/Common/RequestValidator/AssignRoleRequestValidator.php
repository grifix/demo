<?php declare(strict_types = 1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Ui\Common\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class AssignRoleRequestValidator extends AbstractRequestValidator
{
    public const ROLE_ID = 'role_id';
    public const USER_ID = 'user_id';

    protected function init(): void
    {
        parent::init();
        $this->addField(
            $this->buildFiled(
                self::USER_ID,
                'grifix.acl.user'
            )
                ->required()
                ->text()
        )->addField(
            $this->buildFiled(
                self::ROLE_ID,
                'grifix.acl.role'
            )
                ->required()
                ->text()
        );
    }
}
