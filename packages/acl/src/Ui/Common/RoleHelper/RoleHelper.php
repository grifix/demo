<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Ui\Common\RoleHelper;

use Grifix\Acl\Application\Projection\RoleProjection\RoleDto;
use Grifix\Acl\Application\Projection\RoleProjection\RoleFilter;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQuery;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQueryResult;
use Grifix\Acl\Ui\Common\RoleHelper\Exception\MoreThanOneRoleFoundException;
use Grifix\Acl\Ui\Common\RoleHelper\Exception\RoleNotFoundException;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;

class RoleHelper implements RoleHelperInterface
{
    protected QueryBusInterface $queryBus;

    public function findRole(RoleFilter $userFilter): RoleDto
    {
        /** @var FindRolesQueryResult $result */
        $result = $this->queryBus->execute(new FindRolesQuery($userFilter));
        if (count($result->getRoles()) > 1) {
            throw new MoreThanOneRoleFoundException();
        }
        if (count($result->getRoles()) < 1) {
            throw new RoleNotFoundException();
        }
        return $result->getRoles()[0];
    }
}
