<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Cli\Command;

use Grifix\Acl\Application\Command\Role\ResetPermissions\ResetPermissionsCommand;
use Grifix\Acl\Application\Projection\RoleProjection\RoleFilter;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQuery;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQueryResult;
use Grifix\Acl\Ui\Common\RoleHelper\RoleHelperInterface;
use Grifix\Kit\Cli\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetPermissionsCmd extends AbstractCommand
{
    protected const OPTION_ROLE = 'role';

    public const NAME = 'acl:reset_permissions';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('reset permissions for groups.')
            ->addOption(self::OPTION_ROLE, 'r', null, 'Role name');
    }


    /** @noinspection PhpMissingParentCallCommonInspection */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $roleName = $input->getOption(self::OPTION_ROLE);
        if ($roleName) {
            $role = $this->getShared(RoleHelperInterface::class)->findRole(RoleFilter::create()->setName($roleName));
            $this->executeCommand(new ResetPermissionsCommand($role->getId()));
        } else {
            /**@var $roles FindRolesQueryResult */
            $roles = $this->executeQuery(new FindRolesQuery());
            foreach ($roles->getRoles() as $role) {
                $this->executeCommand(new ResetPermissionsCommand($role->getId()));
            }
        }

        return 0;
    }
}
