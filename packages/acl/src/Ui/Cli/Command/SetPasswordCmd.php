<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Cli\Command;


use Grifix\Acl\Application\Command\User\SetPassword\SetPasswordCommand;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQuery;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQueryResult;
use Grifix\Acl\Domain\User\Password\Exception\InvalidPasswordException;
use Grifix\Kit\Cli\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetPasswordCmd extends AbstractCommand
{
    public const NAME = 'acl:set_password';

    public const ARG_EMAIL = 'email';

    public const ARG_PASSWORD = 'password';

    public const CODE_USER_NOT_EXIST = 1;

    public const CODE_INVALID_PASSWORD = 2;

    protected function configure()
    {
        $this->setName(self::NAME)
            ->setDescription('sets user password')
            ->addArgument(self::ARG_EMAIL, InputArgument::REQUIRED, 'user email')
            ->addArgument(self::ARG_PASSWORD, InputArgument::REQUIRED, 'new password');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument(self::ARG_EMAIL);
        $password = $input->getArgument(self::ARG_PASSWORD);
        $userId = $this->getUserId($email);
        if (!$userId) {
            $output->writeln(sprintf('<error>User with email "%s" does not exist!</error>', $email));
            return self::CODE_USER_NOT_EXIST;
        }
        try{
            $this->executeCommand(new SetPasswordCommand(
                $userId,
                $password
            ));
        }
        catch (InvalidPasswordException $exception){
            $output->writeln('<error>Invalid password!</error>');
            return self::CODE_INVALID_PASSWORD;
        }

        return 0;
    }

    protected function getUserId(string $email): ?string
    {
        /** @var FindUsersQueryResult $result */
        $result = $this->executeQuery(new FindUsersQuery(UserFilter::create()->setEmail($email)));
        if (count($result->getUsers()) === 0) {
            return null;
        }
        return $result->getUsers()[0]->getId();
    }
}
