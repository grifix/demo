<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\User\ConfirmEmail\ConfirmEmailCommand;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Ui\Common\UserHelper\UserHelperInterface;
use Grifix\Acl\Ui\Common\RequestValidator\ConfirmEmailRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class ConfirmEmailActionHandler extends AbstractActionHandler
{

    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $this->createRequestValidator(ConfirmEmailRequestValidator::class)->validate($params->getArray());
        $token = $params->get(ConfirmEmailRequestValidator::TOKEN);
        $user = $this->getShared(UserHelperInterface::class)->getUser(UserFilter::create()->setToken($token));
        $this->executeCommand(
            new ConfirmEmailCommand(
                $user->getId(),
                $token
            )
        );
        return [];
    }
}
