<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\User\SingUp\SignUpCommand;
use Grifix\Acl\Application\Query\GetFreeUserId\GetFreeUserIdQuery;
use Grifix\Acl\Ui\Common\RequestValidator\SignUpRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class SignUpActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $this->createRequestValidator(SignUpRequestValidator::class)->setStrictStrategy()->validate($params);
        $id = $this->executeQuery(new GetFreeUserIdQuery());
        $this->executeCommand(
            new SignUpCommand(
                $id,
                $params[SignUpRequestValidator::EMAIL],
                $this->getCurrentLangCode()
            )
        );

        return [
            'id' => $id,
        ];
    }
}
