<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\User\AssignRole\AssignRoleCommand;
use Grifix\Acl\Ui\Common\RequestValidator\AssignRoleRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class AssignRoleActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $this->createRequestValidator(AssignRoleRequestValidator::class)
            ->setStrictStrategy()
            ->validate($params);

        $this->executeCommand(
            new AssignRoleCommand(
                $params[AssignRoleRequestValidator::USER_ID],
                $params[AssignRoleRequestValidator::ROLE_ID]
            )
        );

        return $params;
    }
}
