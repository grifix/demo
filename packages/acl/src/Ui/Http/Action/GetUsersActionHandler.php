<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQuery;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQueryResult;
use Grifix\Acl\Ui\Common\UserHelper\UserHelperInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\OffsetCalculatorInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class GetUsersActionHandler extends AbstractActionHandler
{
    public const PARAM_PAGE = 'page';
    public const PARAM_ROWS = 'rows';

    protected ArrayHelperInterface $arrayHelper;

    protected OffsetCalculatorInterface $offsetCalculator;

    protected UserHelperInterface $userProtector;

    public function hasSideEffects(): bool
    {
        return false;
    }

    protected function init(): void
    {
        $this->arrayHelper = $this->getShared(ArrayHelperInterface::class);
        $this->offsetCalculator = $this->getShared(OffsetCalculatorInterface::class);
        $this->userProtector = $this->getShared(UserHelperInterface::class);
        parent::init();
    }

    public function __invoke(array $params = []): array
    {
        $page = $this->arrayHelper->get($params, static::PARAM_PAGE);
        $rows = $this->arrayHelper->get($params, static::PARAM_ROWS);

        $page === null ?: $page = (int)$page;
        $rows === null ?: $rows = (int)$rows;

        $offset = $this->offsetCalculator->calculateOffset($page, $rows);

        /**@var $result FindUsersQueryResult */
        $result = $this->executeQuery(new FindUsersQuery(
            new UserFilter(),
            $offset,
            $rows,
            true
        ));
        foreach ($result->getUsers() as $user) {
            $this->userProtector->protectUser($user);
        }
        return ['result' => $result];
    }
}
