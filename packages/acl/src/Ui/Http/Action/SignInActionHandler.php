<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\User\SingIn\SignInCommand;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Acl\Domain\User\Password\Exception\WrongPasswordException;
use Grifix\Acl\Ui\Common\UserHelper\UserHelperInterface;
use Grifix\Acl\Ui\Common\RequestValidator\SignInRequestValidator;
use Grifix\Kit\Exception\UiException;
use Grifix\Kit\Http\ServerInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class SignInActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $this->createRequestValidator(SignInRequestValidator::class)->setStrictStrategy()->validate($params);
        $user = $this->getShared(UserHelperInterface::class)->getUser(UserFilter::create()->setEmail($params[SignInRequestValidator::EMAIL]));
        try {
            $this->executeCommand(
                new SignInCommand(
                    $user->getId(),
                    $params[SignInRequestValidator::PASSWORD],
                    $this->getShared(ServerInterface::class)->getClientIp(),
                    $this->getShared(SessionInterface::class)->getId()
                )
            );
        } catch (WrongPasswordException | UserNotExistsException $e) {
            throw new UiException(
                'grifix.acl.userNotFound',
                $this->translate('grifix.acl.msg_userNotFound'),
                404
            );
        }

        return [SignInRequestValidator::PASSWORD => null];
    }
}
