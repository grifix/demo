<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class GetSignedInUserActionHandler extends AbstractActionHandler
{
    public function hasSideEffects(): bool
    {
        return false;
    }

    public function __invoke(array $params = []): array
    {
        $result = $this->executeQuery(new GetSignedInUserQuery($this->getShared(SessionInterface::class)->getId()));
        return $this->getShared(ArrayHelperInterface::class)->toArray($result);
    }
}
