<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\User\SingOut\SignOutCommand;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Ui\Common\UserHelper\UserHelperInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class SignOutActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $user = $this->getShared(UserHelperInterface::class)
            ->getUser(UserFilter::create()->setSessionId($this->getShared(SessionInterface::class)->getId()));

        $this->executeCommand(new SignOutCommand($user->getId()));

        return [];
    }
}
