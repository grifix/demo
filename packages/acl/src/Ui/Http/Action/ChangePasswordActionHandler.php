<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\User\ChangePassword\ChangePasswordCommand;
use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Acl\Ui\Common\RequestValidator\ChangePasswordRequestValidator;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class ChangePasswordActionHandler extends AbstractActionHandler
{

    public function __invoke(array $params = []): array
    {
        /**@var $user UserDto */
        $user = $this->executeQuery(new GetSignedInUserQuery($this->getShared(SessionInterface::class)->getId()));

        $this->createRequestValidator(ChangePasswordRequestValidator::class)->setStrictStrategy()->validate($params);
        $this->executeCommand(new ChangePasswordCommand(
            $user->getId(),
            $params[ChangePasswordRequestValidator::OLD_PASSWORD],
            $params[ChangePasswordRequestValidator::NEW_PASSWORD],
            $params[ChangePasswordRequestValidator::NEW_PASSWORD_AGAIN]
        ));

        return [];
    }

}
