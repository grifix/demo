<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action\Exception;

use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Kit\Ui\AbstractHttpException;

class UserIsBlockedException extends AbstractHttpException
{
    protected function createMessage(): string
    {
        if ($this->isPeriodBlockade()) {
            /**@var $previous UserIsNotActiveException */
            $previous = $this->getPrevious();

            return $this->translator->translate(
                'grifix.acl.userIsBlockedUntil',
                ['date' => $previous->getActivationDate()->format('c')]
            );
        } else {
            return $this->translator->translate('grifix.acl.userIsBlocked');
        }
    }

    protected function isPeriodBlockade(): bool
    {
        $previous = $this->getPrevious();

        return (
            $previous instanceof UserIsNotActiveException
            && $previous->getActivationDate()
            && $previous->getBlockingReason() != UserInterface::REASON_BRUTEFORCE_ATTACK
        );
    }
}
