<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Route;

use Grifix\Acl\Ui\Http\Action\ConfirmEmailActionHandler;
use Grifix\Acl\Ui\Common\RequestValidator\ConfirmEmailRequestValidator;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\Handler\AbstractRouteHandler;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\View\ViewFactoryInterface;

class ConfirmEmailRouteHandler extends AbstractRouteHandler
{
    public function __invoke(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $this->executeAction(
            ConfirmEmailActionHandler::getAlias(),
            [
                ConfirmEmailRequestValidator::TOKEN => $route->getParam('token')
            ]
        );

        return $response->withContent(
            $this->getShared(ViewFactoryInterface::class)
                ->create('grifix.kit.{skin}.tpl.message')
                ->render([
                    'text' => $this->translate('grifix.acl.msg_emailConfirmationSuccess')
                ])
        );
    }
}
