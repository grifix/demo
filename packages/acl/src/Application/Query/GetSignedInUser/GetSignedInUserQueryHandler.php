<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Application\Query\GetSignedInUser;

use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Projection\UserProjection\UserProjectionInterface;
use Grifix\Kit\Collection\CollectionInterface;

class GetSignedInUserQueryHandler
{
    protected UserProjectionInterface $userFinder;

    public function __construct(UserProjectionInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(GetSignedInUserQuery $query): UserDto
    {
        /**@var $result CollectionInterface*/
        $result = $this->userFinder->find((new UserFilter())->setSessionId($query->getSessionId()));
        
        if (!$result->first()) {
            $result = $this->userFinder->find((new UserFilter())->setIsGuest(true));
        }
        
        return $result->first();
    }
}
