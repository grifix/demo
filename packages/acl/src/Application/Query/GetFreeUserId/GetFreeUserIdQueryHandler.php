<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\GetFreeUserId;

use Grifix\Kit\Uuid\UuidGeneratorInterface;

class GetFreeUserIdQueryHandler
{
    protected UuidGeneratorInterface $uuidGenerator;

    public function __construct(UuidGeneratorInterface $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    public function __invoke(GetFreeUserIdQuery $query): string
    {
        return $this->uuidGenerator->generateUuid4();
    }
}
