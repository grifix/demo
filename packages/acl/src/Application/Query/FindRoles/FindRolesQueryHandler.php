<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindRoles;

use Grifix\Acl\Application\Projection\RoleProjection\RoleProjectionInterface;

class FindRolesQueryHandler
{
    protected RoleProjectionInterface $roleFinder;

    public function __construct(RoleProjectionInterface $roleFinder)
    {
        $this->roleFinder = $roleFinder;
    }

    public function __invoke(FindRolesQuery $query): FindRolesQueryResult
    {
        $roles = $this->roleFinder->find($query->getFilter(), $query->getOffset(), $query->getLimit());
        $total = null;
        if ($query->getCountTotal()) {
            $total = $this->roleFinder->count($query->getFilter());
        }
        return new FindRolesQueryResult($roles, $total);
    }
}
