<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Query\FindRoles;

use Grifix\Acl\Application\Projection\RoleProjection\RoleFilter;

class FindRolesQuery
{
    private RoleFilter $filter;

    protected bool $countTotal;

    protected ?int $offset;

    protected ?int $limit;

    public function __construct(
        ?RoleFilter $filter = null,
        ?int $offset = null,
        ?int $limit = null,
        bool $countTotal = false
    ) {
        $this->filter = $filter ?? new RoleFilter();
        $this->offset = $offset;
        $this->limit = $limit;
        $this->countTotal = $countTotal;
    }

    public function getFilter(): RoleFilter
    {
        return $this->filter;
    }

    public function getCountTotal(): bool
    {
        return $this->countTotal;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }
}
