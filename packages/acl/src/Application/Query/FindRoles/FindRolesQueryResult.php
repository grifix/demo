<?php declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindRoles;

use Grifix\Acl\Application\Projection\RoleProjection\RoleDto;
use Grifix\Kit\Collection\CollectionInterface;

class FindRolesQueryResult
{
    /** @var RoleDto[] | CollectionInterface */
    protected CollectionInterface $roles;

    protected ?int $total;

    public function __construct(CollectionInterface $roles, ?int $total)
    {
        $this->roles = $roles;
        $this->total = $total;
    }

    /**
     * @return RoleDto[]|CollectionInterface
     */
    public function getRoles(): CollectionInterface
    {
        return $this->roles;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }
}
