<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Query\GeneratePassword;

use Grifix\Acl\Infrastructure\Domain\User\Password\PasswordGeneratorInterface;

class GeneratePasswordQueryHandler
{
    protected PasswordGeneratorInterface $passwordGenerator;

    public function __construct(PasswordGeneratorInterface $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    public function __invoke(GeneratePasswordQuery $query): string
    {
        return $this->passwordGenerator->generatePassword();
    }
}
