<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Query\HasUserAccess;

class HasUserAccessQuery
{
    protected string $userId;

    protected string $permission;

    public function __construct(string $userId, string $permission)
    {
        $this->userId = $userId;
        $this->permission = $permission;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }
}
