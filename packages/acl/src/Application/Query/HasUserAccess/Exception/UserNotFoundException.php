<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\HasUserAccess\Exception;

/**
 * Class UserNotFoundException
 * @package Grifix\Acl\Application\Query
 */
class UserNotFoundException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('User not found!');
    }
}
