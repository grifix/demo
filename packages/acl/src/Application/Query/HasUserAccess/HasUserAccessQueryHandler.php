<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\HasUserAccess;

use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Projection\UserProjection\UserProjectionInterface;
use Grifix\Acl\Application\Query\HasUserAccess\Exception\UserNotFoundException;

class HasUserAccessQueryHandler
{
    protected UserProjectionInterface $userFinder;

    public function __construct(UserProjectionInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(HasUserAccessQuery $query): bool
    {
        $filter = (new UserFilter())->setId($query->getUserId())->withPermissions();

        /**@var $user UserDto */
        $user = $this->userFinder->find($filter)->first();
        if (!$user) {
            throw new UserNotFoundException();
        }
        return in_array($query->getPermission(), $user->getPermissions());
    }
}
