<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindUsers;

use Grifix\Acl\Application\Projection\UserProjection\UserProjectionInterface;

class FindUsersQueryHandler
{
    protected UserProjectionInterface $userFinder;

    public function __construct(UserProjectionInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(FindUsersQuery $query): FindUsersQueryResult
    {
        $users = $this->userFinder->find($query->getFilter(), $query->getOffset(), $query->getLimit());
        $total = null;
        if ($query->getCountTotal()) {
            $total = $this->userFinder->count($query->getFilter());
        }
        return new FindUsersQueryResult($users, $total);
    }
}
