<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindUsers;

use Grifix\Acl\Application\Projection\UserProjection\UserFilter;

class FindUsersQuery
{
    protected UserFilter $filter;

    protected ?int $offset;

    protected ?int $limit;

    protected bool $countTotal;

    public function __construct(
        ?UserFilter $filter = null,
        ?int $offset = null,
        ?int $limit = null,
        bool $countTotal = false
    ) {
        $this->filter = $filter ?? new UserFilter();
        $this->offset = $offset;
        $this->limit = $limit;
        $this->countTotal = $countTotal;
    }

    public function getFilter(): UserFilter
    {
        return $this->filter;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function getCountTotal(): bool
    {
        return $this->countTotal;
    }
}
