<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindUsers;

use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Kit\Collection\CollectionInterface;

class FindUsersQueryResult
{

    /**
     * @var UserDto[] | CollectionInterface
     */
    protected CollectionInterface $users;

    protected ?int $total;

    /**
     * FindUsersQueryResult constructor.
     * @param CollectionInterface|UserDto[] $users
     */
    public function __construct(CollectionInterface $users, ?int $total = null)
    {
        $this->users = $users;
        $this->total = $total;
    }

    /**
     * @return UserDto[] | CollectionInterface
     */
    public function getUsers(): CollectionInterface
    {
        return $this->users;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }
}
