<?php declare(strict_types = 1);

namespace Grifix\Acl\Application\Email\ConfirmationEmail;

class ConfirmationEmailVars
{
    protected string $confirmationLink;

    public function __construct(string $confirmationLink)
    {
        $this->confirmationLink = $confirmationLink;
    }

    public function getConfirmationLink(): string
    {
        return $this->confirmationLink;
    }
}
