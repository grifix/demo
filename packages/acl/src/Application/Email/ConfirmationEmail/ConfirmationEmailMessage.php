<?php declare(strict_types = 1);

namespace Grifix\Acl\Application\Email\ConfirmationEmail;

use Grifix\Kit\Mailer\AbstractEmailMessage;

class ConfirmationEmailMessage extends AbstractEmailMessage
{

    public function __construct(
        string $recipientEmail,
        string $subject,
        ConfirmationEmailVars $viewVars,
        string $langCode,
        ?string $recipientName = null,
        ?string $senderEmail = null,
        ?string $senderName = null
    ) {
        parent::__construct(
            $recipientEmail,
            $subject,
            $viewVars,
            sprintf('grifix.acl.email.%s.tpl.emailConfirmation', $langCode),
            $recipientName,
            $senderEmail,
            $senderName
        );
    }
}
