<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Projection\RoleProjection;

class RoleFilter
{
    protected ?string $id = null;

    protected ?string $name = null;

    protected ?string $userId = null;

    public static function create(): self
    {
        return new self();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): RoleFilter
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): RoleFilter
    {
        $this->name = $name;
        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(?string $userId): RoleFilter
    {
        $this->userId = $userId;
        return $this;
    }
}
