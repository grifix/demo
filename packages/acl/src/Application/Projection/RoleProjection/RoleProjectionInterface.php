<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Projection\RoleProjection;

use Grifix\Kit\Collection\CollectionInterface;

interface RoleProjectionInterface
{
    public function find(RoleFilter $filter, ?int $offset = null, ?int $limit = null): CollectionInterface;
}
