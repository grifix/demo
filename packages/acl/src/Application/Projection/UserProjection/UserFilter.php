<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Projection\UserProjection;

class UserFilter
{
    protected ?string $id = null;

    protected ?string $sessionId = null;

    protected ?bool $withPermissions = null;

    protected ?bool $withRoles = null;

    protected ?bool $isGuest = null;

    protected ?string $email = null;

    protected ?string $token = null;

    public static function create(): self
    {
        return new self();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): UserFilter
    {
        $this->email = $email;
        return $this;
    }


    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): UserFilter
    {
        $this->id = $id;
        return $this;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(?string $sessionId): UserFilter
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    public function getWithPermissions(): ?bool
    {
        return $this->withPermissions;
    }

    public function withPermissions(): UserFilter
    {
        $this->withPermissions = true;
        return $this;
    }

    public function isGuest(): ?bool
    {
        return $this->isGuest;
    }

    public function setIsGuest(bool $isGuest): UserFilter
    {
        $this->isGuest = $isGuest;
        return $this;
    }

    public function getWithRoles(): ?bool
    {
        return $this->withRoles;
    }

    public function setWithRoles(bool $withRoles): UserFilter
    {
        $this->withRoles = $withRoles;
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): UserFilter
    {
        $this->token = $token;
        return $this;
    }
}
