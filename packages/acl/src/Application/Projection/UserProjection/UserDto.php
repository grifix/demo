<?php

/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Projection\UserProjection;

use Grifix\Acl\Application\Projection\RoleProjection\RoleDto;
use Grifix\Kit\Collection\CollectionInterface;

class UserDto
{
    protected string $id;

    protected string $email;

    protected int $registrationDate;

    protected bool $isActive;

    protected bool $emailConfirmed;

    protected int $bruteForceCounter;

    protected ?int $lastSignInDate;

    protected ?string $lastIp;

    protected ?string $timeZone;

    protected ?string $token;

    protected bool $isGuest;

    protected ?array $permissions;

    /** @var RoleDto[] | CollectionInterface | null */
    protected ?CollectionInterface $roles;

    public function __construct(
        string $id,
        string $email,
        int $registrationDate,
        bool $isActive,
        bool $emailConfirmed,
        int $bruteForceCounter,
        bool $isGuest,
        ?int $lastSignDate = null,
        ?string $lastIp = null,
        ?string $timeZone = null,
        ?string $token = null,
        ?array $permissions = null,
        CollectionInterface $roles = null
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->registrationDate = $registrationDate;
        $this->isActive = $isActive;
        $this->emailConfirmed = $emailConfirmed;
        $this->bruteForceCounter = $bruteForceCounter;
        $this->lastSignInDate = $lastSignDate;
        $this->lastIp = $lastIp;
        $this->timeZone = $timeZone;
        $this->token = $token;
        $this->isGuest = $isGuest;
        $this->permissions = $permissions;
        $this->roles = $roles;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return RoleDto[]|CollectionInterface|null
     */
    public function getRoles(): ?CollectionInterface
    {
        return $this->roles;
    }


    public function getEmail(): string
    {
        return $this->email;
    }

    public function getRegistrationDate(): int
    {
        return $this->registrationDate;
    }


    public function isIsActive(): bool
    {
        return $this->isActive;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    public function isBruteForceCounter(): int
    {
        return $this->bruteForceCounter;
    }


    public function getLastSignInDate(): ?int
    {
        return $this->lastSignInDate;
    }


    public function getLastIp(): string
    {
        return $this->lastIp;
    }

    public function getTimeZone(): string
    {
        return $this->timeZone;
    }


    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getBruteForceCounter(): int
    {
        return $this->bruteForceCounter;
    }

    public function getIsGuest(): bool
    {
        return $this->isGuest;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getPermissions(): ?array
    {
        return $this->permissions;
    }
}
