<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Projection\UserProjection;

use Grifix\Kit\Collection\CollectionInterface;

interface UserProjectionInterface
{
    public function find(UserFilter $filter, ?int $offset = null, ?int $limit = null): CollectionInterface;

    public function count(UserFilter $filter): int;

    public function debarRole(string $userId, string $roleId): void;

    public function assignRole(string $userId, string $roleId): void;

    public function reset(): void;
}
