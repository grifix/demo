<?php

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\AssignRole;

class AssignRoleCommand
{
    protected string $userId;

    protected string $roleId;

    public function __construct(string $userId, string $roleId)
    {
        $this->userId = $userId;
        $this->roleId = $roleId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getRoleId(): string
    {
        return $this->roleId;
    }
}
