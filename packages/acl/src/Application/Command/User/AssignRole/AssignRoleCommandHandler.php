<?php

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\AssignRole;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

class AssignRoleCommandHandler
{
    protected UserRepositoryInterface $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }


    public function __invoke(AssignRoleCommand $command)
    {
        $user = $this->userRepository->get($command->getUserId());
        $user->assignRole($command->getRoleId());
    }
}
