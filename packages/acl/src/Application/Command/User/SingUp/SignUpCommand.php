<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\SingUp;

class SignUpCommand
{
    protected string $id;

    protected string $email;

    protected string $langCode;

    public function __construct(string $id, string $email, string $langCode)
    {
        $this->email = $email;
        $this->id = $id;
        $this->langCode = $langCode;
    }

    public function getEmail(): string
    {
        return $this->email;
    }


    public function getId(): string
    {
        return $this->id;
    }

    public function getLangCode(): string
    {
        return $this->langCode;
    }
}
