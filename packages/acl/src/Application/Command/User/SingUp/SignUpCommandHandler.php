<?php

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\SingUp;

use Grifix\Acl\Domain\User\UserFactoryInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;

class SignUpCommandHandler
{
    protected UserFactoryInterface $userFactory;

    protected UserRepositoryInterface $userRepository;

    public function __construct(
        UserFactoryInterface $userFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepository;
    }

    public function __invoke(SignUpCommand $command): void
    {
        $user = $this->userFactory->create($command->getId(), $command->getEmail(), $command->getLangCode());
        $this->userRepository->add($user);
    }

}
