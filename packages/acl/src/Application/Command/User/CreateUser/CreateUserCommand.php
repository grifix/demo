<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\CreateUser;

class CreateUserCommand
{
    protected string $id;

    protected string $email;

    protected ?string $password = null;

    protected string $roleId;

    protected string $langCode;

    protected bool $emailConfirmed = false;

    public function __construct(
        string $id,
        string $email,
        string $roleId,
        string $langCode,
        ?string $password = null,
        bool $emailConfirmed = false
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->roleId = $roleId;
        $this->langCode = $langCode;
        $this->emailConfirmed = $emailConfirmed;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getRoleId(): string
    {
        return $this->roleId;
    }

    public function getLangCode(): string
    {
        return $this->langCode;
    }

    public function getEmailConfirmed(): ?bool
    {
        return $this->emailConfirmed;
    }
}
