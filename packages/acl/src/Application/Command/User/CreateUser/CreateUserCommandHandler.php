<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\CreateUser;

use Grifix\Acl\Domain\User\UserFactoryInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;

class CreateUserCommandHandler
{
    private UserFactoryInterface $userFactory;

    private UserRepositoryInterface $userRepository;

    public function __construct(
        UserFactoryInterface $userFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
    }

    public function __invoke(CreateUserCommand $command): void
    {
        $user = $this->userFactory->create(
            $command->getId(),
            $command->getEmail(),
            $command->getLangCode(),
            $command->getPassword(),
            $command->getEmailConfirmed()
        );
        $user->assignRole($command->getRoleId());
        $this->userRepository->add($user);
    }
}
