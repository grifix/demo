<?php declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\ActivateUser;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

class ActivateUserCommandHandler
{
    protected UserRepositoryInterface $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }


    public function __invoke(ActivateUserCommand $command)
    {
        $user = $this->userRepository->get($command->getUserId());
        $user->activate();
    }
}
