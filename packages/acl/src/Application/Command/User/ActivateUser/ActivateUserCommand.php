<?php declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\ActivateUser;

class ActivateUserCommand
{
    protected string $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }
}
