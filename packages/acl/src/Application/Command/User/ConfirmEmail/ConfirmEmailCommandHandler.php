<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\ConfirmEmail;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

class ConfirmEmailCommandHandler
{
    protected UserRepositoryInterface $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ConfirmEmailCommand $command): void
    {
        $user = $this->repository->get($command->getUserId());
        $user->confirmEmail($command->getToken());
    }
}
