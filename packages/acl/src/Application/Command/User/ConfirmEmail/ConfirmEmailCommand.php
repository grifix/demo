<?php

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\ConfirmEmail;

class ConfirmEmailCommand
{
    protected string $userId;

    protected string $token;

    public function __construct(string $userId, string $token)
    {
        $this->userId = $userId;
        $this->token = $token;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
