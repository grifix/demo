<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\SetPassword;


use Grifix\Acl\Domain\User\UserRepositoryInterface;

class SetPasswordCommandHandler
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(SetPasswordCommand $command): void
    {
        $this->userRepository->get($command->getUserId())->setPassword($command->getPassword());
    }
}
