<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\SingIn;

class SignInCommand
{
    protected string $userId;

    protected string $password;

    protected string $ip;

    protected string $sessionId;

    public function __construct(string $userId, string $password, string $ip, string $sessionId)
    {
        $this->userId = $userId;
        $this->password = $password;
        $this->ip = $ip;
        $this->sessionId = $sessionId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getSessionId(): string
    {
        return $this->sessionId;
    }
}
