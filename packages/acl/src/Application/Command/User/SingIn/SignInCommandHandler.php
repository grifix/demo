<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\SingIn;

use Grifix\Acl\Domain\User\UserRepositoryInterface;
use Grifix\Shared\Application\Common\Clock\ClockInterface;

class SignInCommandHandler
{
    protected UserRepositoryInterface $userRepository;

    protected ClockInterface $clockService;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ClockInterface $clockService
    ) {
        $this->userRepository = $userRepository;
        $this->clockService = $clockService;
    }

    public function __invoke(SignInCommand $command): void
    {
        $user = $this->userRepository->get($command->getUserId());

        $user->signIn(
            $command->getPassword(),
            $command->getIp(),
            $command->getSessionId(),
            $this->clockService->getTime()
        );
    }
}
