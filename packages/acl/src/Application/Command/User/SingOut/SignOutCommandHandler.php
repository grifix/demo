<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\SingOut;

use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;

class SignOutCommandHandler
{
    protected UserRepositoryInterface $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function __invoke(SignOutCommand $command): void
    {
        if ($command->getUserId() !== UserInterface::GUEST_ID) {
            $user = $this->userRepository->get($command->getUserId());
            $user->signOut();
        }
    }
}
