<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\User\ChangePassword;

class ChangePasswordCommand
{
    protected string $userId;

    protected string $oldPassword;

    protected string $newPassword;

    protected string $newPasswordAgain;

    public function __construct(string $userId, string $oldPassword, string $newPassword, string $newPasswordAgain)
    {
        $this->userId = $userId;
        $this->oldPassword = $oldPassword;
        $this->newPassword = $newPassword;
        $this->newPasswordAgain = $newPasswordAgain;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    public function getNewPasswordAgain(): string
    {
        return $this->newPasswordAgain;
    }
}
