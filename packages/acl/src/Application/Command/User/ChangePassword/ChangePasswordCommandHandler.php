<?php

declare(strict_types = 1);

namespace Grifix\Acl\Application\Command\User\ChangePassword;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

class ChangePasswordCommandHandler
{
    protected UserRepositoryInterface $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ChangePasswordCommand $command): void
    {
            $user = $this->repository->get($command->getUserId());
            $user->changePassword(
                $command->getOldPassword(),
                $command->getNewPassword(),
                $command->getNewPasswordAgain()
            );
    }
}
