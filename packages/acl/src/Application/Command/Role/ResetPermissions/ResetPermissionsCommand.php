<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\Role\ResetPermissions;

class ResetPermissionsCommand
{
    protected string $roleId;

    public function __construct(string $roleId)
    {
        $this->roleId = $roleId;
    }

    public function getRoleId(): string
    {
        return $this->roleId;
    }
}
