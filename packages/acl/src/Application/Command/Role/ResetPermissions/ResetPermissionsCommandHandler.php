<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\Role\ResetPermissions;

use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Kernel\KernelInterface;

class ResetPermissionsCommandHandler
{
    protected RoleRepositoryInterface $roleRepository;

    protected KernelInterface $kernel;

    protected ConfigInterface $config;

    public function __construct(
        RoleRepositoryInterface $roleRepository,
        KernelInterface $kernel,
        ConfigInterface $config
    ) {
        $this->roleRepository = $roleRepository;
        $this->kernel = $kernel;
        $this->config = $config;
    }

    public function __invoke(ResetPermissionsCommand $command): void
    {
        $role = $this->roleRepository->get($command->getRoleId());
        $role->resetPermissions();
    }
}
