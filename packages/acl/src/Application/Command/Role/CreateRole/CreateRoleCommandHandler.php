<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Application\Command\Role\CreateRole;

use Grifix\Acl\Domain\Role\RoleFactoryInterface;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;

class CreateRoleCommandHandler
{
    protected RoleFactoryInterface $roleFactory;

    protected RoleRepositoryInterface $roleRepository;

    public function __construct(
        RoleFactoryInterface $roleFactory,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->roleFactory = $roleFactory;
        $this->roleRepository = $roleRepository;
    }

    public function __invoke(CreateRoleCommand $command): void
    {
        $role = $this->roleFactory->createRole($command->getId(), $command->getName());
        $this->roleRepository->add($role);
    }
}
