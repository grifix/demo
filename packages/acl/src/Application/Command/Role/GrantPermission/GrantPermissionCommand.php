<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Application\Command\Role\GrantPermission;

class GrantPermissionCommand
{
    protected string $roleId;

    protected string $permission;

    public function __construct(string $roleId, string $permission)
    {
        $this->roleId = $roleId;
        $this->permission = $permission;
    }

    public function getRoleId(): string
    {
        return $this->roleId;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }
}
