<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Application\Command\Role\GrantPermission;

use Grifix\Acl\Domain\Role\RoleRepositoryInterface;

class GrantPermissionCommandHandler
{
    protected RoleRepositoryInterface $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function __invoke(GrantPermissionCommand $command): void
    {
        $this->roleRepository->get($command->getRoleId())->grantPermission($command->getPermission());
    }
}
