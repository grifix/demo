<?php declare(strict_types=1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Application\Subscriber;

use Grifix\Acl\Application\Email\ConfirmationEmail\ConfirmationEmailMessage;
use Grifix\Acl\Application\Email\ConfirmationEmail\ConfirmationEmailVars;
use Grifix\Acl\Domain\User\Event\UserCreatedEvent;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Mailer\MailerInterface;
use Grifix\Kit\Route\UrlMaker\UrlMakerInterface;
use Grifix\Shared\Application\Subscriber\PermanentSubscriber;

class EmailNotificationSubscriber extends PermanentSubscriber
{
    protected MailerInterface $mailer;

    protected TranslatorInterface $translator;

    protected UrlMakerInterface $urlMaker;

    public function __construct(
        MailerInterface $mailer,
        TranslatorInterface $translator,
        UrlMakerInterface $urlMaker
    ) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->urlMaker = $urlMaker;
    }

    public function onUserCreated(UserCreatedEvent $event)
    {
        if (!$event->isEmailConfirmed()) {
            $this->mailer->sendMessage(
                new ConfirmationEmailMessage(
                    $event->getEmail(),
                    $this->translator->translateToLang('grifix.acl.msg_emailConfirmation', $event->getLangCode()),
                    new ConfirmationEmailVars(
                        $this->urlMaker->makeUrl('grifix.acl.root.confirmEmail', ['token' => $event->getToken()])
                    ),
                    $event->getLangCode(),
                    $event->getEmail()
                )
            );
        }
    }
}
