<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Application\Subscriber;

use Grifix\Acl\Application\Projection\UserProjection\UserProjectionInterface;
use Grifix\Acl\Domain\User\Event\RoleAssignedEvent;
use Grifix\Acl\Domain\User\Event\RoleDebarredEvent;
use Grifix\Shared\Application\Subscriber\PermanentSubscriber;
use Grifix\Shared\Application\Subscriber\ResettableSubscriberInterface;

class UserProjectionSubscriberInterface extends PermanentSubscriber implements ResettableSubscriberInterface
{
    protected UserProjectionInterface $userProjection;

    public function __construct(UserProjectionInterface $userProjection)
    {
        $this->userProjection = $userProjection;
    }

    public function orDebarRole(RoleDebarredEvent $event)
    {
        $this->userProjection->debarRole($event->getUserId(), $event->getRoleId());
    }

    public function onAssignRole(RoleAssignedEvent $event)
    {
        $this->userProjection->assignRole($event->getUserId(), $event->getRoleId());
    }

    public function reset(): void
    {
        $this->userProjection->reset();
    }
}
