<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Application\Projection;

use Grifix\Acl\Application\Projection\RoleProjection\RoleFilter;
use Grifix\Acl\Application\Projection\RoleProjection\RoleProjectionInterface;
use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Projection\UserProjection\UserProjectionInterface;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Conversion\ConversionFactoryInterface;
use Grifix\Kit\Conversion\Converter\StringToBoolConverter;
use Grifix\Kit\Conversion\Converter\StringToIntConverter;
use Grifix\Kit\Conversion\Converter\UtcDateStringToTimestampConverter;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Type\DateTime\DateTime;

class UserProjection implements UserProjectionInterface
{
    protected const TABLE = 'grifix_acl.user_role_projection';

    protected CollectionFactoryInterface $collectionFactory;

    protected ConnectionInterface $connection;

    protected ConversionFactoryInterface $conversionFactory;

    protected RoleProjectionInterface $roleFinder;

    protected string $name = 'user';

    public function __construct(
        CollectionFactoryInterface $collectionFactory,
        ConnectionInterface $connection,
        ConversionFactoryInterface $conversionFactory,
        RoleProjectionInterface $roleFinder
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->connection = $connection;
        $this->conversionFactory = $conversionFactory;
        $this->roleFinder = $roleFinder;
        if (false === $this->tableExists()) {
            $this->reset();
        }
    }

    public function find(UserFilter $filter, ?int $offset = null, ?int $limit = null): CollectionInterface
    {
        $query = $this->connection->createQuery()
            ->select('*')
            ->from(Table::USER);

        $this->applyFilter($filter, $query);

        if ($offset !== null) {
            $query->offset($offset);
        }

        if ($limit !== null) {
            $query->limit($limit);
        }

        $records = $query->fetchAll();

        $result = [];
        foreach ($records as $record) {
            $permissions = [];
            $roles = null;

            if ($filter->getWithPermissions()) {
                $permissions = $this->fetchUserPermissions($record['id']);
            }

            if ($filter->getWithRoles()) {
                $roles = $this->fetchUserRoles($record['id']) ?? [];
            }
            $result[] = $this->createUserDto(json_decode($record['data'], true), $permissions, $roles);
        }

        return $this->collectionFactory->createCollection($result);
    }

    public function count(UserFilter $filter): int
    {
        $query = $this->connection->createQuery()
            ->select('COUNT(*) AS result')
            ->from(Table::USER);
        $this->applyFilter($filter, $query);
        return $query->fetch()['result'];
    }

    public function debarRole(string $userId, string $roleId): void
    {
        $this->connection->delete($this->makeUserRolesTableName(), [
            'user_id' => $userId,
            'role_id' => $roleId
        ]);
    }

    public function assignRole(string $userId, string $roleId): void
    {
        $this->debarRole($userId, $roleId);
        $this->connection->insert([
            'user_id' => $userId,
            'role_id' => $roleId
        ], $this->makeUserRolesTableName());
    }

    protected function applyFilter(UserFilter $filter, QueryInterface $query): void
    {
        if ($filter->getId()) {
            $query->where('id = :id')->bindValue('id', $filter->getId());
        }

        if ($filter->isGuest()) {
            $query->where('id = :id')->bindValue('id', UserInterface::GUEST_ID);
        }

        if ($filter->getSessionId()) {
            $query->where("data->>'sessionId' = :sessionId")->bindValue('sessionId', $filter->getSessionId());
        }

        if ($filter->getToken()) {
            $query->where("data->>'token' = :token")->bindValue('token', $filter->getToken());
        }

        if ($filter->getEmail()) {
            $query->where("data->'email'->>'value' = :email")->bindValue('email', $filter->getEmail());
        }
    }

    protected function createUserDto(
        array $record,
        ?array $permissions = null,
        ?CollectionInterface $roles = null
    ): UserDto {
        $conversion = $this->conversionFactory->create();
        $conversion->createField('registrationDate')->createConverter(UtcDateStringToTimestampConverter::class);
        $conversion->createField('isActive')->createConverter(StringToBoolConverter::class);
        $conversion->createField('emailConfirmed')->createConverter(StringToBoolConverter::class);
        $conversion->createField('bruteForceCounter')->createConverter(StringToIntConverter::class);
        $conversion->createField('activationDate')->createConverter(UtcDateStringToTimestampConverter::class);
        $conversion->createField('lastSignInDate')->createConverter(UtcDateStringToTimestampConverter::class);
        $conversion->convert($record);
        return new UserDto(
            $record['id']['value'],
            $record['email']['value'],
            DateTime::makeTimeStamp($record['registrationDate']['value']),
            $record['isActive'],
            $record['email']['confirmed'],
            $record['bruteForceCounter'],
            ($record['id']['value'] == UserInterface::GUEST_ID),
            $record['lastSignInDate'] && $record['lastSignInDate']['value'] !== null
                ? DateTime::makeTimeStamp($record['lastSignInDate']['value']) : null,
            $record['lastIp'] !== null ? $record['lastIp']['value'] : null,
            $record['timeZone'],
            $record['token'],
            $permissions,
            $roles
        );
    }

    protected function fetchUserPermissions(string $userId): array
    {
        $rows = $this->connection->fetchAll(
            "SELECT jsonb_array_elements(r.data->'permissions') as permission
                    FROM " . Table::ROLE . " AS r
                    INNER JOIN " . $this->makeUserRolesTableName() . " AS ur ON ur.role_id = r.id
                    WHERE ur.user_id = :user_id",
            ['user_id' => $userId]
        );

        $result = [];
        foreach ($rows as $row) {
            $result[] = json_decode($row['permission']);
        }
        return $result;
    }

    protected function makeUserRolesTableName(): string
    {
        return sprintf('grifix_acl.' . $this->name . '_role_projection');
    }

    protected function fetchUserRoles(string $userId): CollectionInterface
    {
        $filter = new RoleFilter();
        $filter->setUserId($userId);
        return $this->roleFinder->find($filter);
    }

    public function reset(): void
    {
        $sql = <<<SQL
drop table if exists %s;
create table %s
(
	user_id uuid not null,
	role_id uuid not null,
	constraint user_role_projection_idx
		primary key (user_id, role_id)
);
SQL;
        $this->connection->execute(sprintf($sql, self::TABLE, self::TABLE));
    }

    protected function tableExists(): bool
    {
        $table = explode('.', self::TABLE);
        return null !== $this->connection->fetch(
            'select * from pg_catalog.pg_tables where schemaname = :schema and tablename = :table',
            [
                'schema' => $table[0],
                'table' => $table[1]
            ]
        );
    }
}
