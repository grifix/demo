<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Application\Projection;

use Grifix\Acl\Application\Projection\RoleProjection\RoleDto;
use Grifix\Acl\Application\Projection\RoleProjection\RoleFilter;
use Grifix\Acl\Application\Projection\RoleProjection\RoleProjectionInterface;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;

class RoleProjection implements RoleProjectionInterface
{
    private ConnectionInterface $connection;

    private CollectionFactoryInterface $collectionFactory;

    public function __construct(ConnectionInterface $connection, CollectionFactoryInterface $collectionFactory)
    {
        $this->connection = $connection;
        $this->collectionFactory = $collectionFactory;
    }

    public function find(RoleFilter $filter, ?int $offset = null, ?int $limit = null): CollectionInterface
    {
        $query = $this->connection->createQuery()
            ->select('*')
            ->from(Table::ROLE, 'r');

        $this->applyFilter($filter, $query);

        if ($offset !== null) {
            $query->offset($offset);
        }

        if ($limit !== null) {
            $query->limit($limit);
        }

        $records = $query->fetchAll();
        $result = $this->collectionFactory->createCollection();
        foreach ($records as $record) {
            $data = json_decode($record['data'], true);
            $result->add(new RoleDto($record['id'], $data['name']));
        }

        return $result;
    }

    protected function applyFilter(RoleFilter $filter, QueryInterface $query): void
    {
        if ($filter->getId()) {
            $query->where('r.id = :id')->bindValue('id', $filter->getId());
        }

        if ($filter->getName()) {
            $query->where("r.data->>'name' = :name")->bindValue('name', $filter->getName());
        }

        if ($filter->getUserId()) {
            $query
                ->innerJoin('grifix_acl.user_role_projection', 'u2r', 'r.id = u2r.role_id')
                ->where('u2r.user_id = :user_id')
                ->bindValue('user_id', $filter->getUserId());
        }
    }
}
