<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\Role;

use Grifix\Acl\Domain\Role\Role;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Shared\Infrastructure\Internal\AbstractBlueprint;

class RoleBlueprint extends AbstractBlueprint
{
    protected function getEntityClassName(): string
    {
        return Role::class;
    }

    public function getTable(): string
    {
        return Table::ROLE;
    }
}
