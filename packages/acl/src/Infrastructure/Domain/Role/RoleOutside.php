<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\Role;

use Grifix\Acl\Domain\Role\RoleOutsideInterface;
use Grifix\Acl\Infrastructure\Service\PermissionRepository\PermissionRepositoryInterface;

class RoleOutside implements RoleOutsideInterface
{
    protected PermissionRepositoryInterface $permissionRepository;

    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }


    public function getDefaultPermissions(string $roleName): array
    {
        return $this->permissionRepository->getRoleDefaultPermissions($roleName);
    }
}
