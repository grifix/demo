<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\Role;

use Grifix\Acl\Domain\Role\Exception\RoleNotExistsException;
use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Repository\RepositoryInterface;

class RoleRepository implements RoleRepositoryInterface
{
    protected RepositoryInterface $repository;

    use CollectionWrapperTrait;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function get(string $id): RoleInterface
    {
        $result = $this->repository->find($id);
        if (!$result) {
            throw new RoleNotExistsException('id', $id);
        }
        /**@var $result RoleInterface */
        return $result;
    }

    public function add(RoleInterface $role): void
    {
        $this->repository->add($role);
    }

    public function delete(RoleInterface $role): void
    {
        $this->repository->delete($role);
    }

    public function exists(string $id): bool
    {
        return boolval($this->repository->findBy('id', $id));
    }
}
