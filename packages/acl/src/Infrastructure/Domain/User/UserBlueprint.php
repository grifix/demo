<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\User;

use Grifix\Acl\Domain\User\User;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Shared\Infrastructure\Internal\AbstractBlueprint;

class UserBlueprint extends AbstractBlueprint
{
    protected string $entityClass = User::class;

    protected string $table = Table::USER;

    protected function getEntityClassName(): string
    {
        return User::class;
    }

    public function getTable(): string
    {
        return Table::USER;
    }
}
