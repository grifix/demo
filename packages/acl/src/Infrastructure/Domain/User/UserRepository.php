<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\User;

use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;
use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Repository\RepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    protected RepositoryInterface $repository;

    use CollectionWrapperTrait;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function get(string $id): UserInterface
    {
        $result = $this->repository->find($id);
        if (!$result) {
            throw new UserNotExistsException('id', $id);
        }
        /**@var $result UserInterface */
        return $result;
    }

    public function add(UserInterface $user): void
    {
        $this->repository->add($user);
    }

    public function delete(UserInterface $user): void
    {
        $this->repository->delete($user);
    }
}
