<?php
declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Domain\User;

use Grifix\Acl\Domain\User\Email\UserEmail;
use Grifix\Shared\Infrastructure\Internal\Serializer\ObjectSerializer;

class UserSerializer extends ObjectSerializer
{
    protected function init(): void
    {
        parent::init();
        $this->addObjectPropertySerializer('email', UserEmail::class, ObjectSerializer::class);
    }
}
