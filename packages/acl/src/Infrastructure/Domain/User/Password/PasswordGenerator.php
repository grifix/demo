<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\User\Password;

use Grifix\Kit\Type\Range\RangeInterface;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;

class PasswordGenerator implements PasswordGeneratorInterface
{
    protected RequirementPasswordGenerator $passwordGenerator;

    /**
     * @param int $length <cfg:grifix.acl.password.length>
     * @param RangeInterface $upperCase <cfg:grifix.acl.password.upperCase>
     * @param RangeInterface $loverCase <cfg:grifix.acl.password.loverCase>
     * @param RangeInterface $numbers <cfg:grifix.acl.password.numbers>
     * @param RangeInterface $symbols <cfg:grifix.acl.password.symbols>
     */
    public function __construct(
        RequirementPasswordGenerator $computerPasswordGenerator,
        int $length,
        RangeInterface $upperCase = null,
        RangeInterface $loverCase = null,
        RangeInterface $numbers = null,
        RangeInterface $symbols = null
    ) {
        $this->passwordGenerator = $computerPasswordGenerator;
        $this->passwordGenerator->setLength($length);
        if ($upperCase) {
            $this->passwordGenerator->setUppercase(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_UPPER_CASE,
                $upperCase->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_UPPER_CASE,
                $upperCase->getTo()
            );
        } else {
            $this->passwordGenerator->setUppercase(false);
        }

        if ($loverCase) {
            $this->passwordGenerator->setLowercase(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_LOWER_CASE,
                $loverCase->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_LOWER_CASE,
                $loverCase->getTo()
            );
        } else {
            $this->passwordGenerator->setLowercase(false);
        }

        if ($numbers) {
            $this->passwordGenerator->setNumbers(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_NUMBERS,
                $numbers->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_NUMBERS,
                $numbers->getTo()
            );
        } else {
            $this->passwordGenerator->setNumbers(false);
        }

        if ($symbols) {
            $this->passwordGenerator->setSymbols(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_SYMBOLS,
                $symbols->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_SYMBOLS,
                $symbols->getTo()
            );
        } else {
            $this->passwordGenerator->setSymbols(false);
        }
    }

    public function generatePassword(): string
    {
        return $this->passwordGenerator->generatePassword();
    }

    public function validatePassword(string $password): bool
    {
        return $this->passwordGenerator->validatePassword($password);
    }
}
