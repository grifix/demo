<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\User\Password;

use Grifix\Acl\Domain\User\Password\PasswordOutsideInterface;
use Grifix\Kit\Mailer\MailerInterface;

class PasswordOutside implements PasswordOutsideInterface
{
    protected PasswordGeneratorInterface $generator;

    protected PasswordHasherInterface $hasher;

    protected MailerInterface $mailer;

    public function __construct(
        PasswordGeneratorInterface $generator,
        PasswordHasherInterface $hasher,
        MailerInterface $mailer
    ) {
        $this->generator = $generator;
        $this->hasher = $hasher;
        $this->mailer = $mailer;
    }

    public function generatePassword(): string
    {
        return $this->generator->generatePassword();
    }

    public function validatePassword(string $password): bool
    {
        return $this->generator->validatePassword($password);
    }

    public function hashPassword(string $password): string
    {
        return $this->hasher->hashPassword($password);
    }

    public function verifyPassword(string $password, string $hash): bool
    {
        return $this->hasher->verifyPassword($password, $hash);
    }
}
