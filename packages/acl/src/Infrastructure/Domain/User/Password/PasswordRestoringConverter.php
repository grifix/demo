<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\User\Password;

use Grifix\Acl\Domain\User\Password\Password;
use Grifix\Acl\Domain\User\Password\PasswordOutsideInterface;
use Grifix\Kit\Conversion\Converter\AbstractConverter;

class PasswordRestoringConverter extends AbstractConverter
{
    protected PasswordOutsideInterface $passwordInfrastructure;

    public function __construct(PasswordOutsideInterface $passwordInfrastructure)
    {
        $this->passwordInfrastructure = $passwordInfrastructure;
    }

    protected function doConvert($value)
    {
        $reflectionClass = new \ReflectionClass(Password::class);
        $result = $reflectionClass->newInstanceWithoutConstructor();
        $valueProperty = $reflectionClass->getProperty('value');
        $valueProperty->setAccessible(true);
        $valueProperty->setValue($result, $value);
        $valueProperty->setAccessible(false);
        $infrastructureProperty = $reflectionClass->getProperty('infrastructure');
        $infrastructureProperty->setAccessible(true);
        $infrastructureProperty->setValue($result, $this->passwordInfrastructure);
        $infrastructureProperty->setAccessible(false);
        return $result;
    }
}
