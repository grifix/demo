<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Domain\User\Email;

use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Projection\UserProjection\UserProjectionInterface;
use Grifix\Acl\Domain\User\Email\UserEmailOutsideInterface;

class UserEmailOutside implements UserEmailOutsideInterface
{
    protected UserProjectionInterface $userFinder;

    public function __construct(UserProjectionInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function emailExists(string $email): bool
    {
        return (bool) $this->userFinder->find(UserFilter::create()->setEmail($email), 0, 1)->count();
    }
}
