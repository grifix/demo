<?php declare(strict_types=1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Infrastructure\Domain\User;

use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Acl\Domain\User\UserInfrastructureInterface;
use Grifix\Acl\Infrastructure\Domain\User\Password\PasswordGeneratorInterface;
use Grifix\Kit\Mailer\MailerInterface;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherTrait;
use Grifix\Kit\Type\DateTime\DateTimeFactoryInterface;
use Grifix\Kit\Type\DateTime\DateTimeInterface;

class UserInfrastructure implements UserInfrastructureInterface
{

    use EventPublisherTrait;

    protected MailerInterface $mailer;

    protected int $maxSignInAttempts;

    protected int $bruteForceBlockInterval;

    protected PasswordGeneratorInterface $passwordGenerator;

    protected RoleRepositoryInterface $roleRepository;

    protected DateTimeFactoryInterface $dateTimeFactory;

    /**
     * @param int $maxSingInAttempts <cfg:grifix.acl.signIn.maxAttempts>
     * @param int $bruteForceBlockInterval <cfg:grifix.acl.signIn.bruteForceBlockInterval>
     */
    public function __construct(
        EventCollectorInterface $eventCollector,
        MailerInterface $mailer,
        int $maxSingInAttempts,
        int $bruteForceBlockInterval,
        PasswordGeneratorInterface $passwordGenerator,
        RoleRepositoryInterface $roleRepository,
        DateTimeFactoryInterface $dateTimeFactory
    ) {
        $this->eventCollector = $eventCollector;
        $this->mailer = $mailer;
        $this->maxSignInAttempts = $maxSingInAttempts;
        $this->bruteForceBlockInterval = $bruteForceBlockInterval;
        $this->passwordGenerator = $passwordGenerator;
        $this->roleRepository = $roleRepository;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    public function getBruteForceBlockInterval(): int
    {
        return $this->bruteForceBlockInterval;
    }

    public function getDefaultTimezone(): string
    {
        return date_default_timezone_get();
    }

    public function getMaxSignInAttempts(): int
    {
        return $this->maxSignInAttempts;
    }

    public function generatePassword(): string
    {
        return $this->passwordGenerator->generatePassword();
    }

    public function roleExists(string $roleId): bool
    {
        return $this->roleRepository->exists($roleId);
    }

    public function createDateTime(int $timeStamp): DateTimeInterface
    {
        return $this->dateTimeFactory->createFromTimeStamp($timeStamp);
    }
}
