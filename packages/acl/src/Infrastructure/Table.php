<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure;

interface Table
{
    public const USER = 'grifix_acl."user"';
    public const ROLE = 'grifix_acl."role"';
    public const USER_PROJECTION = 'grifix_acl."user_projection"';
}
