<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */
class M20171214083057User extends AbstractMigration
{
    public function up(): void
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS grifix_acl."user" (
  "id" uuid NOT NULL,
  "data" JSONB NOT NULL,
  PRIMARY KEY(id),
    CHECK (validate_json_schema($$
{
    "type": "object",
    "additionalProperties": false,
    "required": [
        "id",
        "email",
        "password",
        "langCode",
        "registrationDate",
        "activationDate",
        "lastSignInDate",
        "isActive",
        "bruteForceCounter",
        "lastIp",
        "timeZone",
        "blockingReason",
        "token",
        "sessionId",
        "roles"
    ],
    "properties": {
        "id": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "string"
                }
            }
        },
        "email": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "confirmed",
                "value"
            ],
            "properties": {
                "confirmed": {
                    "type": "boolean"
                },
                "value": {
                    "type": "string"
                }
            }
        },
        "password": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "string"
                }
            }
        },
        "langCode": {
            "type": "string"
        },
        "registrationDate": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "string"
                }
            }
        },
        "activationDate": {
            "oneOf": [
                {
                    "type": "null"
                },
                {
                    "type": "object",
                    "additionalProperties": false,
                    "required": [
                        "value"
                    ],
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                }
            ]
        },
        "lastSignInDate": {
            "oneOf": [
                {
                    "type": "null"
                },
                {
                    "type": "object",
                    "additionalProperties": false,
                    "required": [
                        "value"
                    ],
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                }
            ]
        },
        "isActive": {
            "type": "boolean"
        },
        "bruteForceCounter": {
            "type": "integer"
        },
        "lastIp": {
            "oneOf": [
                {
                    "type": "null"
                },
                {
                    "type": "object",
                    "additionalProperties": false,
                    "required": [
                        "value"
                    ],
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                }
            ]
        },
        "timeZone": {
            "type": "string"
        },
        "blockingReason": {
            "type": [
                "string",
                "null"
            ]
        },
        "token": {
            "type": [
                "string",
                "null"
            ]
        },
        "sessionId": {
            "type": [
                "string",
                "null"
            ]
        },
        "roles": {
            "oneOf": [
                {
                    "type": "array"
                },
                {
                    "type": "object"
                }
            ]
    }
    }
}
    $$, data))
  )          
WITH (oids = false);
CREATE INDEX  idx_user_data ON grifix_acl."user" USING GIN (data);
CREATE UNIQUE INDEX idx_user_data_token ON grifix_acl."user"(((data->>'token')::varchar(255)));
CREATE UNIQUE INDEX idx_user_data_email ON grifix_acl."user"(((data->'email'->>'value')::varchar(255)));

SQL;


        $this->execute($sql);
    }

    public function down(): void
    {
        $this->execute('
            DROP TABLE IF EXISTS grifix_acl."user"
        ');
    }
}
