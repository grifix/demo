<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */
class M20171212080225Schema extends AbstractMigration
{
    public function up(): void
    {
        $sql = <<<SQL
CREATE SCHEMA IF NOT EXISTS "grifix_acl";
SQL;
        $this->execute($sql);
    }

    public function down(): void
    {
        $sql = <<<SQL
DROP SCHEMA IF EXISTS "grifix_acl" CASCADE ;
SQL;
        $this->execute($sql);
    }
}
