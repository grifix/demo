<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Kit\Migration\AbstractMigration;

class M20171214080828Role extends AbstractMigration
{
    public function up(): void
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS grifix_acl."role" (
    id uuid NOT NULL,
    data JSONB NOT NULL,
    PRIMARY KEY(id),
    CHECK (validate_json_schema($$
{
    "type": "object",
    "additionalProperties": false,
    "required": [
        "id",
        "name",
        "permissions"
    ],
    "properties": {
        "id": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "string"
                }
            }
        },
        "name": {
            "type": "string"
        },
        "permissions": {

            "oneOf": [
                {
                    "type": "array"
                },
                {
                    "type": "object"
                }
            ]
            
        }
    }
}
    $$, data))
  ) 
  WITH (OIDS = FALSE);
  CREATE INDEX IF NOT EXISTS idx_role_data ON grifix_acl."role" USING GIN (data);
  CREATE UNIQUE INDEX idx_role_data_name ON grifix_acl."role"(((data->>'name')::varchar(255)));
SQL;

        $this->execute($sql);
    }

    public function down(): void
    {
        $this->execute('
            DROP TABLE IF EXISTS grifix_acl."role"
        ');
    }
}
