<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Grifix;

use Grifix\Acl\Domain\Role\Role;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Acl\Domain\User\User;
use Grifix\Acl\Domain\User\UserRepositoryInterface;
use Grifix\Acl\Infrastructure\Domain\Role\RoleBlueprint;
use Grifix\Acl\Infrastructure\Domain\Role\RoleRepository;
use Grifix\Acl\Infrastructure\Domain\User\UserBlueprint;
use Grifix\Acl\Infrastructure\Domain\User\UserRepository;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Repository\RepositoryFactoryInterface;

class Bootstrap implements ModuleCommandInterface
{
    use ModuleClassTrait;

    /**
     * @return void
     */
    public function run(): void
    {
        $entityManager = $this->getShared(EntityManagerInterface::class);

        $entityManager->registerBlueprint(UserBlueprint::class);
        $entityManager->registerBlueprint(RoleBlueprint::class);

        $this->setShared(UserRepositoryInterface::class, function () {
            /**@var IocContainerInterface $this */
            return $this->get(RepositoryFactoryInterface::class)->createWrapper(
                User::class,
                UserRepository::class
            );
        });

        $this->setShared(RoleRepositoryInterface::class, function () {
            /**@var IocContainerInterface $this */
            return $this->get(RepositoryFactoryInterface::class)->createWrapper(
                Role::class,
                RoleRepository::class
            );
        });
    }
}
