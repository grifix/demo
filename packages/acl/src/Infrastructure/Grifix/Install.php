<?php

/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Grifix;

use Grifix\Acl\Application\Command\Role\CreateRole\CreateRoleCommand;
use Grifix\Acl\Application\Command\Role\ResetPermissions\ResetPermissionsCommand;
use Grifix\Acl\Application\Command\User\AssignRole\AssignRoleCommand;
use Grifix\Acl\Application\Command\User\CreateUser\CreateUserCommand;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQuery;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQueryResult;
use Grifix\Acl\Application\Query\GeneratePassword\GeneratePasswordQuery;
use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Psr\Log\LoggerInterface;

class Install implements ModuleCommandInterface
{
    use ModuleClassTrait;

    protected QueryBusInterface $queryBus;

    protected CommandBusInterface $commandBus;

    protected function init()
    {
        $this->queryBus = $this->getShared(QueryBusInterface::class);
        $this->commandBus = $this->getShared(CommandBusInterface::class);
    }

    public function run(): void
    {
        $this->init();
        $this->createRoles();
        $this->createGuest();

        $adminEmail = 'admin@grifix.net';
        $adminPassword = $this->queryBus->execute(new GeneratePasswordQuery());
        $this->createAdmin($adminEmail, $adminPassword);

        $this->resetPermissions();

        echo "\nACL module has been installed\nadmin login: " . $adminEmail . "\npassword: " . $adminPassword . "\n\n";
        $this->getShared(LoggerInterface::class)->debug('ACL module has been installed');
    }

    protected function resetPermissions()
    {
        /**@var $result FindRolesQueryResult */
        $result = $this->queryBus->execute(new FindRolesQuery());
        foreach ($result->getRoles() as $role) {
            $this->commandBus->execute(new ResetPermissionsCommand($role->getId()));
        }
    }

    protected function createRoles()
    {
        $this->commandBus->execute(new CreateRoleCommand(RoleInterface::ID_ADMIN, 'admin'));
        $this->commandBus->execute(new CreateRoleCommand(RoleInterface::ID_GUEST, 'guest'));
        $this->commandBus->execute(new CreateRoleCommand(RoleInterface::ID_USER, 'user'));
    }

    protected function createGuest()
    {
        $this->commandBus->execute(new CreateUserCommand(
            UserInterface::GUEST_ID,
            'guest@grifix.net',
            RoleInterface::ID_GUEST,
            $this->getShared(TranslatorInterface::class)->getCurrentLang()->getCode()
        ));
    }

    protected function createAdmin(string $email, string $password): void
    {
        $this->commandBus->execute(new CreateUserCommand(
            UserInterface::ADMIN_ID,
            $email,
            RoleInterface::ID_ADMIN,
            $this->getShared(TranslatorInterface::class)->getCurrentLang()->getCode(),
            $password,
            true
        ));
        $this->commandBus->execute(new AssignRoleCommand(UserInterface::ADMIN_ID, RoleInterface::ID_GUEST));
    }
}
