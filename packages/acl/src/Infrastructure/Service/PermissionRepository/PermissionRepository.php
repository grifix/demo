<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Infrastructure\Service\PermissionRepository;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Shared\Ui\Common\PermissionDefinition;

class PermissionRepository implements PermissionRepositoryInterface
{
    protected ConfigInterface $config;

    protected KernelInterface $kernel;

    public function __construct(ConfigInterface $config, KernelInterface $kernel)
    {
        $this->config = $config;
        $this->kernel = $kernel;
    }

    public function getRoleDefaultPermissions(string $roleName): array
    {
        $result = [];
        foreach ($this->kernel->getModules() as $module) {
            $prefix = $module->getVendor() . '.' . $module->getName();
            $permissions = $this->config->get($prefix . '.acl.permissions', []);
            foreach ($permissions as $permission) {
                /** @var PermissionDefinition $permission */
                if (in_array($roleName, $permission->getRoles())) {
                    $result[] = $permission->getName();
                }
            }
        }
        return $result;
    }
}
