Feature: Assign role
  Background:
    Given there is an active user "Admin" with permission "grifix.acl.assignRole"
    And there is an active user "User"
    And there is an active user "Joe"
    And there is a role "Owner"
    And I am signed in as "Admin"

  Scenario: Happy path
    When I assign the role "Owner" to the user "Joe"
    Then the user "Joe" should have the role "Owner"

  Scenario: Access denied
    Given I am signed in as "User"
    When I assign the role "Owner" to the user "Joe"
    Then I should be denied access

  Scenario: Need authentication
    Given I am not signed in
    When I assign the role "Owner" to the user "Joe"
    Then I should get an error about needing authentication


