Feature: Sign in
  Background:
    Given I am not signed in
    And there is an active user "Mike" with email "mike@grifix.net" and password "QwYu12!f^Dic"

  Scenario: Happy path
    When I sign in with email "mike@grifix.net" and password "QwYu12!f^Dic"
    Then I should be signed in as "Mike"

  Scenario: Invalid password
    When I sign in with email "mike@grifix.net" and password "xxxx"
    Then I should get an error that the user is not exists

  Scenario: BruteForce attack
    When I sign in with email "mike@grifix.net" and password "xxxx" more times than maximum of sign-in limit
    Then I should get an error that the user is blocked

  Scenario: User does not exists
    When I sign in with email "nick@grifix.net" and password "xxxx"
    Then I should get an error that the user is not exists

  Scenario: Not confirmed email
    Given there is a user "Nick" with not confirmed email "nick@grifix.net" and password "QwYu12!f^Dic"
    When I sign in with email "nick@grifix.net" and password "QwYu12!f^Dic"
    Then I should get the error that email is not confirmed
