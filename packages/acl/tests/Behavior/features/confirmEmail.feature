Feature: Confirm email
  Background:
    Given there is user "Joe" with not confirmed email "joe@grifix.net"
    And I am not signed in

  Scenario: Happy path
    When I confirm email with user "Joe" token
    Then user "Joe" should have a confirmed email

  Scenario: Email already confirmed
    Given there is user "Nick" with confirmed email "nick@grifix.net"
    When I confirm email with user "Nick" token
    Then I should get an error that email is already confirmed

  Scenario: Wrong token
    When I confirm email with wrong token
    Then I should get an error that token is wrong
