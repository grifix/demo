Feature: Set password
  Background:
    Given there is an user "Joe" with a password "~e.UL.P8<<dqykX3"

  Scenario: Set password
    When I set password "g62cQ>!L4N)x+)G&" for the user "Joe"
    Then the user "Joe" should have a password "g62cQ>!L4N)x+)G&"

  Scenario: Set invalid password
    When I set password "123456" for the user "Joe"
    Then I should get an error that password is invalid

  Scenario: Set password for non existing user
    When I set password for non existing user
    Then I should get an error that user does not exist
