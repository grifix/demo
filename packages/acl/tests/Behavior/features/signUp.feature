Feature: Sign up
  Background:
    Given I am not signed in

  Scenario: Happy path
    When I sign up with email "mike@grifix.net"
    Then the letter with confirmation link should be sent to "mike@grifix.net"
    And there should be an user with not confirmed email "mike@grifix.net"
    
  Scenario: Email already used
    Given there is an user "Mike" with not confirmed email "mike@grifix.net"
    When I sign up with email "mike@grifix.net"
    Then I should get the error that user with that email already used
    
