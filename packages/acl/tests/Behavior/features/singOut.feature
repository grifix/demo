Feature: Sign out
  Background:
    Given there is an active user "Mike" with email "mike@grifix.net" and password "QwYu12!f^Dic"
    And I am signed in as "Mike"

  Scenario: Sign out
    When I sign out
    Then I should be signed out

  Scenario: Sign out when I'm not signed im
    Given I am not signed in
    When I sign out
    Then I should get an error that the user is not exists
