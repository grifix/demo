<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Test\Behavior\Context;

use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use PHPUnit\Framework\Assert;

class AssignRoleFeatureContext extends AbstractAclContext
{
    /**
     * @When I assign the role :role to the user :user
     */
    public function iAssignRole(string $roleAlias, string $userAlias): void
    {
        $this->client->assignRole(
            $this->helper->getUserFixture($userAlias)->getId(),
            $this->helper->getRoleFixture($roleAlias)->getId()
        );
    }

    /**
     * @Then the user :user should have the role :role
     */
    public function userShouldHaveRole(string $userAlias, string $roleAlias): void
    {
        $user = $this->helper->findUserByAlias(
            $userAlias,
            UserFilter::create()
                ->setWithRoles(true)
        );
        $roleFixture = $this->helper->getRoleFixture($roleAlias);
        $rolesIds = [];
        foreach ($user->getRoles() as $role) {
            $rolesIds[] = $role->getId();
        }
        Assert::assertTrue(in_array($roleFixture->getId(), $rolesIds));
    }
}
