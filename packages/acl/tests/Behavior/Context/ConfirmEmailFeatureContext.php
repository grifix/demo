<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Test\Behavior\Context;

use Grifix\Acl\Test\Common\Fixture\User\UserFixture;
use PHPUnit\Framework\Assert;

class ConfirmEmailFeatureContext extends AbstractAclContext
{
    /**
     * @Given there is user :user with not confirmed email :email
     */
    public function thereIsUserWithNotConfirmedEmail(string $userAlias, string $email): void
    {
        $this->helper->createUser(
            UserFixture::create()
                ->setEmail($email)
                ->setEmailConfirmed(false),
            $userAlias
        );
    }

    /**
     * @Given there is user :user with confirmed email :email
     */
    public function thereIsUserWithConfirmedEmail(string $userAlis, string $email): void
    {
        $this->helper->createUser(
            UserFixture::create()
                ->setEmail($email)
                ->setEmailConfirmed(true)
            ,
            $userAlis
        );
    }

    /**
     * @When I confirm email with user :userAlias token
     */
    public function iConfirmEmailWithUserToken(string $userAlias): void
    {
        $this->client->confirmEmail($this->helper->findUserByAlias($userAlias)->getToken());
    }

    /**
     * @Then user :user should have a confirmed email
     */
    public function userShouldHaveConfirmedEmail(string $userAlias): void
    {
        Assert::assertTrue($this->helper->findUserByAlias($userAlias)->isEmailConfirmed());
    }

    /**
     * @Then I should get an error that email is already confirmed
     */
    public function iShouldGetErrorThatEmailAlreadyConfirmed(): void
    {
        $this->assertHttpError(400, 'grifix.acl.emailAlreadyConfirmed');
    }

    /**
     * @Then I should get an error that token is wrong
     */
    public function iShouldGetErrorThatTokenIsWrong(): void
    {
        $this->assertHttpError(404, 'grifix.acl.userNotFound');
    }

    /**
     * @When /^I confirm email with wrong token$/
     */
    public function iConfirmEmailWithWrongToken()
    {
        $this->client->confirmEmail('wrong_token');
    }
}
