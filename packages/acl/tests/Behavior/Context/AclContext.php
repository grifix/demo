<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Test\Behavior\Context;

use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Acl\Test\Common\Fixture\Role\RoleFixture;
use Grifix\Acl\Test\Common\Fixture\User\UserFixture;

class AclContext extends AbstractAclContext
{
    /**
     * @Given there is an active user :user
     */
    public function thereIsActiveUser(string $userAlias): void
    {
        $this->helper->createUser(UserFixture::create()->setEmailConfirmed(true), $userAlias);
    }

    /**
     * @Given there is a role :role
     */
    public function thereIsRole(string $roleAlias): void
    {
        $this->helper->createRole(RoleFixture::create()->setName($roleAlias), $roleAlias);
    }

    /**
     * @Given there is an active user :user with permission :permission
     */
    public function thereIsActiveUserWithPermission(string $userAlias, string $permission): void
    {
        $this->createUserWithPermissions($userAlias, [$permission]);
    }

    /**
     * @Given there is an active user :user with permissions:
     */
    public function thereIsActiveUserWithPermissions(string $userAlias, string $permissions): void
    {
        $this->createUserWithPermissions($userAlias, yaml_parse($permissions));
    }


    private function createUserWithPermissions(string $userAlias, array $permissions = []): void
    {
        $role = RoleFixture::create()->setName($userAlias);
        $this->helper->createRole($role, $userAlias);
        foreach ($permissions as $permission) {
            $this->helper->grantPermission($userAlias, $permission);
        }
        $this->helper->createUser(
            UserFixture::create()
                ->setEmailConfirmed(true)
                ->setRoleId($role->getId()),
            $userAlias
        );
    }

    /**
     * @Given I am signed in as :userAlias
     */
    public function iAmSignedInAs(string $userAlias): void
    {
        if ($this->helper->getSignedInUser()->getId() !== UserInterface::GUEST_ID) {
            $this->client->signOut();
        }
        $userFixture = $this->helper->getUserFixture($userAlias);
        $this->client->signIn($userFixture->getEmail(), $userFixture->getPassword());
    }

    /**
     * @Given I am not signed in
     */
    public function iAmNotSignedIn()
    {
        if ($this->helper->getSignedInUser()->getId() !== UserInterface::GUEST_ID) {
            $this->client->signOut();
        }
    }

    /**
     * @Then I should be denied access
     */
    public function iShouldBeDeniedAccess(): void
    {
        $this->assertLastResponseCode(403);
        $this->assertLastResponseMessageKey('grifix.kit.msg_accessDenied');
    }

    /**
     * @Then I should get an error about needing authentication
     */
    public function iShouldGetAnErrorAboutNeedingAuthentication(): void
    {
        $this->assertLastResponseCode(401);
        $this->assertLastResponseMessageKey('grifix.kit.msg_unauthorized');
    }
}
