<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Test\Behavior\Context;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Acl\Test\Common\AclTestClient;
use Grifix\Acl\Test\Common\AclTestHelper;
use Grifix\Shared\Test\Behavior\Context\AbstractContext;

abstract class AbstractAclContext extends AbstractContext
{
    protected ?AclTestClient $client = null;

    protected ?AclTestHelper $helper = null;

    protected function beforeScenario(BeforeScenarioScope $scope): void
    {
        $this->client = $this->getShared(AclTestClient::class);
        $this->helper = $this->getShared(AclTestHelper::class);
    }
}
