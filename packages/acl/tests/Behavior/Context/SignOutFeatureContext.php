<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Test\Behavior\Context;

use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Kit\Test\Common\Assert\Assert;

class SignOutFeatureContext extends AbstractAclContext
{

    /**
     * @When /^I sign out$/
     */
    public function iSignOut()
    {
        $this->client->signOut();
    }

    /**
     * @Then /^I should be signed out$/
     */
    public function iShouldBeSignedOut()
    {
        Assert::assertEquals(UserInterface::GUEST_ID, $this->helper->getSignedInUser()->getId());
    }
}
