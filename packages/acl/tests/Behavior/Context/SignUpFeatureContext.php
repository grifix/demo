<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Test\Behavior\Context;

use Grifix\Acl\Application\Email\ConfirmationEmail\ConfirmationEmailMessage;
use Grifix\Acl\Application\Email\ConfirmationEmail\ConfirmationEmailVars;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Test\Common\Fixture\User\UserFixture;
use Grifix\Kit\Route\UrlMaker\UrlMakerInterface;
use PHPUnit\Framework\Assert;

class SignUpFeatureContext extends AbstractAclContext
{
    /**
     * @When I sign up with email :email
     */
    public function iSignUpWithEmail(string $email): void
    {
        $this->client->signUp($email);
    }

    /**
     * @Then the letter with confirmation link should be sent to :email
     */
    public function theLetterShouldBeSent(string $email): void
    {
        $user = $this->helper->findUser(UserFilter::create()->setEmail($email));
        $this->assertEmailWasSent($email);
        $message = $this->getSentEmailMessages($email)[0];
        Assert::assertEquals(ConfirmationEmailMessage::class, $message->getClass());
        /** @var ConfirmationEmailVars $content */
        $content = $message->getContent();
        Assert::assertInstanceOf(ConfirmationEmailVars::class, $content);
        Assert::assertEquals($email, $user->getEmail());
        Assert::assertEquals(
            $content->getConfirmationLink(),
            $this->getShared(UrlMakerInterface::class)
                ->makeUrl('grifix.acl.root.confirmEmail', ['token' => $user->getToken()])
        );
    }

    /**
     * @Then there should be an user with not confirmed email :email
     */
    public function thereShouldBeUserWithConfirmedEmail(string $email): void
    {
        $user = $this->helper->findUser(UserFilter::create()->setEmail($email));
        Assert::assertFalse($user->isEmailConfirmed());
    }

    /**
     * @Given there is an user :user with not confirmed email :email
     */
    public function thereIsUserWithNotConfirmedEmail(string $userAlias, string $email): void
    {
        $this->helper->createUser(
            UserFixture::create()
                ->setEmail($email)
                ->setEmailConfirmed(true),
            $userAlias
        );
    }

    /**
     * @Then I should get the error that user with that email already used
     */
    public function assertEmailAlreadyUsedException()
    {
        $this->assertHttpError(400, 'grifix.acl.userEmailAlreadyExists');
    }
}
