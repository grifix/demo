<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Acl\Test\Behavior\Context;

use Grifix\Acl\Domain\User\Exception\EmailIsNotConfirmedException;
use Grifix\Acl\Test\Common\Fixture\User\UserFixture;
use PHPUnit\Framework\Assert;

class SignInFeatureContext extends AbstractAclContext
{

    /**
     * @Given there is a user :user with not confirmed email :email and password :password
     */
    public function thereIsUserWithNonConfirmedEmail(string $userAlis, string $email, string $password): void
    {
        $this->helper->createUser(
            UserFixture::create()
                ->setEmail($email)
                ->setPassword($password)
                ->setEmailConfirmed(false),
            $userAlis
        );
    }

    /**
     * @Given there is an active user :user with email :email and password :password
     */
    public function thereIsUserWithEmailAndPassword(string $userAlias, string $email, string $password): void
    {
        $this->helper->createUser(
            UserFixture::create()
                ->setEmail($email)
                ->setPassword($password)
                ->setEmailConfirmed(true),
            $userAlias
        );
    }

    /**
     * @When I sign in with email :email and password :password
     */
    public function iSignInWithEmailAndPassword(string $email, string $password): void
    {
        $this->client->signIn($email, $password);
    }

    /**
     * @Then I should be signed in as :user
     */
    public function iShouldBeSignedInAs(string $userAlias): void
    {
        $signedInUser = $this->helper->getSignedInUser();
        $expectedUser = $this->helper->getUserFixture($userAlias);
        Assert::assertEquals($expectedUser->getId(), $signedInUser->getId());
    }

    /**
     * @When I sign in with email :email and password :password more times than maximum of sign-in limit
     */
    public function iSignInMoreThanMaximum(string $email, string $password): void
    {
        $maxAttempts = $this->getConfig('grifix.acl.signIn.maxAttempts');
        for ($i = 0; $i < $maxAttempts + 1; $i++) {
            $this->client->signIn($email, $password);
        }
    }

    /**
     * @Then I should get an error that the user is blocked
     */
    public function assertUserIsBlockedException()
    {
        $this->assertHttpError(400, 'grifix.acl.userIsBlocked');
    }

    /**
     * @Then I should get an error that the user is not exists
     */
    public function assertUserNotExistsException()
    {
        $this->assertHttpError(404, 'grifix.acl.userNotFound');
    }

    /**
     * @Then I should get the error that email is not confirmed
     */
    public function assertEmailNotConfirmedException(): void
    {
        $this->assertLastResponseCode(400);
        Assert::assertEquals(EmailIsNotConfirmedException::class, $this->getLastResponseArray()['previous']['class']);
    }
}
