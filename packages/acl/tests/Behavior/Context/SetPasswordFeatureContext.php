<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Acl\Test\Behavior\Context;


use Behat\Behat\Tester\Exception\PendingException;
use Grifix\Acl\Application\Command\User\SingIn\SignInCommand;
use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Acl\Test\Common\Fixture\User\UserFixture;
use Grifix\Acl\Ui\Cli\Command\SetPasswordCmd;
use Grifix\Kit\Test\Common\Assert\Assert;
use Throwable;

class SetPasswordFeatureContext extends AbstractAclContext
{

    /**
     * @Given /^there is an user "([^"]*)" with a password "([^"]*)"$/
     */
    public function thereIsAnUserWithAPassword(string $userAlias, string $password)
    {
        $this->helper->createUser(UserFixture::create()->setPassword($password), $userAlias);
    }

    /**
     * @When I set password :password for the user :userAlias
     */
    public function iSetPasswordForTheUser(string $password, string $userAlias)
    {
        $userFixture = $this->helper->getUserFixture($userAlias);
        $this->performAction($userFixture->getEmail(), $password);
    }

    /**
     * @Then the user :userAlias should have a password :password
     */
    public function theUserShouldHaveAPassword($userAlias, $password)
    {
        $userFixture = $this->helper->getUserFixture($userAlias);
        $ip = '127.0.0.1';
        $sessionId = uniqid('test_session');
        try {
            $this->executeCommand(new SignInCommand(
                $userFixture->getId(),
                $password,
                $ip,
                $sessionId
            ));
        } catch (Throwable $exception) {

        }

        $loggedUser = $this->executeQuery(new GetSignedInUserQuery($sessionId));
        Assert::assertEquals($userFixture->getId(), $loggedUser->getId());
    }

    /**
     * @Then /^I should get an error that password is invalid$/
     */
    public function iShouldGetAnErrorThatPasswordIsInvalid()
    {
        Assert::assertEquals($this->cliClient->getLastCode(), SetPasswordCmd::CODE_INVALID_PASSWORD);
    }

    /**
     * @When /^I set password for non existing user$/
     */
    public function iSetPasswordForNonExistingUser()
    {
        $this->performAction('non_existing_user@non_existing_domain.com', 'g]T&J7M9t{Nx8AxP');
    }

    private function performAction($email, $password): void
    {
        $this->cliClient->executeCommand(
            SetPasswordCmd::NAME,
            [
                SetPasswordCmd::ARG_EMAIL => $email,
                SetPasswordCmd::ARG_PASSWORD => $password
            ]
        );
    }

    /**
     * @Then /^I should get an error that user does not exist$/
     */
    public function iShouldGetAnErrorThatUserDoesNotExist()
    {
        Assert::assertEquals($this->cliClient->getLastCode(), SetPasswordCmd::CODE_USER_NOT_EXIST);
    }
}
