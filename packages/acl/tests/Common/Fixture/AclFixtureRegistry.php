<?php declare(strict_types=1);

namespace Grifix\Acl\Test\Common\Fixture;

use Grifix\Acl\Test\Common\Fixture\Role\RoleFixture;
use Grifix\Acl\Test\Common\Fixture\Role\RoleFixtureMother;
use Grifix\Acl\Test\Common\Fixture\User\UserFixture;
use Grifix\Acl\Test\Common\Fixture\User\UserFixtureMother;
use Grifix\Shared\Test\Common\AbstractFixtureRegistry;

class AclFixtureRegistry extends AbstractFixtureRegistry
{
    protected UserFixtureMother $userFixtureMother;

    protected RoleFixtureMother $roleFixtureMother;

    public function __construct(UserFixtureMother $userFixturePersister, RoleFixtureMother $roleFixturePersister)
    {
        $this->userFixtureMother = $userFixturePersister;
        $this->roleFixtureMother = $roleFixturePersister;
    }

    public function addRoleFixture(string $alias, RoleFixture $fixture): void
    {
        if (null === $fixture->getId()) {
            $this->roleFixtureMother->create($fixture);
        }
        $this->addFixture($alias, $fixture);
    }

    public function getRoleFixture(string $alias): RoleFixture
    {
        return $this->getFixture($alias, RoleFixture::class);
    }

    public function addUserFixture(string $alias, UserFixture $fixture): void
    {
        if (null === $fixture->getId()) {
            $this->userFixtureMother->create($fixture);
        }
        $this->addFixture($alias, $fixture);
    }

    public function getUserFixture(string $alias): UserFixture
    {
        return $this->getFixture($alias, UserFixture::class);
    }
}
