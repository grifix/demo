<?php declare(strict_types=1);

namespace Grifix\Acl\Test\Common\Fixture\User;

use Grifix\Acl\Application\Command\User\CreateUser\CreateUserCommand;
use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Kit\Fixture\AbstractFixtureMother;
use Grifix\Kit\Intl\TranslatorInterface;

class UserFixtureMother extends AbstractFixtureMother
{
    public function create(UserFixture $fixture = null): UserFixture
    {
        null !== $fixture ?: $fixture = new UserFixture();
        $this->prepareFixture($fixture);
        $this->executeCommand(
            new CreateUserCommand(
                $fixture->getId(),
                $fixture->getEmail(),
                $fixture->getRoleId(),
                $fixture->getLangCode(),
                $fixture->getPassword(),
                $fixture->getEmailConfirmed()
            )
        );

        return $fixture;
    }

    protected function prepareFixture(UserFixture $fixture): void
    {
        null !== $fixture->getId() ?: $fixture->setId($this->generateId());
        null !== $fixture->getEmail() ?: $fixture->setEmail($this->faker->setUnique()->fakeEmail());
        null !== $fixture->getPassword() ?: $fixture->setPassword('msz$fw=4%3Vqw2A3');
        null !== $fixture->getRoleId() ?: $fixture->setRoleId(RoleInterface::ID_USER);
        null !== $fixture->getLangCode() ?: $fixture->setLangCode(
            $this->iocContainer->get(TranslatorInterface::class)->getCurrentLang()->getCode()
        );
        null !== $fixture->getEmailConfirmed() ?: $fixture->setEmailConfirmed(true);
    }
}
