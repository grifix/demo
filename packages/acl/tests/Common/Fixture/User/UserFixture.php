<?php declare(strict_types=1);

namespace Grifix\Acl\Test\Common\Fixture\User;

class UserFixture
{
    protected ?string $id = null;

    protected ?string $email = null;

    protected ?string $password = null;

    protected ?string $roleId = null;

    protected ?string $langCode = null;

    protected ?bool $emailConfirmed = null;

    public static function create(): self
    {
        return new self();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): UserFixture
    {
        $this->id = $id;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): UserFixture
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): UserFixture
    {
        $this->password = $password;
        return $this;
    }

    public function getRoleId(): ?string
    {
        return $this->roleId;
    }

    public function setRoleId(?string $roleId): UserFixture
    {
        $this->roleId = $roleId;
        return $this;
    }

    public function getLangCode(): ?string
    {
        return $this->langCode;
    }

    public function setLangCode(?string $langCode): UserFixture
    {
        $this->langCode = $langCode;
        return $this;
    }

    public function getEmailConfirmed(): ?bool
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed(?bool $emailConfirmed): UserFixture
    {
        $this->emailConfirmed = $emailConfirmed;
        return $this;
    }
}
