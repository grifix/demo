<?php declare(strict_types=1);

namespace Grifix\Acl\Test\Common\Fixture\Role;

class RoleFixture
{
    protected ?string $id = null;

    protected ?string $name = null;

    public static function create(): self
    {
        return new self();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): RoleFixture
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): RoleFixture
    {
        $this->name = $name;
        return $this;
    }
}
