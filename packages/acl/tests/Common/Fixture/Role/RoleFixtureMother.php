<?php declare(strict_types = 1);

namespace Grifix\Acl\Test\Common\Fixture\Role;

use Grifix\Acl\Application\Command\Role\CreateRole\CreateRoleCommand;
use Grifix\Kit\Fixture\AbstractFixtureMother;

class RoleFixtureMother extends AbstractFixtureMother
{
    public function create(?RoleFixture $fixture): RoleFixture
    {
        null !== $fixture ?: $fixture = new RoleFixture();
        $this->prepareFixture($fixture);
        $this->executeCommand(new CreateRoleCommand($fixture->getId(), $fixture->getName()));
        return $fixture;
    }

    protected function prepareFixture(RoleFixture $fixture)
    {
        null !== $fixture->getId() ?: $fixture->setId($this->generateId());
        null !== $fixture->getName() ?: $fixture->setName(uniqid($this->faker->setUnique()->fakeWord().'_'));
    }
}
