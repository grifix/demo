<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Acl\Test\Common;

use Grifix\Acl\Application\Command\Role\GrantPermission\GrantPermissionCommand;
use Grifix\Acl\Application\Command\User\ActivateUser\ActivateUserCommand;
use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Projection\UserProjection\UserFilter;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQuery;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQueryResult;
use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Acl\Test\Common\Fixture\AclFixtureRegistry;
use Grifix\Acl\Test\Common\Fixture\Role\RoleFixture;
use Grifix\Acl\Test\Common\Fixture\Role\RoleFixtureMother;
use Grifix\Acl\Test\Common\Fixture\User\UserFixture;
use Grifix\Acl\Test\Common\Fixture\User\UserFixtureMother;
use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Kit\Session\SessionInterface;

class AclTestHelper
{
    protected QueryBusInterface $queryBus;

    protected CommandBusInterface $commandBus;

    protected AclFixtureRegistry $fixtureRegistry;

    protected UserFixtureMother $userFixtureMother;

    protected RoleFixtureMother $roleFixtureMother;

    protected SessionInterface $session;

    public function __construct(
        QueryBusInterface $queryBus,
        CommandBusInterface $commandBus,
        AclFixtureRegistry $fixtureRegistry,
        UserFixtureMother $userFixtureMother,
        RoleFixtureMother $roleFixtureMother,
        SessionInterface $session
    ) {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->fixtureRegistry = $fixtureRegistry;
        $this->userFixtureMother = $userFixtureMother;
        $this->roleFixtureMother = $roleFixtureMother;
        $this->session = $session;
    }

    public function createUser(UserFixture $fixture, ?string $alias = null): void
    {
        $fixture = $this->userFixtureMother->create($fixture);
        if ($alias) {
            $this->fixtureRegistry->addUserFixture($alias, $fixture);
        }
    }

    public function createRole(RoleFixture $fixture, ?string $alias = null): void
    {
        $fixture = $this->roleFixtureMother->create($fixture);
        if ($alias) {
            $this->fixtureRegistry->addRoleFixture($alias, $fixture);
        }
    }

    public function activateUser(string $userAlias): void
    {
        $this->commandBus->execute(
            new ActivateUserCommand($this->fixtureRegistry->getUserFixture($userAlias)->getId())
        );
    }

    public function getUserFixture(string $alias): UserFixture
    {
        return $this->fixtureRegistry->getUserFixture($alias);
    }

    public function getRoleFixture(string $alias): RoleFixture
    {
        return $this->fixtureRegistry->getRoleFixture($alias);
    }

    public function getSignedInUser(): ?UserDto
    {
        return $this->queryBus->execute(new GetSignedInUserQuery($this->session->getId()));
    }

    public function grantPermission(string $roleAlias, string $permission): void
    {
        $this->commandBus->execute(new GrantPermissionCommand(
            $this->fixtureRegistry->getRoleFixture($roleAlias)->getId(),
            $permission
        ));
    }

    public function findUser(UserFilter $filter): ?UserDto
    {
        /** @var FindUsersQueryResult $result */
        $result = $this->queryBus->execute(new FindUsersQuery($filter));
        if ($result->getUsers()) {
            return $result->getUsers()[0];
        }
        return null;
    }

    public function findUserByAlias(string $alias, ?UserFilter $filter = null): ?UserDto
    {
        $filter = $filter ?? UserFilter::create();
        $filter->setId($this->getUserFixture($alias)->getId());
        return $this->findUser($filter);
    }
}
