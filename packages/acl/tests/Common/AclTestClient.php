<?php declare(strict_types = 1);

namespace Grifix\Acl\Test\Common;

use Grifix\Acl\Ui\Http\Action\AssignRoleActionHandler;
use Grifix\Acl\Ui\Http\Action\ChangePasswordActionHandler;
use Grifix\Acl\Ui\Http\Action\ConfirmEmailActionHandler;
use Grifix\Acl\Ui\Http\Action\GetSignedInUserActionHandler;
use Grifix\Acl\Ui\Http\Action\GetUsersActionHandler;
use Grifix\Acl\Ui\Http\Action\SignInActionHandler;
use Grifix\Acl\Ui\Http\Action\SignOutActionHandler;
use Grifix\Acl\Ui\Http\Action\SignUpActionHandler;
use Grifix\Acl\Ui\Common\RequestValidator\AssignRoleRequestValidator;
use Grifix\Acl\Ui\Common\RequestValidator\ChangePasswordRequestValidator;
use Grifix\Acl\Ui\Common\RequestValidator\ConfirmEmailRequestValidator;
use Grifix\Acl\Ui\Common\RequestValidator\SignInRequestValidator;
use Grifix\Acl\Ui\Common\RequestValidator\SignUpRequestValidator;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Shared\Test\Common\HttpClient;

class AclTestClient
{
    protected HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function signIn(string $email, string $password): ResponseInterface
    {
        return $this->httpClient->postAction(
            SignInActionHandler::getAlias(),
            [
                SignInRequestValidator::EMAIL => $email,
                SignInRequestValidator::PASSWORD => $password
            ]
        );
    }

    public function assignRole(string $userId, string $roleId): ResponseInterface
    {
        return $this->httpClient->postAction(
            AssignRoleActionHandler::getAlias(),
            [
                AssignRoleRequestValidator::USER_ID => $userId,
                AssignRoleRequestValidator::ROLE_ID => $roleId
            ]
        );
    }

    public function changePassword(
        string $oldPassword,
        string $newPassword,
        string $newPasswordAgain
    ): ResponseInterface {
        return $this->httpClient->postAction(
            ChangePasswordActionHandler::getAlias(),
            [
                ChangePasswordRequestValidator::OLD_PASSWORD => $oldPassword,
                ChangePasswordRequestValidator::NEW_PASSWORD => $newPassword,
                ChangePasswordRequestValidator::NEW_PASSWORD_AGAIN => $newPasswordAgain
            ]
        );
    }

    public function confirmEmail(string $token): ResponseInterface
    {
        return $this->httpClient->postAction(
            ConfirmEmailActionHandler::getAlias(),
            [
                ConfirmEmailRequestValidator::TOKEN => $token
            ]
        );
    }

    public function getSignedInUser(): ResponseInterface
    {
        return $this->httpClient->getAction(
            GetSignedInUserActionHandler::getAlias()
        );
    }

    public function getUsers(int $page = null, int $rows = null): ResponseInterface
    {
        return $this->httpClient->getAction(
            GetUsersActionHandler::getAlias(),
            [
                GetUsersActionHandler::PARAM_PAGE => $page,
                GetUsersActionHandler::PARAM_ROWS => $rows
            ]
        );
    }

    public function signOut(): ResponseInterface
    {
        return $this->httpClient->postAction(
            SignOutActionHandler::getAlias()
        );
    }

    public function signUp(string $email): ResponseInterface
    {
        return $this->httpClient->postAction(
            SignUpActionHandler::getAlias(),
            [
                SignUpRequestValidator::EMAIL => $email
            ]
        );
    }
}
