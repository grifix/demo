<?php

declare(strict_types = 1);

namespace {

    use Grifix\Acl\Application\Email\ConfirmationEmail\ConfirmationEmailVars;
    use Grifix\Kit\View\ViewInterface;

    /** @var ViewInterface $this */
    $this->inherits('grifix.kit.email.lyt.default');

    /** @var ConfirmationEmailVars $content */
    $content = $this->getVar('content');

    ?>
    <?php $this->startBlock('content') ?>
        <p>Для подтвеждения регистрации нажмите <a href="<?= $content->getConfirmationLink() ?>">подтвердить</a></p>
    <?php $this->endBlock() ?>
    <?php
}
