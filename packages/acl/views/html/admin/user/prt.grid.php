<?php
declare(strict_types=1);
/**@var $this Grifix\Kit\View\ViewInterface */
?>

<div class="<?=$this->getWidgetClass()?>">
    <table data-role="grid">
        <thead>
            <tr>

            </tr>
        </thead>
    </table>
    <div data-role="toolbar">
        <div data-role="buttons">

        </div>
    </div>
</div>
