/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Form.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        grifix.kit.Form,
        {
            redirect:'/',
            target:{
                url: gfx.makeActionUrl('grifix.acl.signIn'),
                method: 'post'
            }
        },
        {
            init: function () {
                var that = this;
                that.on('success', function () {
                    window.location = that.cfg.redirect;
                });
                grifix.kit.Form.prototype.init.call(that);
            }
        }
    );
});
