<?php

declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\Helper\InputViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */
    /**@var InputViewHelper $inputHelper */
    $inputHelper = $this->getHelper(InputViewHelper::class);
    ?>
    <form class="panel <?= $this->makeCssClass() ?> <?= $this->getWidgetClass() ?>"
          data-redirect="<?= $this->getTextVar('redirect', '/') ?>">
        <div class="panel-body">
            <?= $inputHelper->email()
                ->setName('email')
                ->setPlaceholder($this->translate('grifix.kit.email'))
                ->setRequired()
                ->build() ?>
            <?= $inputHelper->password()
                ->setName('password')
                ->setPlaceholder($this->translate('grifix.kit.password'))
                ->setRequired()
                ->build()
            ?>
            <button data-role="submit" class="button"><?= $this->translate('grifix.acl.signIn') ?></button>
        </div>
    </form>
    <?php
}
