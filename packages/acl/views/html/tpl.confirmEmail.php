<?php

declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    $this->inherits('grifix.kit.{skin}.lyt.default');
    ?>

    <?php $this->startBlock('content') ?>
    <div class="<?= $this->makeCssClass() ?>">
        confirmEmail
    </div>
    <?php $this->endBlock() ?>
    <?php
}
