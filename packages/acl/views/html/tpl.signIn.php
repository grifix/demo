<?php

declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    $this->inherits('grifix.kit.{skin}.lyt.default');
    $this->addJs('{src}/grifix/acl/views/{skin}/SignIn.js');
    $this->addCss('{src}/grifix/acl/views/{skin}/tpl.signIn.css');

    ?>

    <?php $this->startBlock('content') ?>
    <div class="<?= $this->makeCssClass() ?>">
        <?= $this->renderPartial('grifix.acl.{skin}.prt.signIn') ?>
    </div>
    <?php $this->endBlock() ?>
    <?php
}
