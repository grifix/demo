<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\Helper\ArrayViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var $this ViewInterface */
    echo json_encode($this->getHelper(ArrayViewHelper::class)->toArray($this->getVars()));
}
