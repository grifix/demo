<?php
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Domain\Translation;

use Grifix\Shared\Domain\Translation\TranslationOutsideInterface;
use Grifix\Kit\Intl\TranslatorInterface;

class TranslationOutside implements TranslationOutsideInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * TranslationInfrastructure constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getLangCodes(): array
    {
        $result = [];
        foreach ($this->translator->getLangs() as $lang) {
            $result[] = $lang->getCode();
        }

        return $result;
    }
}
