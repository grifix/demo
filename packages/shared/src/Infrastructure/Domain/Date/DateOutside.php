<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Domain\Date;

use Grifix\Kit\Type\DateTime\DateTimeFactoryInterface;
use Grifix\Kit\Type\DateTime\DateTimeInterface;
use Grifix\Shared\Application\Common\Clock\ClockInterface;
use Grifix\Shared\Domain\DateTime\DateOutsideInterface;

class DateOutside implements DateOutsideInterface
{
    protected DateTimeFactoryInterface $dateTimeFactory;

    protected ClockInterface $clock;

    public function __construct(DateTimeFactoryInterface $dateTimeFactory, ClockInterface $clockService)
    {
        $this->dateTimeFactory = $dateTimeFactory;
        $this->clock = $clockService;
    }

    public function createDateTime(?int $timestamp): DateTimeInterface
    {
        $timestamp = $timestamp ?? $this->clock->getTime();
        return $this->dateTimeFactory->createFromTimeStamp($timestamp);
    }
}
