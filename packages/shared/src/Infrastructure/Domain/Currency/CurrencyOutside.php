<?php
declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Domain\Currency;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Shared\Domain\Currency\CurrencyOutsideInterface;

class CurrencyOutside implements CurrencyOutsideInterface
{

    /** @var ConfigInterface */
    protected $config;

    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
    }


    public function isAvailable(string $code): bool
    {
        return $this->config->get('grifix.shared.currency.' . $code, false);
    }
}
