<?php
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Domain\Uuid;

use Grifix\Shared\Domain\Uuid\UuidOutsideInterface;
use Ramsey\Uuid\Uuid;

class UuidOutside implements UuidOutsideInterface
{
    public function isValidUuid(string $uuid): bool
    {
        return Uuid::isValid($uuid);
    }
}
