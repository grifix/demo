<?php

declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Domain\File;

use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Helper\MimeHelperInterface;
use Grifix\Kit\PathInfo\PathInfoFactoryInterface;
use Grifix\Kit\PathInfo\PathInfoInterface;
use Grifix\Shared\Domain\File\Infrastructure\Exception\CannotUploadFileException;
use Grifix\Shared\Domain\File\Infrastructure\FileOutsideInterface;

class FileOutside implements FileOutsideInterface
{
    protected array $cache = [];

    public function __construct(
        protected FilesystemInterface $filesystem,
        protected PathInfoFactoryInterface $pathInfoFactory,
        protected MimeHelperInterface $mimeHelper,
        protected FilesystemHelperInterface $filesystemHelper
    ) {
    }

    protected function createPathInfo(string $path): PathInfoInterface
    {
        if (!isset($this->cache[$path])) {
            $this->cache[$path] = $this->pathInfoFactory->createPathInfo($path);
        }
        return $this->cache[$path];
    }

    public function getMimeType(string $path): string
    {
        return $this->createPathInfo($path)->getMimeType();
    }

    public function getFileSize(string $path): int
    {
        return $this->createPathInfo($path)->getSize();
    }

    public function sourceFileExists(string $path): bool
    {
        return file_exists($path);
    }

    public function fileExists(string $path): bool
    {
        return $this->filesystem->has($path);
    }

    public function isImage(string $path): bool
    {
        return $this->createPathInfo($path)->isImage();
    }

    public function getImageWidth(string $path): int
    {
        return $this->createPathInfo($path)->getImageWidth();
    }

    public function getImageHeight(string $path): int
    {
        return $this->createPathInfo($path)->getImageHeight();
    }

    public function upload(string $sourcePath, string $destinationPath): void
    {
        $resource = fopen($sourcePath, 'r');
        if ($this->filesystem->has($destinationPath)) {
            $this->filesystem->delete($destinationPath);
        }
        if (!$this->filesystem->writeStream($destinationPath, $resource)) {
            throw new CannotUploadFileException($sourcePath, $destinationPath);
        }
        fclose($resource);
    }

    public function delete(string $path): void
    {
        $this->filesystem->delete($path);
    }

    public function makeFileName(string $sourcePath, string $destinationName): string
    {
        return sprintf('%s.%s', $destinationName, $this->mimeHelper->getExtension($this->getMimeType($sourcePath)));
    }

    public function extractName(string $path):string{
        return $this->filesystemHelper->getPathInfo($path)->getFilename();
    }

    public function getExtension(string $mimeType): string
    {
        return $this->mimeHelper->getExtension($mimeType);
    }
}
