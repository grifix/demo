<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Grifix;

use Grifix\Kit\Alias;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Event\Bus\EventBusInterface;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Event\SubscribersStarter\EventSubscribersStarter;
use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Grifix\Kit\Route\RouteCollectionInterface;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Ui\Action\Event\BeforeDispatchActionEvent;
use Grifix\Kit\View\Asset\Event\CombineAssetEvent;
use Grifix\Kit\View\Event\BeforeCreateViewEvent;
use Grifix\Shared\Application\Client\Acl\AclClientInterface;
use Grifix\Shared\Infrastructure\Grifix\Exception\AccessToActionDeniedException;
use Grifix\Shared\Infrastructure\Grifix\Exception\AccessToViewDeniedException;

class Bootstrap implements ModuleCommandInterface
{
    use ModuleClassTrait;

    protected EventBusInterface $eventBus;

    protected AclClientInterface $aclClient;

    protected EventStoreInterface $eventStore;


    protected function init()
    {
        $this->eventBus = $this->getShared(EventBusInterface::class);
        $this->aclClient = $this->getShared(AclClientInterface::class);
        $this->eventStore = $this->getShared(EventStoreInterface::class);
    }

    /**
     * @return void
     */
    public function run(): void
    {
        $this->init();
        $this->combineAssets();
        $this->checkAccessToAction();
        $this->checkAccessToView();
        $this->declareEventSubscribersStarter();
    }

    /**
     * @param $path
     *
     * @return array
     * @internal
     *
     * Полчает наймпсейс из пути к css или js файла
     *
     */
    public function getNamespace($path): array
    {
        $info = pathinfo($path);
        $arr = explode(DIRECTORY_SEPARATOR, str_replace($this->getShared(Alias::ROOT_DIR), '', $info['dirname']));
        $vendor = $arr[2];
        $module = $arr[3];
        $arr = array_slice($arr, 6);
        $result = [
            $vendor,
            $module,
        ];
        foreach ($arr as $v) {
            $result[] = $v;
        }
        $result[] = str_replace('.', '_', $info['filename']);

        return $result;
    }

    protected function declareEventSubscribersStarter(): void
    {
        $this->eventStore->declareListener(EventSubscribersStarter::LISTENER_ID);
    }

    protected function combineAssets()
    {
        $that = $this;
        $this->getShared(EventBusInterface::class)->addListener(
            function (CombineAssetEvent $event) use ($that) {
                /**@var $this CombineAssetEvent */
                $info = pathinfo($event->getAssetPath());
                $content = $event->getAssetContent();
                if ($info['extension'] == 'css') {
                    $cssPrefix = '.' . implode('_', $that->getNamespace($event->getAssetPath()));
                    $content = str_replace('._', $cssPrefix, $content);
                }

                if ($info['extension'] == 'js') {
                    $widgetName = '"' . implode('_', $that->getNamespace($event->getAssetPath())) . '"';
                    $constructorName = '"' . implode('.', $that->getNamespace($event->getAssetPath())) . '"';
                    $content = str_replace('gfx.createWidgetName()', $widgetName, $content);
                    $content = str_replace('gfx.createConstructorName()', $constructorName, $content);
                }

                $content = preg_replace_callback("/php\(\/\*(.*)\*\/\)/", function ($matches) use ($that) {
                    /** @noinspection PhpUnusedLocalVariableInspection */
                    $t = $translate = function (
                        $key,
                        $vars = [],
                        $quantity = 1,
                        $case = LangInterface::CASE_NOMINATIVE,
                        $form = LangInterface::FORM_SINGULAR
                    ) use ($that) {
                        return $that->getShared(TranslatorInterface::class)->translate(
                            $key,
                            $vars,
                            $quantity,
                            $case,
                            $form
                        );
                    };

                    /** @noinspection PhpUnusedLocalVariableInspection */
                    $cfg = function (string $key, $defaultValue = null) use ($that) {
                        return $that->getShared(ConfigInterface::class)->get($key, $defaultValue);
                    };

                    /** @noinspection PhpUnusedLocalVariableInspection */
                    $route = function (string $route, array $params = []) use ($that) {
                        /**@var RouteInterface $route */
                        $route = $that->getShared(RouteCollectionInterface::class)->offsetGet($route);
                        if ($route) {
                            return $route->makeUrl($params);
                        }
                        return null;
                    };

                    /** @noinspection PhpUnusedLocalVariableInspection */
                    $langCode = $this->getShared(TranslatorInterface::class)->getCurrentLang()->getCode();

                    return eval("return json_encode(" . $matches[1] . ');');
                }, $content);

                //TODO check this case and try to remove this setter
                $event->setAssetContent($content);
            }
        );
    }

    /**
     * Check acl access on every reques
     *
     * @return void
     */
    protected function checkAccessToAction()
    {
        $that = $this;
        $this->eventBus->addListener(
            function (BeforeDispatchActionEvent $event) use ($that) {
                if (!$that->aclClient->hasAccessToAction(
                    $that->getShared(SessionInterface::class)->getId(),
                    $event->getAction()
                )) {
                    throw new AccessToActionDeniedException($event->getAction());
                }
            }
        );
    }

    protected function checkAccessToView()
    {
        if (php_sapi_name() === "cli") {
            return;
        }
        $that = $this;
        $this->eventBus->addListener(
            function (BeforeCreateViewEvent $event) use ($that) {
                if (!$that->aclClient->hasAccessToView(
                    $that->getShared(SessionInterface::class)->getId(),
                    $event->getPath()
                )) {
                    throw new AccessToViewDeniedException($event->getPath());
                }
            }
        );
    }
}
