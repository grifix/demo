<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Grifix;

use DateTimeImmutable;
use Grifix\Kit\Alias;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Http\CookieInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Intl\Locale\LocaleFactoryInterface;
use Grifix\Kit\Intl\Locale\LocaleInterface;
use Grifix\Kit\Intl\Locale\LocaleRepositoryInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\Module\ModuleClassInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Middleware\AbstractMiddleware;
use Grifix\Kit\Route\Exception\NoRouteMatchException;
use Grifix\Kit\Route\RouteCollectionFactoryInterface;
use Grifix\Kit\Route\RouteCollectionInterface;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\Route\UrlMaker\UrlMakerInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\View\Skin\SkinFactory;
use Grifix\Kit\View\ViewFactoryInterface;
use Grifix\Shared\Application\Client\Acl\AclClientInterface;
use Grifix\Shared\Infrastructure\Grifix\Exception\AccessToRouteDeniedException;
use Locale;
use Psr\Http\Message\RequestInterface;

/**
 * Class FirstMiddleware
 *
 * @category Grifix
 * @package  Grifix\Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Middleware extends AbstractMiddleware implements ModuleClassInterface
{
    use ModuleClassTrait;

    const SKIN_VARIABLE_NAME = 'grifix_kit_view_default_skin';
    const LOCALE_VARIABLE_NAME = 'grifix_kit_intl_locale';

    /**
     * {@inheritdoc}
     */
    protected function before(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $this->setUserTimezone();

        $response = $this->defineSkin($request, $response);
        $currentRoute = $this->detectCurrentRoute($request);

        $this->detectLocale($request, $currentRoute);
        if ($this->defaultLocaleExistsInUri($request)) {
            return $response->withRedirect(
                $this->getShared(UrlMakerInterface::class)->makeUrl(
                    $currentRoute->getAlias(),
                    $currentRoute->getParams()
                )
            );
        }

        if ($currentRoute) {
            $this->checkRouteAccess($currentRoute, $request, $response);
            return $currentRoute->handle($request, $response);
        }
        throw new NoRouteMatchException($request, $response);
    }

    protected function defaultLocaleExistsInUri(RequestInterface $request): bool
    {
        return false !== strpos(
            $request->getUri()->getPath(),
            $this->getShared(LocaleRepositoryInterface::class)->getDefaultLocale()->getCode()
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function after(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $response = $this->setSessionCookie($response);
        return $response;
    }

    protected function setSessionCookie(ResponseInterface $response)
    {
        $session = $this->getShared(SessionInterface::class);
        $expiresAt = new DateTimeImmutable(sprintf('+ %s seconds', $session->getCookieParams()->getLifeTime()));
        return $response->withCookie(
            $session->getName(),
            $session->getId(),
            '/',
            $expiresAt
        );
    }

    protected function setUserTimezone(): void
    {
        $session = $this->getShared(SessionInterface::class);
        $user = $this->getShared(AclClientInterface::class)->getSignedInUser($session->getId());
        date_default_timezone_set($user->getTimeZone());
    }

    protected function checkRouteAccess(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): void {
        $aclService = $this->getShared(AclClientInterface::class);
        $session = $this->getShared(SessionInterface::class);
        if (!$aclService->hasAccessToRoute($session->getId(), $route->getAlias())) {
            throw new AccessToRouteDeniedException($route->getAlias());
        }
    }

    protected function detectCurrentRoute(ServerRequestInterface $request): ?RouteInterface
    {
        $routeCollection = $this->getIoc()->get(RouteCollectionFactoryInterface::class)->createFromConfig();
        $currentRoute = $routeCollection->match($request);
        $this->setShared(RouteCollectionInterface::class, $routeCollection);
        $this->setShared(Alias::CURRENT_ROUTE, $currentRoute);
        return $currentRoute;
    }

    protected function detectLocale(ServerRequestInterface $request, RouteInterface $route = null)
    {
        $enabledLocales = $this->getShared(ConfigInterface::class)->get('grifix.kit.intl.config.enabledLocales');
        $localeCode = $enabledLocales[0];
        $possibleLocales = [
            Locale::acceptFromHttp($request->getServerParam(ServerRequestInterface::HTTP_ACCEPT_LANGUAGE, '')),
            $request->getCookieParam(self::LOCALE_VARIABLE_NAME),
            $this->getShared(SessionInterface::class)->get(self::LOCALE_VARIABLE_NAME),
            ($route) ? $route->getParam('locale') : null,
            $request->getQueryParam('__locale'),
        ];
        foreach ($possibleLocales as $possibleLocale) {
            if (in_array($possibleLocale, $enabledLocales)) {
                $localeCode = $possibleLocale;
            }
        }
        $locale = $this->getShared(LocaleFactoryInterface::class)->create($localeCode);
        $this->getShared(CookieInterface::class)->set(self::LOCALE_VARIABLE_NAME, $localeCode);
        $this->getShared(SessionInterface::class)->set(self::LOCALE_VARIABLE_NAME, $localeCode);
        $this->getShared(TranslatorInterface::class)->setCurrentLang($locale->getLang()->getCode());
        $this->setShared(LocaleInterface::class, $locale);
        Locale::setDefault($localeCode);
    }

    /**
     *
     * @return void
     */
    protected function defineSkin(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $skinName = $this->getShared(ViewFactoryInterface::class)->getSkin()->getName();
        if ($this->getShared(SessionInterface::class)->get(self::SKIN_VARIABLE_NAME)) {
            $skinName = $this->getShared(SessionInterface::class)->get(self::SKIN_VARIABLE_NAME);
        }
        if ($request->getCookieParam(self::SKIN_VARIABLE_NAME)) {
            $skinName = $request->getCookieParam(self::SKIN_VARIABLE_NAME);
        }
        if ($request->getQueryParam('_skin')) {
            $skinName = $request->getQueryParam('_skin');
        }

        $skin = $this->getShared(SkinFactory::class)->create($skinName);

        if ($skin->isPersistent()) {
            $response = $response->withCookie(self::SKIN_VARIABLE_NAME, $skin->getName());
            $this->getShared(SessionInterface::class)->set(self::SKIN_VARIABLE_NAME, $skin->getName());
        }
        $this->getShared(ViewFactoryInterface::class)->setSkin($skin);
        $this->setShared(Alias::CURRENT_SKIN, $skin);
        return $response;
    }
}
