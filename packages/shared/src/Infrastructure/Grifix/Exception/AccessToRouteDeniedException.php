<?php
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Grifix\Exception;

class AccessToRouteDeniedException extends \RuntimeException
{
    protected $routeAlias;

    /**
     * AccessToRouteDeniedException constructor.
     * @param string $routeAlias
     */
    public function __construct(string $routeAlias)
    {
        $this->routeAlias = $routeAlias;
        parent::__construct(sprintf('Access to route "%s" denied!', $routeAlias));
    }
}
