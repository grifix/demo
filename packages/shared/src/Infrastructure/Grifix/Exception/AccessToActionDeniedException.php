<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Grifix\Exception;

class AccessToActionDeniedException extends \Exception
{
    protected $action;
    
    /**
     * RequestAccessDeniedException constructor.
     *
     * @param string $action
     */
    public function __construct(string $action)
    {
        $this->message = 'Access for action "'.$action.'" denied!"';
        $this->action = $action;
    }
}
