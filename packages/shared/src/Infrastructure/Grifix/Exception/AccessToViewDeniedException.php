<?php
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Grifix\Exception;

class AccessToViewDeniedException extends \RuntimeException
{
    protected $viewAlias;

    /**
     * AccessToRouteDeniedException constructor.
     * @param string $viewAlias
     */
    public function __construct(string $viewAlias)
    {
        $this->viewAlias = $viewAlias;
        parent::__construct(sprintf('Access to view "%s" denied!', $viewAlias));
    }
}
