<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Grifix;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Exception\ExceptionProcessorInterface;
use Grifix\Kit\Exception\UiException;
use Grifix\Kit\Exception\UiExceptionDefinition;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\ModuleClassInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Route\Exception\NoRouteMatchException;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Shared\Application\Client\Acl\AclClientInterface;
use Grifix\Shared\Infrastructure\Grifix\Exception\AccessToActionDeniedException;
use Grifix\Shared\Infrastructure\Grifix\Exception\AccessToRouteDeniedException;
use Grifix\Shared\Infrastructure\Grifix\Exception\AccessToViewDeniedException;
use Psr\Log\LoggerInterface;
use Throwable;

class ExceptionProcessor implements ModuleClassInterface, ExceptionProcessorInterface
{
    use ModuleClassTrait;

    /** @var array | UiExceptionDefinition[] */
    protected $exceptionsMapping;

    /**
     * {@inheritdoc}
     */
    public function process(Throwable $exception): Throwable
    {
        if ($exception instanceof ValidationException) {
            return $this->prepareValidationException($exception);
        }
        if (($exception instanceof AccessToActionDeniedException)
            || ($exception instanceof AccessToRouteDeniedException)
            || ($exception instanceof AccessToViewDeniedException)
        ) {
            return $this->prepareAccessDeniedException($exception);
        }
        if ($exception instanceof NoRouteMatchException) {
            return $this->prepare404Exception($exception);
        }

        $exception = $this->prepareException($exception);
        if (false === ($exception instanceof UiException)) {
            $this->getShared(LoggerInterface::class)->error($exception->getMessage(), [
                'type' => get_class($exception),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'code' => $exception->getCode(),
                'trace' => $exception->getTraceAsString(),
            ]);
        }
        return $exception;
    }

    protected function prepareException(Throwable $exception): Throwable
    {
        if ($exception instanceof UiException || false === array_key_exists(get_class($exception),
                $this->getExceptionsMapping())) {
            return $exception;
        }

        $definition = $this->getExceptionsMapping()[get_class($exception)];
        $translationKeys = $this->getShared(TranslatorInterface::class)
            ->getTranslationsVars($definition->getTranslationKey());
        $translationVars = [];
        foreach ($translationKeys as $translationKey) {
            $method = 'get' . $translationKey;
            $translationVars[$translationKey] = $exception->$method();
        }

        return new UiException(
            $definition->getKey(),
            $this->getShared(TranslatorInterface::class)->translate($definition->getTranslationKey(), $translationVars),
            $definition->getHttpCode(),
            $exception
        );
    }

    /**
     * @return UiExceptionDefinition[]
     */
    protected function getExceptionsMapping(): array
    {
        if (null === $this->exceptionsMapping) {
            $this->exceptionsMapping = [];
            foreach ($this->getShared(KernelInterface::class)->getModules() as $module) {
                $this->exceptionsMapping = array_merge(
                    $this->exceptionsMapping,
                    $this->getShared(ConfigInterface::class)->get($module->getConfigPrefix() . '.exception.mapping', [])
                );
            }
        }

        return $this->exceptionsMapping;
    }

    /**
     * @param ValidationException $exception
     *
     * @return ValidationException
     */
    protected function prepareValidationException(ValidationException $exception): ValidationException
    {
        return new ValidationException($exception->getErrors(), 400, $exception);
    }

    /**
     * @param Throwable $exception
     *
     * @return UiException
     */
    protected function prepare404Exception(Throwable $exception)
    {
        return new UiException(
            'grifix.kit.msg_404',
            $this->getShared(TranslatorInterface::class)->translate('grifix.kit.msg_404'),
            404,
            $exception
        );
    }

    /**
     * @param Throwable $exception
     *
     * @return UiException
     */
    protected function prepareAccessDeniedException(Throwable $exception): UiException
    {
        $acl = $this->getShared(AclClientInterface::class);
        $translator = $this->getShared(TranslatorInterface::class);
        if (!$acl->isSignedInUser($this->getShared(SessionInterface::class)->getId())) {
            return new UiException(
                'grifix.kit.msg_unauthorized',
                $translator->translate('grifix.kit.msg_unauthorized'),
                401,
                $exception
            );
        }

        return new UiException(
            'grifix.kit.msg_accessDenied',
            $translator->translate('grifix.kit.msg_accessDenied'),
            403,
            $exception
        );
    }
}
