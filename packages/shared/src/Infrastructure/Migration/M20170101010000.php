<?php

declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Migration;

use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */
class M20170101010000 extends AbstractMigration
{
    public function up(): void
    {
        $sql = <<<SQL
CREATE EXTENSION IF NOT EXISTS "postgres-json-schema";
create table grifix_kit.event
(
	id uuid not null,
	type varchar not null,
	data jsonb not null,
	published_at timestamp,
	created_at timestamp not null,
	number bigint not null,
	constraint event_key
		primary key (type, id)
);

create unique index event_number_uindex
	on grifix_kit.event (number);

create table grifix_kit.subscription
(
    subscriber_type varchar not null,
    id uuid not null, 
    status varchar not null,
    last_received_event_number bigint,
    last_event_received_at timestamp,
    alias varchar not null, 
	constraint subscription_key
		primary key (subscriber_type, id)
);

create table grifix_kit.next_event_number(
    id bigint not null,
    value bigint not null
);
insert into grifix_kit.next_event_number (id, value) values (1,1);

SQL;

        $this->execute($sql);
    }

    public function down(): void
    {
        $sql = <<<SQL
DROP EXTENSION IF EXISTS "postgres-json-schema";
drop table if exists grifix_kit.next_event_number;
drop table if exists grifix_kit.event;
drop table if exists grifix_kit.subscription;
SQL;

        $this->execute($sql);
    }
}
