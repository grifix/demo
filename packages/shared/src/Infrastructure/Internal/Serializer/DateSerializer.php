<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\Serializer;

use DateTimeImmutable;
use DateTimeZone;
use Grifix\Shared\Domain\DateTime\DateTime;
use Grifix\Shared\Domain\DateTime\DateOutsideInterface;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Kit\Orm\Serializer\SerializerInterface;

class DateSerializer implements SerializerInterface
{
    protected $format = 'Y-m-d H:i:s';

    /** @var DateOutsideInterface */
    protected $dateInfrastructure;

    public function __construct(DateOutsideInterface $dateInfrastructure)
    {
        $this->dateInfrastructure = $dateInfrastructure;
    }

    public function serialize($value): ?array
    {
        if ($value instanceof DateTimeInterface) {
            return ['value' => $value->toPhpDateTime()->format($this->format)];
        }

        return null;
    }

    public function unSerialize($value)
    {
        if ($value) {
            $timestamp = (new DateTimeImmutable($value['value'], new DateTimeZone('UTC')))->getTimestamp();
            return new DateTime($this->dateInfrastructure, $timestamp);
        }
        return null;
    }
}
