<?php
declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\Serializer;

use DateTimeInterface;
use Grifix\Kit\Orm\Serializer\AbstractObjectSerializer;
use Grifix\Kit\Orm\Serializer\DateTimeSerializer;
use Grifix\Shared\Domain\DateTime\DateTimeInterface as DomainDateTimeInterface;

class ObjectSerializer extends AbstractObjectSerializer
{
    protected function init(): void
    {
        $this->addTypeSerializer(
            DateTimeInterface::class,
            $this->serializerFactory->createSerializer(DateTimeSerializer::class)
        );

        $this->addTypeSerializer(
            DomainDateTimeInterface::class,
            $this->serializerFactory->createSerializer(DateSerializer::class)
        );
    }
}
