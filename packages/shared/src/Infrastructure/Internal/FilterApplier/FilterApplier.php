<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Application\Common\Filter\AbstractFilter;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\ColumnIsNotSortableException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\InvalidSortingDirectionException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\UndefinedLangCodeException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\UnknownColumnFilterException;

class FilterApplier implements FilterApplierInterface
{
    /**
     * @param SortableColumn[] $sortableColumns
     * @param FilterableColumn[] $filterableColumns
     */
    public function __construct(
        protected ColumnFilterApplierInterface $columnFilterApplier,
        protected array $sortableColumns = [],
        protected array $filterableColumns = []
    ) {
        $this->columnFilterApplier = $columnFilterApplier;
        $this->sortableColumns = $sortableColumns;
        $this->filterableColumns = $filterableColumns;
    }

    public function addSortableColumn(string $name, string $dbField): FilterApplierInterface
    {
        $this->sortableColumns[] = new SortableColumn($name, $dbField);
        return $this;
    }

    public function addFilterableColumn(string $name, string $filterClass, string $dbField): FilterApplierInterface
    {
        $this->filterableColumns[] = new FilterableColumn($name, $filterClass, $dbField);
        return $this;
    }

    public function apply(
        AbstractFilter $filter,
        QueryInterface $query
    ): void {

        if ($filter->getOffset() !== null) {
            $query->offset($filter->getOffset());
        }
        if ($filter->getLimit() !== null) {
            $query->limit($filter->getLimit());
        }
        if ($filter->getSortColumn()) {
            $this->applyOrderBy($filter, $query);
        }
        $this->applyFilter($filter, $query);
    }

    protected function assertSortDirection(string $sortDirection)
    {
        if (!in_array($sortDirection, ['asc', 'desc'])) {
            throw new InvalidSortingDirectionException($sortDirection);
        }
    }

    /**
     * @param SortableColumn $sortableColumns
     */
    protected function applyOrderBy(
        AbstractFilter $filter,
        QueryInterface $query
    ): void {
        if ($filter->getSortDirection()) {
            $this->assertSortDirection($filter->getSortDirection());
        }
        $sortableColumn = $this->getSortableColumn($filter->getSortColumn());
        if ($sortableColumn->isMultiLang()) {
            if (null === $filter->withLangCode()) {
                throw new UndefinedLangCodeException();
            }
            $query->orderBy(
                $sortableColumn->getLangDbField($filter->withLangCode()) . ' ' . $filter->getSortDirection()
            );
        } else {
            $query->orderBy($sortableColumn->getDbField() . ' ' . $filter->getSortDirection());
        }
    }

    protected function getSortableColumn(string $name): SortableColumn
    {
        foreach ($this->sortableColumns as $sortableColumn) {
            if ($sortableColumn->getName() === $name) {
                return $sortableColumn;
            }
        }
        throw new ColumnIsNotSortableException($name);
    }

    protected function applyFilter(AbstractFilter $filter, QueryInterface $query): void
    {
        foreach ($filter->getColumnFilters() as $i => $columnFilter) {
            $this->applyColumnFilter($filter, $query, $columnFilter, $i);
        }
    }

    protected function applyColumnFilter(
        AbstractFilter $filter,
        QueryInterface $query,
        object $columnFilter,
        int $index
    ): void {
        $filterableColumn = $this->getFilterableColumn(get_class($columnFilter));
        $dbField = $filterableColumn->getDbField();
        if ($filterableColumn->isMultiLang()) {
            if (null === $filter->withLangCode()) {
                throw new UndefinedLangCodeException();
            }
            $dbField = $filterableColumn->getLangDbField($filter->withLangCode());
        }
        $this->columnFilterApplier->applyFilter(
            $query,
            $columnFilter,
            $dbField,
            sprintf('%s_%s', $filterableColumn->getName(), $index)
        );
    }

    protected function getFilterableColumn(string $columnFilterClass): FilterableColumn
    {
        foreach ($this->filterableColumns as $filterableColumn) {
            if ($filterableColumn->getFilterClass() === $columnFilterClass) {
                return $filterableColumn;
            }
        }
        throw new UnknownColumnFilterException($columnFilterClass);
    }
}
