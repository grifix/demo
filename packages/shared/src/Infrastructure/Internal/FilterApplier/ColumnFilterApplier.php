<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Application\Common\Filter\ColumnFilter\DateColumnFilter;
use Grifix\Shared\Application\Common\Filter\ColumnFilter\IntColumnFilter;
use Grifix\Shared\Application\Common\Filter\ColumnFilter\StringColumnFilter;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\UnknownFilterTypeException;

class ColumnFilterApplier implements ColumnFilterApplierInterface
{

    public function applyFilter(QueryInterface $query, object $filter, string $column, string $valueAlias): void
    {
        if ($filter instanceof StringColumnFilter) {
            $this->applyStringFilter($query, $filter, $column, $valueAlias);
            return;
        }
        if ($filter instanceof IntColumnFilter) {
            $this->applyIntFilter($query, $filter, $column, $valueAlias);
            return;
        }
        if ($filter instanceof DateColumnFilter) {
            $this->applyDateFilter($query, $filter, $column, $valueAlias);
            return;
        }

        throw new UnknownFilterTypeException(get_class($filter));
    }


    protected function applyStringFilter(
        QueryInterface $query,
        StringColumnFilter $filter,
        string $column,
        string $valueAlias
    ): void {
        switch ($filter->getCondition()) {
            case StringColumnFilter::CONDITION_EQUAL:
                $query->where(sprintf("%s = :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case StringColumnFilter::CONDITION_NOT_EQUAL:
                $query->where(sprintf("%s != :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case StringColumnFilter::CONDITION_CONTAINS:
                $query->where(sprintf("%s iLike :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, '%' . $filter->getValue() . '%');
                break;

            case StringColumnFilter::CONDITION_BEGIN_WITH:
                $query->where(sprintf("%s iLike :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue() . '%');
                break;

            case StringColumnFilter::CONDITION_END_WITH:
                $query->where(sprintf("%s iLike :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, '%' . $filter->getValue());
                break;
        }
    }

    protected function applyIntFilter(
        QueryInterface $query,
        IntColumnFilter $filter,
        string $column,
        string $valueAlias
    ): void {
        switch ($filter->getCondition()) {
            case IntColumnFilter::CONDITION_EQUAL:
                $query->where(sprintf("(%s)::int = :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case IntColumnFilter::CONDITION_NOT_EQUAL:
                $query->where(sprintf("(%s)::int != :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case IntColumnFilter::CONDITION_GREATER_THAN:
                $query->where(sprintf("(%s)::int > :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case IntColumnFilter::CONDITION_GREATER_THAN_OR_EQUAL:
                $query->where(sprintf("(%s)::int >= :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case IntColumnFilter::CONDITION_LESS_THAN:
                $query->where(sprintf("(%s)::int < :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;

            case IntColumnFilter::CONDITION_LESS_THAN_OR_EQUAL:
                $query->where(sprintf("(%s)::int <= :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $filter->getValue());
                break;
        }
    }

    protected function applyDateFilter(
        QueryInterface $query,
        DateColumnFilter $filter,
        string $column,
        string $valueAlias
    ): void {
        $value = date('Y-m-d H:i:s', $filter->getValue());
        switch ($filter->getCondition()) {
            case DateColumnFilter::CONDITION_EQUAL:
                $query->where(sprintf("(%s)::timestamp = :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $value);
                break;

            case DateColumnFilter::CONDITION_NOT_EQUAL:
                $query->where(sprintf("(%s)::timestamp != :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $value);
                break;

            case DateColumnFilter::CONDITION_GREATER_THAN:
                $query->where(sprintf("(%s)::timestamp > :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $value);
                break;

            case DateColumnFilter::CONDITION_GREATER_THAN_OR_EQUAL:
                $query->where(sprintf("(%s)::timestamp >= :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $value);
                break;

            case DateColumnFilter::CONDITION_LESS_THAN:
                $query->where(sprintf("(%s)::timestamp < :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $value);
                break;

            case DateColumnFilter::CONDITION_LESS_THAN_OR_EQUAL:
                $query->where(sprintf("(%s)::timestamp <= :%s", $column, $valueAlias))
                    ->bindValue($valueAlias, $value);
                break;
        }
    }
}
