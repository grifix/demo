<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Application\Common\Filter\AbstractFilter;

interface FilterApplierInterface
{
    public function apply(
        AbstractFilter $filter,
        QueryInterface $query
    ): void;

    public function addSortableColumn(string $name, string $dbField): FilterApplierInterface;

    public function addFilterableColumn(string $name, string $filterClass, string $dbField): FilterApplierInterface;
}
