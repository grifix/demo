<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

interface FilterApplierFactoryInterface
{
    /**
     * @param SortableColumn[] $sortableColumns
     * @param FilterableColumn[] $filterableColumns
     * @return FilterApplierInterface
     */
    public function createFilterApplier(array $sortableColumns = [], array $filterableColumns = []): FilterApplierInterface;
}
