<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class FilterApplierFactory extends AbstractFactory implements FilterApplierFactoryInterface
{
    /** @var ColumnFilterApplierInterface */
    protected $columnFilterApplier;

    public function __construct(
        ClassMakerInterface $classMaker,
        ColumnFilterApplierInterface $columnFilterApplier
    ) {
        parent::__construct($classMaker);
        $this->columnFilterApplier = $columnFilterApplier;
    }

    /**
     * @param SortableColumn[] $sortableColumns
     * @param FilterableColumn[] $filterableColumns
     * @return FilterApplierInterface
     */
    public function createFilterApplier(array $sortableColumns = [], array $filterableColumns = []): FilterApplierInterface
    {
        $class = $this->makeClassName(FilterApplier::class);
        return new $class(
            $this->columnFilterApplier,
            $sortableColumns,
            $filterableColumns
        );
    }
}
