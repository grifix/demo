<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\FieldIsNotMultiLangException;

class FilterableColumn
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $filterClass;

    /** @var string */
    protected $dbField;

    public function __construct(string $name, string $filterClass, string $dbFiled)
    {
        $this->name = $name;
        $this->filterClass = $filterClass;
        $this->dbField = $dbFiled;
    }

    public function getFilterClass(): string
    {
        return $this->filterClass;
    }

    public function getDbField(): string
    {
        return $this->dbField;
    }

    public function isMultiLang(): bool
    {
        return strpos($this->dbField, '%s') !== false;
    }

    public function getLangDbField(string $langCode): string
    {
        if (!$this->isMultiLang()) {
            throw new FieldIsNotMultiLangException($this->name);
        }
        return sprintf($this->dbField, $langCode);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
