<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Kit\Db\Query\QueryInterface;

interface ColumnFilterApplierInterface
{
    public function applyFilter(QueryInterface $query, object $filter, string $column, string $valueAlias): void;
}
