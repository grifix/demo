<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier;

use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\FieldIsNotMultiLangException;

class SortableColumn
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $dbField;

    public function __construct(string $name, string $dbField)
    {
        $this->name = $name;
        $this->dbField = $dbField;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDbField(): string
    {
        return $this->dbField;
    }

    public function getLangDbField(string $langCode): string
    {
        if (!$this->isMultiLang()) {
            throw new FieldIsNotMultiLangException($this->name);
        }
        return sprintf($this->dbField, $langCode);
    }

    public function isMultiLang(): bool
    {
        return strpos($this->dbField, '%s') !== false;
    }
}
