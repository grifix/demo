<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception;

use Mockery\Exception\RuntimeException;

class InvalidSortingDirectionException extends RuntimeException
{
    public function __construct(string $sortingDirection)
    {
        parent::__construct(sprintf("Invalid sorting direction %s!", $sortingDirection));
    }
}
