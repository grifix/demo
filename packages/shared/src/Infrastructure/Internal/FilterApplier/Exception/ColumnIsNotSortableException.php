<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception;

use Mockery\Exception\RuntimeException;

class ColumnIsNotSortableException extends RuntimeException
{
    /** @var string */
    protected $columnName;

    public function __construct(string $columnName)
    {
        $this->columnName = $columnName;
        parent::__construct(sprintf('Column is not sortable %s', $columnName));
    }
}
