<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception;

use Mockery\Exception\RuntimeException;

class FieldIsNotMultiLangException extends RuntimeException
{
    /** @var string */
    protected $filedName;

    public function __construct(string $filedName)
    {
        $this->filedName = $filedName;
        parent::__construct(sprintf('%s is not multi language!', $filedName));
    }

}
