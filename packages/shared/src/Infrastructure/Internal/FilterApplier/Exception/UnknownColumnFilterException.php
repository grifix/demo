<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception;

use Mockery\Exception\RuntimeException;

class UnknownColumnFilterException extends RuntimeException
{
    /** @var string */
    protected $filterClass;

    public function __construct(string $filterClass)
    {
        $this->filterClass = $filterClass;
        parent::__construct(sprintf('Unknown filter class "%s"!', $filterClass));
    }
}
