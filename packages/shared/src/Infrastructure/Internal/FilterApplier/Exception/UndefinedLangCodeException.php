<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception;

use Mockery\Exception\RuntimeException;

class UndefinedLangCodeException extends RuntimeException
{
    protected $message = 'Undefined lang code!';
}
