<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Internal\EventRepository\Exception;

final class TypeAlreadyRegisteredException extends \Exception
{
    public function __construct(private string $eventType, private string $moduleName)
    {
        parent::__construct(sprintf(
            'Cannot register event type "%s" for module "%s" event type has been already registered in another module!',
            $this->eventType,
            $this->moduleName
        ));
    }
}
