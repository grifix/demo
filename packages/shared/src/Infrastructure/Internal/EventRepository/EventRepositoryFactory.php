<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Internal\EventRepository;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Event\Store\Repository\EventRepository;
use Grifix\Kit\Event\Store\Repository\EventRepositoryInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Grifix\Kit\Normalizer\NormalizerInterface;
use Grifix\Kit\Uuid\UuidGeneratorInterface;
use Grifix\Shared\Application\Common\Clock\ClockInterface;
use Grifix\Shared\Infrastructure\Internal\EventRepository\Exception\TypeAlreadyRegisteredException;

final class EventRepositoryFactory
{
    public function __construct(
        protected KernelInterface $kernel,
        protected ConnectionInterface $connection,
        protected UuidGeneratorInterface $uuidGenerator,
        protected ClockInterface $clock,
        protected NormalizerInterface $normalizer
    ) {

    }

    public function createEventRepository(): EventRepositoryInterface
    {
        $eventsMap = [];
        foreach ($this->kernel->getModules() as $module) {
            $this->fetchModuleEventsMap($module, $eventsMap);
        }

        return new EventRepository(
            $this->connection,
            $this->uuidGenerator,
            $this->clock,
            $this->normalizer,
            $eventsMap
        );
    }

    protected function fetchModuleEventsMap(ModuleInterface $module, array &$eventsMap): void
    {
        $config = $module->getConfig($this->kernel->getEnvironment());
        if (!isset($config['events'])) {
            return;
        }
        foreach ($config['events'] as $type => $class) {
            if (array_key_exists($type, $eventsMap)) {
                throw new TypeAlreadyRegisteredException($type, $module->getName());
            }
            $eventsMap[$type] = $class;
        }
    }
}
