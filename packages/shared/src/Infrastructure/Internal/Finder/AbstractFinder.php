<?php declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Internal\Finder;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Application\Common\Filter\AbstractFilter;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\ColumnFilterApplierInterface;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\ColumnIsNotSortableException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\InvalidSortingDirectionException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\UndefinedLangCodeException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\Exception\UnknownColumnFilterException;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\FilterableColumn;

abstract class AbstractFinder
{
    /** @var ConnectionInterface */
    protected $connection;

    /** @var CollectionFactoryInterface */
    protected $collectionFactory;

    /** @var ColumnFilterApplierInterface */
    protected $filterApplier;

    public function __construct(
        ConnectionInterface $connection,
        CollectionFactoryInterface $collectionFactory,
        ColumnFilterApplierInterface $filterApplier
    ) {
        $this->connection = $connection;
        $this->collectionFactory = $collectionFactory;
        $this->filterApplier = $filterApplier;
    }

    abstract protected function getSortableColumns(): array;

    /**
     * @return FilterableColumn[]
     */
    abstract protected function getFilterableColumns(): array;

    protected function createQuery(
        string $table,
        ?int $offset = null,
        ?int $limit = null
    ): QueryInterface {

        $query = $this->connection->createQuery()
            ->select('*')
            ->from($table);

        if ($offset !== null) {
            $query->offset($offset);
        }

        if ($limit !== null) {
            $query->limit($limit);
        }

        return $query;
    }

    protected function applyOrderBy(
        string $columnName,
        QueryInterface $query,
        string $langCode = null,
        string $sortDirection = 'asc'
    ): void {
        if (!in_array($columnName, array_keys($this->getSortableColumns()))) {
            throw new ColumnIsNotSortableException($columnName);
        }

        $fieldName = $this->getSortableColumns()[$columnName];
        if (strpos($fieldName, '%s') !== false) {
            if (null === $langCode) {
                throw new UndefinedLangCodeException();
            }
            $fieldName = sprintf($fieldName, $langCode);
        }
        if (!in_array(mb_strtolower($sortDirection), ['asc', 'desc'])) {
            throw new InvalidSortingDirectionException($sortDirection);
        }

        $query->orderBy(sprintf('%s %s', $fieldName, $sortDirection));
    }

    protected function createFilterableColumn(string $name, string $filterClass, string $dbField): FilterableColumn
    {
        return new FilterableColumn($name, $filterClass, $dbField);
    }

    protected function applyFilter(AbstractFilter $filter, QueryInterface $query): void
    {
        if ($filter->getId()) {
            $query->where('id = :id')->bindValue('id', $filter->getId());
        }

        foreach ($filter->getColumnFilters() as $i => $columnFilter) {
            $this->applyColumnFilter($filter, $query, $columnFilter, $i);
        }
    }

    protected function getFilterableColumn(string $filterClass): FilterableColumn
    {
        foreach ($this->getFilterableColumns() as $filterableColumn) {
            if ($filterableColumn->getFilterClass() === $filterClass) {
                return $filterableColumn;
            }
        }
        throw new UnknownColumnFilterException($filterClass);
    }

    protected function applyColumnFilter(
        AbstractFilter $filter,
        QueryInterface $query,
        object $columnFilter,
        int $index
    ): void {
        $filterableColumn = $this->getFilterableColumn(get_class($columnFilter));
        if ($filterableColumn->isMultiLang()) {
            if (!$filter->withLangCode()) {
                throw new UndefinedLangCodeException();
            }
        }
    }
}
