<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Internal;

use Grifix\Shared\Infrastructure\Internal\Serializer\ObjectSerializer;

abstract class AbstractBlueprint extends \Grifix\Kit\Orm\Blueprint\AbstractBlueprint
{
    public function getSerializerClass(): string
    {
        return ObjectSerializer::class;
    }
}
