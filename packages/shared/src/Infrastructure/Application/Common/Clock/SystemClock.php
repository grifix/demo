<?php declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Application\Common\Clock;

use Grifix\Shared\Application\Common\Clock\ClockInterface;

class SystemClock implements ClockInterface
{
    public function getTime(): int
    {
        return time();
    }

    public function getDate(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }

    public function getMicroTime(): float
    {
        return microtime(true);
    }
}
