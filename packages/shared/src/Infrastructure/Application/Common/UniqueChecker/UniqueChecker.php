<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Shared\Infrastructure\Application\Common\UniqueChecker;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Shared\Application\Common\UniqueChecker\UniqueCheckerInterface;

class UniqueChecker implements UniqueCheckerInterface
{
    /** @var ConnectionInterface */
    protected $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function isUnique(string $field, string $table, $value, ?string $recordId = null): bool
    {
        $query = $this->connection->createQuery()
            ->select('COUNT(*) AS count')
            ->from($table)->where(sprintf('%s = :value', $field))->bindValue('value', $value);
        if ($recordId) {
            $query->where('id != :id')->bindValue('id', $recordId);
        }

        return !(bool) $query->fetch()['count'];
    }
}
