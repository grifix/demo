<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Infrastructure\Application\Client\Acl\Adapter;

use Grifix\Shared\Application\Client\Acl\Dto\UserDto;

/**
 * Class GrifixAdapter
 *
 * @category Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface AdapterInterface
{
    /**
     * @param string $sessionId
     * @param string $permission
     *
     * @return bool
     */
    public function hasAccess(string $sessionId, string $permission): bool;
    
    /**
     * @param string $sessionId
     *
     * @return \Grifix\Shared\Application\Client\Acl\Dto\UserDto
     */
    public function getSignedInUser(string $sessionId): UserDto;
}
