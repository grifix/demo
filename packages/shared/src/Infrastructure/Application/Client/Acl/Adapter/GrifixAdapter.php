<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Application\Client\Acl\Adapter;

use Grifix\Acl\Application\Projection\UserProjection\UserDto as AclUserDto;
use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Acl\Application\Query\HasUserAccess\HasUserAccessQuery;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Shared\Application\Client\Acl\Dto\UserDto;

/**
 * Class GrifixAdapter
 *
 * @category Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GrifixAdapter implements AdapterInterface
{
    /**
     * @var QueryBusInterface
     */
    protected $queryBus;

    /**
     * GrifixAdapter constructor.
     *
     * @param QueryBusInterface $queryBus
     */
    public function __construct(QueryBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAccess(string $sessionId, string $permission): bool
    {
        /**@var $user AclUserDto */
        $user = $this->queryBus->execute(new GetSignedInUserQuery($sessionId));

        return $this->queryBus->execute(new HasUserAccessQuery($user->getId(), $permission));
    }

    /**
     * {@inheritdoc}
     */
    public function getSignedInUser(string $sessionId): UserDto
    {
        /**@var $userModel \Grifix\Shared\Application\Client\Acl\Dto\UserDto */
        $userModel = $this->queryBus->execute(new GetSignedInUserQuery($sessionId));

        return new UserDto(
            $userModel->getId(),
            $userModel->getEmail(),
            $userModel->getIsGuest(),
            $userModel->getTimeZone()
        );
    }
}
