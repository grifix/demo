<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Infrastructure\Application\Client\Acl;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Shared\Application\Client\Acl\AclClientInterface;
use Grifix\Shared\Application\Client\Acl\Dto\UserDto;
use Grifix\Shared\Application\Client\Acl\Exception\ThereIsNoPermissionDefinedForActionException;
use Grifix\Shared\Application\Client\Acl\Exception\ThereIsNoPermissionDefinedForRouteException;
use Grifix\Shared\Application\Client\Acl\Exception\ThereIsNoPermissionDefinedForViewException;
use Grifix\Shared\Infrastructure\Application\Client\Acl\Adapter\AdapterInterface;

class AclClient implements AclClientInterface
{

    private const TYPE_ROUTE = 'routes';
    private const TYPE_VIEW = 'views';
    private const TYPE_ACTION = 'actions';

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     * @var bool
     */
    protected $strictActonAcl = false;

    /**
     * @var bool
     */
    protected $strictRouteAcl = false;

    /**
     * @var bool
     */
    protected $strictViewAcl = false;


    /**
     * AclService constructor.
     *
     * @param AdapterInterface $adapter <\Grifix\Shared\Infrastructure\Application\Client\Acl\Adapter\GrifixAdapter>
     * @param ConfigInterface $config
     * @param bool $strictActonAcl <cfg:grifix.shared.http.strict_action_acl>
     * @param bool $strictRouteAcl <cfg:grifix.shared.http.strict_route_acl>
     * @param bool $strictViewAcl <cfg:grifix.shared.http.strict_view_acl>
     */
    public function __construct(
        AdapterInterface $adapter,
        ConfigInterface $config,
        $strictActonAcl = false,
        $strictRouteAcl = false,
        $strictViewAcl = false
    ) {
        $this->config = $config;
        $this->adapter = $adapter;
        $this->strictActonAcl = $strictActonAcl;
        $this->strictRouteAcl = $strictRouteAcl;
        $this->strictViewAcl = $strictViewAcl;
    }


    /**
     * {@inheritdoc}
     */
    public function hasAccessToAction(string $sessionId, string $action)
    {
        $permission = $this->getResourcePermission($action, static::TYPE_ACTION);
        if ($permission) {
            return $this->adapter->hasAccess($sessionId, $permission);
        } elseif ($this->strictActonAcl) {
            throw new ThereIsNoPermissionDefinedForActionException($action);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAccessToRoute(string $sessionId, string $routeAlias): bool
    {
        $permission = $this->getResourcePermission($routeAlias, static::TYPE_ROUTE);
        if ($permission) {
            return $this->adapter->hasAccess($sessionId, $permission);
        } elseif ($this->strictRouteAcl) {
            throw new ThereIsNoPermissionDefinedForRouteException($routeAlias);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAccessToView(string $sessionId, string $viewAlias): bool
    {
        $arr = explode('.', $viewAlias);
        $arr[2] = '{skin}';
        $commonViewAlias = implode('.', $arr);
        $permission = $this->getResourcePermission($commonViewAlias, static::TYPE_VIEW);
        if ($permission) {
            return $this->adapter->hasAccess($sessionId, $permission);
        } elseif ($this->strictViewAcl) {
            throw new ThereIsNoPermissionDefinedForViewException($viewAlias);
        }
        return true;
    }

    public function hasAccess(string $sessionId, string $permission): bool
    {
        return $this->adapter->hasAccess($sessionId, $permission);
    }

    /**
     * @param string $resourceAlias
     * @param string $resourceType
     * @return null|string
     */
    private function getResourcePermission(string $resourceAlias, string $resourceType): ?string
    {
        $arr = explode('.', $resourceAlias);
        assert(count($arr) > 3, 'Invalid request handler alias "' . $resourceAlias . '"');
        $vendor = array_shift($arr);
        $module = array_shift($arr);

        $resources = $this->config->get($vendor . '.' . $module . '.acl.' . $resourceType, []);
        if (isset($resources[$resourceAlias])) {
            return $resources[$resourceAlias];
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function isSignedInUser(string $sessionId): bool
    {
        return !$this->adapter->getSignedInUser($sessionId)->getIsGuest();
    }

    public function getSignedInUser(string $sessionId): UserDto
    {
        return $this->adapter->getSignedInUser($sessionId);
    }
}
