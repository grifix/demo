<?php declare(strict_types=1);

namespace Grifix\Shared\Ui\Common\RequestValidator;

use Grifix\Kit\Validation\Field\BoolValidationField;
use Grifix\Kit\Validation\Field\DateValidationField;
use Grifix\Kit\Validation\Field\EmailValidationField;
use Grifix\Kit\Validation\Field\FloatValidationField;
use Grifix\Kit\Validation\Field\IntValidationField;
use Grifix\Kit\Validation\Field\IpValidationField;
use Grifix\Kit\Validation\Field\MoneyValidationField;
use Grifix\Kit\Validation\Field\PositiveFloatValidationField;
use Grifix\Kit\Validation\Field\PositiveIntValidationField;
use Grifix\Kit\Validation\Field\TextValidationField;
use Grifix\Kit\Validation\Field\UploadedFileValidationField;
use Grifix\Kit\Validation\Field\UrlValidationField;
use Grifix\Kit\Validation\Field\UuidValidationField;

class ValidationFiledBuilder
{
    protected string $class;

    protected string $name;


    protected string $label;

    /** @var mixed|null */
    protected $equalValue = null;

    protected bool $multiLang = false;

    /** @var int|float|null */
    protected $greaterOrEqual = null;

    /** @var int|float|null */
    protected $greaterThan = null;

    protected ?array $inArray = null;

    /** @var int|float|null */
    protected $lessOrEqual;

    /** @var int|float|null */
    protected $lessThan;

    protected ?int $maxSymbols = null;

    protected ?int $minSymbols = null;

    protected ?string $regExp = null;

    protected ?string $dateFormat = null;

    protected bool $required = false;

    protected bool $notEmpty = false;

    protected bool $arrayValue = false;

    public function __construct(string $name, string $title)
    {
        $this->name = $name;
        $this->label = $title;
        $this->class = TextValidationField::class;
    }

    public function bool(): self
    {
        $this->class = BoolValidationField::class;
        return $this;
    }

    public function arrayValue(): self
    {
        $this->arrayValue = true;
        return $this;
    }

    public function isArrayValue(): bool
    {
        return $this->arrayValue;
    }

    public function date(string $format = null): self
    {
        $this->class = DateValidationField::class;
        $this->dateFormat = $format;
        return $this;
    }

    public function email(): self
    {
        $this->class = EmailValidationField::class;
        return $this;
    }

    public function money(): self
    {
        $this->class = MoneyValidationField::class;
        return $this;
    }

    public function int(): self
    {
        $this->class = IntValidationField::class;
        return $this;
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    public function ip(): self
    {
        $this->class = IpValidationField::class;
        return $this;
    }

    public function positiveInt(): self
    {
        $this->class = PositiveIntValidationField::class;
        return $this;
    }

    public function float(): self
    {
        $this->class = FloatValidationField::class;
        return $this;
    }

    public function positiveFloat(): self
    {
        $this->class = PositiveFloatValidationField::class;
        return $this;
    }

    public function text(): self
    {
        $this->class = TextValidationField::class;
        return $this;
    }

    public function uuid(): self
    {
        $this->class = UuidValidationField::class;
        return $this;
    }

    public function notEmpty(): self
    {
        $this->notEmpty = true;
        return $this;
    }

    public function isNotEmpty(): bool
    {
        return $this->notEmpty;
    }

    public function uploadedFile(): self
    {
        $this->class = UploadedFileValidationField::class;
        return $this;
    }

    public function url(): self
    {
        $this->class = UrlValidationField::class;
        return $this;
    }

    public function equals($value): self
    {
        $this->equalValue = $value;
        return $this;
    }

    public function multiLang(): self
    {
        $this->multiLang = true;
        return $this;
    }

    public function isMultiLang(): bool
    {
        return $this->multiLang;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getEqualValue()
    {
        return $this->equalValue;
    }

    public function getGreaterOrEqual()
    {
        return $this->greaterOrEqual;
    }

    public function greaterOrEqual($greaterOrEqual)
    {
        $this->greaterOrEqual = $greaterOrEqual;
        return $this;
    }

    public function getGreaterThan()
    {
        return $this->greaterThan;
    }

    public function greaterThan($greaterThan)
    {
        $this->greaterThan = $greaterThan;
        return $this;
    }

    public function getInArray(): ?array
    {
        return $this->inArray;
    }

    public function inArray(?array $inArray): ValidationFiledBuilder
    {
        $this->inArray = $inArray;
        return $this;
    }

    public function getLessOrEqual()
    {
        return $this->lessOrEqual;
    }

    public function lessOrEqual($lessOrEqual)
    {
        $this->lessOrEqual = $lessOrEqual;
        return $this;
    }

    public function getLessThan()
    {
        return $this->lessThan;
    }

    public function lessThan($lessThan)
    {
        $this->lessThan = $lessThan;
        return $this;
    }

    public function getMaxSymbols(): ?int
    {
        return $this->maxSymbols;
    }

    public function maxSymbols(?int $maxSymbols): ValidationFiledBuilder
    {
        $this->maxSymbols = $maxSymbols;
        return $this;
    }

    public function getMinSymbols(): ?int
    {
        return $this->minSymbols;
    }

    public function minSymbols(?int $minSymbols): ValidationFiledBuilder
    {
        $this->minSymbols = $minSymbols;
        return $this;
    }

    public function getRegExp(): ?string
    {
        return $this->regExp;
    }

    public function regExp(string $regExp): ValidationFiledBuilder
    {
        $this->regExp = $regExp;
        return $this;
    }

    public function getDateFormat(): ?string
    {
        return $this->dateFormat;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function required(): ValidationFiledBuilder
    {
        $this->required = true;
        return $this;
    }
}
