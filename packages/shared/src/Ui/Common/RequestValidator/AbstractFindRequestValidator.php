<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Ui\Common\RequestValidator;


class AbstractFindRequestValidator extends AbstractRequestValidator
{
    public const PARAM_ID = 'id';
    public const PARAM_PAGE_INDEX = 'pageIndex';
    public const PARAM_PAGE_SIZE = 'pageSize';
    public const PARAM_COUNT_TOTAL = 'countTotal';
    public const PARAM_LANG_CODE = 'langCode';
    public const PARAM_SORT_COLUMN = 'sortColumn';
    public const PARAM_SORT_DIRECTION = 'sortDirection';
    public const PARAM_COLUMN_FILTERS = 'columnFilters';

    public function init(): void
    {
        parent::init();
        $this->addField(
            $this->buildFiled(
                self::PARAM_ID,
                'grifix.kit.id'
            )
                ->uuid()
        )->addField(
            $this->buildFiled(
                self::PARAM_PAGE_INDEX,
                'grifix.kit.pageIndex'
            )
                ->positiveInt()
        )->addField(
            $this->buildFiled(
                self::PARAM_PAGE_SIZE,
                'grifix.kit.pageSize'
            )
                ->positiveInt()
        )->addField(
            $this->buildFiled(
                self::PARAM_COUNT_TOTAL,
                'grifix.kit.countTotal'
            )
                ->bool()
        )->addField(
            $this->buildFiled(
                self::PARAM_LANG_CODE,
                'grifix.kit.langCode'
            )
                ->text()
        )->addField(
            $this->buildFiled(
                self::PARAM_SORT_COLUMN,
                'grifix.kit.sortColumn'
            )
                ->text()
        )->addField(
            $this->buildFiled(
                self::PARAM_SORT_DIRECTION,
                'grifix.kit.sortDirection'
            )
                ->inArray(['asc', 'ASC', 'desc', 'DESC'])
        )->addField(
            $this->buildFiled(
                self::PARAM_COLUMN_FILTERS,
                'grifix.kit.columnFilters'
            )
                ->arrayValue()
        );
    }
}
