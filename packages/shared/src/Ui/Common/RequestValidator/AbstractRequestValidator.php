<?php declare(strict_types=1);

namespace Grifix\Shared\Ui\Common\RequestValidator;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Validation\ValidationInterface as BaseValidation;
use Grifix\Kit\Validation\Validator\DateValidator;
use Grifix\Kit\Validation\Validator\EqualValidator;
use Grifix\Kit\Validation\Validator\GreaterOrEqualThanValidator;
use Grifix\Kit\Validation\Validator\GreaterThanValidator;
use Grifix\Kit\Validation\Validator\InArrayValidator;
use Grifix\Kit\Validation\Validator\LessOrEqualThanValidator;
use Grifix\Kit\Validation\Validator\LessThanValidator;
use Grifix\Kit\Validation\Validator\MaxNumOfSymbolsValidator;
use Grifix\Kit\Validation\Validator\MinNumOfSymbolsValidator;
use Grifix\Kit\Validation\Validator\NotEmptyValidator;
use Grifix\Kit\Validation\Validator\RegExpValidator;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

class AbstractRequestValidator implements RequestValidatorInterface
{

    /** @var string[] */
    private array $langFields = [];

    final public function __construct(
        private TranslatorInterface $translator,
        private ArrayHelperInterface $arrayHelper,
        private BaseValidation $validation,
        private ValidatorFactoryInterface $validatorFactory
    ) {
        $this->init();
    }

    protected function init(): void
    {
    }

    private function prepareData(array $data): array
    {
        $result = [];
        foreach ($data as $key => $val) {
            if (!in_array($key, $this->langFields)) {
                $result[$key] = $val;
            } else {
                $result = array_merge($result, $this->createLangData($data, $key));
            }
        }
        return $result;
    }

    private function isLangField(string $name)
    {
        return in_array($name, $this->langFields);
    }

    private function createLangName(string $name, string $langCode)
    {
        return sprintf('%s[%s]', $name, $langCode);
    }

    private function createLangLabel(string $translationKey, string $langName)
    {
        return sprintf('%s (%s)', $this->translator->translate($translationKey), $langName);
    }

    private function createLangData(array $fromData, string $fieldName): array
    {
        $result = [];
        foreach ($this->translator->getLangs() as $lang) {
            $fromKey = sprintf('%s.%s', $fieldName, $lang->getCode());
            $toKey = sprintf('%s[%s]', $fieldName, $lang->getCode());
            $result[$toKey] = $this->arrayHelper->get($fromData, $fromKey);
        }
        return $result;
    }

    public function setStrictStrategy(): RequestValidatorInterface
    {
        $this->validation->setStrictStrategy();
        return $this;
    }

    public function setSoftStrategy(): RequestValidatorInterface
    {
        $this->validation->setSoftStrategy();
        return $this;
    }

    public function validate(array $data)
    {
        $this->validation->validateOrFail($this->prepareData($data));
    }


    protected function buildFiled(string $name, string $label): ValidationFiledBuilder
    {
        return new ValidationFiledBuilder($name, $label);
    }

    protected function addField(ValidationFiledBuilder $builder): self
    {
        if ($builder->isMultiLang()) {
            $this->langFields[] = $builder->getName();
            foreach ($this->translator->getLangs() as $lang) {
                $this->createValidationField(
                    $this->createLangName($builder->getName(), $lang->getCode()),
                    $this->createLangLabel($builder->getLabel(), $lang->getCode()),
                    $builder
                );
            }
            return $this;
        }

        $this->createValidationField(
            $builder->getName(),
            $this->translator->translate($builder->getLabel()),
            $builder
        );

        return $this;
    }

    private function createValidationField(string $name, string $label, ValidationFiledBuilder $builder): void
    {
        $field = $this->validation->createField($name, $builder->getClass());
        $field->setLabel($label);
        if (null !== $builder->getEqualValue()) {
            $field->createValidator(EqualValidator::class)->setValue($builder->getEqualValue());
        }

        if (null !== $builder->getDateFormat()) {
            $field->getValidator(DateValidator::class)->setFormat($builder->getDateFormat());
        }

        if (null !== $builder->getGreaterOrEqual()) {
            $field->createValidator(GreaterOrEqualThanValidator::class)->setMinValue($builder->getGreaterOrEqual());
        }

        if (null !== $builder->getGreaterThan()) {
            $field->createValidator(GreaterThanValidator::class)->setMinValue($builder->getGreaterThan());
        }

        if (null !== $builder->getLessThan()) {
            $field->createValidator(LessThanValidator::class)->setMaxValue($builder->getLessThan());
        }

        if (null !== $builder->getLessOrEqual()) {
            $field->createValidator(LessOrEqualThanValidator::class)->setMaxValue($builder->getLessOrEqual());
        }

        if (null !== $builder->getInArray()) {
            $field->createValidator(InArrayValidator::class)->setArray($builder->getInArray());
        }

        if (null !== $builder->getMaxSymbols()) {
            $field->createValidator(MaxNumOfSymbolsValidator::class)->setNumOfSymbols($builder->getMaxSymbols());
        }

        if (null !== $builder->getMinSymbols()) {
            $field->createValidator(MinNumOfSymbolsValidator::class)->setNumOfSymbols($builder->getMinSymbols());
        }

        if (true === $builder->isNotEmpty()) {
            $field->createValidator(NotEmptyValidator::class);
        }

        if (null !== $builder->getRegExp()) {
            $field->createValidator(RegExpValidator::class)->setRegExp($builder->getRegExp());
        }

        if (true === $builder->isRequired()) {
            $field->setRequired($builder->isRequired());
        }

        if(true === $builder->isArrayValue()){
            $field->setValueIsArray();
        }
    }
}
