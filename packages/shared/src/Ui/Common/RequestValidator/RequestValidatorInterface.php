<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\RequestValidator;

interface RequestValidatorInterface
{
    public function validate(array $data);

    public function setStrictStrategy(): RequestValidatorInterface;

    public function setSoftStrategy(): RequestValidatorInterface;
}
