<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\RequestValidator;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Validation\ValidationFactoryInterface as BaseValidationFactoryInterface;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

class RequestValidatorFactory extends AbstractFactory implements RequestValidatorFactoryInterface
{
    /** @var BaseValidationFactoryInterface */
    protected $validationFactory;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    /** @var ValidatorFactoryInterface */
    protected $validatorFactory;

    public function __construct(
        BaseValidationFactoryInterface $validationFactory,
        TranslatorInterface $translator,
        ArrayHelperInterface $arrayHelper,
        ClassMakerInterface $classMaker,
        ValidatorFactoryInterface $validatorFactory
    ) {
        $this->validationFactory = $validationFactory;
        $this->translator = $translator;
        $this->arrayHelper = $arrayHelper;
        $this->validatorFactory = $validatorFactory;
        parent::__construct($classMaker);
    }

    public function create(string $class): RequestValidatorInterface
    {
        $class = $this->makeClassName($class);

        return new $class(
            $this->translator,
            $this->arrayHelper,
            $this->validationFactory->create(),
            $this->validatorFactory
        );
    }
}
