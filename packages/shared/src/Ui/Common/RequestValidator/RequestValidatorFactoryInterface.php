<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\RequestValidator;

interface RequestValidatorFactoryInterface
{
    public function create(string $class): RequestValidatorInterface;
}
