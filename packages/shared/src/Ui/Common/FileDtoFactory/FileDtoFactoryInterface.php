<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Ui\Common\FileDtoFactory;

use Grifix\Shared\Domain\File\FileDto;
use Psr\Http\Message\UploadedFileInterface;

interface FileDtoFactoryInterface
{
    public function createFileDtoFromUploadFile(UploadedFileInterface $uploadedFile): FileDto;
}
