<?php

declare(strict_types=1);

namespace Grifix\Shared\Ui\Common;

class PermissionDefinition
{
    protected string $name;

    /**
     * @var string[]
     */
    protected array $roles = [];

    protected ?string $translation;

    public function __construct(string $name, array $roles, ?string $translation = null)
    {
        $this->name = $name;
        $this->roles = $roles;
        $this->translation = $translation;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getTranslation(): ?string
    {
        return $this->translation;
    }
}
