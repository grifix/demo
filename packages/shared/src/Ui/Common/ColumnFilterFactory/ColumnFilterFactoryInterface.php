<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\ColumnFilterFactory;

interface ColumnFilterFactoryInterface
{
    /**
     * @param string|int|float|bool|string[]|int[]|float[]|bool[] $value
     */
    public function createColumnFilter(string $filterClass, $value, string $condition);
}
