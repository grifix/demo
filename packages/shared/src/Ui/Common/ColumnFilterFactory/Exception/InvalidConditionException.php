<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\ColumnFilterFactory\Exception;

use InvalidArgumentException;

class InvalidConditionException extends InvalidArgumentException
{
    /** @var string */
    protected $condition;

    public function __construct(string $condition)
    {
        $this->condition = $condition;
        parent::__construct(sprintf('Invalid condition "%s"', $condition));
    }
}
