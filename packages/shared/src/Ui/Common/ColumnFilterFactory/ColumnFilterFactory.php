<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\ColumnFilterFactory;

use Grifix\Shared\Ui\Common\ColumnFilterFactory\Exception\InvalidConditionException;

class ColumnFilterFactory implements ColumnFilterFactoryInterface
{
    public function createColumnFilter(string $filterClass, $value, string $condition)
    {
        $result = null;
        $condition = strtolower($condition);
        if (!in_array($condition, [
            'equal',
            'not_equal',
            'contains',
            'begin_with',
            'end_with',
            'greater_than',
            'greater_than_or_equal',
            'less_than',
            'less_than_or_equal',

        ])) {
            throw new InvalidConditionException($condition);
        }

        return new $filterClass($value, $condition);
    }
}
