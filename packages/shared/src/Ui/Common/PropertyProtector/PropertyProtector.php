<?php
declare(strict_types = 1);

namespace Grifix\Shared\Ui\Common\PropertyProtector;

use Grifix\Acl\Application\Projection\UserProjection\UserDto;
use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Acl\Application\Query\HasUserAccess\HasUserAccessQuery;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Reflection\ReflectionObject\ReflectionObjectFactoryInterface;
use Grifix\Kit\Session\SessionInterface;

class PropertyProtector implements PropertyProtectorInterface
{
    protected const TRANSLATION_KEY = 'grifix.kit.msg_accessDenied';

    /**
     * @var QueryBusInterface
     */
    protected $queryBus;

    /**
     * @var ReflectionObjectFactoryInterface
     */
    protected $reflectionWrapperFactory;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var SessionInterface
     */
    protected $session;

    /** @var ArrayHelperInterface */
    protected $arrayHelper;

    /** @var array */
    protected $cache = [];

    public function __construct(
        QueryBusInterface $queryBus,
        ReflectionObjectFactoryInterface $reflectionWrapperFactory,
        TranslatorInterface $translator,
        SessionInterface $session,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->queryBus = $queryBus;
        $this->reflectionWrapperFactory = $reflectionWrapperFactory;
        $this->translator = $translator;
        $this->session = $session;
        $this->arrayHelper = $arrayHelper;
    }

    public function protectProperty(object $object, string $property, string $permission): void
    {
        if (!$this->hasAccess($permission)) {
            $reflection = $this->reflectionWrapperFactory->create($object);
            $reflection->setPropertyValue($property, $this->translator->translate(self::TRANSLATION_KEY));
        }
    }

    public function protectProperties(object $object, array $protections)
    {
        $reflection = $this->reflectionWrapperFactory->create($object);
        foreach ($protections as $protection) {
            if (!$this->hasAccess($protection->getPermission())) {
                $reflection->setPropertyValue(
                    $protection->getProperty(),
                    $this->translator->translate(self::TRANSLATION_KEY)
                );
            }
        }
    }

    protected function hasAccess(string $permission): bool
    {
        $cache = $this->arrayHelper->get($this->cache, $permission);

        if (null != $cache) {
            return $cache;
        }
        /**@var $user UserDto */
        $user = $this->queryBus->execute(new GetSignedInUserQuery($this->session->getId()));
        $result =  $this->queryBus->execute(new HasUserAccessQuery($user->getId(), $permission));

        $this->arrayHelper->set($this->cache, $permission, $result);
        return $result;
    }
}
