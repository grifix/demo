<?php
declare(strict_types=1);

namespace Grifix\Shared\Ui\Common\PropertyProtector;

interface PropertyProtectorInterface
{
    public function protectProperty(object $object, string $property, string $permission): void;

    public function protectProperties(object $object, array $protections);
}
