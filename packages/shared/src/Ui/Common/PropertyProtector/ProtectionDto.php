<?php
declare(strict_types=1);

namespace Grifix\Shared\Ui\Common\PropertyProtector;

class ProtectionDto
{

    /**
     * @var string
     */
    protected $property;

    /**
     * @var string
     */
    protected $permission;

    /**
     * ProtectionDto constructor.
     * @param string $property
     * @param string $permission
     */
    public function __construct(string $property, string $permission)
    {
        $this->property = $property;
        $this->permission = $permission;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @return string
     */
    public function getPermission(): string
    {
        return $this->permission;
    }
}
