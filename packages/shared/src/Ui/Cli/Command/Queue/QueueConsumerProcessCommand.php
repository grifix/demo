<?php

declare(strict_types=1);

namespace Grifix\Shared\Ui\Cli\Command\Queue;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Exception\ExceptionNormalizerInterface;
use Grifix\Kit\MessageBroker\ConsumerFactory\ConsumerFactoryInterface;
use Grifix\Kit\MessageBroker\MessageBrokerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class QueueConsumerProcessCommand extends AbstractCommand
{
    public const NAME = 'shared:queue:consumer_process';
    public const ARG_QUEUE = 'queue';
    public const ARG_CONSUMER = 'consumer';
    public const ARG_EXCLUSIVE = 'exclusive';

    protected MessageBrokerInterface $messageBroker;

    protected ConsumerFactoryInterface $consumerFactory;

    protected LoggerInterface $logger;

    protected ExceptionNormalizerInterface $exceptionNormalizer;

    protected function init()
    {
        parent::init();
        $this->messageBroker = $this->getShared(MessageBrokerInterface::class);
        $this->consumerFactory = $this->getShared(ConsumerFactoryInterface::class);
        $this->exceptionNormalizer = $this->getShared(ExceptionNormalizerInterface::class);
        $this->logger = $this->getShared(LoggerInterface::class);
    }

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Starts queue consumer.')
            ->addArgument(self::ARG_QUEUE, InputArgument::REQUIRED, 'Queue name')
            ->addArgument(self::ARG_CONSUMER, InputArgument::REQUIRED, 'Queue consumer class')
            ->addArgument(self::ARG_EXCLUSIVE, InputArgument::OPTIONAL, 'Is consumer exclusive');
    }



    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->messageBroker->registerConsumer(
                $this->consumerFactory->create(
                    $input->getArgument(self::ARG_CONSUMER)
                ),
                $input->getArgument(self::ARG_QUEUE),
                (bool)$input->getArgument(self::ARG_EXCLUSIVE)
            );
        } catch (\Throwable $exception) {
            $this->logger->critical($exception->getMessage(), $this->exceptionNormalizer->normalize($exception));
            return 1;
        }
        return 0;
    }
}
