<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Ui\Cli\Command\Queue;

use Grifix\Kit\Alias;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\MessageBroker\QueueDefinitionsRepository\QueueDefinitionsRepositoryInterface;
use Grifix\Kit\ProcessManager\Exception\CannotStartProcessException;
use Grifix\Kit\ProcessManager\Exception\CannotStopProcessException;
use Grifix\Kit\ProcessManager\ProcessManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class QueueConsumersCommand extends AbstractCommand
{

    public const NAME = 'shared:queue:consumers';
    protected const ARGUMENT_ACTION = 'action';
    protected const ACTION_START = 'start';
    protected const ACTION_STOP = 'stop';
    protected const ACTION_RESTART = 'restart';

    protected QueueDefinitionsRepositoryInterface $queueDefinitionsRepository;

    protected ProcessManagerInterface $processManager;

    protected LoggerInterface $logger;

    public function init()
    {
        parent::init();
        $this->queueDefinitionsRepository = $this->getShared(QueueDefinitionsRepositoryInterface::class);
        $this->processManager = $this->getShared(ProcessManagerInterface::class);
        $this->logger = $this->getShared(LoggerInterface::class);
    }

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Manage queue consumers')
            ->addArgument(
                self::ARGUMENT_ACTION,
                InputArgument::REQUIRED,
                self::ACTION_START . '|' . self::ACTION_STOP . '|' . self::ACTION_RESTART
            );
    }



    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument(self::ARGUMENT_ACTION);
        switch ($action) {
            case self::ACTION_START:
                $this->start($output);
                break;

            case self::ACTION_STOP:
                $this->stop($output);
                break;

            case self::ACTION_RESTART:
                $this->stop($output);
                $this->start($output);
                break;

            default:
                throw new \InvalidArgumentException('Unknown action "' . $action . '"');
        }

        return 0;
    }

    /**
     * @param OutputInterface $output
     */
    protected function start(OutputInterface $output)
    {
        foreach ($this->queueDefinitionsRepository->find() as $definition) {
            for ($i = 0; $i < $definition->getQuantity(); $i++) {
                $this->startConsumerProcess($definition->getQueueName(), $definition->getConsumerClass(), $i, $output);
            }
        }
    }

    /**
     * @param string $class
     * @param int $number
     * @return string
     */
    protected function makeProcessId(string $class, int $number)
    {
        return str_replace('\\', '_', $class) . '_' . $number;
    }

    /**
     * @param OutputInterface $output
     */
    protected function stop(OutputInterface $output)
    {
        foreach ($this->queueDefinitionsRepository->find() as $definition) {
            for ($i = 0; $i < $definition->getQuantity(); $i++) {
                $this->stopConsumerProcess($definition->getConsumerClass(), $i, $output);
            }
        }
    }

    protected function stopConsumerProcess(string $consumerClass, int $index, OutputInterface $output): void
    {
        $processId = $this->makeProcessId($consumerClass, $index);
        if ($this->processManager->processExists($processId)) {
            $output->writeln(sprintf('Stopping "%s" consumer...', $processId));
            try {
                $this->processManager->stopProcess($processId);
            } catch (CannotStopProcessException $exception) {
                $this->warning(
                    sprintf('Cannot stop consumer "%s": %s)', $processId, $exception->getMessage()),
                    $output
                );
                return;
            }

            $this->info(sprintf('Consumer "%s" has been stopped!', $processId), $output);
        }
    }

    protected function startConsumerProcess(
        string $queue,
        string $consumerClass,
        int $index,
        OutputInterface $output
    ): void {
        $processId = $this->makeProcessId($consumerClass, $index);
        if ($this->processManager->processExists($processId)) {
            $this->warning(sprintf('Consumer %s is already started!', $processId), $output);
            return;
        }

        $output->writeln(sprintf('Starting %s consumer...', $processId));
        try {
            $this->processManager->startProcess(
                $processId,
                sprintf(
                    'php %s/cli --env=%s %s "%s" "%s"',
                    $this->getShared(Alias::ROOT_DIR),
                    $this->getShared(Alias::ENVIRONMENT),
                    QueueConsumerProcessCommand::NAME,
                    $queue,
                    $consumerClass
                )
            );
        } catch (CannotStartProcessException $exception) {
            $this->warning(sprintf('Cannot start consumer %s: %s', $processId, $exception->getMessage()), $output);
            return;
        }

        $this->info(sprintf('Consumer %s has been started', $processId), $output);
    }
}
