<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Ui\Cli\Command;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Kernel\KernelInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PublishAssetsCmd extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('shared:publish_assets')
            ->setDescription('publish modules assets');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getShared(KernelInterface::class)->publishAssets();
    }
}
