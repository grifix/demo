<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Ui\Cli\Command;


use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Type\DateTime\DateTime;
use Grifix\Shared\Domain\Event\TestEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCmd extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('shared:test')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getShared(ConnectionInterface::class)->execute('truncate table grifix_kit.event');
        $this->getShared(ConnectionInterface::class)->execute('update grifix_kit.next_event_number set value = 1 where id = 1');
        $stop = new DateTime();
        $stop = $stop->addSeconds(1);
        $i = 0;
        while ($stop > new DateTime()) {
            $id = $i+2;
            $this->getShared(ConnectionInterface::class)
                ->execute('insert into grifix_kit.next_event_number (id, value) values(:id, :value)', ['id' => $id, 'value' => $id]);
//            $this->getShared(ConnectionInterface::class)->beginTransaction();
//            $this->getShared(EventStoreInterface::class)->add(new TestEvent());
//            $this->getShared(ConnectionInterface::class)->commitTransaction();
            $i++;
        }
        $output->writeln($i);
    }
}
