<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Ui\Cli\Command;

use Exception;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Event\Bus\EventBusInterface;
use Grifix\Kit\Migration\Event\MigrateDownEvent;
use Grifix\Kit\Migration\Event\MigrateUpEvent;
use Grifix\Kit\Migration\MigrationRunnerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('shared:migrate')
            ->setDescription('Runs migrations.')
            ->addArgument('action', InputArgument::REQUIRED, 'up or down')
            ->addArgument(
                'migration',
                InputArgument::OPTIONAL,
                'Name of the migration e.g. (grifix.shared.M201712080659)'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $migrationRunner = $this->getShared(MigrationRunnerInterface::class);
        $eventBus = $this->getShared(EventBusInterface::class);

        $counter = 0;

        $eventBus->addListener(function (MigrateUpEvent $event) use ($output, &$counter) {
            $output->writeln($event->getMigrationClass() . ' has been executed.');
            $counter++;
        });

        $eventBus->addListener(function (MigrateDownEvent $event) use ($output, &$counter) {
            $output->writeln($event->getMigrationClass() . ' has been executed.');
            $counter++;
        });

        if (!in_array($input->getArgument('action'), ['up', 'down'])) {
            throw new Exception('Invalid action argument "' . $input->getArgument('action') . '"!');
        }

        if ($input->getArgument('action') == 'up') {
            $migrationRunner->up($input->getArgument('migration'));
        } elseif ($input->getArgument('action') == 'down') {
            $migrationRunner->down($input->getArgument('migration'));
        }

        if (!$counter) {
            $output->writeln('Nothing to migrate.');
        } else {
            $output->writeln('All migrations was executed successfully.');
        }

        return 0;
    }
}
