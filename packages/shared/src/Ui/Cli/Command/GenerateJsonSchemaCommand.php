<?php declare(strict_types=1);

namespace Grifix\Shared\Ui\Cli\Command;

use Grifix\Kit\ClassFetcher\ClassFetcher\ClassFetcherInterface;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Kit\Orm\SchemaGenerator\SchemaGeneratorInterface;
use Grifix\Shared\Infrastructure\Internal\AbstractBlueprint;
use ReflectionClass;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class GenerateJsonSchemaCommand extends AbstractCommand
{
    private const ARGUMENT_CLASS = 'class';

    private const OPTION_ONLY_NEW = 'only-new';

    public const NAME = 'shared:generate_json_schema';

    protected FilesystemHelperInterface $fileSystemHelper;

    protected ClassFetcherInterface $classFetcher;

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Generates json schema for entity.')
            ->addArgument(self::ARGUMENT_CLASS, InputArgument::OPTIONAL, 'An entity class name')
            ->addOption(self::OPTION_ONLY_NEW, null, InputOption::VALUE_NONE,
                'Only for new entities that do not have schemas already.');
    }

    protected function init()
    {
        parent::init();
        $this->fileSystemHelper = $this->getShared(FilesystemHelperInterface::class);
        $this->classFetcher = $this->getShared(ClassFetcherInterface::class);

    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $class = $input->getArgument(self::ARGUMENT_CLASS);

        if ($class) {
            $this->generateJsonSchema($class, $input, $output);
            return 0;
        }

        $blueprints = $this->classFetcher->fetchClasses('\Infrastructure\Domain', AbstractBlueprint::class);
        foreach ($blueprints as $blueprintClass) {
            /** @var AbstractBlueprint $blueprint */
            $blueprint = $this->getShared($blueprintClass);
            $this->generateJsonSchema($blueprint->getEntityClass(), $input, $output);
        }

        return 0;
    }

    protected function generateJsonSchema(string $class, InputInterface $input, OutputInterface $output): void
    {
        $onlyNew = $input->getOption(self::OPTION_ONLY_NEW);
        $path = $this->getPath($class);
        $schema = $this->getShared(SchemaGeneratorInterface::class)->generate($class);
        if ($this->fileSystemHelper->fileExists($path)) {
            if ($onlyNew) {
                return;
            }
            $question = new ConfirmationQuestion(sprintf('File %s already exists, overwrite?', $path));
            if ($this->getHelper('question')->ask($input, $output, $question)) {
                $this->fileSystemHelper->writeToFile($path, $schema);
                $output->writeln(sprintf('Schema "%s" has been generated!', $path));
            }
        } else {
            $this->fileSystemHelper->writeToFile($path, $schema);
            $output->writeln(sprintf('Schema "%s" has been generated!', $path));
        }
    }

    protected function getPath(string $class): string
    {
        $reelection = new ReflectionClass($class);
        $result = $reelection->getFileName();
        return str_replace(
            ['Domain', $reelection->getShortName() . '.php'],
            ['Infrastructure/Domain', 'schema.json'],
            $result
        );
    }
}
