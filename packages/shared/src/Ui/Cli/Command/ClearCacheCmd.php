<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Ui\Cli\Command;

use Grifix\Kit\Alias;
use Grifix\Kit\Cli\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearCacheCmd extends AbstractCommand
{
    public const NAME = 'shared:clear_cache';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Clears cache.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->getShared(Alias::SLOW_CACHE)->clear();
        $this->getShared(Alias::QUICK_CACHE)->clear();
        return 0;
    }
}
