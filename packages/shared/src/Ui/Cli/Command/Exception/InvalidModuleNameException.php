<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Ui\Cli\Command\Exception;

class InvalidModuleNameException extends \Exception
{
    protected string $moduleName;

    public function __construct(string $moduleName)
    {
        $this->moduleName = $moduleName;
        $this->message = 'Invalid module name "'.$moduleName.'"!';
        parent::__construct();
    }
}
