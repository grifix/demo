<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Ui\Cli\Command\Event\Subscriber;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Cli\CliInterface;
use Grifix\Kit\Event\Store\EventEnvelope;
use Grifix\Kit\Event\Store\EventStoreInterface;
use Grifix\Kit\Event\Subscription\EventSubscriptionInterface;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotActiveException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotResettableException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotSuspendedException;
use Grifix\Kit\Event\Subscription\Repository\EventSubscriptionFilter;
use Grifix\Kit\Event\Subscription\Repository\EventSubscriptionRepositoryInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\ProcessManager\Exception\ProcessAlreadyExistsException;
use Grifix\Kit\ProcessManager\Exception\ProcessNotExistsException;
use Grifix\Kit\ProcessManager\ProcessManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EventSubscribersCommand extends AbstractCommand
{
    public const NAME = 'shared:event:subscribers';

    protected const ARG_SUBSCRIBER_TYPE = 'subscriber_type';

    protected const ARG_SUBSCRIPTION_ID = 'subscription_id';

    protected const ARG_ACTION = 'action';

    protected const ACTION_STOP = 'stop';

    protected const ACTION_START = 'start';

    protected const ACTION_SUSPEND = 'suspend';

    protected const ACTION_WAKE_UP = 'wake_up';

    protected const ACTION_LIST = 'list';

    protected const ACTION_RESET = 'reset';

    protected EventSubscriptionRepositoryInterface $eventSubscriptionRepository;

    protected ProcessManagerInterface $processManager;

    protected CliInterface $cli;

    protected ClassHelperInterface $classHelper;

    protected EventStoreInterface $eventStore;

    protected function init()
    {
        parent::init();
        $this->eventSubscriptionRepository = $this->getShared(EventSubscriptionRepositoryInterface::class);
        $this->processManager = $this->getShared(ProcessManagerInterface::class);
        $this->cli = $this->getShared(CliInterface::class);
        $this->classHelper = $this->getShared(ClassHelperInterface::class);
        $this->eventStore = $this->getShared(EventStoreInterface::class);
    }

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Starts and stops event subscribers')
            ->addArgument(
                self::ARG_ACTION,
                InputArgument::REQUIRED,
                sprintf('Action %s', implode('|', $this->getAvailableActions()))
            )
            ->addArgument(
                self::ARG_SUBSCRIBER_TYPE,
                InputArgument::OPTIONAL,
                'Subscriber type'
            )
            ->addArgument(
                self::ARG_SUBSCRIPTION_ID,
                InputArgument::OPTIONAL,
                'Subscription id'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $action = $input->getArgument(self::ARG_ACTION);
        $subscriberType = $input->getArgument(self::ARG_SUBSCRIBER_TYPE);
        $subscriptionId = $input->getArgument(self::ARG_SUBSCRIPTION_ID);

        if (self::ACTION_LIST === $action) {
            foreach ($this->processManager->findProcesses('subscriber_') as $subscriber) {
                $eventSubscription = $this->eventSubscriptionRepository->getDto($this->extractSubscriptionId($subscriber));
                $output->writeln($eventSubscription->getId() . ' ' . $eventSubscription->getSubscriberType());
            }
            return 0;
        }

        if ($subscriberType && $subscriptionId) {
            $this->doAction(
                $this->eventSubscriptionRepository->get($subscriberType, $subscriptionId),
                $action,
                $output
            );
        } else {
            if ($action === self::ACTION_WAKE_UP) {
                $subscriptions = $this->getSuspendedSubscriptions();
            } else {
                $subscriptions = $this->getActiveSubscriptions();
            }
            foreach ($subscriptions as $subscription) {
                $this->doAction($subscription, $action, $output);
            }
        }

        return 0;
    }

    protected function doAction(EventSubscriptionInterface $subscription, string $action, OutputInterface $output): void
    {
        switch ($action) {
            case self::ACTION_START:
                $this->startSubscriberProcess($subscription, $output);
                break;
            case self::ACTION_STOP:
                $this->stopSubscriberProcess($subscription, $output);
                break;
            case self::ACTION_SUSPEND:
                $this->suspendSubscription($subscription, $output);
                break;
            case self::ACTION_WAKE_UP:
                $this->wakeUpSubscription($subscription, $output);
                break;
            case self::ACTION_RESET:
                $this->resetSubscription($subscription, $output);
                break;

            default:
                throw new \InvalidArgumentException(sprintf('Unknown action %s', $action));
        }
    }

    protected function resetSubscription(EventSubscriptionInterface $subscription, OutputInterface $output): void
    {
        try {
            $this->info(sprintf('Resetting subscription %s', $subscription->getAlias()), $output);
            $progressBar = new ProgressBar($output, $this->eventStore->countEvents());
            $progressBar->start();
            $subscription->reset(function (EventEnvelope $envelope) use ($output, $progressBar) {
                $progressBar->advance();
            });
            $progressBar->finish();
            $this->info(sprintf('Subscription %s has been reset!', $subscription->getAlias()), $output);
            $this->startSubscriberProcess($subscription, $output);
        } catch (SubscriptionIsNotResettableException $exception) {
            $this->warning($exception->getMessage(), $output);
        } catch (SubscriptionIsNotActiveException $exception) {
            $this->warning($exception->getMessage(), $output);
        }
    }

    protected function suspendSubscription(EventSubscriptionInterface $subscription, OutputInterface $output): void
    {
        try {
            $subscription->suspend();
            $this->info(sprintf('Subscription %s has been suspended!', $subscription->getAlias()), $output);
        } catch (SubscriptionIsNotActiveException $exception) {
            $this->warning($exception->getMessage(), $output);
        }
    }


    protected function wakeUpSubscription(EventSubscriptionInterface $subscription, OutputInterface $output): void
    {
        try {
            $subscription->wakeUp();
            $this->info(sprintf('Subscription %s has been woke up!', $subscription->getAlias()), $output);
            $this->startSubscriberProcess($subscription, $output);
        } catch (SubscriptionIsNotSuspendedException $exception) {
            $this->warning($exception->getMessage(), $output);
        }
    }

    protected function startSubscriberProcess(EventSubscriptionInterface $subscription, OutputInterface $output): void
    {
        if (false === $subscription->isActive()) {
            $this->warning(sprintf('Subscription "%s" is not active!', $subscription->getAlias()), $output);
            return;
        }
        $alias = $this->makeAlias($subscription->getAlias());
        try {
            $this->processManager->startProcess(
                $alias,
                $this->cli->createCommandString(
                    EventSubscriberProcessCommand::NAME,
                    $subscription->getSubscriberType(),
                    $subscription->getId()
                )
            );
            $this->info(sprintf('%s has been started!', $this->extractSubscriptionId($alias)), $output);
        } catch (ProcessAlreadyExistsException $exception) {
            $this->warning(
                sprintf(
                    '%s is already running!',
                    $this->extractSubscriptionId($exception->getProcessId())
                ),
                $output
            );
        }
    }

    protected function stopSubscriberProcess(EventSubscriptionInterface $subscription, OutputInterface $output): void
    {
        $alias = $this->makeAlias($subscription->getAlias());
        try {
            $this->processManager->stopProcess($alias);
            $this->info(sprintf('%s has been stopped!', $this->extractSubscriptionId($alias)), $output);
        } catch (ProcessNotExistsException $exception) {
            $this->warning(
                sprintf(
                    '%s is not running!',
                    $this->extractSubscriptionId($exception->getProcessId())
                ),
                $output
            );
        } finally {
            $subscription->stopListenEvents();
        }
    }

    protected function makeAlias(string $subscriptionAlias): string
    {
        return 'subscriber_' . $subscriptionAlias;
    }

    /**
     * @return EventSubscriptionInterface[]
     */
    protected function getActiveSubscriptions(): array
    {
        return $this->eventSubscriptionRepository->find(EventSubscriptionFilter::create()->onlyActive());
    }

    /**
     * @return EventSubscriptionInterface[]
     */
    protected function getSuspendedSubscriptions(): array
    {
        return $this->eventSubscriptionRepository->find(EventSubscriptionFilter::create()->onlySuspended());
    }

    protected function getAvailableActions(): array
    {
        return [
            self::ACTION_STOP,
            self::ACTION_START,
            self::ACTION_SUSPEND,
            self::ACTION_WAKE_UP,
            self::ACTION_RESET,
            self::ACTION_LIST
        ];
    }

    protected function extractSubscriptionId(string $processId): string
    {
        return str_replace('subscriber_', '', $processId);
    }
}
