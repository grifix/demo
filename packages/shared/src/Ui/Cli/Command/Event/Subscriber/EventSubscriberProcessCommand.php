<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Ui\Cli\Command\Event\Subscriber;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Event\Subscription\EventSubscriptionFactoryInterface;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsFinishedException;
use Grifix\Kit\Event\Subscription\Exception\SubscriptionIsNotActiveException;
use Grifix\Kit\Event\Subscription\Repository\EventSubscriptionRepositoryInterface;
use Grifix\Kit\Exception\ExceptionNormalizerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EventSubscriberProcessCommand extends AbstractCommand
{

    public const NAME = 'shared:event:subscriber_process';

    public const ARG_SUBSCRIBER_TYPE = 'subscriber_type';

    public const ARG_SUBSCRIPTION_ID = 'subscription_id';

    protected EventSubscriptionRepositoryInterface $eventSubscriptionRepository;

    protected EventSubscriptionFactoryInterface $eventSubscriptionFactory;

    protected LoggerInterface $logger;

    protected ExceptionNormalizerInterface $exceptionNormalizer;

    protected function init()
    {
        parent::init();
        $this->eventSubscriptionRepository = $this->getShared(EventSubscriptionRepositoryInterface::class);
        $this->eventSubscriptionFactory = $this->getShared(EventSubscriptionFactoryInterface::class);
        $this->logger = $this->getShared(LoggerInterface::class);
        $this->exceptionNormalizer = $this->getShared(ExceptionNormalizerInterface::class);
    }

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Starts event subscriber')
            ->addArgument(
                self::ARG_SUBSCRIBER_TYPE,
                InputArgument::REQUIRED,
                'Subscriber type',
            )
            ->addArgument(
                self::ARG_SUBSCRIPTION_ID,
                InputArgument::REQUIRED,
                'Subscriber id',
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $subscriberType = $input->getArgument(self::ARG_SUBSCRIBER_TYPE);
        $subscriptionId = $input->getArgument(self::ARG_SUBSCRIPTION_ID);
        try {
            $this->logger->debug(sprintf('Event subscriber "%s:%s" has been started!', $subscriberType, $subscriptionId));
            $this->eventSubscriptionRepository->get($subscriberType, $subscriptionId)->listenEvents();
        } catch (SubscriptionIsNotActiveException $exception) {
            $this->logger->warning($exception->getMessage());
            return 0;
        } catch (SubscriptionIsFinishedException $exception) {
            $this->logger->warning($exception->getMessage());
            return 0;
        } catch (\Throwable $exception) {
            $this->logger->critical($exception->getMessage(), $this->exceptionNormalizer->normalize($exception));
            return 1;
        }
        return 0;
    }
}
