<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Ui\Cli\Command\Event\Starter;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Cli\CliInterface;
use Grifix\Kit\ProcessManager\ProcessManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EventSubscribersStartersCommand extends AbstractCommand
{
    public const NAME = 'shared:event:subscribers_starters';

    protected const ARG_ACTION = 'action';

    protected const ARG_QUANTITY = 'quantity';

    protected const ACTION_START = 'start';

    protected const ACTION_STOP = 'stop';

    protected const ACTION_LIST = 'list';

    protected const PROCESS_PREFIX = 'shared_event_subscribers_starter_';

    protected CliInterface $cli;

    protected ProcessManagerInterface $processManager;

    protected function init()
    {
        $this->cli = $this->getShared(CliInterface::class);
        $this->processManager = $this->getShared(ProcessManagerInterface::class);
        parent::init();
    }

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Starts and stops the subscribers starter.')
            ->addArgument(
                self::ARG_ACTION,
                InputArgument::REQUIRED,
                sprintf('Action %s', implode('|', $this->getAvailableActions()))
            )
            ->addArgument(
                self::ARG_QUANTITY,
                InputArgument::OPTIONAL,
                'quantity of processes',
                1
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $action = $input->getArgument(self::ARG_ACTION);
        $quantity = (int)$input->getArgument(self::ARG_QUANTITY);

        switch (true) {
            case self::ACTION_STOP === $action:
                $this->stop($output);
                break;
            case self::ACTION_START === $action:
                for ($i = 0; $i < $quantity; $i++) {
                    $processId = $this->createProcessId();
                    $this->processManager->startProcess(
                        $processId,
                        $this->cli->createCommandString(EventSubscribersStarterProcessCommand::NAME)
                    );
                    $this->info(sprintf('%s has been started.', $processId), $output);
                }

                break;
            case self::ACTION_LIST === $action:
                foreach ($this->processManager->findProcesses(self::PROCESS_PREFIX) as $starter) {
                    $output->writeln($starter);
                }
                break;
            default:
                throw new \InvalidArgumentException(sprintf('Unknown action %s', $action));
        }

        return 0;
    }

    protected function getAvailableActions(): array
    {
        return [
            self::ACTION_START,
            self::ACTION_STOP,
            self::ACTION_LIST
        ];
    }

    protected function createProcessId(): string
    {
        return uniqid(self::PROCESS_PREFIX);
    }

    protected function stop(OutputInterface $output): void
    {
        foreach ($this->processManager->findProcesses(self::PROCESS_PREFIX) as $processId) {
            $this->processManager->stopProcess($processId);
            $this->info(sprintf('%s has been stopped.', $processId), $output);
        }
    }
}
