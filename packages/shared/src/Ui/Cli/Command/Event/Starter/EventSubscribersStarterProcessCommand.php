<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Ui\Cli\Command\Event\Starter;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Event\SubscribersStarter\EventSubscribersStarterInterface;
use Grifix\Kit\Exception\ExceptionNormalizerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EventSubscribersStarterProcessCommand extends AbstractCommand
{
    public const NAME = 'shared:event:subscriber_starter_process';

    protected EventSubscribersStarterInterface $starter;
    protected LoggerInterface $logger;
    protected ExceptionNormalizerInterface $exceptionNormalizer;


    protected function init()
    {
        $this->starter = $this->getShared(EventSubscribersStarterInterface::class);
        $this->logger = $this->getShared(LoggerInterface::class);
        $this->exceptionNormalizer = $this->getShared(ExceptionNormalizerInterface::class);
        parent::init();
    }

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Starts event subscribers starter process');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->logger->debug('Event subscriber starter has been started');
            $this->starter->start();
        } catch (\Throwable $exception) {
            $this->logger->critical(
                $exception->getMessage(),
                $this->exceptionNormalizer->normalize($exception)
            );
            return 1;
        }

        return 0;
    }
}
