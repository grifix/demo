<?php

/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Ui\Http\Route;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\Handler\AbstractRouteHandler;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\Ui\Action\ActionDispatcherInterface;
use Grifix\Kit\View\Renderer\RendererFactoryInterface;
use Grifix\Kit\View\Skin\SkinFactoryInterface;
use Grifix\Kit\View\Skin\SkinInterface;
use Grifix\Kit\View\ViewFactoryInterface;
use Grifix\Shared\Ui\Http\Route\Exception\InvalidActionException;
use Grifix\Shared\Ui\Http\Route\Exception\InvalidRequestMethodException;
use Grifix\Shared\Ui\Http\Route\Exception\NoActionException;
use Grifix\Shared\Ui\Http\Route\Exception\NoRequestViewException;

class ActionRouteHandler extends AbstractRouteHandler
{

    protected const ACTION = 'action';
    protected const ACTIONS = 'actions';
    protected const VIEW = 'view';
    protected const PARAMS = 'params';

    /**
     * @param RouteInterface $route
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws InvalidActionException
     * @throws InvalidRequestMethodException
     * @throws NoActionException
     * @throws NoRequestViewException
     * @throws \Grifix\Kit\Ui\Action\Exception\CannotInvokeActionHandlerException
     * @throws \Grifix\Kit\View\Renderer\Exception\UnknownContentTypeException
     */
    public function __invoke(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $this->checkMethod($request->getMethod());
        if ($route->getParam(self::ACTION) || $route->getParam(self::VIEW)) {
            return $this->handleSingleAction($route, $request, $response);
        } elseif ($request->getQueryParam(self::ACTIONS)) {
            return $this->handleMultipleActions($request, $request->getQueryParam(self::ACTIONS), $response);
        } elseif ($request->getParsedBodyParam(self::ACTIONS)) {
            return $this->handleMultipleActions($request, $request->getParsedBodyParam(self::ACTIONS), $response);
        }
        throw new InvalidActionException();
    }

    /**
     * @param RouteInterface $route
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws NoActionException
     * @throws NoRequestViewException
     * @throws \Grifix\Kit\Ui\Action\Exception\CannotInvokeActionHandlerException
     */
    protected function handleMultipleActions(
        ServerRequestInterface $request,
        array $actions,
        ResponseInterface $response
    ): ResponseInterface {
        $results = [];
        foreach ($actions as $action) {
            if (!isset($action[self::ACTION])) {
                throw new NoActionException();
            }
            if (!isset($action[self::PARAMS])) {
                $action[self::PARAMS] = [];
            }
            $data = $this->getShared(ActionDispatcherInterface::class)->dispatch(
                $action[self::ACTION],
                $action[self::PARAMS],
                false === in_array($request->getMethod(), ['GET', 'HEAD'])
            );
            if (isset($action[self::VIEW])) {
                $results[] = $this->getShared(ViewFactoryInterface::class)->create($action[self::VIEW])->render(
                    array_merge($action[self::PARAMS], $data)
                );
            } else {
                $results[] = $data;
            }
        }

        return $response->withHeader('content-type', 'application/json')->withContent(json_encode($results));
    }

    /**
     * @param string $method
     *
     * @return void
     * @throws InvalidRequestMethodException
     */
    protected function checkMethod(string $method)
    {
        if (!in_array($method, ['GET', 'POST'])) {
            throw new InvalidRequestMethodException($method);
        }
    }

    /**
     * @param RouteInterface $route
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Grifix\Kit\Ui\Action\Exception\CannotInvokeActionHandlerException
     * @throws \Grifix\Kit\View\Renderer\Exception\UnknownContentTypeException
     */
    protected function handleSingleAction(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $viewAlias = $route->getParam(self::VIEW, 'grifix.shared.json.prt.default');
        if ($viewAlias == 'json') {
            $viewAlias = 'grifix.shared.json.prt.default';
        }
        $parsedBody = $request->getParsedBody();
        if (!is_array($parsedBody)) {
            $parsedBody = [];
        }
        $params = array_merge($request->getQueryParams(), $parsedBody, $request->getUploadedFiles());

        $data = [];
        if ($route->getParam(self::ACTION)) {
            $data = $this->getShared(ActionDispatcherInterface::class)->dispatch(
                $route->getParam(self::ACTION),
                $params,
                false === in_array($request->getMethod(), ['GET', 'HEAD'])
            );
        }

        $view = $this->getShared(ViewFactoryInterface::class)->create($viewAlias);
        $renderer = $this->getShared(RendererFactoryInterface::class)->createRenderer(
            $this->detectSkin($viewAlias)->getContentType()
        );

        return $renderer->render($response, $view, array_merge($request->getQueryParams(), $parsedBody, $data));
    }

    /**
     * @param string $viewAlias
     *
     * @return SkinInterface
     */
    protected function detectSkin(string $viewAlias): SkinInterface
    {
        $skinName = explode('.', $viewAlias)[2];
        if ($skinName == '{skin}') {
            return $this->getShared(ViewFactoryInterface::class)->getSkin();
        }

        return $this->getShared(SkinFactoryInterface::class)->create($skinName);
    }
}
