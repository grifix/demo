<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Ui\Http\Route\Exception;

class InvalidRequestMethodException extends \Exception
{
    protected $method;
    
    /**
     * InvalidRequestMethodException constructor.
     *
     * @param string $method
     */
    public function __construct(string $method)
    {
        $this->method = $method;
        $this->method = 'Request method can be only "GET" or "POST", "'.$method.'" given!"';
        parent::__construct();
    }
}
