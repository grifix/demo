<?php declare(strict_types=1);

namespace Grifix\Shared\Ui\Http\Action;

use Grifix\Kit\Conversion\Converter\ConverterFactoryInterface;
use Grifix\Kit\Conversion\Converter\StringToFloatConverter;
use Grifix\Kit\Conversion\Converter\StringToIntConverter;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Helper\OffsetCalculatorInterface;
use Grifix\Kit\Http\CookieInterface;
use Grifix\Kit\Intl\Locale\LocaleInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Type\DateTime\DateTime;
use Grifix\Kit\Ui\Action\AbstractActionHandler as BaseActionHandler;
use Grifix\Shared\Application\Client\Acl\AclClientInterface;
use Grifix\Shared\Application\Client\Acl\Dto\UserDto;
use Grifix\Shared\Application\Common\Filter\AbstractFilter;
use Grifix\Shared\Application\Common\Filter\ColumnFilter\DateColumnFilter;
use Grifix\Shared\Application\Common\Filter\ColumnFilter\IntColumnFilter;
use Grifix\Shared\Application\Query\GetFreeId\GetFreeUuidQuery;
use Grifix\Shared\Application\Query\HasAccess\HasAccessQuery;
use Grifix\Shared\Domain\File\FileDto;
use Grifix\Shared\Ui\Common\FileDtoFactory\FileDtoFactoryInterface;
use Grifix\Shared\Ui\Http\Action\Exception\ColumnFilterIsNotDefinedException;
use Grifix\Shared\Ui\Common\ColumnFilterFactory\ColumnFilterFactoryInterface;
use Grifix\Shared\Ui\Common\RequestValidator\RequestValidatorFactoryInterface;
use Grifix\Shared\Ui\Common\RequestValidator\RequestValidatorInterface;
use Psr\Http\Message\UploadedFileInterface;

abstract class AbstractActionHandler extends BaseActionHandler
{
    public const PARAM_ID = 'id';
    public const PARAM_PAGE_INDEX = 'pageIndex';
    public const PARAM_PAGE_SIZE = 'pageSize';
    public const PARAM_COUNT_TOTAL = 'countTotal';
    public const PARAM_LANG_CODE = 'langCode';
    public const PARAM_SORT_COLUMN = 'sortColumn';
    public const PARAM_SORT_DIRECTION = 'sortDirection';
    public const PARAM_COLUMN_FILTERS = 'columnFilters';
    public const PARAM_RESULT = 'result';

    protected ClassHelperInterface $classHelper;

    protected FileDtoFactoryInterface $fileDtoFactory;

    protected bool $hasSideEffects = true;

    protected function init(): void
    {
        parent::init();
        $this->classHelper = $this->getShared(ClassHelperInterface::class);
        $this->fileDtoFactory = $this->getShared(FileDtoFactoryInterface::class);
    }

    public function hasSideEffects(): bool
    {
        return $this->hasSideEffects;
    }

    public static function getAlias(): string
    {
        $path = explode('\\', static::class);
        if ($path[0] === 'App') {
            array_shift($path);
        }
        $path[2] = null;
        $path[3] = null;
        $path[4] = null;

        $result = [];
        foreach ($path as $val) {
            if (null !== $val) {
                $result[] = lcfirst($val);
            }
        }
        $result = implode('.', $result);
        $result = str_replace('ActionHandler', '', $result);
        return $result;
    }


    protected function getFreeId(): string
    {
        return $this->executeQuery(new GetFreeUuidQuery());
    }

    protected function createFileDto(UploadedFileInterface $uploadedFile): FileDto
    {
        return $this->fileDtoFactory->createFileDtoFromUploadFile($uploadedFile);
    }

    protected function getUploadedFilePath(UploadedFileInterface $uploadedFile): string
    {
        return $uploadedFile->getStream()->getMetadata()['uri'];
    }

    protected function createRequestValidator(string $validationClass): RequestValidatorInterface
    {
        return $this->getShared(RequestValidatorFactoryInterface::class)->create($validationClass);
    }

    protected function calculateOffset(?int $pageIndex, ?int $pageSize): ?int
    {
        return $this->getShared(OffsetCalculatorInterface::class)->calculateOffset($pageIndex, $pageSize);
    }

    protected function createTimestamp($date): ?int
    {
        if (!empty($date)) {
            return (new DateTime($date))->getTimestamp();
        }
        return null;
    }

    protected function convertToInt($value): ?int
    {
        return $this->getShared(ConverterFactoryInterface::class)
            ->create(StringToIntConverter::class)
            ->convert($value);
    }

    protected function convertToFloat($value): ?float
    {
        return $this->getShared(ConverterFactoryInterface::class)
            ->create(StringToFloatConverter::class)
            ->convert($value);
    }

    protected function getCurrentLangCode(): string
    {
        return $this->getShared(LocaleInterface::class)->getLang()->getCode();
    }

    protected function getCurrentLocaleCode(): string
    {
        return $this->getShared(LocaleInterface::class)->getCode();
    }

    protected function getSession(): SessionInterface
    {
        return $this->getShared(SessionInterface::class);
    }

    protected function hasAccess(string $permission): bool
    {
        return $this->executeQuery(new HasAccessQuery($this->getSession()->getId(), $permission));
    }

    protected function applyColumnFilters(
        AbstractFilter $filter,
        array $columnFiltersClasses,
        array $columnFilters
    ): void {
        foreach ($columnFilters as $columnFilter) {
            if (!isset($columnFiltersClasses[$columnFilter['column']])) {
                throw new ColumnFilterIsNotDefinedException($columnFilter['column']);
            }
            $filterClass = $columnFiltersClasses[$columnFilter['column']];
            $value = $columnFilter['value'];
            if ($this->classHelper->isInstanceOf($filterClass, IntColumnFilter::class)
                || $this->classHelper->isInstanceOf($filterClass, DateColumnFilter::class)) {
                $value = (int)$value;
            }
            $filter->withColumnFilter($this->getShared(ColumnFilterFactoryInterface::class)->createColumnFilter(
                $filterClass,
                $value,
                $columnFilter['condition']
            ));
        }
    }

    protected function getCookie(string $name, ?string $defaultValue = null): string
    {
        $this->getShared(CookieInterface::class)->get($name, $defaultValue);
    }

    protected function setCookie(string $name, ?string $value)
    {
        $this->getShared(CookieInterface::class)->set($name, $value);
    }

    protected function getFirstElement(array $array)
    {
        return $this->getShared(ArrayHelperInterface::class)->getFirstElement($array);
    }

    protected function getLastElement(array $array)
    {
        return $this->getShared(ArrayHelperInterface::class)->getLastElement($array);
    }

    protected function getCurrentUser(): UserDto
    {
        return $this->getShared(AclClientInterface::class)->getSignedInUser($this->getSession()->getId());
    }
}
