<?php declare(strict_types = 1);

namespace Grifix\Shared\Ui\Http\Action\Exception;

use Exception;

class ColumnFilterIsNotDefinedException extends Exception
{
    public function __construct(string $column)
    {
        parent::__construct(sprintf('Column filter for column "%s" is not defined!', $column));
    }
}
