<?php declare(strict_types = 1);

namespace Grifix\Shared;

interface Alias
{
    public const PUBLIC_FILESYSTEM = 'public_filesystem';
}
