<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Url;

interface UrlFactoryInterface
{
    public function createUrl(string $value): UrlInterface;
}
