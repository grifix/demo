<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Url\Exception;

use DomainException;

class InvalidUrlException extends DomainException
{

    /***
     * @var string
     */
    protected $url;

    /**
     * InvalidUrlException constructor.
     * @param $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
        parent::__construct(sprintf('Invalid url %s', $url));
    }
}
