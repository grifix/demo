<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Url;

use Grifix\Kit\Kernel\AbstractFactory;

class UrlFactory extends AbstractFactory implements UrlFactoryInterface
{
    public function createUrl(string $value):UrlInterface
    {
        $class = $this->makeClassName(Url::class);
        return new $class($value);
    }
}
