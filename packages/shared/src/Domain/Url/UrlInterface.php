<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Url;

interface UrlInterface
{
    public function withValue(string $value): UrlInterface;
}
