<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Url;

use Grifix\Shared\Domain\StringableInterface;
use Grifix\Shared\Domain\Url\Exception\InvalidUrlException;

class Url implements UrlInterface, StringableInterface
{

    protected string $value;

    public function __construct(string $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            throw new InvalidUrlException($value);
        }
        $this->value = $value;
    }

    public function withValue(string $value): UrlInterface
    {
        return new self($value);
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
