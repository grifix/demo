<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain;

/**
 * Interface EventPublisherInterface
 * @package Grifix\Kit\Orm\EventCollector
 */
interface EventPublisherInterface
{

    public function publishEvent(object $event, object $aggregate): void;
}
