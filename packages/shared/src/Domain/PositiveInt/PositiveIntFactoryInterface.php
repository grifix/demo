<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\PositiveInt;

use Grifix\Shared\Domain\PositiveInt\Exception\ValueMustBePositiveException;

interface PositiveIntFactoryInterface
{
    /**
     * @throws ValueMustBePositiveException
     */
    public function createPositiveInt(int $value): PositiveIntInterface;
}
