<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\PositiveInt;

use Grifix\Shared\Domain\PositiveInt\Exception\ValueMustBePositiveException;

class PositiveInt implements PositiveIntInterface
{
    protected int $value;

    /**
     * @throws ValueMustBePositiveException
     */
    public function __construct(int $value)
    {
        if ($value < 0) {
            throw new ValueMustBePositiveException();
        }
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function increase(int $number): static
    {
        $number = new self($number);
        return new self($this->value + $number->getValue());
    }

    /**
     * @inheritDoc
     */
    public function decrease(int $number): static
    {
        $number = new self($number);
        return new self($this->value - $number->getValue());
    }
}
