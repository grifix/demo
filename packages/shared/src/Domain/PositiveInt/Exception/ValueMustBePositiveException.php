<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\PositiveInt\Exception;

use DomainException;

class ValueMustBePositiveException extends DomainException
{
}
