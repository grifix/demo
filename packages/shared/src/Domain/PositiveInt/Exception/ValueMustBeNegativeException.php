<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Domain\PositiveInt\Exception;

use DomainException;

class ValueMustBeNegativeException extends DomainException
{

}
