<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\PositiveInt;

use Grifix\Shared\Domain\PositiveInt\Exception\ValueMustBePositiveException;

interface PositiveIntInterface
{
    public function getValue(): int;

    public function increase(int $number): static;

    /**
     * @throws ValueMustBePositiveException
     */
    public function decrease(int $number): static;
}
