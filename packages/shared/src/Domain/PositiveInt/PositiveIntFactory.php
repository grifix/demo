<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\PositiveInt;

use Grifix\Kit\Kernel\AbstractFactory;

class PositiveIntFactory extends AbstractFactory implements PositiveIntFactoryInterface
{
    public function createPositiveInt(int $value): PositiveIntInterface
    {
        $class = $this->makeClassName(PositiveInt::class);
        return new $class($value);
    }
}
