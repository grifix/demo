<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\DateTime;

use Grifix\Kit\Type\DateTime\DateTimeInterface;

interface DateOutsideInterface
{
    public function createDateTime(?int $timestamp): DateTimeInterface;
}
