<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\DateTime;

interface DateTimeFactoryInterface
{
    public function createDateTime(?int $timestamp = null): DateTimeInterface;
}
