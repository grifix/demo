<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\DateTime;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class DateTimeFactory extends AbstractFactory implements DateTimeFactoryInterface
{
    /** @var DateOutsideInterface */
    protected DateOutsideInterface $dateInfrastructure;

    public function __construct(ClassMakerInterface $classMaker, DateOutsideInterface $dateInfrastructure)
    {
        parent::__construct($classMaker);
        $this->dateInfrastructure = $dateInfrastructure;
    }

    public function createDateTime(?int $timestamp = null): DateTimeInterface
    {
        return new DateTime($this->dateInfrastructure, $timestamp);
    }
}
