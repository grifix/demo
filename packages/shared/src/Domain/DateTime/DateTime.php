<?php
/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Grifix\Shared\Domain\DateTime;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Kit\Type\DateTime\DateTimeInterface as PhpDateTimeInterface;

class DateTime implements DateTimeInterface
{
    protected PhpDateTimeInterface $value;

    public function __construct(
        #[Dependency]
        protected DateOutsideInterface $outside,
        ?int $timestamp = null
    )
    {
        $this->value = $outside->createDateTime($timestamp);
        $this->outside = $outside;
    }

    public function toPhpDateTime(): PhpDateTimeInterface
    {
        return clone $this->value;
    }

    public function withTimestamp(int $timestamp): DateTimeInterface
    {
        return new self($this->outside, $timestamp);
    }
}
