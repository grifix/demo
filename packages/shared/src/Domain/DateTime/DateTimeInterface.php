<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\DateTime;

use DateTimeInterface as PhpDateTimeInterface;

interface DateTimeInterface
{
    public function toPhpDateTime(): PhpDateTimeInterface;

    public function withTimestamp(int $timestamp): DateTimeInterface;
}
