<?php

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types=1);

namespace Grifix\Shared\Domain\Currency;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\Currency\Exception\CurrencyIsNotAvailableException;

class Currency implements CurrencyInterface
{
    public function __construct(
        #[Dependency]
        protected CurrencyOutsideInterface $outside,
        protected string $value
    )
    {
        $this->outside = $outside;
        if (!$this->outside->isAvailable($value)) {
            throw new CurrencyIsNotAvailableException($value);
        }
        $this->value = $value;
    }

    public function withCode(string $newCode): CurrencyInterface
    {
        return new self($this->outside, $newCode);
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
