<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Currency;

interface CurrencyFactoryInterface
{
    public function create(string $code): CurrencyInterface;
}
