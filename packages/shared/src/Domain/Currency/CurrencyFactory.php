<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Currency;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class CurrencyFactory extends AbstractFactory implements CurrencyFactoryInterface
{
    /** @var CurrencyOutsideInterface */
    protected $currencyInfrastructure;

    public function __construct(
        ClassMakerInterface $classMaker,
        CurrencyOutsideInterface $currencyInfrastructure
    ) {
        parent::__construct($classMaker);
        $this->currencyInfrastructure = $currencyInfrastructure;
    }

    public function create(string $code): CurrencyInterface
    {
        $class = $this->makeClassName(Currency::class);
        return new $class($this->currencyInfrastructure, $code);
    }
}
