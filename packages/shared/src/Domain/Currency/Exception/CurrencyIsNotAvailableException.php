<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Currency\Exception;

use DomainException;

class CurrencyIsNotAvailableException extends DomainException
{

    public function __construct(string $currencyCode)
    {
        parent::__construct(sprintf('Currency %s is not available!', $currencyCode));
    }
}
