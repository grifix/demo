<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Currency;

interface CurrencyInterface
{
    public const PLN = 'PLN';
    public const USD = 'USD';
    public const EUR = 'EUR';

    public function withCode(string $newCode): CurrencyInterface;
}
