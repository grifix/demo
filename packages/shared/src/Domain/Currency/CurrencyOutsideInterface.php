<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Currency;

interface CurrencyOutsideInterface
{
    public function isAvailable(string $code):bool;
}
