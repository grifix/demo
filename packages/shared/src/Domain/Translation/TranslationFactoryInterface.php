<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Translation;

interface TranslationFactoryInterface
{
    /**
     * @param string[] $values
     * @return TranslationInterface
     */
    public function createTranslation(array $values): TranslationInterface;
}
