<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Translation;

interface TranslationOutsideInterface
{
    public function getLangCodes(): array;
}
