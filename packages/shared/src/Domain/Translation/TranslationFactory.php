<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Translation;

use Grifix\Shared\Infrastructure\Domain\Translation\TranslationOutside;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class TranslationFactory extends AbstractFactory implements TranslationFactoryInterface
{
    /**
     * @var TranslationOutside
     */
    protected $translationInfrastructure;

    public function __construct(ClassMakerInterface $classMaker, TranslationOutside $translationInfrastructure)
    {
        $this->translationInfrastructure = $translationInfrastructure;
        parent::__construct($classMaker);
    }


    /**
     * @param string[] $values
     * @return TranslationInterface
     */
    public function createTranslation(array $values): TranslationInterface
    {
        $class = $this->makeClassName(Translation::class);
        return new $class($this->translationInfrastructure, $values);
    }
}
