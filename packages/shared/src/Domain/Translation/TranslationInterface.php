<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Translation;

interface TranslationInterface
{

    /**
     * @return self
     */
    public function withValues(array $values);
}
