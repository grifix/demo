<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Translation;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\AssocArray;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\Translation\Exception\InvalidTranslationException;

class Translation implements TranslationInterface
{

    /**
     * @var string[] $values
     */
    public function __construct(
        #[Dependency]
        protected TranslationOutsideInterface $outside,
        protected array $values
    )
    {
        $this->outside = $outside;
        $this->assertLangCodes($values);
        $this->values = $values;
    }

    public function withValues(array $values)
    {
        return new static($this->outside, $values);
    }

    protected function assertLangCodes(array $values)
    {
        $allLangCodes = $this->outside->getLangCodes();
        sort($allLangCodes);
        $langCodes = array_keys($values);
        sort($langCodes);
        if ($allLangCodes !== $langCodes) {
            throw new InvalidTranslationException($values);
        }
    }

    protected function assertMaxChars(array $values, int $maxChars, callable $handler): void
    {
        foreach ($values as $langCode => $value) {
            if (mb_strlen($value) > $maxChars) {
                $handler($langCode);
            }
        }
    }

    protected function assertMinChars(array $values, int $minChars, callable $handler): void
    {
        foreach ($values as $langCode => $value) {
            if (mb_strlen($value) < $minChars) {
                $handler($langCode);
            }
        }
    }
}
