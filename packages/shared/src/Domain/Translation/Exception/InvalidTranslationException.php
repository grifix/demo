<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Translation\Exception;

use DomainException;

class InvalidTranslationException extends DomainException
{

    /**
     * @var string[]
     */
    protected $translation;

    /**
     * InvalidTranslationException constructor.
     * @param string[] $translation
     */
    public function __construct(array $translation)
    {
        $this->translation = $translation;
        parent::__construct('Invalid translation!');
    }
}

