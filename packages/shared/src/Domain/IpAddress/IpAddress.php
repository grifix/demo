<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\IpAddress;

use Grifix\Shared\Domain\IpAddress\Exception\InvalidIpAddressException;

class IpAddress implements IpAddressInterface
{
    protected $value;

    public function __construct(string $value)
    {
        if (!is_null($value) && filter_var($value, FILTER_VALIDATE_IP) === false) {
            throw new InvalidIpAddressException($value);
        }
        $this->value = $value;
    }
}
