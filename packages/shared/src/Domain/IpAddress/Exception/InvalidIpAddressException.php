<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\IpAddress\Exception;

use DomainException;

class InvalidIpAddressException extends DomainException
{
    /**
     * @var string
     */
    protected $ipAddress;

    /**
     * @param string $ipAddress
     */
    public function __construct(string $ipAddress)
    {
        $this->ipAddress = $ipAddress;
        parent::__construct(sprintf('Invalid ip address %s!', $ipAddress));
    }
}
