<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\IpAddress;

interface IpAddressFactoryInterface
{
    public function createIpAddress(string $value): IpAddressInterface;
}
