<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\IpAddress;

use Grifix\Kit\Kernel\AbstractFactory;

class IpAddressFactory extends AbstractFactory implements IpAddressFactoryInterface
{
    public function createIpAddress(string $value): IpAddressInterface
    {
        $class = $this->makeClassName(IpAddress::class);
        return new $class($value);
    }
}
