<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Money;

use Grifix\Shared\Domain\Currency\CurrencyInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;

interface MoneyInterface extends PositiveIntInterface
{
    public function withAmount(int $newAmount): MoneyInterface;

    public function withCurrency(CurrencyInterface $currency): MoneyInterface;
}
