<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Money;

interface MoneyFactoryInterface
{
    public function createMoney(int $amount, string $currencyCode): MoneyInterface;
}
