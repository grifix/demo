<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Money;

class MoneyDto
{
    /** @var int */
    private $amount;

    /** @var string */
    private $currencyCode;

    /**
     * MoneyDto constructor.
     * @param int $amount
     * @param string $currencyCode
     */
    public function __construct(int $amount, string $currencyCode)
    {
        $this->amount = $amount;
        $this->currencyCode = $currencyCode;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }
}
