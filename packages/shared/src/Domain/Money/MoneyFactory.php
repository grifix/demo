<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain\Money;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Shared\Domain\Currency\CurrencyFactoryInterface;

class MoneyFactory extends AbstractFactory implements MoneyFactoryInterface
{
    /** @var CurrencyFactoryInterface */
    protected $currencyFactory;

    public function __construct(ClassMakerInterface $classMaker, CurrencyFactoryInterface $currencyFactory)
    {
        parent::__construct($classMaker);
        $this->currencyFactory = $currencyFactory;
    }

    public function createMoney(int $amount, string $currencyCode): MoneyInterface
    {
        $class = $this->makeClassName(Money::class);
        return new $class($amount, $this->currencyFactory->create($currencyCode));
    }
}
