<?php

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types=1);

namespace Grifix\Shared\Domain\Money;

use Grifix\Shared\Domain\Currency\CurrencyInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveInt;

class Money extends PositiveInt implements MoneyInterface
{
    public function __construct(int $value, protected CurrencyInterface $currency)
    {
        parent::__construct($value);
    }

    public function withAmount(int $newAmount): MoneyInterface
    {
        return new self($newAmount, $this->currency);
    }

    public function withCurrency(CurrencyInterface $currency): MoneyInterface
    {
        return new self($this->value, $currency);
    }
}
