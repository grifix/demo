<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\PositiveFloat;

interface PositiveFloatInterface
{
    public function getValue(): float;
}
