<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\PositiveFloat;

use Grifix\Kit\Kernel\AbstractFactory;

class PositiveFloatFactory extends AbstractFactory implements PositiveFloatFactoryInterface
{
    public function create(float $value): PositiveFloatInterface
    {
        $class = $this->makeClassName(PositiveFloat::class);
        return new $class($value);
    }
}
