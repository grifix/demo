<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\PositiveFloat\Exception;

use DomainException;

class ValueIsNotPositiveException extends DomainException
{
}
