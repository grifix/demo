<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\PositiveFloat;

use Grifix\Shared\Domain\PositiveFloat\Exception\ValueIsNotPositiveException;

class PositiveFloat implements PositiveFloatInterface
{
    /** @var float */
    protected $value;

    public function __construct(float $value)
    {
        if ($value < 0) {
            throw new ValueIsNotPositiveException();
        }
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
