<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\PositiveFloat;

interface PositiveFloatFactoryInterface
{
    public function create(float $value): PositiveFloatInterface;
}
