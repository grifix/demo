<?php /** @noinspection PhpMissingFieldTypeInspection */

/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types=1);

namespace Grifix\Shared\Domain\File;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\File\Exception\FileIsNotImageException;
use Grifix\Shared\Domain\File\Exception\FileNotExistsException;
use Grifix\Shared\Domain\File\Exception\ImageHeightTooBigException;
use Grifix\Shared\Domain\File\Exception\ImageHeightTooSmallException;
use Grifix\Shared\Domain\File\Exception\ImageWidthTooBigException;
use Grifix\Shared\Domain\File\Exception\ImageWidthTooSmallException;
use Grifix\Shared\Domain\File\Exception\InvalidImageHeightException;
use Grifix\Shared\Domain\File\Exception\InvalidImageWidthException;
use Grifix\Shared\Domain\File\Exception\InvalidMimeTypeException;
use Grifix\Shared\Domain\File\Exception\TooBigFileException;
use Grifix\Shared\Domain\File\Exception\TooSmallFileException;
use Grifix\Shared\Domain\File\Infrastructure\FileOutsideInterface;

class File implements FileInterface
{
    protected string $mimeType;

    protected ?string $path;

    protected FileRestrictionsDto $restrictions;

    public function __construct(
        #[Dependency]
        protected FileOutsideInterface $outside,
        string $sourcePath,
        protected string $name,
        protected string $directory,
        ?FileRestrictionsDto $restrictions = null
    ) {
        $this->outside = $outside;
        $this->name = $name;
        $this->directory = $directory;
        $this->restrictions = $restrictions ?? new FileRestrictionsDto();
        $this->upload($sourcePath);
    }

    protected function validate(string $sourcePath): void
    {
        $this->assertFileExists($sourcePath);

        if ($this->restrictions->getTypes()) {
            $this->assertMimeType($sourcePath, $this->restrictions->getTypes());
        }

        if ($this->restrictions->mustBeImage()) {
            $this->assertIsImage($sourcePath);
        }

        if ($this->restrictions->getMinSize()) {
            $this->assertMinFileSize($sourcePath, $this->restrictions->getMinSize());
        }

        if ($this->restrictions->getMaxSize()) {
            $this->assertMaxFileSize($sourcePath, $this->restrictions->getMaxSize());
        }

        if ($this->restrictions->getMinImageHeight()) {
            $this->assertMinImageHeight($sourcePath, $this->restrictions->getMinImageHeight());
        }

        if ($this->restrictions->getMaxImageHeight()) {
            $this->assertMaxImageHeight($sourcePath, $this->restrictions->getMaxImageHeight());
        }

        if ($this->restrictions->getMinImageWidth()) {
            $this->assertMinImageWidth($sourcePath, $this->restrictions->getMinImageWidth());
        }

        if($this->restrictions->getMaxImageWidth()){
            $this->assertMaxImageWidth($sourcePath, $this->restrictions->getMaxImageWidth());
        }

        if ($this->restrictions->getImageHeight()) {
            $this->assertImageHeight($sourcePath, $this->restrictions->getImageHeight());
        }

        if ($this->restrictions->getImageWidth()) {
            $this->assertImageWidth($sourcePath, $this->restrictions->getImageWidth());
        }
    }

    protected function assertFileExists(string $sourcePath): void
    {
        if (!$this->outside->sourceFileExists($sourcePath)) {
            throw new FileNotExistsException($sourcePath);
        }
    }

    protected function upload(string $sourcePath): void
    {
        $this->validate($sourcePath);
        $this->mimeType = $this->outside->getMimeType($sourcePath);
        $destinationPath = $this->directory . DIRECTORY_SEPARATOR . $this->outside->extractName($this->name) . '.' . $this->outside->getExtension($this->mimeType);
        $this->outside->upload($sourcePath, $destinationPath);
        $this->path = $destinationPath;
    }

    public function change(string $sourcePath, ?string $name = null): static
    {
        $name = $name ?? $this->name;
        return new self($this->outside, $sourcePath, $name, $this->directory, $this->restrictions);
    }

    public function getPath(): string
    {
        return $this->path;
    }

    protected function assertMaxFileSize(string $sourcePath, int $maxSize): void
    {
        $size = $this->outside->getFileSize($sourcePath);
        if ($size > $maxSize) {
            throw new TooBigFileException($size, $maxSize);
        }
    }

    protected function assertMinFileSize(string $sourcePath, int $minSize): void
    {
        $size = $this->outside->getFileSize($sourcePath);
        if ($size < $minSize) {
            throw new TooSmallFileException($size, $minSize);
        }
    }

    /**
     * @param string[] $allowedMimeTypes
     */
    protected function assertMimeType(string $sourcePath, array $allowedMimeTypes): void
    {
        $mimeType = $this->outside->getMimeType($sourcePath);
        if (!in_array($mimeType, $allowedMimeTypes)) {
            throw new InvalidMimeTypeException($mimeType, $allowedMimeTypes);
        }
    }

    protected function assertIsImage(string $sourcePath): void
    {
        if (!$this->outside->isImage($sourcePath)) {
            throw new FileIsNotImageException();
        }
    }

    protected function assertMinImageWidth(string $sourcePath, int $minWidth): void
    {
        $width = $this->outside->getImageWidth($sourcePath);
        if ($width < $minWidth) {
            throw new ImageWidthTooSmallException($width, $minWidth);
        }
    }

    protected function assertMaxImageWidth(string $sourcePath, int $maxWidth): void
    {
        $width = $this->outside->getImageWidth($sourcePath);
        if ($width > $maxWidth) {
            throw new ImageWidthTooBigException($width, $maxWidth);
        }
    }

    protected function assertMaxImageHeight(string $sourcePath, int $maxHeight): void
    {
        $height = $this->outside->getImageHeight($sourcePath);
        if ($height > $maxHeight) {
            throw new ImageHeightTooBigException($height, $maxHeight);
        }
    }

    protected function assertMinImageHeight(string $sourcePath, int $minHeight): void
    {
        $height = $this->outside->getImageHeight($sourcePath);
        if ($height < $minHeight) {
            throw new ImageHeightTooSmallException($height, $minHeight);
        }
    }

    protected function assertImageWidth(string $sourcePath, int $requiredWidth): void
    {
        $actualWidth = $this->outside->getImageWidth($sourcePath);
        if ($actualWidth !== $requiredWidth) {
            throw new InvalidImageWidthException($actualWidth, $requiredWidth);
        }
    }

    protected function assertImageHeight(string $sourcePath, int $requiredHeight): void
    {
        $actualHeight = $this->outside->getImageHeight($sourcePath);
        if ($actualHeight !== $requiredHeight) {
            throw new InvalidImageHeightException($actualHeight, $requiredHeight);
        }
    }

    public function delete(): void
    {
        $this->outside->delete($this->path);
        $this->path = null;
    }
}
