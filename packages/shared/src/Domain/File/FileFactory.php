<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\File;

use Grifix\Shared\Domain\File\Infrastructure\FileOutsideInterface;

class FileFactory implements FileFactoryInterface
{
    public function __construct(protected FileOutsideInterface $infrastructure)
    {
    }

    public function createFile(
        string $sourcePath,
        string $name,
        string $directory,
        ?FileRestrictionsDto $restrictions = null
    ): FileInterface {
        return new File($this->infrastructure, $sourcePath, $name, $directory, $restrictions);
    }
}
