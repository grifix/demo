<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Infrastructure\Exception;

use DomainException;

class CannotUploadFileException extends DomainException
{
    public function __construct(string $sourcePath, string $destinationPath)
    {
        parent::__construct(sprintf('Cannot upload file from %s to %s!', $sourcePath, $destinationPath));
    }
}
