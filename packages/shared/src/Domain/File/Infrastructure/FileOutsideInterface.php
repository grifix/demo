<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Infrastructure;

use Grifix\Shared\Domain\File\Infrastructure\Exception\CannotUploadFileException;
use Grifix\Shared\Infrastructure\Domain\File\FileOutside;

interface FileOutsideInterface
{
    public function getFileSize(string $path): int;

    public function sourceFileExists(string $path): bool;

    /**
     * @throws CannotUploadFileException
     */
    public function upload(string $sourcePath, string $destinationPath): void;

    public function getMimeType(string $path): string;

    public function isImage(string $path): bool;

    public function getImageWidth(string $path): int;

    public function getImageHeight(string $path): int;

    public function delete(string $path): void;

    public function getExtension(string $mimeType): string;

    public function fileExists(string $path): bool;

    public function extractName(string $path): string;
}
