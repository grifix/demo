<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\File;

interface FileFactoryInterface
{

    public function createFile(
        string $sourcePath,
        string $name,
        string $directory,
        ?FileRestrictionsDto $restrictions = null
    ): FileInterface;
}
