<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Domain\File;

class FileRestrictionsDto
{

    public function __construct(
        protected ?int $minSize = null,
        protected ?int $maxSize = null,
        protected ?int $imageWidth = null,
        protected ?int $imageHeight = null,
        protected ?int $minImageWidth = null,
        protected ?int $maxImageWidth = null,
        protected ?int $minImageHeight = null,
        protected ?int $maxImageHeight = null,
        protected array $types = [],
        protected bool $mustBeImage = false,
    )
    {
    }

    public function getMinSize(): ?int
    {
        return $this->minSize;
    }

    public function getMaxSize(): ?int
    {
        return $this->maxSize;
    }

    public function getImageWidth(): ?int
    {
        return $this->imageWidth;
    }

    public function getImageHeight(): ?int
    {
        return $this->imageHeight;
    }

    public function getMinImageWidth(): ?int
    {
        return $this->minImageWidth;
    }

    public function getMaxImageWidth(): ?int
    {
        return $this->maxImageWidth;
    }

    public function getMinImageHeight(): ?int
    {
        return $this->minImageHeight;
    }

    public function getMaxImageHeight(): ?int
    {
        return $this->maxImageHeight;
    }

    public function getTypes(): array
    {
        return $this->types;
    }

    public function mustBeImage(): bool
    {
        return $this->mustBeImage;
    }
}
