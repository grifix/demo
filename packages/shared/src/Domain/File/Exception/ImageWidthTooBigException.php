<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class ImageWidthTooBigException extends DomainException
{
    protected $actualWidth;

    protected $maxWidth;

    public function __construct($actualWidth, $maxWidth)
    {
        $this->actualWidth = $actualWidth;
        $this->maxWidth = $maxWidth;
        parent::__construct('Image width is too big!');
    }

    /**
     * @return mixed
     */
    public function getActualWidth()
    {
        return $this->actualWidth;
    }

    /**
     * @return mixed
     */
    public function getMaxWidth()
    {
        return $this->maxWidth;
    }
}
