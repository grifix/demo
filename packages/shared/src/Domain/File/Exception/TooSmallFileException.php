<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class TooSmallFileException extends DomainException
{
    /** @var int */
    protected $actualSize;

    /** @var int */
    protected $minSize;

    public function __construct(int $actualSize, int $maxSize)
    {
        $this->actualSize = $actualSize;
        $this->minSize = $maxSize;
        parent::__construct(sprintf('Too small file!'));
    }

    public function getActualSize(): int
    {
        return $this->actualSize;
    }

    public function getMinSize(): int
    {
        return $this->minSize;
    }
}
