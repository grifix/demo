<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class ImageHeightTooBigException extends DomainException
{
    protected $actualHeight;

    protected $maxHeight;

    public function __construct($actualWidth, $maxWidth)
    {
        $this->actualHeight = $actualWidth;
        $this->maxHeight = $maxWidth;
        parent::__construct('Image height is too big!');
    }

    public function getActualHeight()
    {
        return $this->actualHeight;
    }

    public function getMaxHeight()
    {
        return $this->maxHeight;
    }
}
