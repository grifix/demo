<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class InvalidImageHeightException extends DomainException
{
    /** @var int */
    protected $actualHeight;

    /** @var int */
    protected $requiredHeight;

    public function __construct(int $actualWidth, int $requiredWidth)
    {
        $this->actualHeight = $actualWidth;
        $this->requiredHeight = $requiredWidth;
        parent::__construct('Invalid image height!');
    }

    /**
     * @return int
     */
    public function getActualHeight(): int
    {
        return $this->actualHeight;
    }

    /**
     * @return int
     */
    public function getRequiredHeight(): int
    {
        return $this->requiredHeight;
    }
}
