<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class InvalidImageWidthException extends DomainException
{
    /** @var int */
    protected $actualWidth;

    /** @var int */
    protected $requiredWidth;

    public function __construct(int $actualWidth, int $requiredWidth)
    {
        $this->actualWidth = $actualWidth;
        $this->requiredWidth = $requiredWidth;
        parent::__construct('Invalid image width!');
    }

    /**
     * @return int
     */
    public function getActualWidth(): int
    {
        return $this->actualWidth;
    }

    /**
     * @return int
     */
    public function getRequiredWidth(): int
    {
        return $this->requiredWidth;
    }
}
