<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class InvalidMimeTypeException extends DomainException
{
    /** @var string */
    protected $actualMimeType;

    /** @var string[] */
    protected $allowedMimeTypes;

    /**
     * @var string[] $allowedMimeTypes
     */
    public function __construct(string $actualMimeType, array $allowedMimeTypes)
    {
        $this->actualMimeType = $actualMimeType;
        $this->allowedMimeTypes = $allowedMimeTypes;
        parent::__construct(sprintf(
            'File type "%s" is not in allowed types "%s"!',
            $actualMimeType,
            implode(', ', $allowedMimeTypes)
        ));
    }

    /**
     * @return string
     */
    public function getActualMimeType(): string
    {
        return $this->actualMimeType;
    }

    /**
     * @return string[]
     */
    public function getAllowedMimeTypes(): array
    {
        return $this->allowedMimeTypes;
    }
}
