<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class FileNotExistsException extends DomainException
{
    /**
     * @var string
     */
    protected $path;

    public function __construct(string $path)
    {
        $this->path = $path;
        parent::__construct(sprintf('File %s does not exist!', $path));
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
