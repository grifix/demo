<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class FileIsNotImageException extends DomainException
{

}
