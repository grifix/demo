<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class ImageHeightTooSmallException extends DomainException
{
    protected $actualHeight;

    protected $minHeight;

    public function __construct(int $actualWidth, int $maxWidth)
    {
        $this->actualHeight = $actualWidth;
        $this->minHeight = $maxWidth;
        parent::__construct('Image height is too small!');
    }

    public function getActualHeight(): int
    {
        return $this->actualHeight;
    }

    public function getMinHeight(): int
    {
        return $this->minHeight;
    }
}
