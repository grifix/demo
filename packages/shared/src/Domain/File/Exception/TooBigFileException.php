<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class TooBigFileException extends DomainException
{
    /** @var int */
    protected $actualSize;

    /** @var int */
    protected $maxSize;

    public function __construct(int $actualSize, int $maxSize)
    {
        $this->actualSize = $actualSize;
        $this->maxSize = $maxSize;
        parent::__construct(sprintf('Too big file!'));
    }

    /**
     * @return int
     */
    public function getActualSize(): int
    {
        return $this->actualSize;
    }

    /**
     * @return int
     */
    public function getMaxSize(): int
    {
        return $this->maxSize;
    }
}
