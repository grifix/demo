<?php declare(strict_types=1);

namespace Grifix\Shared\Domain\File\Exception;

use DomainException;

class ImageWidthTooSmallException extends DomainException
{
    protected $actualWidth;

    protected $minWidth;

    public function __construct($actualWidth, $maxWidth)
    {
        $this->actualWidth = $actualWidth;
        $this->minWidth = $maxWidth;
        parent::__construct('Image width is too small!');
    }

    /**
     * @return mixed
     */
    public function getActualWidth()
    {
        return $this->actualWidth;
    }

    /**
     * @return mixed
     */
    public function getMinWidth()
    {
        return $this->minWidth;
    }
}
