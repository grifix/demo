<?php declare(strict_types = 1);

namespace Grifix\Shared\Domain\File;

class FileDto
{
    public function __construct(private string $sourcePath, private string $name)
    {
        $this->sourcePath = $sourcePath;
        $this->name = $name;
    }

    public function getSourcePath(): string
    {
        return $this->sourcePath;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
