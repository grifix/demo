<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\File;

interface FileInterface
{
    public function getPath(): string;

    public function delete(): void;

    public function change(string $sourcePath, ?string $name = null): static;
}
