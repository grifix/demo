<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Email;

interface EmailInterface
{
    public function matchTo(string $email): bool;
}
