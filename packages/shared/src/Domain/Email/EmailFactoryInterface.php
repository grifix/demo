<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Email;

interface EmailFactoryInterface
{
    public function createEmail(string $email): EmailInterface;
}
