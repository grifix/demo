<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Email;

use Grifix\Kit\Kernel\AbstractFactory;

class EmailFactory extends AbstractFactory implements EmailFactoryInterface
{
    public function createEmail(string $email):EmailInterface
    {
        $class = $this->makeClassName(Email::class);
        return new $class($email);
    }
}
