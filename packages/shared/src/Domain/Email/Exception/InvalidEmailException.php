<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Email\Exception;

use DomainException;

class InvalidEmailException extends DomainException
{

    /**
     * @var string
     */
    protected $email;

    /**
     * InvalidEmailException constructor.
     * @param $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
        parent::__construct(sprintf('Email %s is invalid!', $email));
    }
}
