<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Email;

use Grifix\Shared\Domain\Email\Exception\InvalidEmailException;

class Email implements EmailInterface
{
    protected string $value;

    public function __construct(string $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException($value);
        }
        $this->value = $value;
    }

    public function matchTo(string $email): bool
    {
        return $this->value === $email;
    }
}
