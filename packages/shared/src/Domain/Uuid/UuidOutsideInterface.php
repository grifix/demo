<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Uuid;

interface UuidOutsideInterface
{
    public function isValidUuid(string $uuid):bool;
}
