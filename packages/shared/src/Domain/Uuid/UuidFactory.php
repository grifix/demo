<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Uuid;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class UuidFactory extends AbstractFactory implements UuidFactoryInterface
{
    /**
     * @var UuidOutsideInterface
     */
    protected $uuidInfrastructure;

    public function __construct(ClassMakerInterface $classMaker, UuidOutsideInterface $uuidInfrastructure)
    {
        $this->uuidInfrastructure = $uuidInfrastructure;
        parent::__construct($classMaker);
    }


    public function createUuid(string $value): UuidInterface
    {
        $class = $this->makeClassName(Uuid::class);
        return new $class($this->uuidInfrastructure, $value);
    }
}
