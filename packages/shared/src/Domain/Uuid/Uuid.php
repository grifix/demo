<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Uuid;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\Uuid\Exception\InvalidUuidException;

class Uuid implements UuidInterface
{
    public function __construct(
        #[Dependency]
        protected UuidOutsideInterface $outside,
        protected string $value
    )
    {
        $this->outside = $outside;
        if (!$this->outside->isValidUuid($value)) {
            throw new InvalidUuidException($value);
        }
        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
