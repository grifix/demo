<?php

declare(strict_types=1);

namespace Grifix\Shared\Domain\Uuid\Exception;

use DomainException;

class InvalidUuidException extends DomainException
{
    /**
     * @var string
     */
    protected $uuid;

    /**
     * InvalidUuidException constructor.
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
        parent::__construct(sprintf('Invalid uuid %s', $uuid));
    }
}
