<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Uuid;

interface UuidInterface
{
    public function __toString(): string;
}
