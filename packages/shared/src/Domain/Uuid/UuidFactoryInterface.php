<?php
declare(strict_types=1);

namespace Grifix\Shared\Domain\Uuid;

interface UuidFactoryInterface
{
    public function createUuid(string $value): UuidInterface;
}
