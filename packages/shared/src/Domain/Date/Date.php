<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Domain\Date;

use Grifix\Shared\Domain\Date\Exception\InvalidDateException;
use Mockery\Exception;

final class Date implements DateInterface
{
    public function __construct(
        private int $year,
        private int $month,
        private int $day
    ) {
        try {
            new \DateTime(sprintf('%d-%d-%d', $this->year, $this->month, $this->day));
        } catch (Exception) {
            throw new InvalidDateException(\DateTime::getLastErrors());
        }
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getDay(): int
    {
        return $this->day;
    }

    public function isEqualTo(DateInterface $date): bool
    {
        if ($this->getYear() !== $date->getYear()) {
            return false;
        }

        if ($this->getMonth() !== $date->getMonth()) {
            return false;
        }

        if ($this->getDay() !== $date->getDay()) {
            return false;
        }

        return true;
    }

    public function isGreaterThan(DateInterface $date): bool
    {
        if ($this->getYear() > $date->getYear()) {
            return true;
        }
        if ($this->getMonth() > $date->getMonth()) {
            return true;
        }
        if ($this->getDay() > $date->getDay()) {
            return true;
        }

        return false;
    }

    public function isLessThan(DateInterface $date): bool
    {
        if (!$this->isGreaterThan($date) && !$this->isEqualTo($date)) {
            return true;
        }

        return false;
    }
}
