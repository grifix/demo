<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Domain\Date;

interface DateInterface
{
    public function getMonth(): int;

    public function getYear(): int;

    public function getDay(): int;

    public function isEqualTo(DateInterface $date): bool;
}
