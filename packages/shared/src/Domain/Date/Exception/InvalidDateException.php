<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Domain\Date\Exception;

final class InvalidDateException extends \InvalidArgumentException
{
    public function __construct(private array $errors)
    {
        parent::__construct('Invalid date!');
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
