<?php
declare(strict_types = 1);

namespace Grifix\Shared\Domain;

interface StringableInterface
{
    public function __toString():string ;
}
