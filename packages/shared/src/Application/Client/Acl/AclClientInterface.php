<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Shared\Application\Client\Acl;

use Grifix\Shared\Application\Client\Acl\Dto\UserDto;

interface AclClientInterface
{
    /**
     * @param string $sessionId
     * @param string $action
     *
     * @return bool
     */
    public function hasAccessToAction(string $sessionId, string $action);
    
    /**
     * @param string $sessionId
     *
     * @return bool
     */
    public function isSignedInUser(string $sessionId): bool;

    /**
     * @param string $sessionId
     * @param string $routeAlias
     * @return bool
     */
    public function hasAccessToRoute(string $sessionId, string $routeAlias): bool;

    /**
     * @param string $sessionId
     * @param string $viewAlias
     * @return bool
     */
    public function hasAccessToView(string $sessionId, string $viewAlias): bool;

    public function hasAccess(string $sessionId, string $permission): bool;

    public function getSignedInUser(string $sessionId): UserDto;
}
