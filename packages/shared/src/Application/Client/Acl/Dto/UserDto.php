<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Application\Client\Acl\Dto;

class UserDto
{
    protected string $id;

    protected string $email;

    protected bool $isGuest = true;

    protected string $timeZone;

    public function __construct(string $id, string $email, bool $isGuest, string $timeZone)
    {
        $this->id = $id;
        $this->email = $email;
        $this->isGuest = $isGuest;
        $this->timeZone = $timeZone;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getIsGuest(): bool
    {
        return $this->isGuest;
    }

    public function getTimeZone(): string
    {
        return $this->timeZone;
    }
}
