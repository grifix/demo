<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Application\Client\Acl\Exception;

class ThereIsNoPermissionDefinedForRouteException extends \RuntimeException
{
    protected $routeAlias;
    
    protected $code = 500;
    
    /**
     * AclResourceIsNotDefinedException constructor.
     *
     * @param string $routeAlias
     */
    public function __construct(string $routeAlias)
    {
        $this->routeAlias = $routeAlias;
        $this->message = 'There is no permission defined for route "'.$routeAlias.'"!!';
    }
}
