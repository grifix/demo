<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Application\Client\Acl\Exception;

class ThereIsNoPermissionDefinedForActionException extends \RuntimeException
{
    protected $action;
    
    protected $code = 500;
    
    /**
     * AclResourceIsNotDefinedException constructor.
     *
     * @param string $action
     */
    public function __construct(string $action)
    {
        $this->action = $action;
        $this->message = 'There is no permission defined for action "'.$action.'"!!';
    }
}
