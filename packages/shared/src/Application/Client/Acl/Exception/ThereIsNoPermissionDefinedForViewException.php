<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Shared\Application\Client\Acl\Exception;

class ThereIsNoPermissionDefinedForViewException extends \RuntimeException
{
    protected $viewAlias;
    
    protected $code = 500;
    
    /**
     * AclResourceIsNotDefinedException constructor.
     *
     * @param string $viewAlias
     */
    public function __construct(string $viewAlias)
    {
        $this->viewAlias = $viewAlias;
        $this->message = 'There is no permission defined for view "'.$viewAlias.'"!!';
    }
}
