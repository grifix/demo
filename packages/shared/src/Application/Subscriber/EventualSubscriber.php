<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Subscriber;

use Grifix\Shared\Application\Subscriber\Exception\IdGetterMustBeStaticException;
use Grifix\Shared\Application\Subscriber\Exception\SubscriberHasNoIdGetterException;

abstract class EventualSubscriber extends AbstractEventSubscriber
{
    protected string $subscriptionId;

    public function __construct(string $subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
        self::assertIdGetters();
    }

    public static function createSubscriptionId(object $event): string
    {
        $method = self::createEventIdGetterMethodName(get_class($event));
        return static::$method($event);
    }


    public function shouldReceiveEvent(object $event): bool
    {
        if (!self::isEventSupported($event)) {
            return false;
        }
        return static::createSubscriptionId($event) === $this->subscriptionId;
    }

    protected static function assertIdGetters()
    {
        $reflection = new \ReflectionClass(static::class);
        foreach (self::getEventTypes() as $eventType) {
            $methodName = self::createEventIdGetterMethodName($eventType);
            if (false === $reflection->hasMethod($methodName)) {
                throw new SubscriberHasNoIdGetterException(get_class(static::class), $methodName);
            }
            $method = $reflection->getMethod($methodName);
            if (false === $method->isStatic()) {
                throw new IdGetterMustBeStaticException(static::class, $methodName);
            }
        }
    }

    protected static function createEventIdGetterMethodName(string $eventType): string
    {
        $eventType = explode('\\', $eventType);
        return sprintf('getIdFrom%s', array_pop($eventType));
    }
}
