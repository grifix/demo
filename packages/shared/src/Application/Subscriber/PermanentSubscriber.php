<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Subscriber;

use Ramsey\Uuid\Uuid;

abstract class PermanentSubscriber extends AbstractEventSubscriber
{
    protected const ID_NAMESPACE = 'd60ca670-71d9-434e-8864-3a42e3aec0b0';

    public static function createSubscriptionId(object $event): string
    {
        return Uuid::uuid5(self::ID_NAMESPACE, static::class)->toString();
    }

    public function shouldReceiveEvent(object $event): bool
    {
        return self::isEventSupported($event);
    }

    public static function getStaringEvents(): array
    {
        return [];
    }

    public static function getFinishingEvents(): array
    {
        return [];
    }
}
