<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Subscriber\Exception;

use RuntimeException;

final class SubscriberHasNoIdGetterException extends RuntimeException
{

    public function __construct(string $subscriberClass, string $method)
    {
        parent::__construct(sprintf('Subscriber "%s" has no method "%s"', $subscriberClass, $method));
    }
}
