<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Subscriber\Exception;

final class EventIsNotSupportedException extends \Exception
{

    public function __construct(string $subscriberClass, string $eventType)
    {
        parent::__construct(sprintf('Event "%s" is not supported by subscriber "%s"', $eventType, $subscriberClass));
    }
}
