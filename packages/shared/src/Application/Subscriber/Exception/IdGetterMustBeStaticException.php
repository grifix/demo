<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Subscriber\Exception;

use RuntimeException;

final class IdGetterMustBeStaticException extends RuntimeException
{

    public function __construct(string $subscriberClass, string $methodName)
    {
        parent::__construct(sprintf('%s::%s must be static!', $subscriberClass, $methodName));
    }
}
