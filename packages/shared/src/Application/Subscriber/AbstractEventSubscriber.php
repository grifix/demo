<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Subscriber;

use Grifix\Kit\Event\CallableEventListener\EventHandlersExtractor\EventHandlersExtractor;
use Grifix\Kit\EventOld\ReceiverWrapper\EventReceiversExtractor\EventReceiversExtractor;
use Grifix\Shared\Application\Subscriber\Exception\EventIsNotSupportedException;

abstract class AbstractEventSubscriber implements EventSubscriberInterface
{
    /**
     * @return string[]
     */
    protected static function getEventTypes(): array
    {
        $result = [];
        foreach (EventHandlersExtractor::getInstance()->extract(static::class) as $eventHandler) {
            $result[] = $eventHandler->getEventType();
        }
        return $result;
    }

    public static function isEventSupported(object $event): bool
    {
        return in_array(get_class($event), self::getEventTypes());
    }

    protected static function assertEventIsSupported(object $event): void
    {
        if (false === self::isEventSupported($event)) {
            throw new EventIsNotSupportedException(static::class, get_class($event));
        }
    }
}
