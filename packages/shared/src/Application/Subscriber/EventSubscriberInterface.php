<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Application\Subscriber;

interface EventSubscriberInterface
{
    public static function createSubscriptionId(object $event): string;

    public static function getStaringEvents(): array;

    public static function getFinishingEvents(): array;

    public static function isEventSupported(object $event): bool;

    public function shouldReceiveEvent(object $event): bool;
}
