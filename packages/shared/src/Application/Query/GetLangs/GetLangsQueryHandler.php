<?php
declare(strict_types=1);

namespace Grifix\Shared\Application\Query\GetLangs;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Shared\Application\Common\LangsRepository\LangDto;

class GetLangsQueryHandler
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * GetLocalesQueryHandler constructor.
     * @param ConfigInterface $config
     */
    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
    }


    public function __invoke(GetLangsQuery $query)
    {
        $locales = $this->config->get('grifix.kit.intl.locales');
        $enabledLangs = $this->config->get('grifix.kit.intl.config.enabledLangs');
        $result = [];
        foreach ($locales as $locale => $data) {
            if (in_array($data['lang'], $enabledLangs)) {
                $result[] = new LangDto($data['lang'], $locale);
            }
        }
        return $result;
    }
}
