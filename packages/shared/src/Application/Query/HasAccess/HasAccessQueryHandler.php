<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Query\HasAccess;

use Grifix\Shared\Application\Client\Acl\AclClientInterface;

class HasAccessQueryHandler
{
    /** @var AclClientInterface */
    protected $aclService;

    public function __construct(AclClientInterface $aclService)
    {
        $this->aclService = $aclService;
    }

    public function __invoke(HasAccessQuery $query):bool
    {
        return $this->aclService->hasAccess($query->getSessionId(), $query->getPermission());
    }
}
