<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Query\HasAccess;

class HasAccessQuery
{
    /** @var string */
    protected $sessionId;

    /** @var string */
    protected $permission;

    public function __construct(string $sessionId, string $permission)
    {
        $this->sessionId = $sessionId;
        $this->permission = $permission;
    }

    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }
}
