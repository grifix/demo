<?php declare(strict_types=1);

namespace Grifix\Shared\Application\Query\Dto;

class FileDto
{
    public function __construct(
        protected string $name,
        protected string $path,
        protected string $mimeType
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }
}
