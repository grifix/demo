<?php declare(strict_types = 1);

namespace Grifix\Shared\Application\Query\GetFreeId;

use Grifix\Kit\Uuid\UuidGeneratorInterface;

class GetFreeUuidQueryHandler
{
    private UuidGeneratorInterface $uuidGenerator;

    public function __construct(UuidGeneratorInterface $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    public function __invoke(GetFreeUuidQuery $query): string
    {
        return $this->uuidGenerator->generateUuid4();
    }
}
