<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Application\Listener;

use Grifix\Kit\Db\Event\AfterStatementExecuteEvent;
use Grifix\Kit\Db\Event\TransactionCommitedEvent;
use Grifix\Kit\Db\Event\TransactionStartedEvent;
use Grifix\Kit\Db\Event\TransactionWasRollbackEvent;
use Psr\Log\LoggerInterface;

class DbLoggerListener
{
    protected LoggerInterface $logger;

    protected bool $traceSql;

    protected bool $logSql;

    /**
     * @param bool $traceSql <cfg:grifix.shared.log.traceSql>
     * @param bool $logSql <cfg:grifix.shared.log.logSql>
     */
    public function __construct(LoggerInterface $logger, bool $traceSql = true, bool $logSql = true)
    {
        $this->logger = $logger;
        $this->traceSql = $traceSql;
        $this->logSql = $logSql;
    }


    public function onStatementExecute(AfterStatementExecuteEvent $event): void
    {
        if (!$this->logSql) {
            return;
        }
        $this->logger->debug(
            $event->getSql(),
            $this->traceSql ? debug_backtrace(~DEBUG_BACKTRACE_PROVIDE_OBJECT) : []
        );
    }

    public function onTransactionStart(TransactionStartedEvent $event): void
    {
        if (!$this->logSql) {
            return;
        }
        $this->logger->debug(
            'BEGIN TRANSACTION',
            $this->traceSql ? debug_backtrace(~DEBUG_BACKTRACE_PROVIDE_OBJECT) : []
        );
    }

    public function onTransactionCommit(TransactionCommitedEvent $event): void
    {
        if (!$this->logSql) {
            return;
        }
        $this->logger->debug(
            'COMMIT',
            $this->traceSql ? debug_backtrace(~DEBUG_BACKTRACE_PROVIDE_OBJECT) : []
        );
    }

    public function onTransactionRollback(TransactionWasRollbackEvent $event)
    {
        if (!$this->logSql) {
            return;
        }
        $this->logger->debug(
            'ROLLBACK',
            $this->traceSql ? debug_backtrace(~DEBUG_BACKTRACE_PROVIDE_OBJECT) : []
        );
    }
}
