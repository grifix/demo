<?php declare(strict_types = 1);

namespace Grifix\Shared\Application\Common\Filter\ColumnFilter;

abstract class StringColumnFilter
{

    public const CONDITION_EQUAL = 'equal';
    public const CONDITION_NOT_EQUAL = 'not_equal';
    public const CONDITION_CONTAINS = 'contains';
    public const CONDITION_BEGIN_WITH = 'begin_with';
    public const CONDITION_END_WITH = 'end_with';

    /** @var string */
    protected $value;

    /** @var string */
    protected $condition;

    public function __construct(string $value, string $condition)
    {
        $this->value = $value;
        $this->condition = $condition;
    }

    public static function equal(string $value): self
    {
        return new static($value, self::CONDITION_EQUAL);
    }

    public static function notEqual(string $value): self
    {
        return new static($value, self::CONDITION_NOT_EQUAL);
    }

    public static function contains(string $value): self
    {
        return new static($value, self::CONDITION_CONTAINS);
    }

    public static function beginWith(string $value): self
    {
        return new static($value, self::CONDITION_BEGIN_WITH);
    }

    public static function endWith(string $value): self
    {
        return new static($value, self::CONDITION_END_WITH);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getCondition(): string
    {
        return $this->condition;
    }
}
