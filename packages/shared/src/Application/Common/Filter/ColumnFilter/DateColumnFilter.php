<?php declare(strict_types = 1);

namespace Grifix\Shared\Application\Common\Filter\ColumnFilter;

abstract class DateColumnFilter
{

    public const CONDITION_EQUAL = 'equal';
    public const CONDITION_NOT_EQUAL = 'not_equal';
    public const CONDITION_GREATER_THAN = 'greater_than';
    public const CONDITION_GREATER_THAN_OR_EQUAL = 'greater_than_or_equal';
    public const CONDITION_LESS_THAN = 'less_than';
    public const CONDITION_LESS_THAN_OR_EQUAL = 'less_than_or_equal';

    /** @var int */
    protected $value;

    /** @var string */
    protected $condition;

    public function __construct(int $value, string $condition)
    {
        $this->value = $value;
        $this->condition = $condition;
    }

    public static function equal(int $value): self
    {
        return new static($value, self::CONDITION_EQUAL);
    }

    public static function notEqual(int $value): self
    {
        return new static($value, self::CONDITION_NOT_EQUAL);
    }

    public static function greaterThan(int $value)
    {
        return new static($value, self::CONDITION_GREATER_THAN);
    }

    public static function greaterThanOrEqual(int $value)
    {
        return new static($value, self::CONDITION_GREATER_THAN_OR_EQUAL);
    }

    public static function lessThan(int $value)
    {
        return new static($value, self::CONDITION_LESS_THAN);
    }

    public static function lessThanOrEqual(int $value)
    {
        return new static($value, self::CONDITION_LESS_THAN_OR_EQUAL);
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getCondition(): string
    {
        return $this->condition;
    }
}
