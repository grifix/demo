<?php declare(strict_types = 1);

namespace Grifix\Shared\Application\Common\Filter;

abstract class AbstractFilter
{
    public const SORT_DIRECTION_ASC = 'asc';
    public const SORT_DIRECTION_DESC = 'desc';

    protected ?string $id = null;

    /** @var string[] */
    protected array $ids = [];

    /** @var object[] */
    protected array $columnFilters = [];

    protected ?string $langCode = null;

    protected ?string $sortColumn = null;

    protected ?string $sortDirection = null;

    protected ?int $offset = null;

    protected ?int $limit = null;

    private function __construct()
    {
    }

    public static function create(): static
    {
        return new static();
    }

    public function withLangCode(): ?string
    {
        return $this->langCode;
    }

    public function setLangCode(?string $langCode): static
    {
        $this->langCode = $langCode;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function withId(?string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getIds(): ?array
    {
        return $this->ids;
    }

    public function withIds(array $ids): static
    {
        $this->ids = $ids;
        return $this;
    }

    public function getColumnFilters(): array
    {
        return $this->columnFilters;
    }

    public function withColumnFilters(array $columnFilters): static
    {
        $this->columnFilters = $columnFilters;
        return $this;
    }

    public function clearColumnFilters(): void
    {
        $this->columnFilters = [];
    }

    public function withColumnFilter(object $columnFilter): void
    {
        $this->columnFilters[] = $columnFilter;
    }

    public function getSortColumn(): ?string
    {
        return $this->sortColumn;
    }

    public function setSortColumn(?string $sortColumn): static
    {
        $this->sortColumn = $sortColumn;
        return $this;
    }

    public function getSortDirection(): ?string
    {
        return $this->sortDirection;
    }

    public function setSortDirection(?string $sortDirection): static
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): static
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): static
    {
        $this->limit = $limit;
        return $this;
    }

    public function withoutLimitsAndSorts(): static
    {
        $result = clone $this;
        $result->limit = null;
        $result->sortColumn = null;
        $result->sortDirection = null;
        $result->offset = null;

        return $result;
    }

    public function hasColumnFilter(string $columnFilterClass): bool
    {
        foreach ($this->columnFilters as $columnFilter) {
            if ($columnFilter instanceof $columnFilterClass) {
                return true;
            }
        }
        return false;
    }
}
