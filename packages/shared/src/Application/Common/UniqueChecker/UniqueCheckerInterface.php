<?php declare(strict_types = 1);

namespace Grifix\Shared\Application\Common\UniqueChecker;

interface UniqueCheckerInterface
{
    public function isUnique(string $field, string $table, $value, ?string $recordId = null): bool;
}
