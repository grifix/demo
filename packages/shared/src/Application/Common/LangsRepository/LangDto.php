<?php
declare(strict_types=1);

namespace Grifix\Shared\Application\Common\LangsRepository;

class LangDto
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $locale;

    /**
     * Lang constructor.
     * @param string $code
     * @param string $locale
     */
    public function __construct(string $code, string $locale)
    {
        $this->code = $code;
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }
}
