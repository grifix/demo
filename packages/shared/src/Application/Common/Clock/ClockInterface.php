<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Shared\Application\Common\Clock;

interface ClockInterface
{
    public function getTime(): int;

    public function getDate(): \DateTimeImmutable;

    public function getMicroTime(): float;
}
