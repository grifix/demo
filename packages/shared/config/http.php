<?php
declare(strict_types=1);

namespace {

    return [
        'strict_action_acl' => false, //need permission for any action
        'strict_route_acl' => false, //need permission for any route
        'strict_view_acl' => false, //need permission for any view
    ];
}
