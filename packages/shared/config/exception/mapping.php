<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {

    use Grifix\Demo\Domain\Item\Exception\ThereIsNoFileException;
    use Grifix\Demo\Domain\Producer\Name\Exception\TooLongNameException;
    use Grifix\Demo\Domain\Producer\Name\Exception\TooShortNameException;
    use Grifix\Demo\Domain\Producer\Rating\Exception\TooBigRatingException;
    use Grifix\Demo\Domain\Producer\Rating\Exception\TooLowRatingException;
    use Grifix\Demo\Domain\Producer\Title\Exception\TooLongTitleException;
    use Grifix\Demo\Domain\Producer\Title\Exception\TooShortTitleException;
    use Grifix\Kit\Exception\UiExceptionDefinition;
    use Grifix\Shared\Domain\Currency\Exception\CurrencyIsNotAvailableException;
    use Grifix\Shared\Domain\Email\Exception\InvalidEmailException;
    use Grifix\Shared\Domain\File\Exception\FileIsNotImageException;
    use Grifix\Shared\Domain\File\Exception\FileNotExistsException;
    use Grifix\Shared\Domain\File\Exception\ImageHeightTooBigException;
    use Grifix\Shared\Domain\File\Exception\ImageHeightTooSmallException;
    use Grifix\Shared\Domain\File\Exception\ImageWidthTooBigException;
    use Grifix\Shared\Domain\File\Exception\ImageWidthTooSmallException;
    use Grifix\Shared\Domain\File\Exception\InvalidImageHeightException;
    use Grifix\Shared\Domain\File\Exception\InvalidImageWidthException;
    use Grifix\Shared\Domain\File\Exception\InvalidMimeTypeException;
    use Grifix\Shared\Domain\File\Exception\TooBigFileException;
    use Grifix\Shared\Domain\File\Exception\TooSmallFileException;
    use Grifix\Shared\Domain\File\Infrastructure\Exception\CannotUploadFileException;
    use Grifix\Shared\Domain\IpAddress\Exception\InvalidIpAddressException;
    use Grifix\Shared\Domain\PositiveFloat\Exception\ValueIsNotPositiveException;
    use Grifix\Shared\Domain\PositiveInt\Exception\ValueMustBePositiveException as IntValueMustBePositiveException;
    use Grifix\Shared\Domain\Translation\Exception\InvalidTranslationException;
    use Grifix\Shared\Domain\Url\Exception\InvalidUrlException;
    use Grifix\Shared\Domain\Uuid\Exception\InvalidUuidException;

    return [
        CurrencyIsNotAvailableException::class => new UiExceptionDefinition(
            'grifix.shared.currencyIsNotAvailable',
            'grifix.shared.msg_currencyIsNotAvailable'
        ),

        InvalidEmailException::class => new UiExceptionDefinition(
            'grifix.shared.invalidEmail',
            'grifix.shared.msg_invalidEmail'
        ),

        FileNotExistsException::class => new UiExceptionDefinition(
            'grifix.shared.fileNotExists',
            'grifix.shared.msg_fileNotExists'
        ),

        ImageHeightTooSmallException::class => new UiExceptionDefinition(
            'grifix.shared.imageHeightIsTooSmall',
            'grifix.shared.msg_imageHeightIsTooSmall'
        ),

        ImageWidthTooBigException::class => new UiExceptionDefinition(
            'grifix.shared.imageWidthIsTooBig',
            'grifix.shared.msg_imageWidthIsTooBig'
        ),

        ImageWidthTooSmallException::class => new UiExceptionDefinition(
            'grifix.shared.imageWidthIsTooSmall',
            'grifix.shared.msg_imageWidthIsTooSmall'
        ),

        InvalidImageHeightException::class => new UiExceptionDefinition(
            'grifix.shared.invalidImageHeight',
            'grifix.shared.msg_invalidImageHeight'
        ),

        InvalidImageWidthException::class => new UiExceptionDefinition(
            'grifix.shared.invalidImageWidth',
            'grifix.shared.msg_invalidImageWidth'
        ),

        InvalidMimeTypeException::class => new UiExceptionDefinition(
            'grifix.shared.invalidMimeType',
            'grifix.shared.msg_invalidMimeType'
        ),

        TooBigFileException::class => new UiExceptionDefinition(
            'grifix.shared.tooBigFile',
            'grifix.shared.msg_tooBigFile'
        ),

        TooSmallFileException::class => new UiExceptionDefinition(
            'grifix.shared.tooSmallFile',
            'grifix.shared.msg_tooSmallFile'
        ),

        CannotUploadFileException::class => new UiExceptionDefinition(
            'grifix.shared.cannotUploadFile',
            'grifix.shared.msg_cannotUploadFile'
        ),

        InvalidIpAddressException::class => new UiExceptionDefinition(
            'grifix.shared.invalidIpAddress',
            'grifix.shared.msg_invalidIpAddress'
        ),

        InvalidTranslationException::class => new UiExceptionDefinition(
            'grifix.shared.invalidTranslation',
            'grifix.shared.msg_invalidTranslation'
        ),

        InvalidUrlException::class => new UiExceptionDefinition(
            'grifix.shared.invalidUrl',
            'grifix.shared.invalidUrl'
        ),

        InvalidUuidException::class => new UiExceptionDefinition(
            'grifix.shared.invalidUuid',
            'grifix.shared.msg_invalidUuid'
        ),

        ImageHeightTooBigException::class => new UiExceptionDefinition(
            'grifix.shared.imageHeightIsTooBig',
            'grifix.shared.msg_imageHeightIsTooBig'
        ),

        ThereIsNoFileException::class => new UiExceptionDefinition(
            'grifix.demo.thereIsNoFile',
            'grifix.demo.msg_thereIsNoFile',
            404
        ),

        TooLongNameException::class => new UiExceptionDefinition(
            'grifix.demo.tooLongName',
            'grifix.demo.tooLongName'
        ),

        TooShortNameException::class => new UiExceptionDefinition(
            'grifix.demo.tooShortName',
            'grifix.demo.tooShortName'
        ),

        TooBigRatingException::class => new UiExceptionDefinition(
            'grifix.demo.tooBigRating',
            'grifix.demo.msg_tooBigRating'
        ),

        TooLowRatingException::class => new UiExceptionDefinition(
            'grifix.demo.ratingMustBePositive',
            'grifix.demo.msg_ratingMustBePositive'
        ),

        TooLongTitleException::class => new UiExceptionDefinition(
            'grifix.demo.tooLongTitle',
            'grifix.demo.tooLongTitle'
        ),

        TooShortTitleException::class => new UiExceptionDefinition(
            'grifix.demo.tooShortTitle',
            'grifix.demo.tooShortTitle'
        ),

        FileIsNotImageException::class => new UiExceptionDefinition(
            'grifix.shared.fileIsNotImage',
            'grifix.shared.msg_fileMustBeImage'
        ),

        ValueIsNotPositiveException::class => new UiExceptionDefinition(
            'grifix.shared.msg_valueIsNotPositive',
            'grifix.shared.msg_valueMustBePositive'
        ),

        IntValueMustBePositiveException::class => new UiExceptionDefinition(
            'grifix.shared.msg_valueIsNotPositive',
            'grifix.shared.msg_valueMustBePositive'
        )

    ];
}
