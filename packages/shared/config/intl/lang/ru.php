<?php /** @noinspection PhpCSValidationInspection */
declare(strict_types=1);

namespace {

    return [
        'msg_invalidEmail' => 'Недопустимый адрес электронной почты!',
        'msg_invalidTranslation' => 'Недопустимый перевод!',
        'msg_invalidMimeType' => 'Недопустимый тип файла {actualMimeType} допустимые типы {allowedMimeTypes}!',
        'msg_imageHeightIsTooBig' => 'Высота изображения {actualHeight} превышает максимально допустимую высоту {maxHeight}!',
        'msg_tooSmallFile' => 'Размер файла {actualSize} меньше чем {minSize}!',
        'msg_tooBigFile' => 'Размер файла {actualSize} больше чем {maxSize}!',
        'msg_imageWidthIsTooSmall' => 'Ширина изображения {actualWidth} меньше чем минимально допустимая ширина {minWidth}',
        'msg_imageWidthIsTooBig' => 'Ширина изображения {actualWidth} больше чем максимально допустимая ширина {maxWidth}',
        'msg_valueMustBePositive' => 'Значения должно быть больше нуля!',
        'msg_invalidImageHeight' => 'Высота изображения должна быть {requiredHeight}px!',
        'msg_invalidImageWidth' => 'Ширина изображения должна быть {requiredWidth}px!',
        'msg_fileMustBeImage' => 'Файл должен быть изоражением в формате (jpg, png, gif)!'
    ];
}
