<?php
declare(strict_types=1);

namespace {

    use Grifix\Kit\Alias;
    use Grifix\Kit\Cqrs\Command\CommandBus\CommandBus;
    use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
    use Grifix\Kit\Cqrs\Command\CommandBus\TransactionalCommandBus;
    use Grifix\Kit\Event\Store\Repository\EventRepositoryInterface;
    use Grifix\Kit\Filesystem\FilesystemFactoryInterface;
    use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;
    use Grifix\Kit\Ioc\IocContainerInterface;
    use Grifix\Shared\Alias as SharedAlias;
    use Grifix\Shared\Domain\File\Infrastructure\FileOutsideInterface;
    use Grifix\Shared\Domain\Translation\Translation;
    use Grifix\Shared\Domain\Translation\TranslationInterface;
    use Grifix\Shared\Infrastructure\Domain\File\FileOutside;
    use Grifix\Shared\Infrastructure\Internal\EventRepository\EventRepositoryFactory;

    return [
        SharedAlias::PUBLIC_FILESYSTEM => function () {
            /**@var $this IocContainerInterface */
            return $this->get(FilesystemFactoryInterface::class)->createLocal($this->get(Alias::PUBLIC_DIR));
        },
        FileOutsideInterface::class => new DependencyDefinition(FileOutside::class, [
            SharedAlias::PUBLIC_FILESYSTEM
        ]),
        CommandBusInterface::class => new DependencyDefinition(TransactionalCommandBus::class, [
            CommandBus::class
        ]),
        TranslationInterface::class => new DependencyDefinition(Translation::class),
        EventRepositoryInterface::class => function () {
            /**@var $this IocContainerInterface */
            return $this->get(EventRepositoryFactory::class)->createEventRepository();
        }
    ];
}
