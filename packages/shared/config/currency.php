<?php
declare(strict_types = 1);

namespace {

    use Grifix\Shared\Domain\Currency\CurrencyInterface;

    return [
        CurrencyInterface::PLN => false,
        CurrencyInterface::EUR => false,
        CurrencyInterface::USD => false
    ];
}
