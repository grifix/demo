<?php
declare(strict_types = 1);

namespace {

    use Grifix\Kit\Route\RouteDefinition;
    use Grifix\Shared\Ui\Http\Route\ActionRouteHandler;
    use Grifix\Shared\Ui\Http\Route\DefaultRouteHandler;

    return [
        'main' => RouteDefinition::withHandler('/{locale:locale?}', DefaultRouteHandler::getAlias()),
        'root' => RouteDefinition::withChildren(
            '/{locale:locale?}/grifix/shared',
            [
                'action' => RouteDefinition::withHandler('/action/{action?}/{view?}', ActionRouteHandler::getAlias()),
                'view' => RouteDefinition::withHandler('/view/{view}/{action?}', ActionRouteHandler::getAlias())
            ]
        )
    ];
}
