<?php

declare(strict_types=1);

namespace {

    use Grifix\Kit\Cqrs\Command\CommandBus\AsyncCommandBus;
    use Grifix\Kit\Cqrs\Command\Queue\CommandBusConsumer;
    use Grifix\Kit\MessageBroker\QueueDefinition;

    return [
        'command_bus' => new QueueDefinition(
            CommandBusConsumer::class,
            AsyncCommandBus::EXCHANGE_NAME,
            1
        )
    ];
}
