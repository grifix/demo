<?php
declare(strict_types = 1);

namespace Grifix\Shared\Test\Unit\Domain\Date;

use DateTimeZone;
use Grifix\Kit\Type\DateTime\DateTime;
use Grifix\Kit\Type\TimeZone\TimeZoneInterface;
use Grifix\Shared\Domain\DateTime\DateTime as DomainDateTime;
use Grifix\Shared\Domain\DateTime\DateOutsideInterface;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Mockery;

class DateMother
{
    /** @var DateOutsideInterface|null */
    protected $infrastructure;

    /** @var int|null */
    protected $timestamp;

    public function create(): DateTimeInterface
    {
        return new DomainDateTime($this->createInfrastructure(), $this->timestamp);
    }

    public function setInfrastructure(?DateOutsideInterface $infrastructure): DateMother
    {
        $this->infrastructure = $infrastructure;
        return $this;
    }

    public function setTimestamp(?int $timestamp): DateMother
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    protected function createInfrastructure(): DateOutsideInterface
    {
        if ($this->infrastructure) {
            return $this->infrastructure;
        }
        /** @var DateOutsideInterface|Mockery\Mock $result */
        $result = Mockery::mock(DateOutsideInterface::class)
            ->shouldReceive('createDateTime')
            ->byDefault()
            ->andReturnUsing(function (?int $timestamp) {
                $timeZone = new DateTimeZone(TimeZoneInterface::TIMEZONE_ZULU);
                $date = new DateTime("now", $timeZone);
                if($timestamp){
                    $date = $date->setTimestamp($timestamp);
                }
                return new DateTime($date->format('Y-m-d H:i:s'), $timeZone);
            })->getMock();
        return $result;
    }
}
