<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Test\Behavior\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\Environment\InitializedContextEnvironment;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use DateTimeImmutable;
use DateTimeZone;
use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;
use Grifix\Kit\ArrayWrapper\ArrayWrapperInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\MoneyHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Mailer\EmailMessageInterface;
use Grifix\Kit\Mailer\MailerInterface;
use Grifix\Kit\Type\TimeZone\TimeZone;
use Grifix\Shared\Application\Common\Clock\ClockInterface;
use Grifix\Shared\Application\Query\GetLangs\GetLangsQuery;
use Grifix\Shared\Test\Common\CliClient;
use Grifix\Shared\Test\Common\HttpClient;
use Grifix\Shared\Test\Common\PayloadBuilder\PayloadBuilder;
use Grifix\Shared\Test\Common\PayloadBuilder\PayloadBuilderFactory;
use Grifix\Shared\Test\Common\Stub\FrozenClock;
use Grifix\Shared\Test\Common\Stub\Mailer\FakeMailer;
use Grifix\Shared\Test\Common\Stub\Mailer\Message;
use Grifix\Shared\Test\Common\UploadedFileFactory;
use PHPUnit\Framework\Assert;
use Psr\Http\Message\UploadedFileInterface;

abstract class AbstractContext implements Context
{
    private ?ApplicationContext $applicationContext;

    private ?IocContainerInterface $iocContainer;

    private ?InitializedContextEnvironment $environment;

    protected ?HttpClient $httpClient;

    protected ?ConnectionInterface $connection;

    protected ?FakeMailer $mailer;

    protected ?ArrayHelperInterface $arrayHelper;

    protected ?CliClient $cliClient;

    protected array $langs = [];

    /**
     * @BeforeScenario
     */
    public function init(BeforeScenarioScope $scope)
    {
        $this->environment = $scope->getEnvironment();
        $this->applicationContext = $this->environment->getContext(ApplicationContext::class);
        $this->iocContainer = $this->applicationContext->getApplicationScope()->getIocContainer();
        $this->httpClient = $this->getShared(HttpClient::class);
        $this->connection = $this->getShared(ConnectionInterface::class);
        $this->mailer = $this->getShared(MailerInterface::class);
        $this->arrayHelper = $this->getShared(ArrayHelperInterface::class);
        $this->cliClient = $this->getShared(CliClient::class);
        $this->beforeScenario($scope);
    }

    abstract protected function beforeScenario(BeforeScenarioScope $scope): void;

    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }

    protected function sendPostAction(string $action, array $params = [], array $uploadedFiles = []): void
    {
        $this->httpClient->postAction($action, $params, $uploadedFiles);
    }

    protected function sendGetAction(string $action, array $params = []): void
    {
        $this->httpClient->getAction($action, $params);
    }

    protected function getLastResponseCode(): int
    {
        return $this->httpClient->getLastResponse()->getStatusCode();
    }

    protected function getLastResponseArray(): array
    {
        return $this->httpClient->getLastResponseAsArray();
    }

    protected function getLastResponse(): string
    {
        return (string)$this->httpClient->getLastResponse()->getBody();
    }

    protected function getLastResponseWrapperArray(): ArrayWrapperInterface
    {
        return $this->getShared(ArrayWrapperFactoryInterface::class)
            ->create($this->httpClient->getLastResponseAsArray());
    }

    protected function assertLastResponseCode(int $expectedCode): void
    {
        Assert::assertEquals($expectedCode, $this->getLastResponseCode());
    }

    protected function assertHttpError(int $code, string $key): void
    {
        $this->assertLastResponseCode($code);
        $this->assertLastResponseMessageKey($key);
    }

    protected function assertLastResponseMessageKey(string $expectedKey): void
    {
        $actualKey = $this->getLastResponseArray()['key'];
        Assert::assertEquals(
            $actualKey,
            $expectedKey,
            sprintf('expected "%s" got "%s"', $expectedKey, $actualKey)
        );
    }

    protected function getConfig(string $key)
    {
        return $this->getShared(ConfigInterface::class)->get($key);
    }

    protected function wrapArray(array $array): ArrayWrapperInterface
    {
        return $this->getShared(ArrayWrapperFactoryInterface::class)->create($array);
    }

    protected function createUploadedFile(string $path): UploadedFileInterface
    {
        return $this->getShared(UploadedFileFactory::class)->createUploadedFile($path);
    }

    protected function createInstance(string $alias)
    {
        return $this->iocContainer->createNewInstance($alias);
    }

    public function getContext(string $contextClass)
    {
        return $this->environment->getContext($contextClass);
    }

    protected function setShared(string $alias, $value): void
    {
        $this->iocContainer->set($alias, $value);
    }

    protected function executeCommand(object $command): void
    {
        $this->getShared(CommandBusInterface::class)->execute($command);
    }

    protected function executeQuery(object $query)
    {
        return $this->getShared(QueryBusInterface::class)->execute($query);
    }

    protected function findRecord(string $table, string $id): ArrayWrapperInterface
    {
        $record = $this->connection->fetch(
            sprintf("select data from %s where id = :id", $table),
            [
                'id' => $id
            ]
        );
        if (!$record) {
            return $this->getShared(ArrayWrapperFactoryInterface::class)->create([]);
        }

        return $this->getShared(ArrayWrapperFactoryInterface::class)->create(json_decode($record['data'], true));
    }

    /**
     * @return Message[]
     */
    protected function getSentEmailMessages(string $email): array
    {
        $result = [];
        foreach ($this->mailer->getMessages() as $message) {
            /** @var Message $message */
            if ($message->getRecipientEmail() === $email) {
                $result[] = $message;
            }
        }
        return $result;
    }

    /**
     * @return EmailMessageInterface[]
     */
    protected function assertEmailWasSent(string $email): void
    {
        foreach ($this->mailer->getMessages() as $message) {
            /** @var Message $message */
            if ($message->getRecipientEmail() === $email) {
                return;
            }
        }
        throw new \Exception('Sent email not found!');
    }

    protected function dateToTimestamp(string $date): int
    {
        return (new DateTimeImmutable($date))->getTimestamp();
    }

    protected function amountToInt($price): int
    {
        return $this->getShared(MoneyHelperInterface::class)->toInt((string)$price);
    }

    protected function setCurrentTime(string $dateTime): void
    {
        $dateTime = new DateTimeImmutable($dateTime, new DateTimeZone(TimeZone::TIMEZONE_ZULU));
        /** @var FrozenClock $clockService */
        $clockService = $this->getShared(ClockInterface::class);
        if (false === ($clockService instanceof FrozenClock)) {
            $clockService = new FrozenClock($dateTime->getTimestamp());
            $this->setShared(ClockInterface::class, $clockService);
        }
        $clockService->setTime($dateTime->getTimestamp());
    }

    protected function toArray($var): array
    {
        return $this->arrayHelper->toArray($var);
    }

    protected function timestampToDate(int $timestamp): string
    {
        return date('Y-m-d H:i:s', $timestamp);
    }

    protected function createPayloadBuilder(): PayloadBuilder
    {
        return $this->getShared(PayloadBuilderFactory::class)->create();
    }

    protected function createMultiLangString(string $var): array
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $var;
        }
        return $result;
    }

    protected function getLangs(): array
    {
        if (!$this->langs) {
            $this->langs = $this->iocContainer->get(QueryBusInterface::class)->execute(new GetLangsQuery());
        }
        return $this->langs;
    }

    protected function convertTextToMoney(string|float $price): ?int
    {
        if (in_array($price, array(null, ''))) {
            return null;
        }
        if (is_string($price)) {
            str_replace(',', '.', $price);
        }
        return (int)((float)$price * 100);
    }

    protected function convertTextToBool(?string $text): ?bool
    {
        if (null === $text) {
            return null;
        }
        return (bool)json_encode($text);
    }

    protected function convertTextToInt(?string $text): ?int
    {
        if (null === $text) {
            return null;
        }
        return (int)$text;
    }

    public function createPayload(array $map, array $data): array
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$map[$key]] = $value;
        }
        return $result;
    }
}
