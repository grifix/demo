<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Shared\Test\Behavior\Context;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Shared\Test\Common\HttpClient;
use PHPUnit\Framework\Assert;

class CommonContext extends AbstractContext
{
    protected function beforeScenario(BeforeScenarioScope $scope): void
    {
    }

    /** @Then I should get an error about bad request */
    public function iShouldGetAnErrorAboutBadRequest()
    {
        Assert::assertEquals(400, $this->getShared(HttpClient::class)->getLastResponse()->getStatusCode());
    }

    /**
     * @Given the current time is :date
     */
    public function theCurrentTimeIs(string $dateTime): void
    {
        $this->setCurrentTime($dateTime);
    }

    /**
     * @Then I should get not errors
     */
    public function iShouldGetNoErrors(): void
    {
        Assert::assertTrue(
            400 > $this->getLastResponseCode(),
            sprintf('There was an error %s', $this->getLastResponse())
        );
    }
}
