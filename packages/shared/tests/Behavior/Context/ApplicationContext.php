<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types = 1);

namespace Grifix\Shared\Test\Behavior\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\EntryPoint\IntegrationTestEntryPoint;
use Grifix\Kit\EntryPoint\PipelineFactoryInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\EntryPoint\Stage\StageFactoryInterface;
use Grifix\Kit\Exception\ExceptionPresenterInterface;
use Grifix\Kit\Http\ResponseSenderInterface;
use Grifix\Kit\Http\ServerFactoryInterface;
use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;
use Grifix\Kit\Mailer\MailerInterface;
use Grifix\Kit\MessageBroker\MessageBrokerFactoryInterface;
use Grifix\Kit\MessageBroker\MessageBrokerInterface;
use Grifix\Kit\Session\SessionFactoryInterface;
use Grifix\Shared\Test\Common\Stub\Http\ExceptionPresenter;
use Grifix\Shared\Test\Common\Stub\Http\ResponseSender;
use Grifix\Shared\Test\Common\Stub\Http\ServerFactory;
use Grifix\Shared\Test\Common\Stub\Http\SessionFactory;
use Grifix\Shared\Test\Common\Stub\Mailer\FakeMailer;

/**
 * Class WebUiContext
 * @package Grifix\Kit\Behat
 */
final class ApplicationContext implements Context
{
    protected string $rootDir;

    protected ?PipelineScopeInterface $applicationScope = null;

    protected static bool $installed = false;


    public function __construct()
    {
        $this->rootDir = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
    }


    /**
     * @BeforeScenario
     */
    public function init(BeforeScenarioScope $scenarioScope): void
    {
        $entryPoint = new IntegrationTestEntryPoint(
            $this->rootDir,
            function (PipelineScopeInterface $scope) use ($scenarioScope) {

                $iocContainer = $scope->getIocContainer();
                if ($scenarioScope->getScenario()->hasTag('sync')) {
                    $iocContainer->set(
                        MessageBrokerInterface::class,
                        $iocContainer->get(MessageBrokerFactoryInterface::class)->createSyncMessageBroker()
                    );
                }
                $iocContainer->set(SessionFactoryInterface::class, new DependencyDefinition(SessionFactory::class));
                $iocContainer->set(ServerFactoryInterface::class, new DependencyDefinition(ServerFactory::class));
                $iocContainer->set(ResponseSenderInterface::class, new DependencyDefinition(ResponseSender::class));
                $iocContainer->set(PipelineScopeInterface::class, $scope);
                /** @var FakeMailer $mailer */
                $mailer = $iocContainer->get(MailerInterface::class);
                $mailer->clearMessages();
            }
        );

        $this->applicationScope = $entryPoint->enter();
        $iocContainer = $this->applicationScope->getIocContainer();

        $pipelineFactory = $iocContainer->get(PipelineFactoryInterface::class);
        $stageFactory = $iocContainer->get(StageFactoryInterface::class);
        $pipeline = $pipelineFactory->createEmptyPipeline();
        $pipeline = $pipeline->pipe($stageFactory->createInitSessionStage());
        $pipeline($this->applicationScope);

        if ((bool) getenv('GRIFIX_BEHAT_DEBUG')) {
            $iocContainer->set(
                ExceptionPresenterInterface::class,
                new DependencyDefinition(ExceptionPresenter::class)
            );
            echo "\033[31mWARNING, DEBUG MODE IS ON!\n\033[0m";
            ob_flush();
        }

        ob_flush();
    }

    /**
     * @AfterScenario
     */
    public function tearDown()
    {
        $this->getApplicationScope()->getIocContainer()->get(ConnectionInterface::class)->disconnect();
    }

    public function getApplicationScope(): PipelineScopeInterface
    {
        return $this->applicationScope;
    }
}
