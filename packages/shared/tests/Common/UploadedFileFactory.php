<?php declare(strict_types = 1);

namespace Grifix\Shared\Test\Common;

use Grifix\Shared\Test\Common\Exception\UploadedFileNotExistException;
use GuzzleHttp\Psr7\UploadedFile;
use Psr\Http\Message\UploadedFileInterface;

class UploadedFileFactory
{
    public function createUploadedFile(string $path): UploadedFileInterface
    {
        if(!file_exists($path)){
            throw new UploadedFileNotExistException($path);
        }
        return new UploadedFile($path, filesize($path), UPLOAD_ERR_OK, $path);
    }
}
