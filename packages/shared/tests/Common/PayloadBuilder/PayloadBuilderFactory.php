<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Test\Common\PayloadBuilder;

use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;

class PayloadBuilderFactory
{
    protected ArrayWrapperFactoryInterface $arrayWrapperFactory;

    public function __construct(ArrayWrapperFactoryInterface $arrayWrapperFactory)
    {
        $this->arrayWrapperFactory = $arrayWrapperFactory;
    }

    public function create(): PayloadBuilder
    {
        return new PayloadBuilder($this->arrayWrapperFactory);
    }
}
