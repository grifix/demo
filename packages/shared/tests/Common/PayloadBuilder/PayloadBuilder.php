<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Test\Common\PayloadBuilder;

use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;

class PayloadBuilder
{
    protected array $map = [];

    protected ArrayWrapperFactoryInterface $arrayWrapperFactory;

    public function __construct(ArrayWrapperFactoryInterface $arrayWrapperFactory)
    {
        $this->arrayWrapperFactory = $arrayWrapperFactory;
    }

    public function bindKey(string $dataKey, string $payloadKey): self
    {
        $this->map[$dataKey] = $payloadKey;
        return $this;
    }

    public function build(array $data): array
    {
        $data = $this->arrayWrapperFactory->create($data);
        $result = $this->arrayWrapperFactory->create([]);
        foreach ($this->map as $dataKey => $payloadKey) {
            if ($data->keyExists($dataKey)) {
                $result->set($payloadKey, $data->get($dataKey));
            }
        }
        return $result->getArray();
    }
}
