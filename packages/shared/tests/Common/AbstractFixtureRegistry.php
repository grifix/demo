<?php declare(strict_types=1);

namespace Grifix\Shared\Test\Common;

use Grifix\Shared\Test\Common\Exception\FixtureAlreadyExistsException;
use Grifix\Shared\Test\Common\Exception\FixtureDoesNotExistException;

abstract class AbstractFixtureRegistry
{
    protected array $fixtures = [];

    protected function getFixture(string $alias, string $class)
    {
        if (!isset($this->fixtures[$class]) || !isset($this->fixtures[$class][$alias])) {
            throw new FixtureDoesNotExistException($alias, $class);
        }
        return $this->fixtures[$class][$alias];
    }

    protected function addFixture(string $alias, object $fixture): void
    {
        $class = get_class($fixture);
        if (!isset($this->fixtures[$class])) {
            $this->fixtures[$class] = [];
        }
        if (array_key_exists($alias, $this->fixtures[$class])) {
            throw new FixtureAlreadyExistsException($alias, $class);
        }
        $this->fixtures[$class][$alias] = $fixture;
    }
}
