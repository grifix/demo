<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Test\Common;


use Grifix\Kit\Cli\ApplicationFactory\CliApplicationFactoryInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

class CliClient
{
    private ?BufferedOutput $lastOutput;

    private ?int $lastCode;

    public function __construct(
        private CliApplicationFactoryInterface $cliApplicationFactory
    )
    {
    }

    public function executeCommand(string $command, array $arguments = [], array $options = []): OutputInterface
    {
        $inputArray = [
            'command' => $command,
        ];
        $this->injectArguments($inputArray, $arguments);
        $this->injectOptions($inputArray, $options);
        $output = new BufferedOutput();
        $application = $this->cliApplicationFactory->createApplication();
        $application->setAutoExit(false);
        $this->lastCode = $application->run(
            new ArrayInput($inputArray),
            $output
        );
        $this->lastOutput = $output;
        return $output;
    }

    public function getLastOutput(): ?BufferedOutput
    {
        return $this->lastOutput;
    }

    public function getLastCode(): ?int
    {
        return $this->lastCode;
    }

    private function injectOptions(array &$inputArray, array $options): void
    {
        foreach ($options as $key => $value) {
            $inputArray['--' . $key] = $value;
        }
    }

    private function injectArguments(array &$inputArray, array $arguments): void
    {
        $inputArray = array_merge($inputArray, $arguments);
    }
}
