<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Shared\Test\Common\Exception;

final class UploadedFileNotExistException extends \Exception
{
    public function __construct(string $path)
    {
        parent::__construct(sprintf('Uploaded file "%s" does not exist!', $path));
    }
}
