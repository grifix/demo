<?php declare(strict_types=1);

namespace Grifix\Shared\Test\Common\Exception;

use Exception;

class FixtureDoesNotExistException extends Exception
{
    public function __construct(string $alias, string $class)
    {
        parent::__construct(sprintf('Fixture "%s" with alias "%s" does not exist!', $class, $alias));
    }
}
