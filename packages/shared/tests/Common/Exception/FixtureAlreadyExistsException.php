<?php declare(strict_types=1);

namespace Grifix\Shared\Test\Common\Exception;

use Exception;

class FixtureAlreadyExistsException extends Exception
{
    public function __construct(string $alias, string $class)
    {
        parent::__construct(sprintf('Fixture "%s" with alias "%s" already exists!', $class, $alias));
    }
}
