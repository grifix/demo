<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Test\Common;

abstract class AbstractPathMaker
{
    abstract protected function getFilesDir():string;

    public function makePath(string $file): string
    {
        return realpath(sprintf($this->getFilesDir() . '/%s', $file));
    }
}
