<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Shared\Test\Common;

use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Shared\Application\Common\LangsRepository\LangDto;
use Grifix\Shared\Application\Query\GetLangs\GetLangsQuery;

class StringGenerator
{
    protected QueryBusInterface $queryBus;

    protected ?array $langs = null;

    public function __construct(QueryBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function createMultiLangString(string $var): array
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $var;
        }
        return $result;
    }

    /**
     * @return LangDto[]
     */
    protected function getLangs(): array
    {
        if (!$this->langs) {
            $this->langs = $this->queryBus->execute(new GetLangsQuery());
        }
        return $this->langs;
    }
}
