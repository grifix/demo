<?php declare(strict_types=1);

namespace Grifix\Shared\Test\Common;

use Exception;
use Grifix\Kit\EntryPoint\PipelineFactoryInterface;
use Grifix\Kit\EntryPoint\Scope\PipelineScopeInterface;
use Grifix\Kit\EntryPoint\Stage\StageFactoryInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestFactoryInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use GuzzleHttp\Psr7\UploadedFile;

class HttpClient
{
    private PipelineScopeInterface $applicationScope;

    private PipelineFactoryInterface $pipelineFactory;

    private ResponseInterface $lastResponse;

    private StageFactoryInterface $stageFactory;

    private ServerRequestFactoryInterface $serverRequestFactory;

    public function __construct(
        PipelineScopeInterface $applicationScope,
        PipelineFactoryInterface $pipelineFactory,
        StageFactoryInterface $stageFactory,
        ServerRequestFactoryInterface $serverRequestFactory
    )
    {
        $this->applicationScope = $applicationScope;
        $this->pipelineFactory = $pipelineFactory;
        $this->stageFactory = $stageFactory;
        $this->serverRequestFactory = $serverRequestFactory;
    }

    private function sendRequest(ServerRequestInterface $request): ResponseInterface
    {
        $request = $request->withAddedAjaxHeaders();
        $this->applicationScope->setRequest($request);
        $pipeline = $this->pipelineFactory->createEmptyPipeline();
        $pipeline = $pipeline->pipe($this->stageFactory->createHandleRequestStage());
        $pipeline($this->applicationScope);
        $this->lastResponse = $this->applicationScope->getResponse();
        if ($this->lastResponse->getStatusCode() >= 500) {
            throw new Exception($this->lastResponse->getBody()->getContents());
        }
        return $this->lastResponse;
    }

    public function get(string $uri, array $params = []): ResponseInterface
    {
        $this->stringifyParams($params);
        $request = $this->serverRequestFactory->create('GET', $uri)->withQueryParams($params);
        return $this->sendRequest($request);
    }

    public function post(string $uri, array $params = [], array $uploadedFiles = []): ResponseInterface
    {
        $this->stringifyParams($params);
        $request = $this->serverRequestFactory->create('POST', $uri)->withParsedBody($params);
        if (count($uploadedFiles) > 0) {
            $request = $request->withUploadedFiles($uploadedFiles);
        }
        return $this->sendRequest($request);
    }

    public function postAction(string $action, array $params = [], array $uploadedFiles = []): ResponseInterface
    {

        return $this->post($this->createActionUrl($action), $params, $uploadedFiles);
    }

    public function getAction(string $action, array $params = []): ResponseInterface
    {
        return $this->get($this->createActionUrl($action), $params);
    }

    protected function createActionUrl(string $action): string
    {
        return sprintf('/grifix/shared/action/%s/json', $action);
    }

    public function getLastResponseAsArray(): array
    {
        return json_decode($this->lastResponse->getBody()->__toString(), true);
    }

    public function getLastResponse(): ResponseInterface
    {
        return clone $this->lastResponse;
    }

    private function stringifyParams(array &$params): void
    {
        foreach ($params as &$param) {
            if($param instanceof UploadedFile){
                continue;
            }
            if (is_array($param)) {
                $this->stringifyParams($param);
                continue;
            }
            $param = (string) $param;
        }
    }
}
