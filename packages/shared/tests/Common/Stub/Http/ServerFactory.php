<?php declare(strict_types=1);


namespace Grifix\Shared\Test\Common\Stub\Http;

use Grifix\Kit\Http\ServerFactoryInterface;
use Grifix\Kit\Http\ServerInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class ServerFactory
 * @package Grifix\Kit\Behat\Stub
 */
class ServerFactory implements ServerFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createServer(ServerRequestInterface $request): ServerInterface
    {
        return new Server($request);
    }
}
