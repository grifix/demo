<?php declare(strict_types = 1);

namespace Grifix\Shared\Test\Common\Stub\Mailer;

use Grifix\Kit\Mailer\EmailMessageInterface;
use Grifix\Kit\Mailer\MailerInterface;

class FakeMailer implements MailerInterface
{
    /** @var MailerInterface */
    protected $realMailer;

    /** @var Message[] */
    protected $messages = [];

    /** @var string */
    protected $messagesPath;

    public function __construct(MailerInterface $realMailer, string $tmpDir)
    {
        $this->realMailer = $realMailer;
        $this->messagesPath = $tmpDir . DIRECTORY_SEPARATOR . 'fakeMailer.dump';
    }

    public function send(
        string $toEmail,
        string $subject,
        string $content,
        string $toName,
        ?string $fromEmail = null,
        ?string $fromName = null
    ): void {
        $this->storeMessage(new Message($fromEmail, $fromName, $toEmail, $toName, $content, $subject));
        if (getenv('GRIFIX_SEND_MAILS_IN_TESTS')) {
            $this->realMailer->send($toEmail, $subject, $content, $toName, $fromEmail, $fromName);
        }
    }

    protected function storeMessage(Message $message): void
    {
        $messages = $this->getMessages();
        $messages[] = $message;
        file_put_contents($this->messagesPath, serialize($messages));
    }


    /**
     * @return Message[]
     */
    public function getMessages(): array
    {
        if (file_exists($this->messagesPath)) {
            return unserialize(file_get_contents($this->messagesPath));
        }
        return [];
    }

    public function sendMessage(EmailMessageInterface $message): void
    {
        $this->storeMessage(
            new Message(
                $message->getSenderEmail(),
                $message->getSenderName(),
                $message->getRecipientEmail(),
                $message->getRecipientName(),
                $message->getViewVars(),
                $message->getSubject(),
                $message->getViewPath(),
                get_class($message)
            )
        );
        if (getenv('GRIFIX_SEND_MAILS_IN_TESTS')) {
            $this->realMailer->sendMessage($message);
        }
    }

    public function clearMessages(): void
    {
        if (is_file($this->messagesPath)) {
            unlink($this->messagesPath);
        }
    }
}
