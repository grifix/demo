<?php declare(strict_types = 1);

namespace Grifix\Shared\Test\Common\Stub\Mailer;

class Message
{
    /** @var string|null */
    protected $senderEmail;

    /** @var string|null */
    protected $senderName;

    /** @var string */
    protected $recipientEmail;

    /** @var string|null */
    protected $recipientName;

    /** @var string|null */
    protected $viewPath;

    /** @var object|string */
    protected $content;

    /** @var string */
    protected $subject;

    /** @var string|null */
    protected $class;

    /**
     * @param object|string $content
     */
    public function __construct(
        ?string $senderEmail,
        ?string $senderName,
        string $recipientEmail,
        ?string $recipientName,
        $content,
        string $subject,
        ?string $templatePath = null,
        ?string $class = null
    ) {
        $this->senderEmail = $senderEmail;
        $this->senderName = $senderName;
        $this->recipientEmail = $recipientEmail;
        $this->recipientName = $recipientName;
        $this->viewPath = $templatePath;
        $this->content = $content;
        $this->subject = $subject;
        $this->class = $class;
    }

    public function getSenderEmail(): ?string
    {
        return $this->senderEmail;
    }

    public function getSenderName(): ?string
    {
        return $this->senderName;
    }

    public function getRecipientEmail(): string
    {
        return $this->recipientEmail;
    }

    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    public function getViewPath(): ?string
    {
        return $this->viewPath;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }
}
