<?php

declare(strict_types=1);

namespace Grifix\Shared\Test\Common\Stub;

use Grifix\Shared\Application\Common\Clock\ClockInterface;

class FrozenClock implements ClockInterface
{
    private float $microTime;

    private int $time;

    protected \DateTimeImmutable $date;

    public function __construct(float $microTime)
    {
        $this->setTime($microTime);
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function setTime(float $microTime): void
    {
        $this->microTime = $microTime;
        $this->time = (int)($microTime);
        $this->date = new \DateTimeImmutable(date('Y-m-d H:i:s', $this->time));
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getMicroTime(): float
    {
        return $this->microTime;
    }
}
