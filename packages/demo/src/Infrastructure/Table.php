<?php
declare(strict_types=1);

namespace Grifix\Demo\Infrastructure;

interface Table
{
    const SCHEMA = 'grifix_demo';
    const ITEM = self::SCHEMA.'.item';
    const ITEM_PHOTO =self::SCHEMA.'.item_photo';
    const PRODUCER = self::SCHEMA.'.producer';
}
