<?php declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Application\Projection\ProducerProjection;

use DateTimeImmutable;
use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ItemProjectionInterface;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\CreatedAtColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\NameColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\RatingColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\TitleColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerDto;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerProjectionInterface;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\FilterApplierFactoryInterface;

class ProducerProjection implements ProducerProjectionInterface
{
    /** @var ConnectionInterface */
    protected $connection;

    /** @var FilterApplierFactoryInterface */
    protected $filterApplierFactory;

    /** @var CollectionFactoryInterface */
    protected $collectionFactory;

    /** @var ItemProjectionInterface */
    protected $itemFinder;

    /** @var ArrayWrapperFactoryInterface */
    protected $arrayWrapperFactory;

    public function __construct(
        ConnectionInterface $connection,
        FilterApplierFactoryInterface $filterApplierFactory,
        CollectionFactoryInterface $collectionFactory,
        ItemProjectionInterface $itemFinder,
        ArrayWrapperFactoryInterface $arrayWrapperFactory
    ) {
        $this->connection = $connection;
        $this->filterApplierFactory = $filterApplierFactory;
        $this->collectionFactory = $collectionFactory;
        $this->itemFinder = $itemFinder;
        $this->arrayWrapperFactory = $arrayWrapperFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function find(
        ?ProducerFilter $filter = null
    ): CollectionInterface {
        null !== $filter ?: $filter = ProducerFilter::create();
        $query = $this->connection->createQuery()
            ->select('*')
            ->from(Table::PRODUCER);

        $this->applyFilter($filter, $query);

        $records = $query->fetchAll();

        foreach ($records as &$record) {
            $record = json_decode($record['data'], true);
        }

        if ($filter->getItemFilter()) {
            $this->mergeItems($records, $filter->getItemFilter());
        }

        $result = [];
        foreach ($records as $r) {
            $result[] = $this->createProducerDto($r);
        }

        return $this->collectionFactory->createCollection($result);
    }

    public function count(?ProducerFilter $filter = null): int
    {
        null !== $filter ?: $filter = ProducerFilter::create();
        $query = $this->connection->createQuery()
            ->select('COUNT(*) AS result')
            ->from(Table::PRODUCER);
        $this->applyFilter($filter->withoutLimitsAndSorts(), $query);
        return $query->fetch()['result'];
    }

    protected function createProducerDto(array $record): ProducerDto
    {
        $record = $this->arrayWrapperFactory->create($record);
        return new ProducerDto(
            $record->get('id.value'),
            $record->get('name.value'),
            $record->get('title.values'),
            $record->get('rating.value'),
            (new DateTimeImmutable($record->get('createdAt.value')))->getTimestamp(),
            $record->get('items')
        );
    }

    protected function applyFilter(
        ProducerFilter $filter,
        QueryInterface $query
    ): void {
        if ($filter->getId()) {
            $query->where('id = :id')->bindValue('id', $filter->getId());
        }
        if ($filter->getIds()) {
            $query->where('id IN (:ids)')->bindValue('ids', $filter->getIds());
        }

        $this->filterApplierFactory->createFilterApplier()
            ->addSortableColumn('id', 'id')
            ->addSortableColumn('name', "data->>'name'")
            ->addSortableColumn('rating', "data->>'rating'")
            ->addSortableColumn('createdAt', "data->>'createdAt'")
            ->addSortableColumn('title', "data->'title'->'values'->>'%s'")
            ->addFilterableColumn('name', NameColumnFilter::class, "data->'name'->>'value'")
            ->addFilterableColumn('createdAt', CreatedAtColumnFilter::class, "data->'createdAt'->>'value'")
            ->addFilterableColumn('rating', RatingColumnFilter::class, "data->'rating'->>'value'")
            ->addFilterableColumn('title', TitleColumnFilter::class, "data->'title'->'values'->>'%s'")
            ->apply($filter, $query);
    }

    protected function mergeItems(array &$records, ItemFilter $filter): void
    {
        $producerIds = [];
        foreach ($records as $record) {
            $producerIds[] = $record['id']['value'];
        }
        $filter->setProducerIds($producerIds);
        $items = $this->itemFinder->find($filter);
        foreach ($records as &$record) {
            $record['items'] = [];
            foreach ($items as $item) {
                if ($item->getProducerId() === $record['id']['value']) {
                    $record['items'][] = $item;
                }
            }
        }
    }
}
