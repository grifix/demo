<?php declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Application\Projection\ItemPhotoProjection;

use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoDto;
use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoFilter;
use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoProjectionInterface;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Application\Query\Dto\FileDto;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\FilterApplierFactoryInterface;

class ItemPhotoProjection implements ItemPhotoProjectionInterface
{
    public function __construct(
        private ConnectionInterface $connection,
        private CollectionFactoryInterface $collectionFactory,
        private FilterApplierFactoryInterface $filterApplierFactory
    )
    {
    }

    /**
     * @return CollectionInterface|ItemPhotoDto[]
     */
    public function find(ItemPhotoFilter $filter): CollectionInterface
    {
        $query = $this->connection->createQuery()
            ->select('*')
            ->from(Table::ITEM_PHOTO, 'photo');
        $this->applyFilter($filter, $query);

        $photos = $query->fetchAll();
        $result = [];
        foreach ($photos as $record) {
            $photo = json_decode($record['data'], true);
            $photo['item_id'] = $record['item_id'];
            $result[] = $this->createItemPhotoDto($photo);
        }

        return $this->collectionFactory->createCollection($result);
    }

    public function count(ItemPhotoFilter $filter): int
    {
        $query = $this->connection->createQuery()
            ->select('COUNT(*) as result')
            ->from(Table::ITEM_PHOTO, 'photo');
        $this->applyFilter($filter->withoutLimitsAndSorts(), $query);
        return $query->fetch()['result'];
    }

    private function applyFilter(ItemPhotoFilter $filter, QueryInterface $query):void{
        if ($filter->getItemIds()) {
            $query->where('photo.item_id IN (:item_id)')->bindValue('item_id', $filter->getItemIds());
        }
        if ($filter->getId()) {
            $query->where('photo.id = :id')->bindValue('id', $filter->getId());
        }

        $this->filterApplierFactory->createFilterApplier()
            ->addSortableColumn('id', 'id')
            ->apply($filter, $query);
    }

    protected function createItemPhotoDto(array $data): ItemPhotoDto
    {
        return new ItemPhotoDto(
            $data['id']['value'],
            $data['item_id'],
            new FileDto(
                $data['file']['name'],
                $data['file']['path'],
                $data['file']['mimeType']
            ),
            $data['description']['values'],
            $data['sequence']['value']
        );
    }
}
