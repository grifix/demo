<?php declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Application\Projection\ItemProjection;

use Grifix\Demo\Application\Projection\ItemProjection\ItemProjectionInterface;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerProjectionInterface;

interface ProducerProjectionDependencyResolverInterface
{
    /**
     * @internal
     */
    public function resolveProducerProjection(ItemProjectionInterface $itemProjection): ProducerProjectionInterface;
}
