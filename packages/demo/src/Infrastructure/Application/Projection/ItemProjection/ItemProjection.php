<?php declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Application\Projection\ItemProjection;

use DateTimeImmutable;
use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoFilter;
use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoProjectionInterface;
use Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter\NameColumnFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter\ProducerNameColumnFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter\TitleColumnFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ItemDto;
use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ItemProjectionInterface;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerProjectionInterface;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Shared\Application\Query\Dto\FileDto;
use Grifix\Shared\Infrastructure\Internal\FilterApplier\FilterApplierFactoryInterface;

class ItemProjection implements ItemProjectionInterface
{


    protected ProducerProjectionInterface $producerFinder;

    public function __construct(
        protected CollectionFactoryInterface $collectionFactory,
        protected ConnectionInterface $connection,
        protected FilterApplierFactoryInterface $filterApplierFactory,
        protected ItemPhotoProjectionInterface $itemPhotoFinder,
        protected ArrayWrapperFactoryInterface $arrayWrapperFactory,
        protected ItemProjectionDependencyResolver $producerFinderCircularDependencyResolver
    ) {
        $this->producerFinder = $producerFinderCircularDependencyResolver->resolveProducerProjection($this);
    }

    /**
     * @return CollectionInterface|ItemDto[]
     */
    public function find(
        ItemFilter $filter
    ): CollectionInterface {
        $query = $this->connection->createQuery()
            ->select('i.*')
            ->from(Table::ITEM, 'i');

        $this->applyFilter($filter, $query);
        $records = $query->fetchAll();
        foreach ($records as &$record) {
            $record = json_decode($record['data'], true);
        }
        if ($filter->getWithPhotos()) {
            $this->mergePhotos($records);
        }
        if ($filter->getWithProducer()) {
            $this->mergeProducers($records);
        }

        $result = [];
        foreach ($records as $item) {
            $result[] = $this->createItemDto($item);
        }

        return $this->collectionFactory->createCollection($result);
    }

    public function count(ItemFilter $filter): int
    {
        $query = $this->connection->createQuery()
            ->select('COUNT(*) AS result')
            ->from(Table::ITEM, 'i');
        $this->applyFilter($filter->withoutLimitsAndSorts(), $query);
        return $query->fetch()['result'];
    }

    protected function mergePhotos(array &$items): void
    {
        $ids = [];
        foreach ($items as $item) {
            $item['photos'] = [];
            $ids[] = $item['id']['value'];
        }

        $photos = $this->itemPhotoFinder->find((ItemPhotoFilter::create())->setItemIds($ids));
        foreach ($photos as $photo) {
            foreach ($items as &$item) {
                if ($photo->getItemId() === $item['id']['value']) {
                    $item['photos'][] = $photo;
                }
            }
        }
    }

    private function mergeProducers(array &$items): void
    {
        $producerIds = [];
        foreach ($items as $item) {
            $item['producer'] = null;
            $producerIds[] = $item['producerId']['value'];
        }
        $producers = $this->producerFinder->find(ProducerFilter::create()->withIds($producerIds));
        foreach ($items as &$item) {
            foreach ($producers as $producer) {
                if ($producer->getId() === $item['producerId']['value']) {
                    $item['producer'] = $producer;
                }
            }
        }
    }

    protected function createItemDto(array $data): ItemDto
    {
        $data = $this->arrayWrapperFactory->create($data);
        return new ItemDto(
            $data->get('id.value'),
            $data->get('producerId.value'),
            $data->get('name'),
            $data->get('title.values'),
            $data->get('status.value'),
            (new DateTimeImmutable($data->get('date.value')))->getTimestamp(),
            $data->get('email.value'),
            $data->get('public'),
            $data->get('url.value'),
            $data->get('ip.value'),
            $data->get('description.values'),
            $data->get('width.value'),
            $data->get('height.value'),
            null !== $data->get('file') ? new FileDto(
                $data->get('file.name'),
                $data->get('file.path'),
                $data->get('file.mimeType')
            ) : null,
            null !== $data->get('image') ? new FileDto(
                $data->get('image.name'),
                $data->get('image.path'),
                $data->get('image.mimeType')
            ) : null,
            $data->get('photos'),
            $data->get('producer')
        );
    }

    protected function applyFilter(
        ItemFilter $filter,
        QueryInterface $query
    ): void {

        if ($filter->hasColumnFilter(ProducerNameColumnFilter::class)) {
            $query->leftJoin(Table::PRODUCER, 'p', "p.id = i.data->'producerId'->>'value'");
        }

        if ($filter->getId()) {
            $query->where('i.id = :id')->bindValue('id', $filter->getId());
        }

        if ($filter->getStatus()) {
            $query->where("i.data->'status'->>'value' = :status")->bindValue('status', $filter->getStatus());
        }

        if ($filter->getProducerId()) {
            $query->where("i.data->'producerId'->>'value' = :producerId")
                ->bindValue('producerId', $filter->getProducerId());
        }

        if ($filter->getProducerIds()) {
            $query->where("i.data->'producerId'->>'value' IN(:producerIds)")
                ->bindValue('producerIds', $filter->getProducerIds());
        }


        $this->filterApplierFactory->createFilterApplier()
            ->addSortableColumn('id', 'id')
            ->addSortableColumn('name', "data->>'name'")
            ->addSortableColumn('producerId', "data->>'producerId'")
            ->addSortableColumn('status', "data->>'status'")
            ->addSortableColumn('title', "data->'title'->'values'->>'%s'")
            ->addFilterableColumn('name', NameColumnFilter::class, "data->'name'->>'value'")
            ->addFilterableColumn('title', TitleColumnFilter::class, "data->'title'->'values'->>'%s'")
            ->addFilterableColumn('producerName', ProducerNameColumnFilter::class, "p.data->'name'->>'value'")
            ->apply($filter, $query);
    }
}
