<?php declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Application\Projection\ItemProjection;

use Grifix\Demo\Application\Projection\ItemProjection\ItemProjectionInterface;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerProjectionInterface;
use Grifix\Kit\Ioc\IocContainerInterface;

class ItemProjectionDependencyResolver implements ProducerProjectionDependencyResolverInterface
{
    protected IocContainerInterface $iocContainer;

    public function __construct(IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
    }

    public function resolveProducerProjection(ItemProjectionInterface $itemProjection): ProducerProjectionInterface
    {
        return $this->iocContainer->createNewInstance(
            ProducerProjectionInterface::class,
            [
                ItemProjectionInterface::class => $itemProjection
            ]
        );
    }
}
