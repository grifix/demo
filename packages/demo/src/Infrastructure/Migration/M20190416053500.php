<?php

declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Migration;

use Grifix\Demo\Infrastructure\Table;
use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */
class M20190416053500 extends AbstractMigration
{

    public function up(): void
    {
        $sql = <<<SQL
CREATE SCHEMA IF NOT EXISTS grifix_demo;

CREATE TABLE grifix_demo.producer (
  id uuid NOT NULL,
  data JSONB NOT NULL,
  CONSTRAINT producer_pkey PRIMARY KEY(id),
  CHECK (validate_json_schema($$
{
    "type": "object",
    "additionalProperties": false,
    "required": [
        "id",
        "name",
        "title",
        "createdAt",
        "rating"
    ],
    "properties": {
        "id": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "string"
                }
            }
        },
        "name": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value",
                "producerId"
            ],
            "properties": {
                "value": {
                    "type": "string"
                },
                "producerId": {
                    "type": "string"
                }
            }
        },
        "title": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "values"
            ],
            "properties": {
                "values": {
                    "oneOf": [
                        {
                            "type": "array"
                        },
                        {
                            "type": "object"
                        }
                    ]
                }
            }
        },
        "createdAt": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "string"
                }
            }
        },
        "rating": {
            "type": "object",
            "additionalProperties": false,
            "required": [
                "value"
            ],
            "properties": {
                "value": {
                    "type": "integer"
                }
            }
        }
    }
}
    $$, data))
) 
WITH (oids = false);

CREATE INDEX producer_idx ON grifix_demo.producer
  USING gin (data);

CREATE UNIQUE INDEX producer_data_name_idx ON grifix_demo.producer( (data->>'name') ) ;


CREATE TABLE grifix_demo.item (
  id uuid NOT NULL,
  data JSONB NOT NULL,
  CONSTRAINT item_pkey PRIMARY KEY(id)
) 
WITH (oids = false);

CREATE INDEX item_idx ON grifix_demo.item
  USING gin (data);


CREATE TABLE grifix_demo.item_photo (
  id uuid NOT NULL,
  item_id uuid NOT NULL,
  data JSONB NOT NULL,
  CONSTRAINT item_photo_pkey PRIMARY KEY(id),
  CONSTRAINT item_photo_fk FOREIGN KEY (item_id)
    REFERENCES grifix_demo.item(id)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE
) 
WITH (oids = false);

CREATE INDEX item_photo_idx ON grifix_demo.item_photo
  USING gin (data);

SQL;

        $this->execute($sql);
    }

    public function down(): void
    {
        $sql = <<<EOT
DROP TABLE IF EXISTS grifix_demo.item CASCADE ;
DROP TABLE IF EXISTS grifix_demo.item_photo CASCADE ;
DROP TABLE IF EXISTS grifix_demo.producer CASCADE ;
DROP SCHEMA IF EXISTS grifix_demo CASCADE ;
EOT;

        $this->execute($sql);
    }
}
