<?php
declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Grifix;

use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Psr\Log\LoggerInterface;

class Install implements ModuleCommandInterface
{
    use ModuleClassTrait;

    public function run(): void
    {
        echo "\nDemo module has been installed\n\n";
        $this->getShared(LoggerInterface::class)->debug('Demo module has been installed');
    }
}
