<?php
declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Grifix;

use Grifix\Demo\Domain\Item\Item;
use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;
use Grifix\Demo\Domain\Producer\Producer;
use Grifix\Demo\Domain\Producer\Repository\ProducerRepositoryInterface;
use Grifix\Demo\Infrastructure\Domain\Item\ItemBlueprint;
use Grifix\Demo\Infrastructure\Domain\Item\ItemRepository;
use Grifix\Demo\Infrastructure\Domain\Item\Photo\ItemPhotoBlueprint;
use Grifix\Demo\Infrastructure\Domain\Producer\ProducerBlueprint;
use Grifix\Demo\Infrastructure\Domain\Producer\ProducerRepository;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Repository\RepositoryFactoryInterface;

class Bootstrap implements ModuleCommandInterface
{
    use ModuleClassTrait;

    public function run(): void
    {
        $entityManager = $this->getShared(EntityManagerInterface::class);
        $entityManager->registerBlueprint(ProducerBlueprint::class);
        $entityManager->registerBlueprint(ItemBlueprint::class);
        $entityManager->registerBlueprint(ItemPhotoBlueprint::class);

        $this->setShared(ProducerRepositoryInterface::class, function () {
            /**@var IocContainerInterface $this */
            return $this->get(RepositoryFactoryInterface::class)->createWrapper(
                Producer::class,
                ProducerRepository::class
            );
        });

        $this->setShared(ItemRepositoryInterface::class, function () {
            /**@var IocContainerInterface $this */
            return $this->get(RepositoryFactoryInterface::class)->createWrapper(
                Item::class,
                ItemRepository::class
            );
        });
    }
}
