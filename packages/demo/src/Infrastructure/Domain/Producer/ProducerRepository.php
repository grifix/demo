<?php
declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Domain\Producer;

use Grifix\Demo\Domain\Producer\ProducerInterface;
use Grifix\Demo\Domain\Producer\Repository\Exception\ProducerNotFoundException;
use Grifix\Demo\Domain\Producer\Repository\ProducerRepositoryInterface;
use Grifix\Kit\Orm\Repository\RepositoryInterface;

class ProducerRepository implements ProducerRepositoryInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * ProducerRepository constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getById(string $id): ProducerInterface
    {
        $result = $this->repository->find($id);
        if (!$result) {
            throw new ProducerNotFoundException($id);
        }

        /**@var ProducerInterface $result */
        return $result;
    }

    public function add(ProducerInterface $producer): void
    {
        $this->repository->add($producer);
    }

    public function delete(ProducerInterface $producer): void
    {
        $this->repository->delete($producer);
    }
}
