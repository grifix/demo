<?php
declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Domain\Producer;

use Grifix\Demo\Domain\Producer\Producer;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Shared\Infrastructure\Internal\AbstractBlueprint;

class ProducerBlueprint extends AbstractBlueprint
{
    protected function getEntityClassName(): string
    {
        return Producer::class;
    }

    public function getTable(): string
    {
        return Table::PRODUCER;
    }
}
