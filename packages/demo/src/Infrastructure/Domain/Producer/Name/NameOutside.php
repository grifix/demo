<?php declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Domain\Producer\Name;

use Grifix\Demo\Domain\Producer\Name\NameOutsideInterface;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Shared\Application\Common\UniqueChecker\UniqueCheckerInterface;

class NameOutside implements NameOutsideInterface
{
    /** @var UniqueCheckerInterface */
    protected $uniqueChecker;

    public function __construct(UniqueCheckerInterface $uniqueChecker)
    {
        $this->uniqueChecker = $uniqueChecker;
    }

    public function isUnique(string $name, string $producerId): bool
    {
        return $this->uniqueChecker->isUnique("data->'name'->>'value'", Table::PRODUCER, $name, $producerId);
    }
}
