<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Infrastructure\Domain\Producer;

use Grifix\Demo\Domain\Producer\ProducerInfrastructureInterface;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherTrait;
use Grifix\Shared\Application\Common\Clock\ClockInterface;

class ProducerInfrastructure implements ProducerInfrastructureInterface
{
    use EventPublisherTrait;

    protected ClockInterface $clock;

    public function __construct(EventCollectorInterface $eventCollector, ClockInterface $clock)
    {
        $this->eventCollector = $eventCollector;
        $this->clock = $clock;
    }

    public function getCurrentTime(): int
    {
        return $this->clock->getTime();
    }
}
