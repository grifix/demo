<?php declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Domain\Item;

use Grifix\Demo\Domain\Item\ItemInfrastructureInterface;
use Grifix\Demo\Domain\Item\Photo\ItemPhotoCollectionInterface;
use Grifix\Demo\Infrastructure\Domain\Item\Photo\ItemPhotoCollection;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherTrait;
use Grifix\Shared\Domain\File\FileRestrictionsDto;

class ItemInfrastructure implements ItemInfrastructureInterface
{
    use EventPublisherTrait;


    public function __construct(
        protected EventCollectorInterface $eventCollector,
        protected CollectionFactoryInterface $collectionFactory,
        protected ConfigInterface $config
    ) {
    }

    public function createPhotosCollection(): ItemPhotoCollectionInterface
    {
        return $this->collectionFactory->createArrayCollectionWrapper(ItemPhotoCollection::class);
    }

    public function defineFileDirectory(): string
    {
        return sprintf(
            $this->config->get('grifix.demo.item.file.directoryTemplate'),
            date('Y_m'),
        );
    }

    public function defineImageDirectory(): string
    {
        return sprintf(
            $this->config->get('grifix.demo.item.image.directoryTemplate'),
            date('Y_m'),
        );
    }

    public function getFileRestrictions(): FileRestrictionsDto
    {
        return new FileRestrictionsDto(
            minSize: $this->config->get('grifix.demo.item.file.minSize'),
            maxSize: $this->config->get('grifix.demo.item.file.maxSize'),
            types: $this->config->get('grifix.demo.item.file.allowedTypes')
        );
    }

    public function getImageRestrictions(): FileRestrictionsDto
    {
        return new FileRestrictionsDto(
            mustBeImage: true,
            minImageWidth: $this->config->get('grifix.demo.item.image.minWidth'),
            maxImageWidth: $this->config->get('grifix.demo.item.image.maxWidth'),
            minImageHeight: $this->config->get('grifix.demo.item.image.minHeight'),
            maxImageHeight: $this->config->get('grifix.demo.item.image.maxHeight')
        );
    }
}
