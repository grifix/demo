<?php declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Domain\Item;

use Grifix\Demo\Domain\Item\ItemInterface;
use Grifix\Demo\Domain\Item\Repository\Exception\ItemNotFoundException;
use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\Repository\RepositoryInterface;
use Grifix\Kit\Specification\SpecificationInterface;

class ItemRepository implements ItemRepositoryInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * ProducerRepository constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getById(string $id): ItemInterface
    {
        $result = $this->repository->find($id);
        if (!$result) {
            throw new ItemNotFoundException($id);
        }

        /**@var ItemInterface $result */
        return $result;
    }

    public function add(ItemInterface $item): void
    {
        $this->repository->add($item);
    }

    public function delete(ItemInterface $item): void
    {
        $this->repository->delete($item);
    }
}
