<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Photo\ItemPhotoInfrastructureInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Shared\Domain\File\FileRestrictionsDto;

class ItemPhotoInfrastructure implements ItemPhotoInfrastructureInterface
{
    public function __construct(protected ConfigInterface $config)
    {
    }

    public function defineFileDirectory(): string
    {
        return sprintf(
            $this->config->get('grifix.demo.item.photo.file.directoryTemplate'),
            date('Y_m'),
        );
    }

    public function getFileRestrictions(): FileRestrictionsDto
    {
        return new FileRestrictionsDto(mustBeImage: true);
    }
}
