<?php declare(strict_types = 1);

namespace Grifix\Demo\Infrastructure\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Photo\Exception\ItemPhotoNotExistException;
use Grifix\Demo\Domain\Item\Photo\ItemPhotoCollectionInterface;
use Grifix\Demo\Domain\Item\Photo\ItemPhotoInterface;
use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Collection\CollectionInterface;

class ItemPhotoCollection implements ItemPhotoCollectionInterface
{
    use CollectionWrapperTrait;

    protected $collection;

    public function __construct(CollectionInterface $collection)
    {
        $this->collection = $collection;
    }

    public function add(ItemPhotoInterface $photo): void
    {
        $this->collection->add($photo);
    }

    public function getById(string $id): ItemPhotoInterface
    {
        /** @var ItemPhotoInterface|null $result */
        $result = $this->collection->find($id);
        if (!$result) {
            throw new ItemPhotoNotExistException($id);
        }
        return $result;
    }

    public function remove(ItemPhotoInterface $photo): void
    {
        $this->collection->remove($photo);
    }
}
