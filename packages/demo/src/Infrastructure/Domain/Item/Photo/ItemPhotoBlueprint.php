<?php declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Item;
use Grifix\Demo\Domain\Item\Photo\ItemPhoto;
use Grifix\Demo\Infrastructure\Domain\Item\ItemBlueprint;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Shared\Infrastructure\Internal\AbstractBlueprint;

class ItemPhotoBlueprint extends AbstractBlueprint
{
    const ITEM_COLUMN = 'item_id';

    public function init(): void
    {
        $this->addRelationToParent(
            Item::class,
            ItemBlueprint::PHOTOS_PROPERTY,
            self::ITEM_COLUMN
        );
    }

    protected function getEntityClassName(): string
    {
        return ItemPhoto::class;
    }

    public function getTable(): string
    {
        return Table::ITEM_PHOTO;
    }
}
