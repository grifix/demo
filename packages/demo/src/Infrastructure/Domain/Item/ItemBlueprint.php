<?php declare(strict_types=1);

namespace Grifix\Demo\Infrastructure\Domain\Item;

use Grifix\Demo\Domain\Item\Item;
use Grifix\Demo\Domain\Item\Photo\ItemPhoto;
use Grifix\Demo\Infrastructure\Domain\Item\Photo\ItemPhotoBlueprint;
use Grifix\Demo\Infrastructure\Domain\Item\Photo\ItemPhotoCollection;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Shared\Infrastructure\Internal\AbstractBlueprint;

class ItemBlueprint extends AbstractBlueprint
{
    const PHOTOS_PROPERTY = 'photos';

    protected function init(): void
    {
        $this->addRelationToChild(
            self::PHOTOS_PROPERTY,
            ItemPhoto::class,
            ItemPhotoBlueprint::ITEM_COLUMN,
            ItemPhotoCollection::class
        );
    }

    protected function getEntityClassName(): string
    {
        return Item::class;
    }

    public function getTable(): string
    {
        return Table::ITEM;
    }
}
