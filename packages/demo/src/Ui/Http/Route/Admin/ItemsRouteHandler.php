<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Route\Admin;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\Handler\AbstractRouteHandler;
use Grifix\Kit\Route\RouteInterface;

class ItemsRouteHandler extends AbstractRouteHandler
{
    public function __invoke(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        return $this->render($response, 'grifix.demo.{skin}.admin.item.tpl.index');
    }
}
