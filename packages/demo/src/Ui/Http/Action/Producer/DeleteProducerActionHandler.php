<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Producer;

use Grifix\Demo\Application\Command\Producer\Delete\DeleteProducerCommand;
use Grifix\Demo\Ui\Common\Producer\RequestValidator\DeleteProducerRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class DeleteProducerActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $this->createRequestValidator(DeleteProducerRequestValidator::class)->validate($params->getArray());
        $ids = $params->get(DeleteProducerRequestValidator::ID);
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        foreach ($ids as $id) {
            $this->executeCommand(new DeleteProducerCommand($id));
        }

        return [];
    }
}
