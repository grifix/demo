<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Producer;

use Grifix\Demo\Application\Projection\ProducerProjection\ProducerFilter;
use Grifix\Demo\Application\Query\FindProducers\FindProducersQuery;
use Grifix\Demo\Application\Query\FindProducers\FindProducersQueryResult;
use Grifix\Kit\Exception\UiException;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class GetProducerActionHandler extends AbstractActionHandler
{
    public function hasSideEffects(): bool
    {
        return false;
    }

    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $filter = ProducerFilter::create();
        $filter->withId($params->get('id'));

        /** @var FindProducersQueryResult $result */
        $result = $this->executeQuery(new FindProducersQuery($filter));

        if (count($result->getProducers()) < 1) {
            throw new UiException(
                'grifix.demo.producerNotFound',
                $this->getShared(TranslatorInterface::class)->translate('grifix.demo.msg_ProducerNotFound'),
                404
            );
        }

        return ['producer' => $result->getProducers()[0]];
    }
}
