<?php
declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Producer;

use Grifix\Demo\Application\Command\Producer\Create\CreateProducerCommand;
use Grifix\Demo\Ui\Common\Producer\RequestValidator\CreateProducerRequestValidator;
use Grifix\Shared\Application\Query\GetFreeId\GetFreeUuidQuery;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;
use Grifix\Shared\Ui\Common\RequestValidator\RequestValidatorFactoryInterface;

class CreateProducerActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $id = $this->getFreeProducerId();

        $this->getShared(RequestValidatorFactoryInterface::class)
            ->create(CreateProducerRequestValidator::class)
            ->setStrictStrategy()
            ->validate($params->getArray());

        $this->executeCommand(new CreateProducerCommand(
            $id,
            $params->get(CreateProducerRequestValidator::NAME),
            $params->get(CreateProducerRequestValidator::TITLE)
        ));
        return ['id' => $id];
    }


    protected function getFreeProducerId(): string
    {
        return $this->executeQuery(new GetFreeUuidQuery());
    }
}
