<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Producer;

use Grifix\Demo\Application\Command\Producer\Update\UpdateProducerCommand;
use Grifix\Demo\Ui\Common\Producer\RequestValidator\UpdateProducerRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class UpdateProducerActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $id = $params->get(UpdateProducerRequestValidator::ID);
        $this->createRequestValidator(UpdateProducerRequestValidator::class)->validate($params->getArray());
        $this->executeCommand(new UpdateProducerCommand(
            $id,
            $params->get(UpdateProducerRequestValidator::NAME),
            $params->get(UpdateProducerRequestValidator::TITLE)
        ));
        return [];
    }
}
