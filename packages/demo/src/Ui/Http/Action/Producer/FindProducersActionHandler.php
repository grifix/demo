<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Producer;

use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\CreatedAtColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\NameColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\RatingColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ColumnFilter\TitleColumnFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerFilter;
use Grifix\Demo\Application\Query\FindProducers\FindProducersQuery;
use Grifix\Demo\Application\Query\FindProducers\FindProducersQueryResult;
use Grifix\Demo\Ui\Common\Producer\Protector\ProducerProtectorInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\OffsetCalculatorInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;
use Grifix\Shared\Ui\Common\ColumnFilterFactory\ColumnFilterFactoryInterface;

class FindProducersActionHandler extends AbstractActionHandler
{
    public const PARAM_ID = 'id';
    public const PARAM_NAME = 'name';
    public const PARAM_TITLE = 'title';
    public const PARAM_CREATED_AT = 'createdAt';
    public const PARAM_RATING = 'rating';
    public const PARAM_WITH_ITEMS = 'withItems';
    public const PARAM_WITH_ITEMS_PHOTOS = 'withItemsPhotos';

    public function hasSideEffects(): bool
    {
        return false;
    }

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var OffsetCalculatorInterface
     */
    protected $offsetCalculator;

    /** @var ProducerProtectorInterface $producerProtector */
    protected $producerProtector;

    /** @var ColumnFilterFactoryInterface */
    protected $columnFilterFactory;

    protected function init(): void
    {
        $this->arrayHelper = $this->getShared(ArrayHelperInterface::class);
        $this->offsetCalculator = $this->getShared(OffsetCalculatorInterface::class);
        $this->producerProtector = $this->getShared(ProducerProtectorInterface::class);
        $this->columnFilterFactory = $this->getShared(ColumnFilterFactoryInterface::class);
        parent::init();
    }

    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);

        $pageIndex = $this->convertToInt($params->get(self::PARAM_PAGE_INDEX));
        $pageSize = $this->convertToInt($params->get(self::PARAM_PAGE_SIZE));

        $offset = $this->calculateOffset($pageIndex, $pageSize);

        $filter = ProducerFilter::create();

        $filter->setSortDirection('desc');

        $filter
            ->withId($params->get(self::PARAM_ID))
            ->setLangCode($params->get(self::PARAM_LANG_CODE, $this->getCurrentLangCode()))
            ->setOffset($offset)
            ->setLimit($pageSize)
            ->setSortColumn($params->get(self::PARAM_SORT_COLUMN, 'createdAt'))
            ->setSortDirection($params->get(self::PARAM_SORT_DIRECTION));

        if ($params->get(self::PARAM_WITH_ITEMS)) {
            $itemFilter = ItemFilter::create();
            if ($params->get(self::PARAM_WITH_ITEMS_PHOTOS)) {
                $itemFilter->setWithPhotos(true);
            }
            $filter->setItemFilter($itemFilter);
        }

        $this->applyColumnFilters(
            $filter,
            [
                self::PARAM_NAME => NameColumnFilter::class,
                self::PARAM_TITLE => TitleColumnFilter::class,
                self::PARAM_CREATED_AT => CreatedAtColumnFilter::class,
                self::PARAM_RATING => RatingColumnFilter::class
            ],
            $params->get(self::PARAM_COLUMN_FILTERS, [])
        );

        /** @var FindProducersQueryResult $result */
        $result = $this->executeQuery(
            new FindProducersQuery(
                $filter,
                (bool) $params->get(self::PARAM_COUNT_TOTAL),
            )
        );

        foreach ($result->getProducers() as $producer) {
            $this->producerProtector->protectProducer($producer);
        }

        return [self::PARAM_RESULT => $result];
    }

}
