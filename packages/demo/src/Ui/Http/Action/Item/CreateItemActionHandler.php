<?php
declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Command\Item\Create\CreateItemCommand;
use Grifix\Demo\Ui\Common\Item\RequestValidator\CreateItemRequestValidator;
use Grifix\Shared\Application\Query\GetFreeId\GetFreeUuidQuery;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class CreateItemActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $params->set(CreateItemRequestValidator::PUBLIC, $this->convertBool($params->get(CreateItemRequestValidator::PUBLIC)));
        $id = $this->executeQuery(new GetFreeUuidQuery());

        $this->createRequestValidator(CreateItemRequestValidator::class)->validate($params->getArray());

        if ($params->get(CreateItemRequestValidator::FILE)) {
            $params->set(CreateItemRequestValidator::FILE, $this->createFileDto($params->get(CreateItemRequestValidator::FILE)));
        }

        if ($params->get(CreateItemRequestValidator::IMAGE)) {
            $params->set(CreateItemRequestValidator::IMAGE, $this->createFileDto($params->get(CreateItemRequestValidator::IMAGE)));
        }

        if ($params->get(CreateItemRequestValidator::DATE)) {
            $params->set(CreateItemRequestValidator::DATE, $this->convertDate($params->get(CreateItemRequestValidator::DATE)));
        }

        $this->executeCommand(new CreateItemCommand(
            $id,
            $params->get(CreateItemRequestValidator::PRODUCER_ID),
            $params->get(CreateItemRequestValidator::NAME),
            $params->get(CreateItemRequestValidator::TITLE),
            $params->get(CreateItemRequestValidator::DATE),
            $params->get(CreateItemRequestValidator::EMAIL),
            (int) $params->get(CreateItemRequestValidator::WIDTH),
            (int) $params->get(CreateItemRequestValidator::HEIGHT),
            $params->get(CreateItemRequestValidator::FILE),
            $params->get(CreateItemRequestValidator::IMAGE),
            (bool) $params->get(CreateItemRequestValidator::PUBLIC),
            $params->get(CreateItemRequestValidator::URL),
            $params->get(CreateItemRequestValidator::IP),
            $params->get(CreateItemRequestValidator::DESCRIPTION)
        ));
        return ['id' => $id];
    }
}
