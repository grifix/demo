<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Command\Item\AddPhoto\AddItemPhotoCommand;
use Grifix\Demo\Application\Command\Item\AddPhoto\Dto\PhotoDto;
use Grifix\Demo\Ui\Common\Item\RequestValidator\AddItemPhotoRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class AddItemPhotoActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);

        $this->createRequestValidator(AddItemPhotoRequestValidator::class)->validate($params->getArray());

        $id = $this->getFreeId();

        $this->executeCommand(
            new AddItemPhotoCommand(
                $params->get(AddItemPhotoRequestValidator::ITEM_ID),
                new PhotoDto(
                    $id,
                    $this->createFileDto($params->get(AddItemPhotoRequestValidator::IMAGE)),
                    $params->get(AddItemPhotoRequestValidator::DESCRIPTION)
                )
            )
        );

        return [
            'id' => $id
        ];
    }
}
