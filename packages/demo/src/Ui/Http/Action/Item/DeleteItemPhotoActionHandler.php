<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Command\Item\DeletePhoto\DeleteItemPhotoCommand;
use Grifix\Demo\Ui\Common\Item\RequestValidator\DeleteItemPhotoRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class DeleteItemPhotoActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);

        $this->createRequestValidator(DeleteItemPhotoRequestValidator::class)->validate($params->getArray());

        $this->executeCommand(
            new DeleteItemPhotoCommand(
                $params->get(DeleteItemPhotoRequestValidator::ITEM_ID),
                $params->get(DeleteItemPhotoRequestValidator::PHOTO_ID)
            )
        );

        return [];
    }
}
