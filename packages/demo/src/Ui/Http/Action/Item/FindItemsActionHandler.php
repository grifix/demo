<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter\NameColumnFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter\ProducerNameColumnFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter\TitleColumnFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Demo\Application\Query\FindItems\FindItemsQuery;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class FindItemsActionHandler extends AbstractActionHandler
{

    public const PARAM_ID = 'id';
    public const PARAM_PRODUCER_ID = 'producerId';
    public const PARAM_STATUS = 'status';
    public const PARAM_NAME = 'name';
    public const PARAM_TITLE = 'title';
    public const PARAM_WITH_PHOTOS = 'withPhotos';
    public const PARAM_WITH_PRODUCER = 'withProducer';
    public const PARAM_PRODUCER_NAME = 'producerName';

    public function hasSideEffects(): bool
    {
        return false;
    }

    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);

        $pageIndex = $this->convertToInt($params->get(self::PARAM_PAGE_INDEX));
        $pageSize = $this->convertToInt($params->get(self::PARAM_PAGE_SIZE));

        $offset = $this->calculateOffset($pageIndex, $pageSize);

        $filter = ItemFilter::create();
        $filter
            ->withId($params->get(self::PARAM_ID))
            ->setProducerId($params->get(self::PARAM_PRODUCER_ID))
            ->setStatus($params->get(self::PARAM_STATUS))
            ->setLangCode($params->get(self::PARAM_LANG_CODE, $this->getCurrentLangCode()))
            ->setOffset($offset)
            ->setLimit($pageSize)
            ->setWithPhotos((bool)$params->get(self::PARAM_WITH_PHOTOS))
            ->setWithProducer((bool) $params->get(self::PARAM_WITH_PRODUCER))
            ->setSortColumn($params->get(self::PARAM_SORT_COLUMN))
            ->setSortDirection($params->get(self::PARAM_SORT_DIRECTION));

        $this->applyColumnFilters(
            $filter,
            [
                self::PARAM_NAME => NameColumnFilter::class,
                self::PARAM_TITLE => TitleColumnFilter::class,
                self::PARAM_PRODUCER_NAME => ProducerNameColumnFilter::class
            ],
            $params->get(self::PARAM_COLUMN_FILTERS, [])
        );

        $result = $this->executeQuery(new FindItemsQuery($filter, (bool) $params->get(self::PARAM_COUNT_TOTAL)));

        return [self::PARAM_RESULT => $result];
    }
}
