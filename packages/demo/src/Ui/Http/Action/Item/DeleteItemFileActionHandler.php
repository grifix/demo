<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Command\Item\DeleteFile\DeleteItemFileCommand;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class DeleteItemFileActionHandler extends AbstractActionHandler
{

    public const PARAM_ITEM_ID = 'item_id';

    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $this->executeCommand(new DeleteItemFileCommand($params->get(self::PARAM_ITEM_ID)));
        return [];
    }
}
