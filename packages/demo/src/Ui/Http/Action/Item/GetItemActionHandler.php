<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Demo\Application\Query\FindItems\FindItemsQuery;
use Grifix\Demo\Application\Query\FindItems\FindItemsQueryResult;
use Grifix\Kit\Exception\UiException;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

class GetItemActionHandler extends AbstractActionHandler
{
    public function hasSideEffects(): bool
    {
        return false;
    }

    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);
        $filter = ItemFilter::create();
        $filter->withId($params->get('id'));

        /** @var FindItemsQueryResult $result */
        $result = $this->executeQuery(new FindItemsQuery($filter));

        if (count($result->getItems()) < 1) {
            throw new UiException(
                'grifix.demo.producerNotFound',
                $this->translate('grifix.demo.msg_ProducerNotFound'),
                404
            );
        }

        return ['item' => $result->getItems()[0]];
    }
}
