<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action\Item;

use Grifix\Demo\Application\Command\Item\ChangeFile\ChangeItemFileCommand;
use Grifix\Demo\Ui\Common\Item\RequestValidator\ChangeItemFileRequestValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;
use Grifix\Shared\Ui\Common\RequestValidator\RequestValidatorFactoryInterface;

class ChangeItemFileActionHandler extends AbstractActionHandler
{
    public function __invoke(array $params = []): array
    {
        $params = $this->wrapArray($params);

        $this->createRequestValidator(ChangeItemFileRequestValidator::class)->validate($params->getArray());

        $fileDto = $this->createFileDto($params->get(ChangeItemFileRequestValidator::FILE));
        $this->executeCommand(
            new ChangeItemFileCommand($params->get(ChangeItemFileRequestValidator::ITEM_ID), $fileDto)
        );

        return [];
    }
}
