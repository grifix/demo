<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action;

use Grifix\Kit\Validation\Field\EmailValidationField;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Kit\Validation\ValidationInterface;
use Grifix\Kit\Validation\Validator\NoTagsValidator;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

/**
 * Class SubmitFormRequestHandler
 *
 * @category Grifix
 * @package  Grifix\Demo\Ui\Http\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SubmitFormActionHandler extends AbstractActionHandler
{
    const NAME = 'name';
    const EMAIL = 'email';
    const ADDRESS = 'address';
    const FILE = 'file';
    
    const ADDRESS_COUNT = 3;

    /**
     * {@inheritdoc}
     */
    public function __invoke(array $params = []): array
    {
        sleep(2);
        $data = $params;
        for ($i = 1; $i < (self::ADDRESS_COUNT + 1); $i++) {
            $data[self::ADDRESS . '[' . $i . ']'] = $params[self::ADDRESS][$i];
        }
        
        $this->doCreateValidation()->validateOrFail($data);
        
        return [];
    }
    
    /**
     * @return ValidationInterface
     */
    protected function doCreateValidation(): ValidationInterface
    {
        $validation = $this->getShared(ValidationFactoryInterface::class)
            ->create()->setStrictStrategy();
        $validation->createField(self::EMAIL, EmailValidationField::class)->setLabel('Email')->setNotEmpty();
       // $validation->createField(self::FILE, EmailField::class)->setLabel('File')->setNotEmpty();
        $validation->createField(self::NAME)->setLabel('Name')->createValidator(NoTagsValidator::class);
        for ($i = 1; $i < (self::ADDRESS_COUNT + 1); $i++) {
            $validation->createField(self::ADDRESS . '[' . $i . ']')->setLabel('Address ' . $i)->setNotEmpty();
        }
       
        
        return $validation;
    }
}
