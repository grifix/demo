<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Demo\Ui\Http\Action;


use Grifix\Demo\Application\Query\GetList\Dto\ItemsList;
use Grifix\Demo\Application\Query\GetList\GetListQuery;
use Grifix\Kit\Conversion\ConversionFactoryInterface;
use Grifix\Kit\Conversion\Converter\StringToIntConverter;
use Grifix\Kit\Validation\Field\IntValidationField;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Shared\Ui\Http\Action\AbstractActionHandler;

/**
 * Class GetListRequestHandler
 *
 * @category Grifix
 * @package  Grifix\Demo\Ui\Http\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetListActionHandler extends AbstractActionHandler
{
    public function hasSideEffects(): bool
    {
        return false;
    }

    public function __invoke(array $params = []): array
    {
        
        $this->convert($params);
        $this->validate($params);
        
        /**@var $itemsList ItemsList */
        $itemsList = $this->executeQuery(new GetListQuery($params['page']));
        
        return [
            'items' => $itemsList->getItems(),
            'numOfPages' => $itemsList->getNumOfPages(),
        ];
    }
    
    /**
     * @param array $request
     *
     * @return void
     */
    protected function validate(array $request)
    {
        $validation = $this->getShared(ValidationFactoryInterface::class)->create();
        $validation->createField('page', IntValidationField::class)->setNotEmpty();
        $validation->validateOrFail($request);
    }
    
    /**
     * @param array $request
     *
     * @return void
     */
    protected function convert(array &$request)
    {
        $conversion = $this->getShared(ConversionFactoryInterface::class)->create();
        $conversion->createField('page')->createConverter(StringToIntConverter::class);
        $conversion->convert($request);
    }
}
