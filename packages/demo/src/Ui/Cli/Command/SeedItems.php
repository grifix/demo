<?php declare(strict_types = 1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Ui\Cli\Command;

use Grifix\Demo\Application\Command\Item\AddPhoto\AddItemPhotoCommand;
use Grifix\Demo\Application\Command\Item\AddPhoto\Dto\PhotoDto;
use Grifix\Demo\Application\Command\Item\Create\CreateItemCommand;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Kit\Alias;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Faker\FakerInterface;
use Grifix\Kit\Helper\FilesystemHelper\FilesystemHelperInterface;
use Grifix\Shared\Application\Common\LangsRepository\LangDto;
use Grifix\Shared\Application\Query\GetFreeId\GetFreeUuidQuery;
use Grifix\Shared\Application\Query\GetLangs\GetLangsQuery;
use Grifix\Shared\Domain\File\FileDto;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SeedItems extends AbstractCommand
{
    protected const ARGUMENT_QUANTITY = 'quantity';

    /**
     * @var FakerInterface
     */
    protected $faker;

    /**
     * @var LangDto[]
     */
    protected $langs = [];

    protected function configure()
    {
        $this
            ->setName('demo:seed_items')
            ->setDescription('Seed items')
            ->addArgument(self::ARGUMENT_QUANTITY, InputArgument::OPTIONAL, 'quantity', 100);
        parent::configure();
    }

    protected function init()
    {
        $this->faker = $this->getShared(FakerInterface::class);
        $this->langs = $this->executeQuery(new GetLangsQuery());
        parent::init();
    }

    /**
     * @return string[]
     */
    protected function createWord(): array
    {
        $result = [];
        foreach ($this->langs as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeWord();
        }
        return $result;
    }

    protected function createDescription(): array
    {
        $result = [];
        foreach ($this->langs as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeText();
        }
        return $result;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var int $quantity */
        $quantity = $input->getArgument(self::ARGUMENT_QUANTITY);

        $this->getShared(ConnectionInterface::class)->delete(Table::ITEM_PHOTO, []);
        $this->getShared(ConnectionInterface::class)->delete(Table::ITEM, []);
        $this->getShared(FilesystemHelperInterface::class)
            ->deleteDir($this->getShared(Alias::PUBLIC_DIR) . '/demo/items');

        $io = new SymfonyStyle($input, $output);
        $io->progressStart($quantity);

        for ($i = 0; $i < $quantity; $i++) {
            $itemId = $this->executeQuery(new GetFreeUuidQuery());
            $this->executeCommand(new CreateItemCommand(
                $itemId,
                $this->getProducerId(),
                $this->faker->setUnique()->fakeWord(),
                $this->createWord(),
                $this->faker->fakeDate()->getTimestamp(),
                $this->faker->setUnique()->fakeEmail(),
                mt_rand(1, 100),
                mt_rand(1, 100),
                new FileDto(
                    $this->makeFilePath(),
                    sprintf('%s.txt', $this->faker->fakeWord())
                ),
                new FileDto(
                    $this->makeImagePath(),
                    sprintf('%s.png', $this->faker->fakeWord())
                ),
                (bool) mt_rand(0, 1),
                $this->faker->fakeUrl(),
                $this->faker->fakeIp(),
                $this->createDescription()
            ));

            $maxPhotos = mt_rand(1, 10);
            for ($j = 0; $j < $maxPhotos; $j++) {
                $this->executeCommand(new AddItemPhotoCommand(
                    $itemId,
                    new PhotoDto(
                        $this->executeQuery(new GetFreeUuidQuery()),
                        new FileDto(
                            $this->makeImagePath(),
                            $this->executeQuery(new GetFreeUuidQuery()),
                            $this->faker->fakeWord()
                        ),
                        $this->createDescription(),
                        $j + 1
                    )
                ));
            }

            $io->progressAdvance();
        }
        $io->progressFinish();
        return 0;
    }

    protected function getProducerId(): string
    {
        $producer = $this->getShared(ConnectionInterface::class)->createQuery()
            ->select('id')
            ->from(Table::PRODUCER)
            ->orderBy("random()")
            ->limit(1)
            ->fetch();
        if (!$producer) {
            throw new \Exception('There is no producers, seed producers first!');
        }
        return $producer['id'];
    }

    protected function makeFilePath(): string
    {
        return sprintf('%s/packages/demo/tests/files/file.txt', $this->getShared(Alias::ROOT_DIR));
    }

    protected function makeImagePath(): string
    {
        return sprintf('%s/packages/demo/tests/files/50x50.png', $this->getShared(Alias::ROOT_DIR));
    }
}
