<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Ui\Cli\Command;

use Grifix\Demo\Application\Command\Test\TestCommand;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommandBusCommand extends AbstractCommand
{

    protected const ARGUMENT_MESSAGE = 'message';

    /**
     * @var CommandBusInterface
     */
    protected $commandBus;

    protected function configure()
    {
        $this
            ->setName('demo:test_command_bus')
            ->setDescription('Command bus test. If it works it will be new records in tmp/command.log')
            ->addArgument(self::ARGUMENT_MESSAGE, InputArgument::OPTIONAL, 'message', 'test');
    }

    protected function init()
    {
        $this->commandBus = $this->getShared(CommandBusInterface::class);
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->commandBus->execute(new TestCommand(
            $input->getArgument(self::ARGUMENT_MESSAGE),
            new \DateTime()
        ));

        return 0;
    }
}
