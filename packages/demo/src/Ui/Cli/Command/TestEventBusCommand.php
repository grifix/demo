<?php

declare(strict_types=1);

namespace Grifix\Demo\Ui\Cli\Command;

use Grifix\Demo\Application\Event\TestEvent;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Event\Bus\EventBusInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestEventBusCommand extends AbstractCommand
{

    protected const ARGUMENT_MESSAGE = 'message';

    /**
     * @var \Grifix\Kit\Event\Bus\EventBusInterface
     */
    protected $eventBus;

    protected function configure()
    {
        $this
            ->setName('demo:test_event_bus')
            ->setDescription('Event bus test. If it works it will be new records in tmp/event.log')
            ->addArgument(self::ARGUMENT_MESSAGE, InputArgument::OPTIONAL, 'message', 'test');
    }

    protected function init()
    {
        $this->eventBus = $this->getShared(EventBusInterface::class);
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->eventBus->send(new TestEvent(
            $input->getArgument(self::ARGUMENT_MESSAGE),
            new \DateTime()
        ));

        return 0;
    }
}
