<?php declare(strict_types=1);

namespace Grifix\Demo\Ui\Cli\Command;

use Grifix\Demo\Application\Command\Producer\Create\CreateProducerCommand;
use Grifix\Demo\Application\Command\Producer\Delete\DeleteProducerCommand;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerProjectionInterface;
use Grifix\Demo\Domain\Producer\Repository\Exception\ProducerNotFoundException;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Faker\FakerInterface;
use Grifix\Shared\Application\Common\LangsRepository\LangDto;
use Grifix\Shared\Application\Query\GetFreeId\GetFreeUuidQuery;
use Grifix\Shared\Application\Query\GetLangs\GetLangsQuery;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SeedProducers extends AbstractCommand
{
    protected const ARGUMENT_QUANTITY = 'quantity';

    protected FakerInterface $faker;

    /**
     * @var LangDto[]
     */
    protected array $langs = [];

    protected ProducerProjectionInterface $producerProjection;

    protected function configure()
    {
        $this
            ->setName('demo:seed_producers')
            ->setDescription('Seed producers')
            ->addArgument(self::ARGUMENT_QUANTITY, InputArgument::OPTIONAL, 'quantity', 100);
        parent::configure();
    }

    protected function init()
    {
        $this->faker = $this->getShared(FakerInterface::class);
        $this->langs = $this->executeQuery(new GetLangsQuery());
        $this->producerProjection = $this->getShared(ProducerProjectionInterface::class);
        parent::init();
    }

    /**
     * @return string[]
     */
    protected function createCompanyTitle(): array
    {
        $result = [];
        foreach ($this->langs as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeCompanyName();
        }
        return $result;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var int $quantity */
        $quantity = $input->getArgument(self::ARGUMENT_QUANTITY);
        $io = new SymfonyStyle($input, $output);

        $output->writeln('Deleting existing produces... ');
        $existedProducers = $this->producerProjection->count();
        $io->progressStart($existedProducers);
        foreach ($this->producerProjection->find() as $producerDto) {
            try {
                $this->executeCommand(new DeleteProducerCommand($producerDto->getId()));
            } catch (ProducerNotFoundException $exception) {
            }

            $io->progressAdvance();
        }
        $io->progressFinish();

        $output->writeln('Creating new producers... ');
        $io->progressStart($quantity);
        for ($i = 0; $i < $quantity; $i++) {
            $this->executeCommand(new CreateProducerCommand(
                $this->executeQuery(new GetFreeUuidQuery()),
                $this->faker->setLocale()->setUnique(true)->fakeCompanyName() . uniqid(),
                $this->createCompanyTitle(),
                $this->faker->fakeInt(1, 5),
                $this->faker->fakeDate()->getTimestamp()
            ));
            $io->progressAdvance();
        }
        $io->progressFinish();
        return 0;
    }
}
