<?php
declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common;

interface DemoModulePermissions
{
    public const CREATE_PRODUCER = 'grifix.demo.createProducer';
    public const ADMIN_PRODUCERS = 'grifix.demo.admin.producers';
    public const READ_PRODUCER_NAME = 'grifix.demo.readProducerName';
    public const READ_PRODUCER_TITLE = 'grifix.demo.readProducerTitle';
    public const DELETE_PRODUCER = 'grifix.demo.deleteProducer';
    public const UPDATE_PRODUCER = 'grifix.demo.updateProducer';
}
