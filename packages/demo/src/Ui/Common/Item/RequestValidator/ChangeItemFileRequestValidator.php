<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Item\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class ChangeItemFileRequestValidator extends AbstractRequestValidator
{
    public const ITEM_ID = 'item_id';
    public const FILE = 'file';

    protected function init(): void
    {
        parent::init();

        $this
            ->addField(
                $this->buildFiled(
                    self::ITEM_ID,
                    'grifix.demo.item'
                )
                    ->uuid()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::FILE,
                    'grifix.kit.file'
                )
                    ->uploadedFile()
                    ->required()
            );
    }
}
