<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Item\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class AddItemPhotoRequestValidator extends AbstractRequestValidator
{
    public const ITEM_ID = 'item_id';
    public const IMAGE = 'image';
    public const DESCRIPTION = 'description';
    public const SEQUENCE = 'sequence';

    protected function init(): void
    {
        parent::init();

        $this
            ->addField(
                $this->buildFiled(
                    self::ITEM_ID,
                    'grifix.demo.item'
                )
                    ->uuid()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::IMAGE,
                    'grifix.kit.file'
                )
                    ->uploadedFile()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::DESCRIPTION,
                    'grifix.kit.description'
                )
                    ->text()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::SEQUENCE,
                    'grifix.kit.sequence'
                )
                    ->int()
            );
    }
}
