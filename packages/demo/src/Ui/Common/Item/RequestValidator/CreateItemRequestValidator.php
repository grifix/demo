<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Item\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class CreateItemRequestValidator extends AbstractRequestValidator
{
    public const NAME = 'name';
    public const TITLE = 'title';
    public const PRODUCER_ID = 'producer_id';
    public const DATE = 'date';
    public const EMAIL = 'email';
    public const PUBLIC = 'public';
    public const FILE = 'file';
    public const IMAGE = 'image';
    public const URL = 'url';
    public const IP = 'ip';
    public const DESCRIPTION = 'description';
    public const HEIGHT = 'height';
    public const WIDTH = 'width';

    protected function init(): void
    {
        parent::init();

        $this
            ->addField(
                $this->buildFiled(
                    self::NAME,
                    'grifix.kit.name'
                )
                    ->text()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::TITLE,
                    'grifix.kit.title'
                )
                    ->text()
                    ->required()
                    ->multiLang()
            )
            ->addField(
                $this->buildFiled(
                    self::PRODUCER_ID,
                    'grifix.demo.producer'
                )
                    ->text()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::WIDTH,
                    'grifix.kit.width'
                )
                    ->positiveInt()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::HEIGHT,
                    'grifix.kit.height'
                )
                    ->positiveInt()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::DATE,
                    'grifix.kit.date'
                )
                    ->date()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::EMAIL,
                    'grifix.kit.email'
                )
                    ->email()
                    ->required()
            )
            ->addField(
                $this->buildFiled(
                    self::PUBLIC,
                    'grifix.kit.published'
                )
                    ->bool()
            )
            ->addField(
                $this->buildFiled(
                    self::IP,
                    'grifix.kit.ip'
                )
                    ->ip()
            )
            ->addField(
                $this->buildFiled(
                    self::DESCRIPTION,
                    'grifix.kit.description'
                )
                    ->text()
                    ->multiLang()
            )
            ->addField(
                $this->buildFiled(
                    self::FILE,
                    'grifix.kit.file'
                )
                    ->uploadedFile()
            )
            ->addField(
                $this->buildFiled(
                    self::IMAGE,
                    'grifix.kit.image'
                )
                    ->uploadedFile()
            );
    }
}
