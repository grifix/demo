<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Producer\RequestValidator;

use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class DeleteProducerRequestValidator extends AbstractRequestValidator
{
    public const ID = 'id';

    protected function init(): void
    {
        parent::init();
        $this->addField(
            $this->buildFiled(
                self::ID,
                'grifix.demo.producer'
            )
                ->uuid()
                ->required()
        );
    }
}
