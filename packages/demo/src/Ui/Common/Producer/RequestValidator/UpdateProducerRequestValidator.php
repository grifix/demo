<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Producer\RequestValidator;

use Grifix\Demo\Domain\Producer\Name\NameInterface;
use Grifix\Demo\Domain\Producer\Title\TitleInterface;
use Grifix\Shared\Ui\Common\RequestValidator\AbstractRequestValidator;

class UpdateProducerRequestValidator extends AbstractRequestValidator
{
    public const ID = 'id';
    public const NAME = 'name';
    public const TITLE = 'title';

    protected function init(): void
    {
        parent::init();

        $this->addField(
            $this->buildFiled(
                self::ID,
                'grifix.kit.id'
            )
                ->uuid()
                ->required()
        )->addField(
            $this->buildFiled(
                self::NAME,
                'grifix.kit.name'
            )
                ->text()
                ->maxSymbols(NameInterface::MAX_LEN)
                ->minSymbols(NameInterface::MIN_LEN)
        )->addField(
            $this->buildFiled(
                self::TITLE,
                'grifix.kit.title'
            )
                ->text()
                ->multiLang()
                ->minSymbols(TitleInterface::MIN_LEN)
                ->maxSymbols(TitleInterface::MAX_LEN)
        );
    }
}
