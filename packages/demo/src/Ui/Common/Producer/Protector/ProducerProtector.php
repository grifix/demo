<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Producer\Protector;

use Grifix\Demo\Application\Projection\ProducerProjection\ProducerDto;
use Grifix\Demo\Ui\Common\DemoModulePermissions;
use Grifix\Shared\Ui\Common\PropertyProtector\PropertyProtectorInterface;
use Grifix\Shared\Ui\Common\PropertyProtector\ProtectionDto;

class ProducerProtector implements ProducerProtectorInterface
{
    /** @var PropertyProtectorInterface */
    protected $propertyProtector;

    public function __construct(PropertyProtectorInterface $propertyProtector)
    {
        $this->propertyProtector = $propertyProtector;
    }

    public function protectProducer(ProducerDto $producerDto)
    {
        $this->propertyProtector->protectProperties($producerDto, [
            new ProtectionDto('name', DemoModulePermissions::READ_PRODUCER_NAME),
            new ProtectionDto('title', DemoModulePermissions::READ_PRODUCER_TITLE)
        ]);
    }
}
