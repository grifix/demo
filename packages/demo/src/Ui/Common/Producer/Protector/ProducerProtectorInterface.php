<?php declare(strict_types = 1);

namespace Grifix\Demo\Ui\Common\Producer\Protector;

use Grifix\Demo\Application\Projection\ProducerProjection\ProducerDto;

interface ProducerProtectorInterface
{
    public function protectProducer(ProducerDto $producerDto);
}
