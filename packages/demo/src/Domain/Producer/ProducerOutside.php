<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Domain\Producer;

use Grifix\Demo\Domain\Producer\Name\NameFactoryInterface;
use Grifix\Demo\Domain\Producer\Name\NameInterface;
use Grifix\Demo\Domain\Producer\Rating\RatingFactoryInterface;
use Grifix\Demo\Domain\Producer\Rating\RatingInterface;
use Grifix\Demo\Domain\Producer\Title\TitleFactoryInterface;
use Grifix\Demo\Domain\Producer\Title\TitleInterface;
use Grifix\Shared\Domain\DateTime\DateTimeFactoryInterface;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class ProducerOutside implements ProducerOutsideInterface
{
    public function __construct(
        protected RatingFactoryInterface $ratingFactory,
        protected NameFactoryInterface $nameFactory,
        protected TitleFactoryInterface $titleFactory,
        protected DateTimeFactoryInterface $dateTimeFactory,
        protected UuidFactoryInterface $uuidFactory,
        protected ProducerInfrastructureInterface $infrastructure
    )
    {
    }

    public function createRating(int $rating): RatingInterface
    {
        return $this->ratingFactory->createRating($rating);
    }

    public function createName(string $name, string $producerId): NameInterface
    {
        return $this->nameFactory->createName($name, $producerId);
    }

    public function createTitle(array $title): TitleInterface
    {
        return $this->titleFactory->createTitle($title);
    }

    public function createDateTime(?int $timestamp = null): DateTimeInterface
    {
        return $this->dateTimeFactory->createDateTime($timestamp);
    }

    public function createUuid(string $uuid): UuidInterface
    {
        return $this->uuidFactory->createUuid($uuid);
    }

    public function publishEvent(object $event, object $aggregate): void
    {
        $this->infrastructure->publishEvent($event, $aggregate);
    }

    public function getCurrentTime(): int
    {
        return $this->infrastructure->getCurrentTime();
    }
}
