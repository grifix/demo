<?php
declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer;

interface ProducerFactoryInterface
{
    public function create(
        string $id,
        string $name,
        array $title,
        int $rating,
        ?int $createdAt = null
    );
}
