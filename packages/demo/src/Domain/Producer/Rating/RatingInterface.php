<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Rating;

interface RatingInterface
{
    public function withValue(int $value): RatingInterface;
}
