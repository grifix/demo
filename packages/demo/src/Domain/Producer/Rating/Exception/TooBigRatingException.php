<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Rating\Exception;

use DomainException;

class TooBigRatingException extends DomainException
{

}
