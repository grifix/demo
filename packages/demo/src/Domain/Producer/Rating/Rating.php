<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Rating;

use Grifix\Demo\Domain\Producer\Rating\Exception\TooLowRatingException;
use Grifix\Demo\Domain\Producer\Rating\Exception\TooBigRatingException;

class Rating implements RatingInterface
{
    public const MIN_VALUE = 0;
    public const MAX_VALUE = 5;

    /** @var int */
    protected $value;

    public function __construct(int $value)
    {
        if ($value < self::MIN_VALUE) {
            throw new TooLowRatingException();
        }

        if ($value > self::MAX_VALUE) {
            throw new TooBigRatingException();
        }

        $this->value = $value;
    }

    public function withValue(int $value): RatingInterface
    {
        return new self($value);
    }
}
