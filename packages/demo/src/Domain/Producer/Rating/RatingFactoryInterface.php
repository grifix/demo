<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Rating;

interface RatingFactoryInterface
{
    public function createRating(int $value): RatingInterface;
}
