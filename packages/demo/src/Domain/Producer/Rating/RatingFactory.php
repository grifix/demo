<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Rating;

use Grifix\Kit\Kernel\AbstractFactory;

class RatingFactory extends AbstractFactory implements RatingFactoryInterface
{
    public function createRating(int $value): RatingInterface
    {
        $class = $this->makeClassName(Rating::class);
        return new $class($value);
    }
}
