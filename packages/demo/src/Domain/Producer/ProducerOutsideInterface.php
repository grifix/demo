<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer;

use Grifix\Demo\Domain\Producer\Name\NameFactoryInterface;
use Grifix\Demo\Domain\Producer\Rating\RatingFactoryInterface;
use Grifix\Demo\Domain\Producer\Title\TitleFactoryInterface;
use Grifix\Shared\Domain\DateTime\DateTimeFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;

interface ProducerOutsideInterface extends
    RatingFactoryInterface,
    NameFactoryInterface,
    TitleFactoryInterface,
    UuidFactoryInterface,
    ProducerInfrastructureInterface,
    DateTimeFactoryInterface
{
}
