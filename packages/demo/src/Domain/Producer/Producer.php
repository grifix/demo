<?php

declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer;

use Grifix\Demo\Domain\Producer\Event\ProducerCreatedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerDeletedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerNameChangedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerRatingChangedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerTitleChangedEvent;
use Grifix\Demo\Domain\Producer\Name\NameInterface;
use Grifix\Demo\Domain\Producer\Rating\RatingInterface;
use Grifix\Demo\Domain\Producer\Title\TitleInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class Producer implements ProducerInterface
{
    protected UuidInterface $id;

    protected NameInterface $name;

    protected TitleInterface $title;

    protected DateTimeInterface $createdAt;

    protected RatingInterface $rating;

    public function __construct(
        #[Dependency]
        protected ProducerOutsideInterface $outside,
        string $id,
        string $name,
        array $title,
        int $rating,
        ?int $createdAt
    ) {
        $this->outside = $outside;
        $this->id = $outside->createUuid($id);
        $this->name = $outside->createName($name, $id);
        $this->title = $outside->createTitle($title);
        $this->rating = $outside->createRating($rating);
        $createdAt = $createdAt ?? $this->outside->getCurrentTime();
        $this->createdAt = $outside->createDateTime($createdAt);
        $this->outside->publishEvent(new ProducerCreatedEvent($id), $this);
    }

    public function changeName(string $name): void
    {
        $this->name = $this->name->withValue($name);
        $this->outside->publishEvent(new ProducerNameChangedEvent((string)$this->id, $name), $this);
    }

    public function changeTitle(array $title): void
    {
        $this->title = $this->title->withValues($title);
        $this->outside->publishEvent(new ProducerTitleChangedEvent((string)$this->id, $title), $this);
    }

    public function changeRating(int $rating): void
    {
        $this->rating = $this->rating->withValue($rating);
        $this->outside->publishEvent(new ProducerRatingChangedEvent((string)$this->id, $rating), $this);
    }

    public function delete()
    {
        $this->outside->publishEvent(new ProducerDeletedEvent((string)$this->id), $this);
    }
}
