<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Title;

use Grifix\Shared\Domain\Translation\TranslationFactory;

class TitleFactory extends TranslationFactory implements TitleFactoryInterface
{
    public function createTitle(array $value): TitleInterface
    {
        $class = $this->makeClassName(Title::class);
        return new $class($this->translationInfrastructure, $value);
    }
}
