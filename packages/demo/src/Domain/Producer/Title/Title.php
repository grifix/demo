<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Title;

use Grifix\Demo\Domain\Producer\Title\Exception\TooLongTitleException;
use Grifix\Demo\Domain\Producer\Title\Exception\TooShortTitleException;
use Grifix\Shared\Domain\Translation\Translation;
use Grifix\Shared\Domain\Translation\TranslationOutsideInterface;

class Title extends Translation implements TitleInterface
{
    /**
     * (@inheritDoc)
     */
    public function __construct(TranslationOutsideInterface $outside, array $values)
    {
        foreach ($values as $value) {
            $this->validateValue($value);
        }
        parent::__construct($outside, $values);
    }

    protected function validateValue(string $value)
    {
        if (mb_strlen($value) < self::MIN_LEN) {
            throw new TooShortTitleException();
        }

        if (mb_strlen($value) > self::MAX_LEN) {
            throw new TooLongTitleException();
        }
    }


    /**
     * @return self
     */
    public function withValues(array $values)
    {
        return new self($this->outside, $values);
    }
}
