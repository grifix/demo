<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Title\Exception;

use DomainException;

class TooShortTitleException extends DomainException
{
}
