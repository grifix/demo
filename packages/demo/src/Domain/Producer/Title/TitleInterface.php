<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Title;

interface TitleInterface
{
    public const MIN_LEN = 3;
    public const MAX_LEN = 55;

    /**
     * @return self
     */
    public function withValues(array $values);
}
