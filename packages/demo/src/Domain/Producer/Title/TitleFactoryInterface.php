<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Title;

interface TitleFactoryInterface
{
    public function createTitle(array $value): TitleInterface;
}
