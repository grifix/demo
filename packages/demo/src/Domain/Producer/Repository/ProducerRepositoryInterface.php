<?php
declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Repository;

use Grifix\Demo\Domain\Producer\ProducerInterface;
use Grifix\Demo\Domain\Producer\Repository\Exception\ProducerNotFoundException;
use Grifix\Demo\Infrastructure\Domain\Producer\ProducerRepository;

interface ProducerRepositoryInterface
{
    /**
     * @param string $id
     *
     * @return ProducerInterface
     *
     * @throws ProducerNotFoundException
     */
    public function getById(string $id): ProducerInterface;

    public function add(ProducerInterface $producer): void;

    public function delete(ProducerInterface $producer): void;
}
