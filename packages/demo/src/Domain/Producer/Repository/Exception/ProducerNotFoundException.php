<?php

declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Repository\Exception;

use DomainException;

class ProducerNotFoundException extends DomainException
{

    /**
     * @var string
     */
    protected $producerId;

    public function __construct($itemId)
    {
        $this->producerId = $itemId;
        parent::__construct(sprintf('Producer with id %s not found', $itemId));
    }
}
