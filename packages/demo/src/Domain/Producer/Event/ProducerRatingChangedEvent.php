<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Domain\Producer\Event;

final class ProducerRatingChangedEvent
{
    private string $producerId;

    private int $newRating;

    public function __construct(string $producerId, int $newRating)
    {
        $this->producerId = $producerId;
        $this->newRating = $newRating;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }

    public function getNewRating(): int
    {
        return $this->newRating;
    }
}
