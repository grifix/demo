<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Domain\Producer\Event;

final class ProducerNameChangedEvent
{
    private string $producerId;

    private string $newName;

    public function __construct(string $producerId, string $newName)
    {
        $this->producerId = $producerId;
        $this->newName = $newName;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }

    public function getNewName(): string
    {
        return $this->newName;
    }
}
