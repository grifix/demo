<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Domain\Producer\Event;

final class ProducerTitleChangedEvent
{
    private string $producerId;

    private array $newTitle;

    public function __construct(string $producerId, array $newTitle)
    {
        $this->producerId = $producerId;
        $this->newTitle = $newTitle;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }

    public function getNewTitle(): array
    {
        return $this->newTitle;
    }
}
