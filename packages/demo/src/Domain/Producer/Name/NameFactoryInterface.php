<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Name;

interface NameFactoryInterface
{
    public function createName(string $value, string $producerId): NameInterface;
}
