<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Name;

use Grifix\Demo\Domain\Producer\Name\Exception\NameIsNotUniqueException;
use Grifix\Demo\Domain\Producer\Name\Exception\TooLongNameException;
use Grifix\Demo\Domain\Producer\Name\Exception\TooShortNameException;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;

class Name implements NameInterface
{
    public function __construct(
        #[Dependency]
        protected NameOutsideInterface $infrastructure,
        protected string $value,
        protected string $producerId
    ) {
        $this->infrastructure = $infrastructure;
        $this->producerId = $producerId;

        if (mb_strlen($value) < self::MIN_LEN) {
            throw new TooShortNameException();
        }
        if (mb_strlen($value) > self::MAX_LEN) {
            throw new TooLongNameException();
        }

        if (!$this->infrastructure->isUnique($value, $producerId)) {
            throw new NameIsNotUniqueException();
        }

        $this->value = $value;
    }

    public function withValue(string $value): NameInterface
    {
        return new self($this->infrastructure, $value, $this->producerId);
    }
}
