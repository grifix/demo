<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Name\Exception;

use DomainException;

class TooShortNameException extends DomainException
{
}
