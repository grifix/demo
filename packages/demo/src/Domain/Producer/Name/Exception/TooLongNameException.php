<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Name\Exception;

use DomainException;

class TooLongNameException extends DomainException
{
    public function getTranslationKey(): string
    {
        return 'grifix.demo.tooLongName';
    }
}
