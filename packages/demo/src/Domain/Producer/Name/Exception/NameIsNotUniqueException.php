<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer\Name\Exception;

use DomainException;

class NameIsNotUniqueException extends DomainException
{
    public function __construct()
    {
        parent::__construct('Producer name is not unique!');
    }
}
