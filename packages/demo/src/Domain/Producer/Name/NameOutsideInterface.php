<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Name;

interface NameOutsideInterface
{
    public function isUnique(string $name, string $producerId): bool;
}
