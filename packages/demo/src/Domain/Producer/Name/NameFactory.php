<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Name;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

class NameFactory extends AbstractFactory implements NameFactoryInterface
{
    public function __construct(ClassMakerInterface $classMaker, protected NameOutsideInterface $outside)
    {
        parent::__construct($classMaker);
    }

    public function createName(string $value, string $producerId): NameInterface
    {
        $class = $this->makeClassName(Name::class);

        return new $class($this->outside, $value, $producerId);
    }
}
