<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Producer\Name;

interface NameInterface
{
    public const MAX_LEN = 55;
    public const MIN_LEN = 3;
}
