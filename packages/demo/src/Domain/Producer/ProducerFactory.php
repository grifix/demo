<?php

declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer;

class ProducerFactory implements ProducerFactoryInterface
{
    public function __construct(
        protected ProducerOutsideInterface $producerOutside,
    )
    {
    }


    public function create(
        string $id,
        string $name,
        array $title,
        int $rating = 0,
        ?int $createdAt = null
    )
    {
        return new Producer(
            $this->producerOutside,
            $id,
            $name,
            $title,
            $rating,
            $createdAt
        );
    }
}
