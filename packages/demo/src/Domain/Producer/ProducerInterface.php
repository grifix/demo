<?php
declare(strict_types=1);

namespace Grifix\Demo\Domain\Producer;

interface ProducerInterface
{
    public function changeTitle(array $title): void;

    public function changeRating(int $rating): void;

    public function changeName(string $name): void;

    public function delete();
}
