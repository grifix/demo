<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Domain\Producer;

use Grifix\Shared\Domain\EventPublisherInterface;

interface ProducerInfrastructureInterface extends EventPublisherInterface
{

    public function getCurrentTime(): int;
}
