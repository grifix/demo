<?php
/** @noinspection PhpFullyQualifiedNameUsageInspection */

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
declare(strict_types=1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Demo\Domain\Item\Event\FileChangedEvent;
use Grifix\Demo\Domain\Item\Event\FileDeletedEvent;
use Grifix\Demo\Domain\Item\Event\ItemCreatedEvent;
use Grifix\Demo\Domain\Item\Event\ItemDeletedEvent;
use Grifix\Demo\Domain\Item\Event\PhotoDeletedEvent;
use Grifix\Demo\Domain\Item\Exception\ThereIsNoFileException;
use Grifix\Demo\Domain\Item\Photo\ItemPhotoCollectionInterface;
use Grifix\Demo\Domain\Item\Status\ItemStatusInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Relation;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\Email\EmailInterface;
use Grifix\Shared\Domain\File\FileDto;
use Grifix\Shared\Domain\File\FileInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;
use Grifix\Shared\Domain\Translation\TranslationInterface;
use Grifix\Shared\Domain\Url\UrlInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class Item implements ItemInterface
{
    protected UuidInterface $id;

    protected UuidInterface $producerId;

    protected string $name;

    protected TranslationInterface $title;

    protected ItemStatusInterface $status;

    protected DateTimeInterface $date;

    protected EmailInterface $email;

    protected ?FileInterface $file;

    protected ?FileInterface $image;

    protected bool $public;

    protected ?UrlInterface $url;

    protected ?IpAddressInterface $ip;

    protected ?TranslationInterface $description;

    protected PositiveIntInterface $width;

    protected PositiveIntInterface $height;

    #[Relation]
    protected ItemPhotoCollectionInterface $photos;

    public function __construct(
        #[Dependency]
        protected ItemOutsideInterface $outside,
        string $id,
        string $producerId,
        string $name,
        array $title,
        int $date,
        string $email,
        string $status,
        int $width,
        int $height,
        ?FileDto $file = null,
        ?FileDto $image = null,
        bool $public = true,
        ?string $url = null,
        ?string $ip = null,
        ?array $description = null
    )
    {
        $this->outside = $outside;
        $this->id = $this->outside->createUuid($id);
        $this->producerId = $this->outside->createUuid($producerId);
        $this->width = $this->outside->createPositiveInt($width);
        $this->height = $this->outside->createPositiveInt($height);
        $this->name = $name;
        $this->title = $this->outside->createTranslation($title);
        $this->status = $this->outside->createItemStatus($status);
        $this->date = $this->outside->createDateTime($date);
        $this->email = $this->outside->createEmail($email);
        if ($file) {
            $this->file = $this->outside->createFile(
                $file->getSourcePath(),
                $file->getName(),
                $this->outside->defineFileDirectory(),
                $this->outside->getFileRestrictions()
            );
        }
        if ($image) {
            $this->image = $this->outside->createFile(
                $image->getSourcePath(),
                $image->getName(),
                $this->outside->defineImageDirectory(),
                $this->outside->getImageRestrictions()
            );
        }
        $this->public = $public;

        if (null !== $url) {
            $this->url = $this->outside->createUrl($url);
        }

        if (null !== $ip) {
            $this->ip = $this->outside->createIpAddress($ip);
        }

        if (null !== $description) {
            $this->description = $this->outside->createTranslation($description);
        }

        $this->photos = $this->outside->createPhotosCollection();
        $this->outside->publishEvent(new ItemCreatedEvent((string)$this->id), $this);
    }

    public function changeFile(string $sourcePath, string $name): void
    {
        $event = new FileChangedEvent((string)$this->id, $this->file->getPath());
        $this->file = $this->file->change($sourcePath, $name);
        $this->outside->publishEvent($event, $this);
    }

    public function deleteFile(): void
    {
        if (null === $this->file) {
            throw new ThereIsNoFileException();
        }
        $event = new FileDeletedEvent((string)$this->id, $this->file->getPath());
        $this->file = null;
        $this->outside->publishEvent($event, $this);
    }

    public function delete(): void
    {
        $this->outside->publishEvent(
            new ItemDeletedEvent(
                (string)$this->id,
                $this->file ? $this->file->getPath() : null,
                $this->image ? $this->image->getPath() : null
            ),
            $this
        );
    }

    public function addPhoto(
        string $sourcePath,
        string $id,
        string $fileName,
        array $description = [],
        ?int $sequence = null
    ): void
    {
        $sequence = $sequence ?? $this->photos->count() + 1;
        $this->photos->add(
            $this->outside->createPhoto(
                (string)$this->id,
                $id,
                $sourcePath,
                $fileName,
                $sequence,
                $description,
            )
        );
    }

    public function deletePhoto(string $photoId): void
    {
        $photo = $this->photos->getById($photoId);
        $event = new PhotoDeletedEvent((string)$this->id, $photo->getFilePath());
        $this->photos->remove($photo);
        $this->outside->publishEvent($event, $this);
    }

    public function changeProducer(string $producerId): void
    {
        $this->producerId = $this->outside->createUuid($producerId);
    }
}
