<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Demo\Domain\Item\Photo\ItemPhotoCollectionInterface;
use Grifix\Demo\Domain\Item\Photo\ItemPhotoFactoryInterface;
use Grifix\Demo\Domain\Item\Photo\ItemPhotoInterface;
use Grifix\Demo\Domain\Item\Status\ItemStatusFactoryInterface;
use Grifix\Demo\Domain\Item\Status\ItemStatusInterface;
use Grifix\Shared\Domain\DateTime\DateTimeFactoryInterface;
use Grifix\Shared\Domain\DateTime\DateTimeInterface;
use Grifix\Shared\Domain\Email\EmailFactoryInterface;
use Grifix\Shared\Domain\Email\EmailInterface;
use Grifix\Shared\Domain\File\FileFactoryInterface;
use Grifix\Shared\Domain\File\FileInterface;
use Grifix\Shared\Domain\File\FileRestrictionsDto;
use Grifix\Shared\Domain\IpAddress\IpAddressFactoryInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntFactoryInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;
use Grifix\Shared\Domain\Translation\TranslationFactoryInterface;
use Grifix\Shared\Domain\Translation\TranslationInterface;
use Grifix\Shared\Domain\Url\UrlFactoryInterface;
use Grifix\Shared\Domain\Url\UrlInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class ItemOutside implements ItemOutsideInterface
{
    public function __construct(
        protected FileFactoryInterface $fileFactory,
        protected ItemPhotoFactoryInterface $itemPhotoFactory,
        protected EmailFactoryInterface $emailFactory,
        protected IpAddressFactoryInterface $ipAddressFactory,
        protected UrlFactoryInterface $urlFactory,
        protected PositiveIntFactoryInterface $positiveIntFactory,
        protected UuidFactoryInterface $uuidFactory,
        protected TranslationFactoryInterface $translationFactory,
        protected ItemStatusFactoryInterface $itemStatusFactory,
        protected DateTimeFactoryInterface $dateTimeFactory,
        protected ItemInfrastructureInterface $infrastructure
    ) {
    }

    public function createPhoto(
        string $itemId,
        string $id,
        string $sourcePath,
        string $name,
        int $sequence,
        array $description = [],
    ): ItemPhotoInterface {
        return $this->itemPhotoFactory->createPhoto($itemId, $id, $sourcePath, $name, $sequence, $description);
    }

    public function createFile(
        string $sourcePath,
        string $name,
        string $directory,
        ?FileRestrictionsDto $restrictions = null
    ): FileInterface {
        return $this->fileFactory->createFile($sourcePath, $name, $directory, $restrictions);
    }

    public function createEmail(string $email): EmailInterface
    {
        return $this->emailFactory->createEmail($email);
    }

    public function createPositiveInt(int $value): PositiveIntInterface
    {
        return $this->positiveIntFactory->createPositiveInt($value);
    }

    public function createUrl(string $value): UrlInterface
    {
        return $this->urlFactory->createUrl($value);
    }

    public function createUuid(string $value): UuidInterface
    {
        return $this->uuidFactory->createUuid($value);
    }

    public function createIpAddress(string $value): IpAddressInterface
    {
        return $this->ipAddressFactory->createIpAddress($value);
    }

    public function createTranslation(array $values): TranslationInterface
    {
        return $this->translationFactory->createTranslation($values);
    }

    public function createItemStatus(string $status): ItemStatusInterface
    {
        return $this->itemStatusFactory->createItemStatus($status);
    }

    public function createDateTime(?int $timestamp = null): DateTimeInterface
    {
        return $this->dateTimeFactory->createDateTime($timestamp);
    }

    public function publishEvent(object $event, object $aggregate): void
    {
        $this->infrastructure->publishEvent($event, $aggregate);
    }

    public function createPhotosCollection(): ItemPhotoCollectionInterface
    {
        return $this->infrastructure->createPhotosCollection();
    }

    public function defineFileDirectory(): string
    {
        return $this->infrastructure->defineFileDirectory();
    }

    public function getFileRestrictions(): FileRestrictionsDto
    {
        return $this->infrastructure->getFileRestrictions();
    }

    public function getImageRestrictions(): FileRestrictionsDto
    {
        return $this->infrastructure->getImageRestrictions();
    }

    public function defineImageDirectory(): string
    {
        return $this->infrastructure->defineImageDirectory();
    }
}
