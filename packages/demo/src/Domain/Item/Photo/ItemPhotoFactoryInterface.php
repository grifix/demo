<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Photo;

interface ItemPhotoFactoryInterface
{
    /**
     * @param string[] $description
     */
    public function createPhoto(
        string $itemId,
        string $id,
        string $sourcePath,
        string $name,
        int $sequence,
        array $description = []
    ): ItemPhotoInterface;
}
