<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo\Exception;

use DomainException;

class ItemPhotoNotExistException extends DomainException
{
    /** @var string */
    protected $photoId;

    public function __construct(string $photoId)
    {
        $this->photoId = $photoId;
        parent::__construct(sprintf('Item photo with id "%s" does not exist', $photoId));
    }
}
