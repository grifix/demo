<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo;


use Grifix\Shared\Domain\File\FileDto;

class ItemPhotoFactory implements ItemPhotoFactoryInterface
{

    public function __construct(
        protected ItemPhotoOutsideInterface $itemPhotoOutside,
    ) {
    }

    public function createPhoto(
        string $itemId,
        string $id,
        string $sourcePath,
        string $name,
        int $sequence,
        array $description = []
    ): ItemPhotoInterface {
        return new ItemPhoto(
            $this->itemPhotoOutside,
            $id,
            new FileDto($sourcePath, basename($sourcePath)),
            $sequence,
            $description
        );
    }
}
