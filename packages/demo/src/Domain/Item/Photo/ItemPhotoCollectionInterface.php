<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Photo\Exception\ItemPhotoNotExistException;
use Grifix\Kit\Collection\GenericCollectionInterface;

interface ItemPhotoCollectionInterface extends GenericCollectionInterface
{
    public function add(ItemPhotoInterface $photo): void;

    /**
     * @throws ItemPhotoNotExistException
     */
    public function getById(string $id): ItemPhotoInterface;

    public function remove(ItemPhotoInterface $photo): void;
}
