<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Photo\Description\PhotoDescriptionFactoryInterface;
use Grifix\Demo\Domain\Item\Photo\Description\PhotoDescriptionInterface;
use Grifix\Shared\Domain\File\FileFactoryInterface;
use Grifix\Shared\Domain\File\FileInterface;
use Grifix\Shared\Domain\File\FileRestrictionsDto;
use Grifix\Shared\Domain\PositiveInt\PositiveIntFactoryInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

final class ItemPhotoOutside implements ItemPhotoOutsideInterface
{

    public function __construct(
        protected UuidFactoryInterface $uuidFactory,
        protected FileFactoryInterface $fileFactory,
        protected PositiveIntFactoryInterface $positiveIntFactory,
        protected PhotoDescriptionFactoryInterface $photoDescriptionFactory,
        protected ItemPhotoInfrastructureInterface $infrastructure
    )
    {
    }

    public function createFile(
        string $sourcePath,
        string $name,
        string $directory,
        ?FileRestrictionsDto $restrictions = null
    ): FileInterface
    {
        return $this->fileFactory->createFile($sourcePath, $name, $directory, $restrictions);
    }

    public function createPhotoDescription(array $values): PhotoDescriptionInterface
    {
        return $this->photoDescriptionFactory->createPhotoDescription($values);
    }

    public function createPositiveInt(int $value): PositiveIntInterface
    {
        return $this->positiveIntFactory->createPositiveInt($value);
    }

    public function createUuid(string $value): UuidInterface
    {
        return $this->uuidFactory->createUuid($value);
    }

    public function defineFileDirectory(): string
    {
        return $this->infrastructure->defineFileDirectory();
    }

    public function getFileRestrictions(): FileRestrictionsDto
    {
        return $this->infrastructure->getFileRestrictions();
    }
}
