<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Photo\Description;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Shared\Domain\Translation\TranslationOutsideInterface;

class PhotoDescriptionFactory extends AbstractFactory implements PhotoDescriptionFactoryInterface
{
    /** @var TranslationOutsideInterface */
    protected $translationInfrastructure;

    public function __construct(
        ClassMakerInterface $classMaker,
        TranslationOutsideInterface $translationInfrastructure
    ) {
        parent::__construct($classMaker);
        $this->translationInfrastructure = $translationInfrastructure;
    }

    /**
     * @var string[] $values
     */
    public function createPhotoDescription(array $values): PhotoDescriptionInterface
    {
        $class = $this->makeClassName(PhotoDescription::class);
        return new $class($this->translationInfrastructure, $values);
    }
}
