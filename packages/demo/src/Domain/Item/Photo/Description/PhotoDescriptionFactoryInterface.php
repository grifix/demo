<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Photo\Description;

interface PhotoDescriptionFactoryInterface
{
    /**
     * @var string[] $values
     */
    public function createPhotoDescription(array $values): PhotoDescriptionInterface;
}
