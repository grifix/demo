<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Photo\Description;

use Grifix\Demo\Domain\Item\Photo\Description\Exception\TooLongDescriptionException;
use Grifix\Demo\Domain\Item\Photo\Description\Exception\TooShortDescriptionException;
use Grifix\Shared\Domain\Translation\Translation;
use Grifix\Shared\Domain\Translation\TranslationOutsideInterface;

class PhotoDescription extends Translation implements PhotoDescriptionInterface
{
    public const MAX_CHARS = 255;
    public const MIN_CHARS = 5;

    public function __construct(TranslationOutsideInterface $outside, array $values)
    {
        parent::__construct($outside, $values);
        $this->assertMinChars($values, self::MIN_CHARS, function (string $langCode) {
            throw new TooShortDescriptionException(self::MIN_CHARS, $langCode);
        });

        $this->assertMaxChars($values, self::MAX_CHARS, function (string $langCode) {
            throw new TooLongDescriptionException(self::MAX_CHARS, $langCode);
        });
    }
}
