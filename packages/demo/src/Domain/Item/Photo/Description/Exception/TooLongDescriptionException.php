<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo\Description\Exception;

use DomainException;

class TooLongDescriptionException extends DomainException
{
    /** @var int */
    protected $maxChars;

    /** @var string */
    protected $langCode;

    public function __construct(int $maxChars, string $langCode)
    {
        $this->maxChars = $maxChars;
        $this->langCode = $langCode;
        parent::__construct('Too short description!');
    }

    public function getMaxChars(): int
    {
        return $this->maxChars;
    }

    public function getLangCode(): string
    {
        return $this->langCode;
    }


}
