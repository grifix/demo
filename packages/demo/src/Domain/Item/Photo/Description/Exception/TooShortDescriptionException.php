<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo\Description\Exception;

use DomainException;

class TooShortDescriptionException extends DomainException
{
    /** @var int */
    protected $minChars;

    /** @var string */
    protected $langCode;

    public function __construct(int $minChars, string $langCode)
    {
        $this->minChars = $minChars;
        $this->langCode = $langCode;
        parent::__construct('Too short description!');
    }

    /**
     * @return int
     */
    public function getMinChars(): int
    {
        return $this->minChars;
    }

    /**
     * @return string
     */
    public function getLangCode(): string
    {
        return $this->langCode;
    }
}
