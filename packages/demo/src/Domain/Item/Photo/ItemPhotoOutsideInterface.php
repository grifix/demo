<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Photo\Description\PhotoDescriptionFactoryInterface;
use Grifix\Shared\Domain\File\FileFactoryInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;

interface ItemPhotoOutsideInterface extends
    FileFactoryInterface,
    UuidFactoryInterface,
    PhotoDescriptionFactoryInterface,
    PositiveIntFactoryInterface,
    ItemPhotoInfrastructureInterface

{

}
