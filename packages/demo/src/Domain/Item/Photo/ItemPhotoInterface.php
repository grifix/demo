<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Photo;

use Grifix\Shared\Domain\File\FileInterface;

interface ItemPhotoInterface
{
    public function getFilePath(): string;
}
