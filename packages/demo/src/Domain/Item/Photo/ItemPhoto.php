<?php
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Photo;

use Grifix\Demo\Domain\Item\Photo\Description\PhotoDescriptionInterface;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\File\FileDto;
use Grifix\Shared\Domain\File\FileInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class ItemPhoto implements ItemPhotoInterface
{
    protected UuidInterface $id;

    protected FileInterface $file;

    protected PhotoDescriptionInterface $description;

    protected PositiveIntInterface $sequence;

    public function __construct(
        #[Dependency]
        protected ItemPhotoOutsideInterface $outside,
        string $id,
        FileDto $file,
        int $sequence,
        array $description
    ) {
        $this->outside = $outside;
        $this->id = $this->outside->createUuid($id);
        $this->file = $this->outside->createFile(
            $file->getSourcePath(),
            $file->getName(),
            $this->outside->defineFileDirectory(),
            $this->outside->getFileRestrictions()
        );
        $this->description = $this->outside->createPhotoDescription($description);
        $this->sequence = $this->outside->createPositiveInt($sequence);
    }

    public function getFilePath(): string
    {
        return $this->file->getPath();
    }
}
