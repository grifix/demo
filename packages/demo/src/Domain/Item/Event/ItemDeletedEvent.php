<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Event;

class ItemDeletedEvent
{
    protected string $itemId;

    protected ?string $itemFilePath;

    protected ?string $itemImagePath;

    public function __construct(string $itemId, ?string $itemFilePath, ?string $itemImagePath)
    {
        $this->itemId = $itemId;
        $this->itemFilePath = $itemFilePath;
        $this->itemImagePath = $itemImagePath;
    }

    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    public function getItemFilePath(): ?string
    {
        return $this->itemFilePath;
    }

    public function getItemImagePath(): string
    {
        return $this->itemImagePath;
    }
}
