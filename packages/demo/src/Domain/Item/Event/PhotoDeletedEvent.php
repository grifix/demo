<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Event;

class PhotoDeletedEvent
{
    protected string $photoImagePath;

    protected string $itemId;

    public function __construct(string $itemId, string $photoImagePath)
    {
        $this->photoImagePath = $photoImagePath;
        $this->itemId = $itemId;
    }

    public function getPhotoImagePath(): string
    {
        return $this->photoImagePath;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }
}
