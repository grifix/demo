<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Event;

class FileChangedEvent
{
    protected string $oldFilePath;

    protected string $itemId;

    public function __construct(string $itemId, string $oldFilePath)
    {
        $this->oldFilePath = $oldFilePath;
        $this->itemId = $itemId;
    }

    public function getOldFilePath(): string
    {
        return $this->oldFilePath;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }
}
