<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Event;

class FileDeletedEvent
{
    protected string $filePath;

    protected string $itemId;

    public function __construct(string $itemId, string $filePath)
    {
        $this->filePath = $filePath;
        $this->itemId = $itemId;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }
}
