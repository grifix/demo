<?php
declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Demo\Domain\Item\Status\ItemStatus;
use Grifix\Shared\Domain\File\FileDto;

class ItemFactory implements ItemFactoryInterface
{
    public function __construct(
        protected ItemOutsideInterface $itemOutside,
    ) {
    }

    public function create(
        string $id,
        string $producerId,
        string $name,
        array $title,
        int $date,
        string $email,
        int $width,
        int $height,
        ?FileDto $file = null,
        ?FileDto $image = null,
        bool $public = false,
        ?string $url = null,
        ?string $ip = null,
        ?array $description = null
    ): ItemInterface {
        return new Item(
            $this->itemOutside,
            $id,
            $producerId,
            $name,
            $title,
            $date,
            $email,
            ItemStatus::STATUS_NEW,
            $width,
            $height,
            $file,
            $image,
            $public,
            $url,
            $ip,
            $description
        );
    }
}
