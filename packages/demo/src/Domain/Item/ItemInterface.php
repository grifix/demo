<?php
declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Demo\Domain\Item\Event\ItemDeletedEvent;
use Grifix\Demo\Domain\Item\Event\PhotoDeletedEvent;
use Grifix\Demo\Domain\Item\Exception\ThereIsNoFileException;

interface ItemInterface
{
    public function changeFile(string $sourcePath, string $name): void;

    /**
     * @throws ThereIsNoFileException
     */
    public function deleteFile(): void;

    public function delete(): void;

    public function addPhoto(
        string $sourcePath,
        string $id,
        string $fileName,
        array $description,
        ?int $sequence = null
    ): void;

    public function deletePhoto(string $photoId): void;

    public function changeProducer(string $producerId): void;
}
