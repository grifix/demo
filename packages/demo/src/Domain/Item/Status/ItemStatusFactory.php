<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Status;

use Grifix\Kit\Kernel\AbstractFactory;

class ItemStatusFactory extends AbstractFactory implements ItemStatusFactoryInterface
{
    public function createItemStatus(string $status): ItemStatusInterface
    {
        $class = $this->makeClassName(ItemStatus::class);
        return new $class($status);
    }
}
