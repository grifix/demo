<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Status;

use Grifix\Demo\Domain\Item\Status\Exception\InvalidStatusException;

class ItemStatus implements ItemStatusInterface
{
    public const STATUS_NEW = 'new';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_CLOSED = 'closed';

    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        if (!in_array($value, [self::STATUS_NEW, self::STATUS_CONFIRMED, self::STATUS_CLOSED])) {
            return new InvalidStatusException($value);
        }

        $this->value = $value;
    }

    public function change(string $status): ItemStatusInterface
    {
        return new self($status);
    }
}
