<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Status;

interface ItemStatusFactoryInterface
{
    public function createItemStatus(string $status): ItemStatusInterface;
}
