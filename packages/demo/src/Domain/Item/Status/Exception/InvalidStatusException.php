<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Status\Exception;

use DomainException;

class InvalidStatusException extends DomainException
{
    private $status;

    public function __construct(string $status)
    {
        $this->status = $status;
        parent::__construct(sprintf('%s is invalid item status!', $status));
    }
}
