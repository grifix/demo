<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Status;

interface ItemStatusInterface
{
    public function change(string $status): ItemStatusInterface;
}
