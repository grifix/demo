<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Repository\Exception;

use DomainException;

class ItemNotFoundException extends DomainException
{
    /**
     * @var string
     */
    protected $itemId;

    public function __construct($itemId)
    {
        $this->itemId = $itemId;
        parent::__construct(sprintf('Item with id %s not found', $itemId));
    }

    /**
     * @return string
     */
    public function getItemId(): string
    {
        return $this->itemId;
    }
}
