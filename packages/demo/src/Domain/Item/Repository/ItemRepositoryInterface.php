<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item\Repository;

use Grifix\Demo\Domain\Item\ItemInterface;
use Grifix\Demo\Infrastructure\Domain\Item\ItemRepository;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Specification\SpecificationInterface;

interface ItemRepositoryInterface
{
    public function getById(string $id): ItemInterface;

    public function add(ItemInterface $item): void;

    public function delete(ItemInterface $item): void;

}
