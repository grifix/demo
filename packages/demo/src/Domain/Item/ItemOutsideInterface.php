<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Demo\Domain\Item\Photo\ItemPhotoFactoryInterface;
use Grifix\Demo\Domain\Item\Status\ItemStatusFactoryInterface;
use Grifix\Shared\Domain\DateTime\DateTimeFactoryInterface;
use Grifix\Shared\Domain\Email\EmailFactoryInterface;
use Grifix\Shared\Domain\File\FileFactoryInterface;
use Grifix\Shared\Domain\IpAddress\IpAddressFactoryInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntFactoryInterface;
use Grifix\Shared\Domain\Translation\TranslationFactoryInterface;
use Grifix\Shared\Domain\Url\UrlFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;

interface ItemOutsideInterface extends
    FileFactoryInterface,
    ItemPhotoFactoryInterface,
    UuidFactoryInterface,
    UrlFactoryInterface,
    IpAddressFactoryInterface,
    EmailFactoryInterface,
    PositiveIntFactoryInterface,
    TranslationFactoryInterface,
    ItemStatusFactoryInterface,
    DateTimeFactoryInterface,
    ItemInfrastructureInterface
{
}
