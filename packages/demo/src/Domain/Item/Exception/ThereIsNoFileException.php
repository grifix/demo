<?php declare(strict_types=1);

namespace Grifix\Demo\Domain\Item\Exception;

use DomainException;

class ThereIsNoFileException extends DomainException
{
    public function getTranslationKey(): string
    {
        return 'grifix.demo.msg_thereIsNoFile';
    }
}
