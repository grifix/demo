<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Demo\Domain\Item\Photo\ItemPhotoCollectionInterface;
use Grifix\Demo\Infrastructure\Domain\Item\ItemInfrastructure;
use Grifix\Demo\Infrastructure\Domain\Item\Photo\ItemPhotoCollection;
use Grifix\Shared\Domain\EventPublisherInterface;
use Grifix\Shared\Domain\File\FileRestrictionsDto;

interface ItemInfrastructureInterface extends EventPublisherInterface
{

    public function createPhotosCollection(): ItemPhotoCollectionInterface;

    public function defineFileDirectory():string;

    public function getFileRestrictions(): FileRestrictionsDto;

    public function getImageRestrictions(): FileRestrictionsDto;

    public function defineImageDirectory(): string;
}
