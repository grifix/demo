<?php declare(strict_types = 1);

namespace Grifix\Demo\Domain\Item;

use Grifix\Shared\Domain\File\FileDto;

interface ItemFactoryInterface
{

    /** @noinspection PhpTooManyParametersInspection */
    public function create(
        string $id,
        string $producerId,
        string $name,
        array $title,
        int $date,
        string $email,
        int $width,
        int $height,
        ?FileDto $file = null,
        ?FileDto $image = null,
        bool $public = false,
        ?string $url = null,
        ?string $ip = null,
        ?array $description = null
    ): ItemInterface;
}
