<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Application\Subscriber;

use Grifix\Demo\Domain\Producer\Event\ProducerCreatedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerDeletedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerNameChangedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerRatingChangedEvent;
use Grifix\Demo\Domain\Producer\Event\ProducerTitleChangedEvent;
use Grifix\Shared\Application\Subscriber\EventualSubscriber;
use Psr\Log\LoggerInterface;

class ProducerSubscriber extends EventualSubscriber
{
    protected string $rootDir;

    protected LoggerInterface $logger;

    /**
     * @param string $rootDir <root_dir>
     */
    public function __construct(string $subscriptionId, string $rootDir, LoggerInterface $logger)
    {
        parent::__construct($subscriptionId);
        $this->rootDir = $rootDir;
        $this->logger = $logger;
    }


    public static function getStaringEvents(): array
    {
        return [ProducerCreatedEvent::class];
    }

    public static function getFinishingEvents(): array
    {
        return [ProducerDeletedEvent::class];
    }

    public function onProducerCreated(ProducerCreatedEvent $event): void
    {
        $this->log(get_class($event));
    }

    public static function getIdFromProducerCreatedEvent(ProducerCreatedEvent $event): string
    {
        return $event->getProducerId();
    }

    public function onProducerDeleted(ProducerDeletedEvent $event): void
    {
        $this->log(get_class($event));
    }

    public static function getIdFromProducerDeletedEvent(ProducerDeletedEvent $event): string
    {
        return $event->getProducerId();
    }

    public function onProducerNameChanged(ProducerNameChangedEvent $event): void
    {
        $this->log(get_class($event));
    }

    public static function getIdFromProducerNameChangedEvent(ProducerNameChangedEvent $event): string
    {
        return $event->getProducerId();
    }

    public function onProducerRatingChanged(ProducerRatingChangedEvent $event): void
    {
        $this->log(get_class($event));
    }

    public static function getIdFromProducerRatingChangedEvent(ProducerRatingChangedEvent $event): string
    {
        return $event->getProducerId();
    }

    public function onProducerTitleChanged(ProducerTitleChangedEvent $event): void
    {
        $this->log(get_class($event));
    }

    public static function getIdFromProducerTitleChangedEvent(ProducerTitleChangedEvent $event): string
    {
        return $event->getProducerId();
    }

    protected function log(string $text): void
    {
        $this->logger->info($text.' '.$this->subscriptionId);
        file_put_contents(
            sprintf('%s/tmp/producer.log', $this->rootDir),
            sprintf("%s: %s %s \n", $this->subscriptionId, date('Y-m-d H:i:s'), $text),
            FILE_APPEND
        );
    }
}
