<?php

declare(strict_types=1);

namespace Grifix\Demo\Application\Subscriber;

use Grifix\Demo\Application\Event\TestEvent;
use Grifix\Shared\Application\Subscriber\PermanentSubscriber;

/**
 * Class TestEventListener
 * @package Grifix\Demo\Application\Listener
 */
class TestEventSubscriber extends PermanentSubscriber
{
    protected string $rootDir;

    /**
     * TestEventListener constructor.
     * @param string $rootDir <root_dir>
     */
    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * @param TestEvent $event
     */
    public function onTest(TestEvent $event)
    {
        file_put_contents(
            $this->rootDir . '/tmp/event.log',
            $event->getDate()->format('Y-m-d H:i:s') . "\t" . $event->getMessage() . "\n",
            FILE_APPEND
        );
    }
}
