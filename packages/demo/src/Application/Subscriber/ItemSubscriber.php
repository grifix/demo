<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);


namespace Grifix\Demo\Application\Subscriber;

use Grifix\Demo\Domain\Item\Event\FileChangedEvent;
use Grifix\Demo\Domain\Item\Event\FileDeletedEvent;
use Grifix\Demo\Domain\Item\Event\ItemDeletedEvent;
use Grifix\Demo\Domain\Item\Event\PhotoDeletedEvent;
use Grifix\Shared\Application\Subscriber\PermanentSubscriber;
use Grifix\Shared\Domain\File\Infrastructure\FileOutsideInterface;

final class ItemSubscriber extends PermanentSubscriber
{

    public function __construct(private FileOutsideInterface $fileInfrastructure)
    {
    }

    public function onDeleteFile(FileDeletedEvent $event): void
    {
        $this->fileInfrastructure->delete($event->getFilePath());
    }

    public function onItemDelete(ItemDeletedEvent $event)
    {
        if ($event->getItemFilePath()) {
            $this->fileInfrastructure->delete($event->getItemFilePath());
        }
        if ($event->getItemImagePath()) {
            $this->fileInfrastructure->delete($event->getItemImagePath());
        }
    }

    public function onPhotoDelete(PhotoDeletedEvent $event)
    {
        $this->fileInfrastructure->delete($event->getPhotoImagePath());
    }

    public function onFileChanged(FileChangedEvent $event)
    {
        $this->fileInfrastructure->delete($event->getOldFilePath());
    }
}
