<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ProducerProjection;

use Grifix\Demo\Application\Projection\ItemProjection\ItemDto;

class ProducerDto
{

    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string[] */
    protected $title;

    /** @var int */
    protected $rating;

    /** @var int */
    protected $createdAt;

    /** @var ItemDto[]|null */
    protected $items;

    /**
     * @param ItemDto[] $items
     */
    public function __construct(
        string $id,
        string $name,
        array $title,
        int $rating,
        int $createdAt,
        ?array $items
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->title = $title;
        $this->rating = $rating;
        $this->createdAt = $createdAt;
        $this->items = $items;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function getItems(): ?array
    {
        return $this->items;
    }
}
