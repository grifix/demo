<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ProducerProjection;

use Grifix\Kit\Collection\CollectionInterface;

interface ProducerProjectionInterface
{
    /**
     * @return CollectionInterface|ProducerDto[]
     */
    public function find(
        ?ProducerFilter $filter = null
    ): CollectionInterface;

    public function count(?ProducerFilter $filter = null): int;
}
