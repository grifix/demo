<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ProducerProjection;

use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Shared\Application\Common\Filter\AbstractFilter;

class ProducerFilter extends AbstractFilter
{
    /** @var ItemFilter|null */
    protected $itemFilter;

    public function getItemFilter(): ?ItemFilter
    {
        return $this->itemFilter;
    }

    public function setItemFilter(?ItemFilter $itemFilter): ProducerFilter
    {
        $this->itemFilter = $itemFilter;
        return $this;
    }
}
