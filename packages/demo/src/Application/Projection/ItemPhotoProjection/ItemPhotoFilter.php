<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Projection\ItemPhotoProjection;

use Grifix\Shared\Application\Common\Filter\AbstractFilter;

class ItemPhotoFilter extends AbstractFilter
{
    /** @var string[] */
    protected array $itemIds = [];

    public function getItemIds(): array
    {
        return $this->itemIds;
    }

    public function setItemIds(array $itemIds): ItemPhotoFilter
    {
        $this->itemIds = $itemIds;
        return $this;
    }
}
