<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ItemPhotoProjection;

use Grifix\Shared\Application\Query\Dto\FileDto;

class ItemPhotoDto
{
    /** @var string */
    protected $id;

    /** @var FileDto */
    protected $image;

    /** @var string */
    protected $itemId;

    /** @var string[] */
    protected $description;

    /** @var int */
    protected $sequence;

    public function __construct(string $id, string $itemId, FileDto $image, array $description, int $sequence)
    {
        $this->id = $id;
        $this->itemId = $itemId;
        $this->image = $image;
        $this->description = $description;
        $this->sequence = $sequence;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getImage(): FileDto
    {
        return $this->image;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }
}
