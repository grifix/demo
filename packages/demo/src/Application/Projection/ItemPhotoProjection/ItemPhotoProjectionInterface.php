<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ItemPhotoProjection;

use Grifix\Demo\Infrastructure\Application\Projection\ItemPhotoProjection\ItemPhotoProjection;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Kit\Collection\CollectionInterface;

interface ItemPhotoProjectionInterface
{
    /**
     * @return CollectionInterface|ItemPhotoDto[]
     */
    public function find(ItemPhotoFilter $filter): CollectionInterface;

    public function count(ItemPhotoFilter $filter): int;
}
