<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ItemProjection;

use Grifix\Kit\Collection\CollectionInterface;

interface ItemProjectionInterface
{
    /**
     * @return CollectionInterface|ItemDto[]
     */
    public function find(
        ItemFilter $filter
    ): CollectionInterface;

    public function count(ItemFilter $filter): int;
}
