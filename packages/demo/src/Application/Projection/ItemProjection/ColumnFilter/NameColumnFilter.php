<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ItemProjection\ColumnFilter;

use Grifix\Shared\Application\Common\Filter\ColumnFilter\StringColumnFilter;

class NameColumnFilter extends StringColumnFilter
{

}
