<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Projection\ItemProjection;

use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoDto;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerDto;
use Grifix\Shared\Application\Query\Dto\FileDto;
use function Webmozart\Assert\Tests\StaticAnalysis\null;

class ItemDto
{

    public function __construct(
        private string $id,
        private string $producerId,
        private string $name,
        private array $title,
        private string $status,
        private int $date,
        private string $email,
        private bool $public,
        private string $url,
        private string $ip,
        private array $description,
        private int $width,
        private int $height,
        private ?FileDto $file = null,
        private ?FileDto $image = null,
        private ?array $photos = null,
        private ?ProducerDto $producer = null
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTitle(): array
    {
        return $this->title;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getDate(): int
    {
        return $this->date;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFile(): ?FileDto
    {
        return $this->file;
    }

    public function getImage(): ?FileDto
    {
        return $this->image;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return ItemPhotoDto[]|null
     */
    public function getPhotos(): ?array
    {
        return $this->photos;
    }

    public function getProducer(): ?ProducerDto
    {
        return $this->producer;
    }
}
