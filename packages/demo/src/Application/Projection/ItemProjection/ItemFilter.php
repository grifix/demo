<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Projection\ItemProjection;

use Grifix\Shared\Application\Common\Filter\AbstractFilter;

class ItemFilter extends AbstractFilter
{
    /** @var bool */
    protected $withPhotos = false;

    /** @var string|null */
    protected $status;

    /** @var string|null */
    protected $producerId;

    /** @var string[]|null */
    protected $producerIds;

    /** @var bool */
    private $withProducer = false;

    public function setWithPhotos(?bool $value): self
    {
        $this->withPhotos = $value;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): ItemFilter
    {
        $this->status = $status;
        return $this;
    }

    public function getProducerId(): ?string
    {
        return $this->producerId;
    }

    public function setProducerId(?string $producerId): ItemFilter
    {
        $this->producerId = $producerId;
        return $this;
    }

    public function getWithPhotos(): ?bool
    {
        return $this->withPhotos;
    }

    public function getWithProducer(): ?bool
    {
        return $this->withProducer;
    }

    public function setWithProducer(?bool $value): ItemFilter
    {
        $this->withProducer = $value;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getProducerIds(): ?array
    {
        return $this->producerIds;
    }

    /**
     * @param string[]|null $producerIds
     */
    public function setProducerIds(?array $producerIds): ItemFilter
    {
        $this->producerIds = $producerIds;
        return $this;
    }
}
