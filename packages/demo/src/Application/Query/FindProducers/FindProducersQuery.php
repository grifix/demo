<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\FindProducers;

use Grifix\Demo\Application\Projection\ProducerProjection\ProducerFilter;

class FindProducersQuery
{
    /** @var ProducerFilter */
    protected $filter;

    /**
     * @var bool
     */
    protected $countTotal = false;

    public function __construct(ProducerFilter $filter, bool $countTotal = false)
    {
        $this->filter = $filter;
        $this->countTotal = $countTotal;
    }

    public function getFilter(): ProducerFilter
    {
        return $this->filter;
    }

    public function getCountTotal(): bool
    {
        return $this->countTotal;
    }
}
