<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\FindProducers;

use Grifix\Demo\Application\Projection\ProducerProjection\ProducerProjectionInterface;

class FindProducersQueryHandler
{
    /** @var ProducerProjectionInterface */
    protected $producerFinder;

    public function __construct(ProducerProjectionInterface $producerFinder)
    {
        $this->producerFinder = $producerFinder;
    }

    public function __invoke(FindProducersQuery $query): FindProducersQueryResult
    {
        $producers = $this->producerFinder->find($query->getFilter());
        $total = null;
        if ($query->getCountTotal()) {
            $total = $this->producerFinder->count($query->getFilter());
        }
        return new FindProducersQueryResult($producers->toArray(), $total);
    }
}
