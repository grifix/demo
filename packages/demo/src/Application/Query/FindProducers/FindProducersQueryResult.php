<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\FindProducers;

use Grifix\Demo\Application\Projection\ProducerProjection\ProducerDto;

class FindProducersQueryResult
{
    /** @var ProducerDto[] */
    protected $producers;

    /** @var int|null */
    protected $total;

    public function __construct(array $producers, ?int $total)
    {
        $this->producers = $producers;
        $this->total = $total;
    }

    /**
     * @return ProducerDto[]
     */
    public function getProducers(): array
    {
        return $this->producers;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }
}
