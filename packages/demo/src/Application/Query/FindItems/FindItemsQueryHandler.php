<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\FindItems;

use Grifix\Demo\Application\Projection\ItemProjection\ItemProjectionInterface;

class FindItemsQueryHandler
{

    public function __construct(private ItemProjectionInterface $itemProjection)
    {
    }

    public function __invoke(FindItemsQuery $query): FindItemsQueryResult
    {
        $items = $this->itemProjection->find($query->getFilter());

        $total = null;
        if ($query->getCountTotal()) {
            $total = $this->itemProjection->count($query->getFilter());
        }
        return new FindItemsQueryResult($items->toArray(), $total);
    }
}
