<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\FindItems;

use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;

class FindItemsQuery
{
    protected ItemFilter $filter;

    protected bool $countTotal;

    public function __construct(ItemFilter $filter, bool $countTotal = false)
    {
        $this->filter = $filter;
        $this->countTotal = $countTotal;
    }

    public function getFilter(): ItemFilter
    {
        return $this->filter;
    }

    public function getCountTotal(): bool
    {
        return $this->countTotal;
    }
}
