<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\FindItems;

use Grifix\Demo\Application\Projection\ItemProjection\ItemDto;

class FindItemsQueryResult
{
    /** @var ItemDto[] */
    protected $items;

    /** @var int|null */
    protected $total;

    public function __construct(array $items, ?int $total)
    {
        $this->items = $items;
        $this->total = $total;
    }

    /**
     * @return ItemDto[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }
}
