<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Application\Query\FindItemPhotos;

use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoProjectionInterface;

final class FindItemPhotosQueryHandler
{
    public function __construct(private ItemPhotoProjectionInterface $projection)
    {
    }

    public function __invoke(FindItemPhotosQuery $query)
    {
        return new FindItemPhotosQueryResult(
            $this->projection->find($query->getFilter())->toArray(),
            false !== $query->getCountTotal() ? $this->projection->count($query->getFilter()) : null
        );
    }
}
