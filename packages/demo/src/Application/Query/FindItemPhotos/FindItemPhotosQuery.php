<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Application\Query\FindItemPhotos;

use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoFilter;

final class FindItemPhotosQuery
{
    public function __construct(
        private ItemPhotoFilter $filter,
        private bool $countTotal = false
    ) {
    }

    public function getFilter(): ItemPhotoFilter
    {
        return $this->filter;
    }

    public function getCountTotal(): bool
    {
        return $this->countTotal;
    }
}
