<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Application\Query\FindItemPhotos;

use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoDto;

final class FindItemPhotosQueryResult
{

    /**
     * @param ItemPhotoDto[] $itemPhotos
     */
    public function __construct(
        private array $itemPhotos,
        private ?int $total
    ) {
    }

    /**
     * @return ItemPhotoDto[]
     */
    public function getItemPhotos(): array
    {
        return $this->itemPhotos;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }
}
