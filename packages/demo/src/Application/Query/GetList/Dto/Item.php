<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\GetList\Dto;

/**
 * Class Item
 *
 * @category Grifix
 * @package  Grifix\Demo\Application\Dto
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Item
{
    
    protected $name;
    
    protected $priority;
    /**
     * Item constructor.
     *
     * @param string $name
     * @param int    $size
     */
    public function __construct(string $name, int $size)
    {
        $this->name = $name;
        $this->priority = $size;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}
