<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\GetList\Dto;

use Grifix\Demo\Application\Query\GetList\Dto\Item;

/**
 * Class ItemsList
 *
 * @category Grifix
 * @package  Grifix\Demo\Application\Dto
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ItemsList
{
    protected $items = [];
    
    protected $numOfPages;
    
    /**
     * ItemsList constructor.
     *
     * @param Item[] $items
     * @param int   $numOfPages
     */
    public function __construct(array $items, int $numOfPages)
    {
        $this->items = $items;
        $this->numOfPages = $numOfPages;
    }
    
    /**
     * @return array|Item[]
     */
    public function getItems()
    {
        return $this->items;
    }
    
    /**
     * @return int
     */
    public function getNumOfPages(): int
    {
        return $this->numOfPages;
    }
    
}
