<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\GetList;

/**
 * Class GetListQuery
 *
 * @category Grifix
 * @package  Grifix\Demo\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetListQuery
{
    /**
     * @var string
     */
    protected $page;
    
    /**
     * GetListQuery constructor.
     *
     * @param int $page
     */
    public function __construct(int $page)
    {
        $this->page = $page;
    }
    
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
    
}
