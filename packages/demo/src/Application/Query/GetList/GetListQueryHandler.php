<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Demo\Application\Query\GetList;

use Grifix\Demo\Application\Query\GetList\Dto\Item;
use Grifix\Demo\Application\Query\GetList\Dto\ItemsList;

/**
 * Class GetListQueryHandler
 *
 * @category Grifix
 * @package  Grifix\Demo\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetListQueryHandler
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(GetListQuery $query)
    {
        $data = [
            1 => [
                new Item('Alpha', 1),
                new Item('Beta', 2),
                new Item('Gamma', 3),
            ],
            2 => [
                new Item('Kilo', 4),
                new Item('Papa', 5),
                new Item('Lima', 6),
            ],
            3 => [
                new Item('Tiger', 7),
                new Item('Jamaica', 8),
                new Item('Cuba', 9),
            ],
        ];
        
        return new ItemsList($data[$query->getPage()], count($data));
    }
}
