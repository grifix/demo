<?php
declare(strict_types=1);

namespace Grifix\Demo\Application\Event;

/**
 * Class TestEvent
 * @package Grifix\Demo\Application\Listener
 */
class TestEvent
{
    /**
     * @var string
     */
    protected $message;

    /**
     * @var \DateTimeInterface
     */
    protected $date;

    /**
     * TestEvent constructor.
     * @param string $message
     * @param \DateTimeInterface $date
     */
    public function __construct(string $message, \DateTimeInterface $date)
    {
        $this->message = $message;
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }


}
