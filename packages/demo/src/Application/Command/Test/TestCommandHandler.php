<?php
declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Test;

/**
 * Class TestCommandHandler
 * @package Grifix\Demo\Application\TestCommand
 */
class TestCommandHandler
{

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * TestConsumer constructor.
     * @param string $rootDir <root_dir>
     */
    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * @param TestCommand $command
     */
    public function __invoke(TestCommand $command)
    {
        file_put_contents(
            $this->rootDir . '/tmp/command.log',
            $command->getDate()->format('Y-m-d H:i:s') . "\t" .$command->getMessage()."\n",
            FILE_APPEND
        );
    }
}
