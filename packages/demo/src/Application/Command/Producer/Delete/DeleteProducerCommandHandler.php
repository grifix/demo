<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Producer\Delete;

use Grifix\Demo\Domain\Producer\Repository\ProducerRepositoryInterface;

class DeleteProducerCommandHandler
{
    protected ProducerRepositoryInterface $producerRepository;

    public function __construct(
        ProducerRepositoryInterface $producerRepository
    ) {
        $this->producerRepository = $producerRepository;
    }

    public function __invoke(DeleteProducerCommand $command): void
    {
        $producer = $this->producerRepository->getById($command->getProducerId());
        $producer->delete();
        $this->producerRepository->delete($producer);
    }
}
