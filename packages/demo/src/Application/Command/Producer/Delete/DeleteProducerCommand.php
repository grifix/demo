<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Producer\Delete;

class DeleteProducerCommand
{
    /** @var string */
    protected $producerId;

    public function __construct(string $producerId)
    {
        $this->producerId = $producerId;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }
}
