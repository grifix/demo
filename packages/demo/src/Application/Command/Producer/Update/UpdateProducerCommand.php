<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Producer\Update;

class UpdateProducerCommand
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string[] */
    private $title;

    public function __construct(string $id, string $name, array $title)
    {
        $this->id = $id;
        $this->name = $name;
        $this->title = $title;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTitle(): array
    {
        return $this->title;
    }
}
