<?php declare(strict_types=1);
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Application\Command\Producer\Update;

use Grifix\Demo\Domain\Producer\Repository\ProducerRepositoryInterface;

class UpdateProducerCommandHandler
{
    /** @var ProducerRepositoryInterface */
    protected $producerRepository;

    public function __construct(
        ProducerRepositoryInterface $producerRepository
    ) {
        $this->producerRepository = $producerRepository;
    }

    public function __invoke(UpdateProducerCommand $command): void
    {
        $producer = $this->producerRepository->getById($command->getId());
        $producer->changeName($command->getName());
        $producer->changeTitle($command->getTitle());
    }
}
