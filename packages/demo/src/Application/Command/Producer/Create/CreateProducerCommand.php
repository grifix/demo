<?php

declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Producer\Create;

class CreateProducerCommand
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string[]
     */
    protected $title;

    /**
     * @var int|null
     */
    protected $createdAt;

    /** @var int */
    protected $rating;

    public function __construct(
        string $id,
        string $name,
        array $title,
        int $rating = 0,
        ?int $createdAt = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->title = $title;
        $this->rating = $rating;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    public function getRating(): int
    {
        return $this->rating;
    }
}
