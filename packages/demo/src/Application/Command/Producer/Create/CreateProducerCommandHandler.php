<?php

declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Producer\Create;

use Grifix\Demo\Domain\Producer\ProducerFactoryInterface;
use Grifix\Demo\Domain\Producer\Repository\ProducerRepositoryInterface;

class CreateProducerCommandHandler
{
    /**
     * @var ProducerRepositoryInterface
     */
    protected $producerRepository;

    /**
     * @var ProducerFactoryInterface
     */
    protected $producerFactory;

    public function __construct(
        ProducerRepositoryInterface $producerRepository,
        ProducerFactoryInterface $producerFactory
    ) {
        $this->producerRepository = $producerRepository;
        $this->producerFactory = $producerFactory;
    }

    public function __invoke(CreateProducerCommand $command): void
    {
        $producer = $this->producerFactory->create(
            $command->getId(),
            $command->getName(),
            $command->getTitle(),
            $command->getRating(),
            $command->getCreatedAt()
        );
        $this->producerRepository->add($producer);
    }
}
