<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\DeletePhoto;

class DeleteItemPhotoCommand
{
    /** @var string */
    protected $itemId;

    /** @var string */
    protected $photoId;

    public function __construct(string $itemId, string $photoId)
    {
        $this->itemId = $itemId;
        $this->photoId = $photoId;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }

    public function getPhotoId(): string
    {
        return $this->photoId;
    }
}
