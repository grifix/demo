<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\DeletePhoto;

use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

class DeleteItemPhotoCommandHandler
{
    /** @var ItemRepositoryInterface */
    protected $itemRepository;

    public function __construct(
        ItemRepositoryInterface $itemRepository
    ) {
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(DeleteItemPhotoCommand $command)
    {
        $item = $this->itemRepository->getById($command->getItemId());
        $item->deletePhoto($command->getPhotoId());
    }
}
