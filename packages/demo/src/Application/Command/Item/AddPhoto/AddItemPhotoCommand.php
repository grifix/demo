<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\AddPhoto;

use Grifix\Demo\Application\Command\Item\AddPhoto\Dto\PhotoDto;

class AddItemPhotoCommand
{
    /** @var string */
    protected $itemId;

    /** @var PhotoDto */
    protected $photo;

    public function __construct(string $itemId, PhotoDto $photo)
    {
        $this->itemId = $itemId;
        $this->photo = $photo;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }

    public function getPhoto(): PhotoDto
    {
        return $this->photo;
    }
}
