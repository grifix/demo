<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\AddPhoto;

use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

class AddItemPhotoCommandHandler
{
    /** @var ItemRepositoryInterface */
    protected $itemRepository;

    public function __construct(
        ItemRepositoryInterface $itemRepository
    ) {
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(AddItemPhotoCommand $command)
    {
        $item = $this->itemRepository->getById($command->getItemId());
        $photo = $command->getPhoto();
        $item->addPhoto(
            $photo->getImage()->getSourcePath(),
            $photo->getId(),
            $photo->getImage()->getName(),
            $photo->getDescription(),
            $photo->getSequence()
        );
    }
}
