<?php declare(strict_types = 1);

namespace Grifix\Demo\Application\Command\Item\AddPhoto\Dto;

use Grifix\Shared\Domain\File\FileDto;

class PhotoDto
{
    /** @var string */
    protected $id;

    /** @var FileDto */
    protected $image;

    /** @var string[] */
    protected $description;

    /** @var int|null */
    protected $sequence;

    public function __construct(string $id, FileDto $image, array $description, ?int $sequence = null)
    {
        $this->id = $id;
        $this->image = $image;
        $this->description = $description;
        $this->sequence = $sequence;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getImage(): FileDto
    {
        return $this->image;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }
}
