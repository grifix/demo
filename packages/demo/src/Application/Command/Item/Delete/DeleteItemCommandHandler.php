<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\Delete;

use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

class DeleteItemCommandHandler
{
    /** @var ItemRepositoryInterface */
    protected $itemRepository;

    public function __construct(
        ItemRepositoryInterface $itemRepository
    ) {
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(DeleteItemCommand $command): void
    {
        $item = $this->itemRepository->getById($command->getItemId());
        $item->delete();
        $this->itemRepository->delete($item);
    }
}
