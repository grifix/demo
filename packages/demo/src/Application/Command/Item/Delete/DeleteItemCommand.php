<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\Delete;

class DeleteItemCommand
{
    /** @var string */
    protected $itemId;

    public function __construct(string $itemId)
    {
        $this->itemId = $itemId;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }
}
