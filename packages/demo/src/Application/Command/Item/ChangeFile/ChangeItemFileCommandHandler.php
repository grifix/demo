<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\ChangeFile;

use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

class ChangeItemFileCommandHandler
{

    public function __construct(
        protected ItemRepositoryInterface $itemRepository
    ) {
    }

    public function __invoke(ChangeItemFileCommand $command): void
    {
        $this->itemRepository->getById($command->getItemId())
            ->changeFile($command->getFile()->getSourcePath(), $command->getFile()->getName());
    }
}
