<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\ChangeFile;

use Grifix\Shared\Domain\File\FileDto;

class ChangeItemFileCommand
{
    /** @var string */
    protected $itemId;

    /** @var FileDto */
    protected $file;

    public function __construct(
        string $itemId,
        FileDto $file
    ) {
        $this->itemId = $itemId;
        $this->file = $file;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }

    public function getFile(): FileDto
    {
        return $this->file;
    }
}
