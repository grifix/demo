<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\DeleteFile;

use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

class DeleteItemFileCommandHandler
{
    /** @var ItemRepositoryInterface */
    protected $itemRepository;

    public function __construct(
        ItemRepositoryInterface $itemRepository
    ) {
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(DeleteItemFileCommand $command): void
    {
        $item = $this->itemRepository->getById($command->getItemId());
        $item->deleteFile();
    }
}
