<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\Create;

use Grifix\Shared\Domain\File\FileDto;

class CreateItemCommand
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $producerId;

    /** @var string */
    protected $name;

    /** @var string[] */
    protected $title;

    /** @var int */
    protected $date;

    /** @var string */
    protected $email;

    /** @var int */
    protected $width;

    /** @var int */
    protected $height;

    /** @var FileDto|null */
    protected $file;

    /** @var FileDto|null */
    protected $image;

    /** @var bool */
    protected $public;

    /** @var string|null */
    protected $url;

    /** @var string|null */
    protected $ip;

    /** @var string[]|null */
    protected $description;

    public function __construct(
        string $id,
        string $producerId,
        string $name,
        array $title,
        int $date,
        string $email,
        int $width,
        int $height,
        ?FileDto $file,
        ?FileDto $image,
        bool $public,
        ?string $url,
        ?string $ip,
        ?array $description
    ) {
        $this->id = $id;
        $this->producerId = $producerId;
        $this->name = $name;
        $this->title = $title;
        $this->date = $date;
        $this->email = $email;
        $this->file = $file;
        $this->image = $image;
        $this->public = $public;
        $this->url = $url;
        $this->ip = $ip;
        $this->description = $description;
        $this->width = $width;
        $this->height = $height;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    public function getDate(): int
    {
        return $this->date;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFile(): ?FileDto
    {
        return $this->file;
    }

    public function getImage(): ?FileDto
    {
        return $this->image;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @return string[]|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }
}
