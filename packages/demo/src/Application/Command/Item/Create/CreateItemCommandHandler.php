<?php declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\Create;

use Grifix\Demo\Domain\Item\ItemFactoryInterface;
use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

class CreateItemCommandHandler
{
    /** @var ItemRepositoryInterface */
    protected $itemRepository;

    /** @var ItemFactoryInterface */
    protected $itemFactory;

    public function __construct(
        ItemRepositoryInterface $itemRepository,
        ItemFactoryInterface $itemFactory
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemFactory = $itemFactory;
    }

    public function __invoke(CreateItemCommand $command): void
    {
        $item = $this->itemFactory->create(
            $command->getId(),
            $command->getProducerId(),
            $command->getName(),
            $command->getTitle(),
            $command->getDate(),
            $command->getEmail(),
            $command->getWidth(),
            $command->getHeight(),
            $command->getFile(),
            $command->getImage(),
            $command->isPublic(),
            $command->getUrl(),
            $command->getIp(),
            $command->getDescription()
        );

        $this->itemRepository->add($item);
    }
}
