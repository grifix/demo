<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\ChangeProducer;

use Grifix\Demo\Domain\Item\Repository\ItemRepositoryInterface;

final class ChangeItemProducerCommandHandler
{
    public function __construct(private ItemRepositoryInterface $repository)
    {
    }

    public function __invoke(ChangeItemProducerCommand $command)
    {
        $this->repository->getById($command->getId())->changeProducer($command->getProducerId());
    }
}
