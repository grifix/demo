<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Application\Command\Item\ChangeProducer;

final class ChangeItemProducerCommand
{
    public function __construct(
        private string $id,
        private string $producerId
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProducerId(): string
    {
        return $this->producerId;
    }
}
