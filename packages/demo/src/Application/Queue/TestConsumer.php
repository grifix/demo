<?php
declare(strict_types=1);

namespace Grifix\Demo\Application\Queue;

class TestConsumer
{
    protected $rootDir;

    /**
     * TestConsumer constructor.
     * @param string $rootDir <root_dir>
     */
    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * @param TestMessage $message
     */
    public function __invoke(TestMessage $message)
    {
        file_put_contents(
            $this->rootDir . '/tmp/queue.log',
            $message->getDate() . "\t" .$message->getText()."\n",
            FILE_APPEND
        );
    }
}
