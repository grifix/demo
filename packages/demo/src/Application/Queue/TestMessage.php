<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Application\Queue;

class TestMessage
{
    private string $text;

    protected string $date;

    public function __construct(string $text, string $date)
    {
        $this->text = $text;
        $this->date = $date;
    }


    public function getText(): string
    {
        return $this->text;
    }

    public function getDate(): string
    {
        return $this->date;
    }
}
