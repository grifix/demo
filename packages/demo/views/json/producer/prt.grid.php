<?php declare(strict_types = 1);

namespace {

    use Grifix\Demo\Application\Query\FindProducers\FindProducersQueryResult;
    use Grifix\Kit\View\Helper\ArrayViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */

    /**@var FindProducersQueryResult $result */
    $result = $this->getVar('result');
    $json = [
        'total' => $result->getTotal(),
        'rows' => []
    ];
    foreach ($result->getProducers() as $producer) {
        $row = $this->getHelper(ArrayViewHelper::class)->toArray($producer);
        $json['rows'][] = $row;
    }

    echo json_encode($json);
}
