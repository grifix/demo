<?php declare(strict_types = 1);

namespace {

    use Grifix\Demo\Application\Query\FindProducers\FindProducersQueryResult;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */

    /**@var FindProducersQueryResult $result */
    $result = $this->getVar('result');
    $json = [];
    foreach ($result->getProducers() as $producer) {
        $json[] = [
            'id' => $producer->getId(),
            'name' => $producer->getName()
        ];
    }

    echo json_encode($json);
}
