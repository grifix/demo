<?php declare(strict_types = 1);

namespace {

    use Grifix\Demo\Application\Query\FindItems\FindItemsQueryResult;
    use Grifix\Kit\View\Helper\ArrayViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */

    /**@var FindItemsQueryResult $result */
    $result = $this->getVar('result');
    $json = [
        'total' => $result->getTotal(),
        'rows' => []
    ];
    foreach ($result->getItems() as $item) {
        $row = $this->getHelper(ArrayViewHelper::class)->toArray($item);
        $row['producerName'] = $item->getProducer()->getName();
        $json['rows'][] = $row;
    }

    echo json_encode($json);
}
