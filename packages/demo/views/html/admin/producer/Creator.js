/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Form.js*/
jQuery(function ($) {
    "use strict";

    // noinspection JSUnresolvedVariable
    var parent = grifix.admin.Form;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            target:{
                url: gfx.makeActionUrl('grifix.demo.producer.createProducer')
            }
        },
        {
            source: {
                url: gfx.makeWidgetUrl(null, 'grifix.demo.{skin}.admin.producer.prt.creator')
            },
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
            },
            getTitle: function () {
                // noinspection JSUnresolvedFunction
                return php(/*$t('grifix.demo.newProducer')*/);
            }
        }
    );

});
