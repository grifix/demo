/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Grid.js*/
/*require {src}/grifix/admin/views/{skin}/Window.js*/
/*require {src}/grifix/demo/views/{skin}/admin/producer/Creator.js*/
/*require {src}/grifix/demo/views/{skin}/admin/producer/Editor.js*/
jQuery(function ($) {
    "use strict";

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        grifix.admin.Grid,
        {
            api: {
                load: gfx.routeToAction('grifix.demo.producer.findProducers', 'grifix.demo.json.producer.prt.grid'),
                delete: gfx.routeToAction('grifix.demo.producer.deleteProducer')
            },
            creator: {
                widget: {
                    name: 'grifix_demo_admin_producer_Creator'
                }
            },
            editor: {
                widget: {
                    name: 'grifix_demo_admin_producer_Editor'
                }
            },
            columns: [
                {
                    field: 'name',
                    title: php(/*$translate('grifix.kit.name')*/),
                    sortable: true,
                    filter: {
                        type: 'text'
                    }
                },

                {
                    field: 'title',
                    title: php(/*$translate('grifix.kit.title')*/),
                    sortable: true,
                    intl: true,
                    filter: {
                        type: 'text'
                    }
                },
                {
                    field: 'createdAt',
                    title: php(/*$translate('grifix.kit.createdAt')*/),
                    sortable: true,
                    type: 'dateTime',
                    filter: {
                        type: 'date'
                    }
                },
                {
                    field: 'rating',
                    title: php(/*$translate('grifix.demo.rating')*/),
                    sortable: true,
                    filter: {
                        type: 'int'
                    }
                }
            ]
        },
        {
            source: {
                url: gfx.makeWidgetUrl(null, 'grifix.demo.{skin}.admin.producer.prt.grid')
            },
            init: function () {
                var that = this;
                that.parent.prototype.init.call(that);
            }

        }
    );

});
