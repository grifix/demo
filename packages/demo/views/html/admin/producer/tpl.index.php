<?php
declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\Helper\StringViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this*/
    $this->inherits('grifix.admin.{skin}.lyt.default');
    $this->addJs('{src}/grifix/demo/views/{skin}/admin/producer/Grid.js');
    $title = $this->getHelper(StringViewHelper::class)->ucFirst($this->translate('grifix.demo.producer.p'));

    ?>
    <?php $this->startBlock('title') ?>
    <?= $title ?> - <?= $this->getParentBlock() ?>
    <?php $this->endBlock() ?>

    <?php $this->startBlock('content') ?>
    <?= $this->renderPartial('grifix.demo.{skin}.admin.producer.prt.grid') ?>
    <?php $this->endBlock() ?>
    <?php
}
