<?php declare(strict_types = 1);
/**@var $this Grifix\Kit\View\ViewInterface */
?>

<div class="<?= $this->getWidgetClass() ?>">
    <table data-role="grid">
        <thead>
            <tr>

            </tr>
        </thead>
    </table>
    <div data-role="toolbar">
        <div data-role="buttons">
            <div
                data-role="add"
                iconCls="ico-add-16"
                title="<?= $this->translate('grifix.kit.add') ?>">
            </div>
            <div
                data-role="refresh"
                iconCls="ico-refresh-16"
                title="<?= $this->translate('grifix.kit.refresh') ?>">
            </div>
            <div
                data-role="delete"
                iconCls="ico-delete-16"
                title="<?= $this->translate('grifix.kit.deleteChecked') ?>">
            </div>
        </div>
    </div>
</div>
