<?php declare(strict_types = 1);

namespace {

    use Grifix\Admin\Ui\Common\Input\AdminInputViewHelper;
    use Grifix\Kit\View\Helper\LocaleViewHelper\LocaleViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */

    $inputHelper = $this->getHelper(AdminInputViewHelper::class);
    $localeHelper = $this->getHelper(LocaleViewHelper::class);

    ?>
    <div class="<?= $this->getWidgetClass() ?>">
        <div data-options="region:'north', border:0" class="admin-form-toolbar">
            <div
                    data-role="save"
                    title="<?= $this->translate('grifix.kit.save') ?>"
                    class="easyui-linkbutton" data-options="iconCls:'ico-save-16', plain:true">

            </div>
        </div>
        <div class="admin-form-body" data-options="region:'center', fit:true, border:0">

            <form method="post" enctype="multipart/form-data" data-role="form">
                <?= $inputHelper->text()
                    ->setLabel($this->translate('grifix.kit.name'))
                    ->setName('name')
                    ->setRequired(true)
                    ->setPlaceholder($this->translate('grifix.kit.name'))
                    ->setDescription($this->translate('grifix.demo.msg_nameDescription'))
                    ->build()
                ?>
                <?php foreach ($localeHelper->getLangs() as $lang): ?>
                    <?= $inputHelper->text()
                        ->setLabel(sprintf('%s (%s)', $this->translate('grifix.kit.title'), $lang->getName()))
                        ->setName(sprintf('title[%s]', $lang->getCode()))
                        ->setRequired(true)
                        ->build()
                    ?>
                <?php endforeach; ?>
            </form>
            <div class="admin-form-space"></div>
        </div>
    </div>
    <?php
}
