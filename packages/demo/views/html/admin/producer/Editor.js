/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Form.js*/
jQuery(function ($) {
    "use strict";

    // noinspection JSUnresolvedVariable
    var parent = grifix.admin.Form;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            target:{
                url: gfx.makeActionUrl('grifix.demo.producer.updateProducer')
            }
        },
        {
            source: {
                url: gfx.makeWidgetUrl('grifix.demo.producer.getProducer', 'grifix.demo.{skin}.admin.producer.prt.editor')
            },
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
            },
            getTitle: function () {
                // noinspection JSUnresolvedFunction
                return php(/*$t('grifix.demo.editionProducer')*/);
            }
        }
    );

});
