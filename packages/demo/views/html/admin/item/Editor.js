/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Form.js*/
jQuery(function ($) {
    "use strict";

    // noinspection JSUnresolvedVariable
    var parent = grifix.admin.Form;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            target:{
                url: gfx.makeActionUrl('grifix.demo.item.updateItem')
            }
        },
        {
            source: {
                url: gfx.makeWidgetUrl('grifix.demo.item.getItem', 'grifix.demo.{skin}.admin.item.prt.editor')
            },
            init: function () {
                var that = this;
                parent.prototype.init.call(that);
            },
            getTitle: function () {
                // noinspection JSUnresolvedFunction
                return php(/*$t('grifix.demo.editionProducer')*/);
            }
        }
    );

});
