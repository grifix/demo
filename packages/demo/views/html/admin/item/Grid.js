/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Grid.js*/
/*require {src}/grifix/admin/views/{skin}/Window.js*/
/*require {src}/grifix/demo/views/{skin}/admin/item/Creator.js*/
/*require {src}/grifix/demo/views/{skin}/admin/item/Editor.js*/
jQuery(function ($) {
    "use strict";

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        grifix.admin.Grid,
        {
            api: {
                load: gfx.routeToAction('grifix.demo.item.findItems', 'grifix.demo.json.item.prt.grid') + '?withProducer=1',
                delete: gfx.routeToAction('grifix.demo.producer.deleteProducer')
            },
            creator: {
                widget: {
                    name: 'grifix_demo_admin_item_Creator'
                }
            },
            editor: {
                widget: {
                    name: 'grifix_demo_admin_item_Editor'
                }
            },
            columns: [
                {
                    field: 'name',
                    title: php(/*$translate('grifix.kit.name')*/),
                    sortable: true,
                    filter: {
                        type: 'text'
                    }
                },
                {
                    field: 'title',
                    title: php(/*$translate('grifix.kit.title')*/),
                    sortable: true,
                    intl: true,
                    filter: {
                        type: 'text'
                    }
                },
                {
                    field: 'producerName',
                    title: gfx.capitalize(php(/*$translate('grifix.demo.producer')*/)),
                    sortable: true,
                    type: 'text',
                    filter: {
                        type: 'text'
                    }
                }
            ]
        },
        {
            source: {
                url: gfx.makeWidgetUrl(null, 'grifix.demo.{skin}.admin.producer.prt.grid')
            },
            init: function () {
                var that = this;
                that.parent.prototype.init.call(that);
            }

        }
    );

});
