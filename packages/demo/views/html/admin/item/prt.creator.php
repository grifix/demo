<?php declare(strict_types = 1);

namespace {

    use Grifix\Admin\Ui\Common\Input\AdminInputViewHelper;
    use Grifix\Demo\Domain\Item\Status\ItemStatus;
    use Grifix\Demo\Ui\Http\Action\Producer\FindProducersActionHandler;
    use Grifix\Demo\Ui\Common\Item\RequestValidator\CreateItemRequestValidator;
    use Grifix\Kit\View\Helper\LocaleViewHelper\LocaleViewHelper;
    use Grifix\Kit\View\Helper\UrlViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */

    /** @var AdminInputViewHelper $inputHelper */
    $inputHelper = $this->getHelper(AdminInputViewHelper::class);

    /** @var UrlViewHelper $routeHelper */
    $routeHelper = $this->getHelper(UrlViewHelper::class);

    /** @var LocaleViewHelper $localeHelper */
    $localeHelper = $this->getHelper(LocaleViewHelper::class);

    ?>
    <div class="<?= $this->getWidgetClass() ?>">
        <div data-options="region:'north', border:0" class="admin-form-toolbar">
            <div
                    data-role="save"
                    title="<?= $this->translate('grifix.kit.save') ?>"
                    class="easyui-linkbutton" data-options="iconCls:'ico-save-16', plain:true">
            </div>
        </div>
        <div class="admin-form-body" data-options="region:'center', fit:true, border:0">

            <form method="post" enctype="multipart/form-data" data-role="form">
                <?= $inputHelper->text()
                    ->setLabel($this->translate('grifix.kit.name'))
                    ->setName(CreateItemRequestValidator::NAME)
                    ->setRequired(true)
                    ->setPlaceholder($this->translate('grifix.kit.name'))
                    ->build()
                ?>

                <?= $inputHelper->combo()
                    ->setDataUrl($routeHelper->makeActionUrl(
                        FindProducersActionHandler::getAlias(),
                        'grifix.demo.json.producer.prt.list'
                    ))
                    ->setLabel($this->translate('grifix.demo.producer'))
                    ->setName(CreateItemRequestValidator::PRODUCER_ID)
                    ->setDescription('Producer description')
                    ->setRequired(true)
                    ->build() ?>

                <?php foreach ($localeHelper->getLangs() as $lang): ?>
                    <?= $inputHelper->text()
                        ->setLabel(sprintf('%s (%s)', $this->translate('grifix.kit.title'), $lang->getName()))
                        ->setName(sprintf(CreateItemRequestValidator::TITLE . '[%s]', $lang->getCode()))
                        ->setRequired(true)
                        ->setDescription('title description')
                        ->build()
                    ?>
                <?php endforeach; ?>

                <?= $inputHelper->select()
                    ->setOptions([
                        ItemStatus::STATUS_NEW => $this->translate('grifix.demo.new'),
                        ItemStatus::STATUS_CLOSED => $this->translate('grifix.demo.closed'),
                        ItemStatus::STATUS_CONFIRMED => $this->translate('grifix.demo.confirmed'),
                    ])
                    ->setName('status')
                    ->setLabel($this->translate('grifix.demo.status'))
                    ->setDescription('status description')
                    ->build()
                ?>

                <?= $inputHelper->date()
                    ->setLabel($this->translate('grifix.kit.date'))
                    ->setName('date')
                    ->setValue((new DateTimeImmutable())->format('Y-m-d'))
                    ->setDescription('date description')
                    ->build()?>

                <?= $inputHelper->text()
                    ->setLabel($this->translate('grifix.kit.email'))
                    ->setName(CreateItemRequestValidator::EMAIL)
                    ->setRequired(true)
                    ->setPlaceholder($this->translate('grifix.kit.email'))
                    ->build()
                ?>

                <?= $inputHelper->text()
                    ->setLabel('URL')
                    ->setName(CreateItemRequestValidator::URL)
                    ->setRequired(true)
                    ->setPlaceholder('URL')
                    ->build()
                ?>

                <?= $inputHelper->text()
                    ->setLabel('IP')
                    ->setName(CreateItemRequestValidator::IP)
                    ->setRequired(true)
                    ->setPlaceholder('IP')
                    ->build()
                ?>

                <?= $inputHelper->file()
                    ->setLabel($this->translate('grifix.kit.file'))
                    ->setName(CreateItemRequestValidator::FILE)
                    ->setRequired(true)
                    ->setDescription('file description')
                    ->build()
                ?>

                <?= $inputHelper->file()
                    ->setLabel($this->translate('grifix.kit.image'))
                    ->setName(CreateItemRequestValidator::IMAGE)
                    ->setRequired(true)
                    ->setDescription('image description')
                    ->build()
                ?>

                <?= $inputHelper->checkBox()
                    ->setName(CreateItemRequestValidator::PUBLIC)
                    ->setDescription($this->translate('grifix.kit.published'))
                    ->build()
                ?>

                <?= $inputHelper->text()
                    ->setLabel($this->translate('grifix.kit.width'))
                    ->setName(CreateItemRequestValidator::WIDTH)
                    ->build()
                ?>

                <?= $inputHelper->text()
                    ->setLabel($this->translate('grifix.kit.height'))
                    ->setName(CreateItemRequestValidator::HEIGHT)
                    ->build()
                ?>

                <?= $inputHelper->textarea()
                    ->setWidth(500)
                    ->setHeight(100)
                    ->setLabel($this->translate('grifix.kit.description'))
                    ->setName(CreateItemRequestValidator::DESCRIPTION)
                    ->build()?>
            </form>
            <div class="admin-form-space"></div>
        </div>
    </div>
    <?php
}
