<?php
/**@var $this \Grifix\Kit\View\ViewInterface */
$this->inherits('grifix.kit.{skin}.lyt.default');
$position = $_GET['position'];
if (!$position) {
    $position = 'n';
}
?>

<?php $this->startBlock('content') ?>
    <script type="text/javascript">
        jQuery(function () {
            $('#button').click(function () {
                $('#messenger').data('grifix_kit_Messenger').showMessage('test');
            });
    
            $('#button2').click(function () {
                $('#messenger').data('grifix_kit_Messenger').showMessage('Lore ipsum, Lore ipsum, Lore ipsum');
            });
        });
    </script>
    <button type="button" id="button">show message</button>
    <button type="button" id="button2">show message</button>
    <div id="messenger" class="wg-grifix_kit_Messenger" style="width: 500px; height: 1500px; margin: 50px; overflow: auto; border: 1px solid black">
        <div style="height: 600px"></div>
    </div>

<?php $this->endBlock() ?>