<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\View\Helper\InputViewHelper;
    use Grifix\Kit\View\ViewInterface;

    /**@var ViewInterface $this */
    $inputHelper = $this->getHelper(InputViewHelper::class);
    ?>
    <form class="<?= $this->makeCssClass() ?> <?= $this->getWidgetClass() ?> panel">
        <div class="panel-body">

            <?= $inputHelper->text()
                ->setName('name')
                ->setPlaceholder('Name')
                ->setDescription('<b>name</b> description')
                ->build()
            ?>
            <?= $inputHelper->email()
                ->setName('email')
                ->setPlaceholder('Email')
                ->setDescription('Email description')
                ->setRequired()
                ->setLabel('Email')
                ->build()
            ?>

            <?= $inputHelper->file()
                ->setName('file')
                ->setLabel('File')
                ->setDescription('File description')
                ->build()
            ?>
            <div class="panel">
                <div class="panel-heading">
                    Address
                </div>
                <div class="panel-body">
                    <?= $inputHelper->text()
                        ->setName('address[1]')
                        ->setPlaceholder('Address 1')
                        ->setRequired()
                        ->build()
                    ?>

                    <?= $inputHelper->text()
                        ->setName('address[2]')
                        ->setPlaceholder('Address 2')
                        ->setRequired()
                        ->build()
                    ?>

                    <?= $inputHelper->text()
                        ->setName('address[3]')
                        ->setPlaceholder('Address 3')
                        ->setRequired()
                        ->build()
                    ?>
                </div>
            </div>
            <div class="padding-top">
                <button type="button" data-role="submit" class="button">Submit</button>
            </div>
            <div class="arrow-up"></div>

        </div>
    </form>
    <?php
}
