<?php
/**@var $this \Grifix\Kit\View\ViewInterface*/
$this->inherits('grifix.kit.{skin}.lyt.default');
$this->addJs('{src}/grifix/demo/views/{skin}/form/Form.js');
$this->addCss('{src}/grifix/demo/views/{skin}/form/tpl.layout.css');
?>

<?php $this->startBlock('content')?>
<div class="<?=$this->makeCssClass()?> row">
    <?= $this->renderPartial('grifix.demo.{skin}.form.prt.form')?>
</div>
<?php $this->endBlock()?>
