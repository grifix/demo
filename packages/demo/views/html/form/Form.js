/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Form.js*/

// noinspection JSUnusedLocalSymbols
jQuery(function ($) {
    "use strict";

    // noinspection JSUnresolvedVariable
    var parent = grifix.kit.Form;

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        parent,
        {
            target:{
                url: gfx.makeActionUrl('grifix.demo.submitForm'),
                method: 'post'
            },
            on:{
                success:function () {
                    // noinspection JSUnresolvedFunction
                    gfx.showSuccess(php(/*$translate('grifix.kit.msg_successfullySaved')*/));
                }
            }
        },
        {
            source:{
                url: gfx.makeWidgetUrl('grifix.demo.getForm','grifix.demo.{skin}.form.prt.form')
            },
            init: function () {
                var that = this;
                parent.prototype.init.call(that);

            }
        }
    );
});
