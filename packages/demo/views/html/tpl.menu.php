<?php
/**@var $this \Grifix\Kit\View\ViewInterface */
$this->inherits('grifix.kit.{skin}.lyt.default');
$position = $_GET['position'];
if (!$position) {
    $position = 'n';
}
?>

<?php $this->startBlock('content') ?>
    <script type="text/javascript">
        jQuery(function () {
            $('#button').grifix_kit_Menu({
                items: [
                    {
                        id: 1,
                        text: 'One'
                    },
                    {
                        id:2,
                        text: 'Two'
                    }
                ]
                
                
            });
        });
    </script>
    <button type="button" id="button">show menu</button>

<?php $this->endBlock() ?>