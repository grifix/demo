<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\ViewInterface */
/**@var $list \Grifix\Demo\Application\Query\GetList\Dto\ItemsList*/
?>
<div class="<?= $this->makeCssClass() ?> <?= $this->getWidgetClass() ?> panel panel-default">
    <div class="panel-body">
        <ul class="list-group">
            <?php foreach ($this->getVar('items') as $item): /**@var $item \Grifix\Demo\Application\Query\GetList\Dto\Item*/?>
                <li class="list-group-item"><?= $item->getName() ?> (<?=$item->getPriority()?>)</li>
            <?php endforeach; ?>
        </ul>
        <nav>
            <?php for ($i = 1; $i < $this->getVar('numOfPages') + 1; $i++): ?>
                <a class="btn <?php if($i == $this->getVar('page')):?>btn-success<?php else:?>btn-default<?php endif;?>" href="#" data-page="<?=$i?>" data-role="page"><?= $i ?></a>
            <?php endfor; ?>
        </nav>
    </div>
</div>
