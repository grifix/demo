/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {},
        {
            source:{
                url: gfx.makeWidgetUrl('grifix.demo.getList','grifix.demo.{skin}.component.prt.list')
            },
            init: function () {
                var that = this;
                that.parent.prototype.init.call(that);
                that.elPages = that.find('page');
                
                that.elPages.on('click',function () {
                    that.loadPage($(this).data('page'));
                    return false;
                });
            },
            
            loadPage:function (page) {
                var that = this;
                that.reload({
                    source:{
                        data:{
                            page:page
                        }
                    }
                });
                return that.el();
            }
        }
    );
});
