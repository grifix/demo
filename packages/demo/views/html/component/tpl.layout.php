<?php
/**@var $this \Grifix\Kit\View\ViewInterface*/
$this->inherits('grifix.kit.{skin}.lyt.default');
$this->addJs('{src}/grifix/demo/views/{skin}/component/Controller.js')
?>

<?php $this->startBlock('content')?>
<div class="<?=$this->makeCssClass()?>">
    <?= $this->renderPartial('grifix.demo.{skin}.component.prt.controller')?>
</div>
<?php $this->endBlock()?>
