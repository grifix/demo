/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/demo/views/{skin}/component/List.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {},
        {
            init: function () {
                var that = this;
                that.parent.prototype.init.call(that);
                that.elRefresh = that.find('refresh');
                that.elRefresh.on('click',function () {
                    gfx.reloadWidgets([
                        {
                            el:that.find('list1').find('.wg-grifix_demo_component_List'),
                            name:'grifix_demo_component_List',
                            params:{
                                page: 1
                            }
                        },
                        {
                            el:that.find('list2').find('.wg-grifix_demo_component_List'),
                            name:'grifix_demo_component_List',
                            params:{
                                page: 3
                            }
                        }
                    ]);
                });
            }
        }
    );
});
