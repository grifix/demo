<?php
/**@var $this \Grifix\Kit\View\ViewInterface */
?>
<div class="<?= $this->makeCssClass() ?> <?= $this->getWidgetClass() ?> panel panel-default"">
    <div class="panel-body row">
        <div class="col-md-6" data-role="list1">
            <?= $this->renderComponent('grifix.demo.getList', 'grifix.demo.{skin}.component.prt.list', ['page' => 2]) ?>
        </div>
        <div class="col-md-6" data-role="list2">
            <?= $this->renderComponent('grifix.demo.getList', 'grifix.demo.{skin}.component.prt.list', ['page' => 1]) ?>
        </div>
    </div>
    <div class="panel-footer">
        <button data-role="refresh" class="btn btn-default">Refresh</button>
    </div>
</div>
