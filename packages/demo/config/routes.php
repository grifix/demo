<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace {

    use Grifix\Kit\Route\RouteDefinition;

    return [
        'root' => RouteDefinition::withChildren(
            '/{locale:locale?}/grifix/demo',
            [
                'component' => RouteDefinition::withHandler('/component', 'grifix.demo.component'),
                'form' => RouteDefinition::withHandler('/form', 'grifix.demo.form'),
                'test' => RouteDefinition::withHandler('/test', 'grifix.demo.test'),
                'messenger' => RouteDefinition::withHandler('/messenger', 'grifix.demo.messenger'),
                'menu' => RouteDefinition::withHandler('/menu', 'grifix.demo.menu'),
                'admin' => RouteDefinition::withChildren(
                    '/admin',
                    [
                        'producers' => RouteDefinition::withHandler('/producers', 'grifix.demo.admin.producers'),
                        'items' => RouteDefinition::withHandler('/items', 'grifix.demo.admin.items')
                    ]
                )
            ]
        )
    ];
}
