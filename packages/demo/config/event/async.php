<?php
declare(strict_types = 1);

namespace {

    use Grifix\Demo\Application\Event\TestEvent;

    return [
        TestEvent::class
    ];
}
