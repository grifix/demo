<?php

declare(strict_types = 1);

namespace {

    return [
        '_module' => 'Демо',
        'producer' => [
            'cases' => [
                'nom' => ['производитель', 'производители'],
            ],
        ],
        'item' => [
            'cases' => [
                'nom' => ['предмет', 'предметы']
            ]
        ],
        'rating' => 'Рейтинг',
        'newProducer' => 'Новый производитель',
        'msg_nameDescription' => 'Официальное название производителя',
        'editionProducer' => 'Редактирование производителя',
        'msg_producerNameMustBeUnique' => 'Имя производителя должно быть уникальным!',
        'msg_itemNotFound' => 'Предмет не найден!',
        'photo' => 'Фотография',
        'newItem' => 'Новый предмет',
        'closed' => 'Закрыт',
        'new' => 'Новый',
        'confirmed' => 'Подтвержден',
        'status' => 'Статус'
    ];
}
