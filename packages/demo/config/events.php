<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {
    use Grifix\Demo\Domain\Item\Event as Item;
    use Grifix\Demo\Domain\Producer\Event as Producer;
    return [
        'grifix.demo.item.file.changed' => Item\FileChangedEvent::class,
        'grifix.demo.item.file.deleted' => Item\FileDeletedEvent::class,
        'grifix.demo.item.created' => Item\ItemCreatedEvent::class,
        'grifix.demo.item.deleted' => Item\ItemDeletedEvent::class,
        'grifix.demo.item.photo.deleted' => Item\PhotoDeletedEvent::class,
        'grifix.demo.producer.created' => Producer\ProducerCreatedEvent::class,
        'grifix.demo.producer.deleted' => Producer\ProducerDeletedEvent::class,
        'grifix.demo.producer.name.changed' => Producer\ProducerNameChangedEvent::class,
        'grifix.demo.producer.rating.changed' => Producer\ProducerRatingChangedEvent::class,
        'grifix.demo.producer.title.changed' => Producer\ProducerTitleChangedEvent::class
    ];
}
