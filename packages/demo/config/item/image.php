<?php declare(strict_types = 1);

namespace {

    return [
        'minWidth' => 50,
        'maxWidth' => 50,
        'minHeight' => 50,
        'maxHeight' => 50,
        'directoryTemplate' => '/demo/items/images/%s'
    ];
}
