<?php declare(strict_types = 1);

namespace {

    use Grifix\Kit\Filesystem\FilesystemInterface;
    use Grifix\Kit\Helper\MimeHelper;

    return [
        'maxSize' => 10 * FilesystemInterface::KB,
        'minSize' => 1 * FilesystemInterface::KB,
        'allowedTypes' => [MimeHelper::DOC, MimeHelper::CSV, MimeHelper::PDF, MimeHelper::TXT],
        'directoryTemplate' => '/demo/items/files/%s'
    ];
}
