<?php declare(strict_types = 1);

namespace {

    return [
        'directoryTemplate' => '/demo/items/photos/%s'
    ];
}
