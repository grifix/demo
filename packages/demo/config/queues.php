<?php
declare(strict_types=1);

namespace {

    use Grifix\Demo\Application\Queue\TestConsumer;
    use Grifix\Demo\Ui\Cli\Command\QueueTestCommand;
    use Grifix\Kit\MessageBroker\QueueDefinition;

    return [
        'test' => new QueueDefinition(TestConsumer::class, QueueTestCommand::EXCHANGE_NAME, 2),
    ];
}
