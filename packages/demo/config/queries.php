<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace {
    
    use Grifix\Demo\Application\Query\GetList\GetListQuery;
    use Grifix\Kit\Conversion\Converter\StringToIntConverter;
    use Grifix\Kit\Validation\Validator\IntValidator;
    
    return [
        GetListQuery::class => [
            'resource' => null,
            'arguments' => [
                'page' => [
                    'converters' => StringToIntConverter::class,
                    'validators' => IntValidator::class
                ]
            ]
        ]
    ];
}
