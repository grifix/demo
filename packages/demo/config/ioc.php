<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {

    use Grifix\Demo\Domain\Item\ItemInfrastructureInterface;
    use Grifix\Demo\Domain\Item\Photo\ItemPhotoInfrastructureInterface;
    use Grifix\Demo\Domain\Producer\ProducerInfrastructureInterface;
    use Grifix\Demo\Infrastructure\Domain\Item\ItemInfrastructure;
    use Grifix\Demo\Infrastructure\Domain\Item\Photo\ItemPhotoInfrastructure;
    use Grifix\Demo\Infrastructure\Domain\Producer\ProducerInfrastructure;
    use Grifix\Kit\Ioc\DefinitionMaker\DependencyDefinition;

    return [
        ItemPhotoInfrastructureInterface::class => new DependencyDefinition(ItemPhotoInfrastructure::class),
        ItemInfrastructureInterface::class => new DependencyDefinition(ItemInfrastructure::class),
        ProducerInfrastructureInterface::class => new DependencyDefinition(ProducerInfrastructure::class),
    ];

}
