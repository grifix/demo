<?php
/**
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace {

    use Grifix\Demo\Domain\Item\Photo\Description\Exception\TooLongDescriptionException;
    use Grifix\Demo\Domain\Item\Photo\Description\Exception\TooShortDescriptionException;
    use Grifix\Demo\Domain\Item\Photo\Exception\ItemPhotoNotExistException;
    use Grifix\Demo\Domain\Item\Repository\Exception\ItemNotFoundException;
    use Grifix\Demo\Domain\Item\Status\Exception\InvalidStatusException;
    use Grifix\Demo\Domain\Producer\Name\Exception\NameIsNotUniqueException;
    use Grifix\Demo\Domain\Producer\Repository\Exception\ProducerNotFoundException;
    use Grifix\Kit\Exception\UiExceptionDefinition;

    return [
        TooLongDescriptionException::class => new UiExceptionDefinition(
            'grifix.demo.tooLongDescription',
            'grifix.demo.msg_tooLongDescription'
        ),

        TooShortDescriptionException::class => new UiExceptionDefinition(
            'grifix.demo.tooShortDescription',
            'grifix.demo.msg_tooShortDescription'
        ),

        ItemPhotoNotExistException::class => new UiExceptionDefinition(
            'grifix.demo.itemPhotoNotExists',
            'grifix.demo.msg_itemPhotoNotExists'
        ),

        ItemNotFoundException::class => new UiExceptionDefinition(
            'grifix.demo.itemNotFound',
            'grifix.demo.msg_itemNotFound'
        ),

        InvalidStatusException::class => new UiExceptionDefinition(
            'grifix.demo.invalidStatus',
            'grifix.demo.msg_invalidStatus'
        ),

        NameIsNotUniqueException::class => new UiExceptionDefinition(
            'grifix.demo.producerNameMustBeUnique',
            'grifix.demo.msg_producerNameMustBeUnique'
        ),

        ProducerNotFoundException::class => new UiExceptionDefinition(
            'grifix.demo.producerNotFound',
            'grifix.demo.msg_producerNotFound',
            404
        )

    ];
}
