<?php
declare(strict_types = 1);

namespace {

    use Grifix\Acl\Domain\Role\RoleInterface;
    use Grifix\Demo\Ui\Common\DemoModulePermissions as P;
    use Grifix\Shared\Ui\Common\PermissionDefinition;

    return [
        P::CREATE_PRODUCER => new PermissionDefinition(
            P::CREATE_PRODUCER,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::ADMIN_PRODUCERS => new PermissionDefinition(
            P::ADMIN_PRODUCERS,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_PRODUCER_TITLE => new PermissionDefinition(
            P::READ_PRODUCER_TITLE,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::READ_PRODUCER_NAME => new PermissionDefinition(
            P::READ_PRODUCER_NAME,
            [RoleInterface::ROLE_GUEST]
        ),
        P::DELETE_PRODUCER => new PermissionDefinition(
            P::DELETE_PRODUCER,
            [RoleInterface::ROLE_ADMIN]
        ),
        P::UPDATE_PRODUCER => new PermissionDefinition(
            P::UPDATE_PRODUCER,
            [RoleInterface::ROLE_ADMIN]
        )
    ];
}

