<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace {

    use Grifix\Demo\Ui\Http\Action\Producer\CreateProducerActionHandler;
    use Grifix\Demo\Ui\Http\Action\Producer\DeleteProducerActionHandler;
    use Grifix\Demo\Ui\Http\Action\Producer\UpdateProducerActionHandler;
    use Grifix\Demo\Ui\Common\DemoModulePermissions;

    return [
        CreateProducerActionHandler::getAlias() => DemoModulePermissions::CREATE_PRODUCER,
        DeleteProducerActionHandler::getAlias() => DemoModulePermissions::DELETE_PRODUCER,
        UpdateProducerActionHandler::getAlias() => DemoModulePermissions::UPDATE_PRODUCER
    ];
}
