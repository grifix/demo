<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common;

use Grifix\Demo\Infrastructure\Table;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Kit\Db\ConnectionInterface;

class DemoDbWrapper
{
    /** @var ConnectionInterface */
    protected $connection;

    /** @var DemoFixtureRegistry */
    protected $fixtureRegistry;

    public function __construct(ConnectionInterface $connection, DemoFixtureRegistry $fixtureRegistry)
    {
        $this->connection = $connection;
        $this->fixtureRegistry = $fixtureRegistry;
    }

    public function getProducer(string $producerAlias): ?array
    {
        $record = $this->connection->createQuery()
            ->select('*')
            ->from(Table::PRODUCER)
            ->where('id = :id')
            ->bindValue('id', $this->fixtureRegistry->getProducerFixture($producerAlias)->getId())->fetch();
        if (!$record) {
            return null;
        }
        return json_decode($record['data'], true);
    }

    public function getItem(string $alias): ?array
    {
        $record = $this->connection->createQuery()
            ->select('*')
            ->from(Table::ITEM)
            ->where('id = :id')
            ->bindValue('id', $this->fixtureRegistry->getItemFixture($alias)->getId())->fetch();
        if (!$record) {
            return null;
        }
        return json_decode($record['data'], true);
    }
}
