<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common\Fixture\Producer;

use Grifix\Demo\Domain\Producer\Producer;
use Grifix\Kit\Fixture\AbstractFixtureMother;

class ProducerFixtureMother extends AbstractFixtureMother
{

    public function create(?ProducerFixture $fixture = null): ProducerFixture
    {
        null !== $fixture ?: $fixture = new ProducerFixture();
        $this->prepareFixture($fixture);
        $this->insertFixture($fixture);
        return $fixture;
    }

    public function update(ProducerFixture $fixture): void
    {
        $this->updateData(Producer::class, $fixture);
    }

    protected function prepareFixture(ProducerFixture $fixture): void
    {
        null !== $fixture->getId() ?: $fixture->setId($this->generateId());
        null !== $fixture->getRating() ?: $fixture->setRating(1);
        null !== $fixture->getCreatedAt() ?: $fixture->setCreatedAt('2019-01-01 12:00:00');
        null !== $fixture->getName() ?: $fixture->setName($this->faker->setUnique(true)->fakeWord());
        null !== $fixture->getTitle() ?: $fixture->setTitle($this->createTitle());
    }

    protected function insertFixture(ProducerFixture $fixture): void
    {
        $this->insertData(Producer::class, $fixture->extractData());
    }


    protected function createTitle()
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeWord();
        }
        return $result;
    }
}
