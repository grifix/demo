<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common\Fixture\Producer;

class ProducerFixture
{
    /** @var string|null */
    protected $id;

    /** @var string|null */
    protected $name;

    /** @var array|null */
    protected $title;

    /** @var string|null */
    protected $createdAt;

    /** @var int|null */
    protected $rating;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): ProducerFixture
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ProducerFixture
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): ?array
    {
        return $this->title;
    }

    public function setTitle(?array $title): ProducerFixture
    {
        $this->title = $title;
        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?string $createdAt): ProducerFixture
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): ProducerFixture
    {
        $this->rating = $rating;
        return $this;
    }

    public function extractData(): array
    {
        return [
            'id' => [
                'value' => $this->id
            ],
            'name' => [
                'value' => $this->name,
                'producerId' => $this->id
            ],
            'rating' => [
                'value' => $this->rating
            ],
            'title' => [
                'values' => $this->title
            ],
            'createdAt' => [
                'value' => $this->createdAt
            ]
        ];
    }
}
