<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common\Fixture\Item\Photo;

class ItemPhotoFixture
{
    protected ?string $id = null;

    protected ?string $file = null;

    protected ?array $description = null;

    protected ?int $sequence = null;

    protected ?string $itemId = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): ItemPhotoFixture
    {
        $this->id = $id;
        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): ItemPhotoFixture
    {
        $this->file = $file;
        return $this;
    }

    public function getDescription(): ?array
    {
        return $this->description;
    }

    public function setDescription(?array $description): ItemPhotoFixture
    {
        $this->description = $description;
        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): ItemPhotoFixture
    {
        $this->sequence = $sequence;
        return $this;
    }

    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    public function setItemId(?string $itemId): ItemPhotoFixture
    {
        $this->itemId = $itemId;
        return $this;
    }
}
