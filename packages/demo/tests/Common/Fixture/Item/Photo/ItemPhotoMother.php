<?php declare(strict_types=1);

namespace Grifix\Demo\Test\Common\Fixture\Item\Photo;

use Grifix\Demo\Application\Command\Item\AddPhoto\AddItemPhotoCommand;
use Grifix\Demo\Application\Command\Item\AddPhoto\Dto\PhotoDto;
use Grifix\Demo\Test\Common\DemoPathMaker;
use Grifix\Demo\Test\Common\Fixture\Item\ItemFixtureMother;
use Grifix\Kit\Fixture\AbstractFixtureMother;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Shared\Domain\File\FileDto;

class ItemPhotoMother extends AbstractFixtureMother
{

    protected ItemFixtureMother $itemFixtureMother;

    protected DemoPathMaker $pathMaker;

    public function __construct(IocContainerInterface $iocContainer)
    {
        parent::__construct($iocContainer);
        $this->itemFixtureMother = $iocContainer->get(ItemFixtureMother::class);
        $this->pathMaker = $iocContainer->get(DemoPathMaker::class);
    }

    public function create(?ItemPhotoFixture $fixture = null): ItemPhotoFixture
    {
        null !== $fixture ?: $fixture = new ItemPhotoFixture();
        $this->prepareFixture($fixture);
        $this->executeCommand(new AddItemPhotoCommand(
            $fixture->getItemId(),
            new PhotoDto(
                $fixture->getId(),
                new FileDto(
                    $fixture->getFile(),
                    basename($fixture->getFile())
                ),
                $fixture->getDescription(),
                $fixture->getSequence()
            )
        ));
        return $fixture;
    }

    protected function prepareFixture(ItemPhotoFixture $fixture): void
    {
        null !== $fixture->getId() ?: $fixture->setId($this->generateId());
        null !== $fixture->getItemId() ?: $this->itemFixtureMother->create()->getId();
        null !== $fixture->getSequence() ?: $fixture->setSequence(1);
        null !== $fixture->getDescription() ?: $fixture->setDescription($this->createDescription());
        null !== $fixture->getFile() ?: $fixture->setFile($this->iocContainer->get(DemoPathMaker::class)->makePath('50x50.png'));
    }


    private function createDescription(): array
    {
        $result = [];
        foreach ($this->getLangs() as $lang) {
            $result[$lang->getCode()] = $this->faker->setLocale($lang->getLocale())->fakeText();
        }
        return $result;
    }
}
