<?php declare(strict_types=1);

namespace Grifix\Demo\Test\Common\Fixture\Item;

use Grifix\Demo\Application\Command\Item\Create\CreateItemCommand;
use Grifix\Demo\Domain\Item\Status\ItemStatus;
use Grifix\Demo\Test\Common\DemoPathMaker;
use Grifix\Demo\Test\Common\Fixture\Producer\ProducerFixtureMother;
use Grifix\Kit\Fixture\AbstractFixtureMother;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Shared\Domain\Date\Date;
use Grifix\Shared\Domain\File\FileDto;

class ItemFixtureMother extends AbstractFixtureMother
{
    protected DemoPathMaker $pathMaker;

    public function __construct(IocContainerInterface $iocContainer)
    {
        parent::__construct($iocContainer);
        $this->pathMaker = $iocContainer->get(DemoPathMaker::class);
    }

    public function create(?ItemFixture $fixture = null): ItemFixture
    {
        null !== $fixture ?: $fixture = new ItemFixture();
        $this->prepareFixture($fixture);
        $this->executeCommand(new CreateItemCommand(
            $fixture->getId(),
            $fixture->getProducerId(),
            $fixture->getName(),
            $fixture->getTitle(),
            $fixture->getDate(),
            $fixture->getEmail(),
            $fixture->getWidth(),
            $fixture->getHeight(),
            new FileDto(
                $fixture->getFile(),
                basename($fixture->getFile())
            ),
            new FileDto(
                $fixture->getImage(),
                basename($fixture->getImage())
            ),
            $fixture->getPublic(),
            $fixture->getUrl(),
            $fixture->getIp(),
            $fixture->getDescription()
        ));

        return $fixture;
    }

    private function prepareFixture(ItemFixture $fixture): void
    {
        null !== $fixture->getId() ?: $fixture->setId($this->generateId());
        null !== $fixture->getProducerId() ?: $fixture->setProducerId($this->createProducerId());
        null !== $fixture->getTitle() ?: $fixture->setTitle($this->createMultiLangWord());
        null !== $fixture->getName() ?: $fixture->setName($this->faker->setUnique(true)->fakeCompanyName());
        null !== $fixture->getStatus() ?: $fixture->setStatus(ItemStatus::STATUS_NEW);
        null !== $fixture->getDate() ?: $fixture->setDate(\DateTime::createFromFormat('Y-m-d', '2019-01-01')->getTimestamp());
        null !== $fixture->getEmail() ?: $fixture->setEmail($this->faker->setUnique(true)->fakeEmail());
        null !== $fixture->getFile() ?: $fixture->setFile($this->pathMaker->makePath('file.txt'));
        null !== $fixture->getImage() ?: $fixture->setImage($this->pathMaker->makePath('50x50.png'));
        null !== $fixture->getPublic() ?: $fixture->setPublic(true);
        null !== $fixture->getUrl() ?: $fixture->setUrl('http://google.com');
        null !== $fixture->getIp() ?: $fixture->setIp('127.0.0.1');
        null !== $fixture->getDescription() ?: $fixture->setDescription($this->createMultiLangText());
        null !== $fixture->getWidth() ?: $fixture->setWidth(100);
        null !== $fixture->getHeight() ?: $fixture->setHeight(100);
    }

    private function createProducerId(): string
    {
        return $this->iocContainer->get(ProducerFixtureMother::class)->create()->getId();
    }
}
