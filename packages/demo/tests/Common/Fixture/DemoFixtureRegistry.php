<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common\Fixture;

use Grifix\Demo\Test\Common\Fixture\Item\ItemFixture;
use Grifix\Demo\Test\Common\Fixture\Item\Photo\ItemPhotoFixture;
use Grifix\Demo\Test\Common\Fixture\Producer\ProducerFixture;
use Grifix\Shared\Test\Common\AbstractFixtureRegistry;

class DemoFixtureRegistry extends AbstractFixtureRegistry
{

    public function addProducerFixture(string $alias, ProducerFixture $fixture): void
    {
        $this->addFixture($alias, $fixture);
    }

    public function getProducerFixture(string $alias): ProducerFixture
    {
        return $this->getFixture($alias, ProducerFixture::class);
    }

    public function addItemFixture(string $alias, ItemFixture $fixture): void
    {
        $this->addFixture($alias, $fixture);
    }

    public function getItemFixture(string $alias): ItemFixture
    {
        return $this->getFixture($alias, ItemFixture::class);
    }

    public function addItemPhotoFixture(string $alias, ItemPhotoFixture $fixture): void
    {
        $this->addFixture($alias, $fixture);
    }

    public function getItemPhotoFixture(string $alias): ItemPhotoFixture
    {
        return $this->getFixture($alias, ItemPhotoFixture::class);
    }
}
