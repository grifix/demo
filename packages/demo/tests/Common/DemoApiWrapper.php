<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common;

use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Shared\Test\Common\HttpClient;

class DemoApiWrapper
{
    /** @var HttpClient */
    protected $httpClient;

    /** @var DemoFixtureRegistry */
    protected $sharedStorage;

    public function __construct(HttpClient $httpClient, DemoFixtureRegistry $sharedStorage)
    {
        $this->httpClient = $httpClient;
        $this->sharedStorage = $sharedStorage;
    }

    public function getProducer(string $producerAlias): array
    {
        $this->httpClient->get('/grifix/shared/action/grifix.demo.producer.findProducers/json', [
            [
                'id' => $this->sharedStorage->getProducerFixture($producerAlias)->getId()
            ]
        ]);
        return $this->httpClient->getLastResponseAsArray()['result']['producers'][0];
    }
}
