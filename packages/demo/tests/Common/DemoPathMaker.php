<?php declare(strict_types = 1);

namespace Grifix\Demo\Test\Common;

use Grifix\Shared\Test\Common\AbstractPathMaker;

class DemoPathMaker extends AbstractPathMaker
{
    protected function getFilesDir(): string
    {
        return __DIR__ . '/../files';
    }
}
