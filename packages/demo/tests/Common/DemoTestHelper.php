<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Test\Common;

use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoDto;
use Grifix\Demo\Application\Projection\ItemPhotoProjection\ItemPhotoFilter;
use Grifix\Demo\Application\Projection\ItemProjection\ItemDto;
use Grifix\Demo\Application\Projection\ItemProjection\ItemFilter;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerDto;
use Grifix\Demo\Application\Projection\ProducerProjection\ProducerFilter;
use Grifix\Demo\Application\Query\FindItemPhotos\FindItemPhotosQuery;
use Grifix\Demo\Application\Query\FindItems\FindItemsQuery;
use Grifix\Demo\Application\Query\FindProducers\FindProducersQuery;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Test\Common\Fixture\Item\ItemFixture;
use Grifix\Demo\Test\Common\Fixture\Item\ItemFixtureMother;
use Grifix\Demo\Test\Common\Fixture\Item\Photo\ItemPhotoFixture;
use Grifix\Demo\Test\Common\Fixture\Item\Photo\ItemPhotoMother;
use Grifix\Demo\Test\Common\Fixture\Producer\ProducerFixture;
use Grifix\Demo\Test\Common\Fixture\Producer\ProducerFixtureMother;
use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
use Grifix\Shared\Domain\File\Infrastructure\FileOutsideInterface;

final class DemoTestHelper
{
    public function __construct(
        private QueryBusInterface $queryBus,
        private CommandBusInterface $commandBus,
        private DemoFixtureRegistry $fixtureRegistry,
        private ProducerFixtureMother $producerFixtureMother,
        private ItemFixtureMother $itemFixtureMother,
        private DemoPathMaker $pathMaker,
        private FileOutsideInterface $fileInfrastructure,
        private ItemPhotoMother $itemPhotoMother
    ) {
    }

    public function createProducer(ProducerFixture $fixture, ?string $alias): ProducerFixture
    {
        $this->producerFixtureMother->create($fixture);
        if ($alias) {
            $this->fixtureRegistry->addProducerFixture($alias, $fixture);
        }
        return $fixture;
    }

    public function createItem(ItemFixture $fixture, ?string $alias): ItemFixture
    {
        $this->itemFixtureMother->create($fixture);
        if ($alias) {
            $this->fixtureRegistry->addItemFixture($alias, $fixture);
        }
        return $fixture;
    }

    public function createItemPhoto(ItemPhotoFixture $fixture, ?string $alias): ItemPhotoFixture
    {
        $this->itemPhotoMother->create($fixture);
        if ($alias) {
            $this->fixtureRegistry->addItemPhotoFixture($alias, $fixture);
        }
        return $fixture;
    }

    public function getItemFixture(string $alias): ItemFixture
    {
        return $this->fixtureRegistry->getItemFixture($alias);
    }

    public function makePath(string $file): string
    {
        return $this->pathMaker->makePath($file);
    }

    public function getItemPhotoFixture(string $alias): ItemPhotoFixture
    {
        return $this->fixtureRegistry->getItemPhotoFixture($alias);
    }

    public function getProducerFixture(string $alias): ProducerFixture
    {
        return $this->fixtureRegistry->getProducerFixture($alias);
    }

    public function findItemById(string $id): ?ItemDto
    {
        $result = $this->queryBus->execute(new FindItemsQuery(ItemFilter::create()->withId($id)));
        if (empty($result->getItems())) {
            return null;
        }
        return $result->getItems()[0];
    }

    public function findItemByAlias(string $alis): ?ItemDto
    {
        return $this->findItemById($this->getItemFixture($alis)->getId());
    }

    public function findItemPhotoById(string $id): ?ItemPhotoDto
    {
        $result = $this->queryBus->execute(new FindItemPhotosQuery(ItemPhotoFilter::create()->withId($id)));
        if (empty($result->getItemPhotos())) {
            return null;
        }
        return $result->getItemPhotos()[0];
    }

    public function findItemPhotoByAlias(string $alias): ?ItemPhotoDto
    {
        return $this->findItemPhotoById($this->getItemPhotoFixture($alias)->getId());
    }

    public function findProducerById(string $id): ?ProducerDto
    {
        $result = $this->queryBus->execute(new FindProducersQuery(ProducerFilter::create()->withId($id)));
        if (empty($result->getProducers())) {
            return null;
        }
        return $result->getProducers()[0];
    }

    /**
     * @return ItemPhotoDto[]
     */
    public function findItemPhotos(string $itemAlias): array
    {
        return $this->queryBus->execute(new FindItemPhotosQuery(ItemPhotoFilter::create()->setItemIds([
            $this->getItemFixture($itemAlias)->getId()
        ])))->getItemPhotos();
    }

    public function fileExists(string $path): bool
    {
        return $this->fileInfrastructure->fileExists($path);
    }
}
