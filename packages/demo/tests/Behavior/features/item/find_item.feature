Feature: Find item
  Background:
    Given there is an item 'Apple' with data:
    """
    id: fd6f135f-86b9-4b2f-9297-927870d33e8f
    """
    And there is an item 'Banana' with data:
    """
    id: b9a03885-c9ed-4e81-ac44-10e5d68147f9
    """
    And there is an item "Peach" with data:
    """
    id: dee254ad-c627-4856-81cb-df60759114cf
    """

  Scenario: Find item by id
    When I find item by id 'fd6f135f-86b9-4b2f-9297-927870d33e8f'
    Then I should find "1" items
    And I should find "Apple" item

  Scenario: Find item with photos
    Given item "Apple" has a photo "Photo" with image "30x50.png"
    And item "Apple" has a photo "Photo2" with image "50x50.png"
    And item "Banana" has a photo "Photo3" with image "50x50.png"
    When I find item by id "fd6f135f-86b9-4b2f-9297-927870d33e8f" with photos
    Then I should find "Apple" item with photos:
    """
    - Photo
    - Photo2
    """

  Scenario: Find item with producer
    Given there is a producer "Intel" with data:
    """
    id: 7e928b6d-7c2b-4825-8209-ca7851333c52
    name: Intel
    title:
      en: Intel
      ru: Intel
    rating: 3
    """
    And the item "Apple" has a producer "Intel"
    When I find item by id "fd6f135f-86b9-4b2f-9297-927870d33e8f" with producer
    Then I should find item "Apple" with producer:
    """
    id: 7e928b6d-7c2b-4825-8209-ca7851333c52
    name: Intel
    title:
      en: Intel
      ru: Intel
    rating: 3
    """

    

