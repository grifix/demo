Feature: Create item

  Background:
    Given there is an active user "Admin"
    And I am signed in as "Admin"
    And there is a producer "Apple"

  Scenario: Create item
    When I create a new item:
    """
    producer: Apple
    name: Laptop
    title:
      en: Laptop
      ru: Лаптоп
    date: 2019-01-01 00:00:00
    email: laptop@grifix.net
    height: 100
    width: 100
    file: file.txt
    image: 50x50.png
    public: true
    url: http://google.com
    ip: 127.0.0.1
    description:
      en: Laptop description
      ru: Описание лаптопа
    """
    Then there should be a new item

  Scenario: Create item with too big file
    When I create a new item:
    """
    producer: Apple
    name: Laptop
    title:
      en: Laptop
      ru: Лаптоп
    date: 2019-01-01
    email: laptop@grifix.net
    height: 100
    width: 100
    file: big_file.txt
    image: 50x50.png
    public: true
    url: http://google.com
    ip: 127.0.0.1
    description:
      en: Laptop description
      ru: Описание лаптопа
    """
    Then I should get an error that file is too big

  Scenario: Create item with too small file
    When I create a new item:
    """
    producer: Apple
    name: Laptop
    title:
      en: Laptop
      ru: Лаптоп
    date: 2019-01-01
    email: laptop@grifix.net
    height: 100
    width: 100
    file: small_file.txt
    image: 50x50.png
    public: true
    url: http://google.com
    ip: 127.0.0.1
    description:
      en: Laptop description
      ru: Описание лаптопа
    """
    Then I should get an error that file is too small

  Scenario: Create item with wrong file type
    When I create a new item:
    """
    producer: Apple
    name: Laptop
    title:
      en: Laptop
      ru: Лаптоп
    date: 2019-01-01
    email: laptop@grifix.net
    height: 100
    width: 100
    file: file.php
    image: 50x50.png
    public: true
    url: http://google.com
    ip: 127.0.0.1
    description:
      en: Laptop description
      ru: Описание лаптопа
    """
    Then I should get an error that file has wrong type

  Scenario: Create item with narrow image
    When I create a new item:
    """
    producer: Apple
    name: Laptop
    title:
      en: Laptop
      ru: Лаптоп
    date: 2019-01-01
    email: laptop@grifix.net
    height: 100
    width: 100
    file: file.txt
    image: 30x50.png
    public: true
    url: http://google.com
    ip: 127.0.0.1
    description:
      en: Laptop description
      ru: Описание лаптопа
    """
    Then I should get an error that image is too narrow

  Scenario: Create item with wide image
    When I create a new item:
    """
    producer: Apple
    name: Laptop
    title:
      en: Laptop
      ru: Лаптоп
    date: 2019-01-01
    email: laptop@grifix.net
    height: 100
    width: 100
    file: file.txt
    image: 60x50.png
    public: true
    url: http://google.com
    ip: 127.0.0.1
    description:
      en: Laptop description
      ru: Описание лаптопа
    """
    Then I should get an error that image is too wide


