Feature: Delete item
  Background:
    Given there is an active user "Admin"
    And I am signed in as "Admin"
    And there is an item "Apple"

    Scenario: Delete item
      When I delete the item "Apple"
      Then there should be no item "Apple"
