Feature: Delete item file
  Background:
    Given there is an active user "Admin"
    And I am signed in as "Admin"
    And there is an item "Apple" with file "file.txt"

  Scenario: delete item file
    When I delete the item "Apple" file
    Then the item "Apple" should have no file
