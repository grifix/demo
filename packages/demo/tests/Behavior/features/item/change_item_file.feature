Feature: Change item file

  Background:
    Given there is an active user "Admin"
    And I am signed in as "Admin"
    And there is an item "Apple" with file "file.txt"

  Scenario: Change item file
    When I change item "Apple" file to "file2.txt"
    Then item "Apple" should have "file2.txt" file
    And there should be no file old item file

  Scenario: Change item file without a file
    When I change item "Apple" file without a file
    Then I should get an error that there is no file
