Feature: Delete item photo

  Background:
    Given there is an active user "Admin"
    And I am signed in as "Admin"
    And there is an item Apple
    And item "Apple" has a photo "Home" with image "60x50.png"

  Scenario: Delete item photo
    When I delete photo "Home" from item "Apple"
    Then item "Apple" should have no photo "Home"
