Feature: Add item photo

  Background:
    Given there is an active user "Admin" with permissions:
    """
    - addPhoto
    """
    And I am signed in as "Admin"
    And there is an item "Apple"

    Scenario: Add item photo
      When I add new photo to the item "Apple" gallery with data:
      """
      image: 30x50.png
      description:
        en: My photo
        ru: Мое фото
      sequence: 1
      """

      Then there should be the photo in the item "Apple" gallery with data:
       """
      image: 30x50.png
      description:
        en: My photo
        ru: Мое фото
      sequence: 1
      """
