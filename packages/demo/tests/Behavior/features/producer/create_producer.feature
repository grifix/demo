Feature: Create producer

  Background:
    Given there is an active user "Admin" with permission "grifix.demo.createProducer"
    And there is an active user "User"
    And I am signed in as "Admin"

  Scenario: Happy path
    When I create a new producer:
    """
    name: Intel
    title:
      en: Intel
      ru: Интел
    """
    Then It should be a new producer:
    """
    name: Intel
    title:
      en: Intel
      ru: Интел
    """

  Scenario: Short name
    When I create a new producer:
    """
    name: I
    title:
      en: Intel
      ru: Интел
    """
    Then I should get an error that name is too short

  Scenario: Short title
    When I create a new producer:
    """
    name: Intel
    title:
      en: I
      ru: И
    """
    Then I should get an error that title is too short

  Scenario: Name is required
    When I create a new producer:
    """
    title:
      en: Intel
      ru: Интел
    """
    Then I should get an error that name is required

  Scenario: Need authentication
    Given I am not signed in
    When I create a new producer:
    """
    name: Intel
    title:
      en: Intel
      ru: Интел
    """
    Then I should get an error about needing authentication

  Scenario: Access denied
    Given I am signed in as "User"
    When I create a new producer:
    """
    name: Intel
    title:
      en: Intel
      ru: Интел
    """
    Then I should be denied access
    
  Scenario: Create produced with non unique name
    Given there is a producer "Sony" with data:
    """
    name: Sony
    """
    When I create a new producer:
    """
    name: Sony
    title:
      en: Sony
      ru: Sony
    """
    Then I should get an error that producer name is not unique

