Feature: Delete producer

  Background:
    Given there is an active user "Admin" with permission "grifix.demo.deleteProducer"
    And there is an active user "User"
    And there is a producer "Sony"
    And there is a producer "Apple"
    And I am signed in as "Admin"

  Scenario: Delete producer
    When I delete producer "Sony"
    Then Producer "Sony" should be deleted

  Scenario: Delete producers
    When I delete producers:
    """
    - Sony
    - Apple
    """
    Then Producer "Sony" should be deleted
    And Producer "Apple" should be deleted

  Scenario: Need authentication
    Given I am not signed in
    When I delete producer "Sony"
    Then I should get an error about needing authentication
    And Producer "Sony" should be not deleted

  Scenario: Try to delete producer as an user
    Given  I am signed in as "User"
    When I delete producer "Sony"
    Then I should be denied access
    And Producer "Sony" should be not deleted

  Scenario: Try to delete producer using GET request
    When I delete producer "Sony" using GET request
    Then I should get an error about bad request
    And Producer "Sony" should be not deleted

