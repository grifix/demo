Feature: Find producer

  Background:
    Given there is an active user "Admin" with permissions:
    """
    - grifix.demo.readProducerName
    - grifix.demo.readProducerTitle
    """
    Given there is an active user "User" with permission "grifix.demo.readProducerName"
    Given there is a producer "Asus" with rating "1"
    And there is a producer "Sony" with rating "2"
    And there is a producer "Samsung" with rating "3"

  Scenario: Find all producers
    When I search all producers
    Then I should find "3" producers

  Scenario: Find all producers ordered by id
    When I search all producers ordered by "id"
    Then I should find "3" producers

  Scenario: Find by id
    When I search producer "Asus" by id
    Then I should find "1" producers
    And I should find "Asus" producer

  Scenario: Rating equal
    When I search producers with "rating" "equal" "2"
    Then I should find "1" producers
    And I should find "Sony" producer

  Scenario: Rating greater than
    When I search producers with "rating" "greater_than" "2"
    Then I should find "1" producers
    And I should find "Samsung" producer

  Scenario: Rating greater or equal
    When I search producers with "rating" "greater_than_or_equal" "2"
    Then I should find "2" producers
    And I should find "Sony" producer
    And I should find "Samsung" producer

  Scenario: Rating less than
    When I search producers with "rating" "less_than" "2"
    Then I should find "1" producers
    And I should find "Asus" producer

  Scenario: Rating less or equal
    When I search producers with "rating" "less_than_or_equal" "2"
    Then I should find "2" producers
    And I should find "Sony" producer
    And I should find "Asus" producer

  Scenario: Rating not equal
    When I search producers with "rating" "not_equal" "2"
    Then I should find "2" producers
    And I should find "Samsung" producer
    And I should find "Asus" producer

  Scenario: Name contains
    When I search producers with "name" "contains" "on"
    Then I should find "1" producers
    And I should find "Sony" producer

  Scenario: Name begins with
    When I search producers with "name" "begin_with" "So"
    Then I should find "1" producers
    And I should find "Sony" producer

  Scenario: Name ends with
    When I search producers with "name" "end_with" "ny"
    Then I should find "1" producers
    And I should find "Sony" producer

  Scenario: User should not see producer title
    Given I am signed in as "User"
    When I search producer "Asus" by id
    Then I should find "1" producers
    And I should find "Asus" producer
    And I should not see producer title

  Scenario: Admin should see producer title
    Given I am signed in as "Admin"
    When I search producer "Asus" by id
    Then I should find "1" producers
    And I should find "Asus" producer
    And I should see producer title

  Scenario: Find producer with items
    Given producer "Asus" has items:
    """
    - Apple
    - Banana
    - Grape
    """
    When I search producer "Asus" with items
    Then I should find producer "Asus" with items:
    """
    - Apple
    - Banana
    - Grape
    """





