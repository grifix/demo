Feature: Update producer

  Background:
    Given there is an active user "Admin" with permission "grifix.demo.updateProducer"
    And there is an active user "User"
    And I am signed in as "Admin"
    And there is a producer "Sony" with data:
     """
    name: Sony
    title:
      en: Sony
      ru: Sony
    """

  Scenario: Update producer
    When I update producer "Sony" with data:
    """
    name: Intel
    title:
      en: Intel
      ru: Intel
    """
    Then producer "Sony" should be updated

  Scenario: Need authentication
    Given I am not signed in
    When I update producer "Sony" with data:
    """
    name: Intel
    title:
      en: Intel
      ru: Intel
    """
    Then I should get an error about needing authentication
    And producer "Sony" should be not updated

  Scenario: Access denied
    Given  I am signed in as "User"
    When I update producer "Sony" with data:
    """
    name: Intel
    title:
      en: Intel
      ru: Intel
    """
    Then I should be denied access
    And producer "Sony" should be not updated


