<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);

namespace Grifix\Demo\Test\Behavior\Context;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Demo\Test\Common\DemoTestHelper;
use Grifix\Shared\Test\Behavior\Context\AbstractContext;

abstract class AbstractDemoContext extends AbstractContext
{
    protected DemoTestHelper $testHelper;

    protected function beforeScenario(BeforeScenarioScope $scope): void
    {
        $this->testHelper = $this->getShared(DemoTestHelper::class);
    }
}
