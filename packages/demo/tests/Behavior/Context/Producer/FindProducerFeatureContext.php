<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Producer;

use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Test\Common\Fixture\Item\ItemFixture;
use Grifix\Demo\Test\Common\Fixture\Producer\ProducerFixture;
use Grifix\Demo\Ui\Http\Action\Producer\FindProducersActionHandler;
use Grifix\Shared\Test\Common\HttpClient;
use PHPUnit\Framework\Assert;

class FindProducerFeatureContext extends AbstractDemoContext
{
    /**
     * @Given there is a producer :producerAlias with rating :rating
     */
    public function thereIsProducerWithRating(string $producerAlias, int $rating): void
    {
        $producerFixture = new ProducerFixture();
        $producerFixture
            ->setName($producerAlias)
            ->setRating($rating);
        $this->testHelper->createProducer($producerFixture, $producerAlias);
    }

    /**
     * @When I search all producers
     */
    public function iSearchAllProducers(): void
    {
        $this->sendGetAction(FindProducersActionHandler::getAlias());
    }

    /**
     * @When I search all producers ordered by :column
     */
    public function iSearchAllProducersOrderedBy(string $sortColumn): void
    {
        $this->sendGetAction(
            FindProducersActionHandler::getAlias(),
            [
                FindProducersActionHandler::PARAM_SORT_COLUMN => $sortColumn,
                FindProducersActionHandler::PARAM_COUNT_TOTAL => true
            ]
        );
    }

    /**
     * @Given producer :producer has items:
     */
    public function producerHasItems(string $producerAlias, string $items): void
    {
        $items = yaml_parse($items);
        $producer = $this->testHelper->getProducerFixture($producerAlias);
        foreach ($items as $itemAlias) {
            $item = (new ItemFixture())->setName($itemAlias)->setProducerId($producer->getId());
            $this->testHelper->createItem($item, $itemAlias);
        }
    }

    /**
     * @When I search producer :producer with items
     */
    public function iSearchProducerWithItems(string $producerAlias): void
    {
        $producer = $this->testHelper->getProducerFixture($producerAlias);
        $this->sendGetAction(
            FindProducersActionHandler::getAlias(),
            [
                FindProducersActionHandler::PARAM_ID => $producer->getId(),
                FindProducersActionHandler::PARAM_WITH_ITEMS => 1
            ]
        );
    }

    /**
     * @Then I should find producer :producer with items:
     */
    public function iShouldFindProducerWithItems(string $producerAlias, string $expectedItems): void
    {
        $expectedItems = yaml_parse($expectedItems);
        $producerFixture = $this->testHelper->getProducerFixture($producerAlias);
        Assert::assertEquals(200, $this->getLastResponseCode());
        $producer = $this->getLastResponseArray()['result']['producers'][0];
        Assert::assertEquals($producerFixture->getId(), $producer['id']);
        Assert::assertEquals(count($expectedItems), count($producer['items']));
        foreach ($producer['items'] as $item) {
            Assert::assertTrue(in_array($item['name'], $expectedItems));
        }
    }

    /**
     * @Then I should find :number producers
     */
    public function iShouldFindProducers(int $number)
    {
        $response = $this->getLastResponseWrapperArray();
        Assert::assertCount($number, $response->get('result.producers'));
    }

    /**
     * @When I search producers with :column :condition :value
     */
    public function iSearchProducersWithCondition(string $column, string $value, string $condition): void
    {
        $this->getShared(HttpClient::class)->getAction(
            FindProducersActionHandler::getAlias(),
            [
                'columnFilters' => [
                    [
                        'column' => $column,
                        'condition' => $condition,
                        'value' => $value
                    ]
                ]
            ]
        );
    }

    /**
     * @When I search producer :producerAlias by id
     */
    public function searchProducerById(string $producerAlias): void
    {
        $this->getShared(HttpClient::class)->getAction(
            FindProducersActionHandler::getAlias(),
            [
                'id' => $this->getShared(DemoFixtureRegistry::class)->getProducerFixture($producerAlias)->getId()
            ]
        );
    }

    /**
     * @Then I should find :producerAlias producer
     */
    public function iShouldFindProducer(string $producerAlias): void
    {
        $response = $this->getLastResponseArray();
        $result = false;
        foreach ($response['result']['producers'] as $row) {
            $producer = $this->testHelper->getProducerFixture($producerAlias);
            if ($row['id'] === $producer->getId() && $row['name'] === $producer->getName()) {
                $result = true;
                break;
            }
        }
        Assert::assertTrue($result);
    }

    /**
     * @Then I should not see producer title
     */
    public function iShouldNotSeeProducerTitle(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertTrue(is_string($response['result']['producers'][0]['title']));
    }

    /**
     * @Then I should see producer title
     */
    public function iShouldSeeProducerTitle(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertTrue(is_array($response['result']['producers'][0]['title']));
    }
}
