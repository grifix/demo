<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Producer;

use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Ui\Http\Action\Producer\CreateProducerActionHandler;
use PHPUnit\Framework\Assert;
use Symfony\Component\Yaml\Yaml;

class CreateProducerFeatureContext extends AbstractDemoContext
{

    protected ?string $newProducerId;

    /**
     * @When I create a new producer:
     */
    public function createNewProducer(string $producer): void
    {
        $this->sendPostAction(CreateProducerActionHandler::getAlias(), Yaml::parse($producer));
        $this->newProducerId = $this->getLastResponseWrapperArray()->get('id');
    }

    /**
     * @Then It should be a new producer:
     */
    public function itShouldBeNewProducer(string $expectedProducer): void
    {
        $expectedProducer = $this->wrapArray(yaml_parse($expectedProducer));
        $actualProducer = $this->testHelper->findProducerById($this->newProducerId);
        Assert::assertEquals($expectedProducer->get('name'), $actualProducer->getName());
        Assert::assertEquals($expectedProducer->get('title'), $actualProducer->getTitle());
    }

    /**
     * @Then I should get an error that name is required
     */
    public function iShouldGetAnErrorThatNameIsRequired(): void
    {
        $this->assertLastResponseCode(400);
        $response = $this->getLastResponseWrapperArray();
        Assert::assertEquals(2, count($response->get('errors')));
        Assert::assertEquals('name', $response->get('errors.0.field'));
    }

    /**
     * @Then I should get an error that name is too short
     */
    public function iShouldGetAnErrorThatNameIsTooShort(): void
    {
        $this->assertLastResponseCode(400);
        $response = $this->getLastResponseArray();
        Assert::assertCount(1, $response['errors']);
        Assert::assertEquals('name', $response['errors'][0]['field']);
        Assert::assertNotEmpty($response['errors'][0]['message']);
    }

    /**
     * @Then I should get an error that title is too short
     */
    public function iShouldGetAnErrorThatTitleIsTooShort(): void
    {
        $this->assertLastResponseCode(400);
    }
}
