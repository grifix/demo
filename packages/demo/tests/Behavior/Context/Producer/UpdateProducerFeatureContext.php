<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Producer;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\DemoDbWrapper;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Ui\Http\Action\Producer\UpdateProducerActionHandler;
use Grifix\Demo\Ui\Common\Producer\RequestValidator\UpdateProducerRequestValidator;
use Grifix\Kit\Test\Asserter;
use Grifix\Shared\Test\Behavior\Context\AbstractContext;
use PHPUnit\Framework\Assert;

class UpdateProducerFeatureContext extends AbstractDemoContext
{

    private array $dataToUpdate;
    /**
     * @When I update producer :producer with data:
     */
    public function iUpdateProducerWithData(string $producerAlias, string $data): void
    {
        $data = yaml_parse($data);
        $this->dataToUpdate = $data;
        $data[UpdateProducerRequestValidator::ID] = $this->getShared(DemoFixtureRegistry::class)
            ->getProducerFixture($producerAlias)->getId();

        $this->sendPostAction(
            UpdateProducerActionHandler::getAlias(),
            $data
        );
    }

    /**
     * @Then producer :producer should be updated
     */
    public function producerShouldHaveUpdatedData(string $producerAlias): void
    {
        $producer = $this->findRecord(
            Table::PRODUCER,
            $this->testHelper->getProducerFixture($producerAlias)->getId()
        );
        Assert::assertEquals($producer->get('name.value'), $this->dataToUpdate['name']);
        Assert::assertEquals($producer->get('title.values'), $this->dataToUpdate['title']);
    }

    /**
     * @Then producer :produced should be not updated
     */
    public function producerShouldBeNotUpdated(string $producerAlias): void
    {
        Asserter::assertArraySubset(
            $this->getShared(DemoDbWrapper::class)->getProducer($producerAlias),
            $this->getShared(DemoFixtureRegistry::class)->getProducerFixture($producerAlias)->extractData()
        );
    }
}
