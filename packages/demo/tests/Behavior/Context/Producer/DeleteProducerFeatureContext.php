<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Producer;

use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Ui\Common\Producer\RequestValidator\DeleteProducerRequestValidator;
use Grifix\Demo\Ui\Http\Action\Producer\DeleteProducerActionHandler;
use Grifix\Demo\Ui\Http\Action\Producer\FindProducersActionHandler;
use PHPUnit\Framework\Assert;

class DeleteProducerFeatureContext extends AbstractDemoContext
{
    /**
     * @When I delete producer :producerAlias
     */
    public function iDeleteProducer(string $producerAlias): void
    {
        $this->sendPostAction(
            DeleteProducerActionHandler::getAlias(),
            [
                DeleteProducerRequestValidator::ID => $this->testHelper->getProducerFixture($producerAlias)->getId()
            ]
        );
    }

    /**
     * @When I delete producers:
     */
    public function iDeleteProducers(string $producersAliases): void
    {
        $producersAliases = yaml_parse($producersAliases);
        $ids = [];
        foreach ($producersAliases as $alias) {
            $ids[] = $this->testHelper->getProducerFixture($alias)->getId();
        }

        $this->sendPostAction(
            DeleteProducerActionHandler::getAlias(),
            [
                DeleteProducerRequestValidator::ID => $ids
            ]
        );
    }

    /**
     * @When I delete producer :producerAlias using GET request
     */
    public function iDeleteProducerUsingGetRequest(string $producerAlias): void
    {
        $this->sendGetAction(
            DeleteProducerActionHandler::getAlias(),
            [
                'id' => $this->testHelper->getProducerFixture($producerAlias)->getId()
            ]
        );
    }

    /**
     * @Then Producer :producerAlias should be deleted
     */
    public function producerShouldBeDeleted(string $producerAlias): void
    {
        $this->sendGetAction(
            FindProducersActionHandler::getAlias(),
            [
                'id' => $this->testHelper->getProducerFixture($producerAlias)->getId()
            ]
        );
        $response = $this->getLastResponseArray();
        Assert::assertEquals(0, count($response['result']['producers']));
    }

    /**
     * @Then Producer :producerAlias should be not deleted
     */
    public function producerShouldBeNotDeleted(string $producerAlias): void
    {
        $this->sendGetAction(
            FindProducersActionHandler::getAlias(),
            [
                'id' => $this->testHelper->getProducerFixture($producerAlias)->getId()
            ]
        );
        $response = $this->getLastResponseArray();
        Assert::assertEquals(1, count($response['result']['producers']));
    }
}
