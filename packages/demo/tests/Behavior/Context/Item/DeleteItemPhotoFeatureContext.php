<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\Fixture\Item\Photo\ItemPhotoFixture;
use Grifix\Demo\Ui\Common\Item\RequestValidator\DeleteItemPhotoRequestValidator;
use Grifix\Demo\Ui\Http\Action\Item\DeleteItemPhotoActionHandler;
use PHPUnit\Framework\Assert;

class DeleteItemPhotoFeatureContext extends AbstractDemoContext
{
    /**
     * @Given item :item has a photo :photo with image :image
     */
    public function itemHasPhotoWithImage(string $itemAlias, string $photoAlis, string $image): void
    {
        $photoFixture = (new ItemPhotoFixture())
            ->setItemId($this->testHelper->getItemFixture($itemAlias)->getId())
            ->setFile($this->testHelper->makePath($image));
        $this->testHelper->createItemPhoto($photoFixture, $photoAlis);
    }

    /**
     * @When I delete photo :photo from item :item
     */
    public function iDeletePhotoFromItem(string $photoAlias, string $itemAlias): void
    {
        $this->sendPostAction(
            DeleteItemPhotoActionHandler::getAlias(),
            [
                DeleteItemPhotoRequestValidator::PHOTO_ID => $this->testHelper
                    ->getItemPhotoFixture($photoAlias)->getId(),
                DeleteItemPhotoRequestValidator::ITEM_ID => $this->testHelper->getItemFixture($itemAlias)->getId()
            ]
        );
    }

    /**
     * @Then item :item should have no photo :photo
     */
    public function itemShouldHaveNoPhoto(string $itemAlias, string $photoAlias): void
    {
        $photoFixture = $this->testHelper->getItemPhotoFixture($photoAlias);
        Assert::assertFalse(
            $this->testHelper->fileExists($photoFixture->getFile())
        );
        Assert::assertNull($this->testHelper->findItemPhotoByAlias($photoAlias));
    }
}
