<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Grifix\Demo\Application\Projection\ItemProjection\ItemDto;
use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Ui\Http\Action\Item\DeleteItemActionHandler;
use Grifix\Shared\Test\Common\HttpClient;
use PHPUnit\Framework\Assert;

class DeleteItemFeatureContext extends AbstractDemoContext
{
    private array $itemFiles = [];
    private array $itemImages = [];

    /**
     * @When I delete the item :item
     */
    public function iDeleteTheItem(string $itemAlias): void
    {
        $itemFixture = $this->testHelper->getItemFixture($itemAlias);
        $this->itemFiles[$itemAlias] = $this->testHelper->findItemByAlias($itemAlias)->getFile()->getPath();
        $this->itemImages[$itemAlias] = $this->testHelper->findItemByAlias($itemAlias)->getImage()->getPath();
        $this->getShared(HttpClient::class)->postAction(
            DeleteItemActionHandler::getAlias(),
            [
                DeleteItemActionHandler::PARAM_ITEM_ID => $itemFixture->getId()
            ]
        );
    }

    /**
     * @Then there should be no item :item
     */
    public function thereShouldBeNoItem(string $itemAlias): void
    {
        $item = $this->testHelper->findItemByAlias($itemAlias);
        Assert::assertNull($item);
        Assert::assertFalse($this->testHelper->fileExists($this->itemFiles[$itemAlias]));
        Assert::assertFalse($this->testHelper->fileExists($this->itemImages[$itemAlias]));
    }
}
