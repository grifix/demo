<?php /*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Ui\Common\Item\RequestValidator\AddItemPhotoRequestValidator;
use Grifix\Demo\Ui\Http\Action\Item\AddItemPhotoActionHandler;
use PHPUnit\Framework\Assert;

class AddItemPhotoFeatureContext extends AbstractDemoContext
{

    /**
     * @When I add new photo to the item :item gallery with data:
     */
    public function iAddNewPhotoToTheItemWithData(string $itemAlias, string $data): void
    {
        $data = $this->wrapArray(yaml_parse($data));
        $this->sendPostAction(
            AddItemPhotoActionHandler::getAlias(),
            [
                AddItemPhotoRequestValidator::ITEM_ID => $this->testHelper->getItemFixture($itemAlias)->getId(),
                AddItemPhotoRequestValidator::DESCRIPTION => $data->get('description'),
                AddItemPhotoRequestValidator::IMAGE => $this->createUploadedFile($this->testHelper->makePath($data->get('image'))),
                AddItemPhotoRequestValidator::SEQUENCE => $data->get('sequence')
            ]
        );
    }

    /**
     * @Then there should be the photo in the item :item gallery with data:
     */
    public function thereShouldBeThePhotoInTheItemGalleryWithData(string $itemAlias, string $expectedPhoto): void
    {
        $expectedPhoto = $this->wrapArray(yaml_parse($expectedPhoto));
        $actualPhoto = $this->testHelper->findItemPhotos($itemAlias)[0];
        Assert::assertEquals($expectedPhoto->get('image'), $actualPhoto->getImage()->getName());
        Assert::assertEquals($expectedPhoto->get('description'), $actualPhoto->getDescription());
        Assert::assertEquals($expectedPhoto->get('sequence'), $actualPhoto->getSequence());
    }
}
