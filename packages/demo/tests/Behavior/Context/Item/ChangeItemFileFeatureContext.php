<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\Fixture\Item\ItemFixture;
use Grifix\Demo\Ui\Common\Item\RequestValidator\ChangeItemFileRequestValidator;
use Grifix\Demo\Ui\Http\Action\Item\ChangeItemFileActionHandler;
use PHPUnit\Framework\Assert;

class ChangeItemFileFeatureContext extends AbstractDemoContext
{
    private string $oldItemFile;

    /**
     * @Given there is an item :item with file :file
     */
    public function thereIsAnItemWithFile(string $itemAlias, string $file): void
    {
        $fixture = new ItemFixture();
        $fixture->setFile($this->testHelper->makePath($file));
        $this->testHelper->createItem($fixture, $itemAlias);
        $this->oldItemFile = $file;
    }

    /**
     * @When I change item :item file to :file
     */
    public function iChangeItemFileTo(string $itemAlias, string $file): void
    {
        $this->httpClient->postAction(
            ChangeItemFileActionHandler::getAlias(),
            [
                ChangeItemFileRequestValidator::ITEM_ID => $this->testHelper->getItemFixture($itemAlias)->getId(),
                ChangeItemFileRequestValidator::FILE => $this->createUploadedFile($this->testHelper->makePath($file))
            ]
        );
    }

    /**
     * @When I change item :item file without a file
     */
    public function iChangeItemFileWithoutFile(string $itemAlias): void
    {
        $this->httpClient->postAction(
            ChangeItemFileActionHandler::getAlias(),
            [
                ChangeItemFileRequestValidator::ITEM_ID => $this->testHelper->getItemFixture($itemAlias)->getId(),
            ]
        );
    }

    /**
     * @Then item :item should have :file file
     */
    public function itemShouldHaveFile(string $itemAlias, string $expectedFile): void
    {
        $actualItem = $this->testHelper->findItemByAlias($itemAlias);
        Assert::assertEquals($expectedFile, $actualItem->getFile()->getName());
        Assert::assertTrue($this->testHelper->fileExists($actualItem->getFile()->getPath()));
    }

    /**
     * @Then there should be no file old item file
     */
    public function thereShouldBeNoOldItemFile(): void
    {
        Assert::assertFalse($this->testHelper->fileExists($this->testHelper->makePath($this->oldItemFile)));
    }

    /**
     * @Then I should get an error that there is no file
     */
    public function iShouldGetAnErrorThatThereIsNoFile()
    {
        $response = $this->httpClient->getLastResponseAsArray();
        Assert::assertEquals(400, $this->httpClient->getLastResponse()->getStatusCode());
        Assert::assertEquals('file', $response['errors'][0]['field']);
    }
}
