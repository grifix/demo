<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Grifix\Demo\Application\Projection\ItemProjection\ItemDto;
use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Ui\Http\Action\Item\DeleteItemFileActionHandler;
use PHPUnit\Framework\Assert;

class DeleteItemFileFeatureContext extends AbstractDemoContext
{

    /** @var string[] */
    private array $itemsFiles = [];

    /**
     * @When I delete the item :item file
     */
    public function iDeleteItemFile(string $itemAlias): void
    {
        $this->itemsFiles[$itemAlias] = $this->testHelper->findItemByAlias($itemAlias)->getFile()->getPath();
        $this->sendPostAction(
            DeleteItemFileActionHandler::getAlias(),
            [
                DeleteItemFileActionHandler::PARAM_ITEM_ID => $this->getShared(DemoFixtureRegistry::class)
                    ->getItemFixture($itemAlias)->getId()
            ]
        );
    }

    /**
     * @Then the item :item should have no file
     */
    public function theItemHasNoFile(string $itemAlias): void
    {
        $item = $this->testHelper->findItemByAlias($itemAlias);
        Assert::assertNull($item->getFile());
        Assert::assertFalse($this->testHelper->fileExists($this->itemsFiles[$itemAlias]));
    }
}
