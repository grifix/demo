<?php declare(strict_types = 1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Demo\Infrastructure\Table;
use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\DemoPathMaker;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Ui\Http\Action\Item\CreateItemActionHandler;
use Grifix\Kit\ArrayWrapper\ArrayWrapperFactoryInterface;
use Grifix\Kit\ArrayWrapper\ArrayWrapperInterface;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Type\DateTime\DateTime;
use Grifix\Shared\Alias;
use Grifix\Shared\Test\Behavior\Context\AbstractContext;
use Grifix\Shared\Test\Common\HttpClient;
use PHPUnit\Framework\Assert;

class CreateItemFeatureContext extends AbstractDemoContext
{
    protected ArrayWrapperInterface $newItem;

    /**
     * @When I create a new item:
     */
    public function createNewItem(string $post): void
    {
        $this->newItem = $this->wrapArray(yaml_parse($post));
        $post = $this->wrapArray(yaml_parse($post));
        $uploadedFiles = $this->wrapArray([]);
        $post->set(
            'producer_id',
            $this->testHelper->getProducerFixture($post->get('producer'))->getId()
        );
        if ($post->get('file')) {
            $this->makeUploadedFile('file', $uploadedFiles, $post);
        }
        if ($post->get('image')) {
            $this->makeUploadedFile('image', $uploadedFiles, $post);
        }

        $this->getShared(HttpClient::class)->postAction(
            CreateItemActionHandler::getAlias(),
            $post->getArray(),
            $uploadedFiles->getArray()
        );
    }

    /** @Then there should be a new item */
    public function thereShouldBeNewItem(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponse();
        Assert::assertEquals(200, $response->getStatusCode());
        $id = $this->getLastResponseWrapperArray()->get('id');
        $actualItem = $this->testHelper->findItemById($id);
        $producerId = $this->testHelper->getProducerFixture($this->newItem->get('producer'))->getId();

        Assert::assertEquals($producerId, $actualItem->getProducerId());
        Assert::assertEquals($this->newItem->get('name'), $actualItem->getName());
        Assert::assertEquals($this->newItem->get('title'), $actualItem->getTitle());
        Assert::assertEquals($this->newItem->get('date'), $this->timestampToDate($actualItem->getDate()));
        Assert::assertEquals($this->newItem->get('email'), $actualItem->getEmail());
        Assert::assertEquals($this->newItem->get('public'), $actualItem->getPublic());
        Assert::assertEquals($this->newItem->get('url'), $actualItem->getUrl());
        Assert::assertEquals($this->newItem->get('ip'), $actualItem->getIp());
        Assert::assertEquals($this->newItem->get('height'), $actualItem->getHeight());
        Assert::assertEquals($this->newItem->get('width'), $actualItem->getWidth());
        Assert::assertEquals($this->newItem->get('description'), $actualItem->getDescription());
        Assert::assertEquals($this->newItem->get('file'), $actualItem->getFile()->getName());
        Assert::assertEquals($this->newItem->get('image'), $actualItem->getImage()->getName());
        Assert::assertTrue($this->testHelper->fileExists($actualItem->getFile()->getPath()));
        Assert::assertTrue($this->testHelper->fileExists($actualItem->getFile()->getPath()));
    }

    /**
     * @Then I should get an error that file is too big
     */
    public function iShouldGetErrorThatFileIsTooBig(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponse();
        $responseArray = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertEquals(400, $response->getStatusCode());
        Assert::assertEquals($responseArray['key'], 'grifix.shared.tooBigFile');
    }

    /**
     * @Then I should get an error that file is too small
     */
    public function iShouldGetErrorThatFileIsTooSmall(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponse();
        $responseArray = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertEquals(400, $response->getStatusCode());
        Assert::assertEquals($responseArray['key'], 'grifix.shared.tooSmallFile');
    }

    /**
     * @Then I should get an error that file has wrong type
     */
    public function iShouldGetErrorThatFileHasWrongType(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponse();
        $responseArray = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertEquals(400, $response->getStatusCode());
        Assert::assertEquals($responseArray['key'], 'grifix.shared.invalidMimeType');
    }

    /**
     * @Then I should get an error that image is too narrow
     */
    public function iShouldGetErrorThatImageIsTooNarrow(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponse();
        $responseArray = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertEquals(400, $response->getStatusCode());
        Assert::assertEquals($responseArray['key'], 'grifix.shared.imageWidthIsTooSmall');
    }

    /**
     * @Then I should get an error that image is too wide
     */
    public function iShouldGetErrorThatImageIsTooWide(): void
    {
        $response = $this->getShared(HttpClient::class)->getLastResponse();
        $responseArray = $this->getShared(HttpClient::class)->getLastResponseAsArray();
        Assert::assertEquals(400, $response->getStatusCode());
        Assert::assertEquals($responseArray['key'], 'grifix.shared.imageWidthIsTooBig');
    }

    protected function makeUploadedFile(
        string $key,
        ArrayWrapperInterface $uploadedFiles,
        ArrayWrapperInterface $post
    ): void {
        $uploadedFiles->set($key, $this->createUploadedFile($this->testHelper->makePath($post->get($key))));
        $post->remove($key);
    }
}
