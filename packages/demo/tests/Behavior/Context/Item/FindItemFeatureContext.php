<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context\Item;

use Grifix\Demo\Application\Command\Item\ChangeProducer\ChangeItemProducerCommand;
use Grifix\Demo\Test\Behavior\Context\AbstractDemoContext;
use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Ui\Http\Action\Item\FindItemsActionHandler;
use Grifix\Kit\Test\Asserter;
use PHPUnit\Framework\Assert;

class FindItemFeatureContext extends AbstractDemoContext
{
    /**
     * @When I find item by id :id
     */
    public function iFindItemById(string $id): void
    {
        $this->sendGetAction(
            FindItemsActionHandler::getAlias(),
            [
                FindItemsActionHandler::PARAM_ID => $id,
                FindItemsActionHandler::PARAM_COUNT_TOTAL => true
            ]
        );
    }

    /**
     * @When I find item by id :id with photos
     */
    public function iFindItemByIdWithPhotos(string $id): void
    {
        $this->sendGetAction(
            FindItemsActionHandler::getAlias(),
            [
                FindItemsActionHandler::PARAM_ID => $id,
                FindItemsActionHandler::PARAM_COUNT_TOTAL => true,
                FindItemsActionHandler::PARAM_WITH_PHOTOS => true
            ]
        );
    }

    /**
     * @When I find item by id :id with producer
     */
    public function iFindItemByIdWithProducer(string $id): void
    {
        $this->sendGetAction(
            FindItemsActionHandler::getAlias(),
            [
                FindItemsActionHandler::PARAM_ID => $id,
                FindItemsActionHandler::PARAM_COUNT_TOTAL => true,
                FindItemsActionHandler::PARAM_WITH_PRODUCER => true
            ]
        );
    }

    /**
     * @Then I should find :quantity items
     */
    public function iShouldFindQuantityItem(int $quantity): void
    {
        $response = $this->getLastResponseArray();
        Assert::assertEquals($quantity, $response['result']['total']);
    }

    /**
     * @Then I should find :item item
     */
    public function iShouldFindItem(string $itemAlias): void
    {
        Assert::assertNotEmpty($this->getItemFromResponse($this->getLastResponseArray(), $itemAlias));
    }

    /**
     * @Then I should find :item item with photos:
     */
    public function iShouldFindItemWithPhotos(string $itemAlias, string $photos): void
    {
        $photos = yaml_parse($photos);
        $this->iShouldFindItem($itemAlias);
        $response = $this->getLastResponseArray();
        $item = $this->getItemFromResponse($response, $itemAlias);

        $actualPhotos = [];
        foreach ($item['photos'] as $photo) {
            $actualPhotos[] = $photo['id'];
        }

        $expectedPhotos = [];
        foreach ($photos as $photoAlias) {
            $expectedPhotos[] = $this->testHelper->getItemPhotoFixture($photoAlias)->getId();
        }

        Assert::assertEquals(count($photos), count($item['photos']));
        Asserter::assertArraySubset($expectedPhotos, $actualPhotos);
    }

    /**
     * @Given the item :item has a producer :producer
     */
    public function theItemHasProducer(string $itemAlias, string $producerAlias): void
    {
        $this->executeCommand(new ChangeItemProducerCommand(
            $this->testHelper->getItemFixture($itemAlias)->getId(),
            $this->testHelper->getProducerFixture($producerAlias)->getId()
        ));
    }

    /**
     * @Then I should find item :item with producer:
     */
    public function iShouldFindItemWithProducer(string $itemAlias, string $producerData): void
    {
        $producerData = yaml_parse($producerData);
        $response = $this->getLastResponseArray();
        $item = $response['result']['items'][0];
        Assert::assertNotEmpty($item);
        Asserter::assertArraySubset($producerData, $item['producer']);
    }

    protected function getItemFromResponse(array $response, string $itemAlias): ?array
    {
        $itemFixture = $this->getShared(DemoFixtureRegistry::class)->getItemFixture($itemAlias);
        foreach ($response['result']['items'] as $item) {
            if ($item['id'] === $itemFixture->getId()) {
                return $item;
            }
        }
        return null;
    }
}
