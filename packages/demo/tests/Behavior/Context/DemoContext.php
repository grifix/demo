<?php declare(strict_types=1);
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace Grifix\Demo\Test\Behavior\Context;

use Grifix\Demo\Test\Common\Fixture\DemoFixtureRegistry;
use Grifix\Demo\Test\Common\Fixture\Item\ItemFixture;
use Grifix\Demo\Test\Common\Fixture\Producer\ProducerFixture;

class DemoContext extends AbstractDemoContext
{
    /**
     * @Given there is a producer :producer
     * @Given there is a producer :producer with data:
     */
    public function thereIsProducer(string $alias, ?string $data = null): void
    {
        null === $data ? $data = [] : $data = yaml_parse($data);

        $data = $this->wrapArray($data);

        $producer = new ProducerFixture();
        $producer->setId($data->get('id'));
        $producer->setName($data->get('name'));
        $producer->setTitle($data->get('title'));
        $producer->setRating($data->get('rating'));
        $this->testHelper->createProducer($producer, $alias);
    }

    /**
     * @Given there is an item :item
     * @Given there is an item :item with data:
     */
    public function thereIsAnItemWithData(string $alias, string $data = '[]'): void
    {
        $data = $this->wrapArray(yaml_parse($data));
        $fixture = new ItemFixture();
        $fixture->setId($data->get('id'));
        $fixture->setProducerId($data->get('producerId'));
        $fixture->setName($data->get('name'));
        $fixture->setTitle($data->get('title'));
        $fixture->setStatus($data->get('status'));
        if ($data->get('date')) {
            $fixture->setDate($this->dateToTimestamp($data->get('date')));
        }
        $fixture->setEmail($data->get('email'));
        $fixture->setFile($data->get('file'));
        $fixture->setImage($data->get('image'));
        $fixture->setPublic($data->get('public'));
        $fixture->setUrl($data->get('url'));
        $fixture->setIp($data->get('ip'));
        $fixture->setDescription($data->get('description'));
        $fixture->setWidth($data->get('width'));
        $fixture->setHeight($data->get('height'));
        if ($data->get('producer')) {
            $fixture->setProducerId(
                $this->getShared(DemoFixtureRegistry::class)
                    ->getProducerFixture($data->get('producer'))
                    ->getId()
            );
        }
        $this->testHelper->createItem($fixture, $alias);
    }

    /**
     * @Then I should get an error that producer name is not unique
     */
    public function iShouldGetAnErrorThatProducerNameIsNotUnique(): void
    {
        $this->assertHttpError(400, 'grifix.demo.producerNameMustBeUnique');
    }
}
