#!/usr/bin/env bash
set -e
set -x
docker-compose down
docker volume rm -f demo_db_test
docker-compose up -d --build
docker-compose exec app rm -f tmp/test_dump.sql
sleep 10
docker-compose exec app ./cli shared:clear_cache
docker-compose exec app ./cli
