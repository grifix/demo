#!/usr/bin/env bash

echo -n "WARNING! This command will destroy project database with all data, if you are sure type \"yes\"? "
read -r answer
if [ "$answer" != "yes" ]
then
  exit 1
fi

echo -n "Are you really sure? "
read -r answer
if [ "$answer" != "yes" ]
then
  exit 1
fi

set -e
set -x
docker-compose down

docker volume rm -f demo_db
docker volume rm -f demo_db_test
docker volume rm -f demo_rabbit
docker volume rm -f demo_rabbit_test

docker-compose up -d --build
docker-compose exec app rm -f tmp/test_dump.sql
docker-compose exec app composer self-update
sleep 10
docker-compose exec app composer install -vvv
docker-compose exec app ./cli demo_stock:create_fixtures
docker-compose exec app ./cli demo_content:create_fixtures
docker-compose exec app ./cli acl:set_password admin@grifix.net abc_123_ABC

