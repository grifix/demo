<?php

namespace PHPSTORM_META {

    use Grifix\Kit\Action\AbstractAction;
    use Grifix\Kit\Alias;
    use Grifix\Kit\Behat\WebUiContext;
    use Grifix\Kit\Cli\AbstractCommand;
    use Grifix\Kit\Conversion\Field\ConversionFieldInterface;
    use Grifix\Kit\Cqrs\AbstractHandler;
    use Grifix\Kit\Cqrs\Command\CommandBus\CommandBusInterface;
    use Grifix\Kit\Cqrs\Query\QueryBus\QueryBus;
    use Grifix\Kit\Cqrs\Query\QueryBus\QueryBusInterface;
    use Grifix\Kit\Db\Connection\ConnectionInterface;
    use Grifix\Kit\Db\Query\QueryInterface;
    use Grifix\Kit\EntryPoint\AbstractBootstrap;
    use Grifix\Kit\Filesystem\FilesystemInterface;
    use Grifix\Kit\Fixture\AbstractFixtureMother;
    use Grifix\Kit\Fixture\AbstractFixtureMother;
    use Grifix\Kit\Ioc\IocContainerInterface;
    use Grifix\Kit\Ioc\ServiceLocatorTrait;
    use Grifix\Kit\Kernel\ClassMakerTrait;
    use Grifix\Kit\Kernel\Module\ModuleClassInterface;
    use Grifix\Kit\Kernel\Module\ModuleClassTrait;
    use Grifix\Kit\Test\Integration\AbstractTest;
    use Grifix\Kit\Validation\Field\ValidationFieldInterface;
    use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;
    use Grifix\Kit\View\Helper\AbstractViewHelper;
    use Grifix\Kit\View\View;
    use Grifix\Kit\View\ViewInterface;
    use Grifix\Shared\Alias as SharedAlias;
    use Grifix\Shared\Test\Bdd\Context\AbstractContext;
    use Grifix\Shared\Test\Bdd\Context\ApplicationContext;
    use Psr\SimpleCache\CacheInterface;

    $mappings = [
        '' => '@',
        Alias::CACHE_DB => CacheInterface::class,
        Alias::SLOW_CACHE => CacheInterface::class,
        Alias::QUICK_CACHE => CacheInterface::class,
        Alias::SYNC_COMMAND_BUS => CommandBusInterface::class,
        Alias::ASYNC_COMMAND_BUS => CommandBusInterface::class,
        SharedAlias::PUBLIC_FILESYSTEM => FilesystemInterface::class
    ];

    override(
        IocContainerInterface::get(0),
        map($mappings)
    );

    override(
        QueryBusInterface::execute(0),
        map([
            '' => '@Result'
        ])
    );

    override(
        IocContainerInterface::createNewInstance(0, 1),
        map($mappings)
    );

    override(
        ModuleClassTrait::getShared(0),
        map($mappings)
    );

    override(
        ModuleClassInterface::getShared(0),
        map($mappings)
    );

    override(
        AbstractContext::getShared(0),
        map($mappings)
    );

    override(
        AbstractContext::createInstance(0),
        map($mappings)
    );

    override(
        AbstractViewHelper::getShared(0),
        map($mappings)
    );

    override(
        AbstractContext::getContext(0),
        map(['' => '@'])
    );

    override(
        ViewInterface::getHelper(0),
        map(['' => '@'])
    );

    override(
        ClassMakerTrait::makeClassName(0),
        map($mappings)
    );

    override(
        AbstractTest::getShared(0),
        map($mappings)
    );

    override(
        ServiceLocatorTrait::getShared(0),
        map($mappings)
    );

    override(
        AbstractHandler::getShared(0),
        map($mappings)
    );

    override(
        AbstractCommand::getShared(0),
        map($mappings)
    );

    override(
        View::getShared(0),
        map($mappings)
    );

    override(
        ApplicationContext::getShared(0),
        map($mappings)
    );

    override(
        AbstractBootstrap::getShared(0),
        map($mappings)
    );

    override(
        ViewInterface::getShared(0),
        map($mappings)
    );

    override(
        WebUiContext::getShared(0),
        map($mappings)
    );

    override(
        AbstractFixtureMother::getShared(0),
        map($mappings)
    );

    override(
        QueryInterface::findPart(0),
        map(['' => '@'])
    );

    override(
        ValidatorFactoryInterface::createValidator(0),
        map(['' => '@'])
    );

    override(
        ValidationFieldInterface::createValidator(0),
        map(['' => '@'])
    );

    override(
        ValidationFieldInterface::getValidator(0),
        map(['' => '@'])
    );

    override(
        ConversionFieldInterface::getConverter(0),
        map(['' => '@'])
    );

    override(
        ConversionFieldInterface::createConverter(0),
        map(['' => '@'])
    );

    override(
        \Mockery::mock(0),
        map(['' => '@'])
    );
}
