<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise;


use App\Grifel\Workout\Domain\Exercise\Name\ExerciseName;
use App\Grifel\Workout\Domain\Exercise\Name\ExerciseNameFactory;
use Grifix\Shared\Domain\Uuid\UuidFactoryInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class ExerciseDomain
{
    private UuidFactoryInterface $uuidFactory;

    private ExerciseNameFactory $exerciseNameFactory;

    public function createUuid(string $value): UuidInterface
    {
        return $this->uuidFactory->createUuid($value);
    }

    public function createName(string $value, string $exerciseId, string $sportsmanId): ExerciseName
    {
        return $this->exerciseNameFactory->create($value, $exerciseId, $sportsmanId);
    }
}
