<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Name\Exception;

class ExerciseNameAlreadyExistsException extends \DomainException
{

    public function __construct(string $name)
    {
        parent::__construct(sprintf('Exercise with name %s already exists', $name));
    }
}