<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Name;

use App\Grifel\Workout\Domain\Exercise\Name\Exception\ExerciseNameAlreadyExistsException;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;

class ExerciseName
{
    private string $value;

    public function __construct(
        #[Dependency]
        private ExerciseNameInfrastructureInterface $infrastructure,
        string $value,
        string $exerciseId,
        string $sportsmanId
    )
    {
        $this->infrastructure = $infrastructure;
        $this->assertNameNotExists($value, $exerciseId, $sportsmanId);

        $this->value = $value;
    }

    private function assertNameNotExists(string $name, string $exerciseId, string $sportsmanId)
    {
        if ($this->infrastructure->nameExists($name, $exerciseId, $sportsmanId)) {
            throw new ExerciseNameAlreadyExistsException($name);
        }
    }
}
