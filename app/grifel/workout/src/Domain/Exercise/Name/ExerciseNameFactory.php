<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Name;

class ExerciseNameFactory
{
    private ExerciseNameInfrastructureInterface $exerciseNameInfrastructure;

    public function create(string $value, string $exerciseId, string $sportsmanId): ExerciseName
    {
        return new ExerciseName($this->exerciseNameInfrastructure, $value, $exerciseId, $sportsmanId);
    }
}