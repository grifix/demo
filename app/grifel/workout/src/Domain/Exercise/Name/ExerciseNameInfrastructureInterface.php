<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Name;

interface ExerciseNameInfrastructureInterface
{
    public function nameExists(string $name, string $exerciseId, string $sportsmanId):bool;
}