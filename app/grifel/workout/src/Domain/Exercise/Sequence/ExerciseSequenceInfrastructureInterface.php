<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Sequence;

interface ExerciseSequenceInfrastructureInterface
{
    public function getMaxSequence(string $sportsmanId): int;

    public function sequenceExists(int $sequence, string $sportsmanId): bool;
}
