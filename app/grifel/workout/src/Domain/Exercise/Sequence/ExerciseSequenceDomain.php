<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Sequence;

use Grifix\Shared\Domain\PositiveInt\PositiveIntFactoryInterface;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;

class ExerciseSequenceDomain
{
    private PositiveIntFactoryInterface $positiveIntFactory;

    /**
     * ExerciseSequenceDomain constructor.
     * @param PositiveIntFactoryInterface $positiveIntFactory
     */
    public function __construct(PositiveIntFactoryInterface $positiveIntFactory)
    {
        $this->positiveIntFactory = $positiveIntFactory;
    }

    public function createPositiveInt(int $value): PositiveIntInterface
    {
        return $this->positiveIntFactory->createPositiveInt($value);
    }
}
