<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Sequence;

use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;

class ExerciseSequence
{
    private PositiveIntInterface $value;

    public function __construct(
        #[Dependency]
        private ExerciseSequenceDomain $domain,
        #[Dependency]
        private ExerciseSequenceInfrastructureInterface $infrastructure,
        string $sportsmanId,
        ?int $value
    ) {
        $this->infrastructure = $infrastructure;
        $this->domain = $domain;
        $maxSequence = $this->infrastructure->getMaxSequence($sportsmanId);
        if (null === $value) {
            $value = $this->infrastructure->getMaxSequence($sportsmanId) + 1;
        }
        if ($value > $maxSequence + 1) {

        }

        $this->value = $domain->createPositiveInt($value);
    }
}
