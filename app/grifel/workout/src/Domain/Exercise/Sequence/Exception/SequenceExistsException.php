<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise\Sequence\Exception;

class SequenceExistsException extends \DomainException
{

    /**
     * SequenceExistsException constructor.
     */
    public function __construct(int $sequence)
    {
    }
}
