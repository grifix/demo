<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

namespace App\Grifel\Workout\Domain\Exercise;

use App\Grifel\Workout\Domain\Exercise\Name\ExerciseName;
use Grifix\Kit\Orm\PropertyAnalyzer\Attribute\Dependency;
use Grifix\Shared\Domain\PositiveInt\PositiveIntInterface;
use Grifix\Shared\Domain\Uuid\UuidInterface;

class Exercise
{
    private UuidInterface $id;

    private ExerciseName $name;

    private UuidInterface $sportsmanId;

    private PositiveIntInterface $sequence;

    public function __construct(
        #[Dependency]
        private ExerciseInfrastructureInterface $infrastructure,
        #[Dependency]
        private ExerciseDomain $domain,
        string $sportsmanId,
        string $id,
        string $name
    ) {
        $this->infrastructure = $infrastructure;
        $this->domain = $domain;
        $this->sportsmanId = $domain->createUuid($sportsmanId);
        $this->id = $domain->createUuid($id);
        $this->name = $domain->createName($name, $id, $sportsmanId);
    }

    public function changeSequence(int $newSequence):void{

    }
}
