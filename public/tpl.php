<?php
    $height = getimagesize($_GET['tpl'].'.png')[1];
?>

<html>
<head>
    <style>
        html, body {
            padding: 0;
            margin: 0;
            height: 100%;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
        }
    </style>
</head>
<body>
<div style="width: 100%; height: <?=$height?>px; background: url('<?=$_GET['tpl']?>.png') no-repeat -170px"></div>
</body>
</html>
