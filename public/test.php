<?php
/*
 * (c) Mike Shapovalov <smike.mbx@gmail.com>
 * For the full copyright and license information, please view the LICENSE file that was
 * distributed with this source code.
 */

declare(strict_types=1);
$response = microtime();
header("Connection: close");
header("Content-Length: " . mb_strlen($response));
echo $response;

flush();
sleep(10);
